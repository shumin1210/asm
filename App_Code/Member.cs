﻿using System;
using GameLibrary;
using Tool;
using System.Data;
using System.Collections.Concurrent;
using System.Collections.Generic;

public static class Member {

    public static bool Login(IDataBase sql, string account, string password, Player p) {
        var cmd = CommandBuilder.Instance;
        sql.ClearParameters();
        sql.TryAddParameter("keystring", account);
        sql.TryAddParameter("check_key", password);
        Login_SelectCmd(cmd);
        cmd.WHERE_params("keystring", "check_key");
        var dt = sql.ExecuteDataTable(cmd.Result);
        bool result = false;
        if (dt.Rows.Count == 0) return false;

        if (!ShowData_Permission.isValid(dt.Rows[0])) return false;

        SetPlayerVal(sql, dt, p);
        p.LastActTime = DateTime.Now;
        
        if (string.IsNullOrWhiteSpace(p.Token)) {
            p.Token = Tool.Random.Generator(6);
            cmd.Clear();
            cmd.UPDATE("userinfo", sql).addColumnAndValue("autcode", p.Token).WHERE_params("keystring", "check_key");
            sql.TryAddParameter("keystring", account);
            sql.TryAddParameter("check_key", password);

            sql.Execute(cmd.Result);
        }
        result = true;

        UpdateLoginTime(sql, p);

        cmd.Free();
        return result;
    }
    public static bool Login(IDataBase sql, string autcode, Player p) {
        var cmd = CommandBuilder.Instance;
        Login_SelectCmd(cmd);
        sql.ClearParameters();
        sql.TryAddParameter("autcode", autcode);
        cmd.WHERE_params("autcode");
        var dt = sql.ExecuteDataTable(cmd.Result);
        cmd.Free();

        if (dt.Rows.Count != 1) return false;

        if (!ShowData_Permission.isValid(dt.Rows[0])) return false;

        SetPlayerVal(sql, dt, p);

        UpdateLoginTime(sql, p);

        return true;
    }

    public static bool GetPlayerData(IDataBase sql, int UID, Player p) {
        var cmd = CommandBuilder.Instance;
        Login_SelectCmd(cmd);
        sql.ClearParameters();
        sql.TryAddParameter("UID", UID);
        cmd.WHERE_params("userinfo.UID");
        var dt = sql.ExecuteDataTable(cmd.Result);
        cmd.Free();

        if (dt.Rows.Count != 1) return false;
                
        SetPlayerVal(sql, dt, p);

        return true;
    }
    #region private method
    static CommandBuilder Login_SelectCmd(CommandBuilder cmd) {
        cmd.SELECT("userinfo.uid",
                "nickname AS name",
                "KeyString",
                "bankcredit AS ud",
                "AccessLevel AS aclv",
                "gamefee.refund",
                "UpLevelTree",
                "UpLevelId",
                "canAddNextLevel",
                "canSeeNextLevel",
                "Currency",
                "Role",
                "betStatus",
                "CompanyBan",
                "autcode",
                "CreditMax",
                "State",
                "CreditRemain")
            .FROM("userinfo")
            .LEFT_JOIN("gamemanager",
                "gamemanager.uid=userinfo.uid")
            .LEFT_JOIN("gamefee", "gamefee.uid=userinfo.uid");
        return cmd;
    }

    static Player SetPlayerVal(IDataBase sql, DataTable dt, Player p) {
        p.Token = dt.Rows[0]["autcode"].ToString();
        p["canAddNextLevel"] = TConvert.ToInt(dt.Rows[0]["canAddNextLevel"], 0);
        p["canSeeNextLevel"] = TConvert.ToInt(dt.Rows[0]["canSeeNextLevel"]);
        p["UpLevelId"] = dt.Rows[0]["UpLevelId"].ToString();
        p["UpLevelTree"] = dt.Rows[0]["UpLevelTree"].ToString();
        p.originUD = p.UD = Convert.ToInt64(dt.Rows[0]["ud"].ToString());
        p.originRefund = p.Refund = TConvert.ToDouble(dt.Rows[0]["refund"].ToString(), 0);
        p.ID = TConvert.ToInt(dt.Rows[0]["uid"]);
        p.Name = dt.Rows[0]["name"].ToString();
        p.isAuthorized = true;
        p.ImportManagerAccess(dt.Rows[0]["aclv"].ToString());
        p.Role = TConvert.ToInt(dt.Rows[0]["Role"]);
        p["Currency"] = TConvert.ToInt(dt.Rows[0]["Currency"]);
        p["betStatus"] = TConvert.ToInt(dt.Rows[0]["betStatus"]);
        p["CompanyBan"] = TConvert.ToInt(dt.Rows[0]["CompanyBan"]);
        p["Status"] = TConvert.ToInt(dt.Rows[0]["betStatus"]) == 2 || TConvert.ToInt(dt.Rows[0]["CompanyBan"]) == 2 ? 1 : 0;
        p["CreditMax"] = Convert.ToInt64(dt.Rows[0]["CreditMax"]);
        p["KeyString"] = dt.Rows[0]["KeyString"].ToString();
        p["Language"] = Languages.Default;
        p["State"] = dt.Rows[0]["State"].ToString();
        p["CreditRemain"] = Convert.ToInt64(dt.Rows[0]["CreditRemain"]);
        p["LoginTime"] = DateTime.Now;

        /*公司稱謂更新*/
        if (Controller.CompanyTitle == null)
            Controller.CompanyTitle = new ConcurrentDictionary<int, string>();
        if (System.Web.Configuration.WebConfigurationManager.AppSettings["FixedGID"] != null) {
            int GID = Convert.ToInt32(System.Web.Configuration.WebConfigurationManager.AppSettings["FixedGID"]);
            int UID = GetTopLevelUID(p.ID, dt.Rows[0]["UpLevelTree"].ToString());
            DataTable titleList = GetCompanyTitle(sql, UID, GID);
            int key = 0;
            for (int i = 0; i < titleList.Rows.Count; i++) {
                key = TConvert.ToInt(titleList.Rows[i]["Level"], 0);
                if (!Controller.CompanyTitle.ContainsKey(key))
                    Controller.CompanyTitle.TryAdd(key, titleList.Rows[i]["Title"].ToString());
            }
        }
        string title;
        int level = GetLevel(p);
        CompanyTitle.Get(p, level, out title);

        p["title"] = title;
        p["Level"] = level;

        return p;
    }

    #endregion
    public static long UpdateUserUD(IDataBase sql, Player p) {
        if (p.ID == 0) return 0;
        var cmd = CommandBuilder.Instance;
        string sqlcmd = "";

        long dif_ud = p.UD - p.originUD;
        if (dif_ud != 0) {
            cmd.db = sql;
            sqlcmd = cmd.UPDATE("userinfo")
                .addColumn("bankcredit").addFunc("bankcredit", "bankcredit+" + dif_ud)
                .WHERE_params("uid").Result;
        }
        sql.TryAddParameter("uid", p.ID);
        var dt = sql.ExecuteDataTable(sqlcmd + "SELECT bankcredit FROM userinfo WHERE uid=@uid");

        p.UD = Convert.ToInt64(dt.Rows[0]["bankcredit"].ToString());
        p.originUD = p.UD;
        
        return p.UD;
    }

    public static void CashFlow(IDataBase sql, iController ctr, Player target, long amount, Player manager) {
        CommandBuilder cmd = CommandBuilder.Instance;
        cmd.INSERT_INTO("cashflow", sql).addColumnAndValue(
            "uid", target.ID,
            "manager", manager.ID,
            "amount", amount,
            "controllerid", ctr.controllerID)
            .addTime("time");
        sql.Execute(cmd.Result);
        cmd.Free();
    }

    public static void UpdateRole(IDataBase sql, int UID, int Role) {
        if (UID == 0) return;
        string sqlcmd = "";

        sqlcmd = "UPDATE userinfo SET canAddNextLevel=@Role ";
        sqlcmd += " WHERE UID=@UID";

        sql.TryAddParameter("UID", UID);
        sql.TryAddParameter("Role", Role);
        sql.Execute(sqlcmd);
    }

    /// <summary> 是否為代理商&&是否被停權(代理商權限) </summary>
    public static bool isCompanyPermission(Player p) {
        if (Convert.ToInt32(p["Status"]) == 1)
            /*被停權*/
            return false;

        return Convert.ToInt32(p["canAddNextLevel"]) == 1;
    }

    public static bool isSharedHolderPermission(Player p) {
        if (Convert.ToInt32(p["Status"]) == 1)
            /*被停權*/
            return false;

        return Convert.ToInt32(p["canAddNextLevel"]) == 2;
    }

    public static bool CheckKeyString(IDataBase sql, string keyString, int uid) {

        string sqlcmd = "";

        sqlcmd = "SELECT COUNT(UID) ";
        sqlcmd += " FROM userinfo ";
        sqlcmd += " WHERE KeyString=@KeyString";

        sql.ClearParameters();
        sql.TryAddParameter("KeyString", keyString);

        if (uid != 0) {
            sqlcmd += " AND UID!=@UID ";
            sql.TryAddParameter("UID", uid);
        }

        return Convert.ToInt32(sql.ExecuteScalar(sqlcmd)) == 0;
    }

    public static DataTable GetCompanyTitle(IDataBase sql, int UID, int GID) {
        string sqlcmd = "";
        sqlcmd = "SELECT * ";
        sqlcmd += " FROM reportcompanytitle t ";
        sqlcmd += " LEFT JOIN reportcompany r ON t.rid=r.rid";
        sqlcmd += " WHERE r.UID=@UID AND r.GID=@GID ";

        sql.ClearParameters();
        sql.TryAddParameter("UID", UID);
        sql.TryAddParameter("GID", GID);

        return sql.ExecuteDataTable(sqlcmd);
    }

    public static bool isAdmin(string uplevelTree) {
        if (String.IsNullOrEmpty(uplevelTree))
            return false;

        string[] tmp = uplevelTree.Split(',');

        return tmp.Length == 3;
    }

    public static bool isMember(DataRow dr) {
        return dr["canAddNextLevel"].ToString() == "0";
    }

    public static bool isMember(DataRowView dr) {
        return dr["canAddNextLevel"].ToString() == "0";
    }

    public static bool isMember(Player p) {
        return p["canAddNextLevel"].ToString() == "0";
    }

    public static int GetTopLevelUID(int UID, string uplevelTree) {
        if (String.IsNullOrEmpty(uplevelTree))
            return 0;

        string[] tmp = uplevelTree.Split(',');
        if (tmp.Length > 2) {
            if (tmp.Length == 3)/*Is Admin*/
                return UID;
            else
                return TConvert.ToInt(tmp[2], 0);
        }

        return 0;
    }

    public static int GetLevel(string uplevelTree) {
        int defaultVal = -1;
        if (String.IsNullOrEmpty(uplevelTree))
            return defaultVal;

        string[] tmp = uplevelTree.Split(',');
        if (tmp.Length > 2) {
            if (tmp.Length == 3)/*Is Admin*/
                return 1;
            else {
                return tmp.Length - 2;
            }
        }

        return defaultVal;
    }

    public static int GetLevel(Player p) {
        int defaultVal = -1;
        if (String.IsNullOrEmpty(p["UpLevelTree"].ToString()))
            return defaultVal;

        string uplevelTree = p["UpLevelTree"].ToString();

        if (isSharedHolderPermission(p)) {
            string[] tmp = uplevelTree.Split(',');
            List<string> tmp_list = new List<string>();

            for (int i = 0; i < tmp.Length - 2; i++) {
                tmp_list.Add(tmp[i]);
            }
            tmp_list.Add("");

            uplevelTree = string.Join(",", tmp_list);
        }
            

        return GetLevel(uplevelTree);
    }

    public static bool CloseRefund(IDataBase sql, int gameid, Player p, int digit = 3, int uid = 0) {
        sql.ClearParameters();
        sql.TryAddParameter("uid", p.ID);
        sql.TryAddParameter("gid", gameid);
        sql.TryAddParameter("digit", digit);
        try {
            sql.ExecuteScalar("CALL DrawClosedRefundByDigit(@uid,@gid,@digit);");
            UpdateUserUD(sql, p);
            RefreshRefund(sql, gameid, p);
        } catch (Exception) {
            return false;
        }
        return true;
    }

    public static void RefreshRefund(IDataBase sql, int gameid, Player p) {
        sql.ClearParameters();
        sql.TryAddParameter("gid", gameid);
        sql.TryAddParameter("uid", p.ID);
        //long result = TConvert.ToLong(sql.ExecuteScalar("CALL SelGameClosedRefund(@uid,@gid)"));
        //p.originRefund = p.Refund = result;
    }

    public static void UpdateLoginTime(IDataBase sql, Player p) {
        sql.ClearParameters();
        sql.TryAddParameter("uid", p.ID);
        sql.TryAddParameter("ip", p.MsgHandler != null?p.MsgHandler.getIP():"");

        string str = "INSERT INTO report_login (CreateDate,UID,ip) VALUES (now(),@uid,@ip) ";
        sql.Execute(str);
    }

    public static bool EditPassword(IDataBase sql, Player p, string oldPwd, string newPwd, out string reason) {
        reason = "";

        sql.ClearParameters();

        sql.TryAddParameter("oldPwd", oldPwd);
        sql.TryAddParameter("newPwd", newPwd);
        sql.TryAddParameter("ID", p.ID);

        /*Check Old Pwd*/
        string strQuery = @"SELECT COUNT(UID) FROM userinfo u WHERE Check_Key=@oldPwd AND UID=@ID ";

        int tmp = Convert.ToInt32(sql.ExecuteScalar(strQuery));
        if (tmp == 0) {
            reason = "舊密碼輸入不正確!";
            return false;
        }

        /*Update New Pwd*/
        strQuery = @"UPDATE userinfo SET Check_Key=@newPwd WHERE UID=@ID ";
        sql.Execute(strQuery);

        reason = "修改成功!";
        return true;
    }

    public static bool HasMember(IDataBase sql, Player p) {
        if (!isCompanyPermission(p))
            return false;

        var cmd = CommandBuilder.Instance;
        sql.ClearParameters();
        sql.TryAddParameter("upleveltree", p["UpLevelTree"].ToString() + p.ID + ",%");
        cmd.addTable("userinfo", "u");

        cmd.SELECT("COUNT(u.UID)")
            .FROM("u")
            .WHERE_condition("UpLevelTree LIKE @upleveltree")
            .WHERE_condition("State=1")
            .WHERE_condition("canAddNextLevel=0");

        long count = Convert.ToInt64(sql.ExecuteScalar(cmd.Result));
        cmd.Free();

        return count > 0;
    }

    public static bool CheckNickName(IDataBase sql, string name, int UID) {
        var cmd = CommandBuilder.Instance;
        sql.ClearParameters();
        sql.TryAddParameter("name", name);
        cmd.addTable("userinfo", "u");

        cmd.SELECT("COUNT(u.UID)")
            .FROM("u")
            .WHERE_condition("nickname=@name");
        if (UID != 0) {
            cmd.WHERE_condition("UID!=@UID");
            sql.TryAddParameter("UID", UID);
        }

        long count = Convert.ToInt64(sql.ExecuteScalar(cmd.Result));
        cmd.Free();

        return count == 0;
    }
}