﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using GameLibrary;

/// <summary>
/// Rule 的摘要描述
/// </summary>
public static class ShowData_Permission {
    /// <summary> 顯示資料(代理商/普通會員) </summary>
    /// <param name="type"> 會員:0,代理商:1,股東:2,點擊後會根據各層級改變</param>
    public static bool isAll(DataRowView dr, int type) {
        return ((type == 1 || type == 2) && Convert.ToInt32(dr["canAddNextLevel"]) == 1) ||
                       (type == 0 && (Convert.ToInt32(dr["canAddNextLevel"]) == 0 || Convert.ToInt32(dr["canAddNextLevel"]) == 2));
    }

    /// <summary> 顯示資料(普通會員) </summary>
    ///  <param name="type"> 會員:0,代理商:1,股東:2,點擊後會根據各層級改變</param>
    public static bool isNormalMember(DataRowView dr, Player p, int type) {
        return dr["canAddNextLevel"].ToString() == "0" && type == 0 && p["canAddNextLevel"].ToString() == "0";
    }

    public static bool isNormalMember(DataRowView dr) {
        return dr["canAddNextLevel"].ToString() == "0";
    }

    public static bool isNormalMember(DataRow dr) {
        return dr["canAddNextLevel"].ToString() == "0";
    }

    public static bool isNormalMember(Player p) {
        return p["canAddNextLevel"].ToString() == "0";
    }

    /// <summary>代理商畫面的第1行會員資料(自己的玩家下線)</summary>
    /// <param name="upLevel">上線ID</param>
    public static bool Group_Member(DataRowView dr, Player p, string upLevel) {
        return upLevel == p.ID.ToString() && dr["canAddNextLevel"].ToString() == "0";
    }

    /// <summary>(自己的玩家下線)</summary>
    public static bool Group_Member(Player p, Player targetPlayer) {
        return targetPlayer["UpLevelId"].ToString() == p.ID.ToString() && targetPlayer["canAddNextLevel"].ToString() == "0";
    }

    /// <summary>自己的玩家&股東下線</summary>
    /// <param name="upLevel">上線ID</param>
    public static bool Lower_MemberAndStockholder(DataRowView dr, Player p, string upLevel) {
        return upLevel == p.ID.ToString() && (dr["canAddNextLevel"].ToString() == "0" || dr["canAddNextLevel"].ToString() == "2");
    }

    /// <summary>自己的玩家&股東下線</summary>
    /// <param name="upLevel">上線ID</param>
    public static bool Lower_MemberAndStockholder(DataRow dr, Player p, string upLevel) {
        return upLevel == p.ID.ToString() && (dr["canAddNextLevel"].ToString() == "0" || dr["canAddNextLevel"].ToString() == "2");
    }

    /// <summary> 確認帳號是否有效 </summary>
    public static bool isValid(DataRow dr) {
        if (dr["State"].ToString() == "0")
            /*Delete*/
            return false;

        /*Ban*/
        if (Ban.getIntStatus(dr) == BanType.BANNED) return false;

        return true;
    }

    public static bool isDelete(Player p) {
        return p["State"].ToString() == "0";
    }

    public static bool isDelete(DataRow dr) {
        return dr["State"].ToString() == "0";
    }

    public static bool isLowerLevel(Player p, string uplevelTree) {
        bool islower = false;
        string[] tmp = uplevelTree.Split(',');
        if (tmp.Length == 3) {
            /*admin*/
        } else if (tmp.Length > 3) {
            return Array.IndexOf(tmp, p.ID.ToString()) != -1;
        }

        return islower;
    }

    public static bool isAdmin(Player p) {
        string[] tmp = p["UpLevelTree"].ToString().Split(',');
        return tmp.Length == 3;
    }

    public static bool isCompany(Player p) {
        return p["canAddNextLevel"].ToString() == "1";
    }

    public static bool isStockHolder(Player p) {
        return p["canAddNextLevel"].ToString() == "2";
    }
}

public static class BanType {
    public static int ACTIVE = 0;
    public static int BANNED = 1;
}

public static class Ban {
    public static int getIntStatus(DataRow dr) {
        if (dr.Table.Columns.Contains("betStatus") && dr.Table.Columns.Contains("CompanyBan"))
            if (betStatus(dr) || CompanyBan(dr))
                /*被停權*/
                return BanType.BANNED;
        return BanType.ACTIVE;
    }

    public static bool betStatus(DataRow dr) {
        return dr["betStatus"].ToString() == "2";
    }

    public static bool CompanyBan(DataRow dr) {
        return dr["CompanyBan"].ToString() == "2";
    }

    public static int getIntStatus(Player p) {
        return p["betStatus"].ToString() == "2" || p["CompanyBan"].ToString() == "2" ? BanType.BANNED : BanType.ACTIVE;
    }

    public static bool IsBan(Player p) {
        return p["betStatus"].ToString() == "2" || p["CompanyBan"].ToString() == "2";
    }
}

public static class UpperLevelRecord {
    public static DataView Add(DataView target, DataTable total, int index, DataRowView playerRow) {
        DataRowView drv;
        return Add(target, total, index, playerRow, out drv);        
    }

    public static DataView Add(DataView target, DataTable total, int index, DataRowView playerRow, out DataRowView drv) {
        string pID = playerRow["uid"].ToString();
        string pName = playerRow["Name"].ToString();
        long creditMax = playerRow.Row.Table.Columns.Contains("CreditMax") && !Convert.IsDBNull(playerRow["CreditMax"]) ? Convert.ToInt64(playerRow["CreditMax"]) : 0;

        int j = target.Find(pID);

        if (j == -1) {
            //找不到資料
            int Currency = -1;
            if (!Convert.IsDBNull(playerRow["Currency"]))
                Currency = Convert.ToInt32(playerRow["Currency"]);

            int Status = Ban.getIntStatus(playerRow.Row);

            DataViewAdd(target, total, index, pID, pName, Currency, Status, creditMax, out drv);

        } else {
            DataViewEdit(target, j, total, index, pID, pName, creditMax, out drv);
        }

        return target;
    }

    public static DataView Add(DataView target, DataTable total,
        int index, string pID, string pName, object currency, int Status) {
        int _Currency = -1;
        if (!Convert.IsDBNull(currency))
            _Currency = Convert.ToInt32(currency);

        int j = target.Find(pID);

        if (j == -1) {
            //找不到資料
            DataViewAdd(target, total, index, pID, pName, _Currency, Status, 0);

        } else {
            DataViewEdit(target, j, total, index, pID, pName, 0);
        }

        return target;
    }

    static DataView DataViewAdd(DataView target, DataTable data,
    int index, string pID, string pName, int Currency,
    int Status, long creditMax, out DataRowView drv) {
        drv = target.AddNew();
        foreach (DataColumn item in data.Columns) {
            if (drv.Row.Table.Columns.Contains(item.ColumnName))
                drv[item.ToString()] = data.Rows[index][item.ToString()];
        }

        drv["Currency"] = Currency;
        drv["UserID"] = pID;
        drv["Name"] = pName;
        drv["Status"] = Status;
        if (drv.Row.Table.Columns.Contains("CreditMax") && drv.Row.Table.Columns.Contains("CanUseCredit")) {
            drv["CreditMax"] = Convert.ToInt64(drv["CreditMax"]) + creditMax;
            drv["CanUseCredit"] = creditMax;
        }
        drv.EndEdit();

        return target;
    }

    static DataView DataViewAdd(DataView target, DataTable data,
    int index, string pID, string pName, int Currency, int Status, long creditMax) {
        DataRowView drv;
        return DataViewAdd(target, data, index, pID, pName, Currency, Status, creditMax, out drv);
    }

    static DataView DataViewEdit(DataView target, int targetIndex, DataTable data, int DataIndex,
        string pID, string pName, long creditMax) {
        DataRowView tmp;
        return DataViewEdit(target, targetIndex, data, DataIndex, pID, pName, creditMax, out tmp);
        
    }

    static DataView DataViewEdit(DataView target, int targetIndex, DataTable data, int DataIndex,
        string pID, string pName, long creditMax, out DataRowView drv) {
        drv = target[targetIndex];
        double tmp;
        drv.BeginEdit();
        string[] ExceptItem = new string[] { "Name", "Currency", "betStatus", "CompanyBan", "Status", "CanUseCredit", "UserUD", "RemainRefund" };
        foreach (DataColumn item in data.Columns) {
            if (Array.IndexOf(ExceptItem, item.ToString()) != -1)
                continue;

            if (double.TryParse(data.Rows[DataIndex][item.ToString()].ToString(), out tmp))
                drv[item.ToString()] = Convert.ToDouble(drv[item.ToString()]) + tmp;
            //drv[item.ToString()] = Convert.ToDouble(drv[item.ToString()]) + Convert.ToDouble(data.Rows[DataIndex][item.ToString()]);
        }

        drv["UserID"] = pID;
        drv["Name"] = pName == "" ? drv["Name"] : pName;

        if (drv.Row.Table.Columns.Contains("CreditMax") && drv.Row.Table.Columns.Contains("CanUseCredit")) {
            drv["CreditMax"] = Convert.ToInt64(drv["CreditMax"]) + creditMax;
        }

        drv.EndEdit();

        return target;
    }
}

public static class Languages {
    public static int TW = 0;
    public static int EN = 1;

    static string[] len = new string[] { "TW", "EN" };

    static string _default = len[0];
    public static string Default {
        get {
            return _default;
        }
        set {
            _default = SetString(value);
        }
    }

    public static string Get(int Language) {
        if (Language >= len.Length)
            return len[0];
        return len[Language];
    }

    public static int GetInt(string Language) {
        int val = Array.IndexOf(len, Language);
        if (val == -1)
            return 0;
        return val;
    }

    public static string SetString(string Language) {
        int val = Array.IndexOf(len, Language);
        if (val == -1)
            return _default;
        return len[val];
    }
}

public static class CompanyTitle {
    public static string Get(Player p, int level, out string title) {
        if (!Controller.CompanyTitle.TryGetValue(level, out title)) {
            title = "";
        } else {
            var tmp = title.Split(';');
            title = tmp[Languages.GetInt(p["Language"].ToString())];
        }

        return title;
    }

    public static Player UpdatePlayer(Player p) {
        string title;
        CompanyTitle.Get(p, (int)p["Level"], out title);
        p["title"] = title;

        return p;
    }
}
