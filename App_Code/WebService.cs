﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Text;
using System.Data;

/// <summary>
/// WebService 的摘要描述
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// 若要允許使用 ASP.NET AJAX 從指令碼呼叫此 Web 服務，請取消註解下列一行。
// [System.Web.Script.Services.ScriptService]
public class WebService : System.Web.Services.WebService {

    public WebService () {

        //如果使用設計的元件，請取消註解下列一行
        //InitializeComponent(); 
    }

    [WebMethod]
    public string GetSubTypeOptions(string mainTypeId)
    {
        StringBuilder sb = new StringBuilder();
        if (!string.IsNullOrWhiteSpace(mainTypeId))
        {
            DataTable items = this.getSubType(mainTypeId);
            for (int i = 0; i < items.Rows.Count; i++)
            {
                sb.Append("<option value=\"" + items.Rows[i]["isubclassid"] + "\">" + items.Rows[i]["isubclasstitle"] + "</option>");
            }
        }
        return sb.ToString();
    }

    private DataTable getSubType(string mainTypId)
    {
        
        return new DataTable();
    }
    
}
