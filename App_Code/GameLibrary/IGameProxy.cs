﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Web.Script.Serialization;
using GameLibrary;
using WebSocketSharp;
using System.Data;

public class IGameProxy : WebSocket {
    /// <summary>代理者狀態</summary>
    public enum ProxyStatus { None, Actived, Reconnect, Stop }

    /// <summary>連線網址</summary>
    //public string URL { get; set; }
    /// <summary>代理者狀態</summary>
    public ProxyStatus Status { get; set; }
    public int uid { get; set; }

    /// <summary>json轉碼器</summary>
    public JavaScriptSerializer jser = new JavaScriptSerializer();

    public Dictionary<string, object> StoreMsg;

    /// <summary>建構式</summary>
    public IGameProxy(string url)
        : base(url) {
            StoreMsg = new Dictionary<string, object>();
        Status = ProxyStatus.None;
        this.OnError += OnErrored;
    }

    /// <summary>開啟連線</summary>
    public void Connected() {

        if (this.ReadyState != WebSocketState.Open) {
            this.Connect();
        }

    }

    /// <summary>重連</summary>
    public void Reconnect() {
        Status = ProxyStatus.Reconnect;
        Connected();
    }

    /// <summary>切斷連線</summary>
    public void Closed() {
        if (this.ReadyState == WebSocketState.Open) {
            this.Close();
            Status = ProxyStatus.Stop;
        }

    }
    /// <summary>發生錯誤事件</summary>
    public void OnErrored(object sender, ErrorEventArgs e) {
        try {
            if ((sender as WebSocket).ReadyState != WebSocketState.Closed) {
                (sender as WebSocket).Close();
            }
        } catch (Exception ex) {
            Console.WriteLine("Catch QuitErr:" + ex.Message);
        }
    }
    /// <summary>連線關閉事件</summary>
    public void OnCloseed(object sender, CloseEventArgs e) {
        try {
            if ((sender as WebSocket).ReadyState != WebSocketState.Closed) {
                (sender as WebSocket).Close();
            }
        } catch (Exception ex) {
            Console.WriteLine("Catch QuitErr:" + ex.Message);
        }
    }

    public void Send(string type, object msg) {
        if (this.ReadyState == WebSocketState.Connecting) {
            Thread.Sleep(10);
            this.Send(type, msg);
        } else if (this.ReadyState == WebSocketState.Open) {
            this.Send(jser.Serialize(new ActionObject(type, msg)));
        }

    }
    public void Send(string maintype, string subtype, object msg) {
        if (this.ReadyState == WebSocketState.Connecting) {
            Thread.Sleep(10);
            this.Send(maintype,subtype, msg);
        } else if (this.ReadyState == WebSocketState.Open) {
            this.Send(jser.Serialize(new ActionCommand(maintype, subtype, jser.Serialize(msg))));
        }

    }

}
