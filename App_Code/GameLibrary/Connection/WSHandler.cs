﻿using System.Security.Cryptography;
using System.Web.Script.Serialization;
using Microsoft.Web.WebSockets;
using Tool;
namespace GameLibrary {
    public class WSHandler : WebSocketHandler, iConnect {
        public static iController Controller;
        public static WebSocketCollection clients = new WebSocketCollection();
        static JavaScriptSerializer jser = new JavaScriptSerializer();
        static int count = 0;
        private string ID;
        public WSHandler(iController c) {
            Controller = c;
        }
        public override void OnOpen() {
            ID = count + "." + new RNGCryptoServiceProvider().Next(100000).ToString();
            count++;
            Controller.onOpen(this);
            clients.Add(this);
        }
        public override void OnClose() {
            clients.Remove(this);
            Controller.onClose(this);
        }
        public override void OnError() {
            Controller.onError(this);
        }
        public override void OnMessage(string message) {
            MsgPack msg;
            try {
                msg = jser.Deserialize<MsgPack>(message);
            } catch (System.Exception) { return; }
            Controller.onMessage(this, msg);
        }
        void iConnect.Send(object msg) {
            if (msg is string)
                base.Send(msg as string);
            else
                base.Send(jser.Serialize(msg));
        }
        public static void Broadcast(object msg) {
            var pmsg = jser.Serialize(msg);
            clients.Broadcast(pmsg);
        }
        void iConnect.Broadcast(object msg) {
            WSHandler.Broadcast(msg);
        }
        public static void SetController(iController ctr) {
            Controller = ctr;
            ctr.Initialize();
        }
        void iConnect.SetController(iController ctr) {
            WSHandler.SetController(ctr);
        }
        public string getIP() {
            return this.WebSocketContext.UserHostAddress;
        }
        string iConnect.getID() {
            return ID;
        }
        public void Disconnect() {
            this.Close();
        }
    }
}
