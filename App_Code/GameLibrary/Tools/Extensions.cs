﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Security.Cryptography;
using System.Text.RegularExpressions;
using System.Web.Script.Serialization;
namespace Tool {
        
    public static class RNGCryptoServiceProviderExtensions {
        private static byte[] rb = new byte[4];
        /// <summary>產生一個非負數的亂數</summary>  
        public static int Next(this RNGCryptoServiceProvider rngp) {
            rngp.GetBytes(rb);
            int value = BitConverter.ToInt32(rb, 0);
            return (value < 0) ? -value : value;
        }
        /// <summary>產生一個非負數且不超過最大值 max 的亂數</summary>  
        /// <param name="max">最大值</param>  
        public static int Next(this RNGCryptoServiceProvider rngp, int max) {
            return Next(rngp) % (max);
        }
        /// <summary>產生一個非負數且最小值在 min 以上且不超過最大值 max的亂數</summary>  
        /// <param name="min">最小值</param>  
        /// <param name="max">最大值</param>  
        public static int Next(this RNGCryptoServiceProvider rngp, int min, int max) {
            return Next(rngp, max - min) + min;
        }
    }
    public static class Random {
        public static RNGCryptoServiceProvider rr = new RNGCryptoServiceProvider();
        public static string ran = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFFGHIJKLMNOPQRSTUVWXYZ";
        public static string Generator(int length, string exclude) {
            return Generator(string.Join("", ran.Where(p => !exclude.Contains(p))), length);
        }
        public static string Generator(string src, int length) {
            string result = "";
            for (int i = 0; i < length; i++)
                result += src[rr.Next(src.Length)];
            return result;
        }
        public static string Generator(int length) {
            return Generator(ran, length);
        }
    }
    public static class TConvert {
        public static int ToInt(object o) { return ToInt(o, 0); }
        public static int ToInt(object o, int defaultValue) {
            if (o == null) return defaultValue;
            var str = o.ToString();
            int result = defaultValue;
            if (!Int32.TryParse(str, out result) && str.Length > 0) {
                str = new Regex("/[^0-9]/g").Replace(str, "");
                Int32.TryParse(str, out result);
            }
            return result;
        }

        public static double ToDouble(object o, double defaultValue) {
            if (o == null) return defaultValue;
            var str = o.ToString();
            double result = defaultValue;
            if (!Double.TryParse(str, out result)) {
                result = defaultValue;
                //str = new Regex("/[^0-9]/g").Replace(str, "");
                //Double.TryParse(str, out result);
            }
            return result;
        }

        public static bool ToBoolean(object o) {
            if (o == null) return false;
            var str = o.ToString();
            int num;
            bool result = false;
            if (int.TryParse(str, out num))
                result = num != 0;
            else if (!bool.TryParse(str, out result) && str.Length > 0)
                result = true;
            else if (!result && o != null)
                result = true;
            return result;
        }
    }
    public static class TMath {
        public static int Range(int i, int min, int max) {
            return Math.Max(Math.Min(i, max), min);
        }
    }
    public static class TString {
        /// <summary>檢測參數陣列中是否有包含null、空值或全空白字元(AND)</summary>
        public static bool ContainNulls(params string[] vals) {
            foreach (string str in vals)
                if (string.IsNullOrWhiteSpace(str))
                    return true;
            return false;
        }
        /// <summary>檢測參數陣列中是否有包含非空白的字串(OR)</summary>
        public static bool ContainValues(params string[] vals) {
            foreach (string str in vals)
                if (!string.IsNullOrWhiteSpace(str))
                    return true;
            return false;
        }
    }
    public static class TDateTime {
        public static string toMySQLDateTime(this DateTime target) {
            return target.ToString("yyyy-MM-dd HH:mm:ss");
        }
    }
}
namespace Tool {
       public static class EnumExtensions {
        public static Dictionary<string, int> toDictionary(Enum e) {
            var dic = new Dictionary<string, int>();
            try {
                foreach (var item in Enum.GetValues(e.GetType())) {
                    dic.Add(item.ToString(), (int)item);
                }
            } catch (Exception) { }
            return dic;
        }

        public static TAttr GetAttribute<TAttr>(this Enum value) where TAttr : Attribute {
            var type = value.GetType();
            var name = Enum.GetName(type, value);
            return type.GetField(name)
                .GetCustomAttributes(false)
                .OfType<TAttr>()
                .SingleOrDefault();
        }

        public static UField GetField<TAttr, UField>(this Enum value, string field) where TAttr : Attribute {
            var type = value.GetType();
            var name = Enum.GetName(type, value);
            var attr = type.GetField(name)
                .GetCustomAttributes(false)
                .OfType<TAttr>()
                .SingleOrDefault();

            //if (Type.isDefaultObject(attr))
            //    return default(UField);

            var f = attr.GetType().GetField(field).GetValue(attr);

            return f is UField ? (UField)f : default(UField);
        }
                   
    }

    public static class LinQExtensions {
        public static IEnumerable<TSource> Distinct<TSource, TKey>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector) {
            HashSet<TKey> seenKeys = new HashSet<TKey>();
            foreach (TSource element in source) {
                var elementValue = keySelector(element);
                if (seenKeys.Add(elementValue)) {
                    yield return element;
                }
            }
        }
    }

    public static class DictionaryExtensions {
        public static void TryAddOrUpdate<T, U>(this Dictionary<T, U> dic, T key, U val) {
            if (dic.ContainsKey(key))
                dic[key] = val;
            else
                dic.Add(key, val);
        }
        public static U GetOr<T, U>(this Dictionary<T, U> dic, T key) {
            return dic.GetOr(key, default(U));
        }
        public static U GetOr<T, U>(this Dictionary<T, U> dic, T key, U defaultValue) {
            if (dic.ContainsKey(key))
                return dic[key];
            else
                return defaultValue;
        }
    }

    public static class DataTableExtensions {
        public static List<string> ColumnNames(this DataTable dt) {
            var result = new List<string>();
            for (int i = 0; i < dt.Columns.Count; i++)
                result.Add(dt.Columns[i].ColumnName);
            return result;
        }

        public static List<List<object>> toList(this DataTable dt) {
            List<List<object>> result = new List<List<object>>();
            for (int i = 0; i < dt.Rows.Count; i++)
                result.Add(dt.RowToList(i));
            return result;
        }
        public static List<string> GetColumnValues(this DataTable dt, string col) {
            if (dt.Columns.Contains(col)) {
                var r = new List<string>();
                for (int i = 0; i < dt.Rows.Count; i++)
                    r.Add(dt.Rows[i][col].ToString());
                return r;
            }
            return null;
        }
        public static List<object> RowToList(this DataTable dt, int rowIndex) {
            var result = new List<object>();
            var dr = dt.Rows[rowIndex];
            for (int i = 0; i < dt.Columns.Count; i++) {
                result.Add(dr[i]);
            }
            return result;
        }
        public static Dictionary<string, object> RowToDictionary(this DataTable dt, int rowIndex) {
            var result = new Dictionary<string, object>();
            var dr = dt.Rows[rowIndex];
            for (int i = 0; i < dt.Columns.Count; i++) {
                result.Add(dt.Columns[i].ColumnName, dr[i]);
            }
            return result;
        }
        public static Dictionary<string, string> toDictionaryByID(this DataTable dt, string key, string val) {
            if (dt.Columns.Contains(key) && dt.Columns.Contains(val)) {
            } else return null;
            var result = new Dictionary<string, string>();
            for (int i = 0; i < dt.Rows.Count; i++) {
                var dr = dt.Rows[i];
                result.Add(dr[key].ToString(), dr[val].ToString());
            }
            return result;
        }
        public static void ConvertColumn<T>(DataTable dt, string colname, Func<Object, T> converter) {
            dt.Columns.Add("_", typeof(string));
            for (int i = 0; i < dt.Rows.Count; i++) {
                var d = converter(dt.Rows[i][colname]);
                dt.Rows[i]["_"] = d;
            }
            var index = dt.Columns[colname].Ordinal;
            dt.Columns.Remove(colname);
            dt.Columns["_"].ColumnName = colname;
            dt.Columns[colname].SetOrdinal(index);
        }
        public static T Get<T>(this DataRow target, string col) {
            if (target[col] is T) {
                return (T)target[col];
            } else {
                try {
                    return (T)Convert.ChangeType(target[col], typeof(T));
                } catch (InvalidCastException) {
                    throw;
                }
            }
        }
    }

    public static class ListExtensions {
        public static T Shift<T>(this List<T> src) {
            if (src.Count() > 0) {
                var result = src.ElementAt(0);
                src.RemoveAt(0);
                return result;
            }
            return default(T);
        }
        public static string Join<T>(this IEnumerable<T> arr, string seperator) {
            return string.Join(seperator, arr);
        }
        public static string Join<T>(this IList<T> list, string seperator) {
            return string.Join(seperator, list);
        }
    }

    public class MsgPack : Dictionary<string, object>, IDisposable {
        public MsgPack() : base() { }
        public MsgPack(Dictionary<string, object> src) {
            if (src != null)
                foreach (var item in src)
                    this.Add(item.Key, item.Value);
        }
        string _type;
        public string Type {
            get {
                if (_type == null)
                    _type = this.Get<string>("Type");
                return _type;
            }
        }
        Dictionary<string, object> _msgpack;
        public Dictionary<string, object> Content {
            get {
                if (_msgpack == null)
                    _msgpack = this.Get<Dictionary<string, object>>("Content");
                return _msgpack;
            }
        }

        public void Dispose() { this._msgpack = null; this.Clear(); }

    }

    public static class DynamicExtensions {
        /// <summary>嘗試取得指定型別的屬性值</summary>
        /// <exception cref="NullReferenceException">屬性值不存在</exception>
        /// <exception cref="InvalidCastException">屬性質型別錯誤</exception>
        public static T TestGet<T>(this Dictionary<string, object> target, string attr) {
            if (!target.ContainsKey(attr))
                throw new NullReferenceException();
            else {
                return target.Get<T>(attr);
            }
        }
        public static void Test(this Dictionary<string, object> target, string attr) {
            if (!target.ContainsKey(attr))
                throw new NullReferenceException();
        }
        public static bool Exist(this Dictionary<string, object> target, string attr) {
            return target.ContainsKey(attr);
        }
        public static bool Is<T>(this Dictionary<string, object> target, string attr) {
            if (target.ContainsKey(attr))
                return target[attr] is T;
            return false;
        }
        public static MsgPack GetMsgPack(this Dictionary<string, object> target, string attr) {
            return new MsgPack(target.Get<Dictionary<string, object>>(attr));
        }

        public static Dictionary<string, object> GetDict(this Dictionary<string, object> target, string attr) {
            return target.Get<Dictionary<string, object>>(attr);
        }
        public static T[] GetArray<T>(this Dictionary<string, object> target, string attr, T defaultValue) {
            try {
                if (target.ContainsKey(attr))
                    return target.GetArray<T>(attr, false);
            } catch (Exception) { }
            return (T[])Array.CreateInstance(defaultValue.GetType(), 0);
        }
        public static T[] GetArray<T>(this Dictionary<string, object> target, string attr) {
            return target.GetArray<T>(attr, false);
        }
        public static T[] GetArray<T>(this Dictionary<string, object> target, string attr, bool exception) {
            if (target.ContainsKey(attr))
                if (target[attr].GetType().IsArray) {
                    object[] objarr = target[attr] as object[];
                    try {
                        if (target[attr].GetType().GetElementType() is T) {
                            return (T[])objarr.Cast<T>().ToArray();
                        } else {
                            var type = typeof(T);
                            var oType = target[attr].GetType();
                            var str = objarr.Join("&").Split('&');
                            object tester = oType.IsValueType ? target[attr].ToString() : target[attr];
                            Type[] Types = { typeof(string) };
                            var tryParse = type.GetMethod("Parse", Types);

                            return (T[])str.Select(o => (T)tryParse.Invoke(null, new object[] { o })).ToArray();
                        }
                    } catch (InvalidCastException e) {
                        if (exception)
                            throw e;
                    } catch (FormatException e) {
                        if (exception)
                            throw e;
                    } catch (ArgumentException e) {
                        if (exception)
                            throw e;
                    }
                }



            return (T[])Array.CreateInstance(typeof(T), 0);
        }

        public static T Get<T>(this Dictionary<string, object> target, string attr, T defaultValue) {
            try {
                if (target.ContainsKey(attr))
                    return target.Get<T>(attr);
            } catch (Exception) { }
            return defaultValue;
        }
        public static T Get<T>(this Dictionary<string, object> target, string attr) {
            return target.Get<T>(attr, false);
        }
        public static T Get<T>(this Dictionary<string, object> target, string attr, bool exception) {
            if (target.ContainsKey(attr))
                if (target[attr] is T)
                    return (T)target[attr];
                else if (target[attr] != null) {
                    var type = typeof(T);
                    var oType = target[attr].GetType();
                    try {
                        if (target[attr] is decimal) {
                            var toT = typeof(decimal).GetMethod("To" + typeof(T).Name);
                            if (toT != null)
                                return (T)toT.Invoke(null, new object[] { target[attr] });
                        } else if (type.IsValueType) {
                            /*實質型別=>TryParse*/
                            object tester = oType.IsValueType ? target[attr].ToString() : target[attr];
                            Type[] Types = { typeof(string), type.MakeByRefType() };
                            var tryParse = type.GetMethod("TryParse", Types);
                            if (tryParse != null) {
                                object[] args = { tester, null };
                                if ((bool)tryParse.Invoke(null, args))
                                    return (T)args[1];
                            }
                        } else {/*參考型別=>ChangeType*/
                            var result = (T)Convert.ChangeType(target[attr], typeof(T));
                            return result;
                        }
                    } catch (InvalidCastException e) {
                        if (exception)
                            throw e;
                    } catch (FormatException e) {
                        if (exception)
                            throw e;
                    } catch (ArgumentException e) {
                        if (exception)
                            throw e;
                    }
                }
            return default(T);
        }
    }
}

