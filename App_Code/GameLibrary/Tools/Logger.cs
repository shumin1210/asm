﻿using System;
using System.IO;

namespace Tool {
    public class Logger {
        public string Owner;
        public bool IsActive = true;
        private static object locker = new object();
        private bool Append = false;
        public string rootPath;
        public string LogDirect;
        public Logger(string owner) {
            rootPath = System.Environment.CurrentDirectory;
            Owner = owner;
        }
        public void RecordLine(string content) {
            if (!IsActive) { return; }
            if (LogDirect != null && LogDirect != "") {
                RecordLine(LogDirect, content);
            }
        }
        public void Destroy(string catagory) {
            string path = rootPath + @"\" + catagory + @"\" + Owner + "_" + DateTime.Now.ToString("yyyy-MM-dd") + " log.txt";
            if (File.Exists(path)) {
                File.Delete(path);
            }
        }
        public void RecordLine(string catagory, string content) {
            if (!IsActive) { return; }
            lock (locker) {
                string path = rootPath + @"\" + catagory + @"\" + Owner + "_" + DateTime.Now.ToString("yyyy-MM-dd") + " log.txt";
                if (!Directory.Exists(rootPath + @"\" + catagory)) {
                    Directory.CreateDirectory(rootPath + @"\" + catagory);
                }
                using (StreamWriter sw = File.AppendText(path)) {
                    if (Append) {
                        sw.WriteLine(content);
                    } else {
                        sw.WriteLine(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "___" + content);
                    }
                    sw.Flush();
                }
            }
            Append = false;
        }
        public void RecordPlus(string catagory, string content) {
            if (!IsActive) { return; }
            lock (locker) {
                string path = rootPath + @"\" + catagory + @"\" + Owner + "_" + DateTime.Now.ToString("yyyy-MM-dd") + " log.txt";
                if (!Directory.Exists(rootPath + @"\" + catagory)) {
                    Directory.CreateDirectory(rootPath + @"\" + catagory);
                }
                using (StreamWriter sw = File.AppendText(path)) {
                    if (Append) {
                        sw.WriteLine(content);
                    } else {
                        sw.WriteLine(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "___" + content);
                    }
                    sw.Flush();
                }
            }
            Append = true;
        }
    }
}
