﻿using System;
using System.Runtime.InteropServices;
using Microsoft.Win32.SafeHandles;

public abstract class BaseClass : IDisposable {
    // Flag: Has Dispose already been called?
    private bool disposed = false;
    // Instantiate a SafeHandle instance.
    SafeHandle handle = new SafeFileHandle(IntPtr.Zero, true);

    // Public implementation of Dispose pattern callable by consumers.
    public void Dispose() {
        Dispose(true);
        GC.SuppressFinalize(this);
    }

    // Protected implementation of Dispose pattern.
    public void Dispose(bool disposing) {
        if (disposed) return;
        if (disposing) {
            handle.Dispose();
            DisposeObjects();
        }
        // Free any unmanaged objects here.
        disposed = true;
    }

    protected virtual void DisposeObjects(){}

    ~BaseClass() {
        Dispose(false);
    }
}