﻿using System;
using System.Collections.Generic;
using System.Linq;
using Tool;
namespace GameLibrary {
    public class Player {
        public int ID = 0;
        string token;
        public string Token {
            get { return token; }
            set {
                if (token != value) {
                    token = value;
                    isAuthorized = false;
                }
            }
        }
        public string Name = "";
        public object this[string attr] {
            get { return info.ContainsKey(attr) ? info[attr] : default(object); }
            set { info.TryAddOrUpdate(attr, value); }
        }
        public void RemoveInfo(string attr) {
            if (info.ContainsKey(attr))
                info.Remove(attr);
        }

        public long originUD = 0;
        long ud = 0;
        public long UD {
            get { return ud; }
            set { lock (this) { ud = value; } }
        }

        public int Role = 0;

        public double originRefund = 0;
        double refund = 0;
        public double Refund {
            get { return refund; }
            set { lock (this) { refund = value; } }
        }
                
        DateTime lastacttime;
        public DateTime LastActTime {
            get { return lastacttime; }
            set {
                if (lastacttime != value) {
                    lastacttime = value;
                }
            }
        }

        public iConnect MsgHandler;
        public List<string> Games = new List<string>();
        bool _isAuthorized = false;
        public bool isAuthorized {
            get { return _isAuthorized; }
            set {
                if (value && !_isAuthorized) {
                    AuthorizedTime = DateTime.Now;
                    _isAuthorized = true;
                }
            }
        }
        public DateTime AuthorizedTime { get; private set; }
        public Dictionary<string, object> info = new Dictionary<string, object>();

        public Player() { }

        public Player(iConnect msghandler)
            : base() {
            MsgHandler = msghandler;
        }

        public void CopyProp(Player pre) {
            if (pre == null) return;

            ID = pre.ID;
            Name = pre.Name;
            originUD = pre.originUD;
            UD = pre.UD;

            info = pre.info;
            ManagerLevel = pre.ManagerLevel;
            isAuthorized = pre.isAuthorized;
            Games = pre.Games;
        }
        public Player(int id, string name, long ud, iConnect msgHandler) {
            this.ID = id; this.Name = name; this.UD = ud; this.MsgHandler = msgHandler;
        }
        public bool Send(string type, object content) {
            return this.Send(new ActionObject(type, content));
        }
        public bool Send(object msg) {
            if (MsgHandler != null) {
                MsgHandler.Send(msg);
                return true;
            }
            return false;
        }
        public void Disconnect() {
            if (MsgHandler != null) {
                MsgHandler.Disconnect();
                MsgHandler = null;
            }
        }

        public void SendInfo() {
            Send(new ActionObject("playerinfo", new { UD, Refund }));
        }


        /*ManagerCheck*/
        public Dictionary<string, int[]> ManagerLevel = new Dictionary<string, int[]>();
        public void ImportManagerAccess(string acs) {
            ManagerLevel = acs.Split(';')
                .Where(p => p.Length > 0)
                .Select(ac => ac.Split(':'))
                .ToDictionary(ac => ac[0], ac => ac[1].Split(',').Select(a => Convert.ToInt32(a)).ToArray());
        }
        public bool ManageAccess(iController ctr, int permission) {
            int[] accs;
            if (ManagerLevel.TryGetValue("c" + ctr.controllerID.ToString(), out accs))
                if (accs.Contains(permission))
                    return true;
            return false;
        }
        public int ManageAccess(iController ctr) {
            int result = 0;
            int[] accs;
            if (ManagerLevel.TryGetValue("c" + ctr.controllerID.ToString(), out accs))
                result = accs.Max();
            return result;
        }
        public bool ManageAccess(iGame gs, int permission) {
            int[] accs;
            if (ManagerLevel.TryGetValue(gs.gameid.ToString(), out accs))
                if (accs.Contains(permission))
                    return true;
            return false;
        }
        public int ManageAccess(iGame gs) {
            int result = 0;
            int[] accs;
            if (ManagerLevel.TryGetValue(gs.gameid.ToString(), out accs))
                result = accs.Max();
            return result;
        }
    }

}