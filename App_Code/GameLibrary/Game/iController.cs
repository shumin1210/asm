﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading;
using System.Web.Script.Serialization;
using Tool;
namespace GameLibrary {
    public abstract class iController : BaseClass {
        public bool enableReconnecter = false;
        public int waitForReconnect = 5000;
        public int controllerID = 0;
        protected bool initialized = false;
        public virtual void Initialize() {
            if (initialized) { return; }
        }

        protected JavaScriptSerializer jser = new JavaScriptSerializer();
        public Dictionary<string, iGame> Games = new Dictionary<string, iGame>();
        /// <summary>玩家列表[iConnect.getID]</summary>
        protected ConcurrentDictionary<string, Player> Players = new ConcurrentDictionary<string, Player>();
        private ConcurrentDictionary<string, Player> HoldPlayers = new ConcurrentDictionary<string, Player>();

        public Logger Recorder = new Logger("iControler");
        public iConnect MessageHandler;


        public Dictionary<string, string> GetConfigProperties(IDataBase db, iGame gs) {
            var cmd = CommandBuilder.Instance;
            
            db.ClearParameters();
            db.TryAddParameter("ControllerID", controllerID);
            db.TryAddParameter("gid", gs.gameid);
            db.TryAddParameter("innergid", gs.GID);
            cmd.addTable("gameidlist", "ids").addTable("gameconfigs", "cfg");
            cmd.SELECT("cfg.property", "cfg.value")
                .FROM("ids")
                .RIGHT_JOIN("cfg", "ids.configID like CONCAT( '%,',cfg.package,',%')")
                .WHERE_params("ids.ControllerID", "ids.gid", "ids.innergid", "cfg.gid");

            var dt = db.ExecuteDataTable(cmd.Result);
            cmd.Free();
            if (dt.Rows.Count > 0)
                return dt.toDictionaryByID("property", "value");
            //return dt.Rows[0]["properties"].ToString().Split(';').Where(p => p.Length > 0).ToDictionary(p => p.Split(':')[0], p => p.Split(':')[1]);
            else
                return new Dictionary<string, string>();
        }
        protected DataTable getConfig(IDataBase db) {
            var cmd = CommandBuilder.Instance;
            cmd.addTable("gameidlist", "ids").addTable("gameserver", "g");
            cmd.SELECT("g.gid",
                "ids.name",
                "g.namespace as namespace",
                "g.type as type",
                "ids.innergid")
                .FROM("ids")
                .LEFT_JOIN("g", "g.gid=ids.gid")
                .WHERE_params("ids.ControllerID")
                .GROUPBY("ids.innergid");
            db.ClearParameters();
            db.TryAddParameter("ControllerID", controllerID);
            var dt = db.ExecuteDataTable(cmd.Result);
            cmd.Free();
            return dt;
        }
        public void BuildGame(IDataBase db) {
            var dt = getConfig(db);
            if (dt == null) return;
            for (int i = 0; i < dt.Rows.Count; i++) {
                var dr = dt.Rows[i];
                iGame g;
                try {
                    //var cfgs = dr["properties"].ToString().Split(';').Where(p => p.Length > 0).ToDictionary(p => p.Split(':')[0], p => p.Split(':')[1]);
                    var type = dr["namespace"] + "." + dr["type"];
                    var assemble = AppDomain.CurrentDomain.GetAssemblies().Where(p => p.GetType(type) != null).First().FullName;
                    g = (iGame)Activator.CreateInstance(Type.GetType(type + ", " + assemble));
                    g.gameid = Convert.ToInt32(dr["gid"].ToString());
                    g.GID = dr["innergid"].ToString();
                    g.main = this;
                    g.Name = dr["name"].ToString();
                    g.Initialize(GetConfigProperties(db, g));
                    db.ClearParameters();
                    db.TryAddParameter("gid", g.gameid);
                    db.TryAddParameter("lan", Languages.Default);
                    var title = db.ExecuteScalar("SELECT text FROM textmap WHERE gid=@gid AND language=@lan AND property='Title'");
                    g.Title = title == null ? "" : title.ToString();

                    Controller.defaultGID = i == 0 ? dr["innergid"].ToString() : "";
                } catch (Exception) { throw; continue; }

                Games.Add(g.GID, g);
            }
        }
        public Player FindPlayer(Func<Player, bool> check) {
            foreach (var item in Players)
                if (item.Value != null && check(item.Value))
                    return item.Value;
            return null;
        }

        public void onOpen(iConnect sender) {
            Players.TryAdd(sender.getID(), new Player(sender));
            handleOpen(Players[sender.getID()]);
        }
        public virtual void handleOpen(Player p) { }


        public void onClose(iConnect sender) {
            Player p;
            Players.TryRemove(sender.getID(), out p);
            p.MsgHandler = null;

            if (enableReconnecter && p.isAuthorized) {
                HoldPlayers.TryAdd(p.Token, p);
                new Thread(() => RemoveReconnect(p)).Start();
            } else
                RemoveConnect(p);
        }

        void RemoveReconnect(Player p) {
            Thread.Sleep(waitForReconnect);

            Player tmp;
            if (HoldPlayers.TryGetValue(p.Token, out tmp))
                RemoveConnect(tmp);

        }

        public virtual void handleClose(Player p) { }

        void RemoveConnect(Player p) {

            handleClose(p);

            /*清除玩家物件*/
            if (enableReconnecter && p.isAuthorized) {
                Player hp;
                HoldPlayers.TryRemove(p.Token, out hp);
            }
        }


        public void onError(iConnect sender) {
            handleError(Players[sender.getID()]);
        }
        public virtual void handleError(Player p) { }


        public void onMessage(iConnect sender, MsgPack msg) {
            handleMessage(Players[sender.getID()], msg);
        }
        public virtual void handleMessage(Player p, MsgPack content) {
            Recorder.RecordLine("Handle", jser.Serialize(content));
        }


        public bool Reconnect(ref Player p) {
            Player tmp = null;
            var token = p.Token;
            var msgh = p.MsgHandler;
            var online = Players.Where(pp => pp.Value.Token == token);
            if (online.Count() == 2) {
                foreach (var pp in online)
                    if (pp.Value != p)
                        tmp = pp.Value;
                p.CopyProp(tmp);
                tmp.Send(new ActionObject("double login", new { }));
                tmp.Disconnect();
                return true;
            } else if (HoldPlayers.TryGetValue(p.Token, out tmp)) {
                lock (HoldPlayers) {
                    Player _tmp;
                    HoldPlayers.TryRemove(tmp.Token, out _tmp);
                }
                p.MsgHandler = msgh;
                p.LastActTime = DateTime.Now;
                return true;
            }
            return false;
        }

        public void Broadcast(object pmsg) {
            string msg;
            if (pmsg is string)
                msg = pmsg as string;
            else
                msg = jser.Serialize(pmsg);
            var l = Players.Values.ToArray();
            foreach (var p in l)
                if (p != null) p.Send(msg);
        }
    }
}