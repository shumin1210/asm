﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.Script.Serialization;
using Tool;

namespace GameLibrary {

    public abstract class iGame : BaseClass {
        public iGame() { }
        protected JavaScriptSerializer jser = new JavaScriptSerializer();
        public bool isUpdated = false;
        
        /// <summary>遊戲類別名字</summary>
        public string Title = "";
        public int gameid;
        /// <summary>遊戲識別碼</summary>
        public string GID;
        /// <summary>遊戲顯示名字(遊戲廳名)</summary>
        public string Name;
        /// <summary>主控者</summary>
        public iController main;
        /// <summary>玩家</summary>
        public ConcurrentDictionary<int, Player> Players = new ConcurrentDictionary<int, Player>();
        /// <summary>遊戲指令</summary>
        /// <param name="p">使用者</param>
        /// <param name="content">資訊包{actionType,actionTime,actionContent}</param>
        public abstract void Play(Player p, MsgPack content);

        public abstract bool checkTime(Player p, MsgPack content);

        public abstract void closeAllGameConnect(Player p);
        /// <summary>發生錯誤時處理</summary>
        public abstract void Report(Player actor, int type, string msg);
        /// <summary>退水</summary>
        public double Refund = .02;
        public abstract bool isManager(Player p);

        /// <summary>新增玩家</summary>
        public virtual bool AddPlayer(Player p) {
            if (!p.Games.Contains(GID))
                p.Games.Add(GID);
            if (Players.ContainsKey(p.ID)) {
                p.UD = Players[p.ID].UD;
            }
            return Players.AddOrUpdate(p.ID, p, (a, b) => p) == p;
        }
        /// <summary>剔除玩家</summary>
        public virtual bool RemovePlayer(Player p) {
            if (p.Games.Contains(GID))
                p.Games.Remove(GID);
            if (Players.Values.Contains(p)) {
                Player tmp;
                Players.TryRemove(p.ID, out tmp);
                return p != null;
            }
            return true;
        }

        public void Initialize(Dictionary<string, string> cfg) {
            ImportConfigs(cfg);
            LateInitialize();
        }
        /// <summary>初始化:載入設定</summary>
        public virtual void ImportConfigs(Dictionary<string, string> cfg) { }
        /// <summary>初始化:開機動作</summary>
        public virtual void LateInitialize() { }

        public void Broadcast(object msg) {
            string pmsg;
            if (msg is string) {
                pmsg = msg as string;
            } else
                pmsg = jser.Serialize(msg);
            var l = Players.Values.ToArray();
            foreach (var p in l)
                if (p != null)
                    try { p.Send(pmsg); } catch (Exception) { }
        }

        public abstract DataTable GetRecord();
        public abstract DataTable GetPlayerRecord(Player p);

        public bool CheckActTime(int uid) {
            Player tmp;
            if (Players.TryGetValue(uid, out tmp)) {
                TimeSpan ts = DateTime.Now - tmp.LastActTime;
                if (ts.TotalMinutes > 15) {
                    return false;
                } else {
                    Players[uid].LastActTime = DateTime.Now;
                    return true;
                }
            }

            return false;
        }

        #region Manage
        public virtual object GameData_Manage() { return new { GID }; }

        DataTable tmpCfg;
        public virtual object GetLanguagePack(string type) {
            switch (type) {
                case "configs":
                    return null;
                default:
                    return null;
            }
        }
        public DataTable GetConfigs(IDataBase db) {
            var cmd = CommandBuilder.Instance;
            cmd.addTable("gameidlist", "a")
                .addTable("gameconfigs", "cfg")
                .addTable("textmap", "text");
            cmd.SELECT(
                "CONCAT('p',cfg.package,'-',cfg.property) AS cfg_id",
                "cfg.value AS cfg_val",
                "cfg.editable AS cfg_edit",
                "cfg.type AS cfg_type",
                "text.text AS cfg_info",
                "cfg.option AS cfg_opt").FROM("a")
                .RIGHT_JOIN("cfg", "a.configID like CONCAT( '%,',cfg.package,',%')")
                .LEFT_JOIN("text", "text.property=cfg.property")
                .WHERE_condition("cfg.display=1")
                .WHERE_params("a.gid", "a.innergid", "cfg.gid", "text.language");
            db.ClearParameters();
            db.TryAddParameter("language", Languages.Default);
            db.TryAddParameter("gid", gameid);
            db.TryAddParameter("innergid", GID);
            tmpCfg = db.ExecuteDataTable(cmd.Result);


            cmd.Clear();
            cmd.SELECT("text").FROM("textmap").WHERE_params("gid", "language").WHERE_condition("property='Title'");
            Title = db.ExecuteScalar(cmd.Result).ToString();

            cmd.Free();
            return tmpCfg;
        }
        public bool SetConfigs(
            IDataBase db,
            Dictionary<string, object> cfg,
             out Dictionary<string, string> updates,
            out Dictionary<string, string> errors) {

            if (tmpCfg == null) GetConfigs(db);

            bool result = checkConfigs(cfg, out updates, out errors);
            if (result)
                updateConfigs(db, updates);
            return result;
        }



        bool checkConfigs(Dictionary<string, object> cfg, out Dictionary<string, string> updatedCfgs, out Dictionary<string, string> errorCfgs) {
            updatedCfgs = new Dictionary<string, string>();
            errorCfgs = new Dictionary<string, string>();
            foreach (var item in cfg) {
                string key = item.Key;
                string value = item.Value.ToString();
                string originVal;
                string val = "";
                var drs = tmpCfg.Select("cfg_id='" + key + "'");

                if (drs.Length == 0) continue;
                DataRow dr = drs[0];

                if (!dr.Field<bool>("cfg_edit")) continue;

                originVal = dr.Field<string>("cfg_val");

                string paramType = dr.Field<string>("cfg_type");
                switch (paramType) {
                    case "string":
                        if (originVal == value) continue;
                        val = value;
                        break;
                    case "number":
                        long number_val;
                        if (Int64.TryParse(value, out number_val))
                            val = originVal == number_val.ToString() ? "" : number_val.ToString();
                        else
                            errorCfgs.Add(key, value);
                        break;
                    case "odd":
                    case "percent":
                        double double_val;
                        if (double.TryParse(value, out double_val))
                            val = originVal == double_val.ToString() ? "" : double_val.ToString();
                        else
                            errorCfgs.Add(key, value);
                        break;
                    case "oddArr":
                    case "strArr":
                    case "numArr":
                    case "perArr":
                        if (value == originVal) continue;

                        var strArr = value.Split(',');
                        var originArr = originVal.Split(',');
                        if (strArr.Length != originArr.Length) {
                            errorCfgs.Add(key, value);
                            continue;
                        }
                        if (paramType == "strArr") {
                            val = value;
                        } else {
                            try {
                                var darr = strArr.Select(s => Convert.ToDouble(s));
                                if (paramType == "numArr") {
                                    if (darr.Where(d => d % 1 != 0).Count() > 0) {
                                        errorCfgs.Add(key, value);
                                        continue;
                                    }
                                    val = value;
                                } else {
                                    //if (darr.Where(d => d < 0 || d > 100).Count() > 0) {
                                    //    errorCfgs.Add(key, value);
                                    //    continue;
                                    //}

                                    val = value;
                                }
                            } catch (Exception) {
                                errorCfgs.Add(key, value);
                                continue;
                            }
                        }

                        break;
                    case "class":
                        break;
                }
                if (!string.IsNullOrWhiteSpace(val))
                    updatedCfgs.Add(key, val);
            }
            return (errorCfgs.Count == 0 && updatedCfgs.Count > 0);
        }

        void updateConfigs(IDataBase db, Dictionary<string, string> updates) {
            string sqlcmd = "UPDATE gameconfigs SET value =(CASE";
            foreach (var item in updates) {
                var tmp = item.Key.Split('-');
                string property = tmp[1];
                string value = item.Value;
                if (property == "Language" && value != Languages.Default) {
                    Languages.Default = value;
                }
                sqlcmd += " WHEN property='" + property + "' THEN '" + value + "'";
            }
            sqlcmd += " ELSE value END)";
            sqlcmd += " WHERE gid=@gid";
            db.ClearParameters();
            db.TryAddParameter("gid", gameid);
            db.Execute(sqlcmd);

            GetConfigs(db);
        }

        #endregion
    }

}
