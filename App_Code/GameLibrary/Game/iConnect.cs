﻿using System;
using System.Web.Script.Serialization;

namespace GameLibrary {
    public interface iConnect {
        string getIP();
        string getID();
        void Disconnect();
        void Send(object msg);
        void Broadcast(object msg);
        void SetController(iController ctr);
    }
    public class ActionObject {
        public string Type;
        public string Time;
        public Object Content;
        public ActionObject(string type, object content) {
            this.Time = DateTime.Now.ToString("yyyy/MM/dd hh:mm:ss");
            this.Type = type;
            this.Content = content;
        }
    }

    public class ActionCommand {
        public string actionmaintype;
        public string actionsubtype;
        public string actiontime;
        public Object actioncontent;
        public ActionCommand(string MainType, string SubType, object content) {
            this.actiontime = DateTime.Now.ToString("yyyy/MM/dd hh:mm:ss");
            this.actionmaintype = MainType;
            this.actionsubtype = SubType;

            this.actioncontent = content;
        }
    }
}

