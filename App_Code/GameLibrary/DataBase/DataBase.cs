﻿using System.Data;

namespace GameLibrary {
    public interface IDataBase {
        void ClearParameters();
        void TryAddParameter(string key, object value);

        int Execute(string sqlcommand);
        DataTable ExecuteDataTable(string sqlcommand);
        object ExecuteScalar(string sqlcommand);
        //object ExecuteReader(string sqlcommand);

        void Open();
        void Close();
    }


    public abstract class iSQLAction : BaseClass {
        protected IDataBase db;
        public iSQLAction(IDataBase db) { this.db = db; }
        public abstract DataTable GetConfigs();

    }
}
