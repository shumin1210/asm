﻿using System;
using System.Configuration;
using System.Data;
using MySql.Data.MySqlClient;
using Tool;
namespace GameLibrary {
    public class MySQL : BaseClass, IDataBase {
        private DataTable dt;

        private MySqlConnection connection;
        public MySqlCommand command;
        public MySqlDataReader reader { get; set; }
        public Logger Recorder;
        protected override void DisposeObjects() {
            if (connection != null) {
                if (connection.State == System.Data.ConnectionState.Open) {
                    try {
                        connection.Close();
                    } catch (MySqlException ex) {
                        RecordLine("DisposeEx", "CloseEx: " + ex.Message);
                    } finally {
                        connection.Close();
                    }
                }
                command.Connection = null;
                connection.Dispose();
                connection = null;
            }

            if (reader != null) { reader.Dispose(); }

            if (command != null) {
                command.Parameters.Clear();
                command.Dispose();
                command = null;
            }


            if (dt != null) { dt.Dispose(); dt = null; }
            Recorder = null;
        }
        bool inuse = false;

        public MySqlParameterCollection parameters {
            get { return command.Parameters; }
        }

        public MySQL(string dbName, Logger recorder)
            : this(dbName) {
            Recorder = recorder;
        }
        public MySQL(string dbName) {
            connection = new MySqlConnection();
            command = new MySqlCommand();
            connection.ConnectionString =
                ConfigurationManager.ConnectionStrings[dbName].ConnectionString;
            command.Connection = connection;
        }

        void RecordLine(string catagory, string msg) {
            if (Recorder != null)
                Recorder.RecordLine(catagory, msg);
        }

        public int Execute(string sqlcommand) {
            var affect = 0;
            inuse = true;
            command.CommandText = sqlcommand;
            Open();
            using (MySqlTransaction tran = connection.BeginTransaction()) {
                try {
                    command.Transaction = tran;
                    affect = command.ExecuteNonQuery();
                    tran.Commit();
                } catch (Exception ex) {
                    tran.Rollback();
                    RecordLine("ExcuteEx", "Ex: " + ex.Message);
                    RecordLine("ExcuteEx", "Query: " + sqlcommand);
                    string str = "";
                    for (int i = 0; i < command.Parameters.Count; i++)
                        str += command.Parameters[i].ToString() + ",";

                    RecordLine("ExcuteEx", "Parameters: " + str);
                    RecordLine("ExcuteEx", "-------------");
                    throw ex;
                } finally {
                    Close();
                    inuse = false;
                }
            }
            inuse = false;
            return affect;
        }

        public DataTable ExecuteDataTable(string query) {
            inuse = true;
            dt = new DataTable();
            command.CommandText = query;
            Open();
            using (MySqlDataAdapter adapter = new MySqlDataAdapter(command)) {
                try {
                    adapter.Fill(dt);
                } catch (Exception e) {
                    RecordLine("ExcuteEx", "Ex: " + e.Message);
                    RecordLine("ExcuteEx", "Query: " + query);
                    RecordLine("ExcuteEx", "-------------");
                    throw e;
                }
            }
            Close();
            inuse = false;
            return dt;
        }

        public object ExecuteReader(string query) {
            command.CommandText = query;
            return command.ExecuteReader();
        }

        public object ExecuteScalar(string query) {
            inuse = true;
            command.CommandText = query;
            Open();
            try {
                return command.ExecuteScalar();
            } catch (Exception) {
                throw;
            } finally {
                Close();
                inuse = false;
            }
        }

        public void Open() {
            if (connection != null && connection.State == ConnectionState.Closed) {
                try { connection.Open(); } catch (Exception) { }
            }
        }
        public void Close() {
            if (connection.State == ConnectionState.Open)
                try { connection.Close(); } catch (Exception) { }
        }

        public void ClearParameters() {
            parameters.Clear();
        }

        public void TryAddParameter(string key, object value) {
            if (parameters.Contains(key))
                parameters[key].Value = value;
            else
                parameters.AddWithValue(key, value);
        }
    }
}