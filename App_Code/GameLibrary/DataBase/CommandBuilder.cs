﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;

namespace GameLibrary {
    public class CommandBuilder {
        public static int poolSize = 20;
        public static CommandBuilder Instance {
            get {
                CommandBuilder c;
                lock (_cmd) {
                    if (!_cmd.TryTake(out c))
                        c = new CommandBuilder();
                }
                return c;
            }
        }
        static ConcurrentBag<CommandBuilder> _cmd = new ConcurrentBag<CommandBuilder>();
        public static void Free(CommandBuilder c) {
            c.Clear();
            if (_cmd.Count < poolSize)
                _cmd.Add(c);
        }

        public void Free() {
            CommandBuilder.Free(this);
        }

        public struct Table {
            public string Name;
            public string sh;
            public Table(string name, string sh) {
                this.Name = name;
                this.sh = sh;
            }
        }

        public IDataBase db;

        public void Clear() {

            from = limit = desc = "";
            select.Clear();
            join.Clear();
            groupby.Clear();
            orderby.Clear();

            update = insertInto = "";
            dt = new DataTable();
            paramType = new List<Dictionary<int, bool>>();
            on_duplicate.Clear();
            addBatch();

            result = "";
            tables.Clear();
            where.Clear();



            type = SqlType.select;
            needUpdate = true;
        }

        enum SqlType { select, insert, update }
        SqlType type = SqlType.select;

        #region Tables
        Dictionary<string, string> tables = new Dictionary<string, string>();
        string tableAs(string table) {
            if (tables.ContainsValue(table)) {
                return string.Join(" ", tables.First(p => p.Value == table).Key, "AS", table);
            } else if (tables.ContainsKey(table)) {
                return string.Join(" ", table, "AS", tables[table]);
            } else
                return table;
        }
        string replaceTableName(string src) {
            string tableNamePattern = @"\w*\.";
            var m = Regex.Match(src, tableNamePattern);
            while (m.Success) {
                var tableName = m.Value.Substring(0, m.Value.IndexOf('.'));
                if (tables.ContainsKey(tableName))
                    src = src.Replace(tableName, tables[tableName]);
                m = m.NextMatch();
            }
            return src;
        }
        public CommandBuilder addTable(string table, string replace) {
            if (!tables.ContainsKey(table))
                tables.Add(table, replace);
            return this;
        }
        #endregion

        #region Result
        string result = "";
        bool needUpdate = false;
        public string Result {
            get {
                if (needUpdate)
                    refresh();
                return result;
            }
        }
        public string Count {
            get {
                this.CountOnly = true;
                if (needUpdate)
                    refresh();
                this.CountOnly = false;
                return result;
            }
        }
        public string ResultWithLastID {
            get {
                return Result + "SELECT LAST_INSERT_ID();";
            }
        }
        void refresh() {
            switch (type) {
                case SqlType.select:
                    result = string.Join(" ",
                            "SELECT", onlyCount ? "COUNT(*)" : string.Join(",", select.Select(s => replaceTableName(s))),
                            "FROM", tableAs(from),
                              string.Join(" ", join.Select(p => string.Join(" ",
                                  p[0], "JOIN", tableAs(p[1]), "ON",
                                  p[3] != ""
                                      ? (replaceTableName(p[2]) + "=" + replaceTableName(p[3])) : replaceTableName(p[2])
                                  ))),
                              (where.Count > 0 ? "WHERE " + string.Join(" AND ", where) : ""),
                              groupby.Count > 0 ? "GROUP BY " + string.Join(",", groupby.Select(o => replaceTableName(o))) : "",
                              onlyCount ? "" :
                              (orderby.Count > 0 ? "ORDER BY " + string.Join(",", orderby.Select(o => replaceTableName(o))) : ""),
                              onlyCount ? "" : desc,
                              onlyCount ? "" : limit,
                              ";");
                    break;
                case SqlType.insert:
                case SqlType.update:
                    List<List<string>> args = new List<List<string>>();
                    for (int r = 0; r < dt.Rows.Count; r++) {
                        args.Add(new List<string>());
                        for (int c = 0; c < dt.Columns.Count; c++) {
                            if (paramType[r].ContainsKey(c) && paramType[r][c]) {
                                args[r].Add(dt.Rows[r][c].ToString());
                                continue;
                            }

                            string id = "row" + r + "col" + c;
                            object val = dt.Rows[r][c];
                            db.TryAddParameter(id, val);
                            args[r].Add("@" + id);
                        }
                    }

                    if (type == SqlType.insert) {
                        result = string.Join(" ",
                                "INSERT INTO", insertInto,
                                "(" + string.Join(",", dt.Columns.Cast<DataColumn>().Select(x => x.ColumnName).ToArray()) + ")",
                                "VALUES", string.Join(",", args.Select(x => "(" + string.Join(",", x) + ")")),
                                on_duplicate.Count > 0 ? ("ON DUPLICATE KEY UPDATE " + string.Join(",", on_duplicate.Select(c => c + "=VALUES(" + c + ")"))) : "",
                                ";");
                        break;
                    } else {
                        result = "";
                        for (int r = 0; r < args.Count; r++) {
                            result += string.Join(" ",
                                    "UPDATE", update,
                                    "SET", string.Join(",", dt.Columns.Cast<DataColumn>().Select(x => x.ColumnName + "=" + args[r][dt.Columns.IndexOf(x.ColumnName)]).ToArray()),
                                    (where.Count > 0 ? "WHERE " + string.Join(" AND ", where) : ""),
                                    ";");
                        }
                    }
                    break;
            }

        }

        #endregion

        #region Where
        List<string> where = new List<string>();
        public CommandBuilder WHERE_condition(params string[] conditions) {
            for (int i = 0; i < conditions.Length; i++) {
                where.Add(conditions[i]);
            }
            needUpdate = true;
            return this;
        }
        public CommandBuilder WHERE_params(params string[] conditions) {
            for (int i = 0; i < conditions.Count(); i++) {
                var split = conditions[i].Split('.');
                if (split.Length == 2 && !where.Contains(split[1])) {
                    where.Add(conditions[i] + "=@" + split[1]);
                } else {
                    where.Add(conditions[i] + "=@" + conditions[i]);
                }
            }
            needUpdate = true;
            return this;
        }

        #endregion

        #region Part Select
        List<string> select = new List<string>();
        string from = "";
        List<string[]> join = new List<string[]>();
        List<string> orderby = new List<string>();
        List<string> groupby = new List<string>();
        string limit = "";
        string desc = "";
        bool onlyCount = false;


        public bool CountOnly {
            get {
                return onlyCount;
            }
            set {
                if (onlyCount != value) {
                    needUpdate = true;
                    onlyCount = value;
                }
            }
        }

        public CommandBuilder SELECT(params string[] columnNames) {
            for (int i = 0; i < columnNames.Length; i++)
                select.Add(columnNames[i]);
            needUpdate = true;
            type = SqlType.select;
            return this;
        }
        public CommandBuilder FROM(string table) {
            from = table;
            needUpdate = true;
            return this;
        }
        public CommandBuilder JOIN(string table, string condition) {
            return _JOIN("", table, condition);
        }
        public CommandBuilder RIGHT_JOIN(string table, string condition) {
            return _JOIN("RIGHT", table, condition);
        }
        public CommandBuilder LEFT_JOIN(string table, string condition) {
            return _JOIN("LEFT", table, condition);
        }
        CommandBuilder _JOIN(string type, string table, string condition) {
            var parts = condition.Split('=');
            join.Add(new string[] { type, table, parts[0], parts.Length == 2 ? parts[1] : "" });
            needUpdate = true;
            return this;
        }
        public CommandBuilder ORDERBY(params string[] conditions) {
            for (int i = 0; i < conditions.Count(); i++)
                orderby.Add(conditions[0]);
            needUpdate = true;
            return this;
        }
        public CommandBuilder DESC() {
            desc = "DESC";
            needUpdate = true;
            return this;
        }
        public CommandBuilder GROUPBY(params string[] conditions) {
            for (int i = 0; i < conditions.Count(); i++)
                groupby.Add(conditions[0]);
            needUpdate = true;
            return this;
        }
        public CommandBuilder LIMIT(string limit) {
            this.limit = limit.Length > 0 ? "LIMIT " + limit : "";
            needUpdate = true;
            return this;
        }
        #endregion

        #region Part Insert/SetValue
        DataTable dt = new DataTable();
        List<Dictionary<int, bool>> paramType = new List<Dictionary<int, bool>>();
        int currentRow = 0;
        int currentCol = 0;
        public CommandBuilder addColumn(params string[] colname) {
            for (int i = 0; i < colname.Length; i++)
                if (!dt.Columns.Contains(colname[i]))
                    dt.Columns.Add(colname[i], typeof(object));
            needUpdate = true;
            return this;
        }
        public CommandBuilder addBatch() {
            dt.Rows.Add();
            paramType.Add(new Dictionary<int, bool>());
            currentRow = dt.Rows.Count - 1;
            currentCol = 0;
            return this;
        }

        public CommandBuilder addColumnAndValue(params object[] pairs) {
            for (int i = 0; i < pairs.Length; i++) {
                if (i % 2 == 0) {
                    addColumn(pairs[i].ToString());
                } else {
                    addValues(pairs[i - 1].ToString(), pairs[i]);
                }
            }
            return this;
        }

        public CommandBuilder addTime(string column, DateTime time) {
            addColumnAndValue(column, time);
            return this;
        }
        public CommandBuilder addTime(string column) {
            addFunc(column, "NOW()");
            return this;
        }

        public CommandBuilder addFunc(string val) {
            return addFunc(currentRow, dt.Columns[currentCol++].ColumnName, val);
        }
        public CommandBuilder addFunc(string col, string val) {
            return addFunc(currentRow, col, val);
        }
        public CommandBuilder addFunc(int index, string col, string val) {
            //dt.Rows[index][col] = val;
            addColumnAndValue(col, val);
            //dt.Rows[index].SetField(col, val);
            paramType[index].Add(dt.Columns.IndexOf(col), true);
            return this;
        }
        public CommandBuilder addValues(params object[] val) {
            for (int i = 0; i < val.Length; i++)
                addValues(currentRow, dt.Columns[currentCol++].ColumnName, val[i]);
            return this;
        }
        public CommandBuilder addValues(string[] col, object[] val) {
            return addValues(currentRow, col, val);
        }
        public CommandBuilder addValues(int index, string[] col, object[] val) {
            for (int i = 0; i < Math.Min(col.Length, val.Length); i++)
                addValues(index, col[i], val[i]);
            return this;
        }
        public CommandBuilder addValues(object val) {
            return addValues(currentRow, dt.Columns[currentCol++].ColumnName, val);
        }
        public CommandBuilder addValues(string col, object val) {
            return addValues(currentRow, col, val);
        }
        public CommandBuilder addValues(int index, string col, object val) {
            if (dt.Rows.Count == 0) {
                this.addBatch();
            }
            dt.Rows[index].SetField(col, val);
            return this;
        }

        #endregion

        #region Part Insert
        string insertInto = "";
        List<string> on_duplicate = new List<string>();
        public CommandBuilder INSERT_INTO(string table, IDataBase db) {
            this.db = db;
            return INSERT_INTO(table);
        }
        public CommandBuilder INSERT_INTO(string table) {
            insertInto = table;
            needUpdate = true;
            type = SqlType.insert;
            return this;
        }
        public CommandBuilder ON_DUPLICATE(params string[] cols) {
            for (int i = 0; i < cols.Length; i++)
                if (dt.Columns.Contains(cols[i]))
                    on_duplicate.Add(cols[i]);
            return this;
        }
        #endregion

        #region Part Update
        string update = "";
        public CommandBuilder UPDATE(string table, IDataBase db) {
            this.db = db;
            return UPDATE(table);
        }
        public CommandBuilder UPDATE(string table) {
            update = table;
            type = SqlType.update;
            return this;
        }
        #endregion
    }
}
