﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using GameLibrary;
using Tool;
using System.Data;
//using GameLibrary.Utility;
//using GameLibrary.Framework;

/// <summary>
/// Controller_opencredit 的摘要描述
/// </summary>
public partial class Controller {
    public void OpenerAct(Player LoginPlayer, MsgPack msg) {
        var sql = new MySQL("unidb");
        var type = msg.Type;
        var ct = msg.GetMsgPack("Content");
        bool isSuccess = false;
        string reason = "";
        int targetuid = ct.Get<int>("uid");
        //long price = 0;
        string price = ct.Get<string>("price");
        Pop_Type excuteType = (Pop_Type)ct.Get<int>("type");

        Player targetPlayer = new Player();
        if (!Member.GetPlayerData(sql, targetuid, targetPlayer)) {
            reason = "查無玩家";

            LoginPlayer.Send(type, new {
                Success = isSuccess,
                msg = reason
            });

            return;
        }

        long valid_price = 0;

        switch (excuteType) {            
            case Pop_Type.SETCREDIT:
                if (Rule_Shared.IsValid_CashCredit(LoginPlayer, targetPlayer, price, out reason) &&
                    Rule_SetCredit.IsValidSetCredit(LoginPlayer, targetPlayer, price, out reason)) {
                    valid_price = Convert.ToInt64(price);
                    Rule_SetCredit.Excute(sql, LoginPlayer, targetPlayer, valid_price);
                    isSuccess = true;
                }
                break;
            default:
                break;

        }
        LoginPlayer.Send("600.0", new {
            Success = isSuccess,
            msg = reason,
            type = excuteType
        });
    }
    
}