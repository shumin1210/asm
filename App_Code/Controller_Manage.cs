﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading;
using System.Web;
using GameLibrary;
using Tool;

public partial class Controller {
    Player mainManager;

    bool manager_login(Player p) {
        if (p.ManageAccess(this) == 0) return false;
        mainManager = p;        
        return true;
    }

    void ManagerAct(Player p, MsgPack msg) {
        var type = msg.Type;
        var ct = msg.GetMsgPack("Content");
        var mlv = p.ManageAccess(this);

        if (mlv == 0)
            return;
        if (mlv > 0 && Manage_lv1(p, type, ct))
            return;
        if (mlv > 1 && Manage_lv2(p, type, ct))
            return;
    }
    bool PlayerUpdated = false;
    bool GameUpdated { get { return Games.Count(gs => gs.Value.isUpdated) > 0; } }
    Dictionary<string, object> GameInfos = new Dictionary<string, object>();

    void managerThread() {
        foreach (var gs in Games) {
            var pack = gs.Value.GameData_Manage();
            GameInfos.Add(gs.Key, pack);
        }
        while (true) {
            if (mainManager != null) {
                if (!GameUpdated && !PlayerUpdated) continue;
                try {
                    //mainManager.Send(UpdateInfos(false));
                } catch (Exception) { }
                if (GameUpdated)
                    foreach (var item in Games)
                        item.Value.isUpdated = false;
                PlayerUpdated = false;
            }
            Thread.Sleep(500);
        }
    }
    
    #region ManageLV1

    bool Manage_lv1(Player p, string type, MsgPack ct) {

        switch (type) {

            case "player.info":
                PlayerInfo(p, ct);
                break;
            case "player.in":
            case "player.out":
                UpdatePlayerUD(p, ct);
                break;
            case "player.record":
                PlayerRecord(p, ct);
                break;
            case "report":
                GenReport(p);
                break;
            default:
                return false;
        }
        return true;
    }



    void PlayerInfo(Player p, MsgPack ct) {
        var seat = ct.Get<int>("seat");
        Player pl = FindPlayer(a => Convert.ToInt32(a["seat"]) == seat);
        if (pl != null)
            p.Send(PlayerInfo(pl));
        else
            p.Send(new ActionObject("error", new { msg = "player not exist" }));
    }

    ActionObject PlayerInfo(Player p) {
        return new ActionObject("player.info", new { p.Name, Seat = p["seat"], p.UD, p.Refund });

    }
    /// <summary>開/洗分</summary>
    void UpdatePlayerUD(Player p, MsgPack ct) {
        var seat = ct.Get<int>("seat");
        Player pl = FindPlayer(a => Convert.ToInt32(a["seat"]) == seat);

        if (pl != null) {
            try {
                long dif = ct.Get<long>("amount", true);
                int type = TConvert.ToBoolean(ct.Get<string>("type", true)) ? -1 : 1;
                var amount = (dif * type);
                pl.UD += amount;
                using (var sql = new MySQL("unidb")) {
                    Member.UpdateUserUD(sql, pl);
                    Member.CashFlow(sql, this, pl, amount, p);
                }
                p.Send(PlayerInfo(pl));
                pl.SendInfo();
            } catch (Exception) {
                p.Send(new ActionObject("error", new { msg = "invalid arguments" }));
            }
        }
    }
    void PlayerRecord(Player p, MsgPack ct) {
        var seat = ct.Get<int>("seat");
        Player pl = FindPlayer(a => Convert.ToInt32(a["seat"]) == seat);
        if (pl != null) {
            DataTable dt = null;
            foreach (var gs in Games)
                if (dt == null) dt = gs.Value.GetPlayerRecord(pl);
                else dt.Merge(gs.Value.GetPlayerRecord(pl), false, MissingSchemaAction.Add);
            p.Send(new ActionObject("player.record", new { Record = dt.toList(), Column = dt.ColumnNames(), seat }));
        } else {
            p.Send(new ActionObject("error", new { msg = "no such player" }));
        }

    }

    /// <summary>當班報表</summary>
    void GenReport(Player p) {

        DateTime Start = new DateTime();
        DateTime End = DateTime.Now;
        long totalIn = 0;
        long totalOut = 0;
        long totalBet = 0;
        long totalWin = 0;
        var cmd = CommandBuilder.Instance;
        DataTable dt = null;
        using (var sql = new MySQL("unidb")) {
            sql.ClearParameters();
            sql.TryAddParameter("controllerid", controllerID);
            sql.TryAddParameter("manager", p.ID);

            /*last report base*/
            var lastReport = sql.ExecuteScalar("SELECT time FROM cashflow_report WHERE controllerid=@controllerid ORDER BY time DESC LIMIT 1");
            if (TConvert.ToBoolean(lastReport))
                Start = Convert.ToDateTime(lastReport);

            sql.TryAddParameter("start", Start.toMySQLDateTime());
            sql.TryAddParameter("end", End.toMySQLDateTime());
            cmd.addTable("cashflow", "c").addTable("userinfo", "u");
            cmd.SELECT(
                "u.nickname AS name",
                "c.amount",
                "c.time AS origintime"
                ).FROM("cashflow").LEFT_JOIN("u", "c.uid=u.uid")
                .WHERE_params("controllerid")
                .WHERE_condition("time BETWEEN @start AND @end");
            dt = sql.ExecuteDataTable(cmd.Result);

            var bdt = sql.ExecuteDataTable("SELECT IFNULL(SUM(bets),0) AS bets,IFNULL(SUM(payout),0) AS payout FROM obaca_result WHERE time BETWEEN @start AND @end ");
            if (bdt.Rows.Count > 0) {
                totalBet = Convert.ToInt64(bdt.Rows[0]["bets"].ToString());
                totalWin = Convert.ToInt64(bdt.Rows[0]["payout"].ToString());
            }
            /*update last report*/
            DateTime now = DateTime.Now;
            sql.TryAddParameter("now", now.toMySQLDateTime());
            sql.Execute("INSERT INTO cashflow_report (controllerid,managerid,time) VALUES (@controllerid,@manager,@now)");

            dt.Columns.Add("time", typeof(string));
            foreach (DataRow dr in dt.Rows) {
                var amount = Convert.ToInt64(dr["amount"].ToString());
                if (amount > 0)
                    totalIn += amount;
                else
                    totalOut = amount;
                dr.SetField("time", Convert.ToDateTime(dr["origintime"].ToString()));
            }
            dt.Columns.Remove("origintime");
        }
        cmd.Free();
        if (dt != null) {
            p.Send(new ActionObject("report", new { Success = true, Manager = p.Name, Start, End, totalIn, totalOut, totalBet, totalWin, Column = dt.ColumnNames(), Data = dt.toList() }));
        } else {
            p.Send(new ActionObject("report", new { Success = false }));
        }

    }

    /// <summary>呼叫服務</summary>
    void CallServe(Player p) {
        bool Success = false;
        if (mainManager != null)
            Success = mainManager.Send(new ActionObject("serve", new { Seat = p["seat"], p.Name }));
        p.Send(new ActionObject("serve", new { Success }));
    }
    #endregion

    #region ManageLV2
    bool Manage_lv2(Player p, string type, MsgPack ct) {
        switch (type) {
            case "configs_request":
                RequestConfigs(p, ct);
                break;
            case "configs_set":
                SetConfigs(p, ct);
                break;
            default:
                return false;
        }
        return true;
    }

    private void SetConfigs(Player p, MsgPack ct) {
        var gamelist = Games.Distinct(g => g.GetType()).ToArray();
        var Errors = new Dictionary<string, Dictionary<string, string>>();
        var Updates = new Dictionary<string, Dictionary<string, string>>();
        using (var sql = new MySQL("unidb")) {
            foreach (var item in gamelist) {
                var title = item.Value.Title;
                if (ct.Exist(title)) {
                    Dictionary<string, string> _errors;
                    Dictionary<string, string> _updates;
                    if (!item.Value.SetConfigs(sql, ct.GetDict(title), out _updates, out _errors))
                        Errors.Add(title, _errors);
                    else if (_updates.Count > 0)
                        Updates.Add(title, _updates);
                }
            }
        }
        p.Send("configs_set", new {
            Success = Errors.Count == 0,
            Errors ,
            Updates 
        });
    }

    private void RequestConfigs(Player p, MsgPack ct) {
        var gamelist = Games.Distinct(g => g.GetType()).ToArray();
        var games = new Dictionary<string, object>();
        using (var sql = new MySQL("unidb")) {
            foreach (var item in gamelist) {
                var dt = item.Value.GetConfigs(sql);
                games.TryAddOrUpdate(item.Value.Title, new { Column = dt.ColumnNames(), Data = dt.toList() });
            }
        }

        p.Send(new ActionObject("configs_request", new { Data = games }));
    }

    #endregion
}