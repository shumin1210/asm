﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Init
{
    /// <summary>
    /// Init 的摘要描述
    /// </summary>
    public class Setting
    {
        public static int MaxOptionCount = 10;
        public static int MinOptionCount = 2;
        public static int DefaultOptionCount = 4;

        public static int BaseCostPoint = 50;
        public static int CommissionPercent = 1;

        public Setting()
        {
            //
            // TODO: 在此加入建構函式的程式碼
            //
        }
    }
}