﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using GameLibrary;
using Tool;
namespace Ssm {
    public static class SQLAction {
        /// <summary>取得牌局歷程</summary>

        public static DataTable GetPlayerRecord(IDataBase sql, Reporter gs, Player p) {
            DataTable result = null;
            var cmd = CommandBuilder.Instance;
            cmd.addTable("obaca_result", "r").addTable("obaca_playerrecord", "p");
            cmd.SELECT("r.gid", "r.round", "r.wintype", "r.pokers", "r.wipoint", "p.bets", "p.totalwin", "p.time").FROM("r").LEFT_JOIN("p", "r.id=p.recordid").WHERE_params("season");
            sql.ClearParameters();
            //sql.TryAddParameter("season", gs.Season);

            cmd.Free();
            return result;
        }

        public static DataTable GetLanguages(IDataBase sql, Reporter report, string Language) {
            DataTable result = null;
            var cmd = CommandBuilder.Instance;
            cmd.addTable("textmap", "t");
            cmd.SELECT("t.property", "t.text")
                .FROM("t")
                .WHERE_params("t.language", "t.GID"); ;
            sql.ClearParameters();
            sql.TryAddParameter("language", Language);
            sql.TryAddParameter("GID", report.gameid);

            result = sql.ExecuteDataTable(cmd.Result);

            cmd.Free();
            return result;
        }

        public static int GetOtherShopBonus(IDataBase sql, string GID, int UpLevelId, int addShopId) {
            sql.ClearParameters();

            sql.TryAddParameter("UpLevelId", UpLevelId);
            sql.TryAddParameter("addShopId", addShopId);
            sql.TryAddParameter("GID", GID);
            string strQuery = @"SELECT SUM(ShopBonus) FROM userinfo u LEFT JOIN gamefee f ON f.UID=u.UID WHERE u.UpLevelId=@UpLevelId ";
            if (addShopId != 0) {
                strQuery += " and u.UID !=@addShopId";
            }

            strQuery += " AND f.GID =@GID AND u.State=1 AND canAddNextLevel=1 ";

            object tmp = sql.ExecuteScalar(strQuery);

            if (!DBNull.Value.Equals(tmp)) {
                return Convert.ToInt32(tmp);
            } else {
                return 0;
            }
        }

        public static double GetMaxShopBonus(IDataBase sql, string GID, int UpLevelId, int addShopId) {
            sql.ClearParameters();

            sql.TryAddParameter("UpLevelId", UpLevelId);
            sql.TryAddParameter("addShopId", addShopId);
            sql.TryAddParameter("GID", GID);
            string strQuery = @"SELECT Max(f.ShopBonus) FROM userinfo u LEFT JOIN gamefee f ON f.UID=u.UID WHERE u.UpLevelId=@UpLevelId ";
            if (addShopId != 0) {
                strQuery += " and u.UID !=@addShopId";
            }

            strQuery += " AND f.GID =@GID AND u.State=1 ";

            object tmp = sql.ExecuteScalar(strQuery);

            if (!DBNull.Value.Equals(tmp)) {
                return Convert.ToDouble(tmp);
            } else {
                return 0;
            }
        }

        public static double GetShopBonusByID(IDataBase sql, string GID, int UID) {
            sql.ClearParameters();

            sql.TryAddParameter("UID", UID);
            sql.TryAddParameter("GID", GID);
            string strQuery = @"SELECT ShopBonus FROM userinfo u LEFT JOIN gamefee f ON f.UID=u.UID WHERE u.UID=@UID ";
            
            strQuery += " AND f.GID =@GID AND u.State=1 ";

            object tmp = sql.ExecuteScalar(strQuery);

            if (!DBNull.Value.Equals(tmp)) {
                return Convert.ToDouble(tmp);
            } else {
                return 0;
            }
        }

        public static bool CheckShopPermission(IDataBase sql, int adminId, int addShopId) {
            if (adminId == addShopId) {//加下一層級公司
                return true;
            }

            bool isOk = false;
            sql.ClearParameters();

            int shopId = addShopId;
            DataTable dt;

            string strQuery;

            while (shopId != 0) {
                sql.ClearParameters();
                sql.TryAddParameter("nowShopId", shopId);
                strQuery = @"SELECT UID,UpLevelId FROM userinfo where UID=@nowShopId ";
                dt = sql.ExecuteDataTable(strQuery);
                if (dt.Rows.Count > 0) {
                    if (adminId == Convert.ToInt32(dt.Rows[0]["UID"])) {
                        isOk = true;
                        break;
                    } else {//繼續上層尋找
                        isOk = false;
                        shopId = Convert.ToInt32(dt.Rows[0]["UpLevelId"]);
                    }
                } else {
                    isOk = false;
                    break;
                }
            }

            return isOk;
        }

        public static double GetUpLevelShopBonus(IDataBase sql, string GID, int UpLevelId) {
            sql.ClearParameters();

            sql.TryAddParameter("GID", GID);
            sql.TryAddParameter("UpLevelId", UpLevelId);

            string strQuery = @"SELECT f.ShopBonus,u.UpLevelId FROM gamefee f LEFT JOIN userinfo u ON u.UID=f.UID ";
            strQuery += " WHERE f.UID=@UpLevelId AND f.GID=@GID AND u.State=1  ";

            DataTable dt = sql.ExecuteDataTable(strQuery);
            if (dt.Rows.Count == 0) {
                return 0;
            }

            if (dt.Rows[0]["UpLevelId"].ToString() == "0") {
                //admin
                return -1;
            }

            return Convert.ToDouble(dt.Rows[0]["ShopBonus"]);
        }

        public static DataTable GetCompanyMember(IDataBase sql, int UID) {
            sql.ClearParameters();

            sql.TryAddParameter("UID", UID);
            string str = "SELECT UID FROM userinfo WHERE UpLevelId=@UID AND canAddNextLevel=0";
            return sql.ExecuteDataTable(str);
        }

        public static DataTable GetUpLevelIdTree(IDataBase sql, string shopId) {
            sql.ClearParameters();

            sql.TryAddParameter("shopId", shopId);
            string strQuery = @"SELECT UpLevelId,UpLevelTree,canAddNextLevel FROM userinfo where UID=@shopId ";

            return sql.ExecuteDataTable(strQuery);

        }

        public static double GetRefund(IDataBase sql, string GID, int UID) {
            sql.ClearParameters();

            sql.TryAddParameter("UId", UID);
            sql.TryAddParameter("GID", GID);
            string strQuery = @"SELECT f.Refund FROM gamefee f ";
            strQuery += " WHERE f.UID=@UId AND f.GID=@GID ";


            object tmp = sql.ExecuteScalar(strQuery);

            if (!DBNull.Value.Equals(tmp)) {
                return Convert.ToDouble(tmp);
            } else {
                return 0;
            }
        }

        public static double GetMaxRefund(IDataBase sql, string GID, int UpLevelId, int addShopId) {
            sql.ClearParameters();

            sql.TryAddParameter("UpLevelId", UpLevelId);
            sql.TryAddParameter("addShopId", addShopId);
            sql.TryAddParameter("GID", GID);
            string strQuery = @"SELECT Max(f.Refund) FROM userinfo u LEFT JOIN gamefee f ON f.UID=u.UID WHERE u.UpLevelId=@UpLevelId ";
            if (addShopId != 0) {
                strQuery += " and u.UID !=@addShopId";
            }

            strQuery += " AND f.GID =@GID AND u.State=1 ";

            object tmp = sql.ExecuteScalar(strQuery);

            if (!DBNull.Value.Equals(tmp)) {
                return Convert.ToDouble(tmp);
            } else {
                return 0;
            }
        }

        public static int GetOtherRefund(IDataBase sql, string GID, int UID) {
            sql.ClearParameters();

            sql.TryAddParameter("UID", UID);
            sql.TryAddParameter("GID", GID);
            string strQuery = @"SELECT UpLevelId FROM userinfo u WHERE u.UID=@UID AND u.State=1  ";

            object tmp = sql.ExecuteScalar(strQuery);

            int UpLevelId = 0;
            if (!DBNull.Value.Equals(tmp)) {
                UpLevelId = Convert.ToInt32(tmp);
            } else {
                return 0;
            }

            strQuery = @"SELECT SUM(f.Refund) FROM userinfo u LEFT JOIN gamefee f ON f.UID=u.UID ";
            strQuery += " WHERE u.UpLevelId=@UpLevelId AND u.UID !=@UID AND u.State=1  ";
            sql.TryAddParameter("UpLevelId", UpLevelId);

            tmp = sql.ExecuteScalar(strQuery);

            if (!DBNull.Value.Equals(tmp)) {
                return Convert.ToInt32(tmp);
            } else {
                return 0;
            }
        }

        //確認是否有使用過此autcode
        public static bool seautcode(IDataBase sql, string Code) {
            bool iscodeok = false;
            sql.ClearParameters();

            sql.TryAddParameter("Code", Code);

            string strQuery = "select Count(*) from userinfo where autcode=@Code";

            int count = Convert.ToInt32(sql.ExecuteScalar(strQuery));
            if (count > 0) {
                iscodeok = true;
            }

            return iscodeok;
        }

        public static string GetGameValidIP(IDataBase sql, int GID) {
            sql.ClearParameters();

            sql.TryAddParameter("gid", GID);
            var cmd = CommandBuilder.Instance;
            cmd.addTable("gameserver", "g");
            cmd.SELECT("g.ValidIP").FROM("g").WHERE_params("gid");

            object ip = sql.ExecuteScalar(cmd.Result);
            cmd.Free();

            return ip == null ? "" : ip.ToString();
        }


        #region QRCode

        public static void insertInviteCode(IDataBase sql, string inviteCode, string path) {
            sql.ClearParameters();

            sql.TryAddParameter("inviteCode", inviteCode);
            sql.TryAddParameter("path", path);

            string strQuery = @"INSERT INTO QRCode (inviteCode, path,CreateDate) " +
                       @"VALUES (@inviteCode, @path,now())";

            sql.Execute(strQuery);
        }

        public static bool checkExistInviteCode(IDataBase sql, string QRcode) {
            sql.ClearParameters();

            sql.TryAddParameter("inviteCode", QRcode);

            var cmd = CommandBuilder.Instance;

            cmd.SELECT("count(inviteCode)").FROM("QRCode").WHERE_params("inviteCode");

            if (Convert.ToInt32(sql.ExecuteScalar(cmd.Result)) > 0) {
                cmd.Free();
                return true;
            }

            cmd.Free();
            return false;
        }

        public static void DeleteInviteCode(IDataBase sql, string QRcode) {
            sql.ClearParameters();

            sql.TryAddParameter("inviteCode", QRcode);

            string str = "DELETE FROM qrcode WHERE inviteCode=@inviteCode";

            sql.Execute(str);
        }

        public static long allDataCount = 0;

        public static DataTable getQRList(IDataBase sql, MsgPack msg, bool isGetAll) {
            var cmd = CommandBuilder.Instance;

            cmd.addTable("QRCode", "q").addTable("userinfo", "u");

            cmd.SELECT("q.*,u.NickName").FROM("q").LEFT_JOIN("u", "u.UID=q.UID").ORDERBY("q.CreateDate Desc");

            if (!isGetAll) {
                #region 查詢相關

                /*查詢相關*/
                int status = msg.Get<int>("status");

                if (status == 1) {
                    /*有設定User*/
                    cmd.WHERE_condition("status=1");
                } else if (status == 2) {
                    /*沒設定User*/
                    cmd.WHERE_condition("status=0");
                }

                string searchuser = "";
                try {
                    searchuser = msg.Get<string>("searchuser");
                } catch (Exception) {
                    searchuser = "";
                }

                if (searchuser != "") {
                    /*有設定User*/
                    cmd.WHERE_condition("NickName Like @NickName");
                    sql.TryAddParameter("NickName", "%" + searchuser + "%");
                }


                if (!msg.Get<bool>("isinit")) {
                    /*分頁沒初始化過*/
                    cmd.CountOnly = true;
                    allDataCount = Convert.ToInt64(sql.ExecuteScalar(cmd.Result));
                }

                cmd.CountOnly = false;

                if (msg.Get<bool>("ispaging")) {
                    /*分頁*/
                    int perPageCount = msg.Get<int>("perpagecount");
                    int pageIndex = msg.Get<int>("pageindex");

                    cmd.LIMIT(" @pindex,@pcount ");
                    sql.TryAddParameter("pindex", pageIndex * perPageCount);
                    sql.TryAddParameter("pcount", perPageCount);
                }

                #endregion
            }

            DataTable dt = sql.ExecuteDataTable(cmd.Result);
            cmd.Free();


            return dt;
        }
        
        public static DataTable getQRUserList(IDataBase sql, Player p, MsgPack msg) {
            string upleveltree = msg.Get<string>("upleveltree");



            var cmd = CommandBuilder.Instance;

            cmd.addTable("QRCode", "q").addTable("userinfo", "u");


            cmd.SELECT("userinfo.uid", "userinfo.NickName", "userinfo.UpLevelId", "userinfo.UpLevelTree", "userinfo.canAddNextLevel")
                .FROM("userinfo")
                .WHERE_condition("State=1")
                .WHERE_condition("canAddNextLevel>0")
                .ORDERBY("uid");

            if (upleveltree == null) {
                sql.TryAddParameter("upleveltree", ",0,%");
                cmd.WHERE_condition("UpLevelTree like @upleveltree");
            }

            DataTable dt = sql.ExecuteDataTable(cmd.Result);
            cmd.Free();

            return dt;
        }

        public static bool CheckQRExist(IDataBase sql, string QRCode) {

            var cmd = CommandBuilder.Instance;

            cmd.addTable("QRCode", "q");

            cmd.SELECT("*")
                .FROM("q")
                .WHERE_params("inviteCode");

            cmd.CountOnly = true;

            sql.TryAddParameter("inviteCode", QRCode);

            int count = Convert.ToInt32(sql.ExecuteScalar(cmd.Result));
            cmd.Free();

            return count != 0;
        }

        public static void SetQRUser(IDataBase sql, string QRCode, int UID) {

            var cmd = CommandBuilder.Instance;

            cmd.UPDATE("QRCode", sql);
            cmd.WHERE_params("inviteCode");

            if (UID == 0) {
                cmd.addColumnAndValue("status", "0")
                    .addColumnAndValue("UID", "0");
            } else {
                cmd.addColumnAndValue("UID", UID)
                    .addColumnAndValue("status", "1");
            }

            sql.TryAddParameter("inviteCode", QRCode);

            sql.Execute(cmd.Result);
            cmd.Free();

        }

        #endregion

        public static DataTable getDayReportList(IDataBase sql, MsgPack msg) {
            /*查詢相關*/
            DateTime start ,end;
            try {
                start = Convert.ToDateTime( msg.Get<string>("start"));
                end = Convert.ToDateTime(msg.Get<string>("end")).AddDays(1);
            } catch (Exception) {
                return new DataTable();
            }

            var cmd = CommandBuilder.Instance;

            cmd.addTable("report_autolist", "a");

            cmd.SELECT("a.start,a.end,a.CreateDate,a.FileName").FROM("a").ORDERBY("a.CreateDate Desc");

            cmd.WHERE_condition("a.CreateDate BETWEEN @start AND @end");
            sql.TryAddParameter("start", start);
            sql.TryAddParameter("end", end);


            if (!msg.Get<bool>("isInit")) {
                /*分頁沒初始化過*/
                cmd.CountOnly = true;
                allDataCount = Convert.ToInt64(sql.ExecuteScalar(cmd.Result));
            }

            cmd.CountOnly = false;

            /*分頁*/
            int perPageCount = msg.Get<int>("perpagecount");
            int pageIndex = msg.Get<int>("pageindex");

            if(pageIndex == 1)
                cmd.LIMIT(" @pcount ");
            else
                cmd.LIMIT(" @pindex,@pcount ");
            sql.TryAddParameter("pindex", (pageIndex - 1) * perPageCount);
            sql.TryAddParameter("pcount", perPageCount);
            

            DataTable dt = sql.ExecuteDataTable(cmd.Result);
            cmd.Free();


            return dt;
        }

        public static void SetUserBan(IDataBase sql, MsgPack msg, Player p) {
            string status, query = "";
            int UID;
            var cmd = CommandBuilder.Instance;

            try {
                UID = msg.Get<int>("UserID");
                status = msg.Get<string>("status");

                Player targetPlayer = new Player();
                Member.GetPlayerData(sql, UID, targetPlayer);

                if (targetPlayer.ID == 0 || !Member.isCompanyPermission(p) ||
                    !targetPlayer["UpLevelTree"].ToString().Contains(p["UpLevelTree"].ToString())) {
                    //SendError(p, "edit.error", "無權限修改此公司");
                    return;
                }

                status = status == "0" ? "2" : "1";

                query += "UPDATE userinfo SET ";

                sql.ClearParameters();
                if (p["canAddNextLevel"].ToString() == "1") {
                    /*代理商,底下都受影響*/
                    query += " CompanyBan= " + status;
                    query += " WHERE UpLevelTree LIKE @upleveltree ";
                    query += " OR UID = @UID ";

                    sql.TryAddParameter("upleveltree", targetPlayer["UpLevelTree"].ToString() + targetPlayer.ID + ",%");
                    
                } else {
                    query += " betStatus= " + status;
                    query += " WHERE UID = @UID ";
                }

                sql.TryAddParameter("UID", targetPlayer.ID);

                sql.Execute(query);
            } catch (Exception) {
            }
            cmd.Free();
        }

        public static DataTable GetUserCreditInfo(IDataBase sql, int pID) {            
            if (pID == 0)
                return new DataTable();

            var cmd = CommandBuilder.Instance;
            cmd.addTable("userinfo", "u");
            cmd.SELECT("u.CreditMax", "u.CreditRemain", 
                "u.BankCredit","u.UpLevelId",
                "UpLevelTree","canAddNextLevel").FROM("u");
            cmd.WHERE_condition("u.uid=@uid");
            
            sql.ClearParameters();
            sql.TryAddParameter("uid", pID);
            DataTable dt = sql.ExecuteDataTable(cmd.Result);
            cmd.Free();
            return dt;            
        }

        public static DataTable GetAllLowerCompanyMemberList(IDataBase sql, Player p) {
            if (p.ID == 0 || !ShowData_Permission.isCompany(p))
                return new DataTable();

            var cmd = CommandBuilder.Instance;
            cmd.addTable("userinfo", "u");
            cmd.SELECT("u.UID").FROM("u");
            /*會員且沒被刪除*/
            cmd.WHERE_condition("u.State=1 AND u.canAddNextLevel=0")
               .WHERE_condition("UpLevelTree LIKE @upleveltree");
            
            sql.ClearParameters();
            sql.TryAddParameter("upleveltree", p["UpLevelTree"].ToString() + p.ID + ",%");

            DataTable dt = sql.ExecuteDataTable(cmd.Result);
            cmd.Free();

            return dt;
        }

        public static DataTable GetBonusInfo(IDataBase sql, string GID, int UID) {
            sql.ClearParameters();
            sql.TryAddParameter("GID", GID);
            sql.TryAddParameter("UID", UID);

            string strQuery = @"SELECT f.ShopBonus,u.UpLevelId FROM gamefee f LEFT JOIN userinfo u ON u.UID=f.UID ";
            strQuery += " WHERE f.UID=@UID AND f.GID=@GID AND u.State=1  ";

            DataTable dt = sql.ExecuteDataTable(strQuery);

            if (dt.Rows.Count ==0) {
                return dt;
            }

            if (dt.Rows[0]["UpLevelId"].ToString() == "0") {
                //admin
                dt.Rows[0]["ShopBonus"] = 100;
            }

            return dt;
        }

        public static DataTable GetReportCheckOutSetting(IDataBase sql, int CurrencyID) {
            sql.ClearParameters();            

            /*Get Currency List*/
            string strQuery = @"SELECT * FROM reportcurrency r ";
            DataTable cList = sql.ExecuteDataTable(strQuery);

            sql.TryAddParameter("CurrencyID", CurrencyID);
            /*check has data*/
            strQuery = @"SELECT * FROM reportcurrency r LEFT JOIN reportcheckout_config c ON c.CurrencyID=r.id ";
            strQuery += @" WHERE c.CurrencyID=@CurrencyID ";

            DataTable record = sql.ExecuteDataTable(strQuery);

            return SetCurrencyTimeSetting_DataTable(cList, record, CurrencyID);
        }

        static DataTable SetCurrencyTimeSetting_DataTable(DataTable currencyList, DataTable record, int CurrencyID) {
            DataTable dt = DataStruct.NewTable(DataStruct.DataType.SETWEEK);
            DataRow dr, record_dr;

            string weekStr;

            dr = dt.NewRow();
            DataStruct.SetDefaultVal(DataStruct.DataType.SETWEEK, dt, out dr);
            dr[(int)DataStruct.Col_SetWeek.ID] = 1;
            dr[(int)DataStruct.Col_SetWeek.Currency] = CurrencyID;

            for (int i = 0; i < record.Rows.Count; i++) {
                record_dr = record.Rows[i];

                weekStr = record_dr["Week"].ToString();

                if (DBNull.Value != record_dr["start"])
                    dr["w" + weekStr] = record_dr["start"].ToString();
            }

            dt.Rows.Add(dr);
            //for (int i = 0; i < currencyList.Rows.Count; i++) {
            //    r_index = tmp_record.Find(currencyList.Rows[i]["id"]);
            //    if (r_index != -1) {
            //        /*有找到設定值*/
            //        record_dr = record.Rows[r_index];

            //        weekStr = record_dr["Week"].ToString().Split(',');

            //        dr = dt.NewRow();
            //        DataStruct.SetVal(DataStruct.DataType.SETWEEK, dr, new object[]{
            //            record_dr["UID"],record_dr["CId"],record_dr["CurrenyID"],weekStr[0],weekStr[1],
            //            weekStr[2],weekStr[3],weekStr[4],weekStr[5],weekStr[6],1
            //        });
            //    } else {
            //        DataStruct.SetDefaultVal(DataStruct.DataType.SETWEEK, dt, out dr);
            //        dr[(int)DataStruct.Col_SetWeek.ID] = currencyList.Rows[i]["id"];
            //        dr[(int)DataStruct.Col_SetWeek.Currency] = currencyList.Rows[i]["id"];
            //        //dr[(int)DataStruct.Col_SetWeek.UserID] = targetUID;
            //    }
            //    dt.Rows.Add(dr);
            //}

            return dt;
        }

        public static bool SetCurrencyTimeSetting(IDataBase sql, int CurrencyID, object[] setting, out string reason) {
            reason = "";
            if (setting.Length != 7) {
                reason = "資料錯誤!";
                return false;
            }

            sql.ClearParameters();
            string str = "SELECT * FROM reportcheckout_config c WHERE c.CurrencyID=@CurrencyID";
            sql.TryAddParameter("CurrencyID", CurrencyID);
            DataTable setting_list = sql.ExecuteDataTable(str);

            DataView old_setting = new DataView(setting_list);
            old_setting.Sort = "week";

            int s_index;

            str = "";

            for (int i = 0; i < setting.Length; i++) {
                s_index = old_setting.Find(i);

                if (s_index == -1) {
                    str += "INSERT INTO reportcheckout_config (CurrencyID,week" + (setting[i].ToString() == "-" ? "" : ",start") + ") ";
                    str += " VALUES (@CurrencyID,"+i+(setting[i].ToString() == "-" ? "" : ",@start"+i)+");";
                    sql.TryAddParameter("start" + i, setting[i]);
                } else {
                    str += "UPDATE reportcheckout_config SET start="+(setting[i].ToString() == "-" ?"NULL":"@start" + i) + " WHERE CurrencyID=@CurrencyID AND week=" + i + ";";
                    sql.TryAddParameter("start" + i, setting[i]);
                }                
            }

            sql.Execute(str);

            return true;
        }

        public static DataTable GetCheckOutTimeList(IDataBase sql, string start, string end) {
            if (start == "" || end == "")
                return new DataTable();
            
            var cmd = CommandBuilder.Instance;
            cmd.addTable("report_checkoutlist", "c");

            cmd.SELECT("Start","End")
                .FROM("c")
                .WHERE_condition("Success=1");

            cmd.WHERE_condition("CreateDate BETWEEN @Start AND @End");
            sql.TryAddParameter("Start", start);
            sql.TryAddParameter("End", end);

            DataTable dt = sql.ExecuteDataTable(cmd.Result);
            cmd.Free();
            
            return dt;
        }

        public static void WriteCheckOutLog(IDataBase sql, string note,string ip = "", int type = 1) {

            string str = "INSERT INTO report_checkoutlog (CreateDate,isCheckOut,isCheckOutFinish,note,type,ip) ";
            str += " VALUES (now(),@isCheckOut,@isCheckOutFinish,@note,@type,@ip)";

            sql.ClearParameters();
            sql.TryAddParameter("isCheckOut", Reporter.isCheckOut ? 1 : 0);
            sql.TryAddParameter("isCheckOutFinish", Reporter.isCheckOutFinish ? 1 : 0);
            sql.TryAddParameter("note", note);
            sql.TryAddParameter("type", type);
            sql.TryAddParameter("ip", ip);
            sql.Execute(str);
            
        }

        public static void setCheckOutStatus(IDataBase sql, int state, int lId) {
            if (lId == 0)
                return;
            string str = "UPDATE report_checkoutlist SET State=@State ";
            str += " WHERE id=@lId";

            sql.ClearParameters();
            sql.TryAddParameter("State", state);
            sql.TryAddParameter("lId", lId);
            sql.Execute(str);

            bool isCheckOut, isCheckOutFinish;

            setCheckOutStatus(state, out isCheckOut, out isCheckOutFinish);
        }

        public static void setCheckOutStatus(IDataBase sql, int state) {
            int lId;
            bool isCheckOut,isCheckOutFinish;
            getCheckOutStatus(sql, out isCheckOut, out isCheckOutFinish, out lId);

            setCheckOutStatus(sql, state, lId);
        }

        public static void getCheckOutStatus(IDataBase sql, out bool isCheckOut, out bool isCheckOutFinish, out int lId) {
            DataTable list = Rule_CheckOut.GetLatestCashOutList(sql);
            int state = 0;

            if (list.Rows.Count > 0) {
                state = Convert.ToInt32(list.Rows[0]["State"]);
                lId = Convert.ToInt32(list.Rows[0]["id"]);
            } else
                lId = 0;

            setCheckOutStatus(state, out isCheckOut, out isCheckOutFinish);
        }

        

        static void setCheckOutStatus(int state, out bool isCheckOut, out bool isCheckOutFinish) {
            switch (state) {
                case 1:
                    /*結帳完畢,等待按結帳完畢按鈕*/
                    Reporter.isCheckOut = isCheckOut = true;
                    Reporter.isCheckOutFinish = isCheckOutFinish = true;
                    break;
                case 3:
                    /*結帳中,等待遊戲通知*/
                    Reporter.isCheckOut = isCheckOut = true;
                    Reporter.isCheckOutFinish = isCheckOutFinish = false;
                    break;
                default:
                case 2:
                    /*可結帳狀態*/
                    Reporter.isCheckOut = isCheckOut = false;
                    Reporter.isCheckOutFinish = isCheckOutFinish = true;
                    break;
            }
        }

        public static DataTable GetABCData(IDataBase sql) {
            string str = "SELECT * FROM abc";
            
            return sql.ExecuteDataTable(str);
        }
    }
}