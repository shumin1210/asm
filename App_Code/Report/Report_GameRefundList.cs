﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Linq;
using System.Threading;
using GameLibrary;
using Tool;
using Microsoft.Security.Application;
using System.Drawing;
using Col_GR = DataStruct.Col_GameRefund;
/// <summary>
/// Report_OpenCah 的摘要描述
/// </summary>
namespace Ssm {
    public partial class Reporter : iGame {
               
        void GameRefundList_Total(IDataBase sql, Player thisplayer, Dictionary<string, object> ct) {
            int Type = ct.Get<int>("Type");
            int pID = ct.Get<int>("pID");
            string start = ct.Get<string>("start");
            string end = ct.Get<string>("end");

            string SearchMember = ct.Get<string>("Member");

            Player p = new Player();
            Member.GetPlayerData(sql, pID, p);
            p["Language"] = thisplayer["Language"];
            CompanyTitle.UpdatePlayer(p);            

            DataTable pLowerList = new DataTable(), dt = new DataTable(), dt2 = new DataTable();

            //取得自己+所有下線總押注總贏分總退水
            pLowerList = getLowerList(sql, p);

            if ((int)thisplayer["canAddNextLevel"] == 0) {
                Type = 0;
            }

            DataTable dt_top;
            dt = GetDbGameRefundList_Total_Data(sql, start, end, p);
            dt = ConvertToDataTable_GameRefundList(dt, pLowerList, p, SearchMember, out dt_top);
            switch (Type) {
                case 1:
                case 2:
                    //玩家資料
                    Operation.Flow.Main(dt, pLowerList, p, Type, DataStruct.DataType.GAMEREFUND, out dt2, out dt);
                    
                    break;
                case 0:
                default:
                    Operation.Flow.Sub(dt, pLowerList, p, Type, thisplayer, SearchMember, DataStruct.DataType.GAMEREFUND, out dt);
                    break;
            }

            thisplayer.Send(new ActionObject("reporter.GameRefundList", new {
                pName = p["title"]+" " + p.Name,
                pID = p.ID,
                Column = dt.ColumnNames(),
                Record = dt.toList(),
                Column_User = dt2.ColumnNames(),
                Record_User = dt2.toList(),
                Currency = p["Currency"],
                dt_top = dt_top.toList()
            }));
        }

        public DataTable GetDbGameRefundList_Total_Data(IDataBase sql, string start, string end, Player p) {           
            
            var cmd = CommandBuilder.Instance;
            sql.ClearParameters();
            if ((int)p["canAddNextLevel"] == 0)
                sql.TryAddParameter("uid", p.ID);
            else
                sql.TryAddParameter("upleveltree", p["UpLevelTree"].ToString() + p.ID + ",%");
            cmd.addTable("userinfo", "u")
                .addTable("refund_close_record", "r");

            cmd.SELECT("r.uid As UserID",
                "u.NickName AS Name",
                "SUM(r.refund) AS Refund")
                .FROM("r")
                .LEFT_JOIN("u", "u.UID=r.uid")
                .WHERE_condition("u.State=1")
                .GROUPBY("r.uid");

            if (start != "" && end != "")
            {
                cmd.WHERE_condition("r.time BETWEEN @Start AND @End");
                sql.TryAddParameter("Start", start);
                sql.TryAddParameter("End", end);
            }

            if ((int)p["canAddNextLevel"] == 0)
                cmd.WHERE_condition("u.uid =@uid");
            else
                cmd.WHERE_condition("u.UpLevelTree LIKE @upleveltree");

            DataTable dt = sql.ExecuteDataTable(cmd.Result);
            cmd.Free();

            return dt;
        }

        public void GetDbGameRefundList_Detail_Data(IDataBase sql, Player p, Dictionary<string, object> ct) {
            PageCount = 0;
            int userID = ct.Get<int>("UserID");
            string modeID = ct.Get<string>("ModeID");
            int pageIndex = ct.Get<int>("pageindex");
            string start = ct.Get<string>("start");
            string end = ct.Get<string>("end");
            string Title = ct.Get<string>("Title");

            sql.ClearParameters();
            string query = "SELECT u.NickName AS Name,r.* ";
            query += " FROM refund_close_record r ";
            query += " LEFT JOIN userinfo u ON u.UID=r.uid ";
            query += " WHERE r.uid = @uid ";

            sql.TryAddParameter("uid", userID);

            if (start != "" && end != "") {
                query += " AND r.time BETWEEN @Start AND @End ";
                sql.TryAddParameter("Start", start);
                sql.TryAddParameter("End", end);
            }

            string orderby = " ORDER BY r.time DESC ";

            DataTable dt = sql.ExecuteDataTable(query + orderby);

            PageCount = Convert.ToInt32(dt.Rows.Count / perPageCount * 1.0);
            PageCount = dt.Rows.Count % perPageCount == 0 ? PageCount : PageCount += 1;

            string pageQuery = query + orderby;
            if (PageCount == 1 && pageIndex == 1) {
                pageQuery += " limit " + perPageCount;
            } else {
                pageQuery += " limit " + (pageIndex - 1) * perPageCount + "," + perPageCount;
            }
            dt = sql.ExecuteDataTable(pageQuery);
            
            p.Send(new ActionObject("reporter.GameRefundRecord", new {
                gameID = "GameRefundList",
                gameTitle = Title,
                Column = dt.ColumnNames(),
                Record = dt.toList(),
                pageCount = PageCount
            }));
        }

        DataTable ConvertToDataTable_GameRefundList(DataTable Record, DataTable loweList, Player thisPlayer, string SearchMember, out DataTable dt_top) {
            /*建立Total的DataTable*/
            DataTable dt = DataStruct.NewTable(DataStruct.DataType.GAMEREFUND);

            dt_top = DataStruct.NewTable(DataStruct.DataType.GAMEREFUND);

            DataRow r_top;
            DataStruct.SetDefaultVal(DataStruct.DataType.GAMEREFUND, dt_top, out r_top);
            dt_top.Rows.Add(r_top);
            
            int uid, status = 0;
            
            for (int i = 0; i < Record.Rows.Count; i++) {
                if (Convert.IsDBNull(Record.Rows[i]["UserID"]))
                    continue;
                uid = Convert.ToInt32(Record.Rows[i]["UserID"]);

                DataRow[] foundRows;
                foundRows = loweList.Select("uid = " + uid);
                if (foundRows.Length == 0)
                    continue;

                DataRow r = dt.NewRow();
                status = Ban.getIntStatus(foundRows[0]);

                DataStruct.SetVal(DataStruct.DataType.GAMEREFUND, r,
                    uid,Record.Rows[i]["Name"],Record.Rows[i]["Refund"],status, foundRows[0]["Currency"],0
                );

                dt.Rows.Add(r);
            }

            return dt;
        }
        
    }
}