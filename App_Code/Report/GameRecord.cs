﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using GameLibrary;
using Tool;

/// <summary>
/// GameRecord 的摘要描述
/// </summary>
public static class GameRecord {
    public static int PageCount;

    public static OBaca.Poker GetPoker(int index) { return index <= 0 ? OBaca.Poker.PokerBox[0] : OBaca.Poker.PokerBox.FirstOrDefault(p => p.ID == index); }

    public static DataTable GetGameTotal(IDataBase sql, List<string> p, string start, string end) {
        DataTable dt, agent_dt;
        var cmd = CommandBuilder.Instance;
        sql.ClearParameters();

        cmd.addTable("obaca_playerrecord", "p").addTable("obaca_result", "r");
        cmd.SELECT(
            "SUM(p.totalbet) AS totalBet",
            "SUM(p.totalwin) AS totalWin",
            "SUM(p.refund) AS Refund",
            "p.uid AS UserID",
            "p.bonusrate",
            "SUM(agent_refund) AS AgentRefund",
            "COUNT(p.time) AS DataCount")
            .FROM("p")
            .LEFT_JOIN("r", "p.recordid=r.id")
            .GROUPBY("p.uid");

        if (start != "" && end != "") {
            cmd.WHERE_condition("r.time BETWEEN @Start AND @End");
            sql.TryAddParameter("Start", start);
            sql.TryAddParameter("End", end);
        }

        List<string> whereString = new List<string>();
        for (int i = 0; i < p.Count; i++) {
            if (!string.IsNullOrWhiteSpace(p[i]) && p[i] != "0") {
                whereString.Add("p.uid=@" + "uid" + i);
                sql.TryAddParameter("uid" + i, p[i]);
            }
        }

        if (whereString.Count > 0)
            cmd.WHERE_condition("(" + string.Join(" OR ", whereString) + ")");

        dt = sql.ExecuteDataTable(cmd.Result);

        //DataColumn col = new DataColumn("tmpBet", typeof(Double));
        //col.DefaultValue = 0;
        //dt.Columns.Add(col);
        //col = new DataColumn("tmpWin", typeof(Double));
        //col.DefaultValue = 0;
        //dt.Columns.Add(col);

        //cmd.Free();

        //cmd = CommandBuilder.Instance;
        ///*compute agent refund totalbet totalwin*/
        //cmd.addTable("obaca_playerrecord", "p").addTable("obaca_result", "r");
        //cmd.SELECT(
        //    "p.totalbet AS totalBet",
        //    "p.totalwin AS totalWin",
        //    "p.uid AS UserID")
        //    .FROM("p")
        //    .LEFT_JOIN("r", "p.recordid=r.id");

        //if (start != "" && end != "")
        //{
        //    cmd.WHERE_condition("r.time BETWEEN @Start AND @End");
        //    sql.TryAddParameter("Start", start);
        //    sql.TryAddParameter("End", end);
        //}
        //if (whereString.Count > 0)
        //    cmd.WHERE_condition("(" + string.Join(" OR ", whereString) + ")");

        //agent_dt = sql.ExecuteDataTable(cmd.Result);
        //DataRow[] drs;
        //DataRow dr;
        //for (int i = 0; i < agent_dt.Rows.Count; i++)
        //{
        //    dr = agent_dt.Rows[i];
        //    if (Convert.ToInt64(dr["totalWin"]) <= Convert.ToInt64(dr["totalBet"]))
        //        continue;

        //    drs = dt.Select("UserID=" + dr["UserID"].ToString());
        //    if (drs.Length == 0)
        //        continue;

        //    drs[0]["tmpBet"] = Convert.ToInt64(dr["totalBet"]) + Convert.ToInt64(drs[0]["tmpBet"]);
        //    drs[0]["tmpWin"] = Convert.ToInt64(dr["totalWin"]) + Convert.ToInt64(drs[0]["tmpWin"]);
        //}

        cmd.Free();

        return dt;
    }

    public static DataTable GetPlayerRecord(IDataBase db, int UserID, int pageIndex, string start, string end, int perPageCount) {
        DataTable dt;
        string query = "SELECT list.name,p.time,r.round,r.pokers ";
        query += " ,r.wintype,p.totalwin,p.refund,p.bets,p.bonusrate,p.bettype as fee,p.agent_refund ";
        query += " FROM obaca_playerrecord p ";
        query += " LEFT JOIN obaca_result r ON p.recordid=r.id ";
        query += " LEFT JOIN gameidlist list ON r.gid=list.innergid ";
        query += " WHERE p.uid=@uid ";

        if (start != "" && end != "") {
            query += " AND r.time BETWEEN @Start AND @End ";
        }
        string orderby = " ORDER BY p.recordid DESC ";

        db.ClearParameters();
        db.TryAddParameter("uid", UserID);
        db.TryAddParameter("Start", start);
        db.TryAddParameter("End", end);

        dt = db.ExecuteDataTable(query + orderby);

        PageCount = Convert.ToInt32(dt.Rows.Count / perPageCount * 1.0);
        PageCount = dt.Rows.Count % perPageCount == 0 ? PageCount : PageCount += 1;

        string pageQuery = query + orderby;
        if (PageCount == 1 && pageIndex == 1) {
            pageQuery += " limit " + perPageCount;
        } else {
            pageQuery += " limit " + (pageIndex - 1) * perPageCount + "," + perPageCount;
        }
        dt = db.ExecuteDataTable(pageQuery);


        DataTable result = dt.Copy();
        result.Columns.Add("points", typeof(string));
        result.Columns["points"].SetOrdinal(4);
        result.Columns.Add("win", typeof(int));
        result.Columns["win"].SetOrdinal(5);
        for (int i = 0; i < dt.Rows.Count; i++) {
            var dr = result.Rows[i];
            if (dr["pokers"].ToString() != "") {
                var pokers = dr["pokers"].ToString().Split(',').Select(po => OBaca.Poker.GetPoker(Convert.ToInt32(po)));
                int playerPoint = pokers.Where((o, ind) => ind % 2 == 0).Sum(o => o.Value) % 10;
                int bankerPoint = pokers.Where((o, ind) => ind % 2 == 1).Sum(o => o.Value) % 10;
                dr.SetField("points", playerPoint + ":" + bankerPoint);
                dr.SetField("win", Array.IndexOf(dr["wintype"].ToString().Split(',').Select(w => TConvert.ToBoolean(w)).ToArray(), true));
            }
        }
        result.Columns.Remove("wintype");
        return result;
    }

}

namespace OBaca {
    public class Poker {
        public static List<Poker> PokerBox = new List<Poker>{
            new Poker(0, "nothing", 0, 0, 0, 0),
            new Poker(1, "spadeA", 1, 1, 1, 4),
            new Poker(2, "spade2", 2, 1, 2, 8),
            new Poker(3, "spade3", 3, 1, 3, 12),
            new Poker(4, "spade4", 4, 1, 4, 16),
            new Poker(5, "spade5", 5, 1, 5, 20),
            new Poker(6, "spade6", 6, 1, 6, 24),
            new Poker(7, "spade7", 7, 1, 7, 28),
            new Poker(8, "spade8", 8, 1, 8, 32),
            new Poker(9, "spade9", 9, 1, 9, 36),
            new Poker(10, "spade10", 10, 1, 10, 40),
            new Poker(11, "spadeJ", 11, 1, 10, 44),
            new Poker(12, "spadeQ", 12, 1, 10, 48),
            new Poker(13, "spadeK", 13, 1, 10, 52),
            new Poker(14, "heartA", 1, 2, 1, 3),
            new Poker(15, "heart2", 2, 2, 2, 7),
            new Poker(16, "heart3", 3, 2, 3, 11),
            new Poker(17, "heart4", 4, 2, 4, 15),
            new Poker(18, "heart5", 5, 2, 5, 19),
            new Poker(19, "heart6", 6, 2, 6, 23),
            new Poker(20, "heart7", 7, 2, 7, 27),
            new Poker(21, "heart8", 8, 2, 8, 31),
            new Poker(22, "heart9", 9, 2, 9, 35),
            new Poker(23, "heart10", 10, 2, 10, 39),
            new Poker(24, "heartJ", 11, 2, 10, 43),
            new Poker(25, "heartQ", 12, 2, 10, 47),
            new Poker(26, "heartK", 13, 2, 10, 51),
            new Poker(27, "diamondA", 1, 3, 1, 2),
            new Poker(28, "diamond2", 2, 3, 2, 6),
            new Poker(29, "diamond3", 3, 3, 3, 10),
            new Poker(30, "diamond4", 4, 3, 4, 14),
            new Poker(31, "diamond5", 5, 3, 5, 18),
            new Poker(32, "diamond6", 6, 3, 6, 22),
            new Poker(33, "diamond7", 7, 3, 7, 26),
            new Poker(34, "diamond8", 8, 3, 8, 30),
            new Poker(35, "diamond9", 9, 3, 9, 34),
            new Poker(36, "diamond10", 10, 3, 10, 38),
            new Poker(37, "diamondJ", 11, 3, 10, 42),
            new Poker(38, "diamondQ", 12, 3, 10, 46),
            new Poker(39, "diamondK", 13, 3, 10, 50),
            new Poker(40, "clubA", 1, 4, 1, 1),
            new Poker(41, "club2", 2, 4, 2, 5),
            new Poker(42, "club3", 3, 4, 3, 9),
            new Poker(43, "club4", 4, 4, 4, 13),
            new Poker(44, "club5", 5, 4, 5, 17),
            new Poker(45, "club6", 6, 4, 6, 21),
            new Poker(46, "club7", 7, 4, 7, 25),
            new Poker(47, "club8", 8, 4, 8, 29),
            new Poker(48, "club9", 9, 4, 9, 33),
            new Poker(49, "club10", 10, 4, 10, 37),
            new Poker(50, "clubJ", 11, 4, 10, 41),
            new Poker(51, "clubQ", 12, 4, 10, 45),
            new Poker(52, "clubK", 13, 4, 10, 49),
            new Poker(53, "error", 0, 0, 0, 0)};
        public static Poker GetPoker(int index) { return index <= 0 ? PokerBox[0] : PokerBox.FirstOrDefault(p => p.ID == index); }
        public static Poker GetPoker(int color, int point) { return PokerBox.FirstOrDefault(p => p.Color == color && p.Point == point); }
        public static Poker[] Reset(int length) {
            Poker[] p = new Poker[length];
            for (int i = 0; i < length; i++)
                p[i] = PokerBox[0];
            return p;
        }
        public static void Reset(ref Poker[] p) {
            for (int i = 0; i < p.Length; i++)
                p[i] = PokerBox[0];
        }
        /// <summary>撲克牌編號</summary>
        public int ID { get; set; }      //
        public string Name { get; set; } //team+level EX spade1
        /// <summary>牌面數字大小</summary>
        public int Point { get; set; }   //poker no 1~13 
        /// <summary>花色(0:ghost 4:spade 3:heart 2:diamond 1:club)</summary>
        public int Color { get; set; }
        /// <summary>點數</summary>
        public int Value { get; set; }
        /// <summary>牌堆中順位(biggest to smallest 52 to 0)</summary>
        public int Order { get; set; }

        /// <summary>撲克牌物件</summary>
        /// <param name="id">編號</param>
        /// <param name="name">花色+數字</param>
        /// <param name="number">牌面數字大小</param>
        /// <param name="color">花色(0:ghost 4:spade 3:heart 2:diamond 1:club)</param>
        /// <param name="value">點數</param>
        /// <param name="order">牌堆中順位(biggest to smallest 52 to 0)</param>
        /// 
        public Poker(int id, string name, int number, int color, int value, int order) {
            ID = id;
            Name = name;
            Point = number;
            Color = color;
            Value = value;
            Order = order;
        }
    }
}
