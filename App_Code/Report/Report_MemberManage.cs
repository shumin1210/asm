﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Linq;
using System.Threading;
using GameLibrary;
using Tool;
using Microsoft.Security.Application;
using System.Drawing;
using Col_M = DataStruct.Col_MemberManage;
/// <summary>
/// Report_OpenCah 的摘要描述
/// </summary>
namespace Ssm {
    public partial class Reporter : iGame {
                
        #region MemberManage
        void MemberManage_Total(IDataBase sql, Player thisplayer, Dictionary<string, object> ct) {
            int Type = ct.Get<int>("Type");
            int pID = ct.Get<int>("pID");

            string SearchMember = ct.Get<string>("Member");

            Player p = new Player();
            Member.GetPlayerData(sql, pID, p);
            p["Language"] = thisplayer["Language"];
            CompanyTitle.UpdatePlayer(p);            

            DataTable pLowerList = new DataTable(), dt = new DataTable(), dt2 = new DataTable();

            //取得自己+所有下線總押注總贏分總退水
            pLowerList = getLowerList(sql, p);

            if ((int)thisplayer["canAddNextLevel"] == 0) {
                Type = 0;
            }

            DataTable dt_top;
            dt = GetDbMemberManage_Total_Data(sql, pLowerList, p);
            dt = ConvertToDataTable_MemberManage(dt, p, SearchMember, out dt_top);
            switch (Type) {
                case 1:
                case 2:
                    //玩家資料
                    Operation.Flow.Main(dt, pLowerList, p, Type, DataStruct.DataType.MEMBERMANAGE, out dt2, out dt);
                    
                    dt = ComputeCreditSum(dt, pLowerList);
                    dt2 = ComputeCreditSum_LowerMember(dt2, pLowerList, p);
                    break;
                case 0:
                default:
                    Operation.Flow.Sub(dt, pLowerList, p, Type, thisplayer, SearchMember, DataStruct.DataType.MEMBERMANAGE, out dt);
                    dt = ComputeCreditSum(dt, pLowerList);
                    break;
            }

            thisplayer.Send(new ActionObject("reporter.OpenCash", new {
                pName = p["title"]+" " + p.Name,
                pID = p.ID,
                Column = dt.ColumnNames(),
                Record = dt.toList(),
                Column_User = dt2.ColumnNames(),
                Record_User = dt2.toList(),
                Currency = p["Currency"],
                dt_top = dt_top.toList()
            }));
        }

        public DataTable GetDbMemberManage_Total_Data(IDataBase sql, DataTable list, Player p) {
            DataTable c_list = Rule_CheckOut.GetLatestCashOutList(sql);        

            string str = "";

            if (c_list.Rows.Count > 0) {
                str += " AND s.CreateDate > " + "'" + Convert.ToDateTime(c_list.Rows[0]["CreateDate"]).ToString("yyyy/MM/dd HH:mm:ss") + "' ";
            }

            var cmd = CommandBuilder.Instance;
            sql.ClearParameters();
            if ((int)p["canAddNextLevel"] == 0)
                sql.TryAddParameter("uid", p.ID);
            else
                sql.TryAddParameter("upleveltree", p["UpLevelTree"].ToString() + p.ID + ",%");
            cmd.addTable("sequence", "s")
                .addTable("sequenceplace", "p")
                .addTable("sequencemode", "m")
                .addTable("userinfo", "u")
                .addTable("gamefee", "f");

            cmd.SELECT("s.UID As UserID",
                "u.NickName AS Name",
                "u.UpLevelId",
                "u.UpLevelTree",
                "u.canAddNextLevel",
                "u.Currency",
                "SUM(s.Price) AS Price",
                "SUM(s.Point) AS UD",
                "u.BankCredit",
                "s.SequenceModeID AS GID",
                "s.CreateDate",
                "p.Place",
                "m.Mode",
                "f.ShopBonus",
                "f.Refund",
                "u.CompanyBan",
                "u.betStatus",
                "u.CreditMax",
                "u.CreditRemain",
                "IFNULL((SELECT SUM(s.Price) FROM sequence s WHERE s.UID=u.UID " + str + " AND s.SequenceModeID='E'),0) AS CashSum"
                )
                .FROM("u")
                .LEFT_JOIN("s", "u.UID=s.uid")
                .LEFT_JOIN("f", "f.UID=s.uid")
                .LEFT_JOIN("p", "p.SequencePlaceID=s.SequencePlaceID")
                .LEFT_JOIN("m", "m.SequenceModeID=s.SequenceModeID")                
                .WHERE_condition("(s.SequenceModeID = 'E')")
                .WHERE_condition("u.State=1")
                .GROUPBY("s.uid,s.SequenceModeID");

            if ((int)p["canAddNextLevel"] == 0)
                cmd.WHERE_condition("u.uid =@uid");
            else
                cmd.WHERE_condition("u.UpLevelTree LIKE @upleveltree");

            DataTable dt = sql.ExecuteDataTable(cmd.Result);
            cmd.Free();

            return dt;
        }

        public void GetDbMemberManage_Detail_Data(IDataBase sql, Player p, Dictionary<string, object> ct) {
            PageCount = 0;
            int userID = ct.Get<int>("UserID");
            string modeID = ct.Get<string>("ModeID");
            int pageIndex = ct.Get<int>("pageindex");
            string start = ct.Get<string>("start");
            string end = ct.Get<string>("end");
            string Title = ct.Get<string>("Title");

            sql.ClearParameters();
            string query = "SELECT s.UID As UserID,u.NickName AS Name,u.Currency,s.Price,s.Point AS UD,";
            query += "p.Place,m.Mode,f.ShopBonus,f.Refund,s.CreateDate,s.SequenceModeID AS GID,u.BankCredit,u.CreditMax,u.canAddNextLevel,u.CreditRemain ";
            query += " FROM sequence s ";
            query += " LEFT JOIN userinfo u ON u.UID=s.uid ";
            query += " LEFT JOIN gamefee f ON f.UID=s.uid ";
            query += " LEFT JOIN sequenceplace p ON p.SequencePlaceID=s.SequencePlaceID ";
            query += " LEFT JOIN sequencemode m ON m.SequenceModeID=s.SequenceModeID ";
            query += " WHERE s.SequenceModeID = @modeID ";
            query += " AND s.uid = @uid ";

            sql.TryAddParameter("uid", userID);
            sql.TryAddParameter("modeID", modeID);

            if (start != "" && end != "") {
                query += " AND s.CreateDate BETWEEN @Start AND @End ";
                sql.TryAddParameter("Start", start);
                sql.TryAddParameter("End", end);
            }

            string orderby = " ORDER BY s.CreateDate DESC ";

            DataTable dt = sql.ExecuteDataTable(query + orderby);

            PageCount = Convert.ToInt32(dt.Rows.Count / perPageCount * 1.0);
            PageCount = dt.Rows.Count % perPageCount == 0 ? PageCount : PageCount += 1;

            string pageQuery = query + orderby;
            if (PageCount == 1 && pageIndex == 1) {
                pageQuery += " limit " + perPageCount;
            } else {
                pageQuery += " limit " + (pageIndex - 1) * perPageCount + "," + perPageCount;
            }
            dt = sql.ExecuteDataTable(pageQuery);

            DataTable dt_top;

            dt = ConvertToDataTable_MemberManage(dt, p, "", out dt_top);

            p.Send(new ActionObject("reporter.GameRecord", new {
                gameID = "CashInOut",
                gameTitle = Title,
                Column = dt.ColumnNames(),
                Record = dt.toList(),
                pageCount = PageCount
            }));
        }

        DataTable ConvertToDataTable_MemberManage(DataTable Record, Player thisPlayer, string SearchMember, out DataTable dt_top) {
            /*建立Total的DataTable*/
            DataTable dt = DataStruct.NewTable(DataStruct.DataType.MEMBERMANAGE);

            dt_top = DataStruct.NewTable(DataStruct.DataType.MEMBERMANAGE);

            DataRow r_top;
            DataStruct.SetDefaultVal(DataStruct.DataType.MEMBERMANAGE, dt_top, out r_top);
            dt_top.Rows.Add(r_top);

            r_top[Col_M.CreditSum.ToString()] = thisPlayer["CreditMax"];
            r_top[Col_M.CreditRemain.ToString()] = thisPlayer[Col_M.CreditRemain.ToString()];

            int uid, status = 0;
            long Price, cash;
            double bonus = 0, bonusrate = 0, agentBonus = 0, companyBonus = 0;
            string Mode = "";

            for (int i = 0; i < Record.Rows.Count; i++) {
                if (Convert.IsDBNull(Record.Rows[i]["UserID"]))
                    continue;
                uid = Convert.ToInt32(Record.Rows[i]["UserID"]);

                bonusrate = Convert.ToDouble(Record.Rows[i]["ShopBonus"]);

                Price = Convert.ToInt64(Record.Rows[i]["Price"]);
                bonus = bonusrate / 100;

                Mode = Record.Rows[i]["GID"].ToString();

                agentBonus = Price * 1.0 * bonus;
                companyBonus = Price * 1.0 * (1 - bonus);
                cash = 0;

                DataRow r = dt.NewRow();
                status = Ban.getIntStatus(Record.Rows[i]);

                DataStruct.SetVal(DataStruct.DataType.MEMBERMANAGE, r,
                    uid,Record.Rows[i]["Name"],Record.Rows[i]["Refund"],Record.Rows[i]["BankCredit"],
                    Record.Rows[i]["Mode"],bonus,bonusrate,Record.Rows[i]["Currency"],
                    Math.Round(agentBonus,0,MidpointRounding.AwayFromZero),Math.Round(companyBonus,0,MidpointRounding.AwayFromZero),
                    cash,1,Record.Rows[i]["GID"],Record.Rows[i]["CreateDate"],status, Record.Rows[i]["CreditMax"], Record.Rows[i]["CreditMax"],
                    Record.Rows[i]["Place"], Record.Rows[i]["Price"], Record.Rows[i]["UD"], Record.Rows[i]["canAddNextLevel"], 
                    Record.Rows[i]["CreditRemain"], "-", 0, Record.Rows[i]["CashSum"]
                );

                dt.Rows.Add(r);
            }

            return dt;
        }
        
        DataTable ComputeCreditSum(DataTable dt, DataTable pList) {
            if (!dt.Columns.Contains(Col_M.CreditSum.ToString()))
                return dt;

            EnumerableRowCollection<DataRow> playerList = pList.AsEnumerable();
            DataView result = new DataView(dt);
            result.Sort = Col_M.UserID.ToString();

            int k;
            for (int i = 0; i < pList.Rows.Count; i++) {
                k = result.Find(Convert.ToInt32(pList.Rows[i]["uid"]));
                if (k == -1)
                    continue;

                var agent = pList.Rows[i]["UpLevelTree"].ToString() + pList.Rows[i]["uid"].ToString() + ",";
                //var data = playerList.Where(d => d.Field<string>("UpLevelTree").Contains(agent))
                //    .Sum(d => d.Field<long>("CreditMax"));

                dt.Rows[k][(int)Col_M.CreditSum] = Convert.ToInt64(pList.Rows[i]["CreditMax"]);
            }

            return dt;
        }

        public DataTable ComputeCreditSum_LowerMember(DataTable dt, DataTable pList, Player p) {
            if (!dt.Columns.Contains(Col_M.CreditSum.ToString()) && dt.Rows.Count == 0)
                return dt;

            EnumerableRowCollection<DataRow> playerList = pList.AsEnumerable();
            for (int i = 0; i < pList.Rows.Count; i++) {
                if (ShowData_Permission.Lower_MemberAndStockholder(pList.Rows[i],p,pList.Rows[i]["UpLevelId"].ToString())) {
                    /*自己會員OR股東*/
                    dt.Rows[0][(int)Col_M.CreditSum] = Convert.ToInt64(dt.Rows[0][(int)Col_M.CreditSum]) + Convert.ToInt64(pList.Rows[i]["CreditMax"]);
                    //dt.Rows[0][(int)Col_M.RemainRefund] = Convert.ToDouble(dt.Rows[0][(int)Col_M.RemainRefund]) + Convert.ToInt64(pList.Rows[i]["CreditMax"]);
                }
            }

            return dt;
        }

        #endregion

        #region StockholderManage
        void StockholderManage_Total(IDataBase sql, Player thisplayer, Dictionary<string, object> ct) {
            int Type = ct.Get<int>("Type");
            int pID = ct.Get<int>("pID");

            string SearchMember = ct.Get<string>("Member");

            Player p = new Player();
            Member.GetPlayerData(sql, pID, p);
            p["Language"] = thisplayer["Language"];
            CompanyTitle.UpdatePlayer(p);

            DataTable pLowerList = new DataTable(), dt = new DataTable(), dt2 = new DataTable();

            //取得自己+所有下線總押注總贏分總退水
            pLowerList = getLowerList_Stockholder(sql, p);

            if ((int)thisplayer["canAddNextLevel"] == 0) {
                Type = 0;
            }

            dt = GetDbStockholderManage_Data(sql, pLowerList, p);
            dt = ConvertToDataTable_StockholderManage(dt, p, SearchMember);
            switch (Type) {
                case 1:
                case 2:
                    Operation.Flow.Main(dt, pLowerList, p, Type, DataStruct.DataType.STOCKHOLDERMANAGE, out dt2, out dt);
                    break;
                case 0:
                default:
                    Operation.Flow.Sub(dt, pLowerList, p, Type, thisplayer, SearchMember, DataStruct.DataType.STOCKHOLDERMANAGE, out dt);
                    break;                             
            }

            thisplayer.Send(new ActionObject("reporter_Stockholder", new {
                pName = p["title"] + " " + p.Name,
                pID = p.ID,
                Column = dt.ColumnNames(),
                Record = dt.toList(),
                Column_User = dt2.ColumnNames(),
                Record_User = dt2.toList(),
                Currency = p["Currency"]
            }));
        }

        public DataTable GetDbStockholderManage_Data(IDataBase sql, DataTable list, Player p) {
            
            var cmd = CommandBuilder.Instance;
            sql.ClearParameters();
            if ((int)p["canAddNextLevel"] == 0)
                sql.TryAddParameter("uid", p.ID);
            else
                sql.TryAddParameter("upleveltree", p["UpLevelTree"].ToString() + p.ID + ",%");
            cmd.addTable("userinfo", "u");

            cmd.SELECT("u.UID As UserID",
                "u.NickName AS Name",
                "u.UpLevelId",
                "u.UpLevelTree",
                "u.canAddNextLevel",
                "u.Currency",
                "u.CompanyBan",
                "u.betStatus"
                )
                .FROM("u")
                .WHERE_condition("u.State=1");

            if ((int)p["canAddNextLevel"] == 0)
                cmd.WHERE_condition("u.uid =@uid");
            else
                cmd.WHERE_condition("u.UpLevelTree LIKE @upleveltree AND canAddNextLevel>0");

            DataTable dt = sql.ExecuteDataTable(cmd.Result);
            cmd.Free();

            return dt;
        }

        DataTable ConvertToDataTable_StockholderManage(DataTable Record, Player thisPlayer, string SearchMember) {
            /*建立Total的DataTable*/
            DataTable dt = DataStruct.NewTable(DataStruct.DataType.STOCKHOLDERMANAGE);

            int uid, status = 0;

            for (int i = 0; i < Record.Rows.Count; i++) {
                if (Convert.IsDBNull(Record.Rows[i]["UserID"]))
                    continue;
                uid = Convert.ToInt32(Record.Rows[i]["UserID"]);
                
                DataRow r = dt.NewRow();
                status = Ban.getIntStatus(Record.Rows[i]);

                DataStruct.SetVal(DataStruct.DataType.STOCKHOLDERMANAGE, r,
                    uid, Record.Rows[i]["Name"], Record.Rows[i]["Currency"], status,1
                );

                dt.Rows.Add(r);
            }

            return dt;
        }
        
        #endregion

        public void changeRole(IDataBase sql, Player thisplayer, Dictionary<string, object> ct) {
            bool Success = false;
            try {
                int role = ct.Get<int>("role");
                int pID = ct.Get<int>("UserID");

                if (role != 0 && role != 2) {
                    /*非設定會員.股東*/
                    thisplayer.Send(new ActionObject("user.changeRole", new {
                        Success
                    }));
                    return;
                }

                if (!Member.isCompanyPermission(thisplayer)) {
                    thisplayer.Send(new ActionObject("user.changeRole", new {
                        Success
                    }));
                    return;
                }

                Player p = new Player();
                Member.GetPlayerData(sql, pID, p);

                if (p["UpLevelId"].ToString() != thisplayer.ID.ToString()) {
                    /*非自己下線*/
                    thisplayer.Send(new ActionObject("user.changeRole", new {
                        Success
                    }));
                    return;
                }
                Member.UpdateRole(sql, pID, role);

                Success = true;

                thisplayer.Send(new ActionObject("user.changeRole", new {
                    Success
                }));
                return;
            } catch (Exception) { }

            thisplayer.Send(new ActionObject("user.changeRole", new {
                Success
            }));
        }
    }
}