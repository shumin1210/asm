﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading;
using GameLibrary;
using Tool;
using System.Web;
using Microsoft.Security.Application;
using System.Drawing;
using WebSocketSharp;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.html;
using iTextSharp.text.pdf;
using iTextSharp.text.html.simpleparser;
using System.Text;
using iTextSharp.tool.xml;
using Col_G = DataStruct.Col_Total;
using Col_Trans = DataStruct.Col_Trans;
using Col_HomePage = DataStruct.Col_HomePage;
using Operation;
namespace Ssm {
    public enum ErrorType { FAILED, NO_MONEY, OVER_LIMIT, WRONG_STEP }

    public enum Layout {
        NONE, LOGIN, MAINPAGE,
        GAMELIST, LAYER1, LAYER2
    }

    public enum Modes { VIEW, ADD, EDIT }    
}

namespace Ssm {
    public partial class Reporter : iGame {
        Dictionary<string, DataRow> gameList = new Dictionary<string, DataRow>();
        public static Dictionary<string, string> Currency = new Dictionary<string, string>();
        public static bool isCheckOut = false;
        public static bool isCheckOutFinish = true;
        

        Tool.Logger log;
        

        public Reporter() {
            gameid = 31;
            log = new Tool.Logger(GID) { rootPath = @"c:\\inetpub\\asm\\logs\\asm\\" };
        }
        IDataBase db = new MySQL("unidb");
        
        #region ========Configs
        /// <summary>使用的規則類別</summary>

        /// <summary>其他資訊</summary>
        public Dictionary<string, string> Configs;

        public List<string> managerIPs = new List<string>();
        public List<int> managerIDs = new List<int>();

        protected override void DisposeObjects() { }
        #endregion

        #region ========GameData

        public DateTime Time_End = DateTime.Now;

        #endregion

        #region ========初始化
        public override void ImportConfigs(Dictionary<string, string> cfg) {
            Configs = cfg;
        }

        public override void LateInitialize() {
            //SQLAction.GetGameRecord(db, this);
            //ResetRound();
            //info.update(this);
        }
        #endregion

        public int perPageCount = 10, PageCount = 0;

        /// <summary>接收遊戲指令</summary>
        public override void Play(Player p, MsgPack content) {
            string type = content.Get<string>("Type");
            var ct = content.GetMsgPack("Content");

            if (type == "auto.report") {
                CheckOutExcute(db, p);
                //Trans_Reporter_Total(db, p, ct, true, 0);
                return;
            }

            if (!CheckActTime(p.ID)) {
                p.Send("error", new {
                    Msg = "超過時間"
                });
                return;
            }

            switch (type) {
                case "request.online":
                    GetOnline(p);
                    break;
                case "request.cashout_list":
                    GetCashOutList(p);
                    break;
                case "enter_report":
                    EnterReport(p, ct);
                    break;
                case "transAction_report":
                    Trans_Reporter_Total(db, p, ct, false, 1);
                    break;
                case "reporter_TransRecord":
                    GetDbTrans_Detail_Data(db, p, ct);
                    break;
                case "report_total":
                    Reporter_Total(db, p, ct);
                    break;
                case "reporter_GameRecord":
                    Reporter_GameRecord(db, p, ct);
                    break;
                case "opencash_total":
                    MemberManage_Total(db, p, ct);
                    break;
                case "reporter_Stockholder":
                    StockholderManage_Total(db, p, ct);
                    break;
                case "reporter_GameRefundList":
                    GameRefundList_Total(db, p, ct);
                    break;
                case "reporter_CashInOutRecord":
                    GetDbMemberManage_Detail_Data(db, p, ct);
                    break;
                case "reporter_GameRefundListRecord":
                    GetDbGameRefundList_Detail_Data(db, p, ct);
                    break;
                case "user.changeRole":
                    changeRole(db, p, ct);
                    break;
                case "editdata":
                    AddEditData(p, ct);
                    GetAccountSum(db, p);
                    break;
                case "deletedata":
                    DeleteDate(p, ct);
                    GetAccountSum(db, p);
                    break;
                case "request.userdata":
                    GetUserData(p, ct);
                    break;
                case "request.language":
                    p["Language"] = Languages.SetString(ct.Get<string>("lan"));
                    DataTable dt = SQLAction.GetLanguages(db, this, p["Language"].ToString());
                    string title;
                    CompanyTitle.Get(p, (int)(p["Level"]), out title);
                    p["title"] = title;
                    p.Send("request.language", new {
                        GID,
                        Record = dt.toList(),
                        Title = p["title"]
                    });
                    break;
                case "request.usercredit":
                    GetUserCredit(p, ct);
                    break;
                case "request.setting_week":
                    GetWeekData(p, ct);
                    break;
                case "request.setting_confirm":
                    ConfirmWeekData(db, p, ct);
                    break;
                case "get.homepage":
                    GetHomePageData(db, p, ct);
                    break;
                case "generate.qrcode":
                    QRCreate(p, ct);
                    break;
                case "delete.qrcode":
                    QRDelete(p, ct);
                    break;
                case "get.qrcodelist":
                    GetQRList(p, ct, false);
                    break;
                case "get.qruserlist":
                    GetQRUserList(p, ct);
                    break;
                case "set.qruser":
                    SetQRUser(p, ct);
                    break;
                case "get.printqrlist":
                    GetQRList(p, ct, true);
                    break;
                case "print_report":
                    //pdfEBookConvert(p, ct.Get<string>("content"));
                    Trans_Reporter_Total(db, p, ct, true, 1);
                    break;
                case "request.DayReport":
                    GetDayReportList(p, ct);
                    break;
                case "user.ban":
                    UserBan(db, p, ct);
                    break;
                case "request.checkout":
                    CheckOutExcute(db, p);
                    break;
                case "request.checkout_finish":
                    CheckOutFinish(db, p);
                    break;
                case "request.checkout_status":
                    p.Send(CheckOutStatus());
                    break;
                case "restore":
                    Rule_CheckOut.restore(db);
                    break;
                case "request.cashout_list_history":
                    GetCashOutHistoryList(p, ct);
                    break;
                case "edit.password":
                    EditPassword(db, p, ct);
                    break;
                case "add.shareholder":
                    AddShareHolder(db, p, ct);
                    break;
                case "request.updatecheckout":
                    GetLastCheckOutTime(db, p);
                    break;
                case "600.0":
                    callToGameCashInOut(db, p, ct);
                    break;
                case "request.checkouttimelist":
                    GetCheckOutTime(db, p, ct);
                    break;
                case "request.autochecklist":

                    break;
                case "request.checkoutrestore":
                    CheckOutRestore(db, p);
                    break;
                case "request.checkoutrelease":
                    CheckOutRelease(db, p);
                    break;
                case "request.accountsum":
                    GetAccountSum(db, p);
                    break;
                case "abc.setstatus":
                    if (!ShowData_Permission.isAdmin(p)) {
                        ABC.SendABCStatus(p, false);
                        return;
                    }
                    ABC.ABCStatus = ct.Get<int>("status");
                    ABC.SendABCStatus(p);
                    break;
                case "abc.getstatus":
                    if (!ShowData_Permission.isAdmin(p)) {
                        ABC.SendABCStatus(p, false);
                        return;
                    }
                    ABC.SendABCStatus(p);
                    break;
                case "abc.requestdata":
                    ABC.RequestABCData(db, p);
                    break;
                case "abc.sendtogame":
                    if(!ABC.CheckValueSendABCToGame(db, p, ct))
                        return;

                    object[] record = ct.Get<object[]>("Record");
                    callToGameABC(p, record);
                    break;
                case "abc.delete":
                    if (!ShowData_Permission.isAdmin(p))
                        return;

                    ABC.deleteABC(ct.Get<object[]>("Column"), ct.Get<object[]>("Record"));
                    ABC.RequestABCData(db, p);
                    break;
                default:
                    break;
            }

        }

        public override bool checkTime(Player p, MsgPack content) {
            string type = content.Get<string>("Type");
            var ct = content.GetMsgPack("Content");

            bool timecheck = false;

            try {
                int pId = ct.Get<int>("pID");
                string ip = SQLAction.GetGameValidIP(db, ct.Get<int>("GameID"));
                timecheck = ip != "" && p.MsgHandler.getIP() == ip && CheckActTime(pId);

            } catch (Exception) { }

            return timecheck;
        }

        #region ========玩家指令

        /// <summary>玩家離開</summary>
        public override bool RemovePlayer(Player p) {
            return base.RemovePlayer(p);

        }

        public override bool AddPlayer(Player p) {
            if (!base.AddPlayer(p))
                return false;
            return true;
        }
        #endregion


        /// <summary>回報錯誤</summary>
        public void Report(Player actor, ErrorType type, string msg) {
            Report(actor, (int)type, msg);
        }

        /// <summary>base 回報錯誤</summary>
        public override void Report(Player actor, int type, string Msg) {
            actor.Send(new ActionObject("error", new { GID, Msg }));
        }

        /// <summary>管理者檢測</summary>
        public override bool isManager(Player p) {
            if (p.ManageAccess(this) > 0)
                return true;
            else
                return false;
        }

        public override DataTable GetRecord() {
            return null;
        }

        public override DataTable GetPlayerRecord(Player p) {
            return null;
        }

        public void EnterReport(Player p, MsgPack msg) {
            //確認身分
            DataTable result = UpdateGIDGameList(p);

            p.Send(new ActionObject("gamelist", new {
                GID,
                Record = result.toList(),
                Column = result.ColumnNames()
            }));
            //}
        }

        //public void transReport(Player p, MsgPack msg) {
        //    //確認身分
        //    string[] uplevelArr = p["UpLevelTree"].ToString().Split(',');

        //    //if (uplevelArr.Length == 3) {
        //    //    //最頂層 Admin

        //    //} else {
        //    db.ClearParameters();

        //    var cmd = CommandBuilder.Instance;
        //    cmd.addTable("reportcompany", "r").addTable("gameserver", "g");
        //    cmd.SELECT("r.GID", "g.websocket", "g.title", "g.version")
        //        .FROM("r")
        //        .LEFT_JOIN("g", "r.GID=g.gid");

        //    cmd = fixedGID(cmd);

        //    List<string> whereString = new List<string>();

        //    for (int i = 0; i < uplevelArr.Length; i++) {
        //        if (!string.IsNullOrWhiteSpace(uplevelArr[i]) && uplevelArr[i] != "0") {
        //            whereString.Add("UID=@" + "company" + i);
        //            db.TryAddParameter("company" + i, uplevelArr[i]);
        //        }
        //    }
        //    if (whereString.Count > 0)
        //        cmd.WHERE_condition(string.Join(" OR ", whereString));
        //    cmd.GROUPBY("r.GID");

        //    var result = db.ExecuteDataTable(cmd.Result);

        //    cmd.Free();

        //    /*update dic gamelist*/
        //    for (int i = 0; i < result.Rows.Count; i++) {
        //        if (gameList.ContainsKey(result.Rows[i]["gid"].ToString()))
        //            continue;
        //        gameList.Add(result.Rows[i]["gid"].ToString(), result.Rows[i]);
        //    }

        //    p.Send(new ActionObject("transReportQue", new {
        //        GID,
        //        Record = result.toList(),
        //        Column = result.ColumnNames()
        //    }));
        //    //}
        //}

        public CommandBuilder fixedGID(CommandBuilder cmd) {
            if (System.Web.Configuration.WebConfigurationManager.AppSettings["FixedGID"] != null) {
                cmd.WHERE_condition("r.gid=@fixedgid");
                db.TryAddParameter("fixedgid", System.Web.Configuration.WebConfigurationManager.AppSettings["FixedGID"]);
            }
            return cmd;
        }

        public DataTable UpdateGIDGameList(Player p) {
            string[] uplevelArr = p["UpLevelTree"].ToString().Split(',');

            //if (uplevelArr.Length == 3) {
            //    //最頂層 Admin

            //} else {
            db.ClearParameters();

            var cmd = CommandBuilder.Instance;
            cmd.addTable("reportcompany", "r").addTable("gameserver", "g");
            cmd.SELECT("r.GID", "g.websocket", "g.title", "g.version")
                .FROM("r")
                .LEFT_JOIN("g", "r.GID=g.gid");

            cmd = fixedGID(cmd);

            List<string> whereString = new List<string>();

            for (int i = 0; i < uplevelArr.Length; i++) {
                if (!string.IsNullOrWhiteSpace(uplevelArr[i]) && uplevelArr[i] != "0") {
                    whereString.Add("UID=@" + "company" + i);
                    db.TryAddParameter("company" + i, uplevelArr[i]);
                }
            }
            if (whereString.Count > 0)
                cmd.WHERE_condition(string.Join(" OR ", whereString));
            cmd.GROUPBY("r.GID");

            var result = db.ExecuteDataTable(cmd.Result);

            cmd.Free();

            /*update dic gamelist*/
            for (int i = 0; i < result.Rows.Count; i++) {
                if (gameList.ContainsKey(result.Rows[i]["gid"].ToString()))
                    continue;
                gameList.Add(result.Rows[i]["gid"].ToString(), result.Rows[i]);
            }

            return result;
        }

        public void GetUserData(Player p, MsgPack msg) {
            if (!msg.Exist("Mode"))
                return;

            int Mode = msg.Get<int>("Mode");
            if (Mode == (int)Modes.ADD) {
                /*抓代理商水錢*/
                double refund = SQLAction.GetRefund(db, Rule_Shared.GetFixedGID(), p.ID);
                p.Send(new ActionObject("request.userdata_refund", new {
                    GID,
                    refund
                }));
                return;
            }


            if (msg.Exist("UserID")) {
                int UID = msg.Get<int>("UserID");

                if (!SQLAction.CheckShopPermission(db, p.ID, UID)) {
                    return;
                }

                db.ClearParameters();

                var cmd = CommandBuilder.Instance;
                cmd.addTable("userinfo", "u").addTable("gamefee", "f");
                cmd.SELECT("u.NickName AS Name",
                    "u.ShopName AS ShopName",
                    "u.ShopAddress",
                    "u.ShopPhone",
                    "u.BankAccount",
                    "u.KeyString",
                    "f.Refund",
                    "f.ShopBonus",
                    "f.MoneyLimit",
                    "u.canLoginIP AS IP",
                    "u.Email",
                    "u.canSeeNextLevel AS CanSeeNext",
                    "u.Currency",
                    "u.CreditMax")
                    .FROM("u")
                    .LEFT_JOIN("f", "f.UID=u.uid");

                cmd.WHERE_params("u.UID");
                db.TryAddParameter("UID", UID);

                var result = db.ExecuteDataTable(cmd.Result);
                cmd.Free();

                if (result.Rows.Count > 0) {
                    for (int i = 0; i < result.Rows.Count; i++) {
                        result.Rows[i]["Refund"] = Convert.ToDouble(result.Rows[i]["Refund"]);
                    }
                }

                p.Send(new ActionObject("request.userdata", new {
                    GID,
                    Record = result.toList(),
                    Column = result.ColumnNames()
                }));
            }
        }

        public void AddEditData(Player p, MsgPack msg) {
            int Mode = msg.Get<int>("Mode");
            int EditShopID = msg.Get<int>("EditShopID");
            bool success = false;
            int Role = msg.Get<int>("Role");

            if (!Member.isCompanyPermission(p)) {
                SendMsg(p, "edit.error", "無權新增/修改");
                return;
            }

            if (!SQLAction.CheckShopPermission(db, p.ID, EditShopID) || EditShopID == 0) {
                SendMsg(p, "edit.error", "無權新增修改此公司");
                return;
            }

            if ((p["canAddNextLevel"].ToString() == "0" || ((int)p["Level"] >= Controller.MaxLevel - 1) && Mode == (int)Modes.ADD) && Role != 0) {
                SendMsg(p, "edit.error", "無權新增下線");
                return;
            }

            //"公司名稱","姓名","分紅%數","IP","BankAccount","Acc","Pa"
            object[] input = msg.Get<object[]>("Input");
            object[] column = msg.Get<object[]>("Column");

            if (!IsDouble(input[1].ToString()) && Role != 0) {
                SendMsg(p, "edit.error", column[1].ToString() + "型態錯誤");
                return;
            }

            if (!IsDouble(input[2].ToString()) && Role != 0) {
                SendMsg(p, "edit.error", column[2].ToString() + "型態錯誤");
                return;
            }

            if (!Islong(input[3].ToString())) {
                SendMsg(p, "edit.error", column[3].ToString() + "型態錯誤");
                return;
            }

            if (!Islong(input[4].ToString())) {
                SendMsg(p, "edit.error", column[4].ToString() + "型態錯誤");
                return;
            }

            if (Mode == (int)Modes.ADD) {
                success = InsertShop(db, p, input, Role, p.ID);
            } else if (Mode == (int)Modes.EDIT) {
                success = UpdateShop(db, p, input, Role, EditShopID);
            }

            if (success) {
                p.Send(new ActionObject("edit.success", new { success }));
            }

        }

        public void DeleteDate(Player p, MsgPack msg) {
            int ShopID = msg.Get<int>("ShopID");
            bool success = false;
            //if (!Member.isCompanyPermission(p) || !SQLAction.CheckShopPermission(db, p.ID, ShopID) || ShopID == 0) {
            //    p.Send(new ActionObject("delete.success", new { success, message = "無權限修改此公司" }));
            //    return;
            //}

            if (!Member.isAdmin(p["UpLevelTree"].ToString()) || ShopID == 0) {
                p.Send(new ActionObject("delete.success", new { success, message = "無權限修改此公司" }));
                return;
            }

            Player deleteShopInfo = new Player();
            if (Member.GetPlayerData(db, ShopID, deleteShopInfo)) {
                if (Member.isCompanyPermission(deleteShopInfo)) {
                    /*代理商,check has member*/
                    if (Member.HasMember(db, deleteShopInfo)) {
                        p.Send(new ActionObject("delete.success", new { success, message = "此代理商尚有會員!" }));
                        return;
                    }
                } else if (Member.isMember(deleteShopInfo)) {
                    /*is Member, check checkout*/
                    DataTable dt = Rule_CheckOut.GetUserCheckOut(db, SQLAction.GetUpLevelIdTree(db, ShopID.ToString()), deleteShopInfo.ID);

                    dt = Rule_CheckOut.GetAllUserCheckOutListRemoveZero(dt);
                    if (dt.Rows.Count > 0) {
                        p.Send(new ActionObject("delete.success", new { success, message = "此會員尚未結帳!" }));
                        return;
                    }
                }

                success = DeleteShop(db, deleteShopInfo, Convert.ToInt64(deleteShopInfo["CreditMax"]));

                p.Send(new ActionObject("delete.success", new { success }));
            } else
                p.Send(new ActionObject("delete.success", new { success, message = "查無資料!" }));
        }

        #region Insert/Update/Del Shop
        public bool InsertShop(IDataBase db, Player p, object[] input, int Role, int EditShopID) {
            DataTable dt = SQLAction.GetUpLevelIdTree(db, EditShopID.ToString());
            if (dt.Rows.Count == 0) {
                return false;
            }

            string name = checkstrreturn(input[0].ToString());
            double shopbonus = Role == 0 ? 0 : Convert.ToDouble(input[1]);
            double refund = Role == 0 ? 0 : Convert.ToDouble(input[2]);
            long creditmax = Convert.ToInt64(input[3]);
            long moneylimit = Convert.ToInt64(input[4]);
            //string address = checkstrreturn(input[3].ToString());
            //string phone = checkstrreturn(input[4].ToString());
            string ip = checkstrreturn(input[5].ToString());
            //string email = checkstrreturn(input[6].ToString());
            string bankaccount = checkstrreturn(input[6].ToString());
            string acc = checkstrreturn(input[7].ToString());
            string pa = checkstrreturn(input[8].ToString());
            string canSeeNext = "1";
            string Currency = "1";

            DataTable bonusInfo = SQLAction.GetBonusInfo(db, Rule_Shared.GetFixedGID(), p.ID);

            if (bonusInfo.Rows.Count == 0) {
                SendMsg(p, "edit.error", "資料錯誤!");
                return false;
            }

            if (Role == 0) {
                /*設定會員,不能看下線*/
                canSeeNext = "0";

                shopbonus = Convert.ToDouble(bonusInfo.Rows[0]["ShopBonus"]);
            }

            if (!Member.CheckNickName(db, name, 0)) {
                SendMsg(p, "edit.error", "名稱重複!");
                return false;
            }

            if (!Member.CheckKeyString(db, acc, 0)) {
                SendMsg(p, "edit.error", "帳號重複!");
                return false;
            }

            if (refund / 100 > 1) {
                SendMsg(p, "edit.error", "水錢輸入不可超過100(Refund input must not exceed 100)");
                return false;
            }
            int accLength = 20;
            if (acc.Length > accLength) {
                SendMsg(p, "edit.error", "帳號長度輸入不可超過" + accLength + "(Account input must not exceed " + accLength + ")");
                return false;
            }

            #region check bonus

            string GID = System.Web.Configuration.WebConfigurationManager.AppSettings["FixedGID"];

            bool isAdmin = false;

            double sumBonus = 0, upLevelBonus = 0;
            if (p.ID != 0 && Role != 0) {
                //sumBonus += SQLAction.GetOtherShopBonus(db, GID, p.ID, 0);
                sumBonus += shopbonus;

                upLevelBonus = SQLAction.GetUpLevelShopBonus(db, GID, p.ID);
                if (upLevelBonus == -1) {
                    //admin
                    isAdmin = true;
                } else if (sumBonus > upLevelBonus) {
                    SendMsg(p, "edit.error", "超過上線公司分紅%數，請重新選擇。");
                    return false;
                }
            }

            //檢查水錢
            sumBonus = 0;
            upLevelBonus = 0;
            if (p.ID != 0) {
                //sumBonus += SQLAction.GetOtherRefund(db, GID, p.ID, 0);
                sumBonus += refund;

                upLevelBonus = SQLAction.GetRefund(db, GID, p.ID);
                if (sumBonus > upLevelBonus) {
                    SendMsg(p, "edit.error", "超過上線公司水錢%數，請重新選擇。");
                    return false;
                }
            }
            #endregion
            /*Update 信用額度*/
            string reason;
            if (!UpdateCreditMax(db, p, creditmax, EditShopID, 0, out reason)) {
                SendMsg(p, "edit.error", reason);
                return false;
            }

            string UpLevelTree = dt.Rows[0]["UpLevelTree"].ToString() + EditShopID + ",";

            string Code = Tool.Random.Generator(6);

            while (SQLAction.seautcode(db, Code))//判斷亂數是否重複
            {
                Code = Tool.Random.Generator(6);
                if (!SQLAction.seautcode(db, Code)) {
                    break;
                }
            }

            #region 負責人新增
            db.ClearParameters();

            db.TryAddParameter("Place", name);
            db.TryAddParameter("ShopType", "2");
            string strQuery = "insert into uni_db.sequenceplace (ShopType,Place) ";
            strQuery += "values (@ShopType,@Place);";

            db.TryAddParameter("UpLevelId", EditShopID);
            db.TryAddParameter("ShopName", name);
            //db.TryAddParameter("ShopAdd", address);
            //db.TryAddParameter("ShopTel", phone);
            db.TryAddParameter("EName", name);
            db.TryAddParameter("Account", acc);
            db.TryAddParameter("Password", pa);
            //db.TryAddParameter("Email", email);
            db.TryAddParameter("BankAccount", bankaccount);
            db.TryAddParameter("UpLevelTree", UpLevelTree);
            db.TryAddParameter("Code", Code);
            db.TryAddParameter("canAddNextLevel", Role);
            db.TryAddParameter("canLoginIP", ip);
            db.TryAddParameter("canSeeNextLevel", canSeeNext);
            db.TryAddParameter("Currency", Currency);
            db.TryAddParameter("upleveluid", p.ID);
            db.TryAddParameter("creditmax", creditmax);

            strQuery += "insert into userinfo (ShopID,Phone,KeyString,Check_Key,Name,NickName,BankAccount,Competence,aut,Resignation,ShopName,UpLevelId,UpLevelTree,Code,canAddNextLevel,canLoginIP,canSeeNextLevel,Currency,IsVerify,CreditMax,CreditRemain) ";

            strQuery += " values ('0',@Account,@Account,@Password,@EName,@EName,@BankAccount,'0','1','0',@ShopName,@UpLevelId,@UpLevelTree,@Code,@canAddNextLevel,@canLoginIP";

            if (p["canSeeNextLevel"].ToString() == "1") {
                strQuery += " ,@canSeeNextLevel ";
            } else
                strQuery += " ,0 ";

            strQuery += ",@Currency,1,@creditmax,@creditmax);SELECT LAST_INSERT_ID();";

            string UID = db.ExecuteScalar(strQuery).ToString();

            db.TryAddParameter("GID", GID);
            db.TryAddParameter("Refund", refund);
            db.TryAddParameter("ShopBonus", shopbonus);
            db.TryAddParameter("uid", UID);
            db.TryAddParameter("MoneyLimit", moneylimit);

            strQuery = "insert into gamefee (UID,GID,ShopBonus,Refund,MoneyLimit) ";
            strQuery += "values (@uid,@GID,@ShopBonus,@Refund,@MoneyLimit);";
            strQuery += "insert into refund_close(uid,gid,time,refund) ";
            strQuery += "values (@uid,@GID,now(),0)";

            #endregion

            db.Execute(strQuery);

            //if (Role != 0) {
            //    if (!ShowData_Permission.isAdmin(p)) {
            //        db.ClearParameters();
            //        db.TryAddParameter("ShopBonus", shopbonus);
            //        db.TryAddParameter("GID", GID);
            //        db.TryAddParameter("UID", p.ID);
            //        strQuery = "UPDATE gamefee SET ShopBonus=ShopBonus-@ShopBonus WHERE UID=@UID AND GID=@GID;";
            //        db.Execute(strQuery);
            //    }
            //}

            return true;
        }

        public bool UpdateShop(IDataBase db, Player p, object[] input, int Role, int EditShopID) {
            string name = checkstrreturn(input[0].ToString());
            double shopbonus = Role == 0 ? 0 : Convert.ToDouble(input[1]);
            double refund = Role == 0 ? 0 : Convert.ToDouble(input[2]);
            long creditmax = Convert.ToInt64(input[3]);
            long moneylimit = Convert.ToInt64(input[4]);
            //string address = checkstrreturn(input[3].ToString());
            //string phone = checkstrreturn(input[4].ToString());
            string ip = checkstrreturn(input[5].ToString());
            //string email = checkstrreturn(input[6].ToString());
            string bankaccount = checkstrreturn(input[6].ToString());
            /*account can't edit*/
            //string acc = checkstrreturn(input[6].ToString());
            string pa = checkstrreturn(input[8].ToString());
            string canSeeNext = "1";
            string Currency = "1";

            string GID = System.Web.Configuration.WebConfigurationManager.AppSettings["FixedGID"];

            if (!Member.CheckNickName(db, name, EditShopID)) {
                SendMsg(p, "edit.error", "名稱重複!");
                return false;
            }

            //if (!Member.CheckKeyString(db, acc, EditShopID)) {
            //    SendError(p, "edit.error", "帳號重複!");
            //    return false;
            //}

            if (refund / 100 > 1) {
                SendMsg(p, "edit.error", "水錢輸入不可超過100(Refund input must not exceed 100)");
                return false;
            }

            DataTable dt = SQLAction.GetUpLevelIdTree(db, EditShopID.ToString());
            if (dt.Rows.Count > 0) {
                DataTable bonusInfo = SQLAction.GetBonusInfo(db, Rule_Shared.GetFixedGID(), p.ID);

                if (bonusInfo.Rows.Count == 0) {
                    SendMsg(p, "edit.error", "資料錯誤!");
                    return false;
                }

                if (dt.Rows[0]["canAddNextLevel"].ToString() == "0") {
                    /*設定會員,不能看下線*/
                    canSeeNext = "0";

                    shopbonus = Convert.ToDouble(bonusInfo.Rows[0]["ShopBonus"]);
                }

                #region check bonus
                int UpLevelId = Convert.ToInt32(dt.Rows[0]["UpLevelId"]);
                double sumBonus = 0;
                double upLevelBonus = 0;
                bool isAdmin = false;

                //if (Role != 0) {
                //    double uplevelbonus = Convert.ToDouble(bonusInfo.Rows[0]["ShopBonus"]);
                //    if (shopbonus > uplevelbonus) {
                //        SendMsg(p, "edit.error", "超過上線公司分紅%數，請重新選擇。");
                //        return false;
                //    }
                //}

                if (UpLevelId != 0 && dt.Rows[0]["canAddNextLevel"].ToString() != "0") {
                    //sumBonus += SQLAction.GetOtherShopBonus(db, GID, UpLevelId, EditShopID);
                    sumBonus += shopbonus;

                    upLevelBonus = SQLAction.GetUpLevelShopBonus(db, GID, UpLevelId);
                    if (upLevelBonus == -1) {
                        isAdmin = true;
                    } else if (sumBonus > upLevelBonus) {
                        SendMsg(p, "edit.error", "超過上線公司分紅%數，請重新選擇。");
                        return false;
                    }
                }

                if (dt.Rows[0]["canAddNextLevel"].ToString() != "0") {
                    //檢查下線公司紅利
                    sumBonus = 0;
                    upLevelBonus = 0;
                    sumBonus += SQLAction.GetMaxShopBonus(db, GID, EditShopID, 0);
                    upLevelBonus = shopbonus;
                    if (sumBonus > upLevelBonus) {
                        SendMsg(p, "edit.error", "少於下線公司分紅%數，請重新選擇。");
                        return false;
                    }
                }

                //檢查水錢
                sumBonus = 0;
                upLevelBonus = 0;
                if (UpLevelId != 0) {
                    //sumBonus += SQLAction.GetOtherRefund(db, GID, UpLevelId, 0);
                    sumBonus += refund;

                    upLevelBonus = SQLAction.GetRefund(db, GID, UpLevelId);
                    if (sumBonus > upLevelBonus) {
                        SendMsg(p, "edit.error", "超過上線公司水錢%數，請重新選擇。");
                        return false;
                    }
                }
                if (dt.Rows[0]["canAddNextLevel"].ToString() != "0") {
                    //檢查下線公司水錢
                    sumBonus = 0;
                    upLevelBonus = 0;
                    sumBonus += SQLAction.GetMaxRefund(db, GID, EditShopID, 0);
                    upLevelBonus = refund;
                    if (sumBonus > upLevelBonus) {
                        SendMsg(p, "edit.error", "少於下線公司水錢%數，請重新選擇。");
                        return false;
                    }
                }
                #endregion

                /*Update 信用額度*/

                DataTable creditinfo = SQLAction.GetUserCreditInfo(db, EditShopID);
                long org_creditmax = Convert.ToInt64(creditinfo.Rows[0]["CreditMax"]);
                creditmax -= org_creditmax;
                string reason;
                if (!UpdateCreditMax(db, p, creditmax, EditShopID, 1, out reason)) {
                    SendMsg(p, "edit.error", reason);
                    return false;
                }

                db.ClearParameters();
                db.TryAddParameter("ShopID", EditShopID);
                db.TryAddParameter("ShopName", name);
                db.TryAddParameter("ShopIP", ip);
                db.TryAddParameter("ShopBonus", shopbonus);
                db.TryAddParameter("GID", GID);
                db.TryAddParameter("Name", name);
                db.TryAddParameter("BankAccount", bankaccount);
                //db.TryAddParameter("acc", acc);
                db.TryAddParameter("pa", pa);
                db.TryAddParameter("canSeeNextLevel", canSeeNext);
                db.TryAddParameter("Currency", Currency);
                db.TryAddParameter("upleveluid", p.ID);
                db.TryAddParameter("Refund", refund);
                db.TryAddParameter("MoneyLimit", moneylimit);

                string strQuery = @"UPDATE userinfo SET ShopName=@ShopName ";
                strQuery += " ,NickName=@Name,Name=@Name,canLoginIP=@ShopIP,BankAccount=@BankAccount ";
                if (pa != "") {
                    strQuery += " ,Check_Key=@pa";
                }
                strQuery += " ,Currency=@Currency ";

                if (p["canSeeNextLevel"].ToString() == "1") {
                    strQuery += " ,canSeeNextLevel=@canSeeNextLevel ";
                }

                strQuery += " WHERE UID=@ShopID;";
                strQuery += " UPDATE gamefee SET ShopBonus=@ShopBonus,Refund=@Refund,MoneyLimit=@MoneyLimit WHERE UID=@ShopID AND GID=@GID;";

                db.Execute(strQuery);

                if (dt.Rows[0]["canAddNextLevel"].ToString() != "0") {

                    /*Company: Update Member*/
                    DataTable member = SQLAction.GetCompanyMember(db, EditShopID);

                    if (member.Rows.Count == 0) {
                        return true;
                    }

                    db.ClearParameters();
                    db.TryAddParameter("ShopBonus", shopbonus);
                    db.TryAddParameter("GID", GID);
                    strQuery = "";

                    for (int i = 0; i < member.Rows.Count; i++) {
                        strQuery += "UPDATE gamefee SET ShopBonus=@ShopBonus WHERE UID=@UID" + i + " AND GID=@GID;";
                        db.TryAddParameter("UID" + i, member.Rows[i]["UID"].ToString());
                    }

                    if (strQuery.Length > 0) {
                        db.Execute(strQuery);
                    }

                }

                return true;
            }

            return false;
        }

        public bool UpdateCreditMax(IDataBase db, Player LoginPlayer, long price, int targetuid, int AddEditType, out string reason) {
            //int uid = 0;
            bool canWashCash = false, isSuccess = false, isBanUser = false, isBanComp = false;
            //long washPrice = 0, assignCredit = 0, ownCash = 0;
            //string sqlcmd = "", CanAddNext = "0";
            reason = "";

            Player targetPlayer = new Player();
            if (!Member.GetPlayerData(db, targetuid, targetPlayer) && AddEditType == 1) {
                reason = "查無玩家";
            } else if (AddEditType == 0) {
                /*Add*/
                if (Rule_Shared.IsValid_CashCredit(LoginPlayer, LoginPlayer, price.ToString(), out reason) &&
                    Rule_SetCredit.IsValidSetCredit(LoginPlayer, price.ToString(), out reason)) {
                    Rule_SetCredit.UpdateCredit(db, LoginPlayer, price);
                    isSuccess = true;
                }
            } else {
                if (Rule_Shared.IsValid_CashCredit(LoginPlayer, targetPlayer, price.ToString(), out reason) &&
                        Rule_SetCredit.IsValidSetCredit(LoginPlayer, targetPlayer, price.ToString(), out reason)) {
                    Rule_SetCredit.Excute(db, LoginPlayer, targetPlayer, price);
                    isSuccess = true;
                }
            }

            //if (islowerlevel(LoginPlayer.ID, targetuid, db, AddEditType, out uid, out canWashCash, out washPrice, out assignCredit, out ownCash, out isBanUser, out isBanComp, out CanAddNext)) {
            //    if (isBanComp || isBanUser)
            //        reason = "已被停權，請和管理員確認!!";

            //    if (Convert.ToInt32(LoginPlayer["UpLevelId"]) != 0)//代表非admin
            //    {
            //        long canAssignPrice = LoginPlayer.originUD >= (long)LoginPlayer["CreditMax"] ? (long)LoginPlayer["CreditMax"] : LoginPlayer.originUD;

            //        if (canAssignPrice < price) {
            //            reason = "可指定額度不足(目前剩餘信用額度:" + LoginPlayer["CreditMax"] + ")，請確認金額";

            //        }

            //        LoginPlayer.originUD -= (price - assignCredit);
            //        LoginPlayer["CreditMax"] = (long)LoginPlayer["CreditMax"] - (price - assignCredit);
            //        sqlcmd = " UPDATE userinfo SET BankCredit=" + LoginPlayer.originUD + ",CreditMax=" + LoginPlayer["CreditMax"] + " WHERE UID=" + LoginPlayer.ID + ";";
            //    }

            //    if (reason == "") {
            //        try {
            //            sqlcmd += " UPDATE userinfo SET CreditMax=" + price + ",BankCredit=" + (price - assignCredit + ownCash) + " WHERE UID=" + uid;
            //            db.Execute(sqlcmd);
            //            isSuccess = true;
            //        } catch (Exception e) {
            //            reason = e.ToString();
            //        }
            //    }

            //} else
            //    reason = "帳號非為本下線代理商或會員!!";

            return isSuccess;
        }

        public bool islowerlevel(int loginAccID, int targetuid, IDataBase sql, int AddEditType, out int uid, out bool canWashCash, out long washPrice, out long assignCredit, out long ownCash, out bool isBanUser, out bool isBanComp, out string CanAddNext) {
            bool flag = false;
            var cmd = CommandBuilder.Instance;
            uid = 0;
            canWashCash = false;
            washPrice = 0;
            assignCredit = 0;
            ownCash = 0;
            isBanUser = false;
            isBanComp = false;
            CanAddNext = "0";

            sql.ClearParameters();
            sql.TryAddParameter("uid", targetuid);
            cmd.addTable("userinfo", "u");
            cmd.SELECT("UpLevelId", "UID", "BankCredit", "CreditMax",
                "betStatus", "CompanyBan", "canAddNextLevel")
                .FROM("u")
                .WHERE_condition("u.UID=@uid");

            DataTable dt = sql.ExecuteDataTable(cmd.Result);

            if (dt.Rows.Count > 0) {
                flag = Convert.ToInt32(dt.Rows[0]["UplevelId"].ToString()) == loginAccID || AddEditType == 0 ? true : false;
                uid = Convert.ToInt32(dt.Rows[0]["UID"].ToString());
                assignCredit = Convert.ToInt64(dt.Rows[0]["CreditMax"].ToString());
                ownCash = Convert.ToInt64(dt.Rows[0]["BankCredit"].ToString());
                canWashCash = ownCash > assignCredit ? true : false;
                washPrice = ownCash - assignCredit;
                isBanUser = Ban.betStatus(dt.Rows[0]);
                isBanComp = Ban.CompanyBan(dt.Rows[0]);
                CanAddNext = dt.Rows[0]["canAddNextLevel"].ToString();
            }

            cmd.Free();

            return flag;
        }

        public bool DeleteShop(IDataBase db, Player p, long creditMax) {
            if (p.ID == 0)
                return false;

            db.ClearParameters();

            db.TryAddParameter("UID", p.ID);
            db.TryAddParameter("UpLevelTree", p["UpLevelTree"].ToString() + p.ID + ",%");
            db.TryAddParameter("price", creditMax);
            db.TryAddParameter("UpperID", p["UpLevelId"].ToString());

            string strQuery = "UPDATE userinfo SET State=0 ";
            strQuery += " WHERE UID=@UID OR UpLevelTree LIKE @UpLevelTree;";
            if (p["UpLevelId"].ToString() == "5342") {
                /*admin*/
                strQuery += " UPDATE userinfo SET CreditMax=CreditMax-@price WHERE UID=@UpperID;";
            } else
                strQuery += " UPDATE userinfo SET CreditRemain=CreditRemain+@price WHERE UID=@UpperID;";

            db.Execute(strQuery);

            return true;
        }

        public void AddShareHolder(IDataBase db, Player p, MsgPack msg) {
            string name = msg.Get<string>("name"),
                acc = msg.Get<string>("acc"),
                pa = msg.Get<string>("pa");

            int Mode = msg.Get<int>("Mode"),
                uid = msg.Get<int>("uid");
            string reason = "";

            bool success = false;

            if (!Member.isCompanyPermission(p)) {
                p.Send(new ActionObject("add.shareholder", new {
                    success,
                    reason = "無權新增"
                }));
                return;
            }
            if(Mode == (int)Modes.EDIT)
                if (!SQLAction.CheckShopPermission(db, p.ID, uid) || uid == 0) {
                    p.Send(new ActionObject("add.shareholder", new {
                        success,
                        reason = "無權修改此股東!"
                    }));
                    return;
                }

            if ((p["canAddNextLevel"].ToString() == "0" || ((int)p["Level"] >= Controller.MaxLevel - 1) && Mode == (int)Modes.ADD)) {
                p.Send(new ActionObject("add.shareholder", new {
                    success,
                    reason = "無權新增股東"
                }));
                return;
            }

            if (!Member.CheckNickName(db, name,(Mode == (int)Modes.ADD)? 0:uid)) {
                p.Send(new ActionObject("add.shareholder", new {
                    success,
                    reason = "名稱重複!"
                }));
                return;
            }

            if (Mode == (int)Modes.ADD) {
                success = InsertShareHolder(db, p, acc, pa, name, out reason);
            } else if (Mode == (int)Modes.EDIT) {
                success = UpdateShareHolder(db, p, acc, pa, name, uid, out reason);
            }

            //if (success) {
            p.Send(new ActionObject("add.shareholder", new {
                success,
                reason
            }));
            //}
        }

        public bool InsertShareHolder(IDataBase db, Player p, string acc, string pa, string name, out string reason) {
            reason = "";

            name = checkstrreturn(name);
            pa = checkstrreturn(pa);
            acc = checkstrreturn(acc);

            if (!Member.CheckKeyString(db, acc, 0)) {
                reason = "帳號重複!";
                return false;
            }

            int accLength = 20;
            if (acc.Length > accLength) {
                reason = "帳號長度輸入不可超過" + accLength + "(Account input must not exceed " + accLength + ")!";
                return false;
            }

            string Code = Tool.Random.Generator(6);

            while (SQLAction.seautcode(db, Code))//判斷亂數是否重複
            {
                Code = Tool.Random.Generator(6);
                if (!SQLAction.seautcode(db, Code)) {
                    break;
                }
            }

            #region 負責人新增
            db.ClearParameters();

            db.TryAddParameter("Place", name);
            db.TryAddParameter("ShopType", "2");
            string strQuery = "insert into uni_db.sequenceplace (ShopType,Place) ";
            strQuery += "values (@ShopType,@Place);";

            db.TryAddParameter("UpLevelId", p.ID);
            db.TryAddParameter("ShopName", name);
            //db.TryAddParameter("ShopAdd", address);
            //db.TryAddParameter("ShopTel", phone);
            db.TryAddParameter("EName", name);
            db.TryAddParameter("Account", acc);
            db.TryAddParameter("Password", pa);
            //db.TryAddParameter("Email", email);
            db.TryAddParameter("BankAccount", "");
            db.TryAddParameter("UpLevelTree", p["UpLevelTree"].ToString() + p.ID + ",");
            db.TryAddParameter("Code", Code);
            db.TryAddParameter("canAddNextLevel", 2);
            db.TryAddParameter("canLoginIP", "");
            db.TryAddParameter("canSeeNextLevel", "0");
            db.TryAddParameter("Currency", "1");
            db.TryAddParameter("upleveluid", p.ID);
            db.TryAddParameter("creditmax", 0);

            strQuery += "insert into userinfo (ShopID,Phone,KeyString,Check_Key,Name,NickName,BankAccount,Competence,aut,Resignation,ShopName,UpLevelId,UpLevelTree,Code,canAddNextLevel,canLoginIP,canSeeNextLevel,Currency,IsVerify,CreditMax,CreditRemain) ";

            strQuery += " values ('0',@Account,@Account,@Password,@EName,@EName,@BankAccount,'0','1','0',@ShopName,@UpLevelId,@UpLevelTree,@Code,@canAddNextLevel,@canLoginIP";

            if (p["canSeeNextLevel"].ToString() == "1") {
                strQuery += " ,@canSeeNextLevel ";
            } else
                strQuery += " ,0 ";

            strQuery += ",@Currency,1,@creditmax,@creditmax);SELECT LAST_INSERT_ID();";

            string UID = db.ExecuteScalar(strQuery).ToString();

            db.TryAddParameter("GID", Rule_Shared.GetFixedGID());
            db.TryAddParameter("Refund", 0);
            db.TryAddParameter("ShopBonus", 0);
            db.TryAddParameter("uid", UID);

            strQuery = "insert into gamefee (UID,GID,ShopBonus,Refund) ";
            strQuery += "values (@uid,@GID,@ShopBonus,@Refund);";
            strQuery += "insert into refund_close(uid,gid,time,refund) ";
            strQuery += "values (@uid,@GID,now(),0)";

            #endregion

            db.Execute(strQuery);

            //if (Role != 0) {
            //    if (!ShowData_Permission.isAdmin(p)) {
            //        db.ClearParameters();
            //        db.TryAddParameter("ShopBonus", shopbonus);
            //        db.TryAddParameter("GID", GID);
            //        db.TryAddParameter("UID", p.ID);
            //        strQuery = "UPDATE gamefee SET ShopBonus=ShopBonus-@ShopBonus WHERE UID=@UID AND GID=@GID;";
            //        db.Execute(strQuery);
            //    }
            //}

            return true;
        }

        public bool UpdateShareHolder(IDataBase db, Player p, string acc, string pa, string name, int uid, out string reason) {
            reason = "";

            name = checkstrreturn(name);
            pa = checkstrreturn(pa);
            acc = checkstrreturn(acc);

            if (!Member.CheckKeyString(db, acc, uid)) {
                reason = "帳號重複!";
                return false;
            }

            int accLength = 20;
            if (acc.Length > accLength) {
                reason = "帳號長度輸入不可超過" + accLength + "(Account input must not exceed " + accLength + ")!";
                return false;
            }


            #region 負責人Update
            db.ClearParameters();

            db.TryAddParameter("EName", name);
            db.TryAddParameter("Account", acc);
            db.TryAddParameter("pa", pa);       
            db.TryAddParameter("UID", uid);

            string strQuery = "UPDATE userinfo SET KeyString = @Account,Name=@EName,NickName=@EName,Check_Key=@pa ";
            strQuery += " WHERE UID=@UID AND canAddNextLevel=2 ";

            db.Execute(strQuery);

            #endregion


            return true;
        }

        #endregion

        #region QRCode
        public void QRCreate(Player p, MsgPack msg) {
            if (msg.Exist("count")) {
                int count = msg.Get<int>("count");

                if (!Member.isCompanyPermission(p)) {
                    return;
                }

                GenerateQR(count);

                GetQRList(p, msg, false);
            }

        }

        public void QRDelete(Player p, MsgPack msg) {
            if (msg.Exist("qrcode")) {
                if (!Member.isCompanyPermission(p)) {
                    return;
                }

                string qrcode = msg.Get<string>("qrcode");

                SQLAction.DeleteInviteCode(db, qrcode);

                GetQRList(p, msg, false);
            }

        }

        public int QRWidth = 180;
        public int QRHeight = 180;


        public List<string> GenerateQR(int count) {
            List<string> list = new List<string>();
            string inviteCode, filePath = @"\QRCode\", content = System.Web.Configuration.WebConfigurationManager.AppSettings["manageServer"] + "/NewMember.aspx?code=";

            int len = 15, exist = 5, while_count = 0;

            bool isValid = true;

            for (int i = 0; i < count; i++) {
                inviteCode = Tool.Random.Generator(len);
                while_count = 0;
                isValid = true;
                while (SQLAction.checkExistInviteCode(db, inviteCode) && while_count < exist) {
                    inviteCode = Tool.Random.Generator(len);
                    while_count += 1;

                    isValid = false;
                }

                if (QRCode.Generate(content + inviteCode, inviteCode) && isValid) {
                    SQLAction.insertInviteCode(db, inviteCode, filePath + inviteCode + ".PNG");
                    list.Add(inviteCode);
                }

            }

            return list;
        }

        public void GetQRList(Player p, MsgPack msg, bool isGetAllForPrint) {
            if (!Member.isCompanyPermission(p)) {
                return;
            }

            var result = SQLAction.getQRList(db, msg, isGetAllForPrint);

            if (!isGetAllForPrint)
                p.Send("request.qrlist", new {
                    Record = result.toList(),
                    Column = result.ColumnNames(),
                    DataCount = SQLAction.allDataCount
                });
            else
                p.Send("request.printqrlist", new {
                    Record = result.toList(),
                    Column = result.ColumnNames(),
                    DataCount = SQLAction.allDataCount
                });
        }

        public void GetQRUserList(Player p, MsgPack msg) {
            if (!Member.isCompanyPermission(p)) {
                return;
            }

            var result = SQLAction.getQRUserList(db, p, msg);

            p.Send("request.qruserlist", new {
                Record = result.toList(),
                Column = result.ColumnNames()
            });
        }

        public void SetQRUser(Player p, MsgPack msg) {
            if (!Member.isCompanyPermission(p)) {
                return;
            }

            Dictionary<string, object> data = msg.Get<Dictionary<string, object>>("data");
            string errorMsg = "";
            int id;
            try {
                id = data.Get<int>("id");
            } catch (Exception ex) {
                id = 0;
            }

            string qrcode = msg.Get<string>("qrcode");

            if (!SQLAction.CheckQRExist(db, qrcode)) {
                errorMsg = "查無此QRCode.";
            }

            SQLAction.SetQRUser(db, qrcode, id);

            p.Send("set.qruser", new {
                msg = errorMsg
            });
        }

        #endregion

        #region 細部資料
        
        #region GameProxy
        
        void ReceiveTotal(Player thisplayer, DataTable lowerList, int Type, Player targetplayer, string GID, DataTable Record) {
            string SearchMember = "";
            
            DataTable dt_top;
            DataTable dt = ConvertArrayToDataTable_Total(Record, lowerList, targetplayer, thisplayer, SearchMember, out dt_top), dt2 = new DataTable();

            switch (Type) {
                case 1:
                case 2:
                    Operation.Flow.Main(dt, lowerList, targetplayer, Type, DataStruct.DataType.GAMELIST, out dt2, out dt);
                    break;
                case 0:
                default:
                    Operation.Flow.Sub(dt, lowerList, targetplayer, Type, thisplayer, SearchMember, DataStruct.DataType.GAMELIST, out dt);
                    break;
            }
            DataRow dr;
            gameList.TryGetValue(GID, out dr);

            thisplayer.Send(new ActionObject("reporter.Total", new {
                gameID = GID,
                gameTitle = dr["title"],
                pName = targetplayer["title"] + " " + targetplayer.Name,
                pID = targetplayer.ID,
                Column = dt.ColumnNames(),
                Record = dt.toList(),
                Column_User = dt2.ColumnNames(),
                Record_User = dt2.toList(),
                Currency = targetplayer["Currency"],
                dt_top = dt_top.toList()
            }));
        }
                
        public void callToGameGetOnlineRecord(Player thisplayer) {
            string GID = Rule_Shared.GetFixedGID();

            IGameProxy g_ws;
            DataRow dr;
            string[] actType = new string[] { "500", "2" };

            UpdateGIDGameList(thisplayer);

            if (gameList.TryGetValue(GID, out dr)) {
                g_ws = getPlayerGameProxy(thisplayer, GID, dr);

                g_ws.StoreMsg.Add("acttype", actType.Join("."));
                g_ws.StoreMsg.Add("GID", GID);
                g_ws.StoreMsg.Add("player", thisplayer);
                g_ws.Connected();

                var sendObj = new { pID = thisplayer.ID };

                if (dr["version"].ToString() == "1")
                    g_ws.Send(actType.Join("."), sendObj);
                else
                    g_ws.Send(actType[0], actType[1], sendObj);
            } else {
                sendGameError("Not Find GID!", thisplayer);
            }
        }

        IGameProxy getPlayerGameProxy(Player p, string GID, DataRow dr) {
            IGameProxy g_ws;
            if (p["IGameProxy"] != null) {
                if (((Dictionary<string, IGameProxy>)(p["IGameProxy"])).TryGetValue(GID, out g_ws)) {
                    g_ws.StoreMsg.Clear();
                    return g_ws;
                }
            } else
                p["IGameProxy"] = new Dictionary<string, IGameProxy>();

            g_ws = new IGameProxy(dr["websocket"].ToString());
            //g_ws = new IGameProxy("wss://pk.crazybet.win/webHandler.ashx");
            g_ws.OnMessage += CheckReceive;
            g_ws.OnClose += GameWSClose;
            g_ws.uid = p.ID;

            ((Dictionary<string, IGameProxy>)(p["IGameProxy"])).Add(GID, g_ws);

            return g_ws;
        }

        public void CheckReceive(object sender, MessageEventArgs e) {
            var o = sender as IGameProxy;
            var tmp = jser.Deserialize<MsgPack>(e.Data);

            var msg = o.StoreMsg;
            var p = (Player)msg["player"];

            //var p = WSHandler.Controller.FindPlayer(a => a.ID == o.uid);            
            if (tmp.ContainsKey("Type") && tmp.Get<String>("Type") == "error") {
                sendGameError("Receive Error!", p);
                return;
            }

            var type = msg["acttype"].ToString();

            try {
                DataRow dr;
                gameList.TryGetValue(msg["GID"].ToString(), out dr);
                switch (type) {                    
                    //case "500.1":
                    //    ReceiveGameRecord(p, tmp, msg, dr["version"].ToString());
                    //    break;
                    case "500.2":
                        ReceiveOnline(p, tmp, msg, dr["version"].ToString());
                        break;
                    case "player.out":
                    case "player.in":
                        ReceiveCashIn(p, tmp, msg, dr["version"].ToString());
                        break;
                }
            } catch (Exception ex) {
                if (p != null) {
                    sendGameError("Receive Error!", p);
                }
                Console.WriteLine("Catch QuitErr:" + ex.Message);
            }
            o.StoreMsg.Clear();

        }

        public void GameWSClose(object sender, CloseEventArgs e) {
            var o = sender as IGameProxy;
            try {
                if ((sender as WebSocket).ReadyState != WebSocketState.Closed) {
                    (sender as WebSocket).Close();
                }
                var p = WSHandler.Controller.FindPlayer(a => a.ID == o.uid);

                if (p != null) {
                    sendGameError("On Close!", p);
                }
            } catch (Exception ex) {
                Console.WriteLine("Catch QuitErr:" + ex.Message);
            }
        }

        public override void closeAllGameConnect(Player p) {
            Dictionary<string, IGameProxy> list;
            if (p["IGameProxy"] == null)
                return;

            list = (Dictionary<string, IGameProxy>)(p["IGameProxy"]);
            foreach (var item in list) {
                try {
                    item.Value.Closed();
                } catch (Exception) {
                }
            }
        }

        #endregion

        #region Get Layer1~2

        #region GameList

        void Reporter_Total(IDataBase sql, Player thisPlayer, Dictionary<string, object> ct) {
            int Type = ct.Get<int>("Type");
            int pID = ct.Get<int>("pID");
            Object[] glist = ct.Get<Object[]>("gidlist");
            //string GTitle = ct.Get<string>("gtitle");
            string start = ct.Get<string>("start");
            string end = ct.Get<string>("end");

            string SearchMember = ct.Get<string>("Member");

            Player p = new Player();
            if (Type == 2 && thisPlayer["canAddNextLevel"].ToString() == "2") {
                pID = Convert.ToInt32(thisPlayer["UpLevelId"]);
            }

            Member.GetPlayerData(sql, pID, p);
            p["Language"] = thisPlayer["Language"];
            CompanyTitle.UpdatePlayer(p);

            string[] GID = new string[glist.Length];
            for (int i = 0; i < glist.Length; i++) {
                GID[i] = glist[i].ToString();
            }

            DataTable pLowerList = new DataTable(), dt = new DataTable(), dt2 = new DataTable();

            //取得自己+所有下線總押注總贏分總退水
            pLowerList = getLowerList(sql, p);

            if ((int)thisPlayer["canAddNextLevel"] == 0) {
                Type = 0;
            }
            
            DataRow dr;
            List<string> plist = getUIDList(pLowerList);

            if (GID.Length == 0) {
                sendGameError("GID Length Zero!", thisPlayer);
                return;
            }
            DataTable record;

            for (int i = 0; i < GID.Length; i++) {
                if (gameList.TryGetValue(GID[i], out dr)) {
                    record = GameRecord.GetGameTotal(db, plist, start, end);

                    ReceiveTotal(thisPlayer, pLowerList, Type, p, GID[i], record);
                } else {
                    sendGameError("Not Find GID!", thisPlayer);
                }
            }
        }

        DataTable ConvertArrayToDataTable_Total(DataTable Record, DataTable loweList, Player thisPlayer, Player loginPlayer, string SearchMember, out DataTable dt_top) {
            /*建立Total的DataTable*/
            DataTable dt = DataStruct.NewTable(DataStruct.DataType.GAMELIST);

            dt_top = DataStruct.NewTable(DataStruct.DataType.GAMELIST);

            DataRow r_top;
            DataStruct.SetDefaultVal(DataStruct.DataType.GAMELIST, dt_top, out r_top);
            dt_top.Rows.Add(r_top);

            string[] upleve_tree;
            DataRow perrecord;
            int uid, status, level = Member.GetLevel(thisPlayer), uplevelId;
            long totalBet, totalWin;
            double bonus = 0, bonusrate = 0, agentrefund;
            long Sum = 0, Profit = 0;
            bool isLowerMember;
            DataStruct.CompanyBonusInfo CompanyBonus;
            string bonusTree;

            int arr_len = Controller.MaxLevel - 1;
            double[] bonus_arr;

            int asignLevel;
            Player tmp_p;

            var sql = new MySQL("unidb");

            for (int i = 0; i < Record.Rows.Count; i++) {
                perrecord = Record.Rows[i];
                bonus_arr = new double[arr_len];

                uid = Convert.ToInt32(perrecord["UserID"]);
                DataRow[] foundRows, uplevelRow;
                foundRows = loweList.Select("uid = " + uid);
                if (foundRows.Length == 0)
                    continue;

                totalBet = Convert.ToInt64(perrecord["totalBet"]);
                totalWin = Convert.ToInt64(perrecord["totalWin"]);
                bonusrate = Convert.ToDouble(perrecord["bonusrate"]);

                Sum = Profit = totalBet - totalWin;

                bonus = bonusrate / 100;
                if (totalWin > 0) {
                    Sum = Sum - Convert.ToInt64(perrecord["Refund"]);
                }

                //if (totalWin - totalBet > 0)
                //{
                    /*贏分退代理商水錢:抓上線代理商pa數*/
                    uplevelId = Convert.ToInt32(foundRows[0]["UpLevelId"]);
                    uplevelRow = loweList.Select("uid=" + uplevelId);

                    agentrefund = Convert.ToInt64(perrecord["Refund"]) * (Convert.ToDouble(uplevelRow[0]["Refund"]));
                //}
                //else
                //    agentrefund = 0;

                bonusTree = Rule_Shared.GetBonusTree(db, foundRows[0]["UpLevelTree"].ToString(), loginPlayer);

                status = Ban.getIntStatus(foundRows[0]);

                DataRow r = dt.NewRow();

                //if (Member.isMember(foundRows[0])) {
                //    /*is Member*/
                //    asignLevel = -1;
                //    //asignLevel = arr_len;
                //} else
                upleve_tree = foundRows[0]["UpLevelTree"].ToString().Split(',');

                for (int j = upleve_tree.Length - 1; j >= 0; j--) {
                    if (upleve_tree[j] == "" || upleve_tree[j] == "0")
                        continue;

                    isLowerMember = foundRows[0]["UpLevelId"].ToString() == upleve_tree[j];

                    asignLevel = j - 1;

                    if (isLowerMember)
                        continue;

                    //if (j == 3)
                    //    asignLevel = 1;
                    //else
                    //    asignLevel = j - 2;
                    if (upleve_tree[j].ToString() == thisPlayer.ID.ToString()) {
                        tmp_p = thisPlayer;
                    } else {
                        tmp_p = new Player();
                        Member.GetPlayerData(sql, Convert.ToInt32(upleve_tree[j]), tmp_p);
                    }

                    if (ShowData_Permission.isDelete(tmp_p) || ShowData_Permission.isDelete(foundRows[0]))
                        break;

                    ComputeCompanyBonus(tmp_p, bonusTree, Sum - Convert.ToInt64(agentrefund),
                    isLowerMember, asignLevel, out CompanyBonus);

                    bonus_arr[asignLevel - 1] += Convert.ToDouble(Math.Round(CompanyBonus.CompanyBonus, 0,
                    MidpointRounding.AwayFromZero));
                }

                //asignLevel -= 2;

                //for (int j = asignLevel + 1; j >= 1; j--) {


                //    ComputeCompanyBonus(thisPlayer, bonusTree, Sum,
                //    isLowerMember, j, out CompanyBonus);

                //    bonus_arr[j - 1] += Convert.ToDouble(Math.Round(CompanyBonus.CompanyBonus, 0,
                //    MidpointRounding.AwayFromZero));
                //}

                isLowerMember = foundRows[0]["UpLevelId"].ToString() == thisPlayer.ID.ToString();

                ComputeCompanyBonus(thisPlayer, bonusTree, Sum - Convert.ToInt64(agentrefund),
                    isLowerMember, level, out CompanyBonus);

                DataStruct.SetVal(DataStruct.DataType.GAMELIST, r, new object[]{
                    totalBet,totalWin,perrecord["Refund"],uid,foundRows[0]["Name"],
                    foundRows[0]["ShopBonus"],foundRows[0]["Refund"],bonusrate,foundRows[0]["Currency"],Sum,
                    Math.Round(CompanyBonus.AgentBonus,0,MidpointRounding.AwayFromZero),Math.Round(CompanyBonus.CompanyBonus,0,
                    MidpointRounding.AwayFromZero),1,status,agentrefund, Profit, perrecord["DataCount"],
                    bonus_arr[0],bonus_arr[1],bonus_arr[2],bonus_arr[3]
                });

                dt.Rows.Add(r);

                string[] tmp = bonusTree.Split(',');
                if (!ShowData_Permission.isNormalMember(thisPlayer) && tmp.Length >= level) {
                    CompanyBonus.sum = 0;
                    CompanyBonus.bonusrate = Convert.ToDouble(tmp[level - 1]);

                    for (int j = 0; j < level - 1; j++) {
                        if (tmp[j] != "") {
                            CompanyBonus.sum += Convert.ToDouble(tmp[j]);
                        }
                    }

                    CompanyBonus.companyBonus_rate = CompanyBonus.sum * 1.0;

                    CompanyBonus.companyBonus_rate = CompanyBonus.companyBonus_rate / 100;
                    CompanyBonus.bonus = CompanyBonus.bonusrate / 100;

                    dt_top.Rows[0][(int)Col_G.AgentBonus] = Math.Round(Convert.ToDouble(dt_top.Rows[0][(int)Col_G.AgentBonus]) + ((Sum - Convert.ToInt64(agentrefund)) * 1.0 * CompanyBonus.bonus), 0, MidpointRounding.AwayFromZero);
                    dt_top.Rows[0][(int)Col_G.CompanyBonus] = Math.Round(Convert.ToDouble(dt_top.Rows[0][(int)Col_G.CompanyBonus]) + ((Sum - Convert.ToInt64(agentrefund)) * 1.0 * (CompanyBonus.companyBonus_rate)), 0, MidpointRounding.AwayFromZero);

                    dt_top.Rows[0][(int)Col_G.AgentRefund] = Convert.ToDouble(dt_top.Rows[0][(int)Col_G.AgentRefund]) + agentrefund;
                }
            }


            return dt;
        }

        void Reporter_GameRecord(IDataBase sql, Player thisPlayer, Dictionary<string, object> ct) {
            PageCount = 0;
            int userID = ct.Get<int>("UserID");
            string GID = ct.Get<string>("gid");
            int pageIndex = ct.Get<int>("pageindex");
            string start = ct.Get<string>("start");
            string end = ct.Get<string>("end");

            DataRow dr;
            if (gameList.TryGetValue(GID, out dr)) {
                DataTable Record = GameRecord.GetPlayerRecord(db, userID, pageIndex, start, end, perPageCount);

                thisPlayer.Send(new ActionObject("reporter.GameRecord", new {
                    gameID = GID,
                    gameTitle = dr["title"],
                    Column = Record.ColumnNames(),
                    Record = Record.toList(),
                    pageCount = GameRecord.PageCount
                }));
            } else {
                sendGameError("Not Find GID!", thisPlayer);
            }
        }

        #endregion

        #region Transation
        void Trans_Reporter_Total(IDataBase sql, Player thisplayer, Dictionary<string, object> ct, bool print, int Dic_Path_Type) {
            int Type = ct.Get<int>("Type");
            int pID = ct.Get<int>("pID");
            string start = ct.Get<string>("start");
            string end = ct.Get<string>("end");
            string transType = "";

            if (ct.Exist("transType")) {
                transType = ct.Get<string>("transType");
            }

            string SearchMember = ct.Get<string>("Member");

            Player p = new Player();

            if (Type == 2 && thisplayer["canAddNextLevel"].ToString() == "2") {
                pID = Convert.ToInt32(thisplayer["UpLevelId"]);
            }

            Member.GetPlayerData(sql, pID, p);
            p["Language"] = thisplayer["Language"];
            CompanyTitle.UpdatePlayer(p);

            DataTable pLowerList = new DataTable(), dt = new DataTable(), dt2 = new DataTable();

            //取得自己+所有下線總押注總贏分總退水
            pLowerList = getLowerList(sql, p);

            if ((int)thisplayer["canAddNextLevel"] == 0) {
                Type = 0;
            }

            DataTable dt_top;

            dt = GetDbTrans_Total_Data(sql, pLowerList, p, start, end, transType);
            dt = ConvertToDataTable_Trans(dt, p, thisplayer, SearchMember, out dt_top);
            switch (Type) {
                case 1:
                case 2:
                    //玩家資料
                    Operation.Flow.Main(dt, pLowerList, p, Type, DataStruct.DataType.TRANS, out dt2, out dt);
                    break;
                case 0:
                default:
                    Operation.Flow.Sub(dt, pLowerList, p, Type, thisplayer, SearchMember, DataStruct.DataType.TRANS, out dt);
                    //dt = getUserTotal_Trans(dt, pLowerList, p, Type, thisplayer, SearchMember);
                    break;
            }
            
            if (!print)
                thisplayer.Send(new ActionObject("reporter.Total", new {
                    pName = p["title"] + " " + p.Name,
                    pID = p.ID,
                    Column = dt.ColumnNames(),
                    Record = dt.toList(),
                    Column_User = dt2.ColumnNames(),
                    Record_User = dt2.toList(),
                    Currency = p["Currency"],
                    dt_top = dt_top.toList()
                }));
            else
                PrintPDF(thisplayer, dt, dt_top, dt2, start, end, Dic_Path_Type);
        }

        public DataTable GetDbTrans_Total_Data(IDataBase sql, DataTable list, Player p, string start, string end, string transType) {
            var cmd = CommandBuilder.Instance;
            sql.ClearParameters();
            if ((int)p["canAddNextLevel"] == 0)
                sql.TryAddParameter("uid", p.ID);
            else
                sql.TryAddParameter("upleveltree", p["UpLevelTree"].ToString() + p.ID + ",%");
            cmd.addTable("sequence", "s")
                .addTable("sequenceplace", "p")
                .addTable("sequencemode", "m")
                .addTable("userinfo", "u")
                .addTable("gamefee", "f");
            cmd.SELECT("s.UID As UserID",
                "u.NickName AS Name",
                "u.UpLevelId",
                "u.UpLevelTree",
                "u.canAddNextLevel",
                "u.Currency",
                "s.Price",
                "s.Point AS UD",
                "s.SequenceModeID AS GID",
                "s.CreateDate",
                "p.Place",
                "m.Mode",
                "f.ShopBonus",
                "f.Refund",
                "u.CompanyBan",
                "u.betStatus",
                "s.agentbonus AS AgentBonus",
                "s.companybonus AS CompanyBonus",
                "s.creditmax AS CreditMax",
                "s.bonustree")
                .FROM("s")
                .LEFT_JOIN("u", "u.UID=s.uid")
                .LEFT_JOIN("f", "f.UID=s.uid")
                .LEFT_JOIN("p", "p.SequencePlaceID=s.SequencePlaceID")
                .LEFT_JOIN("m", "m.SequenceModeID=s.SequenceModeID")
                .WHERE_condition("(s.SequenceModeID = 'E' OR s.SequenceModeID = 'F')")
                .WHERE_condition("u.State=1");
            //.GROUPBY("s.uid,s.SequenceModeID");

            if ((int)p["canAddNextLevel"] == 0)
                cmd.WHERE_condition("u.uid =@uid");
            else
                cmd.WHERE_condition("u.UpLevelTree LIKE @upleveltree");

            if (start != "" && end != "") {
                cmd.WHERE_condition("s.CreateDate > @Start AND s.CreateDate <= @End");
                sql.TryAddParameter("Start", start);
                sql.TryAddParameter("End", end);
            }

            if (transType != "") {
                cmd.WHERE_condition("s.SequenceModeID =@transType");
                sql.TryAddParameter("transType", transType);
            }

            DataTable dt = sql.ExecuteDataTable(cmd.Result);
            cmd.Free();

            return dt;
        }

        public void GetDbTrans_Detail_Data(IDataBase sql, Player p, Dictionary<string, object> ct) {
            PageCount = 0;
            int userID = ct.Get<int>("UserID");
            string modeID = ct.Get<string>("ModeID");
            int pageIndex = ct.Get<int>("pageindex");
            string start = ct.Get<string>("start");
            string end = ct.Get<string>("end");
            string Title = ct.Get<string>("Title");

            sql.ClearParameters();
            string query = "SELECT s.UID As UserID,u.NickName AS Name,u.Currency,s.Price AS Price,s.Point AS UD,u.UpLevelId,";
            query += "p.Place,m.Mode,f.ShopBonus,f.Refund,s.CreateDate,s.SequenceModeID AS GID, ";
            query += " s.agentbonus AS AgentBonus,s.companybonus AS CompanyBonus,s.creditmax AS CreditMax,s.bonustree ";
            query += " FROM sequence s ";
            query += " LEFT JOIN userinfo u ON u.UID=s.uid ";
            query += " LEFT JOIN gamefee f ON f.UID=s.uid ";
            query += " LEFT JOIN sequenceplace p ON p.SequencePlaceID=s.SequencePlaceID ";
            query += " LEFT JOIN sequencemode m ON m.SequenceModeID=s.SequenceModeID ";
            query += " WHERE s.uid = @uid AND u.State=1";

            sql.TryAddParameter("uid", userID);

            if (start != "" && end != "") {
                query += " AND s.CreateDate BETWEEN @Start AND @End ";
                sql.TryAddParameter("Start", start);
                sql.TryAddParameter("End", end);
            }

            string orderby = " ORDER BY s.CreateDate DESC ";

            DataTable dt = sql.ExecuteDataTable(query + orderby);

            PageCount = Convert.ToInt32(dt.Rows.Count / perPageCount * 1.0);
            PageCount = dt.Rows.Count % perPageCount == 0 ? PageCount : PageCount += 1;

            string pageQuery = query + orderby;
            if (PageCount == 1 && pageIndex == 1) {
                pageQuery += " limit " + perPageCount;
            } else {
                pageQuery += " limit " + (pageIndex - 1) * perPageCount + "," + perPageCount;
            }
            dt = sql.ExecuteDataTable(pageQuery);

            Player target = new Player();
            Member.GetPlayerData(sql, userID, target);

            DataTable dt_top;
            dt = ConvertToDataTable_Trans(dt, target, p, "", out dt_top);

            p.Send(new ActionObject("reporter.GameRecord", new {
                gameID = "CashCredit",
                gameTitle = Title,
                Column = dt.ColumnNames(),
                Record = dt.toList(),
                pageCount = PageCount
            }));
        }

        DataTable ConvertToDataTable_Trans(DataTable Record, Player p, Player loginplayer, string SearchMember, out DataTable dt_top) {
            /*建立Total的DataTable*/
            DataTable dt = DataStruct.NewTable(DataStruct.DataType.TRANS);

            dt_top = DataStruct.NewTable(DataStruct.DataType.TRANS);

            DataRow r_top;
            DataStruct.SetDefaultVal(DataStruct.DataType.TRANS, dt_top, out r_top);
            dt_top.Rows.Add(r_top);
            DataStruct.CompanyBonusInfo CompanyBonus;

            int uid, status = 0, level = Member.GetLevel(p);
            long Price, credit, cash;
            string Mode = "", bonusTree;
            bool isLowerMember = false;

            int endindex = ShowData_Permission.isAdmin(p) ? level : level - 1;

            for (int i = 0; i < Record.Rows.Count; i++) {
                uid = Convert.ToInt32(Record.Rows[i]["UserID"]);

                bonusTree = Record.Rows[i]["bonustree"].ToString();
                isLowerMember = Record.Rows[i]["UpLevelId"].ToString() == p.ID.ToString();
                Price = Convert.ToInt64(Record.Rows[i]["Price"]);

                //ComputeCompanyBonus(p, bonusTree, Price, isLowerMember, level, out CompanyBonus);

                Mode = Record.Rows[i]["GID"].ToString();

                if (Mode == "F") {
                    /*信用*/
                    cash = 0;
                    credit = Price;
                } else {
                    cash = Price;
                    credit = 0;
                }

                DataRow r = dt.NewRow();
                status = Ban.getIntStatus(Record.Rows[i]);

                DataStruct.SetVal(DataStruct.DataType.TRANS, r, new object[]{
                    uid,Record.Rows[i]["Name"],Record.Rows[i]["Refund"],Price,Record.Rows[i]["UD"],
                    Record.Rows[i]["Place"],Record.Rows[i]["Mode"],0,0,Record.Rows[i]["Currency"],
                    0,0,
                    1,Record.Rows[i]["GID"],cash,credit,Record.Rows[i]["CreateDate"],status,Record.Rows[i]["CreditMax"], Price, 0
                });

                dt.Rows.Add(r);

                string[] tmp = bonusTree.Split(',');
                if (!ShowData_Permission.isNormalMember(p) && tmp.Length >= level) {
                    //CompanyBonus.sum = 0;
                    //CompanyBonus.bonusrate = Convert.ToDouble(tmp[level - 1]);

                    //for (int j = 0; j < level - 1; j++) {
                    //    if (tmp[j] != "") {
                    //        CompanyBonus.sum += Convert.ToDouble(tmp[j]);
                    //    }
                    //}

                    //CompanyBonus.companyBonus_rate = CompanyBonus.sum * 1.0;

                    //CompanyBonus.companyBonus_rate = CompanyBonus.companyBonus_rate / 100;
                    //CompanyBonus.bonus = CompanyBonus.bonusrate / 100;

                    //dt_top.Rows[0][(int)Col_Trans.AgentBonus] = Math.Round(Convert.ToDouble(dt_top.Rows[0][(int)Col_Trans.AgentBonus]) + (Price * 1.0 * CompanyBonus.bonus), 0, MidpointRounding.AwayFromZero);
                    //dt_top.Rows[0][(int)Col_Trans.CompanyBonus] = Math.Round(Convert.ToDouble(dt_top.Rows[0][(int)Col_Trans.CompanyBonus]) + (Price * 1.0 * (CompanyBonus.companyBonus_rate)), 0, MidpointRounding.AwayFromZero);
                    dt_top.Rows[0][(int)Col_Trans.Cash] = Convert.ToDouble(dt_top.Rows[0][(int)Col_Trans.Cash]) + cash;
                    dt_top.Rows[0][(int)Col_Trans.Credit] = Convert.ToDouble(dt_top.Rows[0][(int)Col_Trans.Credit]) + credit;

                }
            }

            return dt;
        }

        #endregion

        public void ComputeCompanyBonus(Player p, string bonusTree, long Price, bool isLowerMember, int level,
            out DataStruct.CompanyBonusInfo Companybonus) {

            Companybonus = new DataStruct.CompanyBonusInfo();

            string[] tmp = bonusTree.Split(',');

            if (isLowerMember && ShowData_Permission.isAdmin(p)) {
                Companybonus.bonusrate = 100;
            }

            if (level >= 4 && tmp.Length > level) {
                Companybonus.bonusrate = Convert.ToDouble(tmp[3]);
            } else {
                if (isLowerMember) {
                    Companybonus.bonusrate = Convert.ToDouble(tmp[level - 1]);
                } else if (tmp.Length > level && tmp[level] != "") {
                    Companybonus.bonusrate = Convert.ToDouble(tmp[level]);
                } else if (ShowData_Permission.isNormalMember(p)) {
                    Companybonus.bonusrate = Convert.ToDouble(tmp[tmp.Length - 1]);
                }
            }

            if (level > 4) {
                for (int j = 0; j < 3; j++) {
                    if (tmp[j] != "") {
                        Companybonus.sum += Convert.ToDouble(tmp[j]);
                    }
                }
            } else if (isLowerMember) {
                for (int j = 0; j < level - 1; j++) {
                    if (tmp[j] != "") {
                        Companybonus.sum += Convert.ToDouble(tmp[j]);
                    }
                }
            } else if (ShowData_Permission.isNormalMember(p)) {
                Companybonus.sum = 0;
                for (int j = 0; j < tmp.Length - 1; j++) {
                    if (tmp[j] != "") {
                        Companybonus.sum += Convert.ToDouble(tmp[j]);
                    }
                }
            } else
                for (int j = 0; j < level; j++) {
                    if (tmp[j] != "") {
                        Companybonus.sum += Convert.ToDouble(tmp[j]);
                    }
                }

            Companybonus.companyBonus_rate = Companybonus.sum * 1.0;

            if (isLowerMember && ShowData_Permission.isAdmin(p)) {
                /*admin lower member*/
                Companybonus.companyBonus_rate = 0;
            }

            Companybonus.companyBonus_rate = Companybonus.companyBonus_rate / 100;
            Companybonus.bonus = Companybonus.bonusrate / 100;

            Companybonus.AgentBonus += Price * 1.0 * Companybonus.bonus;
            Companybonus.CompanyBonus += Price * 1.0 * (Companybonus.companyBonus_rate);

        }
        
        List<string> getUIDList(DataTable lowerList) {
            List<string> result = new List<string>();
            for (int i = 0; i < lowerList.Rows.Count; i++) {
                result.Add(lowerList.Rows[i]["uid"].ToString());
            }

            return result;
        }


        /// <summary>取得玩家所有下線ID</summary>
        DataTable getLowerList(IDataBase sql, Player p) {
            string GID = Rule_Shared.GetFixedGID();
            var cmd = CommandBuilder.Instance;
            sql.ClearParameters();
            sql.TryAddParameter("upleveltree", p["UpLevelTree"].ToString() + p.ID + ",%");
            sql.TryAddParameter("GID", GID);
            cmd.addTable("gamefee", "f")
                .addTable("user_log", "log")
                .addTable("refund_close", "r");
            cmd.SELECT("userinfo.uid",
                "userinfo.NickName AS Name",
                "userinfo.UpLevelId",
                "userinfo.UpLevelTree",
                "userinfo.canAddNextLevel",
                "userinfo.Currency",
                "userinfo.BankCredit",
                "f.ShopBonus",
                "f.Refund",
                "userinfo.CompanyBan",
                "userinfo.betStatus",
                "userinfo.CreditMax",
                "userinfo.CreditRemain",
                "userinfo.State",
                "(SELECT Max(l.time) FROM user_log l WHERE l.uid=userinfo.UID AND l.action=0 AND l.appid=@GID) AS LastLoginTime",
                "(SELECT r.refund FROM refund_close r WHERE r.uid=userinfo.UID AND r.gid=@GID) AS RemainRefund",
                "(SELECT Max(CreateDate) FROM report_login l WHERE l.UID=userinfo.UID) AS ReportLoginTime"
                )
                .FROM("userinfo")
                .LEFT_JOIN("f", "f.UID=userinfo.uid")
                .WHERE_condition("UpLevelTree LIKE @upleveltree")
                .WHERE_condition("State=1")
                .WHERE_condition("userinfo.canAddNextLevel != 2")
                .GROUPBY("userinfo.UID");

            DataTable dt = sql.ExecuteDataTable(cmd.Result);
            cmd.Free();

            //加上自己
            Member.GetPlayerData(sql, p.ID, p);

            DataRow pRow;
            pRow = dt.NewRow();
            pRow["UpLevelId"] = p.ID;
            pRow["uid"] = p.ID;
            pRow["Name"] = p.Name;
            pRow["canAddNextLevel"] = p["canAddNextLevel"];
            pRow["betStatus"] = p["betStatus"];
            pRow["CompanyBan"] = p["CompanyBan"];
            pRow["Currency"] = p["Currency"];
            pRow["BankCredit"] = p.UD;
            pRow["UpLevelTree"] = p["UpLevelTree"];
            pRow["CreditMax"] = p["CreditMax"];
            pRow["CreditRemain"] = p["CreditRemain"];
            pRow["Refund"] = p.Refund;

            dt.Rows.Add(pRow);

            return dt;
        }

        DataTable getLowerList_Stockholder(IDataBase sql, Player p) {
            string GID = Rule_Shared.GetFixedGID();
            var cmd = CommandBuilder.Instance;
            sql.ClearParameters();
            sql.TryAddParameter("upleveltree", p["UpLevelTree"].ToString() + p.ID + ",%");
            sql.TryAddParameter("GID", GID);
            cmd.addTable("gamefee", "f")
                .addTable("user_log", "log")
                .addTable("refund_close", "r");
            cmd.SELECT("userinfo.uid",
                "userinfo.NickName AS Name",
                "userinfo.UpLevelId",
                "userinfo.UpLevelTree",
                "userinfo.canAddNextLevel",
                "userinfo.Currency",
                "userinfo.BankCredit",
                "f.ShopBonus",
                "f.Refund",
                "userinfo.CompanyBan",
                "userinfo.betStatus",
                "userinfo.CreditMax",
                "userinfo.CreditRemain",
                "userinfo.State",
                "(SELECT Max(l.time) FROM user_log l WHERE l.uid=userinfo.UID AND l.action=0 AND l.appid=@GID) AS LastLoginTime",
                "(SELECT r.refund FROM refund_close r WHERE r.uid=userinfo.UID AND r.gid=@GID) AS RemainRefund",
                "(SELECT Max(CreateDate) FROM report_login l WHERE l.UID=userinfo.UID) AS ReportLoginTime"
                )
                .FROM("userinfo")
                .LEFT_JOIN("f", "f.UID=userinfo.uid")
                .WHERE_condition("UpLevelTree LIKE @upleveltree")
                .WHERE_condition("State=1")
                .WHERE_condition("userinfo.canAddNextLevel > 0")
                .GROUPBY("userinfo.UID");

            DataTable dt = sql.ExecuteDataTable(cmd.Result);
            cmd.Free();

            //加上自己
            Member.GetPlayerData(sql, p.ID, p);

            DataRow pRow;
            pRow = dt.NewRow();
            pRow["UpLevelId"] = p.ID;
            pRow["uid"] = p.ID;
            pRow["Name"] = p.Name;
            pRow["canAddNextLevel"] = p["canAddNextLevel"];
            pRow["betStatus"] = p["betStatus"];
            pRow["CompanyBan"] = p["CompanyBan"];
            pRow["Currency"] = p["Currency"];
            pRow["BankCredit"] = p.UD;
            pRow["UpLevelTree"] = p["UpLevelTree"];
            pRow["CreditMax"] = p["CreditMax"];
            pRow["CreditRemain"] = p["CreditRemain"];

            dt.Rows.Add(pRow);

            return dt;
        }
                
        #endregion
        
        void sendGameError(string msg, Player thisPlayer) {
            thisPlayer.Send(new ActionObject("game.error", new {
                Msg = msg
            }));
        }

        #endregion

        public void PrintPDF(Player thisplayer, DataTable record, DataTable top_record, DataTable userMember, string start, string end, int Dic_Path_Type) {
            List<string> colnames = new List<string>();
            DataTable sum;
            string[] title = new string[] {
                "名稱",
                "現金",
                "信用",
                "會員消費",
                "公司紅利<br/>(會員(%)+下線(%))",
                "上線紅利<br/>(會員(%)+下線(%))",
                "貨幣"
            };

            string[] record_column = new string[] {
                "Name", "Cash", "Credit", "MemberCost", "AgentBonus", "CompanyBonus", "Currency"
            };

            string[] sum_title = new string[] {
                "名稱",
                "現金",
                "信用",
                "會員消費",
                "公司紅利<br/>(會員(%)+下線(%))",
                "上線紅利<br/>(會員(%)+下線(%))"
            };
            bool isEN = Languages.GetInt(thisplayer["Language"].ToString()) == Languages.EN;
            if (isEN) {
                title = new string[] {
                    "Name",
                    "Cash",
                    "Credit",
                    "Member Cost",
                    "Company Bonus<br/>(Member(%)+(DownLine(%))",
                    "Upper Bonus<br/>(Member(%)+(DownLine(%))",
                    "Currency"
                };

                sum_title = new string[] {
                    "Name",
                    "Cash",
                    "Credit",
                    "Member Cost",
                    "Company Bonus<br/>(Member(%)+(DownLine(%))",
                    "Upper Bonus<br/>(Member(%)+(DownLine(%))"
                };
            }
            string[] sum_record_column = new string[] {
                "Title", "Cash", "Credit", "MemberCost", "AgentBonus", "CompanyBonus"
            };

            UpdateCurrency(db, false, gameid);

            userMember.Merge(record);

            DataStruct.ComputeSum(DataStruct.DataType.TRANS, userMember, out sum);
            if (sum.Rows.Count > 0) {
                DataColumn dc = new DataColumn("Title", typeof(System.String));
                //dc.DefaultValue = isEN ? "Sum" : "總和";
                dc.DefaultValue = userMember.Rows[0][(int)Col_Trans.Name];
                sum.Columns.Add(dc);

                sum.Rows[0][(int)Col_Trans.AgentBonus] = top_record.Rows[0][(int)Col_Trans.AgentBonus];
                sum.Rows[0][(int)Col_Trans.CompanyBonus] = top_record.Rows[0][(int)Col_Trans.CompanyBonus];

                sum.Rows[0][(int)Col_Trans.MemberCost] = userMember.Rows[0][(int)Col_Trans.MemberCost];
            }

            int margin = 10;

            string result = "<div style='font-size:20px;margin-bottom:" + margin + "px;text-align:center;'>" + (isEN ? "Member Bonus And Game Records" : "會員分紅及遊戲報表系統") + "</div>";
            result += "<div style='margin-bottom:" + margin + "px;'>" + (isEN ? "Duration " : "期間 ") + ":" + start + " ～ " + end + "</div>";
            result += "<div>" + ConvertTableToHTMLString(sum, sum_title.ToList<string>(), sum_record_column, isEN) + "</div>";
            result += "<div>" + ConvertTableToHTMLString(userMember, title.ToList<string>(), record_column, isEN) + "</div>";
            pdfEBookConvert(thisplayer, result, Dic_Path_Type);
        }

        public static void UpdateCurrency(IDataBase sql, bool opt, int gameid) {
            if (!opt && Currency != null)
                return;

            sql.ClearParameters();
            string query = "SELECT text ";
            query += " FROM textmap ";
            query += " WHERE property = 'Cell_Currency' AND language='TW' AND GID=@gid ";
            sql.TryAddParameter("gid", gameid);

            string result = sql.ExecuteScalar(query).ToString();

            string[] tmp = result.Split(';');
            for (int i = 0; i < tmp.Length; i++) {
                Currency.Add((i + 1).ToString(), tmp[i]);
            }
        }

        public void SendMsg(Player p, string type, string msg) {
            p.Send(new ActionObject(type, new { Msg = msg }));
        }

        //回傳安全值
        private string checkstrreturn(string str) {
            str = splitHTML(Sanitizer.GetSafeHtmlFragment(str.Trim()).Trim()).Trim();
            return HttpUtility.HtmlDecode(str);

        }

        // <summary>         
        /// 過濾所有的HTML Tag(保留字型與斷行)
        /// </summary>         
        /// <param name="text">要被過濾的文字</param>          
        /// <returns></returns>          
        public static string splitHTML(string html) {
            string split = html;
            System.Text.RegularExpressions.Regex regex = new System.Text.RegularExpressions.Regex(@"<(?!(br)|(p)|(/p)|(/font)|(font color))[^>]*?>");
            //System.Text.RegularExpressions.Regex regex = new System.Text.RegularExpressions.Regex("<[^>]+>|]+>");
            split = regex.Replace(split, "");

            //System.Text.RegularExpressions.Regex regex2 = new System.Text.RegularExpressions.Regex("[^%!&+'");
            //split = regex2.Replace(split, "");

            return split;
        }

        //判斷TextBox輸入值是否為數字(Int)
        public static bool IsInt(string TextBoxValue) {
            try {
                int i = Convert.ToInt32(TextBoxValue);
                return true;
            } catch {
                return false;
            }
        }

        public static bool IsDouble(string txt) {
            try {
                double i = Convert.ToDouble(txt);
                return true;
            } catch {
                return false;
            }
        }

        public static bool Islong(string TextBoxValue) {
            try {
                long i = Convert.ToInt64(TextBoxValue);
                return true;
            } catch {
                return false;
            }
        }

        #region Print PDF
        public string ConvertTableToHTMLString(DataTable dt, List<string> colTitle, string[] recordName, bool isEN) {
            string result = "", currency = "";
            List<List<object>> record = dt.toList();
            List<string> dt_column = dt.ColumnNames();
            List<string> tmp = new List<string>();
            int index;
            result += "<table cellspacing='0' style='border:1px solid black;width:100%;text-align:center;margin-top:10px;'>";
            /*Column*/
            result += newTR(colTitle, false);
            string addText = isEN ? "Member Group" : " 會員群組";

            /*Record*/
            for (int i = 0; i < record.Count; i++) {
                tmp.Clear();
                for (int j = 0; j < recordName.Length; j++) {
                    index = dt_column.IndexOf(recordName[j]);
                    if (index != -1) {
                        if (recordName[j] == "Currency") {
                            if (Currency.TryGetValue(record[i][index].ToString(), out currency))
                                tmp.Add(currency);
                            else
                                tmp.Add(record[i][index].ToString());
                        } else if (recordName[j] == "Name")
                            tmp.Add((i == 0 ? addText : record[i][index].ToString()));
                        else
                            tmp.Add(record[i][index].ToString());
                    }
                }
                result += newTR(tmp, true);
            }
            result += "</table>";
            return result;
        }

        string newTR(List<string> list, bool firstLeft) {
            string result = "<tr>";

            for (int i = 0; i < list.Count; i++) {
                result += "<td style='border:1px solid black;padding:5px;" + (i == 0 && firstLeft ? "text-align:left;" : "") + "'>";
                result += list[i];
                result += "</td>";
            }
            result += "</tr>";
            return result;
        }

        public void pdfEBookConvert(Player p, string content, int Dic_path_Type) {
            List<string> errorMsg = new List<string>();
            //try {
            string fileName = DateTime.Now.ToString("yyyy_MM_dd_HH_mm_ss");
            string ManageServer = System.Configuration.ConfigurationManager.AppSettings["manageServer"];
            string wkhtmltopdf = System.Configuration.ConfigurationManager.AppSettings["wkhtmltopdf"];
            string PDFPath = System.Configuration.ConfigurationManager.AppSettings["PDFPath"] + (Dic_path_Type == 0 ? @"AUTO\" : @"SEARCH\");

            string path = PDFPath + fileName + ".pdf";

            content = "<p>" + content + "</p>";

            byte[] data = Encoding.UTF8.GetBytes(content);//字串轉成byte[]
            MemoryStream msInput = new MemoryStream(data);
            Document doc = new Document();//要寫PDF的文件，建構子沒填的話預設直式A4
            PdfWriter writer = PdfWriter.GetInstance(doc, new FileStream(path, FileMode.Create));
            //指定文件預設開檔時的縮放為100%
            PdfDestination pdfDest = new PdfDestination(PdfDestination.XYZ, 0, doc.PageSize.Height, 1f);
            //開啟Document文件 
            doc.Open();
            try {
                //使用XMLWorkerHelper把Html parse到PDF檔裡
                XMLWorkerHelper.GetInstance().ParseXHtml(writer, doc, msInput, null, Encoding.UTF8, new UnicodeFontFactory());
                //將pdfDest設定的資料寫到PDF檔
                PdfAction action = PdfAction.GotoLocalPage(1, pdfDest, writer);
                writer.SetOpenAction(action);
            } finally {
                doc.Close();
                msInput.Close();
            }
            ////HttpContext context = HttpContext.Current;
            //StringReader reader = new StringReader(content);

            ////Create PDF document 
            //Document document = new Document(PageSize.A4);
            //HTMLWorker parser = new HTMLWorker(document);

            //PdfWriter.GetInstance(document, new FileStream(path, FileMode.Create));
            //document.Open();

            //try {
            //    parser.Parse(reader);
            //} catch (Exception ex) {
            //    //Display parser errors in PDF. 
            //    Paragraph paragraph = new Paragraph("Error!" + ex.Message);
            //    Chunk text = paragraph.Chunks[0] as Chunk;
            //    if (text != null) {
            //        text.Font.Color = BaseColor.RED;
            //    }
            //    document.Add(paragraph);
            //} finally {
            //    document.Close();
            //    //DownLoadPdf(PDF_FileName);
            //}


            p.Send(new ActionObject("request.pdf", new { fileName }));
        }
        #endregion

        public void GetDayReportList(Player p, MsgPack msg) {
            var result = SQLAction.getDayReportList(db, msg);

            p.Send("request.DayReport", new {
                Record = result.toList(),
                Column = result.ColumnNames(),
                DataCount = SQLAction.allDataCount
            });
        }

        public void UserBan(IDataBase sql, Player p, MsgPack msg) {
            SQLAction.SetUserBan(sql, msg, p);

            p.Send("user.ban", new {
                Success = true
            });
        }

        public void GetHomePageData(IDataBase sql, Player p, MsgPack msg) {
            if (Member.isMember(p)) {
                //SendError(p, "edit.error", "無權");
                return;
            }

            DataTable dt = DataStruct.NewTable(DataStruct.DataType.HOMEPAGE);
            DataRow dr;

            DataStruct.SetDefaultVal(DataStruct.DataType.HOMEPAGE, dt, out dr);

            dt.Rows.Add(dr);

            dr[(int)Col_HomePage.Account] = p["KeyString"];
            dr[(int)Col_HomePage.NickName] = p.Name;
            int level = Member.GetLevel(p);

            Player tmp_p = new Player();

            if (Member.isSharedHolderPermission(p)) {
                Member.GetPlayerData(sql, Convert.ToInt32(p["UpLevelId"]), tmp_p);
            } else
                tmp_p = p;

            DataTable pList = getLowerList(sql, tmp_p);


            int arr_len = Controller.MaxLevel;
            double[] credit = new double[arr_len];
            double[] creditremain = new double[arr_len];
            int[] count = new int[arr_len];
            int[] ban_count = new int[arr_len];
            double cash = 0;

            int asignLevel;

            for (int i = 0; i < pList.Rows.Count; i++) {
                if (Member.isMember(pList.Rows[i]))
                    /*is Member*/
                    asignLevel = arr_len;
                else
                    asignLevel = Member.GetLevel(pList.Rows[i]["UpLevelTree"].ToString());

                asignLevel -= 1;

                if (asignLevel >= arr_len)
                    continue;

                if (Ban.getIntStatus(pList.Rows[i]) == BanType.BANNED)
                    ban_count[asignLevel] += 1;
                else
                    count[asignLevel] += 1;

                credit[asignLevel] += Convert.ToDouble(pList.Rows[i]["CreditMax"]);
                creditremain[asignLevel] += Convert.ToDouble(pList.Rows[i]["CreditRemain"]);
                if (ShowData_Permission.isNormalMember(pList.Rows[i]))
                    cash += Convert.ToDouble(pList.Rows[i]["BankCredit"]);
            }

            dr[(int)Col_HomePage.Cash] = cash;

            int startIndex = (int)Col_HomePage.Level1Credit;
            int startIndex_remain = (int)Col_HomePage.Level1RemainCredit;
            for (int i = 0; i < arr_len; i++) {
                //dr[startIndex + i] = credit.Skip(i).Sum();
                //dr[startIndex_remain + i] = creditremain.Skip(i).Sum();

                dr[startIndex + i] = credit[i];
                dr[startIndex_remain + i] = creditremain[i];
            }

            startIndex = (int)Col_HomePage.Level2Count;
            for (int i = 1; i < arr_len; i++) {
                dr[startIndex + i - 1] = count[i];
            }

            startIndex = (int)Col_HomePage.Level2BanCount;
            for (int i = 1; i < arr_len; i++) {
                dr[startIndex + i - 1] = ban_count[i];
            }

            p.Send(new ActionObject("request.homepage", new {
                GID,
                Record = dt.toList(),
                Column = dt.ColumnNames(),
                Level = level
            }));

        }

        public void GetUserCredit(Player p, MsgPack msg) {
            int type = msg.Get<int>("type");
            int EditShopID = msg.Get<int>("uid");

            if (!Member.isCompanyPermission(p) || EditShopID == 0) {
                SendMsg(p, type + "_error", "無權新增/修改");
                return;
            }

            DataTable uplevel_dt = SQLAction.GetUpLevelIdTree(db, EditShopID.ToString());

            if (!ShowData_Permission.isLowerLevel(p, uplevel_dt.Rows[0]["UpLevelTree"].ToString())) {
                SendMsg(p, type + "_error", "無權新增/修改");
                return;
            }

            //DataTable dt = SQLAction.GetUserCreditInfo(db, EditShopID);
            DataTable dt = Rule_CheckOut.GetUserCheckOut(db, uplevel_dt, EditShopID);
            if (dt.Rows.Count != 1) {
                SendMsg(p, type + "_error", "查無資料");
                return;
            }

            p.Send(new ActionObject("request.usercredit", new {
                type = type,
                Column = dt.ColumnNames(),
                Record = dt.RowToList(0)
                //credit = dt.Rows[0]["CreditMax"],
                //creditremain = dt.Rows[0]["CreditRemain"],
                //ud = dt.Rows[0]["BankCredit"]
            }));

        }

        #region Online
        public void GetOnline(Player thisplayer) {
            if (ShowData_Permission.isAdmin(thisplayer) || ShowData_Permission.isStockHolder(thisplayer))
                callToGameGetOnlineRecord(thisplayer);
            else
                thisplayer.Send(new ActionObject("request.online", new {
                    Data = ""
                }));
        }

        void ReceiveOnline(Player thisplayer, MsgPack msg, Dictionary<string, object> StoreMsg, string version) {

            thisplayer.Send(new ActionObject("request.online", new {
                Data = msg.Content

            }));
        }
        #endregion

        public void GetWeekData(Player p, MsgPack msg) {
            //int UID = msg.Get<int>("UID");
            int CurrencyID = 1;

            DataTable dt = SQLAction.GetReportCheckOutSetting(db, CurrencyID);

            p.Send(new ActionObject("request.setting_week", new {
                Column = dt.ColumnNames(),
                Record = dt.toList(),
                //Type = msg.Get<int>("Type"),
                UID = CurrencyID
            }));
        }

        public void ConfirmWeekData(IDataBase sql, Player p, MsgPack msg) {
            int CurrencyID = 1;

            object[] setting = msg.Get<object[]>("Data");

            string reason;
            bool Success = SQLAction.SetCurrencyTimeSetting(sql, CurrencyID, setting, out reason);

            p.Send(new ActionObject("request.setting_confirm", new {
                Success,
                reason
            }));
        }

        #region checkout
        public void GetCashOutList(Player thisplayer) {
            if (ShowData_Permission.isAdmin(thisplayer)) {
                DataTable dt = Rule_CheckOut.GetAllUserCheckOutListRemoveZero(db, thisplayer);

                DataRow dr = dt.NewRow();
                dr[0] = "總結";
                for (int i = 1; i < dt.Columns.Count; i++) {
                    dr[i] = 0;
                }
                /*總結*/
                for (int i = 0; i < dt.Rows.Count; i++) {
                    for (int j = 1; j < dt.Columns.Count; j++) {
                        dr[j] = Convert.ToDouble(dr[j]) + Convert.ToDouble(dt.Rows[i][j]);
                    }
                }

                thisplayer.Send(new ActionObject("request.cashout_list", new {
                    Column = dt.ColumnNames(),
                    Record = dt.toList(),
                    Sum = dr.ItemArray,
                    Success = true
                }));
            } else
                thisplayer.Send(new ActionObject("request.cashout_list", new {
                    Success = false
                }));
        }

        public void CheckOutExcute(IDataBase sql, Player thisplayer) {
            if (ShowData_Permission.isAdmin(thisplayer)) {
                int lId;
                SQLAction.getCheckOutStatus(sql, out isCheckOut, out isCheckOutFinish, out lId);
                if (isCheckOut) {
                    thisplayer.Send(new ActionObject("request.checkout", new {
                        Success = false,
                        reason = "結帳中"
                    }));
                    return;
                }

                SQLAction.setCheckOutStatus(sql, 3, lId);

                thisplayer.Send(CheckOutStatus(false));

                SQLAction.WriteCheckOutLog(sql, "request.checkout,通知遊戲端要結帳", thisplayer.MsgHandler!=null ? thisplayer.MsgHandler.getIP():"");
                callToGameCheckOut(thisplayer);

                GetLastCheckOutTime(sql, thisplayer);

                //bool Success = Rule_CheckOut.Excute(sql, thisplayer, out error);
                //thisplayer.Send(new ActionObject("request.checkout", new {
                //    Success,
                //    reason = error
                //}));
            } else
                thisplayer.Send(new ActionObject("request.checkout", new {
                    Success = false,
                    reason = "不是Admin"
                }));
        }

        public void CheckOutFinish(IDataBase sql, Player thisplayer) {
            if (ShowData_Permission.isAdmin(thisplayer)) {
                int lId;
                SQLAction.getCheckOutStatus(sql, out isCheckOut, out isCheckOutFinish, out lId);
                if (!isCheckOut) {
                    thisplayer.Send(new ActionObject("request.checkout", new {
                        Success = false,
                        reason = "尚未結帳"
                    }));
                    return;
                }

                if (!isCheckOutFinish) {
                    thisplayer.Send(new ActionObject("request.checkout", new {
                        Success = false,
                        reason = "結帳中"
                    }));
                    return;
                }

                SQLAction.setCheckOutStatus(sql, 2, lId);

                callToGameCheckOutFinish(thisplayer);

                thisplayer.Send(CheckOutStatus(false));

                GetLastCheckOutTime(sql, thisplayer);

                SQLAction.WriteCheckOutLog(sql, "checkout.finish,結帳完畢", thisplayer.MsgHandler != null ? thisplayer.MsgHandler.getIP() : "");
            } else
                thisplayer.Send(new ActionObject("request.checkout", new {
                    Success = false,
                    reason = "不是Admin"
                }));
        }

        public void CheckOutRestore(IDataBase sql, Player thisplayer) {
            if (!ShowData_Permission.isAdmin(thisplayer)) {
                thisplayer.Send(new ActionObject("request.checkout", new {
                    Success = false,
                    reason = "不是Admin"
                }));
                return;
            }

            SQLAction.setCheckOutStatus(sql, 2);

            thisplayer.Send(CheckOutStatus(false));

            GetLastCheckOutTime(sql, thisplayer);

            SQLAction.WriteCheckOutLog(sql, "手動初始化結帳", thisplayer.MsgHandler != null ? thisplayer.MsgHandler.getIP() : "");
        }

        public void CheckOutRelease(IDataBase sql, Player thisplayer) {
            if (!ShowData_Permission.isAdmin(thisplayer)) {
                thisplayer.Send(new ActionObject("request.checkout", new {
                    Success = false,
                    reason = "不是Admin"
                }));
                return;
            }

            callToGameCheckOutFinish(thisplayer);

            SQLAction.setCheckOutStatus(sql, 2);

            thisplayer.Send(CheckOutStatus(false));

            SQLAction.WriteCheckOutLog(sql, "手動通知遊戲解除結帳", thisplayer.MsgHandler != null ? thisplayer.MsgHandler.getIP() : "");
        }

        public static object CheckOutStatus(bool Reset = true) {
            bool btn_ckeckout = false, btn_checkout_finish = false;
            try {
                using (var sql = new MySQL("unidb")) {
                    if (Reset) {
                        int lId = 0;
                        SQLAction.getCheckOutStatus(sql, out isCheckOut, out isCheckOutFinish, out lId);
                    }

                    if (!isCheckOut) {
                        /*未結帳*/
                        btn_ckeckout = true;
                        btn_checkout_finish = false;
                    } else if (isCheckOut && !isCheckOutFinish) {
                        /*結帳中 未完成*/
                        btn_ckeckout = false;
                        btn_checkout_finish = false;
                    } else if (isCheckOut && isCheckOutFinish) {
                        /*結帳中 已完成*/
                        btn_ckeckout = false;
                        btn_checkout_finish = true;
                    }

                    SQLAction.WriteCheckOutLog(sql, "getStatus", type: 2);
                }
            } catch (Exception) {

            }
            return new ActionObject("request.checkout_status", new {
                btn_ckeckout,
                btn_checkout_finish
            });
        }

        public void callToGameCheckOut(Player thisplayer) {
            string GID = Rule_Shared.GetFixedGID();

            IGameProxy g_ws;
            DataRow dr;
            string[] actType = new string[] { "close", "request" };

            UpdateGIDGameList(thisplayer);

            if (gameList.TryGetValue(GID, out dr)) {
                g_ws = getPlayerGameProxy(thisplayer, GID, dr);

                g_ws.StoreMsg.Add("acttype", actType.Join("."));
                g_ws.StoreMsg.Add("GID", GID);
                g_ws.StoreMsg.Add("player", thisplayer);
                g_ws.Connected();

                var sendObj = new { pID = thisplayer.ID };

                if (dr["version"].ToString() == "1")
                    g_ws.Send(actType.Join("."), sendObj);
                else
                    g_ws.Send(actType[0], actType[1], sendObj);

                g_ws.StoreMsg.Clear();

                g_ws.Closed();
            } else {
                isCheckOut = false;
                sendGameError("Not Find GID!", thisplayer);
                using (var sql = new MySQL("unidb"))
                    SQLAction.WriteCheckOutLog(sql, "error.callToGameCheckOut,not find GID");
            }
        }

        public void callToGameCheckOutFinish(Player thisplayer) {
            string GID = Rule_Shared.GetFixedGID();

            IGameProxy g_ws;
            DataRow dr;
            string[] actType = new string[] { "close", "finish" };

            UpdateGIDGameList(thisplayer);

            if (gameList.TryGetValue(GID, out dr)) {
                g_ws = getPlayerGameProxy(thisplayer, GID, dr);

                g_ws.StoreMsg.Add("acttype", actType.Join("."));
                g_ws.StoreMsg.Add("GID", GID);
                g_ws.StoreMsg.Add("player", thisplayer);
                g_ws.Connected();

                var sendObj = new { pID = thisplayer.ID };

                if (dr["version"].ToString() == "1")
                    g_ws.Send(actType.Join("."), sendObj);
                else
                    g_ws.Send(actType[0], actType[1], sendObj);

                g_ws.StoreMsg.Clear();

                g_ws.Closed();
            } else {
                isCheckOut = false;
                sendGameError("Not Find GID!", thisplayer);
                using (var sql = new MySQL("unidb"))
                    SQLAction.WriteCheckOutLog(sql, "error.callToGameCheckOutFinish,not find GID");
            }
        }

        public void GetCashOutHistoryList(Player thisplayer, MsgPack msg) {
            if (ShowData_Permission.isCompany(thisplayer)) {
                string start = msg.Get<string>("start"),
                    end = msg.Get<string>("end"),
                    name = msg.Get<string>("name");

                DataTable dt = Rule_CheckOut.GetAllUserCheckOutList_History(db, thisplayer, start, end, name);

                dt = Rule_CheckOut.GetAllUserCheckOutListRemoveZero(dt);

                DataRow dr = dt.NewRow();
                dr[1] = "總結";
                for (int i = 2; i < dt.Columns.Count - 1; i++) {
                    dr[i] = 0;
                }
                /*總結*/
                for (int i = 0; i < dt.Rows.Count; i++) {
                    for (int j = 2; j < dt.Columns.Count - 1; j++) {
                        dr[j] = Convert.ToDouble(dr[j]) + Convert.ToDouble(dt.Rows[i][j]);
                    }
                }

                thisplayer.Send(new ActionObject("request.cashout_list_history", new {
                    Column = dt.ColumnNames(),
                    Record = dt.toList(),
                    Sum = dr.ItemArray,
                    Success = true
                }));
            } else
                thisplayer.Send(new ActionObject("request.cashout_list_history", new {
                    Success = false
                }));
        }

        public void GetLastCheckOutTime(IDataBase sql, Player p) {
            DataTable dt = Rule_CheckOut.GetLatestCashOutList(sql);
            string time;

            if (dt.Rows.Count == 0)
                time = "無";
            else
                time = Convert.ToDateTime(dt.Rows[0]["CreateDate"]).ToString("yyyy/MM/dd HH:mm:ss");

            p.Send(new ActionObject("request.updatecheckout", new {
                time,
                finish = !isCheckOut
            }));
        }
        #endregion

        public void callToGameCashInOut(IDataBase sql, Player LoginPlayer, MsgPack ct) {
            Pop_Type excuteType = (Pop_Type)ct.Get<int>("type");
            string reason = "", sqlcmd = "", CanAddNext = "0";
            int targetuid = ct.Get<int>("uid");
            //long price = 0;
            double bonus = 0;
            string price = ct.Get<string>("price");

            bool isSuccess = false;

            Player targetPlayer = new Player();
            if (!Member.GetPlayerData(sql, targetuid, targetPlayer)) {
                reason = "查無玩家";

                LoginPlayer.Send("600.0", new {
                    Success = isSuccess,
                    msg = reason
                });

                return;
            }

            long valid_price = 0;

            string GID = Rule_Shared.GetFixedGID();

            IGameProxy g_ws;
            DataRow dr;
            string[] actType = new string[] { };
            string cashintype = "";
            switch (excuteType) {
                case Pop_Type.CASHIN:
                    cashintype = ct.Get<string>("cashintype");
                    if (Rule_Shared.IsValid_CashCredit(LoginPlayer, targetPlayer, price, out reason) &&
                        Rule_CashIn.IsValid_CashIn(LoginPlayer, targetPlayer, price, cashintype, out reason)) {
                        if (price != "") {
                            valid_price = Convert.ToInt64(price);
                        }
                        actType = new string[] { "player", "in" };
                        //Rule_CashIn.Excute(sql, targetPlayer, LoginPlayer, valid_price, cashintype);
                        isSuccess = true;
                    }

                    break;
                case Pop_Type.CASHOUT:
                    price = "";
                    if (Rule_Shared.IsValid_CashCredit(LoginPlayer, targetPlayer, price, out reason)) {
                        actType = new string[] { "player", "out" };
                        //Rule_CashOut.Excute(sql, targetPlayer, LoginPlayer, valid_price, cashoutInfo);
                        isSuccess = true;
                    }
                    break;
                case Pop_Type.SETCREDIT:
                    if (Rule_Shared.IsValid_CashCredit(LoginPlayer, targetPlayer, price, out reason) &&
                        Rule_SetCredit.IsValidSetCredit(LoginPlayer, targetPlayer, price, out reason)) {
                        valid_price = Convert.ToInt64(price);
                        Rule_SetCredit.Excute(sql, LoginPlayer, targetPlayer, valid_price);
                        isSuccess = true;
                    }
                    break;
                default:
                    break;

            }

            if (!isSuccess) {
                LoginPlayer.Send("600.0", new {
                    Success = isSuccess,
                    msg = reason,
                    type = excuteType
                });

                return;
            }
            UpdateGIDGameList(LoginPlayer);

            if (gameList.TryGetValue(GID, out dr) && actType.Length > 0) {
                g_ws = getPlayerGameProxy(LoginPlayer, GID, dr);

                g_ws.StoreMsg.Add("acttype", actType.Join("."));
                g_ws.StoreMsg.Add("GID", GID);
                g_ws.StoreMsg.Add("player", LoginPlayer);
                g_ws.StoreMsg.Add("type", (int)excuteType);
                g_ws.Connected();

                var sendObj = new {
                    pID = LoginPlayer.ID,
                    price = valid_price,
                    login_uid = LoginPlayer.ID,
                    ID = targetPlayer.ID,
                    cashintype = cashintype,
                    ConnectType = 1
                };

                if (dr["version"].ToString() == "1")
                    g_ws.Send(actType.Join("."), sendObj);
                else
                    g_ws.Send(actType[0], actType[1], sendObj);
            } else {
                isCheckOut = false;
                sendGameError("Not Find GID!", LoginPlayer);
                SQLAction.WriteCheckOutLog(sql, "error.callToGameCashInOut,not find GID");
            }
        }

        public void ReceiveCashIn(Player thisplayer, MsgPack msg, Dictionary<string, object> StoreMsg, string version) {
            thisplayer.Send(new ActionObject("600.0", new {
                type = StoreMsg["type"],
                Success = msg.Content.Get<bool>("isSuccess"),
                msg = msg.Content.Get<string>("reason")
            }));

        }

        public void EditPassword(IDataBase sql, Player p, MsgPack msg) {
            string o_pa = msg.Get<string>("oldpwd");
            string n_pa = msg.Get<string>("newpwd");

            string reason;
            bool Success = Member.EditPassword(sql, p, o_pa, n_pa, out reason);

            p.Send(new ActionObject("edit.password", new {
                Success,
                reason
            }));
        }

        public void GetCheckOutTime(IDataBase sql, Player p, MsgPack msg) {
            string start = msg.Get<string>("start");
            string end = msg.Get<string>("end");
            var List = SQLAction.GetCheckOutTimeList(db, start, end);

            p.Send(new ActionObject("request.checkouttimelist", new {
                Record = List.toList(),
                Column = List.ColumnNames(),
                start = start,
                end = end,
                Type = msg.Get<int>("Type")
            }));
        }

        public void GetAccountSum(IDataBase sql, Player p)
        {
            string title;
            int level = Member.GetLevel(p);

            if (level < Controller.MaxLevel)
            {
                level += 1;
            }

            CompanyTitle.Get(p, level, out title);

            DataTable pList = getLowerList(sql, p);

            int arr_len = Controller.MaxLevel;
            int[] count = new int[arr_len];

            int asignLevel;

            for (int i = 0; i < pList.Rows.Count; i++)
            {
                if (Member.isMember(pList.Rows[i]))
                    /*is Member*/
                    asignLevel = arr_len;
                else
                    asignLevel = Member.GetLevel(pList.Rows[i]["UpLevelTree"].ToString());

                asignLevel -= 1;

                if (asignLevel >= arr_len)
                    continue;

                count[asignLevel] += 1;
            }

            p.Send(new ActionObject("request.accountsum", new
            {
                lowerTitle = title,
                lowerCount = count[level - 1],
                usedCredit = (Rule_Shared.GetCreditMax(p) - Rule_Shared.GetCreditRemain(p))
            }));
        }

        public void callToGameABC(Player thisplayer, object[] record) {
            string GID = Rule_Shared.GetFixedGID();

            IGameProxy g_ws;
            DataRow dr;
            string[] actType = new string[] { "deal", "func" };

            UpdateGIDGameList(thisplayer);

            if (gameList.TryGetValue(GID, out dr)) {
                g_ws = getPlayerGameProxy(thisplayer, GID, dr);

                g_ws.StoreMsg.Add("acttype", actType.Join("."));
                g_ws.StoreMsg.Add("GID", GID);
                g_ws.StoreMsg.Add("player", thisplayer);
                g_ws.Connected();

                var sendObj = new AbcStruct(record);

                if (dr["version"].ToString() == "1")
                    g_ws.Send(actType.Join("."), sendObj);
                else
                    g_ws.Send(actType[0], actType[1], sendObj);

                ABC.ABCStatus = (int)AbcStatus.Enabled;
                ABC.SendABCStatus(thisplayer);
            } else {
                sendGameError("Not Find GID!", thisplayer);
            }
        }

    }
}
