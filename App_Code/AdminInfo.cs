﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// AdminInfo 的摘要描述
/// </summary>
public class AdminInfo : BaseClass {
    public int UID { get; set; }

    public bool IsLogin { get; set; }

    public string Level { get; set; }

    public string Permission { get; set; }

    /// <summary>
    /// 登入類型0:管理者1:店長2:員工
    /// </summary>
    public int loginType { get; set; }

    /// <summary>
    /// 所屬公司ID
    /// </summary>
    public int ShopId { get; set; }

    public AdminInfo() {
        IsLogin = false;
        //
        // TODO: 在這裡新增建構函式邏輯
        //

        //var u = new AdminInfo();
        //var u2 = new AdminInfo();
        //if (u) {
        //    var u3 = u + u2;
        //}
    }

    //public static implicit operator bool(AdminInfo ad) {
    //    return ad != null && ad.UID != 0;
    //}
    //public static AdminInfo operator +(AdminInfo a, AdminInfo b) {
    //    var c = new AdminInfo();
    //    c.UID = a.UID + b.UID;
    //    return c;
    //}
}