﻿using System;
using System.Linq;
using System.Collections.Generic;
using GameLibrary;
using Tool;
using System.Threading;
using System.Collections.Concurrent;
using System.Data;

public partial class Controller : iController {
    /*Singlton*/
    private Controller() {
        controllerID = 3;
        Initialize();
    }
    public static ConcurrentDictionary<int, string> CompanyTitle = new ConcurrentDictionary<int, string>();
    static Controller _instance;
    static object initLocker = new object();
    public static string defaultGID = "";
    public static int MaxLevel = 5;
    public static iController GetInstance {
        get {
            lock (initLocker) {
                if (_instance == null)
                    _instance = new Controller();
            }

            return _instance;
        }
    }

    /*初始化*/
    public override void Initialize() {
        /*參數初始化*/
        base.Initialize();
        if (initialized)
            return;
        initialized = true;
        Recorder.Owner = "Controller";
        WSHandler.Controller = this;
        Recorder.rootPath = "c:\\inetpub\\asm\\logs\\controller";
        /*遊戲初始化*/
        BuildGame(new MySQL("unidb"));
        Ssm.Reporter.UpdateCurrency(new MySQL("unidb"), true, 31);
    }

    /*事件處理*/
    public override void handleMessage(Player p, MsgPack msg) {
        var ct = msg.Content;
        if (ct == null || msg.Type == null) return;
        iGame gs;
        if (msg.Type.Contains("login")) {
            bool isValid = false;

            if (!p.isAuthorized) {

                using (var sql = new MySQL("unidb", Recorder)) {
                    if (ct.Exist("autcode")) {
                        isValid = Member.Login(sql, ct.Get<string>("autcode"), p);
                    } else if (ct.Exist("acc") && ct.Exist("pwd")) {
                        string account = ct.Get<string>("acc");
                        string pwd = ct.Get<string>("pwd");
                        isValid = Member.Login(sql, account, pwd, p);
                    }
                    if (isValid)
                        Reconnect(ref p);


                    if (isValid) {
                        if (ct.Exist("lan"))
                            p["Language"] = Languages.SetString(ct.Get<string>("lan"));
                        Recorder.RecordLine("login", p.Name + "  " + msg.Type);

                        DataTable tmp= Rule_CheckOut.GetLatestCashOutList(sql);

                        DateTime lastCheckoutTime = tmp.Rows.Count > 0 ? Convert.ToDateTime(tmp.Rows[0]["End"]) : Convert.ToDateTime(DateTime.Now.ToString("yyyy-MM-dd 00:00:00"));

                        p.Send(new ActionObject("login", new {
                            p.Token,
                            p.Name,
                            p.ID,
                            Role = p["canAddNextLevel"],
                            MaxLevel,
                            Title = p["title"],
                            Level = p["Level"],
                            //p.Role,
                            Permission = p["canSeeNextLevel"],
                            Games = Games.Select(g => g.Key).ToArray(),
                            isAdmin = ShowData_Permission.isAdmin(p),
                            lastCheckoutTime = lastCheckoutTime.ToString("yyyy-MM-dd HH:mm:ss")
                        }));

                        PlayerUpdated = true;

                        gs = Games[defaultGID];
                        if (gs != null) {
                            gs.AddPlayer(p);
                        }

                        return;
                    }
                }
                p.Send(new ActionObject("login.error", new { Msg = "登入失敗" }));
                return;
            }
        } else if (msg.Type.Contains("601.")) {
            /*開分員*/
            if (p.isAuthorized) {
                //有認證過
                OpenerAct(p, msg);
            }
        } 
        else {
            if (p.isAuthorized) {
                //有認證過

                //確認身分
                string[] uplevelArr = p["UpLevelTree"].ToString().Split(',');
                if (uplevelArr.Length == 3) {
                    //最頂層 Admin
                    if (Games.TryGetValue(ct.Get<string>("GID", ""), out gs)) {
                        gs.Play(p, msg);

                    }
                } else {
                    if (Games.TryGetValue(ct.Get<string>("GID", ""), out gs)) {
                        gs.Play(p, msg);

                    }
                }

            }

        }

        if (msg.Type == "auto.report" && ct.Exist("acc") && ct.Exist("pwd")) {
            /*自動報表*/
            string account = ct.Get<string>("acc");
            string pwd = ct.Get<string>("pwd");
            using (var sql = new MySQL("unidb", Recorder)) {
                Player tmp = new Player();
                bool isValid = Member.Login(sql, account, pwd, tmp);
                if (isValid && Games.TryGetValue(ct.Get<string>("GID", ""), out gs)) {
                    /* && p.MsgHandler.getIP() == "203.66.57.79"*/
                    gs.Play(tmp, msg);
                }
            }
        }

        string[] checkOutChangeStatusAct = new string[] { 
            "auto.report","request.checkout","request.checkout_finish"
        };

        if (checkOutChangeStatusAct.Contains(msg.Type)) {
            Broadcast(Ssm.Reporter.CheckOutStatus());
        }
        
        if (msg.Type == "check.time") {
            bool timecheck = false;

            if (Games.TryGetValue(defaultGID, out gs)) {
                timecheck = gs.checkTime(p, msg);
            }
            p.Send(new ActionObject("check.time", new { Success = timecheck }));
        }

        checkout_message(p, msg.Type);
    }

    /*斷線處理*/
    public override void handleClose(Player p) {
        if (p.isAuthorized) {
            if (p == mainManager)
                mainManager = null;
            /*登出遊戲*/
            iGame gs;
            var l = p.Games.ToList();
            foreach (var id in l)
                if (Games.TryGetValue(id, out gs)) {
                    gs.closeAllGameConnect(p);
                    gs.RemovePlayer(p);
                }
            ////更新U幣
            //using (var sql = new MySQL("unidb"))
            //    Member.UpdateUserUD(sql, p);

        }
        PlayerUpdated = true;

    }

    public void checkout_message(Player p, string msg_type) {
        string reason;
        
        switch (msg_type) {
            case "close.ready":
                Player admin = new Player();
                MySQL sql = new MySQL("unidb");
                int lId;
                Ssm.SQLAction.getCheckOutStatus(sql, out Ssm.Reporter.isCheckOut, out Ssm.Reporter.isCheckOutFinish, out lId);
                if (!Ssm.Reporter.isCheckOut) {
                    return;
                }
                if (Member.GetPlayerData(sql, 5342, admin))
                    Rule_CheckOut.Excute(sql, admin, out reason);

                Broadcast(Ssm.Reporter.CheckOutStatus());

                Ssm.SQLAction.WriteCheckOutLog(sql, "get close.ready,結帳完,等待按結帳完畢按鈕", p.MsgHandler != null ? p.MsgHandler.getIP() : "");
                //IGameProxy g_ws = new IGameProxy("wss://obacar.crazybet.win/webHandler.ashx");
                //g_ws.Connected();
                //g_ws.Send("close.finish", new { Success = true });
                //g_ws.Closed();

                //Ssm.Reporter.isCheckOut = false;

                //p.Disconnect();
                break;
            default:
                break;
        }
    }
}