﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Drawing;
using System.Windows.Media.Imaging;
using Tool;

namespace Ssm {
    public class QRCode {
        public QRCode() {
            //
            // TODO: 在這裡新增建構函式邏輯
            //
        }


        public static bool Generate(string content, string fileName) {
            if (content == "") {
                return false;
            }

            int Width = 180;
            int Height = 180;

            Bitmap bitmap = null;
            //QRCode的設定
            ZXing.BarcodeWriter writer = new ZXing.BarcodeWriter {
                Format = ZXing.BarcodeFormat.QR_CODE,
                Options = new ZXing.QrCode.QrCodeEncodingOptions {
                    //產生出圖片的高度
                    Height = Height,
                    //產生出圖片的寬度
                    Width = Width,
                    //文字是使用哪種編碼方式
                    CharacterSet = "UTF-8",

                    //錯誤修正容量
                    //L水平    7%的字碼可被修正
                    //M水平    15%的字碼可被修正
                    //Q水平    25%的字碼可被修正
                    //H水平    30%的字碼可被修正
                    ErrorCorrection = ZXing.QrCode.Internal.ErrorCorrectionLevel.H

                }
            };
            //將要編碼的文字產生出QRCode的圖檔
            bitmap = writer.Write(content);
            //儲存圖片
            bitmap.Save(System.Web.Configuration.WebConfigurationManager.AppSettings["QRPath"] + fileName + ".png", System.Drawing.Imaging.ImageFormat.Png);

            return true;
        }


    }
}