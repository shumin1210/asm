﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Ssm;
using System.Text;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;
using Tool;
/// <summary>
/// DataStructure 的摘要描述
/// </summary>
public static class DataStruct {

    public enum DataType { 
        GAMELIST, 
        TRANS, 
        MEMBERMANAGE, 
        HOMEPAGE, 
        SETWEEK,
        STOCKHOLDERMANAGE,
        GAMEREFUND
    }

    sealed class ColAttribute : Attribute {
        public Type ColType;
        public object DefaultValue;
        public bool isSum;
        public ColAttribute(Type type, object defaultVal,bool sum=true) {
            ColType = type;
            DefaultValue = defaultVal;
            isSum = sum;
        }
    }

    public class CompanyBonusInfo {
        public double AgentBonus = 0;
        public double CompanyBonus = 0;

        public double bonus = 0;
        public double bonusrate = 0;
        public double companyBonus_rate = 0;
        public double sum = 0;

        public CompanyBonusInfo() { }
        
    }

    public enum Col_Total {
        [Col(typeof(Double), 0)]
        totalBet,
        [Col(typeof(Double), 0)]
        totalWin,
        [Col(typeof(Double), 0)]
        Refund,
        [Col(typeof(Int64), 0)]
        UserID,
        [Col(typeof(String), "")]
        Name,

        [Col(typeof(Double), 0)]
        ShopBonus,
        [Col(typeof(Double), 0)]
        AgentRefundRate,
        [Col(typeof(Double), 0)]
        bonusrate,
        [Col(typeof(Int32), 1)]
        Currency,
        [Col(typeof(Double), 0)]
        Sum,

        [Col(typeof(Double), 0)]
        AgentBonus,
        [Col(typeof(Double), 0)]
        CompanyBonus,
        [Col(typeof(Int32), 1)]
        First,
        [Col(typeof(Int32), 1)]
        Status,
        [Col(typeof(Double), 0)]
        AgentRefund,

        [Col(typeof(Double), 0)]
        Profit,
        [Col(typeof(Int32), 0)]
        DataCount,

        [Col(typeof(Double), 0)]
        Level1Bonus,
        [Col(typeof(Double), 0)]
        Level2Bonus,
        [Col(typeof(Double), 0)]
        Level3Bonus,
        [Col(typeof(Double), 0)]
        Level4Bonus
    }

    public enum Col_Trans {
        [Col(typeof(Int64), 0)]
        UserID,
        [Col(typeof(String), "")]
        Name,
        [Col(typeof(Double), 0)]
        Refund,
        [Col(typeof(Double), 0)]
        Price,
        [Col(typeof(Double), 0)]
        UD,

        [Col(typeof(String), "")]
        Place,
        [Col(typeof(String), "")]
        Mode,
        [Col(typeof(Double), 0)]
        ShopBonus,
        [Col(typeof(Double), 0)]
        bonusrate,
        [Col(typeof(Int32), 1)]
        Currency,

        [Col(typeof(Double), 0)]
        AgentBonus,
        [Col(typeof(Double), 0)]
        CompanyBonus,
        [Col(typeof(Int32), 1)]
        First,
        [Col(typeof(String), "")]
        GID,
        [Col(typeof(Int64), 0)]
        Cash,

        [Col(typeof(Int64), 0)]
        Credit,
        [Col(typeof(DateTime), "")]
        CreateDate,
        [Col(typeof(Int32), 1)]
        Status,
        [Col(typeof(Int64), 0)]
        CreditMax,
        [Col(typeof(Int64), 0)]
        MemberCost,

        [Col(typeof(Double), 0)]
        UpperBonus
    };

    public enum Col_MemberManage {
        [Col(typeof(Int64), 0)]
        UserID,
        [Col(typeof(String), "")]
        Name,
        [Col(typeof(Double), 0)]
        Refund,        
        [Col(typeof(Double), 0)]
        UserUD,
        [Col(typeof(String), "")]
        Mode,

        [Col(typeof(Double), 0)]
        ShopBonus,
        [Col(typeof(Double), 0)]
        bonusrate,
        [Col(typeof(Int32), 1)]
        Currency,
        [Col(typeof(Double), 0)]
        AgentBonus,
        [Col(typeof(Double), 0)]
        CompanyBonus,

        [Col(typeof(Int64), 0)]
        Cash,
        [Col(typeof(Int32), 1)]
        First,
        [Col(typeof(String), "")]
        GID,        
        [Col(typeof(DateTime), "")]
        CreateDate,
        [Col(typeof(Int32), 1)]
        Status,

        [Col(typeof(Int64), 0 )]
        CreditSum,
        [Col(typeof(Int64), 0 )]
        CanUseCredit,
        [Col(typeof(String), "")]
        Place,
        [Col(typeof(Double), 0)]
        Price,
        [Col(typeof(Double), 0)]
        UD,

        [Col(typeof(Int32), 0)]
        Role,
        [Col(typeof(Int64), 0)]
        CreditRemain,
        [Col(typeof(String), "-")]
        LastLoginTime,
        [Col(typeof(String), -1)]
        RemainRefund,
        [Col(typeof(Int64), 0)]
        CashSum
    };

    public enum Col_GameRefund
    {
        [Col(typeof(Int64), 0)]
        UserID,
        [Col(typeof(String), "")]
        Name,
        [Col(typeof(Double), 0)]
        Refund,
        [Col(typeof(Int32), 1)]
        Status,
        [Col(typeof(Int32), 1)]
        Currency,
        [Col(typeof(Int32), 1)]
        First
    }

    public enum Col_HomePage {
        [Col(typeof(String), "")]
        Account,
        [Col(typeof(String), "")]
        NickName,
        [Col(typeof(Double), 0)]
        Cash,
        [Col(typeof(Double), 0)]
        OutstandingPayments,/*未清帳款總額*/

        [Col(typeof(Double), 0)]
        Level1Credit,/*總公司信用額度*/
        [Col(typeof(Double), 0)]
        Level2Credit,/*分公司信用額度*/
        [Col(typeof(Double), 0)]
        Level3Credit,/*超級總代理信用額度*/
        [Col(typeof(Double), 0)]
        Level4Credit,/*代理信用額度*/
        [Col(typeof(Double), 0)]
        Level5Credit,

        [Col(typeof(Int32), 0)]
        Level2Count,        
        [Col(typeof(Int32), 0)]
        Level3Count,        
        [Col(typeof(Int32), 0)]
        Level4Count,
        [Col(typeof(Int32), 0)]
        Level5Count,

        [Col(typeof(Int32), 0)]
        Level2BanCount,
        [Col(typeof(Int32), 0)]
        Level3BanCount,
        [Col(typeof(Int32), 0)]
        Level4BanCount,        
        [Col(typeof(Int32), 0)]
        Level5BanCount,

        [Col(typeof(Double), 0)]
        Level1RemainCredit,/*總公司信用額度*/
        [Col(typeof(Double), 0)]
        Level2RemainCredit,/*分公司信用額度*/
        [Col(typeof(Double), 0)]
        Level3RemainCredit,/*超級總代理信用額度*/
        [Col(typeof(Double), 0)]
        Level4RemainCredit,/*代理信用額度*/
        [Col(typeof(Double), 0)]
        Level5RemainCredit
    }

    public enum Col_SetWeek {
        [Col(typeof(Int64), 0)]
        UserID,
        [Col(typeof(Int32), 0)]
        ID,
        [Col(typeof(Int32), 0)]
        Currency,
        [Col(typeof(string), "-")]
        w0,
        [Col(typeof(string), "-")]
        w1,

        [Col(typeof(string), "-")]
        w2,
        [Col(typeof(string), "-")]
        w3,
        [Col(typeof(string), "-")]
        w4,
        [Col(typeof(string), "-")]
        w5,
        [Col(typeof(string), "-")]
        w6,

        [Col(typeof(Int32), 0)]
        Status
    }

    public enum Col_StockholderManage {
        [Col(typeof(Int64), 0)]
        UserID,
        [Col(typeof(String), "")]
        Name,
        [Col(typeof(Int32), 1)]
        Currency,
        [Col(typeof(Int32), 1)]
        Status,
        [Col(typeof(Int32), 1)]
        First
    }

    public static DataTable NewTable(DataType type) {
        DataTable dt = new DataTable();

        string colName;
        ColAttribute attr;
        int Count = getColCount(type);

        for (int i = 0; i < Count; i++) {
            if (!getColName(type, i, out colName))
                continue;

            attr = getColAttr(type, i);
            addColumn(dt, colName, attr.ColType, attr.DefaultValue);
        }
        return dt;
    }

    public static bool SetDefaultVal(DataType type, DataTable dt, out DataRow dr) {
        dr = dt.NewRow();

        ColAttribute attr;
        int Count = getColCount(type);

        string colName;
        for (int i = 0; i < Count; i++) {
            if (!getColName(type, i, out colName))
                continue;

            attr = getColAttr(type, i);
            if (attr.ColType == typeof(DateTime))
                dr[colName] = DateTime.Now;
            else
                dr[colName] = attr.DefaultValue;
        }
        return true;
    }

    public static DataRow SetVal(DataType type, DataRow dr, Dictionary<int, object> val) {
        if (dr == null)
            return dr;

        ColAttribute attr;
        int Count = getColCount(type);

        string colName;
        foreach (var item in val) {
            if (!getColName(type, item.Key, out colName) || !dr.Table.Columns.Contains(colName))
                continue;
            if (Count > item.Key) {
                attr = getColAttr(type, item.Key);
                try {
                    if (attr.ColType == typeof(Double))
                        dr[colName] = Convert.ToDouble(item.Value);
                    else if (attr.ColType == typeof(Int64))
                        dr[colName] = Convert.ToInt64(item.Value);
                    else if (attr.ColType == typeof(Int32))
                        dr[colName] = Convert.ToInt32(item.Value);
                    else if (attr.ColType == typeof(DateTime))
                        dr[colName] = Convert.ToDateTime(item.Value);
                    else
                        dr[colName] = item.Value;
                } catch (Exception) {
                    if (attr.ColType == typeof(DateTime))
                        dr[colName] = DateTime.Now;
                    else
                        dr[colName] = attr.DefaultValue;
                }
            } else
                dr[colName] = 0;
        }

        return dr;
    }

    /// <summary>
    /// Col_Total:[ totalBet, totalWin, Refund, UserID, Name,
    /// ShopBonus, Refund2, bonusrate, Currency, Sum,
    /// AgentBonus, CompanyBonus, First,Status];
    /// Col_Trans:[ UserID, Name, Refund, Price,UD,
    /// Place, Mode,ShopBonus, bonusrate, Currency, 
    /// AgentBonus, CompanyBonus, First,GID,Status]
    /// </summary>
    /// <param name="type"></param>
    /// <param name="dr"></param>
    /// <param name="val"></param>
    /// <returns></returns>
    public static DataRow SetVal(DataType type, DataRow dr,params object[] val) {
        if (dr == null)
            return dr;

        ColAttribute attr;
        int Count = getColCount(type);

        if (val.Length != Count)
            return dr;

        string colName;
        for (int i = 0; i < val.Length; i++) {
            if (!getColName(type, i, out colName) || !dr.Table.Columns.Contains(colName))
                continue;

            attr = getColAttr(type, i);
            try {
                if (attr.ColType == typeof(Double))
                    dr[colName] = Convert.ToDouble(val[i]);
                else if (attr.ColType == typeof(Int64))
                    dr[colName] = Convert.ToInt64(val[i]);
                else if (attr.ColType == typeof(Int32))
                    dr[colName] = Convert.ToInt32(val[i]);
                else if (attr.ColType == typeof(DateTime))
                    dr[colName] = Convert.ToDateTime(val[i]);
                else
                    dr[colName] = val[i];
            } catch (Exception) {
                if (attr.ColType == typeof(DateTime))
                    dr[colName] = DateTime.Now;
                else
                    dr[colName] = attr.DefaultValue;
            }
        }

        return dr;
    }

    public static DataTable SetVal(DataType type, DataTable dt, DataTable val) {
        if (dt == null || val.Rows.Count == 0)
            return dt;

        ColAttribute attr;
        int Count = getColCount(type);

        int index;
        string colName;
        DataRow dr;
        for (int i = 0; i < val.Rows.Count; i++) {
            dr = dt.NewRow();
            for (int j = 0; j < val.Columns.Count; j++) {
                colName = val.Columns[j].ColumnName;
                if (!getColIndex(type, colName, out index) || !dt.Columns.Contains(colName))
                    continue;

                attr = getColAttr(type, index);
                try {
                    if (attr.ColType == typeof(Double))
                        dr[colName] = Convert.ToDouble(val.Rows[i][j]);
                    else if (attr.ColType == typeof(Int64))
                        dr[colName] = Convert.ToInt64(val.Rows[i][j]);
                    else if (attr.ColType == typeof(Int32))
                        dr[colName] = Convert.ToInt32(val.Rows[i][j]);
                    else if (attr.ColType == typeof(DateTime))
                        dr[colName] = Convert.ToDateTime(val.Rows[i][j]);
                    else
                        dr[colName] = val.Rows[i][j];
                } catch (Exception) {
                    if (attr.ColType == typeof(DateTime))
                        dr[colName] = DateTime.Now;
                    else
                        dr[colName] = attr.DefaultValue;
                }
            }
            dt.Rows.Add(dr);
        }

        return dt;
    }

    /// <summary>
    /// 計算總和
    /// </summary>
    /// <param name="type"></param>
    /// <param name="dt"></param>
    /// <param name="val"></param>
    /// <returns></returns>
    public static bool ComputeSum(DataType type, DataTable val, out DataTable dt) {
        dt = null;

        if (val.Rows.Count == 0)
            return false;

        ColAttribute attr;
        int Count = getColCount(type);

        dt = NewTable(type);

        int index;
        string colName;
        DataRow dr;
        SetDefaultVal(type, dt, out dr);
        for (int i = 0; i < val.Rows.Count; i++) {
            for (int j = 0; j < val.Columns.Count; j++) {
                colName = val.Columns[j].ColumnName;
                if (!getColIndex(type, colName, out index) || !dt.Columns.Contains(colName))
                    continue;

                attr = getColAttr(type, index);

                try {
                    if (attr.ColType == typeof(Double))
                        dr[colName] = Convert.ToDouble(dr[colName]) + Convert.ToDouble(val.Rows[i][j]);
                    else if (attr.ColType == typeof(Int64))
                        dr[colName] = Convert.ToInt64(dr[colName]) + Convert.ToInt64(val.Rows[i][j]);
                    else if (attr.ColType == typeof(Int32))
                        dr[colName] = Convert.ToInt32(dr[colName]) + Convert.ToInt32(val.Rows[i][j]);
                    else if (attr.ColType == typeof(DateTime))
                        dr[colName] = Convert.ToDateTime(val.Rows[i][j]);
                    else
                        dr[colName] = val.Rows[i][j];
                } catch (Exception) {
                    if (attr.ColType == typeof(DateTime))
                        dr[colName] = DateTime.Now;
                    else
                        dr[colName] = attr.DefaultValue;
                }
            }
        }

        dt.Rows.Add(dr);

        return true;
    }

    public static DataRow ComputeSum(DataType type, string[] ExceptCol, DataRow target, DataRow val) {
        if (val == null)
            return target;

        ColAttribute attr;

        int index;
        string colName;

        for (int i = 0; i < val.Table.Columns.Count; i++) {
            colName = val.Table.Columns[i].ColumnName;
            if (Array.IndexOf(ExceptCol, colName) != -1 || !getColIndex(type, colName, out index) || !target.Table.Columns.Contains(colName))
                continue;
                
            attr = getColAttr(type, index);

            try {
                if (attr.ColType == typeof(Double))
                    target[colName] = Convert.ToDouble(target[colName]) + Convert.ToDouble(val[i]);
                else if (attr.ColType == typeof(Int64))
                    target[colName] = Convert.ToInt64(target[colName]) + Convert.ToInt64(val[i]);
                else if (attr.ColType == typeof(Int32))
                    target[colName] = Convert.ToInt32(target[colName]) + Convert.ToInt32(val[i]);
                else if (attr.ColType == typeof(DateTime))
                    target[colName] = Convert.ToDateTime(val[i]);
                else
                    target[colName] = val[i];
            } catch (Exception) {
                if (attr.ColType == typeof(DateTime))
                    target[colName] = DateTime.Now;
                else
                    target[colName] = attr.DefaultValue;
            }
            
        }

        return target;
    }

    #region private method

    static DataTable addColumn(DataTable dt, string columnName, Type dataType, object defaultVal) {
        DataColumn newColumn = new DataColumn(columnName, dataType);
        if (dataType == typeof(DateTime)) 
            newColumn.DefaultValue = DateTime.Now;
        else
            newColumn.DefaultValue = defaultVal;
        dt.Columns.Add(newColumn);
        return dt;
    }

    static bool getColName(DataType type, int index, out string colName) {
        colName = "";
        switch (type) {
            case DataType.GAMELIST:
                colName = Enum.GetName(typeof(Col_Total), index);
                break;
            case DataType.TRANS:
                colName = Enum.GetName(typeof(Col_Trans), index);
                break;
            case DataType.MEMBERMANAGE:
                colName = Enum.GetName(typeof(Col_MemberManage), index);
                break;
            case DataType.HOMEPAGE:
                colName = Enum.GetName(typeof(Col_HomePage), index);
                break;
            case DataType.SETWEEK:
                colName = Enum.GetName(typeof(Col_SetWeek), index);
                break;
            case DataType.STOCKHOLDERMANAGE:
                colName = Enum.GetName(typeof(Col_StockholderManage), index);
                break;
            case DataType.GAMEREFUND:
                colName = Enum.GetName(typeof(Col_GameRefund), index);
                break;
            default:
                break;
        }
        return colName != null && colName != "";
    }

    static bool getColIndex(DataType type, string colName, out int index) {
        index = -1;
        switch (type) {
            case DataType.GAMELIST:
                index = (int)Enum.Parse(typeof(Col_Total), colName);
                break;
            case DataType.TRANS:
                index = (int)Enum.Parse(typeof(Col_Trans), colName);
                break;
            case DataType.MEMBERMANAGE:
                index = (int)Enum.Parse(typeof(Col_MemberManage), colName);
                break;
            case DataType.HOMEPAGE:
                index = (int)Enum.Parse(typeof(Col_HomePage), colName);
                break;
            case DataType.SETWEEK:
                index = (int)Enum.Parse(typeof(Col_SetWeek), colName);
                break;
            case DataType.STOCKHOLDERMANAGE:
                index = (int)Enum.Parse(typeof(Col_StockholderManage), colName);
                break;
            case DataType.GAMEREFUND:
                index = (int)Enum.Parse(typeof(Col_GameRefund), colName);
                break;
            default:
                break;
        }
        return index != null && index != -1;
    }

    static int getColCount(DataType type) {
        int count;
        switch (type) {
            case DataType.GAMELIST:
                count = Enum.GetNames(typeof(Col_Total)).Length;
                break;
            case DataType.TRANS:
                count = Enum.GetNames(typeof(Col_Trans)).Length;
                break;
            case DataType.MEMBERMANAGE:
                count = Enum.GetNames(typeof(Col_MemberManage)).Length;
                break;
            case DataType.HOMEPAGE:
                count = Enum.GetNames(typeof(Col_HomePage)).Length;
                break;
            case DataType.SETWEEK:
                count = Enum.GetNames(typeof(Col_SetWeek)).Length;
                break;
            case DataType.STOCKHOLDERMANAGE:
                count = Enum.GetNames(typeof(Col_StockholderManage)).Length;
                break;
            case DataType.GAMEREFUND:
                count = Enum.GetNames(typeof(Col_GameRefund)).Length;
                break;
            default:
                count = 0;
                break;
        }
        return count;
    }

    static ColAttribute getColAttr(DataType type, int i) {
        ColAttribute attr;
        if (type == DataType.GAMELIST)
            attr = ((Col_Total)(i)).GetAttribute<ColAttribute>();
        else if (type == DataType.TRANS)
            attr = ((Col_Trans)(i)).GetAttribute<ColAttribute>();
        else if (type == DataType.HOMEPAGE)
            attr = ((Col_HomePage)(i)).GetAttribute<ColAttribute>();
        else if (type == DataType.SETWEEK)
            attr = ((Col_SetWeek)(i)).GetAttribute<ColAttribute>();
        else if (type == DataType.STOCKHOLDERMANAGE)
            attr = ((Col_StockholderManage)(i)).GetAttribute<ColAttribute>();
        else if (type == DataType.GAMEREFUND)
            attr = ((Col_GameRefund)(i)).GetAttribute<ColAttribute>();
        else
            attr = ((Col_MemberManage)(i)).GetAttribute<ColAttribute>();

        return attr;
    }

    #endregion
}

public class UnicodeFontFactory : FontFactoryImp {

    private static readonly string arialFontPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Fonts),
        "arialuni.ttf");//arial unicode MS是完整的unicode字型。
    private static readonly string 標楷體Path = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Fonts),
      "KAIU.TTF");//標楷體


    public override Font GetFont(string fontname, string encoding, bool embedded, float size, int style, BaseColor color,
        bool cached) {
        //可用Arial或標楷體，自己選一個
        BaseFont baseFont = BaseFont.CreateFont(標楷體Path, BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
        return new Font(baseFont, size, style, color);
    }


}