﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using GameLibrary;
using System.Data;
using Tool;

namespace Ssm {
    public enum AbcStatus { NONE, Enabled, Recording, Updating }

    public class AbcStruct {
        public string GID = "";
        public string Pokers = "";
        public string Winner="";
        public string Value="";
        public AbcStruct(string GID, string pokers,string winner, string value) {
            this.GID = GID;
            this.Pokers = pokers;
            this.Winner = winner;
            this.Value = value;
        }
        public AbcStruct(object[] record) {
            object[] column = ABC.ABCColumn;
            try {
                for (int i = 0; i < column.Length; i++) {
                    if (column[i].ToString() == "GID")
                        this.GID = record[i].ToString();
                    else if (column[i].ToString() == "Value")
                        this.Value = record[i].ToString();
                    else if (column[i].ToString() == "Winner")
                        this.Winner = record[i].ToString();
                    else if (column[i].ToString() == "Pokers")
                        this.Pokers = record[i].ToString();
                }
            } catch (Exception) {                
            }
        }
        public AbcStruct() {

        }
    }
    public static class ABC {
        private static int _abcStatus = (int)AbcStatus.NONE;
        public static int ABCStatus {
            get {
                return _abcStatus;
            }
            set {
                _abcStatus = value;
                Controller.GetInstance.Recorder.RecordLine("abc", value.ToString());
            }
        }

        public static object[] ABCColumn = new object[] {
            "Value",
            "Winner",
            "Pokers",
            "GID"
        };

        public static void SendABCStatus(Player p, bool success = true) {
            p.Send(new ActionObject("abc.setstatus", new {
                success = success,
                status = success ? ABCStatus : (int)AbcStatus.NONE
            }));
        }

        public static void RequestABCData(IDataBase sql, Player p) {
            if (!ShowData_Permission.isAdmin(p)) {
                p.Send(new ActionObject("abc.requestdata", new {
                    data = new List<string>()
                }));
                return;
            }
            DataTable data = SQLAction.GetABCData(sql);
            p.Send(new ActionObject("abc.requestdata", new {
                Column = data.ColumnNames(),
                Record = data.toList()
            }));
        }
        public static bool CheckValueSendABCToGame(IDataBase sql, Player p, MsgPack msg) {
            if (!ShowData_Permission.isAdmin(p)) {
                return false;
            }
            object[] column = msg.Get<object[]>("Column"),
                    record = msg.Get<object[]>("Record");

            if(!CheckCol(column) || 
                column.Length != record.Length || 
                !CheckABCValue(record)) 
                return false;
            
            return true;
        }

        static bool CheckCol(object[] col) {
            if (col.Length != ABCColumn.Length) {
                return false;
            }
            for (int i = 0; i < col.Length; i++) {
                if (col[i].ToString() != ABCColumn[i].ToString()) {
                    return false;
                }
            }
            return true;
        }

        static bool CheckABCValue(object[] record) {
            if (ABCColumn.Length != record.Length)
                return false;
            int count = 0;
            using (var sql = new MySQL("unidb")) {
                sql.ClearParameters();
                for (int i = 0; i < ABCColumn.Length; i++) {
                    sql.TryAddParameter(ABCColumn[i].ToString(), record[i]);
                }
                count = Convert.ToInt32(sql.ExecuteScalar("SELECT COUNT(value) FROM abc WHERE value=@value AND winner=@winner AND pokers=@pokers AND GID=@GID"));
            }

            return count > 0;
        }
            
        public static void deleteABC(object[] column,object[] record) {
            if (!CheckCol(column) ||
                column.Length != record.Length)
                return;

            using (var sql = new MySQL("unidb")) {
                sql.ClearParameters();
                for (int i = 0; i < ABCColumn.Length; i++) {
                    sql.TryAddParameter(ABCColumn[i].ToString(), record[i]);
                }
                sql.Execute("DELETE FROM abc WHERE value=@value AND winner=@winner AND pokers=@pokers AND GID=@GID");
            }
        }    
    }
}