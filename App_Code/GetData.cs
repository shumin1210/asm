﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Microsoft.Security.Application;
using System.IO;
using EmployeeRelated;
//using MySQLRelated;
using System.Web.Script.Serialization;

/// <summary>
/// GetData 的摘要描述
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// 若要允許使用 ASP.NET AJAX 從指令碼呼叫此 Web 服務，請取消註解下列一行。
[System.Web.Script.Services.ScriptService]
public class GetData : System.Web.Services.WebService {

    Employee em = new Employee();

    System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();

    public GetData () {

        //如果使用設計的元件，請取消註解下列一行
        //InitializeComponent(); 
    }

    [WebMethod]
    public string HelloWorld() {
        return "Hello World";
    }

    #region 左邊menu

    [WebMethod(EnableSession = true)]
    public string MainMenu()
    {
        DataTable dt = new DataTable();

        using (AdminInfo user = Session["AdminInfo"] != null ? (AdminInfo)Session["AdminInfo"] : null)
        {
            if (user != null)
            {
                if (user.Level != "0")
                {
                    dt = em.GetshopinfoByEmployeeID(user.UID);
                }
                else
                {
                    dt = em.GetshopinfoAll();
                }
            }
        }

        return JsonObject(dt);
    }

    [WebMethod(EnableSession = true)]
    public string subMenu(string id)
    {
        //MySQL mysql = new MySQL("UniDB");
        //string strQuery;

        //mysql.parameters.Clear();
        //strQuery = "select UID,ShopName from userinfo where UpLevelId=@id order by  Resignation";
        //mysql.parameters.AddWithValue("id", id);

        //DataTable dt = mysql.ExecuteDataTable(strQuery);


        //return JsonObject(dt);
        return "";
    }

    #endregion

    #region 公司資訊
    [WebMethod(EnableSession = true)]
    public string mainShop()
    {
        DataTable dt = new DataTable();
        JavaScriptSerializer jser = new JavaScriptSerializer();
        AdminInfo user = Session["AdminInfo"] != null ? (AdminInfo)Session["AdminInfo"] : null;
        
        if (user != null)
        {
            dt = em.GetshopinfoByEmployeeID(user.UID);
        }

        return jser.Serialize(new { cooperate = JsonObject(dt) });

        //return JsonObject(dt);
    }

    [WebMethod(EnableSession = true)]
    public string subShop(string id)
    {
        AdminInfo user = Session["AdminInfo"] != null ? (AdminInfo)Session["AdminInfo"] : null;
        
        JavaScriptSerializer jser = new JavaScriptSerializer();
        
        DataTable dt = em.GetshopinfoByEmployeeID(Convert.ToInt32(id));

        string date = DateTime.Now.ToString("yyyy-MM");

        string UpLevelTree = dt.Rows[0]["UpLevelTree"].ToString() + id + ",";

        DataTable employee = em.GetshopinfoEmployee(id, date, UpLevelTree, false, 0, 0);

        return jser.Serialize(new { cooperate = JsonObject(dt), em = JsonObject(employee) });
        
    }

    [WebMethod(EnableSession = true)]
    public string pageChange(string pageIndex, string pageCount, string id)
    {
        AdminInfo user = Session["AdminInfo"] != null ? (AdminInfo)Session["AdminInfo"] : null;

        JavaScriptSerializer jser = new JavaScriptSerializer();
        
        DataTable dt = em.GetshopinfoByEmployeeID(Convert.ToInt32(id));

        string date = DateTime.Now.ToString("yyyy-MM");
        
        int Page = Convert.ToInt32(pageIndex) - 1;

        string UpLevelTree = dt.Rows[0]["UpLevelTree"].ToString() + id + ",";

        DataTable employee = em.GetshopinfoEmployee(id, date, UpLevelTree, true, Page, Convert.ToInt32(pageCount));

        return jser.Serialize(new { em = JsonObject(employee) });

    }

    #endregion

    

    /// <summary>
    /// 轉換物件為Json格式字串(DataTable)
    /// </summary>
    /// <param name="ob"></param>
    /// <returns></returns>
    public string JsonObject(DataTable dt)
    {
        List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
        Dictionary<string, object> row = null;

        foreach (DataRow dr in dt.Rows)
        {
            row = new Dictionary<string, object>();
            foreach (DataColumn col in dt.Columns)
            {
                row.Add(col.ColumnName, dr[col]);
            }
            rows.Add(row);
        }

        return serializer.Serialize(rows);
    }

    public string JsonObject2(List<object> ob)
    {
        
        
        return serializer.Serialize(new { msg = ob });
    }

    #region 預防XSS字串攻擊
    //回傳安全值
    private string checkstrreturn(string str)
    {
        str = splitHTML(Sanitizer.GetSafeHtmlFragment(str.Trim()).Trim()).Trim();
        return HttpUtility.HtmlDecode(str);

    }

    // <summary>         
    /// 過濾所有的HTML Tag(保留字型與斷行)
    /// </summary>         
    /// <param name="text">要被過濾的文字</param>          
    /// <returns></returns>          
    public static string splitHTML(string html)
    {
        string split = html;
        System.Text.RegularExpressions.Regex regex = new System.Text.RegularExpressions.Regex(@"<(?!(br)|(p)|(/p)|(/font)|(font color))[^>]*?>");
        split = regex.Replace(split, "");

        return split;
    }

    /// <summary>
    /// 判斷字串是否與指定正則運算式匹配
    /// </summary>
    /// <param name="input">要驗證的字串</param>
    /// <param name="regularExp">正則運算式</param>
    /// <returns>驗證通過返回true</returns>
    public static bool IsMatch(string input, string regularExp)
    {
        return System.Text.RegularExpressions.Regex.IsMatch(input, regularExp);
    }
    #endregion

}


