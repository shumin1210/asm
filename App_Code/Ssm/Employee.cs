﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Init;
using System.Data;
using Tool;
 
namespace EmployeeRelated
{
    /// <summary>
    /// Employee 的摘要描述
    /// </summary>
    public class Employee
    {
        //private SQL sql;
        private GameLibrary.MySQL mysql;
        //private MySQL myunisql;
        private string strQuery;

        public string s_OrderNumber { get; set; }
        public string employeeID { get; set; }
        public string shopID { get; set; }
        public string eName { get; set; }
        public String phone { get; set; }
        public string eEmail { get; set; }
        public string aut { get; set; }

        public string product { get; set; }
        public int count { get; set; }
        public int price { get; set; }
        public int Surspls { get; set; }
        public int Amount_Limit { get; set; }
        
        public string ShopName { get; set; }
        public string ShopAdd { get; set; }
        public string ShopTel { get; set; }
        public string ShopIP { get; set; }
        public double ShopBonus { get; set; }
        public double Refund { get; set; }
        public int EmployeeBonus { get; set; }
        public string SequnceID { get; set; }
        public int SequencePlaceID { get; set; }
        public int ChangedPoint { get; set; }
        public int Resignation { get; set; }

        public int GID = 25;

        public Employee()
        {
            //
            // TODO: 在此加入建構函式的程式碼
            //

            //sql = new SQL("Auction");
            mysql = new GameLibrary.MySQL("UniDB", new Logger("Employee"));
        }
        public bool IsLogin(string account, string password)
        {
            bool isLogin = false;
            mysql.parameters.Clear();
            mysql.parameters.Clear();
            mysql.parameters.AddWithValue("account", account);
            mysql.parameters.AddWithValue("password", password);

            strQuery = "SELECT * FROM employee " +
                       "WHERE phone=@account AND password=@password ";

            //mysql.CallConnection("open");

            mysql.ExecuteReader(strQuery);
            if (mysql.reader.Read())
            {

                employeeID = Convert.ToString(mysql.reader["EmployeeID"]);
                shopID = Convert.ToString(mysql.reader["ShopID"]);
                phone = Convert.ToString(mysql.reader["Phone"]);
                eName = Convert.ToString(mysql.reader["EName"]);
                eEmail = Convert.ToString(mysql.reader["EEmail"]);
               
                aut = Convert.ToString(mysql.reader["Title"]);

                //mysql.CallConnection("close");
                mysql.parameters.Clear();
                mysql.parameters.Clear();
                mysql.parameters.AddWithValue("shopID", shopID);
                strQuery = "SELECT * from shopinfo where shopID=" + shopID;
                //mysql.CallConnection("open");
                mysql.ExecuteReader(strQuery);
                if (mysql.reader.Read())
                {
                    ShopName = Convert.ToString(mysql.reader["ShopName"]);
                    
                    ShopIP = Convert.ToString(mysql.reader["ShopIP"]);
                    
                }
                //mysql.CallConnection("close");

                isLogin = true;
            }
           

            return isLogin;
        }
        public bool authority(String aut, String webname)
        {
            bool isAut = false;

            mysql.parameters.Clear();
            mysql.parameters.AddWithValue("aut", aut);
            mysql.parameters.AddWithValue("website", webname);

            strQuery = @"SELECT COUNT(*) FROM authority WHERE website=@website AND aut=@aut";

            int count = Convert.ToInt32(mysql.ExecuteScalar(strQuery));
            if (count > 0) isAut = true;

            return isAut;
        }
                    

        public bool CheckShopPermission(int adminId, int adminShopId, int addShopId)
        {
            if (adminShopId == addShopId)
            {//加下一層級公司
                return true;
            }

            bool isOk = false;
            mysql.parameters.Clear();

            int shopId = addShopId;
            DataTable dt;

            while (shopId != 0)
            {
                mysql.parameters.Clear();
                mysql.parameters.AddWithValue("nowShopId", shopId);
                strQuery = @"SELECT UID,UpLevelId FROM userinfo where UID=@nowShopId ";
                dt = mysql.ExecuteDataTable(strQuery);
                if (dt.Rows.Count > 0)
                {
                    if (adminId == Convert.ToInt32(dt.Rows[0]["UID"]))
                    {
                        isOk = true;
                        break;
                    }
                    else
                    {//繼續上層尋找
                        isOk = false;
                        shopId = Convert.ToInt32(dt.Rows[0]["UpLevelId"]);
                    }
                }
                else
                {
                    isOk = false;
                    break;
                }
            }

            return isOk;
        }

        public int GetOtherShopBonus(int UpLevelId, int addShopId)
        {            
            mysql.parameters.Clear();

            mysql.parameters.AddWithValue("UpLevelId", UpLevelId);
            mysql.parameters.AddWithValue("addShopId", addShopId);
            mysql.parameters.AddWithValue("GID", GID);
            strQuery = @"SELECT SUM(ShopBonus) FROM userinfo u LEFT JOIN gamefee f ON f.UID=u.UID WHERE u.UpLevelId=@UpLevelId ";
            if (addShopId != 0)
            {
                strQuery += " and u.UID !=@addShopId";
            }

            strQuery += " AND f.GID =@GID";

            object tmp = mysql.ExecuteScalar(strQuery);

            if (!DBNull.Value.Equals(tmp))
            {
                return Convert.ToInt32(tmp);
            }
            else
            {
                return 0;
            }
        }

        public int GetOtherShopBonusByUID(int UID) {
            mysql.parameters.Clear();

            mysql.parameters.AddWithValue("UID", UID);
            //mysql.parameters.AddWithValue("addShopId", addShopId);
            mysql.parameters.AddWithValue("GID", GID);
            strQuery = @"SELECT UpLevelId FROM userinfo u WHERE u.UID=@UID ";
            
            object tmp = mysql.ExecuteScalar(strQuery);

            int UpLevelId = 0;
            if (!DBNull.Value.Equals(tmp)) {
                UpLevelId = Convert.ToInt32(tmp);
            } else {
                return 0;
            }

            strQuery = @"SELECT SUM(ShopBonus) FROM userinfo u LEFT JOIN gamefee f ON f.UID=u.UID ";
            strQuery += " WHERE u.UpLevelId=@UpLevelId AND u.UID !=@UID ";
            mysql.parameters.AddWithValue("UpLevelId", UpLevelId);

            tmp = mysql.ExecuteScalar(strQuery);

            if (!DBNull.Value.Equals(tmp)) {
                return Convert.ToInt32(tmp);
            } else {
                return 0;
            }
        }

        public double GetUpLevelShopBonus(int UpLevelId)
        {
            mysql.parameters.Clear();

            mysql.parameters.AddWithValue("GID", GID);
            mysql.parameters.AddWithValue("UpLevelId", UpLevelId);
            strQuery = @"SELECT f.ShopBonus FROM gamefee f ";
            strQuery += " WHERE f.UID=@UpLevelId AND f.GID=@GID ";
            

            object tmp = mysql.ExecuteScalar(strQuery);

            if (!DBNull.Value.Equals(tmp))
            {
                return Convert.ToDouble(tmp);
            }
            else
            {
                return 0;
            }
        }
                

        /// <summary>
        /// 取得此公司資訊
        /// </summary>
        /// <param name="shopId"></param>
        /// <returns></returns>
        public DataTable Getshopinfo(string shopId)
        {
            mysql.parameters.Clear();

            mysql.parameters.AddWithValue("shopId", shopId);
            strQuery = @"SELECT * FROM userinfo where UID=@shopId ";

            return mysql.ExecuteDataTable(strQuery);

        }

        public DataTable GetshopinfoByShopId(int ShopId)
        {
            mysql.parameters.Clear();

            mysql.parameters.AddWithValue("ShopId", ShopId);
            strQuery = @"SELECT * FROM shopinfo where ShopType<3 and ShopID=@ShopId order by  Resignation ";

            return mysql.ExecuteDataTable(strQuery);

        }

        public DataTable GetshopinfoByEmployeeID(int UID)
        {
            mysql.parameters.Clear();

            mysql.parameters.AddWithValue("UID", UID);
            mysql.parameters.AddWithValue("GID", GID);

            strQuery = @"SELECT u.UID,ShopName,ShopPhone,Resignation,";
            strQuery += " Name,ShopAddress,UpLevelTree,f.Refund,f.ShopBonus ";
            strQuery += " FROM userinfo u ";
            strQuery += " LEFT JOIN gamefee f ON u.UID = f.UID ";
            strQuery += " WHERE u.UID=@UID AND f.GID=@GID order by  Resignation ";

            return mysql.ExecuteDataTable(strQuery);

        }

        public DataTable GetshopinfoEmployee(string id, string date, string UpLevelTree,bool pageOrNot, int pageIndex,int pageCount)
        {
            mysql.parameters.Clear();
            mysql.parameters.AddWithValue("id", id);
            strQuery = "select u.UID,u.Name,s.Price,u.Code,u.UpLevelId from userinfo u ";
            strQuery += " left join (SELECT UID,Price from sequence WHERE SequenceModeID='B' and CreateDate like '" + date + "%') s ";
            strQuery += " on s.UID = u.UID where u.UpLevelTree LIKE @UpLevelTree or u.UID=@id GROUP BY u.UID ";
            mysql.parameters.AddWithValue("UpLevelTree", UpLevelTree + "%");

            if (pageOrNot)
            {
                strQuery += " LIMIT " + (pageIndex * pageCount) + "," + ((pageIndex + 1) * pageCount);
            }

            return mysql.ExecuteDataTable(strQuery);
        }

        public DataTable GetshopinfoAll()
        {
            mysql.parameters.Clear();
            strQuery = @"SELECT UID,ShopName FROM userinfo where UpLevelId=0 order by  Resignation ";

            return mysql.ExecuteDataTable(strQuery);

        }
                

        public DataTable Getshopsellinfo(String first)
        {
            mysql.parameters.Clear();
            mysql.parameters.AddWithValue("SequenceModeID", "B");
            strQuery = "select  s.shopname,s.shopid,se.CreateDate,e.EmployeeID,e.EName ,(s.ShopBonus+s.EmployeeBonus)as ShopBonus,sum(se.price) as price,(se.price * (s.shopbonus + s.EmployeeBonus))/100 as saleprice from shopinfo s join employee e on s.ShopID=e.ShopID join sequence se on e.employeeid = se.UID where SequenceModeID=@SequenceModeID and se.CreateDate  like '" + first + "%' GROUP BY s.shopid ";
            
            
            return mysql.ExecuteDataTable(strQuery);

        }
        
        public DataTable GetshopsellinfoByShopId(String first, int shopId)
        {
            mysql.parameters.Clear();
            mysql.parameters.AddWithValue("SequenceModeID", "B");
            mysql.parameters.AddWithValue("shopId", shopId);
            strQuery = "select s.shopname,s.shopid,se.CreateDate,e.EmployeeID,e.EName ,(s.ShopBonus+s.EmployeeBonus)as ShopBonus,sum(se.price) as price,(se.price * (s.shopbonus + s.EmployeeBonus))/100 as saleprice from shopinfo s left join employee e on s.ShopID=e.ShopID left join sequence se on e.employeeid = se.UID where SequenceModeID=@SequenceModeID and se.CreateDate  like '" + first + "%' and s.shopid=@shopId ";


            return mysql.ExecuteDataTable(strQuery);

        }
        
        public DataTable GetEmData(string UID) {
            mysql.parameters.Clear();
            mysql.parameters.AddWithValue("GID", GID);
            mysql.parameters.AddWithValue("id", UID);
            strQuery = "SELECT Name,KeyString,Check_Key,Phone,Email,Competence,BankAccount,f.ShopBonus,f.Refund ";
            strQuery += " FROM userinfo u LEFT JOIN gamefee f ON f.UID=u.UID ";
            strQuery += " where u.UID=@id AND f.GID=@GID ";

            return mysql.ExecuteDataTable(strQuery);
        }

        public void EmpautUpdate()
        {
            mysql.parameters.Clear();
            mysql.parameters.AddWithValue("ShopID", shopID);
            mysql.parameters.AddWithValue("ShopName", ShopName);
            mysql.parameters.AddWithValue("ShopAdd", ShopAdd);
            mysql.parameters.AddWithValue("ShopTel", ShopTel);
            mysql.parameters.AddWithValue("ShopIP", ShopIP);
            mysql.parameters.AddWithValue("ShopBonus", ShopBonus);
            mysql.parameters.AddWithValue("refund", Refund);
            mysql.parameters.AddWithValue("EmployeeBonus", EmployeeBonus);
            mysql.parameters.AddWithValue("Resignation", Resignation);
            mysql.parameters.AddWithValue("GID", GID);

            strQuery = @"UPDATE userinfo SET ShopName=@ShopName,ShopAddress=@ShopAdd,ShopPhone=@ShopTel,Resignation=@Resignation WHERE UID=@ShopID;";
            strQuery += " UPDATE gamefee SET ShopBonus=@ShopBonus ,Refund=@refund WHERE UID=@ShopID AND GID=@GID;";

            mysql.Execute(strQuery);

        }
        public void ShopDelete()
        {
            mysql.parameters.Clear();
            mysql.parameters.AddWithValue("ShopID", shopID);
           

           
            strQuery = "Update shopinfo set Resignation='1' Where ShopID= @ShopID";
            mysql.Execute(strQuery);

        }

        
    }
}