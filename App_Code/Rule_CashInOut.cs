﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using GameLibrary;
using Ssm;
using Tool;

/// <summary>
/// 處理開洗分現金及信用
/// </summary>

enum Pop_Type {
    CASHIN,
    CASHOUT,
    SETCREDIT
}

public static class SequenceMode {
    public static string Cash = "E";
    public static string Credit = "F";

    public static bool Exist(string val) {
        return val == Cash || val == Credit;
    }
}

public static class Rule_Shared {
    /*欄位Key值*/
    public static string Col_CreditMax = "CreditMax";
    public static string Col_CreditRemain = "CreditRemain";

    public static int placeId = 34;

    public static bool IsValid_CashCredit(Player LoginPlayer, Player TargetPlayer, string value, out string reason) {
        bool IsValid = false;
        reason = "";

        if (Ban.IsBan(TargetPlayer) || Ban.IsBan(LoginPlayer)) {
            reason = "玩家已被停權";
            return IsValid;
        }

        if (value != "") {
            if (!IsLong(value)) {
                reason = "輸入值格式錯誤";
                return IsValid;
            }
        }

        IsValid = true;

        return IsValid;
    }

    public static string InsertToSequence(IDataBase sql, Player targetPlayer, Player LoginPlayer, long price, string GID, string SequenceMode) {
        if (price == 0)
            return "";
        double shopbonus = SQLAction.GetShopBonusByID(sql, GID, targetPlayer.ID);

        double agentBonus = price * 1.0 * shopbonus;
        double companyBonus = price * 1.0 * (1 - shopbonus);
        long creditremain = GetCreditRemain(targetPlayer);
        long creditmax = GetCreditMax(targetPlayer);

        long point = price;

        string bomustree = GetBonusTree(sql, targetPlayer, LoginPlayer);

        sql.ClearParameters();
        var cmd = CommandBuilder.Instance;
        sql.TryAddParameter("price", price);
        sql.TryAddParameter("point", point);
        sql.TryAddParameter("bomustree", bomustree);
        cmd.INSERT_INTO("sequence", sql).addColumnAndValue(
            "SequenceID", "cash",
            "Price", price,
            "Point", point,
            "UID", targetPlayer.ID,
            "SequencePlaceID", Rule_Shared.placeId,
            "SequenceModeID", SequenceMode,
            "agentbonus", agentBonus,
            "companybonus", companyBonus,
            "creditmax",creditmax,
            "creditremain", creditremain,
            "bonustree", bomustree)
            .addTime("CreateDate");

        string str = cmd.Result;
        str += "SELECT LAST_INSERT_ID();";
        string result = sql.ExecuteScalar(str).ToString();

        cmd.Free();

        return result;
    }

    public static bool IsLong(string val) {
        long tmp;
        return long.TryParse(val, out tmp);
    }

    public static long GetCreditMax(Player p) {
        return Convert.ToInt64(p[Rule_Shared.Col_CreditMax]);
    }

    public static long GetCreditRemain(Player p) {
        return Convert.ToInt64(p[Rule_Shared.Col_CreditRemain]);
    }

    public static string GetFixedGID() {
        string GID = "0";
        if (System.Web.Configuration.WebConfigurationManager.AppSettings["FixedGID"] != null) {
            GID = System.Web.Configuration.WebConfigurationManager.AppSettings["FixedGID"];
        }

        return GID;
    }

    public static string GetBonusTree(IDataBase db ,Player p, Player loginplayer) {
        return GetBonusTree(db, p["UpLevelTree"].ToString(), loginplayer);        
    }

    public static string GetBonusTree(IDataBase db, string upleveltree, Player loginplayer) {
        string result = "";
        db.ClearParameters();
        string[] tmp = upleveltree.Split(',');
        List<string> UID = new List<string>();


        for (int i = 0; i < tmp.Length; i++) {
            if (tmp[i] != "" && tmp[i] != "0") {
                UID.Add("UID =" + tmp[i]);
            }
        }

        if (UID.Count == 1 && ShowData_Permission.isAdmin(loginplayer)) {
            return "100";
        }

        db.TryAddParameter("GID", GetFixedGID());
        string str = "SELECT UID,ShopBonus FROM gamefee f WHERE GID=@GID AND ";
        str += "(" + String.Join(" OR ", UID) + ")";
        DataTable dt = db.ExecuteDataTable(str);
        string[] tmp2 = new string[UID.Count];
        int index;
        double sum = 100, predata = 0;
        for (int i = dt.Rows.Count - 1; i >= 0; i--) {
            index = Array.IndexOf(tmp, dt.Rows[i]["UID"].ToString());
            if (index != -1) {
                index -= 2;
                tmp2[index] = Convert.ToString(Convert.ToDouble(dt.Rows[i]["ShopBonus"]) - predata);
                predata = Convert.ToDouble(dt.Rows[i]["ShopBonus"]);
            }
        }

        result = String.Join(",", tmp2);
        return result;
    }
}


public static class Rule_SetCredit {
    public static bool IsValidSetCredit(Player LoginPlayer, Player TargetPlayer, string price, out string reason) {
        reason = "";
        if (price == "") {
            reason = "請輸入有效數字";
            return false;
        }

        long value = Convert.ToInt64(price);
        //if (value == 0) {
        //    reason = "輸入數字不可為0";
        //    return false;
        //}

        if (!ShowData_Permission.isCompany(LoginPlayer)) {
            reason = "登入帳號非公司層級";
            return false;
        }

        long targetcreditremain = Rule_Shared.GetCreditRemain(TargetPlayer);

        if (value < 0) {
            /*minus*/
            long tmp = -value;
            if (targetcreditremain < tmp) {
                reason = "信用餘額不足";
                return false;
            }
        }

        bool isAdmin = ShowData_Permission.isAdmin(LoginPlayer);

        if (isAdmin) {
            return true;
        }

        long loginplayercredit = Rule_Shared.GetCreditMax(LoginPlayer);

        if (value > 0 && loginplayercredit < value) {
            /*add*/
            reason = "信用額度不足";
            return false;
        }

        reason = "設定錯誤";

        return true;
    }

    public static bool IsValidSetCredit(Player LoginPlayer, string price, out string reason) {
        reason = "";
        if (price == "") {
            reason = "請輸入有效數字";
            return false;
        }

        long value = Convert.ToInt64(price);
        //if (value == 0) {
        //    reason = "輸入數字不可為0";
        //    return false;
        //}

        if (!ShowData_Permission.isCompany(LoginPlayer)) {
            reason = "登入帳號非公司層級";
            return false;
        }
        
        bool isAdmin = ShowData_Permission.isAdmin(LoginPlayer);

        if (isAdmin) {
            return true;
        }

        long loginplayercredit = Rule_Shared.GetCreditMax(LoginPlayer);

        if (value > 0 && loginplayercredit < value) {
            /*add*/
            reason = "信用額度不足";
            return false;
        }

        reason = "設定錯誤";

        return true;
    }

    public static void Excute(IDataBase sql, Player LoginPlayer, Player targetPlayer, long price) {        
        UpdateCredit(sql, LoginPlayer, targetPlayer, price);
    }

    static void UpdateCredit(IDataBase sql, Player LoginPlayer, Player targetPlayer, long price) {
        if (price == 0)
            return;
        sql.ClearParameters();
        sql.TryAddParameter("credit", price);
        string sqlcmd = "UPDATE userinfo SET CreditMax=(CreditMax+@credit),CreditRemain=(CreditRemain+@credit) WHERE UID=" + targetPlayer.ID + ";";
        if (!ShowData_Permission.isAdmin(LoginPlayer)) {
            //LoginPlayer[Rule_Shared.Col_CreditMax] = Rule_Shared.GetCreditMax(LoginPlayer) - price;
            LoginPlayer[Rule_Shared.Col_CreditRemain] = Rule_Shared.GetCreditRemain(LoginPlayer) - price;
            sqlcmd += "UPDATE userinfo SET CreditRemain=(CreditRemain-@credit) WHERE UID=" + LoginPlayer.ID;
        }
        else
        {
            LoginPlayer[Rule_Shared.Col_CreditMax] = Rule_Shared.GetCreditMax(LoginPlayer) + price;
            sqlcmd += "UPDATE userinfo SET CreditMax=(CreditMax+@credit) WHERE UID=" + LoginPlayer.ID;
        }
        sql.Execute(sqlcmd);
    }

     public static void UpdateCredit(IDataBase sql, Player LoginPlayer, long price) {
        sql.ClearParameters();
        sql.TryAddParameter("credit", price);

        if (!ShowData_Permission.isAdmin(LoginPlayer)) { 
             //LoginPlayer[Rule_Shared.Col_CreditMax] = Rule_Shared.GetCreditMax(LoginPlayer) - price;
             LoginPlayer[Rule_Shared.Col_CreditRemain] = Rule_Shared.GetCreditRemain(LoginPlayer) - price;
             string sqlcmd = "UPDATE userinfo SET CreditRemain=(CreditRemain-@credit) WHERE UID=" + LoginPlayer.ID;

             sql.Execute(sqlcmd);
        }
        else
        {
            LoginPlayer[Rule_Shared.Col_CreditMax] = Rule_Shared.GetCreditMax(LoginPlayer) + price;
            string sqlcmd = "UPDATE userinfo SET CreditMax=(CreditMax+@credit) WHERE UID=" + LoginPlayer.ID;

            sql.Execute(sqlcmd);
        }
    }
}

public static class Rule_CashIn {
    public static bool IsValid_CashIn(Player LoginPlayer, Player TargetPlayer, string price, string CashInType, out string reason) {
        bool IsValid = false;
        reason = "";

        if (!SequenceMode.Exist(CashInType)) {
            reason = "資料錯誤";
            return false;
        }

        if (price == "") {
            reason = "請輸入有效數字";
            return false;
        }

        long value = Convert.ToInt64(price);
        if (value <= 0) {
            reason = "請輸入有效數字(輸入數字不可為0,且不可為負)";
            return false;
        }

        if (!ShowData_Permission.Group_Member(LoginPlayer, TargetPlayer)) {
            reason = "帳號非為本下線代理商或會員!!";
            return IsValid;
        }

        if (CashInType == SequenceMode.Credit) {
            /*檢查信用餘額*/
            long creditremain = Rule_Shared.GetCreditRemain(TargetPlayer);
            if (creditremain < value) {
                reason = "玩家信用餘額不足";
                return IsValid;
            }
        }

        IsValid = true;

        return IsValid;
    }

}

public static class Rule_CheckOut {

    public enum Field {
        Name,
        UID,
        credit,
        creditremain, 
        ud, 
        credit_out, 
        credit_return, 
        gamerefund, 
        member_sum,
        cash_sum,
        cash_flow
    }    

    public static bool Excute(IDataBase sql, Player p, out string reason) {
        /*Get List*/
        DataTable dt = GetAllUserCheckOutList(sql, p);

        reason = "";

        if (dt.Rows.Count == 0) {
            reason = "無資料需結帳!";
            return false;
        }

        long list_id;

        /*Insert Record to DB*/
        if (!InsertCheckOutRecord(sql, dt, out list_id)) {
            reason = "結帳錯誤!(資料insert error)";
            return false;
        }
        
        /*結帳*/
        ExcuteCashOut(sql, dt, list_id, p);

        reason = "結帳成功!";
        return true;
    }

    public static void ExcuteCashOut(IDataBase sql, DataTable dt, long list_id, Player p) {
        /*Step1*/
        dt = AddGameRefundToUD(dt);

        /*Step2*/
        dt = InsertToOperationRecordAndSequence(sql, dt, p);

        /*Step3 Reset record*/
        ResetUserCheckOutInfo(sql, dt);

        UpdateSuccessAndEndTime(sql, list_id);
    }
        
    #region 結帳流程
    public static DataTable AddGameRefundToUD(DataTable dt) {
        int refund_index = dt.Columns.IndexOf(Field.gamerefund.ToString()),
            ud_index = dt.Columns.IndexOf(Field.ud.ToString());

        if (refund_index == -1 || ud_index == -1)
            return dt;

        for (int i = 0; i < dt.Rows.Count; i++) {            
            if (Convert.ToDouble(dt.Rows[i][refund_index]) != 0) {
                dt.Rows[i][ud_index] = Convert.ToDouble(dt.Rows[i][ud_index]) + Convert.ToDouble(dt.Rows[i][refund_index]);
            }
        }

        return dt;
    }

    static DataTable InsertToOperationRecordAndSequence(IDataBase sql, DataTable dt, Player loginPlayer) {
        int sum_index = dt.Columns.IndexOf(Field.member_sum.ToString()),
            uid_index = dt.Columns.IndexOf(Field.UID.ToString());

        if (sum_index == -1)
            return dt;

        Player targetPlayer;
        string GID = Rule_Shared.GetFixedGID();
        
        for (int i = 0; i < dt.Rows.Count; i++) {
            if (NotCheckOut(dt.Rows[i])) {
                targetPlayer = new Player();
                if (Member.GetPlayerData(sql, Convert.ToInt32(dt.Rows[i][uid_index]), targetPlayer)) {
                    ExcuteInsertToOperationRecordAndSequence(sql, dt.Rows[i], loginPlayer, targetPlayer, GID);                    
                }
            }
        }

        return dt;
    }

    public static void ExcuteInsertToOperationRecordAndSequence(IDataBase sql, DataRow dr, Player loginPlayer, Player targetPlayer, string GID) {
        int sum_index = dr.Table.Columns.IndexOf(Field.member_sum.ToString());
        int credit_index = dr.Table.Columns.IndexOf(Field.credit.ToString());
        int creditremain_index = dr.Table.Columns.IndexOf(Field.creditremain.ToString());

        if (sum_index == -1 || credit_index == -1 || creditremain_index == -1)
            return;

        /*Insert 洗分紀錄*/
        long tmp = Convert.ToInt64(dr[credit_index]) - Convert.ToInt64(dr[creditremain_index]);
        Rule_Shared.InsertToSequence(sql, targetPlayer, loginPlayer,
            -tmp, GID, SequenceMode.Credit);

        long Price = Convert.ToInt64(dr[sum_index]);
        
        /*Insert Operation Record*/
        InsertOperationRecord(sql, targetPlayer, Price);        
    }

    public static void ResetUserCheckOutInfo(IDataBase sql, DataTable dt) {
        //int uid_index = dt.Columns.IndexOf(Field.UID.ToString());

        //if (uid_index == -1)
        //    return;

        /*Get List*/
        //List<string> uidList = new List<string>(), whereList = new List<string>();
        //for (int i = 0; i < dt.Rows.Count; i++) {
        //    uidList.Add(dt.Rows[i][uid_index].ToString());
        //}

        //if (uidList.Count == 0)
        //    return;

        sql.ClearParameters();
        /*Update Record*/
        string str = "UPDATE userinfo SET BankCredit=0,CreditRemain=CreditMax WHERE canAddNextLevel=0";
        //for (int i = 0; i < uidList.Count; i++) {
        //    whereList.Add("UID=@UID" + i);
        //    sql.TryAddParameter("UID" + i, uidList[i]);
        //}
        //str += whereList.Join(" OR ");
        str += ";";

        /*Update game Refund*/
        str += "UPDATE refund_close SET refund=0  ";
        //str += whereList.Join(" OR ");

        sql.Execute(str);
    }
        
    public static void restore(IDataBase sql) {
        DataTable dt = sql.ExecuteDataTable("SELECT * FROM report_CheckOutDetail WHERE report_CheckOutList_id=14");
        
        int uid_index = dt.Columns.IndexOf(Field.UID.ToString()),
            credit_index = dt.Columns.IndexOf(Field.credit.ToString()),
            ud_index = dt.Columns.IndexOf(Field.ud.ToString()),
            credit_remain_index = dt.Columns.IndexOf(Field.creditremain.ToString()),
            refund_index = dt.Columns.IndexOf(Field.gamerefund.ToString());

        sql.ClearParameters();
        string str = "";
        /*Get List*/
        List<string> uidList = new List<string>(), whereList = new List<string>();
        for (int i = 0; i < dt.Rows.Count; i++) {
            str += "UPDATE userinfo SET BankCredit=@BankCredit" + i + ",CreditMax=@CreditMax" + i + ",CreditRemain=@CreditRemain"+i+" WHERE UID=@UID" + i + ";";
            sql.TryAddParameter("UID" + i, dt.Rows[i][uid_index].ToString());
            sql.TryAddParameter("BankCredit" + i, dt.Rows[i][ud_index].ToString());
            sql.TryAddParameter("CreditMax" + i, dt.Rows[i][credit_index].ToString());
            sql.TryAddParameter("CreditRemain" + i, dt.Rows[i][credit_remain_index].ToString());

            str += "UPDATE refund_close SET refund=@refund" + i + " WHERE UID=@UID" + i + ";";
            sql.TryAddParameter("refund" + i, dt.Rows[i][refund_index].ToString());
        }
        
        sql.Execute(str);
    }
    #endregion

    public static DataTable GetUserCheckOut(IDataBase sql, DataTable info, int UID) {        
        sql.ClearParameters();

        string GID = Rule_Shared.GetFixedGID();

        var cmd = CommandBuilder.Instance;

        SelectCmd_CheckOut(cmd);

        if (info.Rows[0]["canAddNextLevel"].ToString() == "0")
            cmd.WHERE_condition("r.GID=@GID");
        cmd.WHERE_condition("u.UID=@uid");

        sql.TryAddParameter("uid", UID);
        sql.TryAddParameter("GID", GID);

        DataTable dt = sql.ExecuteDataTable(cmd.Result);
        cmd.Free();

        return dt;
    }
       

    public static DataTable GetAllUserCheckOutList(IDataBase sql, Player p) {
        DataTable lowerlist = SQLAction.GetAllLowerCompanyMemberList(sql, p);
        if (lowerlist.Rows.Count == 0)
            return new DataTable();

        sql.ClearParameters();

        string GID = Rule_Shared.GetFixedGID();

        List<string> uidList = new List<string>();
        for (int i = 0; i < lowerlist.Rows.Count; i++) {
            uidList.Add("u.UID=@uid" + i);
            sql.TryAddParameter("uid" + i, lowerlist.Rows[i]["UID"].ToString());
        }

        var cmd = CommandBuilder.Instance;

        SelectCmd_CheckOut(cmd);

        cmd.WHERE_condition("("+uidList.Join(" OR ")+")");
        cmd.WHERE_condition("r.GID=@GID");

        sql.TryAddParameter("GID", GID);

        DataTable dt = sql.ExecuteDataTable(cmd.Result);
        cmd.Free();

        return dt;
    }

    public static DataTable GetAllUserCheckOutListRemoveZero(IDataBase sql, Player p) {
        DataTable dt = GetAllUserCheckOutList(sql, p);
        
        return GetAllUserCheckOutListRemoveZero(dt);
    }

    public static DataTable GetAllUserCheckOutListRemoveZero(DataTable dt) {        
        DataTable result = dt.Clone();

        /*remove (不用結帳)*/
        for (int i = 0; i < dt.Rows.Count; i++) {
            if (NotCheckOut(dt.Rows[i])) {
                result.ImportRow(dt.Rows[i]);
            }
        }

        return result;
    }

    public static bool NotCheckOut(DataRow dr) {
        return Convert.ToDouble(dr[Field.cash_sum.ToString()]) != 0 ||
                Convert.ToDouble(dr[Field.ud.ToString()]) != 0 ||
                Convert.ToDouble(dr[Field.credit.ToString()]) != Convert.ToDouble(dr[Field.creditremain.ToString()])||
                Convert.ToDouble(dr[Field.gamerefund.ToString()]) != 0;
    }

    public static DataTable GetAllUserCheckOutList_History(IDataBase sql, Player p, string start, string end, string searchName) {
        DataTable lowerlist = SQLAction.GetAllLowerCompanyMemberList(sql, p);
        if (lowerlist.Rows.Count == 0)
            return new DataTable();

        sql.ClearParameters();

        string GID = Rule_Shared.GetFixedGID();

        List<string> uidList = new List<string>();
        for (int i = 0; i < lowerlist.Rows.Count; i++) {
            uidList.Add("u.UID=@uid" + i);
            sql.TryAddParameter("uid" + i, lowerlist.Rows[i]["UID"].ToString());
        }

        var cmd = CommandBuilder.Instance;

        cmd.addTable("report_CheckOutDetail", "d")
            .addTable("report_CheckOutList", "l")
            .addTable("userinfo","u");

        cmd.SELECT("l.Start")
            .SELECT("u.NickName AS name")
           // .SELECT("IFNULL((SELECT SUM(s.Price) FROM sequence s WHERE s.UID=d.UID AND (s.CreateDate BETWEEN @start AND @end) AND s.SequenceModeID='E'),0) AS cash_sum")
            .SELECT("d.*")            
            .FROM("d")
            .LEFT_JOIN("l", "l.id=d.report_CheckOutList_id")
            .LEFT_JOIN("u", "u.UID=d.UID");

        cmd.WHERE_condition("("+uidList.Join(" OR ")+")");
        cmd.WHERE_condition("l.CreateDate BETWEEN @start AND @end");

        if (searchName != "") {
            cmd.WHERE_condition("u.KeyString=@name");
            sql.TryAddParameter("name", searchName);
        }
        start = Convert.ToDateTime(start).AddSeconds(1).ToString("yyyy/MM/dd HH:mm:ss");
        sql.TryAddParameter("end", end);
        sql.TryAddParameter("start", start);

        DataTable dt = sql.ExecuteDataTable(cmd.Result);
        cmd.Free();

        return dt;
    }

    static CommandBuilder SelectCmd_CheckOut(CommandBuilder cmd) {
        DataTable dt;
        using (var sql = new MySQL("unidb")) {
            dt = GetLatestCashOutList(sql);
        }

        string str = "", cashstr = "";

        if (dt.Rows.Count > 0) {
            str += " AND s.CreateDate > " + "'" + Convert.ToDateTime(dt.Rows[0]["CreateDate"]).ToString("yyyy/MM/dd HH:mm:ss") + "' ";
            cashstr+=" AND c.time>" + "'" + Convert.ToDateTime(dt.Rows[0]["CreateDate"]).ToString("yyyy/MM/dd HH:mm:ss") + "' ";
        }

        cmd.addTable("userinfo", "u").addTable("refund_close", "r");
        cmd.SELECT("u.NickName AS name", "u.UID", "u.CreditMax AS credit", "u.CreditRemain AS creditremain",
            "u.BankCredit AS ud",
            "(CASE WHEN (u.BankCredit+IFNULL(r.refund,0) -u.CreditMax+u.CreditRemain)>0 THEN u.BankCredit+IFNULL(r.refund,0) -u.CreditMax+u.CreditRemain ELSE 0 END) AS credit_out",
            "(CASE WHEN (u.BankCredit >u.CreditMax-u.CreditRemain) THEN u.CreditMax-u.CreditRemain ELSE u.BankCredit END) AS credit_return",
            "IFNULL(r.refund,0) AS gamerefund",
            "(u.BankCredit+IFNULL(r.refund,0) -u.CreditMax+u.CreditRemain) AS member_sum",
            "IFNULL((SELECT SUM(s.Price) FROM sequence s WHERE s.UID=u.UID " + str + " AND s.SequenceModeID='E'),0) AS cash_sum",
            "IFNULL((SELECT -SUM(c.amount) FROM cashflow c WHERE c.UID=u.UID " + cashstr + " AND c.amount<0),0) AS cash_flow"
            ).FROM("u")
            .LEFT_JOIN("r", "r.uid=u.UID");
        return cmd;
    }
       

    static bool InsertCheckOutRecord(IDataBase sql, DataTable dt, out long list_id) {
        sql.ClearParameters();

        if (!InsertCheckOutList(sql, out list_id)) {
            return false;
        }

        bool Success = InsertCheckOutDetail(sql, dt, list_id);

        if (!Success) {
            /*結帳失敗,刪除list資料*/
            DeleteList(sql, list_id);
        }

        return Success;
    }

    public static bool InsertCheckOutDetail(IDataBase sql, DataTable dt, long list_id) {
        /*Skip Name*/
        string[] list = ((string[])Enum.GetNames(typeof(Field))).Skip(1).ToArray();

        Dictionary<int, double[]> val = new Dictionary<int, double[]>();
        double[] tmp;
        bool Success = true;
        int index;
        for (int i = 0; i < dt.Rows.Count; i++) {
            if (!Success)
                break;

            tmp = new double[list.Length];

            for (int j = 0; j < list.Length; j++) {
                if (!Success)
                    break;
                index = dt.Columns.IndexOf(list[j]);
                if (index == -1) {
                    Success = false;
                    break;
                }

                tmp[j] = Convert.ToDouble(dt.Rows[i][index]);
            }

            val.Add(i, tmp);
        }
        
        sql.ClearParameters();
        sql.TryAddParameter("id", list_id);

        string str = "";
        string[] tmp_list;

        string checkoutTime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:00");

        sql.TryAddParameter("createdate", checkoutTime);

        foreach (var item in val) {
            tmp_list = new string[list.Length];
            for (int i = 0; i < list.Length; i++) {
                tmp_list[i] = "@" + list[i] + item.Key;
                sql.TryAddParameter(tmp_list[i], item.Value[i]);
            }

            str += "INSERT INTO report_CheckOutDetail (report_CheckOutList_id," + list.Join(",") + ",CreateDate)";
            str += " VALUES (@id," + tmp_list.Join(",") + ",@createdate);";
        }

        try {
            sql.Execute(str);
        } catch (Exception) {
            Success = false;
        }

        return Success;
    }

    public static bool InsertCheckOutList(IDataBase sql, out long list_id) {
        sql.ClearParameters();
        list_id = 0;
        DateTime end = DateTime.Now;
        string now = DateTime.Now.ToString("yyyy-MM-dd HH:mm:00");

        /*Start 先 select last record*/
        DataTable LatestCashOutList = GetLatestCashOutList(sql);

        if (LatestCashOutList.Rows.Count != 0)
            end = Convert.ToDateTime(LatestCashOutList.Rows[0]["End"]);

        sql.TryAddParameter("nowtime", now);
        sql.TryAddParameter("start", end);
        string str = "INSERT INTO report_CheckOutList (CreateDate,Start,End) VALUES (@nowtime,@start,@nowtime);SELECT LAST_INSERT_ID();";

        try {
            list_id = Convert.ToInt64(sql.ExecuteScalar(str));
        } catch (Exception) {
            return false;
        }

        return true;
    }

    public static DataTable GetLatestCashOutList(IDataBase sql) {
        return sql.ExecuteDataTable("SELECT * FROM report_CheckOutList ORDER BY id DESC LIMIT 1");
    }

    static void DeleteList(IDataBase sql, long id) {        
        sql.ClearParameters();
        sql.TryAddParameter("id", id);
        sql.Execute("DELETE FROM report_CheckOutList WHERE id=@id");
    }

    static void UpdateSuccessAndEndTime(IDataBase sql, long list_id) {
        sql.ClearParameters();
        sql.TryAddParameter("id", list_id);
        sql.Execute("UPDATE report_CheckOutList SET Success=1,End=now(),CreateDate=now() WHERE id=@id");
    }

    static void InsertOperationRecord(IDataBase sql, Player p, long Price) {
        sql.ClearParameters();
        sql.TryAddParameter("uid", p.ID);        
        sql.TryAddParameter("currency", p["Currency"]);
        string str = "INSERT operationrecord (UID," + (Price > 0 ? "payment" : "receipt") + ",currency,createDate) ";
        str += " VALUES (@uid,@price,@currency,now())";

        sql.TryAddParameter("price", Price > 0 ? Price : -Price);
        sql.Execute(str);
    }
}