﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// User 的摘要描述
/// </summary>
public class UserInfo : BaseClass
{
    public int UID { get; set; }
    public string NickName { get; set; }
    public bool IsLogin { get; set; }
    public string Phone { get; set; }
    public string Email { get; set; }
    public string Address { get; set; }
    public string autcode { get; set; }

    public string InviteCode { get; set; }

    public string upLevelTree { get; set; }

	public UserInfo()
	{
        IsLogin = false;
		//
		// TODO: 在這裡新增建構函式邏輯
		//
	}
}