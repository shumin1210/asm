﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using GameLibrary;
using Col_Trans = DataStruct.Col_Trans;

namespace Operation.Procedures {    
    public class TRANSLIST : Operation.operation {
        DataStruct.DataType col = DataStruct.DataType.TRANS;
        public DataStruct.DataType Col {
            get { return col; }
        }

        public DataTable Step1(DataTable target, Player p) {
            DataRow r;
            if (DataStruct.SetDefaultVal(Col, target, out r)) {
                r[(int)Col_Trans.Currency] = p["Currency"];
                r[(int)Col_Trans.Status] = Ban.getIntStatus(p);
                

                DataTable bonusInfo = Ssm.SQLAction.GetBonusInfo(new MySQL("unidb"), Rule_Shared.GetFixedGID(), p.ID);

                if (bonusInfo.Rows.Count == 0) {
                    r[(int)Col_Trans.ShopBonus] = 0;
                }else
                    r[(int)Col_Trans.ShopBonus] = bonusInfo.Rows[0]["ShopBonus"];

                target.Rows.Add(r);
            }

            return target;
        }

        public void Step2(DataTable dt, DataRow total) {
            dt.Rows[0][(int)Col_Trans.Price] = Convert.ToDouble(dt.Rows[0][(int)Col_Trans.Price]) + Convert.ToDouble(total[(int)Col_Trans.Price]);
            dt.Rows[0][(int)Col_Trans.UD] = Convert.ToDouble(dt.Rows[0][(int)Col_Trans.UD]) + Convert.ToDouble(total[(int)Col_Trans.UD]);
            dt.Rows[0][(int)Col_Trans.Cash] = Convert.ToInt64(dt.Rows[0][(int)Col_Trans.Cash]) + Convert.ToInt64(total[(int)Col_Trans.Cash]);
            dt.Rows[0][(int)Col_Trans.Credit] = Convert.ToInt64(dt.Rows[0][(int)Col_Trans.Credit]) + Convert.ToInt64(total[(int)Col_Trans.Credit]);
            //TODO:是否加退水
            dt.Rows[0][(int)Col_Trans.Refund] = 0;
            //dt.Rows[0]["Refund"] = Convert.ToDouble(dt.Rows[0]["Refund"]) + Convert.ToDouble(total.Rows[i]["Refund"]);
            dt.Rows[0][(int)Col_Trans.AgentBonus] = Convert.ToDouble(dt.Rows[0][(int)Col_Trans.AgentBonus]) + Convert.ToDouble(total[(int)Col_Trans.AgentBonus]);
            dt.Rows[0][(int)Col_Trans.CompanyBonus] = Convert.ToDouble(dt.Rows[0][(int)Col_Trans.CompanyBonus]) + Convert.ToDouble(total[(int)Col_Trans.CompanyBonus]);

            dt.Rows[0][(int)Col_Trans.CreditMax] = Convert.ToDouble(dt.Rows[0][(int)Col_Trans.CreditMax]) + Convert.ToDouble(total[(int)Col_Trans.CreditMax]);
        }

        public DataView Sub_Step1(DataView target, DataTable record, int index, DataRowView playerRow) {
            UpperLevelRecord.Add(target, record, index, playerRow);
            
            return target;
        }

        public DataRowView UpdateFixedRecord_Member(DataRowView target, DataRowView record, Player p) {
            DataTable bonusInfo = Ssm.SQLAction.GetBonusInfo(new MySQL("unidb"), Rule_Shared.GetFixedGID(), p.ID);

            if (bonusInfo.Rows.Count == 0) {
                target[(int)Col_Trans.ShopBonus] = 0;
            } else
                target[(int)Col_Trans.ShopBonus] = 100 - Convert.ToDouble(bonusInfo.Rows[0]["ShopBonus"]);

            return target;
        }

        public DataView LayerOneOtherFunc(DataView target, DataTable record, Player p, DataView lowerpList) {
            /*Initial member cost*/
            int k;
            string upLevel;
            for (int i = 0; i < target.Count; i++) {
                target[i][(int)Col_Trans.MemberCost] = 0;
                target[i][(int)Col_Trans.Cash] = 0;
                target[i][(int)Col_Trans.Credit] = 0;

                k = lowerpList.Find(target[i][(int)Col_Trans.UserID]);

                if (k == -1)
                    continue;
                target[i][(int)Col_Trans.ShopBonus] = 100 - Convert.ToDouble(lowerpList[k]["ShopBonus"]);
            }

            for (int i = 0; i < record.Rows.Count; i++) {
                k = lowerpList.Find(record.Rows[i][(int)Col_Trans.UserID]);

                upLevel = lowerpList[k]["UpLevelId"].ToString();

                if (ShowData_Permission.isNormalMember(lowerpList[k])) {
                    /*只加自己會員的資料*/
                    k = target.Find(upLevel);
                    if (k == -1)
                        continue;

                    target[k][(int)Col_Trans.MemberCost] = Convert.ToInt64(target[k][(int)Col_Trans.MemberCost]) + Convert.ToInt64(record.Rows[i][(int)Col_Trans.MemberCost]);
                    target[k][(int)Col_Trans.Cash] = Convert.ToInt64(target[k][(int)Col_Trans.Cash]) + Convert.ToInt64(record.Rows[i][(int)Col_Trans.Cash]);
                    target[k][(int)Col_Trans.Credit] = Convert.ToInt64(target[k][(int)Col_Trans.Credit]) + Convert.ToInt64(record.Rows[i][(int)Col_Trans.Credit]);                
                }
            }

            return target;
        }

        public DataTable LayerOneTopDataFunc(DataTable target, DataTable record, Player p, DataView lowerpList) {
            if (target.Rows.Count == 0)
                return target;

            int k;
            string upLevel;

            target.Rows[0][(int)Col_Trans.ShopBonus] = 100 - Convert.ToDouble(target.Rows[0][(int)Col_Trans.ShopBonus]);
            
            target.Rows[0][(int)Col_Trans.MemberCost] = 0;
            /*計算會員消費*/
            for (int i = 0; i < record.Rows.Count; i++) {
                k = lowerpList.Find(record.Rows[i][(int)Col_Trans.UserID]);
                upLevel = lowerpList[k]["UpLevelId"].ToString();

                if (upLevel == p.ID.ToString() && ShowData_Permission.isNormalMember(lowerpList[k])) {
                    target.Rows[0][(int)Col_Trans.MemberCost] = Convert.ToDouble(target.Rows[0][(int)Col_Trans.MemberCost]) + Convert.ToInt64(record.Rows[i][(int)Col_Trans.MemberCost]);
                }
            }

            return target;
        }

        public DataView LayerTwoOtherFunc(DataView target, DataTable record, Player p, DataView lowerpList) {            
            return target;
        }
    }

}