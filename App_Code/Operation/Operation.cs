﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using GameLibrary;

/// <summary>
/// Operation 的摘要描述
/// </summary>
namespace Operation {
    public static class Flow {

        public static string RecordSortCol = "UserID";

        public static bool Main(DataTable total, DataTable pList, Player p, int Type, DataStruct.DataType datatype, out DataTable Top_UserData, out DataTable Lower_UserData) {
            var op = Procedures.Operation.GetInstance(datatype);

            DataTable dt = DataStruct.NewTable(datatype);
            Top_UserData = Lower_UserData = dt;

            if (op == null) {
                return false;
            }

            //step1
            op.Step1(dt, p);

            DataView playerList = new DataView(pList);

            string upLevelTree = "", upLevel;
            int k;

            playerList.Sort = "uid";

            DataRow r;
            DataView result = new DataView(DataStruct.NewTable(datatype));
            result.Sort = RecordSortCol;
            
            for (int i = 0; i < total.Rows.Count; i++) {
                k = playerList.Find(total.Rows[i][RecordSortCol]);

                if (k == -1)
                    continue;

                upLevelTree = playerList[k]["UpLevelTree"].ToString();
                if (p["UpLevelTree"].ToString() != "")
                    upLevelTree = upLevelTree.Replace(p["UpLevelTree"].ToString(), "");

                string[] tmp = upLevelTree.Split(',');

                upLevel = tmp[0];
                /*Top_UserData --------------------------------*/
                if (ShowData_Permission.Group_Member(playerList[k], p, upLevel) && tmp.Length == 2) {
                    /*Step2*/
                    op.Step2(dt, total.Rows[i]);
                }
                /*--------------------------------*/

                /*Lower_UserData --------------------------------*/
                if (upLevel == "") {
                    //自己的資料
                    UpperLevelRecord.Add(result, total, i, p.ID.ToString(), p.Name, p["Currency"], (int)p["Status"]);

                    continue;
                } else if (tmp.Length >= 2) {
                    if (tmp[1] == "") {
                        if (ShowData_Permission.isAll(playerList[k], Type))
                            UpperLevelRecord.Add(result, total, i, playerList[k]);
                    } else {

                        UpperLevelRecord.Add(result, total, i, tmp[1], "", playerList[k]["Currency"], Ban.getIntStatus(playerList[k].Row));
                    }
                }

                if (upLevel == p.ID.ToString())
                    continue;

                UpperLevelRecord.Add(result, total, i, upLevel, p.Name, p["Currency"], (int)p["Status"]);
                /*--------------------------------*/
            }

            string title;
            int level = 0;

            level = Member.GetLevel(p["UpLevelTree"].ToString());
            title = CompanyTitle.Get(p, level, out title);

            dt.Rows[0]["Name"] = title + " " + p.Name;
            dt.Rows[0]["UserID"] = p.ID;
            dt.Rows[0]["Status"] = p["Status"];
            
            dt = op.LayerOneTopDataFunc(dt,total, p, playerList);

            Top_UserData = dt;

            dt = DataStruct.NewTable(datatype);

            DataStruct.SetDefaultVal(datatype, dt, out r);
            r["Currency"] = -1;
            dt.Rows.Add(r);
            
            for (int i = 0; i < playerList.Count; i++) {
                k = result.Find(playerList[i]["uid"]);
                
                if (k != -1 || playerList[i]["UpLevelId"].ToString() != p.ID.ToString()) {
                    continue;
                }

                if (ShowData_Permission.isAll(playerList[i], Type))
                    UpperLevelRecord.Add(result, dt, 0, playerList[i]);
            }

            for (int i = 0; i < playerList.Count; i++) {
                k = result.Find(playerList[i]["uid"]);

                if (k != -1) {
                    level = Member.GetLevel(playerList[i]["UpLevelTree"].ToString());
                    title = CompanyTitle.Get(p, level, out title);

                    result[k]["Name"] = title + " " + playerList[i]["Name"].ToString();
                }
            }

            int j = result.Find(p.ID);

            if (j != -1) {
                result[j]["First"] = 0;
                result[j].Delete();
            }

            Lower_UserData = op.LayerOneOtherFunc(result, total, p, playerList).ToTable();

            return true;
        }

        public static bool Sub(DataTable total, DataTable pList, Player p, int Type, Player thisPlayer, string SearchMember, DataStruct.DataType datatype, out DataTable return_dt) {
            var op = Procedures.Operation.GetInstance(datatype);

            DataTable dt = DataStruct.NewTable(datatype);
            DataView playerList = new DataView(pList);

            string upLevelTree = "", upLevel;
            int k;

            playerList.Sort = "uid";

            DataView result = new DataView(dt);
            result.Sort = RecordSortCol;

            for (int i = 0; i < total.Rows.Count; i++) {
                k = playerList.Find(total.Rows[i][RecordSortCol]);

                if (k == -1)
                    continue;

                if (SearchMember != "" && !playerList[k]["Name"].ToString().Contains(SearchMember)) {
                    continue;
                }

                upLevelTree = playerList[k]["UpLevelTree"].ToString();
                if (p["UpLevelTree"].ToString() != "")
                    upLevelTree = upLevelTree.Replace(p["UpLevelTree"].ToString(), "");

                string[] tmp = upLevelTree.Split(',');

                upLevel = tmp[0];
                if (ShowData_Permission.Lower_MemberAndStockholder(playerList[k], p, upLevel) && tmp.Length == 2) {
                    //自己的玩家&股東下線
                    if (tmp[1] == "")
                        UpperLevelRecord.Add(result, total, i, playerList[k]);
                } else if (ShowData_Permission.isNormalMember(playerList[k], thisPlayer, Type)) {
                    //普通玩家
                    UpperLevelRecord.Add(result, total, i, playerList[k]);
                } else if (SearchMember != "" && playerList[k]["canAddNextLevel"].ToString() == "0" && Convert.ToInt32(thisPlayer["canSeeNextLevel"]) == 1) {
                    //代理商
                    UpperLevelRecord.Add(result, total, i, playerList[k]);
                }
            }

            dt = DataStruct.NewTable(datatype);

            DataRow r;
            DataStruct.SetDefaultVal(datatype, dt, out r);
            r["Currency"] = -1;
            dt.Rows.Add(r);

            for (int i = 0; i < playerList.Count; i++) {
                k = result.Find(playerList[i]["uid"]);

                if (k != -1) {
                    result[k]["Name"] = playerList[i]["Name"].ToString();
                    op.UpdateFixedRecord_Member(result[k], playerList[i], p);
                }

                if (SearchMember != "" && playerList[i]["Name"].ToString().Contains(SearchMember)) {

                } else if (k != -1) {
                    continue;
                }

                upLevelTree = playerList[i]["UpLevelTree"].ToString();
                if (p["UpLevelTree"].ToString() != "")
                    upLevelTree = upLevelTree.Replace(p["UpLevelTree"].ToString(), "");

                string[] tmp = upLevelTree.Split(',');

                upLevel = tmp[0];
                if (ShowData_Permission.Lower_MemberAndStockholder(playerList[i], p, upLevel) && tmp.Length == 2) {
                    //自己的玩家&股東下線
                    if (tmp[1] == "") {
                        op.Sub_Step1(result, dt, 0, playerList[i]);
                    }
                } else if (ShowData_Permission.isNormalMember(playerList[i], thisPlayer, Type)) {
                    //普通玩家
                    op.Sub_Step1(result, dt, 0, playerList[i]);
                } else if (SearchMember != "" && playerList[i]["canAddNextLevel"].ToString() == "0" && Convert.ToInt32(thisPlayer["canSeeNextLevel"]) == 1) {
                    //代理商
                    op.Sub_Step1(result, dt, 0, playerList[i]);
                }
            }

            int j = result.Find(p.ID);

            if (j != -1) {
                result[j]["First"] = 0;
                result.Sort = "First";
            }
            
            return_dt = op.LayerTwoOtherFunc(result, total, p, playerList).ToTable();
            return true;
            //return result.ToTable();
        }
    }
}

namespace Operation.Procedures {
    public static class Operation {
        public static Dictionary<DataStruct.DataType, operation> types = new Dictionary<DataStruct.DataType, operation>(){
                {DataStruct.DataType.GAMELIST, new GAMELIST()},
                {DataStruct.DataType.TRANS, new TRANSLIST()},
                {DataStruct.DataType.MEMBERMANAGE, new MEMBERMANAGELIST()},
                {DataStruct.DataType.STOCKHOLDERMANAGE, new STOCKHOLDERMANAGELIST()},
            { DataStruct.DataType.GAMEREFUND, new GAMEREFUNDLIST()}
            };

        public static operation GetInstance(DataStruct.DataType datatype) {
            if (types.ContainsKey(datatype))
                return types[datatype];

            return null;
        }

        public interface operation {
            DataStruct.DataType Col { get; }

            DataTable Step1(DataTable target, Player p);
            void Step2(DataTable target, DataRow dr);

            DataView Sub_Step1(DataView target, DataTable record, int index, DataRowView playerRow);

            DataView LayerOneOtherFunc(DataView result, DataTable record, Player p,DataView lowerpList);

            DataTable LayerOneTopDataFunc(DataTable result, DataTable record, Player p, DataView lowerpList);

            DataRowView UpdateFixedRecord_Member(DataRowView target, DataRowView record, Player p);
            
            DataView LayerTwoOtherFunc(DataView result, DataTable record, Player p, DataView lowerpList);

        }
                
    }
}