﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using GameLibrary;
using Col_M = DataStruct.Col_MemberManage;
using Operation;

namespace Operation.Procedures {    
    public class MEMBERMANAGELIST : Operation.operation {
        DataStruct.DataType col = DataStruct.DataType.MEMBERMANAGE;
        public DataStruct.DataType Col {
            get { return col; }
        }

        public DataTable Step1(DataTable target, Player p) {
            DataRow r;
            if (DataStruct.SetDefaultVal(Col, target, out r)) {
                r[(int)Col_M.Currency] = p["Currency"];
                r[(int)Col_M.UserUD] = 0;
                r[(int)Col_M.CreditSum] = r[(int)Col_M.CanUseCredit] = p["CreditMax"];
                r[(int)Col_M.Status] = Ban.getIntStatus(p);
                r[(int)Col_M.CreditRemain] = p["CreditRemain"];
                r[(int)Col_M.LastLoginTime] = p["LoginTime"];
                target.Rows.Add(r);
            }

            return target;
        }

        public void Step2(DataTable dt, DataRow total) {
            dt.Rows[0][(int)Col_M.CreditSum] = Convert.ToDouble(dt.Rows[0][(int)Col_M.CreditSum]) + Convert.ToDouble(total[(int)Col_M.CreditSum]);
            dt.Rows[0][(int)Col_M.Cash] = Convert.ToInt64(dt.Rows[0][(int)Col_M.Cash]) + Convert.ToInt64(total[(int)Col_M.Cash]);
            dt.Rows[0][(int)Col_M.Refund] = 0;
            dt.Rows[0][(int)Col_M.AgentBonus] = Convert.ToDouble(dt.Rows[0][(int)Col_M.AgentBonus]) + Convert.ToDouble(total[(int)Col_M.AgentBonus]);
            dt.Rows[0][(int)Col_M.CompanyBonus] = Convert.ToDouble(dt.Rows[0][(int)Col_M.CompanyBonus]) + Convert.ToDouble(total[(int)Col_M.CompanyBonus]);
            dt.Rows[0][(int)Col_M.UserUD] = Convert.ToDouble(dt.Rows[0][(int)Col_M.UserUD]) + Convert.ToDouble(total[(int)Col_M.UserUD]);
            dt.Rows[0][(int)Col_M.CreditRemain] = Convert.ToDouble(dt.Rows[0][(int)Col_M.CreditRemain]) + Convert.ToDouble(total[(int)Col_M.CreditRemain]);
        }

        public DataView Sub_Step1(DataView target, DataTable record, int index, DataRowView playerRow) {
            DataRowView drv;
            UpperLevelRecord.Add(target, record, 0, playerRow, out drv);
            UpdateFixedRecord_Member(drv, playerRow, null);

            return target;
        }

        public DataRowView UpdateFixedRecord_Member(DataRowView target, DataRowView record, Player p) {
            target[(int)Col_M.UserUD] = record["BankCredit"];
            target[(int)Col_M.CanUseCredit] = record["CreditMax"];
            target[(int)Col_M.Role] = record["canAddNextLevel"];
            target[(int)Col_M.CreditRemain] = record["CreditRemain"];
            if (record["LastLoginTime"] != DBNull.Value)
                target[(int)Col_M.LastLoginTime] = record["LastLoginTime"];

            if (record["RemainRefund"] != DBNull.Value)
                target[(int)Col_M.RemainRefund] = record["RemainRefund"];
            else
                target[(int)Col_M.RemainRefund] = 0;

            return target;
        }

        public DataTable LayerOneTopDataFunc(DataTable target, DataTable record, Player p, DataView lowerpList) {
            if (target.Rows.Count == 0) 
                return target;

            int k;
            string upLevel;
            target.Rows[0][(int)Col_M.UserUD] = 0;

            //k = lowerpList.Find(p.ID);
            //if (k != -1) {
            //    target.Rows[0][(int)Col_M.CreditSum] = lowerpList[k]["CreditMax"];
            //    target.Rows[0][(int)Col_M.CreditRemain] = lowerpList[k][Col_M.CreditRemain.ToString()];
            //}

            target.Rows[0][(int)Col_M.CreditSum] = 0;
            target.Rows[0][(int)Col_M.CreditRemain] = 0;
            target.Rows[0][(int)Col_M.RemainRefund] = 0;
            /*計算遊戲點數*/
            for (int i = 0; i < lowerpList.Count; i++) {                
                upLevel = lowerpList[i]["UpLevelId"].ToString();

                if (upLevel == p.ID.ToString() && ShowData_Permission.isNormalMember(lowerpList[i])) {
                    target.Rows[0][(int)Col_M.UserUD] = Convert.ToDouble(target.Rows[0][(int)Col_M.UserUD]) + Convert.ToDouble(lowerpList[i]["BankCredit"]);
                    target.Rows[0][(int)Col_M.CreditRemain] = Convert.ToDouble(target.Rows[0][(int)Col_M.CreditRemain]) + Convert.ToDouble(lowerpList[i]["CreditRemain"]);
                    if (DBNull.Value != lowerpList[i]["RemainRefund"])
                        target.Rows[0][(int)Col_M.RemainRefund] = Convert.ToDouble(target.Rows[0][(int)Col_M.RemainRefund]) + Convert.ToDouble(lowerpList[i]["RemainRefund"]);
                }
            }
            return target;
        }

        public DataView LayerOneOtherFunc(DataView target, DataTable record, Player p, DataView lowerpList) {
            /*Initial game point*/
            int k, m;
            string upLevelTree, upLevel;
            for (int i = 0; i < target.Count; i++) {
                target[i][(int)Col_M.UserUD] = 0;
                target[i][(int)Col_M.RemainRefund] = 0;

                k = lowerpList.Find(target[i][(int)Col_M.UserID]);
                if (k == -1)
                    continue;
                target[i][(int)Col_M.CanUseCredit] = lowerpList[k]["CreditMax"];
                target[i][(int)Col_M.CreditRemain] = lowerpList[k]["CreditRemain"];
                /*loginTime*/
                if (lowerpList[k]["ReportLoginTime"] != DBNull.Value)
                    target[i][(int)Col_M.LastLoginTime] = lowerpList[k]["ReportLoginTime"];
            }

            /*計算遊戲點數&&Refund*/
            for (int i = 0; i < lowerpList.Count; i++) {                
                upLevelTree = lowerpList[i]["UpLevelTree"].ToString();

                if (p["UpLevelTree"].ToString() != "")
                    upLevelTree = upLevelTree.Replace(p["UpLevelTree"].ToString()+p.ID+",", "");

                string[] tmp = upLevelTree.Split(',');

                upLevel = tmp[0];

                if (upLevel != "" && ShowData_Permission.isNormalMember(lowerpList[i])) {
                    k = target.Find(upLevel);
                    if (k==-1) 
                        continue;

                    target[k][(int)Col_M.UserUD] = Convert.ToDouble(target[k][(int)Col_M.UserUD]) + Convert.ToDouble(lowerpList[i]["BankCredit"]);
                    if (DBNull.Value != lowerpList[i]["RemainRefund"])
                        target[k][(int)Col_M.RemainRefund] = Convert.ToDouble(target[k][(int)Col_M.RemainRefund]) + Convert.ToDouble(lowerpList[i]["RemainRefund"]);
                }
            }

            return target;
        }

        public DataView LayerTwoOtherFunc(DataView target, DataTable record, Player p, DataView lowerpList) {            
            return target;
        }
    }

}