﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using GameLibrary;
using Col_G = DataStruct.Col_Total;

namespace Operation.Procedures {    
    public class GAMELIST : Operation.operation {
        DataStruct.DataType col = DataStruct.DataType.GAMELIST;
        public DataStruct.DataType Col {
            get { return col; }
        }

        public DataTable Step1(DataTable target, Player p) {
            DataRow r;
            if (DataStruct.SetDefaultVal(Col, target, out r)) {
                r[(int)Col_G.Currency] = p["Currency"];

                DataTable bonusInfo = Ssm.SQLAction.GetBonusInfo(new MySQL("unidb"), Rule_Shared.GetFixedGID(), p.ID);

                if (bonusInfo.Rows.Count == 0) {
                    r[(int)Col_G.ShopBonus] = 0;
                } else
                    r[(int)Col_G.ShopBonus] = bonusInfo.Rows[0]["ShopBonus"];

                target.Rows.Add(r);
            }

            return target;
        }

        public void Step2(DataTable dt, DataRow total) {
            dt.Rows[0][(int)Col_G.totalBet] = Convert.ToDouble(dt.Rows[0][(int)Col_G.totalBet]) + Convert.ToDouble(total[(int)Col_G.totalBet]);
            dt.Rows[0][(int)Col_G.totalWin] = Convert.ToDouble(dt.Rows[0][(int)Col_G.totalWin]) + Convert.ToDouble(total[(int)Col_G.totalWin]);            
            dt.Rows[0][(int)Col_G.Refund] = Convert.ToDouble(dt.Rows[0][(int)Col_G.Refund]) + Convert.ToDouble(total[(int)Col_G.Refund]);
            dt.Rows[0][(int)Col_G.Sum] = Convert.ToDouble(dt.Rows[0][(int)Col_G.Sum]) + Convert.ToDouble(total[(int)Col_G.Sum]);
            dt.Rows[0][(int)Col_G.AgentBonus] = Convert.ToDouble(dt.Rows[0][(int)Col_G.AgentBonus]) + Convert.ToDouble(total[(int)Col_G.AgentBonus]);
            dt.Rows[0][(int)Col_G.CompanyBonus] = Convert.ToDouble(dt.Rows[0][(int)Col_G.CompanyBonus]) + Convert.ToDouble(total[(int)Col_G.CompanyBonus]);
            dt.Rows[0][(int)Col_G.AgentRefund] = Convert.ToDouble(dt.Rows[0][(int)Col_G.AgentRefund]) + Convert.ToDouble(total[(int)Col_G.AgentRefund]);
            dt.Rows[0][(int)Col_G.Profit] = Convert.ToDouble(dt.Rows[0][(int)Col_G.Profit]) + Convert.ToDouble(total[(int)Col_G.Profit]);
            dt.Rows[0][(int)Col_G.DataCount] = Convert.ToDouble(dt.Rows[0][(int)Col_G.DataCount]) + Convert.ToDouble(total[(int)Col_G.DataCount]);

            dt.Rows[0][(int)Col_G.Level1Bonus] = Convert.ToDouble(dt.Rows[0][(int)Col_G.Level1Bonus]) + Convert.ToDouble(total[(int)Col_G.Level1Bonus]);
            dt.Rows[0][(int)Col_G.Level2Bonus] = Convert.ToDouble(dt.Rows[0][(int)Col_G.Level2Bonus]) + Convert.ToDouble(total[(int)Col_G.Level2Bonus]);
            dt.Rows[0][(int)Col_G.Level3Bonus] = Convert.ToDouble(dt.Rows[0][(int)Col_G.Level3Bonus]) + Convert.ToDouble(total[(int)Col_G.Level3Bonus]);
            dt.Rows[0][(int)Col_G.Level4Bonus] = Convert.ToDouble(dt.Rows[0][(int)Col_G.Level4Bonus]) + Convert.ToDouble(total[(int)Col_G.Level4Bonus]);
            
        }

        public DataView Sub_Step1(DataView target, DataTable record, int index, DataRowView playerRow) {
            UpperLevelRecord.Add(target, record, index, playerRow);

            return target;
        }

        public DataRowView UpdateFixedRecord_Member(DataRowView target, DataRowView record, Player p) {
            DataTable bonusInfo = Ssm.SQLAction.GetBonusInfo(new MySQL("unidb"), Rule_Shared.GetFixedGID(), p.ID);

            if (bonusInfo.Rows.Count == 0) {
                target[(int)Col_G.ShopBonus] = 0;
            } else
                target[(int)Col_G.ShopBonus] = 100 - Convert.ToDouble(bonusInfo.Rows[0]["ShopBonus"]);

            return target;
        }

        public DataView LayerOneOtherFunc(DataView target, DataTable record, Player p, DataView lowerpList) {
            int k;
            for (int i = 0; i < target.Count; i++) {
                k = lowerpList.Find(target[i][(int)Col_G.UserID]);

                if (k == -1)
                    continue;
                target[i][(int)Col_G.ShopBonus] = 100 - Convert.ToDouble(lowerpList[k]["ShopBonus"]);
                target[i][(int)Col_G.AgentRefundRate] = Convert.ToDouble(lowerpList[k]["Refund"]);

                /*代理商水錢:四捨五入到小數後2位*/
                //target[i][(int)Col_G.AgentRefund] = Math.Round(Convert.ToDouble(target[i][(int)Col_G.AgentRefund]), 2, MidpointRounding.AwayFromZero);
                target[i][(int)Col_G.AgentRefund] = Convert.ToInt64(Math.Floor((double)target[i][(int)Col_G.AgentRefund]));
            }

            return target;
        }

        public DataTable LayerOneTopDataFunc(DataTable target, DataTable record, Player p, DataView lowerpList) {
            if (target.Rows.Count == 0)
                return target;

            target.Rows[0][(int)Col_G.ShopBonus] = 100 - Convert.ToDouble(target.Rows[0][(int)Col_G.ShopBonus]);
            target.Rows[0][(int)Col_G.Level1Bonus] = target.Rows[0][(int)Col_G.Level2Bonus] = target.Rows[0][(int)Col_G.Level3Bonus] = target.Rows[0][(int)Col_G.Level4Bonus] = 0;

            /*代理商水錢:四捨五入到小數後2位*/
            //target.Rows[0][(int)Col_G.AgentRefund] = Math.Round(Convert.ToDouble(target.Rows[0][(int)Col_G.AgentRefund]), 2, MidpointRounding.AwayFromZero);
            target.Rows[0][(int)Col_G.AgentRefund] = Convert.ToInt64(Math.Floor((double)target.Rows[0][(int)Col_G.AgentRefund]));

            for (int i = 0; i < record.Rows.Count; i++) {
                target.Rows[0][(int)Col_G.Level1Bonus] = Convert.ToDouble(target.Rows[0][(int)Col_G.Level1Bonus]) + Convert.ToDouble(record.Rows[i][(int)Col_G.Level1Bonus]);
                target.Rows[0][(int)Col_G.Level2Bonus] = Convert.ToDouble(target.Rows[0][(int)Col_G.Level2Bonus]) + Convert.ToDouble(record.Rows[i][(int)Col_G.Level2Bonus]);
                target.Rows[0][(int)Col_G.Level3Bonus] = Convert.ToDouble(target.Rows[0][(int)Col_G.Level3Bonus]) + Convert.ToDouble(record.Rows[i][(int)Col_G.Level3Bonus]);
                target.Rows[0][(int)Col_G.Level4Bonus] = Convert.ToDouble(target.Rows[0][(int)Col_G.Level4Bonus]) + Convert.ToDouble(record.Rows[i][(int)Col_G.Level4Bonus]);
            }

            return target;
        }

        public DataView LayerTwoOtherFunc(DataView target, DataTable record, Player p, DataView lowerpList) {            
            return target;
        }
    }

}