﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using GameLibrary;
using Col_GR = DataStruct.Col_GameRefund;
using Operation;

namespace Operation.Procedures {
    public class GAMEREFUNDLIST : Operation.operation {
        DataStruct.DataType col = DataStruct.DataType.GAMEREFUND;
        public DataStruct.DataType Col {
            get { return col; }
        }

        public DataTable Step1(DataTable target, Player p) {
            DataRow r;
            if (DataStruct.SetDefaultVal(Col, target, out r)) {
                r[(int)Col_GR.Name] = p.Name;
                target.Rows.Add(r);
            }

            return target;
        }

        public void Step2(DataTable dt, DataRow total) {
            dt.Rows[0][(int)Col_GR.Refund] = Convert.ToDouble(dt.Rows[0][(int)Col_GR.Refund]) + Convert.ToDouble(total[(int)Col_GR.Refund]);

        }

        public DataView Sub_Step1(DataView target, DataTable record, int index, DataRowView playerRow) {
            DataRowView drv;
            UpperLevelRecord.Add(target, record, 0, playerRow, out drv);
            UpdateFixedRecord_Member(drv, playerRow, null);

            return target;
        }

        public DataRowView UpdateFixedRecord_Member(DataRowView target, DataRowView record, Player p) {
            
            return target;
        }

        public DataTable LayerOneTopDataFunc(DataTable target, DataTable record, Player p, DataView lowerpList) {
           
            return target;
        }

        public DataView LayerOneOtherFunc(DataView target, DataTable record, Player p, DataView lowerpList) {
            
            return target;
        }

        public DataView LayerTwoOtherFunc(DataView target, DataTable record, Player p, DataView lowerpList) {            
            return target;
        }
    }

}