﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Net;

public partial class pdfPage : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string tmp_pdfText = Request.QueryString["filename"];
        string type = Request.QueryString["type"];
        if (tmp_pdfText!= null && tmp_pdfText != "" && type != null && type != "") {
            string tmp_path = Server.MapPath("/pdf/") +(type=="0"? @"AUTO\" : @"SEARCH\") + tmp_pdfText + ".pdf";
            if(File.Exists(tmp_path))
                DownLoadPdf(tmp_path);
        }
    }

    private void DownLoadPdf(string PDF_FileName) {
        WebClient client = new WebClient();
        Byte[] buffer = client.DownloadData(PDF_FileName);
        Response.ContentType = "application/pdf";
        Response.AddHeader("content-length", buffer.Length.ToString());
        Response.BinaryWrite(buffer);
    }
}