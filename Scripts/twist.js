﻿DataSheet.Game_18 = {
    _Record: {
        twist_type: {
            col: "戳獎廳",
            Value: "",
            Center: true,
            Set: function (val) {
                if (val == null)
                    return;
                this.Value = val + "級" + this.col;
            }
        },
        createdate: {
            col: "戳獎時間",
            Value: "",
            Set: function (val) {
                if (val) {
                    val = val.Year + '/' + val.Month + '/' + val.Day + " " + val.Hour + ":" + val.Minute;
                    this.Value = val;
                } else
                    this.Value = "";
            }
        },
        priceType: {
            col: "戳中獎項",
            Value: "",
            Center: true,
            Set: function (val) {
                if (val == null)
                    return;
                this.Value = val == 0 ? "銘謝惠顧" : val + "彩金";
            }
        },
        bigprice: {
            col: "戳中大獎",
            Value: "",
            addText: ["否", "是"],
            Center: true,
            Set: function (val) {
                if (val == null)
                    return;
                this.Value = this.addText[Number(val)];
            }
        },
        cost: {
            col: "總下注",
            Value: "",
            Center: true,
            Set: function (val) {
                if (val == null)
                    return;
                this.Value = val;
            }
        },
        win: {
            col: "贏得彩金",
            Value: "",
            Center: true,
            Set: function (val) {
                if (val == null)
                    return;
                this.Value = val;
            }
        },
        multiple: {
            col: "彩金倍數",
            Value: "",
            Center: true,
            Set: function (val) {
                if (val == null)
                    return;
                this.Value = val;
            }
        },
    },
    //_col: ["戳獎廳", "戳獎時間", "戳中獎項", "戳中大獎"，"總下注"，"贏得彩金", "彩金倍數"],
    _col: ["twist_type", "createdate", "priceType", "bigprice", "cost", "win", "multiple"],
    _width: [250, 250, 200, 200, 200, 200, 200],
    Get: {
        twistType: 0,
        totalCost: 0,
        _price: [1000, 3000, 5000, 10000, 30000],
        mult: function () {
            return this.totalCost / _price[this.twistType];
        }
    }
}