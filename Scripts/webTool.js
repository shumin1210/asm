﻿/// <reference path="jquery-1.12.3.min.js" />
/// <reference path="ToolsForWebTool.js" />
/*/// <reference path="https://www.crazybet.win/library/Tools.latest.js"/>*/

/*
2017-02-24 :add Ajax Master(共用function)
            add WebTool.Cookies
2017-02-23 :fixed WebTool.Page
            add WebTool.Enum
            add Ajax Queue
2017-02-22 :SetAttribute加Try Catch和Console錯誤訊息
2017-02-18 :fixed ie8 SetAttribute
            add WebTool.Console, WebTool.Config
2017-02-16 :edit isIE, add testIsIE
2017-02-13 :fixed Element.SelectOpt ie8下text無法顯示
            add Page Custom append Control
2017-02-11 :增加String.trim
2017-02-10 :增加 WebTool.Hover.addEvent
*/


var WebTool = WebTool || {};

WebTool.Config = {
    "OpenConsole": false/*for debug*/
};

/**
* WebTool.Enum
*/
(function (){
    WebTool.Enum = {
        Status: {
            Init: 0,Preparing:1, Loading: 2, InfoExchanging: 3, Completed: 4
        }
    };
}());
/**
* WebTool.Core 
*/
(function () {
    var core = WebTool;
    var base_width = 1280;
    var base_height = 785;
    var base_font_size = 15;

    var vars = [];
    var resizeFunc = [];
    var namespace = "WebTool";

    var resizeTime = 200;
    var resizeTimer = null;

    /**
    * Read a page's GET URL variables and return them as an associative array.
    */
    core._getUrlVars = function () {
        var hash;
        var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
        for (var i = 0; i < hashes.length; i++) {
            hash = hashes[i].split('=');
            vars.push(hash[0]);
            vars[hash[0]] = decodeURIComponent(hash[1]);
        }
    }

    /**
    * 
    * @param {String} version :版本
    * @param {type} comparison
    * @returns {bool} 
    */
    core.isIE = function (version, comparison) {
        var ua = window.navigator.userAgent;
        var msie = ua.indexOf("MSIE ");

        if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))  // If Internet Explorer, return version number
        {
            if (arguments.length == 0)
                return true;

            if (parseInt(ua.substring(msie + 5, ua.indexOf(".", msie))) == version)
                return true;
            //alert(parseInt(ua.substring(msie + 5, ua.indexOf(".", msie))));
        }
        else  // If another browser, return 0
        {
            //alert('otherbrowser');
        }

        return false;

        //        var cc = 'IE',
        //            b = document.createElement('B'),
        //            docElem = document.documentElement,
        //            isIE;

        //        if (version) {
        //            cc += ' ' + version;
        //            if (comparison) { cc = comparison + ' ' + cc; }
        //        }

        //        b.innerHTML = '<!--[if ' + cc + ']><b id="iecctest"></b><![endif]-->';
        //        docElem.appendChild(b);
        //        isIE = !!document.getElementById('iecctest');
        //        docElem.removeChild(b);
        //        return isIE;
    }

    /**
    * 執行function By String
    * @param {String} func :function名
    * @param {params} parameter :參數
    */
    core.ExecuteFunc = function (functionName, parameter) {
        if (functionName == "" || functionName == undefined)
            return;
        var args = Array.prototype.slice.call(arguments, 1);
        var namespaces = functionName.split(".");
        var func = namespaces.pop();
        var context = window;
        for (var i = 0; i < namespaces.length; i++) {
            context = context[namespaces[i]];
        }

        if (typeof context[func] == 'function') {
            return context[func].apply(context, args);
        }
    }

    core.Console = function (msg) {
        if (!window.console)
            return;
        if (!WebTool.Config.OpenConsole)
            return;
        window.console.log(msg);
    }

    core.Element = function () {
        var el = {};

        /**
        * 水平置中
        * @param {String} id :輸入設定的 id or css 字串
        */
        el.Center = function (id) {
            var control = $(id);

            if (Test.Undefined(control)) {
                return;
            }

            var pa = control.parent();
            var pa_width = pa.width();
            var m_left = (pa_width - control.width()) / 2;
            control.css({
                'margin-left': m_left + 'px'
            });
        }

        /**
        * 垂直置中
        * @param {String} id :輸入設定的 id or css 字串
        */
        el.Middle = function (id) {
            var control = $(id);

            if (Test.Undefined(control)) {
                return;
            }

            var pa = control.parent();
            var pa_height = pa.height();
            var m_top = (pa_height - control.height()) / 2;
            control.css({
                'margin-top': m_top + 'px'
            });
        }

        /**
        * 兩element相同高度
        * @param {HTMLElement} base_control :此control高度
        * @param {HTMLElement} target_control :需改變高度之control
        */
        el.SameHeight = function (base_control, target_control) {
            if (Test.Undefined(base_control) || Test.Undefined(target_control)) {
                return;
            }
            target_control.height = base_control.height;
        }

        /**
        * 兩element相同寬度
        * @param {HTMLElement} base_control :此control寬度
        * @param {HTMLElement} target_control :需改變寬度之control
        */
        el.SameWidth = function (base_control, target_control) {
            if (Test.Undefined(base_control) || Test.Undefined(target_control)) {
                return;
            }
            target_control.width = base_control.width;
        }

        el._AdjustFontSize = [];
        /**
        * 根據螢幕寬度改變此element的字體大小
        * @param {String} tag :可帶ID,Class,Element Tag(EX:#id,.class,span)
        * @param {type} base_font_size :預設(寬=1280)字體大小 (opt)
        */
        el.AdjustFontSize = function (tag, font_size) {
            if (!Tools.IsMobile) {
                function _adjustFontSize(pid, size) {
                    size = Test.Undefined(size) ? base_font_size : size;

                    var w_width, w_height;
                    w_width = $(window).width();

                    var w_radio = w_width / base_width;
                    var _size = w_radio * size;

                    if (_size < 12) {
                        _size = 12;
                    }

                    $(pid).css({
                        'font-size': _size + 'px'
                    });
                }
                WebTool.Orientation.Add(_adjustFontSize.bind(null, tag, font_size));
            }
        }

        /**
        * 新增select 選項
        * @param {HTMLElememt} select
        * @param {String} value
        * @param {String} text
        * @returns select
        */
        el.SelectOpt = function (select, value, text) {
            var option = document.createElement("option");
            option.text = option.innerText = text;
            option.value = value;
            select.appendChild(option);

            return select;
        }

        /**
        * Div或圖片或Control等比例縮放,以螢幕寬或高度為基準
        * @param {String} id :id or class
        * @param {Number} width :圖片原始寬度
        * @param {Number} height :圖片原始高度
        * @param {Number} pa :改變寬或高度(pa數)
        * @param {ConfigObject} opt :  {(選填)
        *                                  h_middle: bool(是否水平置中)(預設:false), 
        *                                  v_middle: bool(是否垂直置中)(預設:false), 
        *                                  Fixed: (0:以寬為基準1:以高為基準) (預設:0)
        *                              } 
        */
        el.SetAutoResize = function (id, width, height, pa, opt) {
            function _resize() {
                var control = $(id);

                if (!control.length)
                    return;

                //parent尺寸
                var w_width, w_height;
                var w_h = width / height;
                var setting = {
                    h_middle: false,
                    v_middle: false,
                    Fixed: 0
                };
                if (opt) {
                    for (var i in opt) {
                        setting[i] = opt[i];
                    }
                }

                var parent = control.parent();
                w_width = parent.width();
                w_height = parent.height();

                if (setting.Fixed == 0) {
                    control.css({
                        'width': w_width * pa + 'px',
                        'height': w_width * pa / w_h + 'px'
                    });
                } else {
                    control.css({
                        'height': w_height * pa + 'px',
                        'width': w_height * pa * w_h + 'px'
                    });
                }

                if (setting.h_middle) {
                    control.css({
                        'margin': '0 auto'
                    });
                }

                if (setting.v_middle) {
                    var pa_height = parent.height();
                    var m_top = (pa_height - control.height()) / 2;
                    control.css({
                        'margin-top': m_top + 'px'
                    });
                }
            }
            WebTool.Orientation.Add(_resize.bind(null, id, width, height, pa, opt));
        }
        return el;
    } ();

    core.IsValid = function () {
        var ch = {};
        /**
        * 是否為有效電子郵件
        * @param {String} Email
        * @returns {bool} 
        */
        ch.Email = function (Email) {
            var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
            return re.test(email);
        }

        return ch;
    } ();

    /** 
    * 設定HTMLElement control的Attribute 
    * @param {HTMLElement} control
    * @param {ConfigObject} setting(Init:true 初始化)
    * @returns control
    */
    core.SetAttribute = function (control, setting) {
        if (Test.Undefined(setting)) {
            return control;
        }

        var init = "Init";
        for (var key in setting) {
            if (key == init) {
                continue;
            }
            if (key in control) {
                if (key == "style") {
                    $(control).css(setting[key]);
                } else if (key.toLowerCase() == "innertext"
                    || key.toLowerCase() == "text") {
                    control.appendChild(document.createTextNode(setting[key]));
                } else {
                    if (setting[key] != "") {
                        try {
                            control[key] = setting[key];
                        } catch (e) {
                            WebTool.Console("控制項設定:" + key + "失敗,msg:" + e.message);
                        }
                    }
                }
            }
        }


        if (init in setting)
            switch (type) {
            case "a":
                control.style.textDecoration = "none";
                break;
            default:

        }

        return control;
    }

    /**
    * 取得HTMLElement control的Attribute 
    * @param {Event} evt:(點擊)事件
    * @param {String} attr :Attribute值
    * * @param {HTMLElement} control:(opt)
    * @returns {object} 
    */
    core.GetAttr = function (evt, attr, control) {
        if (core.isIE8_9) {
            return control.attr;
        }
        try {
            return evt.currentTarget.attributes[attr].value;
        } catch (exception) {
            return "";
        }
    }

    /** 
    * create 控制項
    * @param {String} type 控制項類別
    * @param {type} setting 控制項的設定檔(Init:true 初始化)
    * @returns {HTMLElement} control 
    */
    core.CreateElement = function (type, setting) {
        var control = document.createElement(type);
        control = core.SetAttribute(control, setting);
        return control;
    }

    /**
    * 
    * @param {Number} length :長度
    * @param {String} str :判斷字串
    * @param {String} char :不足補字元
    * @returns {String} str 
    */
    core.PadLeft = function (length, str, char) {
        str = str.toString();
        if (str.length < length) {
            return core.PadLeft(length, char + str, char);
        } else {
            return str;
        }
    }

    /**
    * 取得日期天數
    * @param {Number} month
    * @param {Number} year
    * @returns {Number} Days 
    */
    core.GetDaysInMonth = function (month, year) {
        return new Date(year, month, 0).getDate();
    }

    /**
    * 依外部container來改變target大小
    * @param {String} target
    * @param {Number} org_width :原始寬
    * @param {Number} org_height :原始高    
    */
    core.Fit = function (target, org_width, org_height) {
        var set_w, set_h, control = $(target);
        if (Test.Undefined(control)) {
            return;
        }

        var pa = control.parent();
        var pa_w = pa.width(), pa_h = pa.height();

        if (pa_w > width) {
            set_w = width;
        } else {
            set_w = pa_w;
        }

        if (pa_h > height) {
            set_h = height;
        } else {
            set_h = pa_h;
        }

        pa.css({
            "width": set_w + "px",
            "height": set_h + "px"
        });

        control.each(function (i, obj) {
            var o_width = obj.naturalWidth;
            var o_height = obj.naturalHeight;

            if (o_width >= o_height) {
                $(obj).css({
                    "width": set_w + "px",
                    "height": "auto"
                });
            } else {
                $(obj).css({
                    "height": set_h + "px",
                    "width": "auto"
                });
            }
        });

    }

    /**
    * 限制字串長度,可輸入替代文字     
    * @param {String} str
    * @param {Number} length
    * @param {String} replaceStr :超過設定長度替代字串(opt)
    * @returns str
    */
    core.CutString = function (str, length, replaceStr) {
        if (str.length > length) {
            str = str.substring(0, length);
            str += (arguments.length == 2
                    || Test.Undefined(replaceStr))
                    ? ""
                    : replaceStr;
        }
        return str;
    }

    /** 移入移出效果 */
    core.Hover = function () {
        var b = {};

        /**
        * 改變圖片
        * @param {String} target_id
        * @param {String} orginalImg :路徑
        * @param {String} changeImg :路徑
        */
        b.ChangeImg = function (target_id, orginalImg, changeImg) {
            var target = $("#" + target_id);

            if (Test.Undefined(target)) {
                return;
            }

            target.on({
                mouseenter: function () {
                    target.attr("src", changeImg)
                },
                mouseleave: function () {
                    target.attr("src", orginalImg)
                },
                touchstart: function () {
                    target.attr("src", changeImg)
                },
                touchend: function () {
                    target.attr("src", orginalImg)
                }
            });
        }

        /**
        * add Event
        * @param {HTMLElement} target
        * @param {function} orginalFunc 
        * @param {function} changeFunc 
        */
        b.AddEvent = function (target, orginalFunc, changeFunc) {
            if (Test.Undefined(target)) {
                return;
            }
            target.onmouseover = changeFunc;
            target.onmouseout = orginalFunc;

            //            target.on({
            //                mouseenter: function () {
            //                    target.attr("src", changeImg)
            //                },
            //                mouseleave: function () {
            //                    target.attr("src", orginalImg)
            //                },
            //                touchstart: function () {
            //                    target.attr("src", changeImg)
            //                },
            //                touchend: function () {
            //                    target.attr("src", orginalImg)
            //                }
            //            });
        }

        return b;
    } ();
    
    /**
    * 
    * @param {type} ) {

    }
    */

    //function handler(param, self) {
    //    //...
    //}
    //for (var i = 0; i < 5; i++) {
    //    //TODO :IE8 test
    //    var btn = document.createElement("input");
    //    /*bind =>call,apply*/
    //    btn.onclick = handler.bind(btn, data);
    //    /*closure*/
    //    btn.onclick = function (data) {
    //        var _data = data;
    //        var result = function (evt) {
    //            handler(_data);
    //        }
    //        return result;
    //    }(data);
    //}

    core.addCss = function (style) {
        var ele = document.createElement('style');
        ele.type = 'text/css';
        var text = Tools.Utility.ToStyle(style, true);
        if (ele.styleSheet) {   // for IE
            ele.styleSheet.cssText = text;
        } else {                // others
            ele.innerHTML = text;
        }
        document.getElementsByTagName('head')[0].appendChild(ele);
    }

    core._Init = function () {
        core._Init = function () {

        }
        core._getUrlVars();
        core.isIE8_9 = core.isIE(8) || core.isIE(9);
        core.testIsIE = core.isIE();

        core.addCss({
            ".pointer": {
                "cursor": "pointer"
            }, ".pointer:hover": {
                "text-decoration": "underline"
            }
        });

        core.addCss({
            "body": {
                "font-family": "Microsoft JhengHei"
            }
        });

        if (typeof String.prototype.trim !== 'function') {
            String.prototype.trim = function () {
                return this.replace(/^\s+|\s+$/g, '');
            }
        }

        //core.addCss({
        //    ".pointer:hover": {
        //        "text-decoration": "underline"
        //    }
        //})
    }
    core._Init();

    WebTool.UrlVars = vars;

} ());

/**
* WebTool.Ajax 
*/
(function () {
    WebTool.Ajax = {};
    var url = "GetData.asmx/getData";
    var core = WebTool,
        ajax = core.Ajax,
        _status = WebTool.Enum.Status;
        
    var ajaxData = {};
    var ajaxExcuteQueue = {};
    ajax.New = function () {
        this.url = url;
        this.paraments = {};
    }
    ajax.Status = _status.Completed;
    
    /*Some Shared Function*/
    ajax.Master = {
        ComplpeteQueue:null,
        PrepareExcute:null,
        ResponseSuccess:null
    };
    ajax._CreateRequest = function () {
        var request;
        try {
            request = new XMLHttpRequest();
        } catch (tryMS) {
            try {
                request = new ActiveXObject("Msxml2.XMLHTTP");
            } catch (otherMS) {
                try {
                    request = new ActiveXObject("Microsoft.XMLHTTP");
                } catch (failed) {
                    request = null;
                }
            }
        }
        return request;
    }

    function GetData(dataCollection) {
        var datastr = JSON.stringify(ajaxData[dataCollection].paraments);

        var request = ajax._CreateRequest();
        if (request != null) {
            ajaxExcuteQueue[dataCollection] = true;
            statusPrepare();

            WebTool.Console("ajax send:" + dataCollection);
            request.attr = dataCollection;
            request.open("POST", ajaxData[dataCollection].url, true);
            request.responseType = "json";
            request.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
            request.send(datastr);
            request.onreadystatechange = function () {
                if (this.readyState == 4) {                    
                    if (this.status == 200) {
                        WebTool.Console("ajax get:" + this.attr);
                        var responseData = this.response || this.responseText;
                        if (!responseData) {
                            return;
                        }                    
                        var ajaxCollection = ajaxData[this.attr];

                        if (responseData != "") {
                            if (typeof responseData != "object") {
                                responseData = JSON.parse(responseData);
                            }
                            var data = $.parseJSON(responseData.d);
                            if (ajax.Master.ResponseSuccess != null) {
                                ajax.Master.ResponseSuccess(ajaxCollection, data);
                            }

                            if("callback" in data){
                                WebTool.ExecuteFunc(data.callback, data.msg, ajaxCollection.paraments);
                            }
                        }                        
                    } else {
                        //alert("error");
                    }

                    delete ajaxExcuteQueue[this.attr];
                    checkComplete();
                }
            }
        }
    }

    function changeStatus(status) {
        if(ajax.Status == status)
            return;
        ajax.Status = status;
    }
    function statusPrepare() {
        changeStatus(_status.Preparing);
        if(ajax.Master.PrepareExcute != null)
            ajax.Master.PrepareExcute();
    }
    function checkComplete() {
        if($.isEmptyObject(ajaxExcuteQueue))
            changeStatus(_status.Completed);
        if (ajax.Status == _status.Completed && ajax.Master.ComplpeteQueue != null) {
            ajax.Master.ComplpeteQueue();
        }
    }
    /**
    * 
    * @param {String} Type
    * @param {Object} DataToServer
    * @param {Object} DataFromClient
    */
    ajax.Excute = function (Type,
        DataToServer, DataFromClient) {

        if (Test.Undefined(DataToServer)) {
            DataToServer = {};
        }

        var tmp = window.JSON.stringify({
            type: Type,
            content: DataToServer
        });

        if (!ajaxData[Type])
            ajaxData[Type] = new ajax.New();

        ajaxData[Type].paraments = {
            message: tmp
        };

        if (!Test.Undefined(DataFromClient))
            for (var item in DataFromClient)
                ajaxData[Type]["paraments"][item] = DataFromClient[item];

        GetData(Type);
    }
} ());

//取得及確認此Div是否有append過(return div)
function getCheckDiv(divName, className, apPosition) {
    var div = document.getElementById(divName);

    if (!div) {
        div = document.createElement("div");
        div.id = divName;
        document.getElementById(apPosition).appendChild(div);
    }

    if (className != undefined && className != "") {
        div.className = className;
    }

    return div;
}

/**
* WebTool.Orientation
* 旋轉.Resize事件 
*/
(function () {
    var o = {};
    /*orientation handler trigger delay time (default:300ms)*/
    o.Delay = 300;
    var timer = 0;
    var handler = function () {
        clearTimeout(timer)
        timer = setTimeout(function () {
            for (var i = 0; i < act.length; i++) {
                act[i]();
            }
        }, o.Delay);
    }
    var act = [];

    /**
    * 
    * @param {Function} fn :Func
    */
    o.Add = function (fn) {
        if (!act.Contain(fn))
            act.push(fn);
    }

    o.Remove = function (fn) {
        act.Remove(fn);
    }

    $(document).ready(function () {
        $(window).resize(handler);
        $(window).on("orientationchange", handler);
    });
    WebTool.Orientation = o;
} ());

/**
* WebTool.Page
* 分頁
*/
(function () {
    function Page() {
        this.DivPage = WebTool.CreateElement("div", {
            style: {
                "text-align": "center"
            }
        });
        this.Config = {
            StyleType: this.StyleType.Default
        };

        this.Text_PreNext = ["Pre *page* Page", "Next *page* Page"];
        this.pageColor = "red";
        this.isInit = false;
        this.isPaging = true;
        this._index = 0;
        this.Count = {
            AllData: 0,
            PerPageData: 5,
            PerPageControl: 10,
            PageCount: 0
        };
        this.Margin = 2;
        this.ele = {
            PrePage: null,
            NextPage: null
        };
        /*
        *使用者自訂分頁樣式
        *{function}Custom Control Style
        */
        this.CustomControl = null;
        this.callback = null;
    }

    Page.prototype = {
        StyleType: {
            "Default": "Default",
            "Custom": "Custom"
        },
        Change: function (index) {
            if (typeof (index) == 'string') {
                if (isNaN(index)) {
                    return false;
                } else {
                    index = Number(index);
                }
            }
            index -= 1;

            if (index == this._index)
                return false;

            if (index >= this.Count.PageCount)
                return false;

            if (index < 0)
                return false;

            this._index = index;

            this.Set(this.Count.AllData);

            if (this.callback != null) {
                this.callback();
            }

            return true;
        },
        /*Default Control Style*/
        _Control: function () {
            if (!this.isPaging) {
                return;
            }
            $(this.DivPage).empty();

            var self = this;
            var index = this._index;
            var cou = this.Count.PerPageControl;

            var startIndex = Math.floor(index / cou) * cou + 1;
            var endIndex = this.Count.PageCount;
            var span;

            /*前10頁*/
            if (startIndex > cou) {
                this.ele.PrePage = WebTool.CreateElement("span", {
                    style: {
                        "margin": this.Margin + "px"
                    },
                    innerText: this.Text_PreNext[0],
                    className: "pointer"
                });
                this.ele.PrePage.onclick = function (evt) {
                    self.Change(startIndex - cou - 1);
                };

                this.DivPage.appendChild(this.ele.PrePage);
            }

            for (var i = startIndex; i <= (startIndex + cou - 1) && i <= endIndex; i++) {
                span = WebTool.CreateElement("span", {
                    style: {
                        "margin": this.Margin + "px",
                        "color": (i - 1) == index ? this.pageColor : ""
                    },
                    innerText: i,
                    className: (i - 1) == index ? "" : "pointer"
                });
                span.setAttribute("pindex", i - 1);
                if ((i - 1) != index)
                    span.onclick = function (evt) {
                        self.Change(WebTool.GetAttr(evt, "pindex", this));
                    };

                this.DivPage.appendChild(span);
            }

            /*後10頁*/
            if (endIndex > startIndex + cou) {
                this.ele.NextPage = WebTool.CreateElement("span", {
                    style: {
                        "margin": this.Margin + "px"
                    },
                    innerText: this.Text_PreNext[1],
                    className: "pointer"
                });
                this.ele.NextPage.onclick = function (evt) {
                    self.Change(startIndex + cou - 1);
                };

                this.DivPage.appendChild(this.ele.NextPage);
            }
        },
        Disable: function () {
            this.isPaging = false;
        },
        Enable: function () {
            this.isPaging = true;
        },
        Reset: function (allDataCount) {
            this.isInit = false;
            this.Set(allDataCount);
        },
        Set: function (allDataCount) {
            if (!this.isInit && Test.Number(allDataCount)) {
                this.isInit = true;
                this.Count.AllData = allDataCount;
                this.Count.PageCount = Math.ceil(this.Count.AllData / this.Count.PerPageData);
                this._index = 0;
            }
            this._appendControl();
        },
        Init: function (Container, allDataCount) {
            Container.appendChild(this.DivPage);
            for (var i = 0; i < this.Text_PreNext.length; i++) {
                this.Text_PreNext[i] = this.Text_PreNext[i].replace("*page*", this.Count.PerPageControl);
            }
            this.Set(allDataCount);
        },
        _appendControl: function () {
            switch (this.Config.StyleType) {
                case this.StyleType.Default:
                    this._Control();
                    break;
                case this.StyleType.Custom:
                    if (this.CustomControl != null && typeof (this.CustomControl) === "function") {
                        this.CustomControl();
                    }
                    break;
            }
        },
        Index: function () {
            return (this._index + 1);
        }
    };

    WebTool.Page = Page;
} ());

/**
* WebTool.Cookies
*/
(function (){
    WebTool.Cookies = {};
    var cookie = WebTool.Cookies;
    cookie.Get = function (cname) {
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for(var i = 0; i <ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    };
    cookie.Set = function (exdays, cname, cvalue) {
        var d = new Date();
        d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
        var expires = "expires=" + d.toUTCString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    };
       
    cookie.Delete = function (cname){ 
        var exp = new Date(); 
        exp.setTime(exp.getTime() - 1); 
        var cval=cookie.Get(cname); 
        if(cval!=null) 
            document.cookie= cname + "="+cval+";expires="+exp.toUTCString(); 
        cval=cookie.Get(cname); 
        if(cval!=null) {
            /*not delete set empty*/
            cookie.Set(0.5,cname,"");
        }
    }  
}());