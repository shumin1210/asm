﻿var Tools = Tools || {};
/*
2017-02-15 :add parseDate to json date object
            fixed ie8 Array indexOf
2017-02-13 :fixed test baseTests to lowercase
*/


/*Extensions String*/
(function () {
    String.prototype.Contains = function (test) {
        var result = false;
        for (var i = 0; i < arguments.length; i++)
            result = result || (this.valueOf().indexOf(arguments[i]) > -1);
        return result;
    }
    String.prototype.SplitToNumber = function (seperator) {
        return this.split(seperator).map(function (item) { return Number(item) });
    }
    String.prototype.capitalizeFirstLetter = function () {
        return this.charAt(0).toUpperCase() + this.slice(1);
    }

    if (!Function.prototype.bind) {
        Function.prototype.bind = function(oThis) {
        if (typeof this !== 'function') {
            // closest thing possible to the ECMAScript 5
            // internal IsCallable function
            throw new TypeError('Function.prototype.bind - what is trying to be bound is not callable');
        }

        var aArgs   = Array.prototype.slice.call(arguments, 1),
            fToBind = this,
            fNOP    = function() {},
            fBound  = function() {
              return fToBind.apply(this instanceof fNOP && oThis
                     ? this
                     : oThis,
                     aArgs.concat(Array.prototype.slice.call(arguments)));
            };

        fNOP.prototype = this.prototype;
        fBound.prototype = new fNOP();

        return fBound;
      };
    }
}());

/*Utility Test*/
(function () {
    var Test = {};
    var baseTests = ["String", "Number", "Boolean"];
    function testBaseType(type, target, dval) {
        switch (arguments.length) {
            case 2:
                return ((typeof (target)).toLowerCase() == type.toLowerCase());
            case 3:
                return ((typeof (target)).toLowerCase() == type.toLowerCase()) ? target : dval;
            default:
                return false;
        }
    }
    
    for (var i = 0; i < baseTests.length; i++)
        Test[baseTests[i]] = testBaseType.bind(this, baseTests[i]);

    Test.Undefined = function (obj, dval) {
        var hasValue = (typeof (obj) != "undefined") && (obj != null);
        if (arguments.length == 2)
            return hasValue ? obj : dval;
        else
            return !hasValue;
    }

    Test.Array = function (obj) {
        return Test.Class(obj, Array);
    }
    Test.Function = function (obj) {
        return obj instanceof Function;
    },
    Test.Class = function (obj, Class) {
        Class = Class || Object;
        return obj instanceof Class;
    }
    Test.InClass = function (obj, Classes) {
        var result = false;
        SliceArgs(arguments, 1).forEach(function (o, i) {
            result = result || obj instanceof o;
        })
        return result;
    }

    Tools.isImageElement = function (img) {
        return Test.InClass(img, Image, HTMLCanvasElement, HTMLImageElement);
    }

    window.Test = Test;
}());

/*initial Detections v1.0*/
(function () {
    var ua = navigator.userAgent.toLowerCase();
    var isAndroid = /android/.test(ua);
    var isiOs = /(iphone|ipod|ipad)/.test(ua);
    var isStandAlone = window.navigator.standalone;
    var isiPad = ua.match(/ipad/);
    var isDevice = 'ontouchstart' in window;
    var isChrome = "chrome" in window;
    var isMoz = "mozAnimationStartTime" in window;
    var LocalStorageSupport = function () {
        try {
            localStorage.setItem("test", "check");
            localStorage.removeItem("test");
            return true;
        } catch (e) {
            return false;
        }
    }();

    var AudioSupport = function () {
        var sup = {};
        var a = document.createElement('audio');
        sup.mp3 = !!(a.canPlayType && a.canPlayType('audio/mpeg;').replace(/no/, ''));
        sup.ogg = !!(a.canPlayType && a.canPlayType('audio/ogg; codecs="vorbis"').replace(/no/, ''));
        sup.wav = !!(a.canPlayType && a.canPlayType('audio/wav; codecs="1"').replace(/no/, ''));
        return sup;
    }();

    var CanvasSupport = function () {
        var elem = document.createElement('canvas');
        return !!(elem.getContext && elem.getContext('2d'));
    }();

    var WSSupport = function () {
        return "WebSocket" in window;
    }();

    var IFrameSupport = function () {
        return document.createElement("iframe");
    }();

    var IsMobile = false;
    var IsIOs = false;
    var iOSversion = false;
    var isOldIOs = false;
    var IsWideScreen = true;
    //偵測手機瀏覽/////////////////////////////////////////////////////////////
    (function CheckMobile() {
        IsMobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);
        screenLock = 1;
        IsIOs = /iPhone|iPad|iPod/i.test(navigator.userAgent);
        if (!Tools.IMobile) {
            var w = document.documentElement.clientWidth;
            var h = document.documentElement.clientHeight;
            var r = w / h;
            IsWideScreen = (r > 1.5);
        }
        if (IsIOs) {
            if (/iP(hone|od|ad)/.test(navigator.platform)) {
                var v = (navigator.appVersion).match(/OS (\d+)_(\d+)_?(\d+)?/);
                iOSversion = [parseInt(v[1], 10), parseInt(v[2], 10), parseInt(v[3] || 0, 10)][0];
                isOldIOs = Tools.IsIOs && (Tools.iOsVersion < 8);
            }
        }
    }());

    Tools.supportAudioType=AudioSupport.mp3 ? "mp3" : (AudioSupport.ogg ? "ogg" : false);
    Tools.supportCanvas=CanvasSupport;
    Tools.supportWebSocket=WSSupport;
    Tools.supportIFrame=IFrameSupport;
    Tools.supportLocalStorage=LocalStorageSupport;    
    
    //偵測瀏覽器支援度/////////////////////////////////////////////////////////////
    Tools.SupportGame=Tools.supportCanvas
                          && Tools.supportWebSocket
                          && Tools.supportIFrame
                          && Tools.supportAudioType;
    Tools.IsMobile=IsMobile;
    Tools.IsIOs=IsIOs;
    Tools.iOsVersion=iOSversion;
    Tools.isOldIOs=isOldIOs;
    Tools.isVertical=document.documentElement.clientWidth < document.documentElement.clientHeight;
    
    //{name,version}取得瀏覽器資訊
    Tools.GetBrowserInfo = function get_browser_info() {
        var ua = navigator.userAgent, tem,
            M = ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
        if (/trident/i.test(M[1])) {
            tem = /\brv[ :]+(\d+)/g.exec(ua) || [];
            return { name: 'IE', version: (tem[1] || '') };
        }
        if (M[1] === 'Chrome') {
            tem = ua.match(/\bOPR\/(\d+)/)
            if (tem != null) { return { name: 'Opera', version: tem[1] }; }
        }
        M = M[2] ? [M[1], M[2]] : [navigator.appName, navigator.appVersion, '-?'];
        if ((tem = ua.match(/version\/(\d+)/i)) != null) { M.splice(1, 1, tem[1]); }
        return {
            name: M[0],
            version: M[1]
        };
    }();

    Tools.LaunchFullSreen = function (element) {
        if (element.requestFullscreen) {
            element.requestFullscreen();
        } else if (element.mozRequestFullScreen) {
            element.mozRequestFullScreen();
        } else if (element.webkitRequestFullscreen) {
            element.webkitRequestFullscreen();
        } else if (element.msRequestFullscreen) {
            element.msRequestFullscreen();
        }
    }

    //旋轉事件
    var c = function () {
        var o = {};
        /*orientation handler trigger delay time (default:300ms)*/
        o.Delay = 300;
        var timer = 0;
        var act = [];
        var handler = function () {
            clearTimeout(timer)
            timer = setTimeout(function () {
                for (var item in act) {
                    if (item instanceof Function)
                    item();
                }
//                act.forEach(function (fn) {
//                    if (fn instanceof Function)
//                        fn();
//                })
            }, o.Delay);
        }
        
        o.Add = function (fn) {
            if (!act.Contain(fn))
                act.push(fn);
        }

        o.Remove = function (fn) {
            act.Remove(fn);
        }

        $(document).ready(function () {
            $(window).resize(handler);
            $(window).on("orientationchange", handler);
        });
        Tools.Orientation = o;
    }();

}());

/*Extensions Date*/
(function () {
    var Signs = {};
    Signs.Date = function (date, char) {
        if (!(date instanceof Date)) {
            char = date;
            date = this;
        }
        switch (char) {
            case "y": case "Y":
                return date.getFullYear();
            case "M":
                return date.getMonth() + 1;
            case "d": case "D":
                return date.getDate();
            case "h": case "H":
                return date.getHours();
            case "m":
                return date.getMinutes();
            case "s": case "S":
                return date.getSeconds();
        }
    }
    Tools.Signs = Signs;

    window.Date.prototype.format = function (form, padElement) {
        var char;
        var output = form;
        var format = form.match(/([yYmMdDhHsS])\1+/g) || [];
        var values = [];
        var seprator = [];
        for (var i = 0; i < format.length - 1; i++) {
            var sign = format[i];
            seprator.push(form.slice(form.indexOf(sign) + sign.length, form.indexOf(format[i + 1])));
        }

        for (var i = 0; i < format.length; i++) {
            var sign = format[i];
            var length = sign.length;
            var value = Tools.Signs.Date.call(this, sign[0]);
            values[i] = Tools.Format.Pad(value, Tools.Format.Repeat("0", length));
        }

        for (var i = 0; i < format.length; i++)
            output = output.replace(format[i], values[i]);
        return output;
    }

    window.ParseDate = function (val) {
        if (val.value)
            val = val.value;
        else if (val.Value)
            val = val.Value;

        var d = new Date(val);
        if (!isNaN(d.getTime()))
            return d;

        if (!(/\/Date\(-?\d+\)\//g).test(val))
            return val;

        return new Date(Number(val.replace(/[^0-9^-]/g, "")));
    }

    Tools.timeDif = 0;
    Tools.Now = function(){
        return new Date(Tools.NowTime)
    };
    Tools.NowTime = function () {
        return (new Date().getTime() + Tools.timeDif);
    }
}());

/*Utility Format*/
(function () {
    var Format = {};

    Format.Pad = function (string, format) {
        if (!Test.String(string) && !string.toString && !string.toString())
            return string;

        string = string.toString();
        var length = Math.max(string.length, format.length)
        var result = [];
        if (string.length < length)
            return format.slice(string.length) + string;
        else
            return string;
    }

    Format.Repeat = function (str, count) {
        if (count == null || str == null)
            return str;
        str = "" + str;
        count = Number(count);
        if (count < 0 || count === Infinity)
            return str;

        count = Math.floor(count);
        var result = "";
        return new Array(count + 1).join(str);
    }

    Format.String = function (pattern, args) {
        if (!Test.String(pattern))
            return pattern;

        var result = pattern;
        for (var i = 1; i < arguments.length; i++)
            result = result.replace(new RegExp("\\{" + (i - 1) + "\\}", "gm"), arguments[i]);
        return result;
    }
    String.Format = Format.String;
    Tools.Format = Format;
}());

/*Utility toStyle*/
(function () {
    Tools.Utility = {};
    Tools.Utility.ToStyle = function (obj, easyVisualize) {
        //var str = JSON.stringify(obj).replace(/\"/g, "").replace(/,/g, ";").replace(/:{/g, "{");
        var str = JSON.stringify(obj)
            .replace(/\"/g, "")/*remove (")*/
            .replace(/([^}]),/g, "$1;").replace(/(\w+)}/g, "$1;}")/*remove (,) and add (;)*/
            .replace(/:{/g, "{").replace(/(\},)/g, "}")/*remove nonvalid(:) and (,)*/
            .replace(/(;})/g, "$&\r\n")
            .slice(1, -1);
        if (easyVisualize)
            str = str.replace(/[{;}]/g, "$&\r\n")
        return str;
    };
}());

/*Array*/
(function () {
    if (!Array.prototype.indexOf)
    {
      Array.prototype.indexOf = function(elt /*, from*/)
      {
        var len = this.length >>> 0;

        var from = Number(arguments[1]) || 0;
        from = (from < 0)
             ? Math.ceil(from)
             : Math.floor(from);
        if (from < 0)
          from += len;

        for (; from < len; from++)
        {
          if (from in this &&
              this[from] === elt)
            return from;
        }
        return -1;
      };
    }

    Array.prototype.Remove = function () {
        var what, a = arguments, L = a.length, ax;

        while (L && this.length) {

            what = a[--L];

            while ((ax = this.indexOf(what)) !== -1) {
                this.splice(ax, 1);
            }
        }
        return this;
    };

    Array.prototype.Contain = function (obj) {
        return this.indexOf(obj) != -1;
    };

}());


