﻿/// <reference path="../JQuery/jquery-1.12.3.min.js" />
/// <reference path="../JQuery/ToolsForWebTool.js" />
/// <reference path="../JQuery/webTool.js" />

/**
* @description     table
**/
var WebTool = WebTool ? WebTool : {};

/*Table排序*/
(function () {
    var stIsIE = /*@cc_on!@*/false;

    function Sort(obj, tdIndex, options, tdOrderValArr) {
        if (!tdOrderValArr || tdOrderValArr.length == 0) {
            return;
        }
        var trJqObjArr = null;
        if (obj._initConfig.IsLazyMode) {
            //!obj._trJqObjArray_Obj && (obj._trJqObjArray_Obj = {});
            trJqObjArr = obj._trJqObjArray_Obj[tdIndex];
        }
        var isExist_trJqObjArr = true;
        if (!options.isInit) {
            options.isInit = true;
            isExist_trJqObjArr = false;
            //trJqObjArr = new Array();
            //var tdOrderValArr = obj._GetOrderTdValueArray(trJqObjArr, tdIndex, options.ValAttr, options.DataType);
            var sort_len = tdOrderValArr.length - 1;
            var isExchanged = false, compareRes;
            for (var i = 0; i < sort_len; i++) {
                isExchanged = false;
                for (var j = sort_len; j > i; j--) {
                    if (options.DataType == "string") {
                        compareRes = options.Desc ? (tdOrderValArr[j].localeCompare(tdOrderValArr[j - 1])) > 0 : (tdOrderValArr[j].localeCompare(tdOrderValArr[j - 1])) < 0;
                    } else {
                        compareRes = options.Desc ? (tdOrderValArr[j] > tdOrderValArr[j - 1]) : (tdOrderValArr[j] < tdOrderValArr[j - 1]);
                    }

                    if (compareRes) {
                        obj._ExchangeArray(tdOrderValArr, j);
                        //交換行顺序
                        obj._ExchangeArray(trJqObjArr, j);
                        isExchanged = true;
                    }
                }
                if (!isExchanged)
                    break;
            }
            obj._initConfig.IsLazyMode && (obj._trJqObjArray_Obj[tdIndex] = trJqObjArr);
        }

        if (trJqObjArr) {
            if (options.Toggle) {
                obj._initConfig.IsLazyMode && isExist_trJqObjArr && trJqObjArr.reverse();
                obj.SortCase = options.Desc ? "desc" : "";
                obj.SortColumn = options.Column;
                options.Desc = !options.Desc;
            }
            obj._ShowTable(trJqObjArr);
        }
    }

    function _Table_Sort() {
        this._tableObj = null;
        this._tbodyObj = null;
        this._tBodyIndex = 0;
        this._initConfig = null;
        this._trJqObjArray_Obj = [];
        this.SortColumn = "";
        this.SortCase = "";

        this.Config = {
            "ShowArrow": true,
            "Color": "black"
        };

        this._upArrow = WebTool.CreateElement("span", {
            "innerHTML": stIsIE ? '&nbsp<font face="webdings">5</font>' : '&nbsp;&#x25B4;'
        });
        this._downArrow = WebTool.CreateElement("span", {
            "innerHTML": stIsIE ? '&nbsp<font face="webdings">6</font>' : '&nbsp;&#x25BE;'
        });
    }

    _Table_Sort.prototype = {
        Init: function (obj, tBodyObj, options) {
            this._tableObj = obj;
            //this._tBodyIndex = tBodyIndex || 0;
            this._tbodyObj = tBodyObj;
            this.options = options || {};
            this._initConfig = {
                IsLazyMode: true, //是否是懒惰模式，默認為:true
                OnSortComplete: null  //排序後的方法,params:trIndex,trJqObj,tbodyObj
            };
            $.extend(this._initConfig, options);
            this._trJqObjArray_Obj = [];

            this._upArrow.style.color = this._downArrow.style.color = this.Config.Color;
        },
        /**
        * 設定排序
        * @param {Object} obj
        * @param {Number} index :要排序列所在的列索引
        * @param {Object} options : {
        *                              ValAttr: false, //排序列的取值属性,默認為：innerText
        *                              DataType: "string", //排序列的值類型(可取值：int|float|date|string)
        *                              OnClick: null, //點擊排序後觸發的方法
        *                              Desc: true, //(是否是降序)排序方式，默認為：降序
        *                              Toggle: true, //切换排序方式
        *                              DefaultOrder: false //是否是默認的排序方式
        *                           }
        */
        SetOrder: function (obj, index, options, trJqObjArr, tdOrderValArr) {
            if (this._tableObj == null) {
                //alert("_tableObj尚未初始化！");
                return;
            }
            this._SetOrderItem(obj, index, options, trJqObjArr, tdOrderValArr);
        },
        _SetOrderItem: function (obj, index, options, trJQueryObjArr, tdOrderValArr) {
            var self = this;
            var orderSettings = {
                ValAttr: false,
                DataType: "string",
                OnClick: null,
                Desc: false,
                Toggle: true,
                DefaultOrder: false,
                isInit: false,
                Column: ""
            };
            $.extend(orderSettings, options);
            orderSettings.DataType = orderSettings.DataType.toLowerCase();
            this._trJqObjArray_Obj[index] = trJQueryObjArr.slice(0);

            obj.style.cursor = "default";

            $(obj).bind("click", function () {
                Sort(self, index, orderSettings, tdOrderValArr);
                self._ShowArrow(this, orderSettings.Desc);
                $.isFunction(orderSettings.OnClick) && orderSettings.OnClick();
            });
            orderSettings.DefaultOrder && Sort(self, index, orderSettings, tdOrderValArr);

            if (orderSettings.DefaultOrder) {
                self._ShowArrow(obj, orderSettings.Desc);
            }
        },
        _ShowArrow: function (target, Desc) {
            if (this._upArrow.parentNode != null) {
                this._upArrow.parentNode.removeChild(this._upArrow);
            }
            if (this._downArrow.parentNode != null) {
                this._downArrow.parentNode.removeChild(this._downArrow);
            }

            if (!this.Config.ShowArrow) {
                return;
            }

            target.appendChild(Desc ? this._upArrow : this._downArrow);
        },
        _GetOrderTdValueArray: function (trJqObjArr, tdIndex, td_valAttr, td_dataType) {
            var self = this;
            var tdOrderValArr = new Array();
            var trObj, tdObj, tdVal;
            this._tbodyObj.find("tr").each(function (trIndex, trItem) {
                trObj = $(trItem);
                trJqObjArr.push(trObj);

                tdObj = trObj.find("td")[tdIndex];
                tdObj = $(tdObj);
                tdVal = td_valAttr ? tdObj.attr(td_valAttr) : tdObj.text();
                tdVal = self._GetValue(tdVal, td_dataType);
                tdOrderValArr.push(tdVal);
            });
            return tdOrderValArr;
        },
        _ExchangeArray: function (array, j) {
            var temp = array[j];
            array[j] = array[j - 1];
            array[j - 1] = temp;
        },
        GetValue: function (tdVal, td_dataType) {
            switch (td_dataType) {
                case "int":
                    return parseInt(tdVal) || 0;
                case "float":
                    return parseFloat(tdVal) || 0;
                case "date":
                    return ParseDate(tdVal) || 0;
                case "string":
                default:
                    {
                        var tdVal = tdVal.toString() || "";
                        //如果值不為空，獲得值是漢字的全拼
                        if (tdVal) {
                            //tdVal = tdVal.substring(0, 1)
                            //                            tdVal = ZhCN_Pinyin.GetQP(tdVal);
                            //                            tdVal = tdVal.toLowerCase();
                        }
                        return tdVal;
                    }
            }
        },
        _ShowTable: function (trJqObjArr) {
            for (var n = 0, len = trJqObjArr.length; n < len; n++) {
                this._tbodyObj.appendChild(trJqObjArr[n]);
                $.isFunction(this._initConfig.OnShow) && (this._initConfig.OnSortComplete(n, trJqObjArr[n], this._tbodyObj));
            }
        },
        SelectAll: function () {

        }
    }

    WebTool.Table_Sort = _Table_Sort;
})();

/*Table*/
(function () {
    /**
    * 內部方法
    */
    function CreateTd(obj, config) {
        var width = config.width;
        var td = WebTool.CreateElement("td");
        td.style.display = (("hidden" in config && config.hidden) ? "none" : "");
        td.style.minWidth = (width ? width : "");
        td.style.width = (width ? width : "");
        td.style.maxWidth = (width ? width : "");
        obj.Td = td;

        //資料顯示
        if (obj.SetDataView != null) {
            obj.SetDataView();
        }
        td.title = config.toolstip
                    ? ((obj.ToolsTipText != "") ? obj.ToolsTipText : obj.Text)
                    : "";

        if (obj.Text != "") {            
            var span = WebTool.CreateElement("span", {
                "innerText": obj.Text
            });

            td.appendChild(span);
        }
        return obj.Td;
    }

    function _Table(Container) {
        this.View = Container;
        this.Data = [];
        this.Title = [];
        this._tableObj = null;
        this._tbodyObj = null;
        this._theadObj = null;
        this._tfooterObj = null;

        this.Table_Sort = new WebTool.Table_Sort();
        this.SortIndexOption = [];
        this.SortBindOnclickControl = [];
        this.SortRecord = [];
        this.SortJQueryArr = [];

        this.Config = {
            NullToEmptyString: true,
            ToolsTip: false,
            FixedHeader: false,
            SetFooter:false
        };

        this.PerCell = {
            /*PerTd(td)()*/
            Td: null,
            TitleObj: null,
            Text: "",
            RecordArr: [],
            /*設定View{function}*/
            SetDataView: null,
            ToolsTipText: "",
            Index: 0,
            AllTitleObj:null
        };
        this.PerTitle = {
            /*PerTd(td)()*/
            Td: null,
            TitleObj: null,
            Text: "",
            /*設定View{function}*/
            SetDataView: null,
            ToolsTipText: "",
            Index:0
        };

        this.PerFoot = {
            Data: [],
            SetDataView: null,
            ToolsTipText: "",
            Td: null,
            Text: ""
        }
    }

    _Table.prototype = {
        Enabled: true,
        DataHeight: 0,
        className: {
            Table: "",
            Title: ""
        },
        style: {
            Table: null
        },        
        Init: function () {
            this.Init = function () {
                this.Table_Sort.Init(this._tableObj, this._tbodyObj);

                this._BindSortTitleEvent();

                this.View.appendChild(this._tableObj);
            }
            $(this.View).empty();

            if (!this.Enabled) {
                return;
            }

            this.View.className += this.Config.FixedHeader ? " fixed_table_header " : "";

            this.CreateTable();

            this.Init();
        },
        SetSortArrow: function (config) {
            for (var item in config) {
                if (item in this.Table_Sort.Config) {
                    this.Table_Sort.Config[item] = config[item];
                }
            }
        },
        getIndex: function (title, columnArr) {
            var index = -1;
            for (var i = 0; i < columnArr.length; i++) {
                if (title == columnArr[i]) {
                    index = i;
                    break;
                }
            }

            return index;
        },
        CreateTable: function () {
            var table;
            this._tableObj = WebTool.CreateElement("table", {
                style: {
                    "table-layout": "fixed"
                },
                className: this.className.Table
            });

            WebTool.SetAttribute(this._tableObj, this.style.Table);

            table = this._tableObj;

            /*Title*/
            this._theadObj = WebTool.CreateElement("thead");
            this._CreateThead(this._theadObj);
            table.appendChild(this._theadObj);

            /*Content*/
            this._tbodyObj = WebTool.CreateElement("tbody", {
                style: {
                    "height": (this.DataHeight ? this.DataHeight + "px" : "")
                }
            });
            this._CreateTbody(this._tbodyObj);
            table.appendChild(this._tbodyObj);

            if (this.Config.SetFooter) {
                this._tfooterObj = WebTool.CreateElement("tfoot");
                table.appendChild(this._tfooterObj);
            }

            return table;
        },
        _CreateTbody: function (tbody) {
            var title = this.Title, tr;
            $(tbody).empty();
            /*Content*/
            if ("Record" in this.Data) {
                for (i = 0; i < this.Data.Record.length; i++) {
                    this.PerCell.RecordArr = this.Data.Record[i];
                    tr = WebTool.CreateElement("tr");
                    this.PerCell.AllTitleObj = title;
                    for (var j = 0; j < title.length; j++) {
                        this.PerCell.Text = "";
                        this.PerCell.ToolsTipText = "";
                        this.PerCell.TitleObj = title[j];
                        this.PerCell.Index = j;

                        if ("column" in title[j])
                            this.PerCell.Text = this.Data.Record[i][this.getIndex(title[j].column, this.Data.Column)];
                        if (this.Config.NullToEmptyString) {
                            this.PerCell.Text = (this.PerCell.Text == null || this.PerCell.Text == "null")
                                                ? (("nulltext" in title[j]) ? title[j].nulltext : "")
                                                : this.PerCell.Text;
                        }

                        /*Sort Record*/
                        if (!this.SortRecord[j]) {
                            this.SortRecord[j] = [];
                        }
                        this.SortRecord[j][i] = this.Table_Sort.GetValue(this.PerCell.Text, ("sort" in title[j]) ? title[j].sort.type : "");

                        tr.appendChild(CreateTd(
                            this.PerCell,
                            {
                                "hidden": title[j].hidden,
                                "width": title[j].width,
                                "toolstip": ("toolstip" in title[j] && title[j].toolstip.content)
                            }));
                    }

                    this.SortJQueryArr.push(tr);

                    tbody.appendChild(tr);
                }
            }

            return tbody;
        },
        _CreateThead: function (thead) {
            $(thead).empty();
            var tr = WebTool.CreateElement("tr"), td;
            /*Title*/
            var title = this.Title;
            for (var j = 0; j < title.length; j++) {
                this.PerTitle.Text = title[j].title;
                this.PerTitle.ToolsTipText = "";
                this.PerTitle.TitleObj = title[j];
                this.PerTitle.Index = j;
                td = CreateTd(this.PerTitle,
                            {
                                "hidden": title[j].hidden,
                                "width": title[j].width,
                                "toolstip": ("toolstip" in title[j] && title[j].toolstip.title)
                            });

                tr.appendChild(td);

                /*Sort Index And Option*/
                if ("sort" in title[j]) {
                    this.SortIndexOption.push({
                        index: j,
                        option: title[j].sort,
                        column:title[j].column
                    });
                    /*Bind Sort Td*/
                    this.SortBindOnclickControl.push(td);
                }
            }

            thead.appendChild(tr);

            return thead;
        },
        _CreateTfoot: function (tfoot) {
            if (!this.Config.SetFooter)
                return;
            $(tfoot).empty();
            var title = this.Title, txt;
            var tr = WebTool.CreateElement("tr"), td;
            /*Title*/
            if ("Record" in this.PerFoot.Data) {
                for (i = 0; i < this.PerFoot.Data.Record.length; i++) {
                    tr = WebTool.CreateElement("tr");
                    for (var j = 0; j < title.length; j++) {
                        if ("column" in title[j])
                            txt = this.PerFoot.Data.Record[i][this.getIndex(title[j].column, this.PerFoot.Data.Column)];
                        if (this.Config.NullToEmptyString) {
                            txt = (txt == null || txt == "null")
                                    ? (("nulltext" in title[j]) ? title[j].nulltext : "")
                                    : txt;
                        }

                        this.PerFoot.Text = txt;

                        tr.appendChild(CreateTd(
                            this.PerFoot,
                            {
                                "toolstip": ""
                            }));
                    }

                    tfoot.appendChild(tr);
                }
            }

            return tfoot;
        },
        _BindSortTitleEvent: function () {
            var option, index;
            for (var i = 0; i < this.SortBindOnclickControl.length; i++) {
                option = this.SortIndexOption[i].option;
                index = this.SortIndexOption[i].index;

                this.Table_Sort.SetOrder(this.SortBindOnclickControl[i],
                                        index,
                                        {
                                            DataType: option.type,
                                            DefaultOrder: ("defaultsort" in option && option.defaultsort),
                                            Desc: ("desc" in option && option.desc),
                                            Column:this.SortIndexOption[i].column
                                        },
                                        this.SortJQueryArr,
                                        this.SortRecord[index]);
            }
        },
        RefreshContent: function () {
            $(this.View).empty();

            if (!this.Enabled) {
                return;
            }

            this.SortRecord = [];
            this.SortJQueryArr = [];

            if (this._tableObj == null) {
                this.CreateTable();
            }

            this._CreateTbody(this._tbodyObj);

            this.Init();
        },
        RefreshTitle: function () {
            this.SortIndexOption = [];
            this.SortBindOnclickControl = [];

            this._CreateThead(this._theadObj);
            this._BindSortTitleEvent();
        },
        RefreshFooter: function () {
            if (!this.Config.SetFooter)
                return;

            this._CreateTfoot(this._tfooterObj);
        },
        EmptyContent:function (){
            if (!this.Enabled) {
                return;
            }
            this.Data = [];

            this.SortRecord = [];
            this.SortJQueryArr = [];

            if (this._tableObj == null) {
                this.CreateTable();
            }

            this._CreateTbody(this._tbodyObj);

            this.Init();
        }
    }
    WebTool.Table = _Table;

    /*fixed title css*/
    $(document).ready(function () {
        WebTool.addCss({
            ".fixed_table_header table": {
                "position": "relative",
                "overflow":"hidden",
                "border-collapse":"collapse"
            }
        });
        /*thead*/
        WebTool.addCss({
            ".fixed_table_header thead": {
                "position": "relative",
                "display":"block",/*seperates the header from the body allowing it to be positioned*/
                "overflow":"visible"
            }
        });
        /*tbody*/
        WebTool.addCss({
            ".fixed_table_header tbody": {
                "position": "relative",
                "display":"block",/*seperates the tbody from the header*/
                "overflow":"auto",
                "overflow-x":"hidden"
            }
        });        
    });    
} ());