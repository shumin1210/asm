﻿/// <reference path="../jquery/jquery-1.12.3.min.js" />
/// <reference path="../jquery/Tools.js" />
/// <reference path="data.js" />
/// <reference path="DataStruct.js" />
/// <reference path="webTool.js" />
/// <reference path="DayTransReport.js" />
/// <reference path="UI.js" />

function setLayout(layout) {
    Layout = layout;

    Login.Hide();
    LeftView_Trans.Hide();
    //MainPage.Hide();
    ReportTitle.Show();
    LeftView.Hide();
    RightView.Hide();
    QRView.Hide();
    Day_Trans_Report_View.Hide();
    CashInOutView.Hide();
    HomePageView.Hide();
    CashOutView.Hide();
    SetCreditView.Hide();
    CashInView.Hide();
    OnlineView.Hide();
    CheckOutView.Hide();
    CheckOutSettingView.Hide();
    CheckOutHistoryView.Hide();
    EditPasswordView.SetPassword.Hide();
    AddShareHolderView.Hide();
    AutoCheckView.Hide();
    ABCView.Hide();

    switch (Layout) {
        case Layouts.LOGIN:
            Right_Title.Set("");
            ReportTitle.Hide();
            $('#pop_win').hide();
            MainPage.Hide();
            QRView.DivAdd.style.display = "none";
            Login.Show();
            break;
        case Layouts.MAINPAGE:
            MainPage.Show();
            HomePageView.Show();
            Reporter.DataType = DataType.NONE;
            break;
        case Layouts.LAYER1:
        case Layouts.LAYER2:
        case Layouts.LAYER3:
        case Layouts.LAYER4:
            if (Reporter.DataType == DataType.GAMELIST)
                LeftView.Show();
            else if (Reporter.DataType == DataType.TRANSACTION)
                LeftView_Trans.Show();
            else
                MainPage.Show();
            RightView.Build();
            RightView.Show.Data();
            break;
        case Layouts.QRVIEW:
            QRView.Show();
            break;
        case Layouts.DAYTRANSREPORT:
            LeftView_Trans.Show();
            Day_Trans_Report_View.Show();
            break;
        case Layouts.TRANS_CONFIG:
            CheckOutSettingView.Show();
            break;
        case Layouts.MEMBERMANAGE:
            RightView.Data.MemberList.InsertFirst = true;
            Control.LeftView.Confirm(true);
            break;
        case Layouts.STOCKHOLDERMANAGE:
            RightView.Data.MemberList.InsertFirst = true;
            Control.LeftView.Confirm(true);
            break;
        case Layouts.GAMEREFUNDLIST:
            RightView.Data.MemberList.InsertFirst = true;
            Control.LeftView.Confirm(true);
            break;
        case Layouts.ONLINE:
            OnlineView.ListView(OnlineView.Data);
            OnlineView.Show();
            break;
        case Layouts.CHECKOUT:
            CheckOutView.ListView(CheckOutView.Data);
            CheckOutView.Show();
            break;
        case Layouts.CHECKOUTHISTORY:
            CheckOutHistoryView.Show();
            break;
        case Layouts.AUTOCHECK:
            AutoCheckView.Show();
            break;
        case Layouts.ABC:
            ABCView.Show();
            break;
        default:

    }

    resize();

    LoadingPanel.Hide();
}

var LeftView = function () {
    var self = {};
    var ele = Object.create(null), Data;

    var check_list = [], game_ws = [];

    self.check_list = [];
    self.check_name = "ck_gamelist";

    ele.Div = buildEle("div", {
        "className": "adjustHeight leftback",
        style: {
            display: "none",
            width: "14%",
            "min-width": "100px"            
        }
    });

    var div = buildEle("div", {
        style: {
            "text-align": "left",
            "padding": "10px",
            "padding-bottom":"0",
            "font-size": FontSizeSetting.LeftView + "px",
            "color": ColorSetting.LeftMenu
        },
        "className": "LeftView"
    });

    ele.Div.appendChild(div);

    ele.Return = buildEle("span", {
        innerText: Language.btn_Return,
        className: "pointer leftBtn",
        style: {            
            "display":"block"
        }
    });
    ele.Return.onclick = function () {
        setLayout(Layouts.MAINPAGE);
    };
    div.appendChild(ele.Return);

    ele.gameListDiv = buildEle("div", {
        id: "Left_gameList",
        style: {
            "text-align": "left",
            "color":ColorSetting.LeftMenu
        }
    });

    ele.Div.appendChild(ele.gameListDiv);

    self.Set = function () {
        $(ele.gameListDiv).empty();

        if (!Data) {
            return;
        }

        var col = Data.Column, row = Data.Record, container;

        for (var i = 0; i < row.length; i++) {
            container = buildEle("div", { style: { "padding": "5px" } });

            var checkbox = document.createElement('input');
            checkbox.type = "checkbox";
            checkbox.name = self.check_name;
            checkbox.value = row[i][0];
            checkbox.id = "ck_" + row[i][0];
            checkbox.gid = row[i][0];
            checkbox.gtitle = row[i][2];
            checkbox.gversion = row[i][3];
            checkbox.style.width = "15px";
            checkbox.checked = true;
            checkbox.onchange = function () {
                RightView.Data.MemberList.InsertFirst = true;
                Control.LeftView.Confirm(true);
            }

            var label = document.createElement('label')
            label.htmlFor = "ck_" + row[i][0];
            label.appendChild(document.createTextNode(row[i][2]));

            container.appendChild(checkbox);
            container.appendChild(label);

            ele.gameListDiv.appendChild(container);
        }

    }

    self.Init = function () {
        $("body").append(ele.Div);
    }
    self.Show = function () {
        ele.Div.style.display = "inline-block";
    }
    self.Hide = function () {
        $(ele.Div).hide();
    }

    self.Refresh = function () {
        ele.Return.innerText = Language.btn_Return;
    }

    Object.defineProperties(self, {
        Data: {
            get: function () { return Data; },
            set: function (val) { Data = val; }
        },
        CheckList: {
            get: function () { return check_list; },
            set: function (val) { check_list = val; }
        },
        game_ws: {
            get: function () { return game_ws; }
        }
    })

    return self;
}();

var LeftView_Trans = function () {
    var self = {};
    var ele = Object.create(null), Data;

    var check_list = [], game_ws = [];

    self.check_list = [];
    self.check_name = "ck_gamelist";

    ele.Div = buildEle("div", {
        "className": "adjustHeight leftback",
        style: {
            display: "none",
            width: "14%",
            "min-width": "100px"
        }
    });

    var div = buildEle("div", {
        style: {
            "text-align": "left",
            "padding": "10px",
            "font-size": FontSizeSetting.LeftView + "px",
            "color": ColorSetting.LeftMenu
        },
        "className": "LeftView"
    }), subMenuDiv = buildEle("div");

    ele.Div.appendChild(div);

    ele.Return = buildEle("span", {
        innerText: Language.btn_Return,
        className: "pointer leftBtn",
        style: {
            "width": "100%",
            display: "block"
        }
    });
    ele.Return.onclick = function () {
        LeftView_Trans.Hide();
        setLayout(Layouts.MAINPAGE);
    };
    div.appendChild(ele.Return);

    ele.reporQue = buildEle("span", {
        innerText: Language.subMenu_reportQue,
        className: "pointer leftBtn",
        style: {
            "width": "100%",
            display: "block"
        }
    });
    ele.reporQue.onclick = function () {
        RightView.Data.MemberList.InsertFirst = true;
        Control.LeftView.FirstConfirm();
        //Control.LeftView.Confirm(true);
    };
    div.appendChild(ele.reporQue);

    ele.day_reporQue = buildEle("span", {
        innerText: Language.subMenu_dayreportQue,
        className: "pointer leftBtn",
        style: {
            "width": "100%",
            display: "none"
        }
    });
    ele.day_reporQue.onclick = function () {
        setLayout(Layouts.DAYTRANSREPORT);
    };
    div.appendChild(ele.day_reporQue);

    ele.report_Cfg = buildEle("span", {
        innerText: Language.subMenu_reoprtCfg,
        className: "pointer",
        style: {
            "width": "100%",
            display: "block"
        }
    });
    ele.report_Cfg.onclick = function () {
        setLayout(Layouts.TRANS_CONFIG);
    };
    div.appendChild(ele.report_Cfg);

    self.Init = function () {
        $("body").append(ele.Div);
    }
    self.Show = function () {
        if (Reporter.Role == 0) {
            ele.report_Cfg.style.display = "none";
            ele.day_reporQue.style.display = "none";
        } else {
            ele.report_Cfg.style.display = "block";
            ele.day_reporQue.style.display = "block";
        }

        ele.report_Cfg.style.display =
            ele.day_reporQue.style.display = "none";
        ele.Div.style.display = "inline-block";

    }
    self.Hide = function () {
        $(ele.Div).hide();
    }

    self.Refresh = function () {
        ele.Return.innerText = Language.btn_Return;
        ele.reporQue.innerText = Language.subMenu_reportQue;
        ele.day_reporQue.innerText = Language.subMenu_dayreportQue;
        ele.report_Cfg.innerText = Language.subMenu_reoprtCfg;
    }

    Object.defineProperties(self, {
        Data: {
            get: function () { return Data; },
            set: function (val) { Data = val; }
        },
        CheckList: {
            get: function () { return check_list; },
            set: function (val) { check_list = val; }
        },
        game_ws: {
            get: function () { return game_ws; }
        }
    })

    return self;
}();

var RightPage = function () {
    var self = {}

    self.Div = buildEle("div", {
        style: {
            "display": "inline-block"
        }
    });

    self.Init = function () {
        $("body").append(self.Div);
    }

    self.Append = function (obj) {
        self.Div.appendChild(obj);
    }

    return self;
}();

var Right_Title = function () {
    var self = {}, ele = {};

    self.Div = buildEle("div", {
        style: {
            //"display": "inline-block",
            "padding": "5px",
            //"width":"70%"
        }
    });
    ele.span_title = buildEle("span");
    self.Div.appendChild(ele.span_title);

    self.Init = function () {
        RightPage.Append(self.Div);
        //$("body").append(self.Div);
    }

    self.Set = function (msg) {
        ele.span_title.innerText = msg;
    }
    return self;
}();

var RightView = function () {
    var self = {};
    var ele = {}, Data_Addtext = [], DataTop = [];
    var Column = [], table;

    self.id = {
        Div: {
            Container: "RightView_DIV"
        },
        Table: {
            Bottom: "RightView_bottom_table",
            Top: "RightView_top_table"
        },
        Radio: {
            Search: "RightView_Search_Radio_Trans"
        }
    }

    self.Data = {
        KeyColumn: "UserID",
        NameColumn: "Name",
        TopTree: [],
        DataTop: {
            Column: [],
            Record: [],
            Trans: []
        },
        CompanyList: {
            Currency: "",
            ReceivePlayerID: 0,
            ReceivePlayerName: "",
            Column: [],
            Record: [],
            List: [],
            Add: function (data, GID, GName) {
                var isFind = -1, UserIDIndex = this.Column.indexOf(self.Data.KeyColumn), UserNameIndex = this.Column.indexOf(self.Data.NameColumn);
                for (var i = 0; i < this.List.length; i++) {
                    if (this.List[i].UserID == data[UserIDIndex]) {
                        isFind = i;
                        break;
                    }
                }

                if (isFind != -1) {
                    this.List[i].Add(data, GID, GName, this.Column);
                } else {
                    var tmp = GameRecordPack.UserTotal.Get(data[UserIDIndex], data[UserNameIndex]);
                    tmp.Add(data, GID, GName, this.Column);
                    this.List.push(tmp);

                    delete tmp;
                }
            },
            Init: function () {
                for (var i = 0; i < this.List.length; i++) {
                    for (var j = 0; j < this.List[i].GameRecord.length; j++) {
                        GameRecordPack.GameRecord.Add(this.List[i].GameRecord[j]);
                    }

                    GameRecordPack.UserTotal.Add(this.List[i]);
                }

                this.List = [];

                return this;
            }
        },
        MemberList: {
            ReceivePlayerID: 0,
            ReceivePlayerName: "",
            Column: [],
            Record: [],
            InsertFirst: true,
            Add: function (data) {
                var UserIDIndex = this.Column.indexOf(self.Data.KeyColumn);
                for (var i = 0; i < data.length; i++) {
                    for (var j = 0; j < data[i].length; j++) {
                        if (UserIDIndex == j)
                            this.Record[j] = Number(data[i][j]);
                        else
                            this.Record[j] = isNaN(data[i][j]) ? data[i][j] : this.Record[j] + Number(data[i][j]);
                    }
                }
            },
        },
        GameList: {
            ReceivePlayerID: 0,
            ReceivePlayerName: ""
        },
        GameDetail: {
            ReceiveGID: 0,
            ReceiveGtitle: "",
            Column: [],
            Record: []
        }
    }

    ele.Div = buildEle("div", {
        id: self.id.Div.Container,
        style: {
            "display": "none",
            "width": "84%",
            "padding-top": "10px",
            "padding-left": "1%"
        }
    });

    self.searchDiv = buildEle("div", { id: "searchDiv", style: { "margin-top": "10px" } });

    //self.searchTimeDiv = buildEle("div", { style: { "margin-bottom": "10px" } });

    self.SelectObj = CreateView.Date.Calendar.Control(CreateView.Date.Type.DateTime);

    self.searchTimeDiv = self.SelectObj.Container;

    self.searchDiv.appendChild(self.searchTimeDiv);
    
    self.searchControlDiv_Total = SearchUI.Normal();

    self.searchDiv.appendChild(self.searchControlDiv_Total);

    self.searchControlDiv_Trans = SearchUI.Trans();
    self.searchDiv.appendChild(self.searchControlDiv_Trans);
    /*Print Btn*/
    ele.printBtn = buildEle("input", {
        type: "button",
        value: Language.btn_Print,
        className: "confirmbtn leftBtn",
        style: {
            "margin": "0",
            "margin-left":"3px",
            "padding": "3px",
            display: "none"
        }
    });

    ele.printBtn.onclick = function () { Control.RightView.Print(); }

    self.searchControlDiv_Trans.appendChild(ele.printBtn);


    /*Duration*/
    self.DurationDiv = buildEle("div", { style: { "margin-top": "10px" } });
    self.DurationDiv.appendChild(buildEle("span", { id: "durationTitle", innerText: "" }));
    self.DurationDiv.appendChild(buildEle("span", { id: "duration", innerText: "" }));

    /*page*/
    self.PageDiv = buildEle("div", { style: { "margin-top": "15px", "text-align": "center" } });
    self.pageIndexSelect = buildEle("select", { id: "pageIndex", style: { "display": "none" } });

    self.PageDiv.appendChild(self.pageIndexSelect);

    /*----------------------------*/

    self.Search = {
        StartTime: "",
        EndTime: "",
        PageIndex: [1, 1, 1, 1],
        PageCount: [],
        Id: [1, 1, 1, 1]
    };

    ele.Div_Tree = buildEle("div", {
        style: {
            "width": "100%"
        }
    });

    ele.Table_top = buildEle("table", {
        id: self.id.Table.Top,
        style: { "margin-top": "10px" }
    });
    self.Table_Bottom = buildEle("table", {
        id: self.id.Table.Bottom,
        style: { "margin-top": "10px" }
    });

    ele.btn_return = buildEle("input", {
        "type": "button",
        "value": Language.btn_Return,
        "style": {
            "display": "none"
        }
    })
    ele.btn_return.onclick = function () {
        setLayout(Layout - 1);
    }

    ele.Div.appendChild(ele.btn_return);

    ele.Div.appendChild(ele.Div_Tree);
    ele.Div.appendChild(self.searchDiv);
    ele.Div.appendChild(self.DurationDiv);

    ele.Div.appendChild(ele.Table_top);
    ele.Div.appendChild(self.Table_Bottom);


    DataSheet.div = buildEle("div");
    ele.Div.appendChild(DataSheet.div);

    ele.Div.appendChild(self.PageDiv);

    /* AddEditPanel ------------------------------- */
    self.AddEditPanel = buildEle("div", {
        style: {
            "display": "none",
            "width": "78%",
            "padding-top": "10px",
            "padding-left": "1%"
        }
    });

    AddEditPack.Create(self.AddEditPanel);
    /* ------------------------------------- */

    function TopTreePush() {
        if (pageClick) {
            pageClick = false;
            return;
        }

        var isFind = false;

        for (var i = 0; i < RightView.Data.TopTree.length; i++) {
            if (RightView.Data.TopTree[i].ID == self.Data.CompanyList.ReceivePlayerID && RightView.Data.TopTree[i].Layer == Layout) {
                isFind = true;
                break;
            }
        }

        if (!isFind) {
            switch (Layout) {
                case Layouts.LAYER1:
                    RightView.Data.TopTree.push({
                        Name: self.Data.CompanyList.ReceivePlayerName + RightView.Col_AddText[0],
                        ID: self.Data.CompanyList.ReceivePlayerID,
                        Layer: Layouts.LAYER1,
                        OnClick: function () {
                            self.Data.MemberList.InsertFirst = true;
                            Control.LeftView.Confirm(false, this.ID);
                        }
                    });
                    break;
                case Layouts.LAYER2:
                    var title_index = self._create.getRowFirstTitleUndex();

                    if (Reporter.Role >= 1) {
                        RightView.Data.TopTree.push({
                            Name: self.Data.CompanyList.ReceivePlayerName + Language.RowFirst_Member[title_index],
                            ID: self.Data.CompanyList.ReceivePlayerID,
                            Layer: Layouts.LAYER2,
                            OnClick: function () { Control.RightView.Layer1.Row({ currentTarget: { UserID: RightView.Data.CompanyList.ReceivePlayerID } }); }
                        });
                    }
                    break;
                case Layouts.LAYER3:
                    RightView.Data.TopTree.push({
                        Name: self.Data.GameList.ReceivePlayerName,
                        ID: self.Data.GameList.ReceivePlayerID,
                        Layer: Layouts.LAYER3,
                        OnClick: function () {
                            Control.RightView.Layer3({
                                currentTarget: {
                                    UserID: RightView.Data.GameList.ReceivePlayerID,
                                    Layer2UserID: RightView.Data.CompanyList.ReceivePlayerID
                                }
                            });
                            //setLayout(Layouts.LAYER3);
                        }
                    });
                    break;
                case Layouts.LAYER4:
                    RightView.Data.TopTree.push({
                        Name: self.Data.GameDetail.ReceiveGtitle,
                        ID: self.Data.GameDetail.ReceiveGID,
                        Layer: Layouts.LAYER4,
                        OnClick: function () { }
                    });
                    break;
            }

        }
    }

    self.Build = function () {
        $(ele.Div_Tree).empty();
        $(ele.Table_top).empty();
        $(self.Table_Bottom).empty();
        $(DataSheet.div).empty();
        $(self.searchDiv).hide();
        $(self.DurationDiv).show();
        ele.btn_return.style.display = "none";
        $(self.pageIndexSelect).hide();

        document.getElementById("duration").innerText = self.Search.StartTime + " ~ " + (self.Search.EndTime == "" ? getNow() : self.Search.EndTime);

        var last = Reporter.Role == 1 && Reporter.ID == self.Data.CompanyList.ReceivePlayerID ? [Language.Column_Manage] : [];
        var _col,_struct=RightView_DataStruct.GetDataStruct();

        _col = _struct.ColumnTitleText;
        Right_Title.Set(_struct.Title);

        switch (Layout) {
            case Layouts.LAYER1:                
                $(self.DurationDiv).hide();
                
                if (Reporter.DataType == DataType.MEMBERMANAGE || Reporter.DataType == DataType.STOCKHOLDERMANAGE) {
                    //$(self.searchDiv).hide();
                } else {
                    $(self.searchDiv).show();
                }

                var top_title = _col.top;
                
                CreateTitle(ele.Table_top, top_title, {
                    pre: [Language.txt_Status],
                    last: Reporter.DataType == DataType.MEMBERMANAGE || Reporter.DataType == DataType.STOCKHOLDERMANAGE ? last : []
                });

                CreateTitle(self.Table_Bottom, Reporter.DataType == DataType.MEMBERMANAGE ? top_title : _col.bottom, {
                    pre: Reporter.Permission || Reporter.Role == 2 ? [""] : [],
                    last: Reporter.DataType == DataType.MEMBERMANAGE ? [Language.Column_Manage] : []
                });
                self._create.CompanyList(self.Table_Bottom, false, 0, true);

                TopTreePush();

                break;
            case Layouts.LAYER2:
                if (Reporter.DataType == DataType.MEMBERMANAGE || Reporter.DataType == DataType.STOCKHOLDERMANAGE) {
                    $(self.DurationDiv).hide();
                }

                CreateTitle(ele.Table_top, _col.top, {
                    pre: [Language.txt_Status],
                });

                CreateTitle(self.Table_Bottom, _col.top, {
                    last: Reporter.DataType == DataType.MEMBERMANAGE || Reporter.DataType == DataType.STOCKHOLDERMANAGE
                        ? [Language.Column_Manage] : []
                });
                self._create.CompanyList(self.Table_Bottom, false, 0, true);

                TopTreePush();

                if (!Reporter.Role) {
                    Control.RightView.GameList({ currentTarget: { UserID: Reporter.ID } });
                    return;
                }
                break;
            case Layouts.LAYER3:
                if (!Reporter.Role) {
                    $(self.searchDiv).show();
                    $(self.DurationDiv).hide();
                }

                if (Reporter.DataType == DataType.TRANSACTION || Reporter.DataType == DataType.GAMEREFUNDLIST) {
                    /*直接進layer 4*/
                    Control.RightView.GameDetail({
                        currentTarget: {
                            gid: "", pageindex: 1,
                            column: RightView.Data.CompanyList.Column[0],
                            record: RightView.Data.GameDetail.Record
                        }
                    });
                    break;
                }

                TopTreePush();

                CreateTitle(ele.Table_top, _col.top, {
                    pre: [Language.txt_Status],
                });

                CreateTitle(self.Table_Bottom, _col.bottom);

                self._create.GameList(self.Table_Bottom);

                break;
            case Layouts.LAYER4:
                TopTreePush();

                DataSheet.Create(DataSheet._obj, self.Data.GameDetail.ReceiveGID);
                
                CreateTitle(ele.Table_top, _col.top, {
                    pre: [Language.txt_Status],
                });
                self._create.GameDetail(ele.Table_top);

                break;
            default:

        }

        self._create.PageIndex();
        self._create.Tree();

    }
    self._create = {
        Tree: function () {
            var tree_arr = self.Data.TopTree, tree_ele, container;

            for (var i = 0; i < tree_arr.length; i++) {
                container = buildEle("div", {
                    style: { "margin-right": "5px", "display": "inline-block" }
                });

                container.appendChild(document.createTextNode(">>"));

                tree_ele = buildEle("a", {
                    innerText: tree_arr[i].Name
                });

                if (i != tree_arr.length - 1) {
                    tree_ele.className = "pointer";
                    tree_ele.arrIndex = i;
                    tree_ele.arrName = tree_arr[i].Name;
                    tree_ele.arrID = tree_arr[i].ID;
                    tree_ele.onclick = function (evt) {
                        var arr_Index = evt.currentTarget.arrIndex;
                        var tmp = [];
                        for (var i = 0; i < arr_Index; i++) {
                            tmp.push(self.Data.TopTree[i]);
                        }

                        self.Data.TopTree = tmp;

                        tree_arr[i].OnClick();
                    };
                }

                container.appendChild(tree_ele);
                ele.Div_Tree.appendChild(container);
            }

        },
        FixedColumn: ["Place", "CanUseCredit"],
        CompanyList: function (tbl, clickabled, lv, groupName, isAddManage) {
            var row, colData, col, IsRed = false, IsNumber = false;

            col = RightView_DataStruct.GetColumn();
            
            var _i_status = self.Data.CompanyList.Column.indexOf("Status"), top_Status = 1;
            if (_i_status != -1)
                top_Status = self.Data.MemberList.Record[_i_status];

            //Initial Top Record
            if (!lv)
                self.Data.DataTop.Record = zeroArr(col.length);

            var _record = [], _column = [];

            for (var i = 0; i < self.Data.CompanyList.List.length; i++) {
                _record.push(RightView.Data.CompanyList.List[i].GetTotal());
                _column.push(RightView.Data.CompanyList.List[i].GetColumn());
            }

            var _tree = "", KeyIndex, treeNode, rowIndex = -1, colIndex = -1;

            //get parent node index
            var parentNode = document.getElementById(getTreeNodeName(self.Data.CompanyList.ReceivePlayerID));
            if (parentNode)
                rowIndex = parentNode.rowIndex;

            var cell, txt;

            for (var j = 0; j < _record.length; j++) {
                //get insert row index
                row = tbl.insertRow(rowIndex == -1 ? j + 1 : rowIndex + 1);                
                KeyIndex = _column[j].indexOf(self.Data.KeyColumn);

                if (Layout == Layouts.LAYER1 && (Reporter.Permission || Reporter.Role == 2)) {
                    /* tree node Setting---------------- */
                    treeNode = new _TreeNode();

                    row.id = getTreeNodeName(_record[j][KeyIndex]);
                    row.className = groupName ? groupName : "";

                    treeNode.Set(_record[j][KeyIndex], self.Data.CompanyList.ReceivePlayerID, lv);

                    row.appendChild(treeNode.CreateTD());
                }
                /* ---------------------------------- */

                row.style.color = ColorSetting.TableTitle.Content;

                /* column Td ------------------- */
                for (var i = 0; i < col.length; i++) {
                    colIndex = _column[j].indexOf(col[i]);

                    colData = colIndex == -1 ? "" : _record[j][colIndex];
                    colData = isNaN(colData) ? colData : Number(colData);

                    IsRed = false;
                    IsNumber = false;

                    if (col[i] == "Currency") {
                        txt = colData == -1 ? "" : Language.Cell_Currency[colData - 1];
                    } else if (col[i] == "CompanyBonus") {
                        if (Number(colData) < 0)
                            IsRed = true;

                        IsNumber = true;
                        var _index = _column[j].indexOf("ShopBonus");
                        txt = numberWithCommas(colData) + (_index != -1 ? " (" + _record[j][_index] + "%)" : "");
                    } else if (col[i] == "AgentRefund") {
                        IsNumber = true;
                        var _index = _column[j].indexOf("AgentRefundRate");
                        txt = numberWithCommas(colData) + (_index != -1 ? " (" + _record[j][_index] + "%)" : "");
                    } else if (col[i] == "RemainRefund") {
                        IsNumber = true;
                        txt = colData == -1 ? "-" : numberWithCommas(colData);
                    } else if (typeof colData == "number") {
                        if (Number(colData) < 0)
                            IsRed = true;
                        IsNumber = true;
                        txt = numberWithCommas(colData);
                    } else
                        txt = colData + Data_Addtext[i];

                    if (i == 0) {
                        if (Layout == Layouts.LAYER1)
                            if (Reporter.Permission || Reporter.Role == 2)
                                createTd(txt, row, true, IsNumber, function (evt) {
                                    self.Data.MemberList.InsertFirst = true;
                                    Control.LeftView.Confirm(false, evt.currentTarget.UserID);
                                }, { UserID: _record[j][KeyIndex] }, { "color": "red" });
                            else
                                createTd(txt, row, true, IsNumber);

                        else if (Layout == Layouts.LAYER2) {
                            if (Reporter.DataType == DataType.MEMBERMANAGE || Reporter.DataType == DataType.STOCKHOLDERMANAGE)
                                createTd(txt, row, true, IsNumber);
                            else
                                createTd(txt, row, true, IsNumber, Control.RightView.GameList, { UserID: _record[j][KeyIndex] }, { "color": "red" });
                        }
                    } else {
                        createTd(txt, row, true, IsNumber, null, null, { "color": IsRed ? "red" : "" });
                    }

                    //Top Data
                    if (this.FixedColumn.indexOf(col[i]) == -1)
                        self.Data.DataTop.Record[i] += colData;
                }

                /*Manage Btn*/
                if (Reporter.DataType == DataType.MEMBERMANAGE) {
                    cell = document.createElement("td");
                    cell.style.textAlign = "right";

                    if (top_Status == 0 && (Layout == Layouts.LAYER1 || Layout == Layouts.LAYER2)) {
                        var btn_struct = DataStruct_MemberManage.Btn;

                        //Edit Btn
                        if (Reporter.ID == self.Data.CompanyList.ReceivePlayerID && Reporter.Role != 2) 
                            cell.appendChild(btn_struct.Edit(_record[j][KeyIndex]));
                        
                        //Delete Btn 
                        if (Reporter.isAdmin)                             
                            cell.appendChild(btn_struct.Delete(_record[j][KeyIndex]));
                        
                        /*Can Ban*/
                        //Ban Btn/ReRight Btn                        
                        cell.appendChild(btn_struct.BanOrReRight(_record[j][_i_status], _record[j][KeyIndex]));

                        if (Reporter.ID == self.Data.CompanyList.ReceivePlayerID && Layout == Layouts.LAYER2) {
                            /*Can Cash In/Out*/                            
                            cell.appendChild(btn_struct.CashIn(_record[j][KeyIndex]));

                            cell.appendChild(btn_struct.CashOut(_record[j][KeyIndex]));
                        }

                        /*Set Credit*/
                        if (Reporter.ID == self.Data.CompanyList.ReceivePlayerID)                             
                            cell.appendChild(btn_struct.SetCredit(_record[j][KeyIndex]));
                        
                    }

                    row.appendChild(cell);
                } else if (Reporter.DataType == DataType.STOCKHOLDERMANAGE) {
                    if (top_Status == 0 && (Layout == Layouts.LAYER2)) {
                        var btn_struct = DataStruct_MemberManage.Btn;

                        cell = document.createElement("td");
                        cell.style.textAlign = "right";

                        //Edit Btn 
                        if (Reporter.ID == self.Data.CompanyList.ReceivePlayerID)                             
                            cell.appendChild(btn_struct.EditShareHolder(_record[j][KeyIndex]));
                        
                        //Delete Btn 
                        if (Reporter.isAdmin)                             
                            cell.appendChild(btn_struct.Delete(_record[j][KeyIndex]));
                        
                        row.appendChild(cell);
                    }
                                        
                }

                if (rowIndex != -1) {
                    rowIndex++;
                }
            }

            var title_index = this.getRowFirstTitleUndex();

            /* Row One*//*PS:第一行會員紅利屬於公司紅利*/
            if (Layout == Layouts.LAYER1 && self.Data.MemberList.InsertFirst) {
                self.Data.MemberList.InsertFirst = false;
                IsNumber = false;
                row = tbl.insertRow(1);
                row.style.color = ColorSetting.TableTitle.Content;

                if (Reporter.Permission || Reporter.Role == 2)
                    createTd("", row, false, IsNumber);

                KeyIndex = RightView.Data.MemberList.Column.indexOf(self.Data.KeyColumn);

                if (RightView.Data.MemberList.Record.length > 0) {

                    createTd(Language.RowFirst_Member[title_index], row, true, IsNumber, Control.RightView.Layer1.Row, { UserID: RightView.Data.MemberList.Record[KeyIndex] }, { "color": ColorSetting.Column.RowOne });

                    /* column Td ------------------- */
                    for (var j = 1; j < col.length; j++) {
                        colIndex = RightView.Data.MemberList.Column.indexOf(col[j]);

                        colData = colIndex == -1 ? "" : RightView.Data.MemberList.Record[colIndex];
                        colData = isNaN(colData) ? colData : Number(colData);

                        IsRed = false;
                        IsNumber = false;

                        if (col[j] == "Currency") {
                            txt = Language.Cell_Currency[self.Data.CompanyList.Currency - 1];
                        } else if (col[j] == "CanUseCredit" || col[j] == "LastLoginTime") {
                            txt = "-";
                        } else if (this.FixedColumn.indexOf(colData[j]) != -1) {
                            txt = colData + Data_Addtext[j];
                        } else if (col[j] == "CompanyBonus") {
                            if (Number(colData) < 0)
                                IsRed = true;

                            IsNumber = true;
                            var _index = RightView.Data.MemberList.Column.indexOf("ShopBonus");
                            txt = numberWithCommas(colData) + (_index != -1 ? " (" + RightView.Data.MemberList.Record[_index] + "%)" : "");
                        } else if (typeof colData == "number") {
                            if (Number(colData) < 0)
                                IsRed = true;

                            IsNumber = true;
                            txt = numberWithCommas(colData);
                        }
                        else
                            txt = colData + Data_Addtext[j];

                        createTd(txt, row, true, IsNumber, null, null, { "color": IsRed ? "red" : "" });

                        //Top Data
                        if (this.FixedColumn.indexOf(col[i]) == -1)
                            self.Data.DataTop.Record[j] += colData;
                    }

                    if (Reporter.DataType == DataType.TRANSACTION) {
                        var col_name = "MemberCost";
                        colIndex = RightView.Data.MemberList.Column.indexOf(col_name);
                        var r_index = col.indexOf(col_name);
                        if (colIndex != -1) {
                            self.Data.DataTop.Record[r_index] = numberWithCommas(Number(RightView.Data.MemberList.Record[colIndex]));
                        }
                    } else if (Reporter.DataType == DataType.MEMBERMANAGE) {
                        var col_name = "LastLoginTime";
                        colIndex = RightView.Data.MemberList.Column.indexOf(col_name);
                        var r_index = col.indexOf(col_name);
                        if (colIndex != -1) {
                            self.Data.DataTop.Record[r_index] = RightView.Data.MemberList.Record[colIndex];
                        } else
                            self.Data.DataTop.Record[r_index] = "-";
                    }
                } else {
                    createTd(Language.RowFirst_Member[title_index], row, true);
                    for (var j = 0; j < col.length; j++) {

                        txt = col[j] == "Currency" ? Language.Cell_Currency[self.Data.CompanyList.Currency - 1] : "";

                        createTd(txt, row, true);
                    }
                }

                if (Reporter.DataType == DataType.MEMBERMANAGE)
                    createTd("", row, false);
            }

            /* Top Column Td ---------------------------- */
            if (!lv) {
                self.Data.DataTop.Record[0] = Layout == Layouts.LAYER2 ? self.Data.CompanyList.ReceivePlayerName + Language.RowFirst_Member[title_index] : self.Data.CompanyList.ReceivePlayerName;

                row = document.createElement("tr");
                row.style.color = ColorSetting.TableTitle.Content;
                /*Add self Credit To CreditSum && Update RemainRefund*/
                if (Layout == Layouts.LAYER1 && Reporter.DataType == DataType.MEMBERMANAGE && RightView.Data.DataTop.Trans.length > 0) {
                    var computeCol = "CreditSum", computeCol2 = "CanUseCredit";
                    var recordIndex = col.indexOf(computeCol);
                    colIndex = self.Data.CompanyList.Column.indexOf(computeCol);
                    var recordIndex2 = col.indexOf(computeCol2);

                    if (colIndex != -1 && recordIndex != -1 && recordIndex2 != -1 && !isNaN(self.Data.DataTop.Record[recordIndex]) && !isNaN(self.Data.DataTop.Record[recordIndex2]))
                        self.Data.DataTop.Record[recordIndex] = Number(self.Data.DataTop.Record[recordIndex]) + Number(self.Data.DataTop.Record[recordIndex2]);

                    recordIndex2 = self.Data.MemberList.Column.indexOf(computeCol);
                    if(recordIndex2 != -1)
                        self.Data.DataTop.Record[recordIndex] = Number(RightView.Data.DataTop.Trans[0][recordIndex2]);

                    computeCol = "CreditRemain";
                    recordIndex2 = self.Data.MemberList.Column.indexOf(computeCol);
                    recordIndex = col.indexOf(computeCol);
                    if (recordIndex2 != -1)
                        self.Data.DataTop.Record[recordIndex] = Number(RightView.Data.DataTop.Trans[0][recordIndex2]);

                }

                /*Update Bonus*/
                if (Layout == Layouts.LAYER1 && (Reporter.DataType == DataType.TRANSACTION || Reporter.DataType == DataType.GAMELIST) && self.Data.DataTop.Trans.length > 0) {
                    this.UpdateTransTopData(col);
                }

                /*Status*/
                if (_i_status != -1) {
                    if (top_Status != 0)
                        createTd(Language.Column_Status[top_Status], row, true, false, null, null, { color: ColorSetting.Column.Status });
                    else
                        createTd(Language.Column_Status[top_Status], row, true);
                } else
                    createTd("", row, false);

                for (var i = 0; i < col.length; i++) {
                    IsRed = false;
                    IsNumber = false;

                    if (i == col.length - 1 && Layout == Layouts.LAYER1 && Reporter.DataType == DataType.GAMELIST) {
                        var DB_DynamicField = "Level_Bonus", _data;
                        for (var j = Reporter.MaxLevel - 1; j > Reporter.Level; j--) {
                            colIndex = self.Data.CompanyList.Column.indexOf(DB_DynamicField.replace('_', j));

                            if (colIndex == -1)
                                continue;

                            _data = Number(RightView.Data.MemberList.Record[colIndex]);

                            if (_data < 0)
                                IsRed = true;

                            createTd(numberWithCommas(_data), row, true, true, null, null, { "color": IsRed ? "red" : "" });
                        }

                    }

                    IsRed = false;
                    IsNumber = false;

                    if (this.FixedColumn.indexOf(col[i]) != -1) {
                        var _i = self.Data.CompanyList.Column.indexOf(col[i]);
                        if (_i == -1)
                            createTd("", row, false);
                        else {
                            var tmp;
                            if (!isNaN(self.Data.MemberList.Record[_i])) {
                                if (Number(self.Data.DataTop.Record[i]) < 0)
                                    IsRed = true;
                                IsNumber = true;
                                tmp = numberWithCommas(self.Data.DataTop.Record[i]);
                            }
                            else
                                tmp = self.Data.DataTop.Record[i];
                            createTd(tmp + Data_Addtext[i], row, true, IsNumber, null, null, { "color": IsRed ? "red" : "" });
                        }
                    }
                    else {
                        if (col[i] == "Currency")
                            createTd(Language.Cell_Currency[self.Data.CompanyList.Currency - 1], row, true)
                        else if (col[i] == "LastLoginTime" && Layout == Layouts.LAYER2)
                            createTd("-", row, true);
                        else if (col[i] == "AgentRefund")
                            createTd(numberWithCommas(self.Data.DataTop.Record[i]), row, true, true);
                        else {
                            var tmp;
                            if (Reporter.DataType == DataType.MEMBERMANAGE && (col[i] == "CreditRemain" || col[i] == "CreditSum") && Layout == Layouts.LAYER1) {
                                tmp = numberWithCommas(self.Data.DataTop.Record[i]);
                            }else if (!isNaN(self.Data.DataTop.Record[i])) {
                                if (Number(self.Data.DataTop.Record[i]) < 0)
                                    IsRed = true;
                                IsNumber = true;
                                tmp = numberWithCommas(self.Data.DataTop.Record[i]);
                            }
                            else
                                tmp = self.Data.DataTop.Record[i];

                            createTd(tmp + Data_Addtext[i], row, true, IsNumber, null, null, { "color": IsRed ? "red" : "" });
                        }
                    }
                }

                /*Manage btn*/
                if ((Reporter.DataType == DataType.MEMBERMANAGE || Reporter.DataType == DataType.STOCKHOLDERMANAGE)
                    && Layout == Layouts.LAYER1 && Reporter.Role == 1 && Reporter.ID == self.Data.CompanyList.ReceivePlayerID) {
                    cell = buildEle("td");
                    cell.style.textAlign = "center";

                    var DataSturctAdd = DataStruct_MemberManage.Btn.Add;

                    if (Reporter.DataType == DataType.MEMBERMANAGE) {
                        if (top_Status == 0 && Reporter.Permission) {
                            /*add agent*/
                            if (Reporter.Level < Reporter.MaxLevel - 1 && Reporter.Level != 0)                                 
                                cell.appendChild(DataSturctAdd.Create(DataSturctAdd.Type.Agnet));
                            
                            /*add member*/
                            cell.appendChild(DataSturctAdd.Create(DataSturctAdd.Type.Member));

                        }

                        if (top_Status == 0) {
                            /*Edit pwd*/
                            if (Layout == Layouts.LAYER1)                                 
                                cell.appendChild(DataStruct_MemberManage.Btn.EditPassword());
                            
                        }
                    } else if (Reporter.DataType == DataType.STOCKHOLDERMANAGE) {
                        /*add shareholder*/
                        if (top_Status == 0 && Reporter.Permission)                             
                            cell.appendChild(DataSturctAdd.Create(DataSturctAdd.Type.ShareHolder));
                        
                    }
                    row.appendChild(cell);
                }
                ele.Table_top.appendChild(row);
            }

            return tbl;
        },
        GameList: function (tbl) {
            var row, colData, col, IsRed = false, IsNumber = false;

            col = RightView_DataStruct.GetColumn();

            //Initial Top Record            
            self.Data.DataTop.Record = zeroArr(col.length);

            var _record = self.Data.CompanyList.Record, _column = self.Data.CompanyList.Column;

            var _tree = "", KeyIndex, GIDIndex, rowIndex = -1, colIndex = -1, ModeIndex;

            var cell, txt, noRecord = false;

            for (var j = 0; j < _record.length; j++) {
                //get insert row index
                row = tbl.insertRow(j + 1);
                KeyIndex = _column[j].indexOf(self.Data.KeyColumn);
                GIDIndex = _column[j].indexOf("GID");
                ModeIndex = _column[j].indexOf("Mode");

                /* column Td ------------------- */
                for (var i = 0; i < col.length; i++) {
                    colIndex = _column[j].indexOf(col[i]);

                    colData = colIndex == -1 ? "" : _record[j][colIndex];
                    colData = isNaN(colData) ? colData : Number(colData);

                    if (_record[j][GIDIndex] == "") {
                        noRecord = true;
                        continue;
                    }

                    IsRed = false;
                    IsNumber = false;

                    if (i == 0) {
                        createTd(colData + Data_Addtext[i], row, true, IsNumber, Control.RightView.GameDetail, {
                            UserID: _record[j][KeyIndex], gid: _record[j][GIDIndex],
                            mode: ((ModeIndex != -1 && (Reporter.DataType == DataType.TRANSACTION || Reporter.DataType == DataType.MEMBERMANAGE)) ? _record[j][ModeIndex] : ""),
                            record: _record[j], column: _column[j],
                            pageindex: 1
                        }, { "color": ColorSetting.Column.RowOne });
                    } else if (col[i] == "Currency") {
                        if (colData == -1 && !Reporter.Role)
                            createTd(Language.Cell_Currency[RightView.Data.CompanyList.Currency - 1], row, true);
                        else
                            createTd(colData == -1 ? "" : Language.Cell_Currency[colData - 1], row, true);
                    }
                    else if (typeof colData == "number") {
                        if (Number(colData) < 0)
                            IsRed = true;

                        IsNumber = true;
                        createTd(numberWithCommas(colData) + Data_Addtext[i], row, true, IsNumber, null, null, { "color": IsRed ? "red" : "" });
                    }
                    else
                        createTd(colData + Data_Addtext[i], row, true);

                    //Top Data
                    self.Data.DataTop.Record[i] += colData;
                }
            }

            var title_index = this.getRowFirstTitleUndex();

            /* Top Column Td ---------------------------- */
            if (DataType.GAMELIST == Reporter.DataType) {
                self.Data.DataTop.Record[2] = self.Data.GameList.ReceivePlayerName + " " + Language.RowFirst_Member[0];
            } else {
                self.Data.DataTop.Record[1] = self.Data.GameList.ReceivePlayerName + " " + Language.RowFirst_Member[title_index];
            }

            row = document.createElement("tr");

            /*Status*/
            var _i_status = self.Data.CompanyList.Column[0].indexOf("Status");
            if (_i_status != -1) {
                if (self.Data.CompanyList.Record[0][_i_status] != 0)
                    createTd(Language.Column_Status[self.Data.CompanyList.Record[0][_i_status]], row, true, false, null, null, { color: ColorSetting.Column.Status });
                else
                    createTd(Language.Column_Status[self.Data.CompanyList.Record[0][_i_status]], row, true);
            } else
                createTd("", row, false);

            for (var i = 1; i < col.length; i++) {
                IsRed = false;
                IsNumber = false;

                if (col[i] == "Currency") {
                    if (colData == -1 && !Reporter.Role)
                        createTd(Language.Cell_Currency[RightView.Data.CompanyList.Currency - 1], row, true);
                    else {
                        if (noRecord && _record.length == 1)
                            createTd(Language.Cell_Currency[self.Data.CompanyList.Record[0][self.Data.CompanyList.Column[0].indexOf("Currency")] - 1], row, true);
                        else
                            createTd(self.Data.DataTop.Record[i] == -1 ? "" : Language.Cell_Currency[self.Data.DataTop.Record[i] - 1], row, true);
                    }
                }
                else {
                    var tmp;
                    if (!isNaN(self.Data.DataTop.Record[i])) {
                        if (Number(self.Data.DataTop.Record[i]) < 0)
                            IsRed = true;
                        IsNumber = true;
                        tmp = numberWithCommas(self.Data.DataTop.Record[i]);
                    }
                    else
                        tmp = self.Data.DataTop.Record[i];
                    createTd(tmp + Data_Addtext[i], row, true, IsNumber, null, null, { "color": IsRed ? "red" : "" });
                }
            }

            ele.Table_top.appendChild(row);

            return tbl;
        },
        GameDetail: function (tbl) {
            var row, colData, col, IsRed = false, IsNumber = false;

            col = RightView_DataStruct.GetColumn();
            
            var _record = self.Data.GameDetail.Record, _column = self.Data.GameDetail.Column;

            row = tbl.insertRow(1);

            /*Status*/
            var _i_status = self.Data.GameDetail.Column.indexOf("Status");
            if (_i_status != -1) {
                if (self.Data.GameDetail.Record[_i_status] != 0)
                    createTd(Language.Column_Status[self.Data.GameDetail.Record[_i_status]], row, true, false, null, null, { color: ColorSetting.Column.Status });
                else
                    createTd(Language.Column_Status[self.Data.GameDetail.Record[_i_status]], row, true);
            } else
                createTd("", row, false);

            /* column Td ------------------- */
            for (var i = 0; i < col.length; i++) {
                IsRed = false;
                IsNumber = false;
                colIndex = _column.indexOf(col[i]);

                colData = colIndex == -1 ? "" : _record[colIndex];
                if (col[i] == "Currency") {
                    createTd(colData == -1 ? "" : Language.Cell_Currency[colData - 1], row, true);
                } else {
                    var tmp;
                    if (!isNaN(colData)) {
                        if (Number(colData) < 0)
                            IsRed = true;
                        IsNumber = true;
                        tmp = numberWithCommas(colData);
                    } else
                        tmp = colData;
                    createTd(tmp, row, true, IsNumber, null, null, { "color": IsRed ? "red" : ColorSetting.TableTitle.Content });
                }
            }

            return tbl;
        },
        PageIndex: function () {
            $(self.PageDiv).empty();
            $(self.pageIndexSelect).empty();
            $(self.pageIndexSelect).show();

            var option, span, pageSpanCount = 10;

            var Pages = self.Search.PageCount[Layout - Layouts.LAYER1];
            var Page = self.Search.PageIndex[Layout - Layouts.LAYER1];


            if (Page / pageSpanCount > 1) {
                span = buildEle("span", {
                    innerText: "前" + pageSpanCount + "頁"
                });

                span.record = self.Data.GameDetail.Record;
                span.column = self.Data.GameDetail.Column;
                span.gid = self.Data.GameDetail.ReceiveGID;

                span.clickabled = true;
                span.className = span.clickabled ? "pointer" : "";
                span.style.textDecoration = span.clickabled ? "" : "underline";
                span.style.marginRight = "5px";

                span.onclick = function (evt) {
                    if (!getAttr(evt, "clickabled")) {
                        return;
                    }

                    pageClick = true;

                    var pIndex = Math.floor(self.Search.PageIndex[Layout - Layouts.LAYER1] / pageSpanCount) * pageSpanCount + 1 - pageSpanCount;

                    //pIndex = pIndex > Page ? Page : pIndex;

                    self.Search.PageIndex[Layout - Layouts.LAYER1] = pIndex;
                    evt.currentTarget.pageindex = pIndex;
                    Control.RightView.GameDetail(evt);
                }

                self.PageDiv.appendChild(span);
                //Temp += '<a href="javascript:void(0)" onclick="GotoPage(' + (Page - 10) + ')" title="前10页">..</a>';
            }

            var startIndex;

            if (self.Search.PageIndex[Layout - Layouts.LAYER1] % pageSpanCount == 0) {
                startIndex = Math.floor(self.Search.PageIndex[Layout - Layouts.LAYER1] / pageSpanCount) * pageSpanCount - pageSpanCount + 1;
            } else
                startIndex = Math.floor(self.Search.PageIndex[Layout - Layouts.LAYER1] / pageSpanCount) * pageSpanCount + 1;

            for (var i = startIndex; i < (startIndex + pageSpanCount) && i <= Pages; i++) {
                option = document.createElement("option");
                option.text = i;
                option.value = i;
                self.pageIndexSelect.appendChild(option);

                span = buildEle("span", {
                    innerText: i
                });

                span.pageindex = i;
                span.record = self.Data.GameDetail.Record;
                span.column = self.Data.GameDetail.Column;
                span.gid = self.Data.GameDetail.ReceiveGID;

                span.clickabled = true;
                span.className = span.clickabled ? "pointer" : "";
                span.style.textDecoration = self.Search.PageIndex[Layout - Layouts.LAYER1] != i ? "" : "underline";
                span.style.marginRight = "5px";

                span.onclick = function (evt) {
                    if (!getAttr(evt, "clickabled")) {
                        return;
                    }

                    pageClick = true;

                    var index = getAttr(evt, "pageindex");
                    self.Search.PageIndex[Layout - Layouts.LAYER1] = Number(index);
                    Control.RightView.GameDetail(evt);
                }

                self.PageDiv.appendChild(span);
            }

            self.pageIndexSelect.value = self.Search.PageIndex[Layout - Layouts.LAYER1];

            if (startIndex + pageSpanCount <= Pages) {
                span = buildEle("span", {
                    innerText: "後" + pageSpanCount + "頁"
                });

                span.record = self.Data.GameDetail.Record;
                span.column = self.Data.GameDetail.Column;
                span.gid = self.Data.GameDetail.ReceiveGID;

                span.clickabled = true;
                span.className = span.clickabled ? "pointer" : "";
                span.style.textDecoration = span.clickabled ? "" : "underline";
                span.style.marginRight = "5px";

                span.onclick = function (evt) {
                    if (!getAttr(evt, "clickabled")) {
                        return;
                    }

                    pageClick = true;

                    var pIndex = Math.floor(self.Search.PageIndex[Layout - Layouts.LAYER1] / pageSpanCount) * pageSpanCount + 1 + pageSpanCount;

                    if (self.Search.PageIndex[Layout - Layouts.LAYER1] % pageSpanCount == 0) {
                        pIndex = Math.floor(self.Search.PageIndex[Layout - Layouts.LAYER1] / pageSpanCount) * pageSpanCount + 1;
                    } else
                        pIndex = Math.floor(self.Search.PageIndex[Layout - Layouts.LAYER1] / pageSpanCount) * pageSpanCount + 1 + pageSpanCount;


                    self.Search.PageIndex[Layout - Layouts.LAYER1] = pIndex;
                    evt.currentTarget.pageindex = pIndex;
                    Control.RightView.GameDetail(evt);
                }

                self.PageDiv.appendChild(span);
                //Temp += " <a href='javascript:void(0)' onclick='GotoPage(" + (Page + 10) + ")' title='后10页'>..</a>";
            }


            switch (Layout) {
                default:
                    self.Search.PageIndex[3] = 1;
                    $(self.pageIndexSelect).hide();
                    break;
                case Layouts.LAYER4:
                    self.pageIndexSelect.record = self.Data.GameDetail.Record;
                    self.pageIndexSelect.column = self.Data.GameDetail.Column;
                    self.pageIndexSelect.gid = self.Data.GameDetail.ReceiveGID;

                    self.pageIndexSelect.onchange = function (evt) {
                        self.Search.PageIndex[Layout - Layouts.LAYER1] = Number(self.pageIndexSelect.value);
                        evt.currentTarget.pageindex = Number(self.pageIndexSelect.value);
                        Control.RightView.GameDetail(evt);
                    }
                    break;

            }

        },
        CheckShowBanBtn: function (status) {
            return status == 0;
        },
        UpdateTransTopData: function (col) {
            var result = {
                ColIndex: -1,
                IsValid: false
            };

            var updateCol = ["AgentBonus", "CompanyBonus", "Cash", "Credit"];
            if (Reporter.DataType == DataType.TRANSACTION)
                updateCol = ["AgentBonus", "CompanyBonus", "Cash", "Credit"];
            else if (Reporter.DataType == DataType.GAMELIST)
                updateCol = ["AgentBonus", "CompanyBonus"];
            var col_index, record_index;

            for (var i = 0; i < updateCol.length; i++) {
                record_index = col.indexOf(updateCol[i]);
                col_index = self.Data.CompanyList.Column.indexOf(updateCol[i]);

                if (col_index != -1 && record_index != -1 &&
                    !isNaN(self.Data.DataTop.Trans[0][record_index])) {
                    self.Data.DataTop.Record[record_index] = Number(self.Data.DataTop.Trans[0][col_index]);
                }
            }

        },
        getRowFirstTitleUndex: function () {
            return Reporter.DataType == DataType.STOCKHOLDERMANAGE ? 1 : 0;
        }
    }

    //opt:pre,last
    function CreateTitle(tbl, col, opt) {
        var row = document.createElement("tr"), text;
        row = tbl.insertRow(0);

        var pre = [], last = [];

        if (opt) {
            if (opt.pre) {
                pre = opt.pre;
            }

            if (opt.last) {
                last = opt.last;
            }
        }


        for (var i = 0; i < pre.length; i++) {
            row = createTd(pre[i], row, true);
        }

        for (var i = 0; i < col.length; i++) {
            text = col[i].split(':');

            text.length == 1 ? Data_Addtext[i] = "" : Data_Addtext[i] = text[1];

            row = createTd(text[0], row, true);
        }

        for (var i = 0; i < last.length; i++) {
            row = createTd(last[i], row, true);
        }

        row.style.backgroundColor = ColorSetting.TableTitle.BG;
        row.style.color = ColorSetting.TableTitle.Font;
        row.style.height = "22px";
        row.style.textAlign = "center";

        tbl.setAttribute('border', '0');

        return tbl;
    }
    
    self.Refresh = function () {
        ele.btn_return.value = Language.btn_Return;

        AddEditPack.Refresh(self.AddEditPanel);

        ele.printBtn.value = Language.btn_Print;

        //SearchUI.Refresh();
    }

    self.Init = function () {
        RightPage.Append(ele.Div);
        RightPage.Append(self.AddEditPanel);
        //$("body").append(ele.Div);
        //$("body").append(self.AddEditPanel);
    }
    self.Show = {
        EditPanel: function (evt, Mode, isRefresh) {
            AddEditPack.SetMode(Mode);

            if (!isRefresh && Mode == Modes.EDIT) {
                LoadingPanel.Show();
                AddEditPack.EditID = evt.currentTarget.UserID;
                ws.Send("request.userdata", {
                    GID: Reporter.GID,
                    UserID: evt.currentTarget.UserID,
                    ReporterID: Reporter.ID,
                    Mode: Mode
                });
            }

            var input_ele = document.getElementById("txtAccount");

            if (input_ele) {
                /*txtAccount can't edit*/
                if (AddEditPack.Mode == Modes.EDIT)
                    input_ele.disabled = true;
                else
                    input_ele.disabled = false;
            }

            var pwd_id = "row_txtPassword",
                re_pwd = "row_txtRePassword",
                bonus_id = "row_txtBonus",
                refund_id = "row_txtRefund";

            //if (Layout == Layouts.LAYER2 || AddEditPack.Mode == Modes.EDIT) {
            //    document.getElementById(pwd_id).style.display = "none";
            //    document.getElementById(re_pwd).style.display = "none";
            //} else {
            //    document.getElementById(pwd_id).style.display = "";
            //    document.getElementById(re_pwd).style.display = "";
            //}

            if (!Reporter.Permission || AddEditPack.Role == 0 || (Layout == Layouts.LAYER2)) {
                //document.getElementById("row_canSeeNext").style.display = "none";
                document.getElementById(bonus_id).style.display = "none";
                document.getElementById(refund_id).style.display = "none";
            }
            else {
                //document.getElementById("row_canSeeNext").style.display = "none";
                document.getElementById(bonus_id).style.display = "";
                document.getElementById(refund_id).style.display = "";
            }
            if (document.getElementById("row_Currency"))
                document.getElementById("row_Currency").style.display = "none";

            if (document.getElementById("row_canSeeNext"))
                document.getElementById("row_canSeeNext").style.display = "none";

            AddEditPack.SetError("");
            self.AddEditPanel.style.display = "inline-block";
            $(ele.Div).hide();
        },
        Data: function () {
            $(self.AddEditPanel).hide();
            AddEditPack.InitInput();
            ele.Div.style.display = "inline-block";

            self.searchControlDiv_Total.style.display = "none";
            self.searchControlDiv_Trans.style.display = "none";

            if (Reporter.DataType == DataType.TRANSACTION) {
                self.searchControlDiv_Trans.style.display = "block";
                if (Reporter.Role >= 1) {
                    ele.printBtn.style.display = "inline-block";
                } else {
                    ele.printBtn.style.display = "none";
                }
            } else if (Reporter.DataType == DataType.GAMELIST)
                self.searchControlDiv_Total.style.display = "block";
            else if (Reporter.DataType == DataType.MEMBERMANAGE) {
                self.searchControlDiv_Total.style.display = "block";
            } else if (Reporter.DataType == DataType.STOCKHOLDERMANAGE) {
                self.searchControlDiv_Total.style.display = "none";
            }

            AddEditPack.SetMode(Modes.VIEW);
        }
    }
    self.Hide = function () {
        $(ele.Div).hide();

        $(self.AddEditPanel).hide();
        AddEditPack.InitInput();

        AddEditPack.SetMode(Modes.VIEW);
    }

    Object.defineProperties(self, {
        Col_AddText: {
            get: function () { return Data_Addtext; }
        },
        Div: {
            get: function () { return ele.Div; }
        }
    })

    return self;
}();

function zeroArr(len) {
    var target = [];

    for (var i = 0; i < len; i++) {
        target[i] = 0;
    }

    return target;
}

var Control = {
    LeftView: {
        FirstConfirm:function () {
            RightView.Data.TopTree = [];
            Control.gameWebsockect.Initial();

            RightView.Search.PageIndex[Layout - Layouts.LAYER1] = 1;

            RightView.Search.StartTime = Control.RightView.Calendar.getSearch(CreateView.Date.Type.DateTime, RightView.SelectObj, CreateView.Date.Type.Start);
            RightView.Search.EndTime = Control.RightView.Calendar.getSearch(CreateView.Date.Type.DateTime, RightView.SelectObj, CreateView.Date.Type.End);

            Control.CheckOutSelect.GetList(RightView.Search.StartTime, RightView.Search.EndTime, 1);

            Control.CheckOutSelect.CheckOutTimeSearch(Control.CheckOutSelect.ActiveOption[1]);
        },
        Confirm: function (initial, isFirst) {
            if (initial) {
                RightView.Data.TopTree = [];
                Control.gameWebsockect.Initial();

                RightView.Search.PageIndex[Layout - Layouts.LAYER1] = 1;

                RightView.Search.StartTime = Control.RightView.Calendar.getSearch(CreateView.Date.Type.DateTime, RightView.SelectObj, CreateView.Date.Type.Start);
                RightView.Search.EndTime = Control.RightView.Calendar.getSearch(CreateView.Date.Type.DateTime, RightView.SelectObj, CreateView.Date.Type.End);

                Control.CheckOutSelect.GetList(RightView.Search.StartTime, RightView.Search.EndTime, 1);
            }
            
            RightView.Data.CompanyList.Init();
            RightView.Data.MemberList.Record = [];

            Control.gameWebsockect.Receive.DataCount = 0;
            Control.gameWebsockect.CompleteFunc = function () {
                if (Reporter.Role >= 1)
                    setLayout(Layouts.LAYER1);
                else
                    setLayout(Layouts.LAYER2);
            };

            Control.RightView.SendTotal(arguments.length == 2 ? arguments[1] : Reporter.ID, (arguments.length == 2 && Reporter.Role == 2) ? 1 : Reporter.Role);
        }
    },
    RightView: {
        Layer1: {
            TreeClick: function (treePack) {
                Control.gameWebsockect.Receive.DataCount = 0;
                RightView.Data.CompanyList.Init();

                var self = treePack;

                Control.gameWebsockect.CompleteFunc = function () {
                    RightView._create.CompanyList(RightView.Table_Bottom, false, self.Level + 1, "tr_" + Layout + "_" + self.ID);

                    LoadingPanel.Hide();
                };

                Control.RightView.SendTotal(self.ID, 1);

            },
            Row: function (evt) {
                Control.gameWebsockect.Receive.DataCount = 0;
                RightView.Data.CompanyList.Init();

                Control.gameWebsockect.CompleteFunc = function () { setLayout(Layouts.LAYER2) };

                Control.RightView.SendTotal(evt.currentTarget.UserID, 0);
            }
        },
        Layer3: function (evt) {
            Control.gameWebsockect.Receive.DataCount = 0;
            RightView.Data.CompanyList.Init();

            Control.gameWebsockect.CompleteFunc = function () {
                Control.RightView.GameList(evt);
                //setLayout(Layouts.LAYER3)
            };

            Control.RightView.SendTotal(evt.currentTarget.Layer2UserID, 0);
        },
        GameList: function (evt) {
            LoadingPanel.Show();
            RightView.Data.CompanyList.Record = [];

            var _uid = evt.currentTarget.UserID, isFind = false;
            for (var i = 0; i < RightView.Data.CompanyList.List.length; i++) {
                if (Number(RightView.Data.CompanyList.List[i].UserID) == Number(_uid)) {
                    isFind = true;
                    var obj = RightView.Data.CompanyList.List[i].GetGameList(), colIndex;

                    RightView.Data.CompanyList.Record = obj.Record;
                    RightView.Data.CompanyList.Column = obj.Column;

                    RightView.Data.GameList.ReceivePlayerID = RightView.Data.CompanyList.List[i].UserID;
                    RightView.Data.GameList.ReceivePlayerName = RightView.Data.CompanyList.List[i].Name;

                    break;
                }
            }

            //if (isFind) {
            setLayout(Layouts.LAYER3);
            //}

        },
        GameDetail: function (evt) {
            LoadingPanel.Show();
            var _gid = evt.currentTarget.gid;

            Control.gameWebsockect.Receive.DataCount = 0;
            Control.gameWebsockect.Receive.ReceiveCount = 1;

            Control.gameWebsockect.CompleteFunc = function () { setLayout(Layouts.LAYER4) };

            if (Reporter.DataType == DataType.GAMELIST) {
                ws.Send("reporter_GameRecord", {
                    GID: Reporter.GID,
                    UserID: RightView.Data.GameList.ReceivePlayerID,
                    gid: _gid,
                    pageindex: evt.currentTarget.pageindex,
                    start: RightView.Search.StartTime,
                    end: RightView.Search.EndTime,
                    Member: ""
                });
            } else if (Reporter.DataType == DataType.TRANSACTION) {
                //var _mode = evt.currentTarget.mode;
                ws.Send("reporter_TransRecord", {
                    GID: Reporter.GID,
                    UserID: RightView.Data.GameList.ReceivePlayerID,
                    ModeID: "",
                    Title: RightView.Data.GameList.ReceivePlayerName,
                    pageindex: evt.currentTarget.pageindex,
                    start: RightView.Search.StartTime,
                    end: RightView.Search.EndTime,
                    Member: ""
                });
            } else if (Reporter.DataType == DataType.MEMBERMANAGE) {
                var _mode = evt.currentTarget.mode;
                ws.Send("reporter_CashInOutRecord", {
                    GID: Reporter.GID,
                    UserID: RightView.Data.GameList.ReceivePlayerID,
                    ModeID: _gid,
                    Title: _mode,
                    pageindex: evt.currentTarget.pageindex,
                    start: RightView.Search.StartTime,
                    end: RightView.Search.EndTime,
                    Member: ""
                });
            } else if (Reporter.DataType == DataType.GAMEREFUNDLIST) {
                ws.Send("reporter_GameRefundListRecord", {
                    GID: Reporter.GID,
                    UserID: RightView.Data.GameList.ReceivePlayerID,
                    ModeID: "",
                    Title: RightView.Data.GameList.ReceivePlayerName,
                    pageindex: evt.currentTarget.pageindex,
                    start: RightView.Search.StartTime,
                    end: RightView.Search.EndTime,
                    Member: ""
                });
            }
            RightView.Data.GameDetail.Record = evt.currentTarget.record;
            RightView.Data.GameDetail.Column = evt.currentTarget.column;

        },
        SendTotal: function (pID, Type) {
            LoadingPanel.Show();

            if (Reporter.DataType == DataType.GAMELIST) {
                Control.gameWebsockect.Receive.ReceiveCount = LeftView.check_list.length;

                var gidlist = [];
                for (var i = 0; i < LeftView.check_list.length; i++) {
                    gidlist.push(LeftView.check_list[i].gid);
                }

                ws.Send("report_total", {
                    GID: Reporter.GID,
                    Type: Type,
                    pID: pID,
                    gidlist: gidlist,
                    start: RightView.Search.StartTime,
                    end: RightView.Search.EndTime,
                    Member: ""
                });
            } else if (Reporter.DataType == DataType.TRANSACTION) {
                Control.gameWebsockect.Receive.ReceiveCount = 1;
                var tmp = $('input[name="' + RightView.id.Radio.Search + '"]:checked').val();

                ws.Send("transAction_report", {
                    GID: Reporter.GID,
                    Type: Type,
                    pID: pID,
                    start: RightView.Search.StartTime,
                    end: RightView.Search.EndTime,
                    Member: "",
                    transType: tmp
                });
            } else if (Reporter.DataType == DataType.MEMBERMANAGE) {
                Control.gameWebsockect.Receive.ReceiveCount = 1;
                ws.Send("opencash_total", {
                    GID: Reporter.GID,
                    Type: Type,
                    pID: pID,
                    Member: ""
                });
            } else if (Reporter.DataType == DataType.STOCKHOLDERMANAGE) {
                Control.gameWebsockect.Receive.ReceiveCount = 1;
                ws.Send("reporter_Stockholder", {
                    GID: Reporter.GID,
                    Type: Type,
                    pID: pID,
                    Member: ""
                });
            } else if (Reporter.DataType == DataType.GAMEREFUNDLIST) {
                Control.gameWebsockect.Receive.ReceiveCount = 1;
                ws.Send("reporter_GameRefundList", {
                    GID: Reporter.GID,
                    Type: Type,
                    pID: pID,
                    Member: "",
                    start: RightView.Search.StartTime,
                    end: RightView.Search.EndTime
                });
            }
        },
        AddEdit: function () {
            LoadingPanel.Show();

            var input = [], ele_input, pa = "";
            var isOK = true;

            var member_exclude_col = ["txtBonus", "txtRefund"]

            for (var i = 0; i < AddEditPack.ID.length; i++) {
                if (AddEditPack.Type[i] == "radio") {
                    ele_input = document.getElementsByName(AddEditPack.ID[i]);
                } else
                    ele_input = document.getElementById(AddEditPack.ID[i]);

                if (ele_input) {
                    if (ele_input.value == "") {
                        if (AddEditPack.Type[i] == "password" && AddEditPack.Mode == Modes.EDIT) {
                        } else if (member_exclude_col.indexOf(AddEditPack.ID[i]) != -1 && AddEditPack.Role == 0) {
                        } else {
                            AddEditPack.SetError(AddEditPack.ColumnTitle[i] + Language.AlertMsg[0]);
                            isOK = false;
                            break;
                        }
                    }

                    if (AddEditPack.Type[i] == "password") {
                        if (pa == "") {
                            pa = ele_input.value;
                        } else if (pa != ele_input.value) {
                            isOK = false;
                            AddEditPack.SetError(Language.AlertMsg[1]);
                        }
                    }

                    if (AddEditPack.Type[i] == "radio") {
                        for (var j = 0; j < ele_input.length; j++) {
                            if (ele_input[j].checked) {
                                input.push(ele_input[j].value);
                                break;
                            }
                        }
                    } else
                        input.push(ele_input.value);
                }
            }

            if (!isOK) {
                LoadingPanel.Hide();
                return;
            }

            AddEditPack.SetError("");

            ws.Send("editdata", {
                GID: Reporter.GID,
                Input: input, Column: AddEditPack.ColumnTitle,
                Type: AddEditPack.Type,
                Mode: AddEditPack.Mode,
                Role: AddEditPack.Role,
                //Layer: Layout,
                EditShopID: AddEditPack.Mode == Modes.EDIT ? AddEditPack.EditID : RightView.Data.CompanyList.ReceivePlayerID
            });
        },
        Search: function () {
            LoadingPanel.Show();
            RightView.Search.PageIndex[Layout - Layouts.LAYER1] = 1;
            RightView.Search.StartTime = this.Calendar.getSearch(CreateView.Date.Type.DateTime, RightView.SelectObj, CreateView.Date.Type.Start);
            RightView.Search.EndTime = this.Calendar.getSearch(CreateView.Date.Type.DateTime, RightView.SelectObj, CreateView.Date.Type.End);

            RightView.Data.MemberList.InsertFirst = true;

            Control.CheckOutSelect.GetList(RightView.Search.StartTime, RightView.Search.EndTime, 1);

            Control.LeftView.Confirm();
        },
        getSearch: function (DateType, val) {
            var result = "";
            var select = val.Year.value;
            result += select + "-";
            select = val.Month.value;
            result += select + "-";
            select = padLeft(2, val.Day.value, "0");
            result += select + " ";

            if (DateType == CreateView.Date.Type.Date) {
                result += "00:00";
                return result;
            }
            select = padLeft(2, val.Hour.value, "0");
            result += select + ":";
            select = padLeft(2, val.Minute.value, "0");
            result += select;

            return result;
        },
        Calendar: {
            getSearch: function (DateType, val, StartOrEndTime) {
                var result = "";

                if (val.Input[StartOrEndTime].YearMonthDay.value == "")
                    val.Input[StartOrEndTime].YearMonthDay.value = new Date().format(Report_Config.DateFormat.Normal);
                result += val.Input[StartOrEndTime].YearMonthDay.value + " ";

                if (DateType == CreateView.Date.Type.Date) {
                    if (StartOrEndTime == CreateView.Date.Type.Start)
                        result += "00:00:00";
                    else
                        result += "23:59:59";

                    return result;
                }

                result += (StartOrEndTime == CreateView.Date.Type.Start ? val.Select[StartOrEndTime].Hour.value : Number(val.Select[StartOrEndTime].Hour.value) - 1) + ":";
                result += StartOrEndTime == CreateView.Date.Type.Start ? "00:00" : "59:59";

                return result;
            }
        },        
        Print: function () {
            LoadingPanel.Show();
            var tmp = $('input[name="' + RightView.id.Radio.Search + '"]:checked').val();

            ws.Send("print_report", {
                GID: Reporter.GID,
                Type: Reporter.Role,
                pID: Reporter.ID,
                start: RightView.Search.StartTime,
                end: RightView.Search.EndTime,
                Member: "",
                transType: tmp
            });
        },
        Ban: function (evt) {
            LoadingPanel.Show();
            var status = evt.currentTarget.User_Status;
            var UserID = evt.currentTarget.UserID;
            ws.Send("user.ban", {
                GID: Reporter.GID,
                UserID: UserID,
                status: status
            });
        },
        ChangeMemberOrStockHolder: function (evt) {
            LoadingPanel.Show();
            var role = evt.currentTarget.User_Role;
            var UserID = evt.currentTarget.UserID;
            ws.Send("user.changeRole", {
                GID: Reporter.GID,
                UserID: UserID,
                role: role
            });
        }
    },
    //WebSockect 傳遞+接收
    gameWebsockect: {
        CompleteFunc: null,
        Initial: function () {
            LeftView.check_list = [];

            $("input[type=checkbox][name=" + LeftView.check_name + "]").each(function () {
                if (this.checked) {
                    LeftView.check_list.push({
                        val: $(this).val(),
                        gid: this.gid,
                        gtitle: this.gtitle,
                        gversion: this.gversion
                    });
                }
            });

        },
        Receive: {
            DataCount: 0,
            ReceiveCount: 0,
            CheckComplete: function () {
                Control.gameWebsockect.Receive.DataCount += 1;

                if (Control.gameWebsockect.Receive.ReceiveCount == 0 || Control.gameWebsockect.Receive.DataCount == Control.gameWebsockect.Receive.ReceiveCount) {
                    //接收完成
                    if (Control.gameWebsockect.CompleteFunc)
                        Control.gameWebsockect.CompleteFunc();
                    LoadingPanel.Hide();
                }
            },
            Total: function (msg) {
                RightView.Data.CompanyList.ReceivePlayerID = msg.pID;
                RightView.Data.CompanyList.ReceivePlayerName = msg.pName;
                RightView.Data.CompanyList.Currency = msg.Currency;
                RightView.Data.CompanyList.Column = msg.Column;
                RightView.Data.MemberList.Column = msg.Column_User;

                if (msg.dt_top)
                    RightView.Data.DataTop.Trans = msg.dt_top;
                else
                    RightView.Data.DataTop.Trans = [];

                if (RightView.Data.MemberList.Record.length == 0) {
                    RightView.Data.MemberList.Record = zeroArr(RightView.Data.MemberList.Column.length);
                }

                RightView.Data.MemberList.Add(msg.Record_User);

                for (var i = 0; i < msg.Record.length; i++) {
                    if (Reporter.DataType == DataType.GAMELIST)
                        RightView.Data.CompanyList.Add(msg.Record[i], msg.gameID, msg.gameTitle);
                    else
                        RightView.Data.CompanyList.Add(msg.Record[i], msg.Record[i][msg.Column.indexOf("GID")], msg.Record[i][msg.Column.indexOf("Mode")]);
                }

                Control.gameWebsockect.Receive.CheckComplete();
            },
            GameRecord: function (msg) {
                //if (msg.Record.length) {
                RightView.Data.GameDetail.ReceiveGID = msg.gameID;
                RightView.Data.GameDetail.ReceiveGtitle = msg.gameTitle;

                if (Reporter.DataType == DataType.TRANSACTION || Reporter.DataType == DataType.GAMEREFUNDLIST) {
                    RightView.Data.GameDetail.Record = RightView.Data.CompanyList.Record[0];
                    RightView.Data.GameDetail.Column = RightView.Data.CompanyList.Column[0];
                }

                DataSheet._obj = msg;
                RightView.Search.PageCount[3] = msg.pageCount;
                //}

                Control.gameWebsockect.Receive.CheckComplete();
            },
            Error: function (msg) {
                Control.gameWebsockect.Receive.CheckComplete();
            }
        }
    },
    DayReport: {
        getSearch: function (pageIsInitial) {
            Day_Trans_Report_View.Search.StartTime = Control.RightView.Calendar.getSearch(CreateView.Date.Type.Date, Day_Trans_Report_View.SelectObj, CreateView.Date.Type.Start);
            Day_Trans_Report_View.Search.EndTime = Control.RightView.Calendar.getSearch(CreateView.Date.Type.Date, Day_Trans_Report_View.SelectObj, CreateView.Date.Type.End);

            if (CreateView.Date.Calendar.isValidDate(Day_Trans_Report_View.Search.StartTime))

                this.Send(pageIsInitial);
        },
        Send: function (pageIsInitial) {
            LoadingPanel.Show();

            ws.Send("request.DayReport", {
                GID: Reporter.GID,
                start: Day_Trans_Report_View.Search.StartTime,
                end: Day_Trans_Report_View.Search.EndTime,
                isInit: pageIsInitial,
                pageindex: Day_Trans_Report_View.Page.Index(),
                perpagecount: Day_Trans_Report_View.PerPageCount
            });
        }
    },
    OpenPDF: function (filName, type) {
        window.open("pdfPage.aspx?type=" + type + "&filename=" + filName, "_blank");
    },
    Cash: {
        InOut: function (userID, type) {
            LoadingPanel.Show();
            var price, cashintype = "";
            switch (type) {
                case PopType.CASHOUT:
                    price = CashOutView.Price;
                    break;
                case PopType.CASHIN:
                    price = CashInView.Price;
                    cashintype = CashInView.Type;
                    break;
                default:
                    price = 0;
                    break;
            }

            ws.Send("600.0", {
                GID: Reporter.GID,
                type: type,
                uid: userID,
                price: price,
                cashintype: cashintype
            });
        },
        SetCredit: function (userID) {
            LoadingPanel.Show();

            ws.Send("601.0", {
                GID: Reporter.GID,
                uid: userID,
                price: SetCreditView.Price,
                type: PopType.SETCREDIT
            });
        },
        GetCreditInfo: function (userID, type) {
            LoadingPanel.Show();

            ws.Send("request.usercredit", {
                GID: Reporter.GID,
                uid: userID,
                type: type
            });
        }
    },
    HomePage: {
        Send: function () {
            if (!Reporter.Role)
                return;
            LoadingPanel.Show();
            ws.Send("get.homepage", {
                GID: Reporter.GID
            });
        }
    },
    Logout: function () {
        LoadingPanel.Show();
        ws.close();
    },
    Online: {
        Send: function () {
            if (!Reporter.isAdmin && Reporter.Role != 2)
                return;
            LoadingPanel.Show();
            ws.Send("request.online", {
                GID: Reporter.GID
            });
        }
    },
    Setting: {
        RequestData: function (uid) {
            if (!Reporter.isAdmin)
                return;
            LoadingPanel.Show();
            ws.Send("request.setting_week", {
                GID: Reporter.GID
            });
        },
        Confirm: function () {
            if (!Reporter.isAdmin)
                return;
            LoadingPanel.Show();
            ws.Send("request.setting_confirm", {
                GID: Reporter.GID,
                Data: CheckOutSettingView.GetSelectVal()
            });
        }
    },
    Cashout: {
        GetList: function () {
            if (!Reporter.isAdmin)
                return;
            LoadingPanel.Show();
            ws.Send("request.cashout_list", {
                GID: Reporter.GID
            });
        },
        Excute: function () {
            CheckOutView.Error("");
            if (!Reporter.isAdmin)
                return;
            LoadingPanel.Show();
            ws.Send("request.checkout", {
                GID: Reporter.GID
            });
        },
        Finish: function () {
            CheckOutView.Error("");
            if (!Reporter.isAdmin)
                return;
            LoadingPanel.Show();
            ws.Send("request.checkout_finish", {
                GID: Reporter.GID
            });
        },
        GetHistory: function () {            
            LoadingPanel.Show();
            
            ws.Send("request.cashout_list_history", {
                GID: Reporter.GID,
                start: CheckOutHistoryView.Search.StartTime,
                end: CheckOutHistoryView.Search.EndTime,
                name: CheckOutHistoryView.TextName.value
            });
        },
        Release: function () {
            if (!Reporter.isAdmin)
                return;
            ws.Send("request.checkoutrelease", {
                GID: Reporter.GID
            });
        },
        GetTime: function () {
            ws.Send("request.updatecheckout", {
                GID: Reporter.GID
            });
        },
        GetStatus: function () {
            ws.Send("request.checkout_status", {
                GID: Reporter.GID
            });
        }
    },
    AddShareHolder: function (name, acc, pa) {
        LoadingPanel.Show();

        ws.Send("add.shareholder", {
            GID: Reporter.GID,
            name: name,
            acc: acc,
            pa: pa,
            uid: AddShareHolderView.UID,
            Mode: AddShareHolderView.Mode
        });
    },
    GetShareHolder: function () {
        LoadingPanel.Show();

        ws.Send("request.userdata", {
            GID: Reporter.GID,
            UserID: AddShareHolderView.UID,
            Mode: AddShareHolderView.Mode
        });
    },
    CheckOutSelect: {
        CheckOutOption: [],
        ActiveOption: [],
        nowType:-1,
        GetList: function (start, end, type) {
            this.nowType = type;
            ws.Send("request.checkouttimelist", {
                start: start,
                end:end,
                GID: Reporter.GID,
                Type:type
            });
        },
        Update: function (msg) {
            this.ClearActive();

            var bindtarget, opt, start, end;
            switch (this.nowType.toString()) {
                case "1":
                    bindtarget = RightView.SelectObj.Select_checkoutduration;
                    break;
                case "2":
                    bindtarget = CheckOutHistoryView.SelectObj.Select_checkoutduration;
                    break;
                case "3":
                    bindtarget = AutoCheckView.SelectObj.Select_checkoutduration;
                    break;
            }

            if (!bindtarget)
                return;

            $(bindtarget).off('change');
            bindtarget.options.length = 0;

            opt = this.GetOpt();
            opt.Start = msg.start;
            opt.End = msg.end;
            opt.Control.text = Language.SearchDefaultText;
            opt.Control.value = 0;
            bindtarget.appendChild(opt.Control);
                  
            if (msg.Record.length > 0) {
                start = ParseDate(msg.Record[msg.Record.length - 1][1]).format(Report_Config.DateFormat.DateTime);
                end = new Date().format(Report_Config.DateFormat.DateTime);

                opt = this.GetOpt();
                opt.Start = start;
                opt.End = end;
                opt.Control.text = "未結帳時段";
                opt.Control.value = 1;

                bindtarget.appendChild(opt.Control);
            }

            for (var i = 0; i < msg.Record.length; i++) {
                start = ParseDate(msg.Record[i][0]).format(Report_Config.DateFormat.DateTime);
                end = ParseDate(msg.Record[i][1]).format(Report_Config.DateFormat.DateTime);

                opt = this.GetOpt();
                opt.Start = start;
                opt.End = end;
                opt.Control.text = start + " ~ " + end;
                opt.Control.value = i + 2;

                bindtarget.appendChild(opt.Control);
            }
            bindtarget.value = "1";
            $(bindtarget).change(function (evt) {
                Control.CheckOutSelect.CheckOutTimeSearch(Control.CheckOutSelect.ActiveOption[Number(this.value)]);
            });            
        },
        GetOpt: function () {
            var obj;
            if (this.CheckOutOption.length > 0) {
                obj = this.CheckOutOption.pop();
                                
                this.ActiveOption.push(obj);
                return obj;
            }
            obj = {};
            obj.Control = buildEle("option");
            obj.Start = "";
            obj.End = "";

            this.ActiveOption.push(obj);

            return obj;
        },
        ClearActive: function () {
            while (this.ActiveOption.length > 0) {
                this.CheckOutOption.push(this.ActiveOption.pop());
            }
        },
        CheckOutTimeSearch: function (opt) {
            switch (Control.CheckOutSelect.nowType.toString()) {
                case "1":
                    RightView.Search.PageIndex[Layout - Layouts.LAYER1] = 1;
                    RightView.Search.StartTime = opt.Start;
                    RightView.Search.EndTime = opt.End;

                    RightView.Data.MemberList.InsertFirst = true;

                    Control.LeftView.Confirm();

                    break;
                case "2":
                    CheckOutHistoryView.Search.StartTime = opt.Start;
                    CheckOutHistoryView.Search.EndTime = opt.End;
                    Control.Cashout.GetHistory();
                    break;
                case "3":
                    AutoCheckView.Search.StartTime = opt.Start;
                    AutoCheckView.Search.EndTime = opt.End;

                    break;
            }            
        }
    },
    GetAccoutSum: function () {
        ws.Send("request.accountsum", {
            GID: Reporter.GID
        });
    }
}

/*tool ws訊息生成(maintype,subtype,參數[])*/
var Action = function () {
    var actioncontent = arguments[2];
    for (var i = 3; i < arguments.length; i++) { actioncontent += ("," + arguments[i]); }
    return {
        "actionmaintype": arguments[0],
        "actionsubtype": arguments[1],
        "actiontime": "",
        "actioncontent": actioncontent
    }
}

var QRView = function () {
    var qr = {};
    qr.QRSize = 100;
    qr.errSpan = null;
    var pageColor = ColorSetting.Page;
    qr.print_DivId = "printZone";

    var id_qrCountInput = "qrcount";

    var ele = {};

    qr.Div = WebTool.CreateElement("div", {
        style: {
            "display": "none",
            "width": "84%"
        }
    });

    qr.Search = {
        Data: {},
        allDataCount: 0,
        Page: {
            isInit: false,
            isPaging: true,
            index: 0,
            perPage: 5,
            Change: function (index) {
                if (typeof (index) == 'string') {
                    if (isNaN(index)) {
                        return;
                    } else {
                        index = Number(index);
                    }
                }

                LoadingPanel.Show();

                qr.Search.Page.index = index;

                qr.GetList();
            },
            Control: function () {
                if (!qr.Search.Page.isPaging) {
                    return;
                }
                $(qr.DivPage).empty();

                var index = qr.Search.Page.index;
                var cou = 10;

                var startIndex = Math.floor(index / cou) * cou + 1;
                var endIndex = Math.ceil(qr.Search.allDataCount / qr.Search.Page.perPage);
                var span;

                /*前10頁*/
                if (startIndex > cou) {
                    this.ele.PrePage = WebTool.CreateElement("span", {
                        style: {
                            "margin": "2px"
                        },
                        innerText: Language.Page[0],
                        className: "pointer"
                    });

                    span.onclick = function (evt) {
                        qr.Search.Page.Change(startIndex - cou - 1);
                    };

                    qr.DivPage.appendChild(span);
                }

                for (var i = startIndex; i <= startIndex + cou && i <= endIndex; i++) {
                    span = WebTool.CreateElement("span", {
                        style: {
                            "margin": "2px",
                            "color": (i - 1) == index ? pageColor : ""
                        },
                        innerText: i,
                        className: (i - 1) == index ? "" : "pointer"
                    });
                    span.setAttribute("pindex", i - 1);
                    if ((i - 1) != index)
                        span.onclick = function (evt) {
                            qr.Search.Page.Change(WebTool.GetAttr(evt, "pindex", this));
                        };

                    qr.DivPage.appendChild(span);
                }

                /*後10頁*/
                if (endIndex > startIndex + cou) {
                    this.ele.NextPage = WebTool.CreateElement("span", {
                        style: {
                            "margin": "2px"
                        },
                        innerText: Language.Page[1],
                        className: "pointer"
                    });

                    span.onclick = function (evt) {
                        qr.Search.Page.Change(startIndex + cou - 1);
                    };

                    qr.DivPage.appendChild(span);
                }
            },
            ele: {
                PrePage: null,
                NextPage: null
            }
        }
    };
    
    /*Search*/
    var searchUIObj = SearchUI.QRCode();

    qr.Search.Div = searchUIObj.Div;

    qr.Search.select_status = searchUIObj.select_status;
    qr.Search.input_user = searchUIObj.input_user;

    qr.GetList = function () {
        var qrsearch = QRView.Search.Page;

        ws.Send("get.qrcodelist", {
            GID: Reporter.GID,
            ispaging: qrsearch.isPaging,
            pageindex: qrsearch.index,
            perpagecount: qrsearch.perPage,
            status: qr.Search.select_status.value,
            isinit: qr.Search.Page.isInit,
            searchuser: qr.Search.input_user.value
        });
    }

    qr.DivPage = WebTool.CreateElement("div", {
        style: {
            "text-align": "center",
            "margin": "5px"
        }
    });

    qr.DivList = WebTool.CreateElement("div", {
        id: "QRCodeView_DIVList",
        style: {
            "width": "100%",
            "padding-top": "10px",
            "padding-left": "1%"
        }
    });


    qr.DivAdd = WebTool.CreateElement("div", {
        style: {
            "display": "none"
        }
    });

    qr.DivAdd.appendChild(
        WebTool.CreateElement("div", {
            className: "pop_window_bg"
        }));

    ele.addPanel = WebTool.CreateElement("div", {
        className: "pop_window",
        style: {
            "width": "400px",
            "height": "90px",
            "margin": "0 auto",
            "margin-top": "5%",
            "background-color": "white",
            "text-align": "left",
            "overflow-y": "auto"
        }
    });

    ele._addPanelDiv = WebTool.CreateElement("div", {
        style: {
            "background-color": "orange",
            "color": "white",
            "text-align": "right",
            "margin-bottom": "0"
        }
    });

    ele.addPanelCloseSpan = WebTool.CreateElement("span", {
        className: "pointer",
        style: {
            "font-size": "large",
            "margin": "1px",
            "margin-right": "10px"
        },
        innerText: "X"
    });

    ele.addPanelCloseSpan.onclick = function () {
        qr.DivAdd.style.display = "none";
    }

    ele._addPanelDiv.appendChild(ele.addPanelCloseSpan);
    ele.addPanel.appendChild(ele._addPanelDiv);
    qr.DivAdd.appendChild(ele.addPanel);

    ele.AddContent = WebTool.CreateElement("div", {
        style: {
            "padding": "5px",
            "text-align": "center"
        }
    });

    /*Error Msg*/
    ele.ErrorMsg = WebTool.CreateElement("span", {
        style: {
            "color": ColorSetting.ErrorMsg
        }
    });
    /*QRCode Count Input*/
    ele.Input_QRCount = WebTool.CreateElement("input", {
        type: "number"
    });
    /* Button Add*/
    qr.Btn = WebTool.CreateElement("input", {
        type: "button",
        value: Language.btn_Add
    });

    ele.addPanel.appendChild(ele.AddContent);

    ele.txtqrcount = WebTool.CreateElement("span", {
        innerText: Language.txt_qrcount
    })

    ele.AddContent.appendChild(ele.txtqrcount);

    /*QRCode Count Input*/
    ele.AddContent.appendChild(ele.Input_QRCount);

    /*Error Msg*/
    ele.AddContent.appendChild(ele.ErrorMsg);

    /* Button Add*/
    ele.AddContent.appendChild(qr.Btn);

    qr.Geneate = function () {
        qr.Error("");

        var count = ele.Input_QRCount.value;
        if (count == "" || Test.Number(count)) {
            qr.Error("請輸入有效數字!");
            return;
        }

        LoadingPanel.Show();
        if (!Test.Undefined(qr.Btn))
            qr.Btn.disabled = true;

        qr.Search.Page.isInit = false;
        qr.Search.Page.index = 0;

        ws.Send("generate.qrcode", {
            GID: Reporter.GID,
            count: count,
            ispaging: qr.Search.Page.isPaging,
            pageindex: qr.Search.Page.index,
            perpagecount: qr.Search.Page.perPage,
            status: qr.Search.select_status.value,
            isinit: qr.Search.Page.isInit,
            searchuser: qr.Search.input_user.value
        })

    }

    qr.ListView = function (list) {
        if (Test.Undefined(qr.DivList)) {
            return;
        }

        if (!qr.Search.Page.isInit) {
            qr.Search.Page.isInit = true;
            qr.Search.allDataCount = list.DataCount;
        }

        qr.Search.Data = list;
        qr.appendList(qr.Search.Data, qr.DivList);

        qr.Search.Page.Control();

        if (!Test.Undefined(qr.Btn))
            qr.Btn.disabled = false;
    }

    qr.appendList = function (list, div, noBtn, perPageCount) {
        div.innerHTML = "";

        var tr = WebTool.CreateElement("tr"),
            date, img, btn, td;

        var table = WebTool.CreateElement("table", {
            "border": "1",
            "style": {
                "width": "100%"
            }

        });
        table.style.pageBreakAfter = "always";

        var Table_Column = Language.Column_QRList.slice();


        if (!Test.Undefined(noBtn) && noBtn) {
            Table_Column.pop();
        }

        table.appendChild(DataSheet.insert_content(tr, Table_Column));


        if (Test.Undefined(list.Record)) {
            return;
        }

        var record = list.Record;
        var col = list.Column, qrcode;

        for (var i = 0; i < record.length; i++) {
            tr = WebTool.CreateElement("tr");

            if (!Test.Undefined(perPageCount) && (i % perPageCount == 0) && i != 0) {
                div.appendChild(table);

                table = WebTool.CreateElement("table", {
                    "border": "1",
                    "style": {
                        "width": "100%"
                    }

                });
                table.style.pageBreakAfter = "always";
                table.appendChild(DataSheet.insert_content(tr, Table_Column));
                tr = WebTool.CreateElement("tr");
            }

            DataSheet.insert_content(tr, (i + 1));

            qrcode = col.indexOf("inviteCode");

            DataSheet.insert_content(tr, record[i][qrcode]);

            img = WebTool.CreateElement("img", {
                "style": {
                    "width": qr.QRSize + "px",
                    "height": qr.QRSize + "px"
                },
                "src": record[i][getIndex(col, "path")]
            });

            DataSheet.insert_content(tr, img.outerHTML);

            date = new Date(ParseDate(record[i][col.indexOf("CreateDate")])).format("yyyy/MM/dd HH:mm:ss");
            DataSheet.insert_content(tr, date);


            var name_index = col.indexOf("NickName");
            if (name_index == -1)
                DataSheet.insert_content(tr, "");
            else
                DataSheet.insert_content(tr, record[i][name_index]);

            if (Test.Undefined(noBtn) || !noBtn) {
                btn = WebTool.CreateElement("input", {
                    "type": "button",
                    "value": Language.btn_Edit
                });
                btn.setAttribute("qrcode", record[i][qrcode])
                btn.onclick = function (evt) {
                    LoadingPanel.Show();
                    textList.GetUserList();
                    textList.Select.QRCode = WebTool.GetAttr(evt, "qrcode", this);
                    $("#pop_win").show();
                }

                td = WebTool.CreateElement("td", {
                    style: {
                        "text-align": "center"
                    }
                });
                td.appendChild(btn);

                btn = WebTool.CreateElement("input", {
                    "type": "button",
                    "value": Language.btn_Delete
                });
                btn.setAttribute("qrcode", record[i][qrcode])
                btn.onclick = function (evt) {
                    LoadingPanel.Show();
                    var qrsearchpage = qr.Search.Page;

                    qrsearchpage.index = 0;
                    qrsearchpage.isInit = false;

                    ws.Send("delete.qrcode", {
                        GID: Reporter.GID,
                        qrcode: WebTool.GetAttr(evt, "qrcode", this),
                        ispaging: qrsearchpage.isPaging,
                        pageindex: qrsearchpage.index,
                        perpagecount: qrsearchpage.perPage,
                        status: qr.Search.select_status.value,
                        isinit: qrsearchpage.isInit,
                        searchuser: qr.Search.input_user.value
                    });
                }
                td.appendChild(btn);

                tr.appendChild(td);
            }

            table.appendChild(tr);
        }


        Table_Column = null;

        div.appendChild(table);

    }

    qr.printReceive = function (obj) {
        var _div = document.getElementById(QRView.print_DivId);

        QRView.appendList(obj, _div, true, 8);

        _div.style.display = "block";

        $("#" + QRView.print_DivId).jqprint();
        _div.style.display = "none";

        LoadingPanel.Hide();
    };

    qr.Init = function () {
        qr.Init = function () {

        }

        qr.Btn.onclick = qr.Geneate;


        qr.Div.appendChild(qr.Search.Div);
        qr.Div.appendChild(qr.DivList);
        qr.Div.appendChild(qr.DivPage);

        RightPage.Append(qr.Div);
        //$("body").append(qr.Div);
        $("body").append(qr.DivAdd);
    }

    qr.GetUser = function (upleveltree) {
        LoadingPanel.Show();
        ws.Send("get.qruserlist", {
            GID: Reporter.GID,
            upleveltree: upleveltree
        });
    }

    qr.Error = function (msg) {
        ele.ErrorMsg.innerText = msg;
    }

    qr.Show = function () {
        //qr.Div.style.display = "none";
        qr.Div.style.display = "inline-block";

    }
    qr.Hide = function () {
        qr.Div.style.display = "none";
    }

    qr.Refresh = function () {
        if (!Test.Undefined(qr.Search.Page.ele.PrePage)) {
            qr.Search.Page.ele.PrePage.innerText = Language.Page[0];
        }

        if (!Test.Undefined(qr.Search.Page.ele.NextPage)) {
            qr.Search.Page.ele.NextPage.innerText = Language.Page[1];
        }

        qr.Btn.value = Language.btn_Add;
        
        ele.txtqrcount.innerText = Language.txt_qrcount;

        qr.appendList(qr.Search.Data, qr.DivList);
    }

    return qr;
}();

var CashInOutView = function () {
    var self = {};
    var acc = "", uid = 0;
    var ele = {};
    var win_width = 200;

    var Id = {
        Radio: "CashInOutRadio"
    }

    ele.div_pop = buildEle("div", {
        className: "pop_window",
        style: { "display": "none" }
    });

    ele.div_pop.appendChild(
        ele.bg = buildEle("div", {
            className: "pop_window_bg"
        }));

    ele.div_pop.appendChild(
        ele.div_call = buildEle("div", {
            className: "confirm_call",
            style: {
                "width": win_width + "px",
                "margin-left": (-win_width / 2) + "px"
            }
        }));

    ele.div_call.appendChild(
        ele.div_title = buildEle("div", {
            className: "confirm_title"
        }));
    ele.div_title.appendChild(
        ele.title = DeepCopyProps.call(document.createElement("span"), {
            innerText: Language.CashInOutTitle
        }));

    ele.div_title.appendChild(
        DeepCopyProps.call(document.createElement("span"), {
            innerText: "X",
            className: "pointer",
            style: {
                float: "right"
            },
            onclick: function () {
                self.Hide();
            }
        }));

    ele.div_call.appendChild(
        ele.div_inner = buildEle("div", {
            style: { padding: "5%" }
        }));

    ele.div_inner.appendChild(
        ele.input_acc = buildEle("input", {
            type: "text",
            className: "form-control",
            style: { width: "95%" }
        }));

    ele.div_inner.appendChild(
        ele.radio_Div = buildEle("div", {
            style: {
                "margin-top": "5%"
            }
        }));

    /*Radio*/
    self.RadioList_Trans_label = [];
    var RadioList = Language.searchRadioList_Trans, searchRadioList_val = ["", "E", "F"], tmp;
    for (var i = 1; i < RadioList.length; i++) {
        tmp = buildEle("input", {
            type: "radio",
            value: searchRadioList_val[i],
            checked: i == 1,
            name: Id.Radio,
            id: Id.Radio + "_" + i
        });
        ele.radio_Div.appendChild(tmp);

        tmp = buildEle("label", {
            htmlFor: Id.Radio + "_" + i,
            innerText: RadioList[i]
        });
        ele.radio_Div.appendChild(tmp);
        self.RadioList_Trans_label.push(tmp);
    }

    /*Error*/
    ele.div_inner.appendChild(
        ele.error = buildEle("div", {

        }));

    ele.div_inner.appendChild(
        ele.div_func = buildEle("div", {
            style: {
                "text-align": "center", "margin-top": "5%"
            }
        }));

    ele.div_func.appendChild(
        ele.btn_cashIn = buildEle("input", {
            type: "button",
            value: Language.btn_CashIn,
            className: "confirmbtn btn",
            style: {
                "width": "65px"
            },
            onclick: function CashIn() {
                acc = ele.input_acc.value;
                if (checkVal()) {
                    //var tmp = $('input[name="' + Id.Radio + '"]:checked').val();

                    //if (tmp)
                    //    tmp == "E" ? Control.Cash.InOut(uid, 1) : Control.Cash.SetCredit(uid);
                    Control.Cash.InOut(uid, 1);
                }
            }
        }));
    ele.div_func.appendChild(
        ele.btn_cashOut = buildEle("input", {
            type: "button",
            value: Language.btn_CashOut,
            className: "confirmbtn btn",
            style: {
                "width": "65px"
            },
            onclick: function CashOut() {
                acc = ele.input_acc.value;

                if (checkVal())
                    Control.Cash.InOut(uid, 0);
            }
        }));

    ele.div_func.appendChild(
        ele.btn_creditSet = buildEle("input", {
            type: "button",
            value: Language.btn_Confirm,
            className: "confirmbtn btn",
            style: {
                "width": "65px"
            },
            onclick: function () {
                acc = ele.input_acc.value;

                if (checkVal())
                    Control.Cash.SetCredit(uid);
            }
        }));
    function checkVal() {
        $(ele.error).empty();
        var msg = "";
        if (acc == "" && !Test.Number(acc))
            msg = "Invalid Number";

        if (msg.length > 0) {
            $(ele.error).append(buildEle("span", { innerText: msg, style: { color: ColorSetting.ErrorMsg } }));
            return false;
        } else
            return true;
    }

    self.Error = function (msg) {
        $(ele.error).empty();
        $(ele.error).append(buildEle("span", { innerText: msg, style: { color: "red" } }));
    }

    self.Complete = function (msg) {
        if (!msg.Success) {
            CashInOutView.Error(msg.msg);
            LoadingPanel.Hide();
        } else {
            setLayout(Layouts.MEMBERMANAGE);
        }
    }

    self.Init = function () {
        $("body").append(ele.div_pop);
    }
    self.Show = function (UserID, type) {
        uid = UserID;
        ele.input_acc.value = acc = "";
        $(ele.error).empty();
        ele.btn_cashIn.style.display = "none";
        ele.btn_cashOut.style.display = "none";
        ele.radio_Div.style.display = "none";
        ele.btn_creditSet.style.display = "none";
        if (type == 0) {
            /*開分View*/
            ele.title.innerText = Language.btn_CashIn;
            ele.btn_cashIn.style.display = "block";
            //ele.radio_Div.style.display = "block";
        } else if (type == 1) {
            /*洗分view*/
            ele.title.innerText = Language.btn_CashOut;
            ele.btn_cashOut.style.display = "block";
        } else if (type == 2) {
            /*設定信用view*/
            ele.title.innerText = Language.CreditTitle;
            ele.btn_creditSet.style.display = "block";
        }

        $(ele.div_pop).show();
    }
    self.Hide = function () {
        uid = 0;
        $(ele.div_pop).hide();
    }

    self.Refresh = function () {
        ele.btn_cashIn.value = Language.btn_CashIn;
        ele.btn_cashOut.value = Language.btn_CashOut;
        ele.btn_creditSet.value = Language.btn_Confirm;
        ele.title.innerText = Language.CashInOutTitle;

        for (var i = 0; i < self.RadioList_Trans_label.length; i++) {
            self.RadioList_Trans_label[i].innerText = Language.searchRadioList_Trans[i + 1];
        }
    }

    Object.defineProperties(self, {
        Price: { get: function () { return acc; } }
    })
    return self;
}();

var HomePageView = function () {
    var home = {};
    var ele = {};
    var DynamicField_startIndex = 3;
    var DB_FixedField = [
        "Account",
        "NickName",
        "Cash"
        //"OutstandingPayments"
    ]

    /*Level 取代底線_ */
    var DB_DynamicField = [
        "Level_Credit",
        "Level_Count",
        "Level_BanCount",
        "Level_RemainCredit"
    ]

    ele.tr_List = [];
    ele.span_ValList = [];
    ele.span_TitleList = [];
    ele.Table = WebTool.CreateElement("table", {
        style: {
            "width": "100%",
            "border":"none"
        },
        border:"1"
    });
    
    ele.TableTitle = WebTool.CreateElement("span", {
        style: {
            "color": ColorSetting.TableTitle.Font
        }
    })
    home.Div = WebTool.CreateElement("div", {
        style: {
            "padding": "10px",
            "display": "inline-block",
            "max-width": "400px",
            //"width": "30%",
            "min-width": "250px"
        }
    });

    var Create = {
        TR: function () {
            return WebTool.CreateElement("tr");
        },
        TD: function (align, width) {
            return WebTool.CreateElement("td", {
                "width": width ? width : "",
                style: {
                    "text-align": align,
                    "padding": "3px",
                    "border": "1px solid " + ColorSetting.TableTitle.Line,
                    "color":ColorSetting.TableTitle.Content
                }
            });
        },
        SPAN: function (text) {
            return WebTool.CreateElement("span", {
                innerText: text,
                style: {
                    "display": "block"
                }
            });
        },
        DIV: function () {
            return WebTool.CreateElement("div", {
                style: {
                    "word-break": "break-all",
                    "word-wrap": "break-word"
                }
            });
        },
        COUNTSPAN: function (container_span) {
            container_span.appendChild(WebTool.CreateElement("span"));
            container_span.appendChild(WebTool.CreateElement("span", {
                style: {
                    "color": "red"
                }
            }));
            container_span.appendChild(WebTool.CreateElement("span"));

            return container_span;
        }
    }

    home.CreateTable = function () {
        home.CreateTable = function () { }

        home.Div.appendChild(ele.Table);

        var _col = Language.Column_HomePage_Title;
        var obj, tr, td, container;

        /*Top Title*/
        tr = Create.TR();
        tr.style.border = "none";
        ele.Table.appendChild(tr);
        td = Create.TD("");
        td.style.border = "none";
        td.style.padding = "3px 0 8px 18px";
        td.className = "rightback";
        tr.appendChild(td);
        //td.style.backgroundColor = ColorSetting.TableTitle.BG;
        td.colSpan = "2";
        td.appendChild(ele.TableTitle);

        for (var i = 0; i < _col.length; i++) {
            tr = Create.TR();
            ele.tr_List.push(tr);
            ele.Table.appendChild(tr);

            /*Title*/
            td = Create.TD("right", "150px");
            tr.appendChild(td);

            obj = Create.SPAN(_col[i]);
            ele.span_TitleList.push(obj);

            container = Create.DIV();
            container.appendChild(obj);
            td.appendChild(container);

            /*Value*/
            td = Create.TD("");
            tr.appendChild(td);

            obj = Create.SPAN("");
            ele.span_ValList.push(obj);
            td.appendChild(obj);
        }
    }

    home.Init = function () {
        home.Init = function () {

        }
        home.CreateTable();

        /*先把欄位隱藏*/
        for (var i = DynamicField_startIndex; i < Language.Column_HomePage_Title.length; i++) {
            ele.tr_List[i].style.display = "none";
        }

        RightPage.Append(home.Div);
        //$("body").append(home.Div);
    }

    home.Show = function () {
        if (!Reporter.Role)
            return;

        Right_Title.Set(Language.Menu_HomePage);
        home.Div.style.display = "inline-block";
    }

    home.Hide = function () {
        home.Div.style.display = "none";
    }

    home.Refresh = {
        Data: function (msg) {
            if (msg.Record.length == 0)
                return;

            ele.TableTitle.innerText = Reporter.Title + " " + Language.txt_HomePageTitle;

            /*先把欄位隱藏*/
            for (var i = DynamicField_startIndex; i < Language.Column_HomePage_Title.length; i++) {
                ele.tr_List[i].style.display = "none";
            }

            var _col = msg.Column;
            var _r = msg.Record[0];
            var col_index, level = msg.Level;
            var max_level = Reporter.MaxLevel;

            for (var i = 0; i < DB_FixedField.length; i++) {
                col_index = _col.indexOf(DB_FixedField[i]);
                if (col_index == -1)
                    continue;

                ele.span_ValList[i].innerText = numberWithCommasAndCheck(_r[col_index]);
            }

            var col_start_index = [DynamicField_startIndex, DynamicField_startIndex + max_level];
            var _index_0, _index_1, col_banIndex, count, col_remainIndex;
            /*根據 level 顯示 tr 和 value*/
            for (var i = level; i < max_level + 1; i++) {
                _index_0 = col_start_index[0] + i - 1;
                _index_1 = col_start_index[1] + i - 1;

                col_index = _col.indexOf(DB_DynamicField[0].replace('_', i));
                col_remainIndex = _col.indexOf(DB_DynamicField[3].replace('_', i));
                if (col_index != -1 && col_remainIndex != -1)
                    ele.span_ValList[_index_0].innerText = numberWithCommasAndCheck(_r[col_index]) + " ( " + numberWithCommasAndCheck(_r[col_remainIndex]) + " ) ";
                ele.tr_List[_index_0].style.display = "";

                col_index = _col.indexOf(DB_DynamicField[1].replace('_', i + 1));
                col_banIndex = _col.indexOf(DB_DynamicField[2].replace('_', i + 1));
                if (ele.tr_List[_index_1]) {
                    if (col_banIndex != -1 && col_index != -1) {
                        count = numberWithCommasAndCheck(_r[col_banIndex] + _r[col_index]);
                        if (ele.span_ValList[_index_1].children.length == 0)
                            Create.COUNTSPAN(ele.span_ValList[_index_1]);
                        ele.span_ValList[_index_1].children[0].innerText = count + " ( " + numberWithCommasAndCheck(_r[col_index]) + " + ";
                        ele.span_ValList[_index_1].children[1].innerText = numberWithCommasAndCheck(_r[col_banIndex]);
                        ele.span_ValList[_index_1].children[2].innerText = " ) ";
                    }

                    ele.tr_List[_index_1].style.display = "";
                }
            }
        },
        Title: function () {
            var _col = Language.Column_HomePage_Title;
            for (var i = 0; i < _col.length; i++) {
                ele.span_TitleList[i].innerText = _col[i];
            }

            ele.TableTitle.innerText = Reporter.Title + " " + Language.txt_HomePageTitle;
        }
    }

    function numberWithCommasAndCheck(val) {
        return !isNaN(val) ? numberWithCommas(Number(val)) : val;
    }

    return home;
}();

var CashInView = function () {
    var self = {};
    var acc = "", uid = 0;
    var ele = {};
    var win_width = 300;
    var ErrorMsg = "Invalid Number";
    var Id = {}
    var CashInType = "";

    var JqObj = {};

    ele = CreateView.Pop.Window(ele, win_width);

    CreateView.Pop.table(ele, Language.Pop_CashIn_Title.length);

    /*Line One*/
    ele.table_content_td[0].appendChild(
        ele.input_cashin = buildEle("input", {
            style: {
                "width": "100%"
            }
        }));

    /*Line TWO*/
    ele.table_content_td[1].appendChild(
        ele.input_creditin = buildEle("input", {
            style: {
                "width": "100%"
            }
        }));

    ele.btn_cashIn = CreateView.Pop.btn(Language.btn_Confirm);
    ele.btn_cashIn.onclick = function () {
        if (ele.input_cashin.value != "") {
            acc = ele.input_cashin.value;
            CashInType = "E";
        } else if (ele.input_creditin.value != "") {
            CashInType = "F";
            acc = ele.input_creditin.value;
        } else {
            self.Error(ErrorMsg);
            return;
        }

        if (checkVal())
            Control.Cash.InOut(uid, PopType.CASHIN);
    }

    ele.div_footer.appendChild(ele.btn_cashIn);

    function checkVal() {
        JqObj.Error.empty();
        var msg = "";
        if (acc == "" && !Test.Number(acc))
            msg = ErrorMsg;

        if (msg.length > 0) {
            self.Error(msg);
            return false;
        } else
            return true;
    }

    self.Error = function (msg) {
        JqObj.Error.empty();
        JqObj.Error.append(buildEle("span", { innerText: msg, style: { color: ColorSetting.ErrorMsg } }));
    }

    self.Complete = function (msg) {
        if (!msg.Success) {
            CashInView.Error(msg.msg);
            LoadingPanel.Hide();
        } else {
            setLayout(Layouts.MEMBERMANAGE);
        }
    }

    self.Init = function () {
        self.Init = function () {

        }
        JqObj.Error = $(ele.error);
        JqObj.Div = $(ele.div_pop);

        self.Refresh();
        ele.btn_close.onclick = self.Hide;

        $("body").append(ele.div_pop);
    }
    self.Show = function (UserID) {
        uid = UserID;
        ele.input_cashin.value = ele.input_creditin.value = acc = CashInType = "";
        JqObj.Error.empty();

        JqObj.Div.show();
    }

    self.Hide = function () {
        uid = 0;
        JqObj.Div.hide();
    }

    self.Refresh = function () {
        ele.title.innerText = Language.btn_CashIn;
        ele.btn_cashIn.value = Language.btn_Confirm;

        for (var i = 0; i < Language.Pop_CashIn_Title.length; i++) {
            ele.table_title_span[i].innerText = Language.Pop_CashIn_Title[i];
        }
    }

    Object.defineProperties(self, {
        Price: { get: function () { return acc; } },
        Type: {
            get: function () {
                return CashInType;
            }
        }
    })
    return self;
}();

var CashOutView = function () {
    var self = {};
    var acc = "", uid = 0;
    var ele = {};
    var win_width = 300;
    var ErrorMsg = "Invalid Number";
    var Id = {}

    var JqObj = {};

    ele = CreateView.Pop.Window(ele, win_width);

    CreateView.Pop.table(ele, Language.Pop_CashOut_Title.length);

    /*Line One*/
    ele.table_content_td[0].appendChild(
        ele.span_credit = buildEle("span")
        );

    /*Line TWO*/
    ele.table_content_td[1].appendChild(
        ele.span_credit_remain = buildEle("span")
        );

    /*Line Three*/
    ele.table_content_td[2].appendChild(
         ele.span_ud = buildEle("span")
        );

    /*Line Four*/
    ele.table_content_td[3].appendChild(
        ele.span_credit_return = buildEle("span", {
            style: {
                color: "red"
            }
        })
        );

    /*Line Five*/
    ele.table_content_td[4].appendChild(
        ele.span_gamerefund = buildEle("span"));

    /*Line Six*/
    ele.table_content_td[5].appendChild(
        ele.span_credit_out = buildEle("span"));

    /*Line Seven*/
    ele.table_content_td[6].appendChild(
        ele.span_member_sum = buildEle("span"));

    /*Line Eight*/
    //ele.table_content_td[7].appendChild(
    //    ele.input_cashout = buildEle("input", {
    //        style: {
    //            "width": "95%"
    //        }
    //    }));

    ele.btn_cashOut = CreateView.Pop.btn(Language.btn_Confirm);
    ele.btn_cashOut.onclick = function () {
        //acc = ele.input_cashout.value;

        //if (checkVal())
        Control.Cash.InOut(uid, PopType.CASHOUT);
    }

    ele.div_footer.appendChild(ele.btn_cashOut);

    function checkVal() {
        JqObj.Error.empty();
        var msg = "";
        //if (acc != "")
        //    msg = ErrorMsg;

        if (msg.length > 0) {
            JqObj.Error.append(buildEle("span", { innerText: msg, style: { color: ColorSetting.ErrorMsg } }));
            return false;
        } else
            return true;
    }

    self.Error = function (msg) {
        JqObj.Error.empty();
        JqObj.Error.append(buildEle("span", { innerText: msg, style: { color: ColorSetting.ErrorMsg } }));
    }

    self.Complete = function (msg) {
        if (!msg.Success) {
            CashOutView.Error(msg.msg);
            LoadingPanel.Hide();
        } else {
            setLayout(Layouts.MEMBERMANAGE);
        }
    }

    self.Init = function () {
        self.Init = function () {

        }
        JqObj.Error = $(ele.error);
        JqObj.Div = $(ele.div_pop);

        self.Refresh();
        ele.btn_close.onclick = self.Hide;

        $("body").append(ele.div_pop);
    }
    self.Show = function (UserID) {
        LoadingPanel.Show();
        uid = UserID;
        //ele.input_cashout.value = acc = "";
        JqObj.Error.empty();

        Control.Cash.GetCreditInfo(UserID, PopType.CASHOUT);
    }

    self.Set = {
        CreditInfo: function (msg) {
            var col = ["name", "credit", "creditremain", "ud", "credit_return", "gamerefund", "credit_out", "member_sum"];

            var record = msg.Record, column = msg.Column

            var cash_out = record[column.indexOf("credit_out")],
                credit_return = record[column.indexOf("credit_return")],
                member_sum = record[column.indexOf("member_sum")];

            ele.span_credit.innerText = numberWithCommas(record[column.indexOf("credit")]);
            ele.span_ud.innerText = numberWithCommas(record[column.indexOf("ud")]);
            ele.span_credit_remain.innerText = numberWithCommas(record[column.indexOf("creditremain")]);
            ele.span_gamerefund.innerText = numberWithCommas(record[column.indexOf("gamerefund")]);

            ele.span_credit_out.innerText = cash_out > 0 ? numberWithCommas(cash_out) : 0;
            ele.span_credit_return.innerText = numberWithCommas(credit_return);
            ele.span_member_sum.innerText = numberWithCommas(member_sum);

            ele.span_member_sum.style.color = member_sum >= 0 ? "" : "red";

            JqObj.Div.show();
        }
    }
    self.Hide = function () {
        uid = 0;
        JqObj.Div.hide();
    }

    self.Refresh = function () {
        ele.title.innerText = Language.btn_CashOut;
        ele.btn_cashOut.value = Language.btn_Confirm;

        for (var i = 0; i < Language.Pop_CashOut_Title.length; i++) {
            ele.table_title_span[i].innerText = Language.Pop_CashOut_Title[i];
        }
    }

    Object.defineProperties(self, {
        Price: { get: function () { return acc; } }
    })
    return self;
}();

var SetCreditView = function () {
    var self = {};
    var acc = "", uid = 0;
    var ele = {};
    var win_width = 300;
    var ErrorMsg = "Invalid Number";
    var Id = {}

    var JqObj = {};

    ele = CreateView.Pop.Window(ele, win_width);

    CreateView.Pop.table(ele, Language.Pop_SetCredit_Title.length);

    /*Line One*/
    ele.table_content_td[0].appendChild(
        ele.span_credit = buildEle("span")
        );

    /*Line TWO*/
    ele.table_content_td[1].appendChild(
         ele.span_credit_remain = buildEle("span")
        );

    /*Line Three*/
    ele.table_content_td[2].appendChild(
        ele.input_add = buildEle("input", {
            style: {
                "width": "100%"
            }
        }));

    /*Line Four*/
    ele.table_content_td[3].appendChild(
        ele.input_minus = buildEle("input", {
            style: {
                "width": "100%"
            }
        }));

    ele.btn_creditSet = CreateView.Pop.btn(Language.btn_Confirm);
    ele.btn_creditSet.onclick = function () {
        if (ele.input_add.value != "") {
            acc = ele.input_add.value;
        } else if (ele.input_minus.value != "") {
            acc = "-" + ele.input_minus.value;
        } else {
            self.Error(ErrorMsg);
            return;
        }

        if (checkVal())
            Control.Cash.SetCredit(uid);
    }


    ele.div_footer.appendChild(ele.btn_creditSet);

    function checkVal() {
        JqObj.Error.empty();
        var msg = "";
        if (acc == "" && !Test.Number(acc))
            msg = ErrorMsg;

        if (msg.length > 0) {
            self.Error(msg);
            return false;
        } else
            return true;
    }

    self.Error = function (msg) {
        JqObj.Error.empty();
        JqObj.Error.append(buildEle("span", { innerText: msg, style: { color: ColorSetting.ErrorMsg } }));
    }

    self.Complete = function (msg) {
        if (!msg.Success) {
            SetCreditView.Error(msg.msg);
            LoadingPanel.Hide();
        } else {
            setLayout(Layouts.MEMBERMANAGE);
        }
    }

    self.Init = function () {
        self.Init = function () {

        }
        JqObj.Error = $(ele.error);
        JqObj.Div = $(ele.div_pop);

        self.Refresh();
        ele.btn_close.onclick = self.Hide;

        $("body").append(ele.div_pop);
    }
    self.Show = function (UserID) {
        LoadingPanel.Show();
        uid = UserID;
        ele.input_minus.value = ele.input_add.value = acc = "";
        JqObj.Error.empty();

        Control.Cash.GetCreditInfo(UserID, PopType.SETCREDIT);
    }

    self.Set = {
        CreditInfo: function (msg) {
            var record = msg.Record, column = msg.Column

            ele.span_credit.innerText = numberWithCommas(record[column.indexOf("credit")]);
            ele.span_credit_remain.innerText = numberWithCommas(record[column.indexOf("creditremain")]);

            JqObj.Div.show();
        }
    }
    self.Hide = function () {
        uid = 0;
        JqObj.Div.hide();
    }

    self.Refresh = function () {
        ele.title.innerText = Language.CreditTitle;
        ele.btn_creditSet.value = Language.btn_Confirm;

        for (var i = 0; i < Language.Pop_SetCredit_Title.length; i++) {
            ele.table_title_span[i].innerText = Language.Pop_SetCredit_Title[i];
        }
    }

    Object.defineProperties(self, {
        Price: { get: function () { return acc; } }
    })
    return self;
}();

var OnlineView = function () {
    var o = {};
    o.errSpan = null;
    var pageColor = ColorSetting.Page;
    o.print_DivId = "printZone";
    var perTablesMaxPlayer = 8;

    o.Data = {};

    var id_qrCountInput = "qrcount";

    var ele = {};

    o.Div = WebTool.CreateElement("div", {
        style: {
            "display": "none"
        }
    });

    o.Div_Menu = buildEle("div", {
        style: {
            "padding": "10px"
        }
    });
    o.Div.appendChild(o.Div_Menu);

    ele.btn_Update = buildEle("input", {
        type: "button",
        className: "confirmbtn leftBtn",
        style: {
            "margin": "0",
            "padding": "3px"
        },
        value: Language.btn_Update
    });
    o.Div_Menu.appendChild(ele.btn_Update);

    o.DivList = WebTool.CreateElement("div", {
        style: {
            "width": "100%",
            "padding-top": "10px",
            "padding-left": "1%"
        }
    });

    /*Error Msg*/
    ele.ErrorMsg = WebTool.CreateElement("span", {
        style: {
            "color": ColorSetting.ErrorMsg
        }
    });

    o.ListView = function (list) {
        ele.btn_Update.value = Language.btn_Update;
        if (Test.Undefined(o.DivList)) {
            return;
        }

        var div = o.DivList;
        div.innerHTML = "";
        o.Data = {};

        if (MainPage.onlineGID == "" || isUndefined(list))
            return;

        o.Data = list;

        if (Test.Undefined(o.Data) || Test.Undefined(o.Data.Games) || Test.Undefined(o.Data.Games[MainPage.onlineGID]))
            return;

        var record = o.Data.Games[MainPage.onlineGID], perrecord, content, playerrecord = o.Data.Players, isFindPlayer, loginTime;

        var tr, date, img, btn, td;
        var table = WebTool.CreateElement("table", {
            "border": "1"
        });
        table.style.pageBreakAfter = "always";
        div.appendChild(table);

        CreateView.Table.Title(table, Language.Column_Online_Title);

        /*Record*/
        tr = WebTool.CreateElement("tr", {
            style: {
                color:ColorSetting.TableTitle.Content
            }
        });
        DataSheet.insert_content(tr, record.Round);
        for (var i = 0; i < record.AllBets.length; i++) {
            DataSheet.insert_content(tr, record.AllBets[i]);
        }
        DataSheet.insert_content(tr, record.AllBets.Sum());
        DataSheet.insert_content(tr, record.AccBet);
        DataSheet.insert_content(tr, record.AccReward);
        table.appendChild(tr);

        table = WebTool.CreateElement("table", {
            "border": "1",
            "style": {
                "margin-top": "10px"
            }
        });
        table.style.pageBreakAfter = "always";

        CreateView.Table.Title(table, Language.Column_Online_PlayerList_Title);

        for (var i = 0; i < playerrecord.length; i++) {
            if (playerrecord[i].Key != MainPage.onlineGID)
                continue;

            for (var j = 0; j < playerrecord[i].Value.length; j++) {
                tr = WebTool.CreateElement("tr", {
                    style: {
                        color: ColorSetting.TableTitle.Content
                    }
                });
                perrecord = playerrecord[i].Value[j];

                DataSheet.insert_content(tr, perrecord.Acc);
                DataSheet.insert_content(tr, perrecord.Name);

                for (var k = 0; k < perrecord.bets.length; k++) {
                    DataSheet.insert_content(tr, perrecord.bets[k]);
                }
                DataSheet.insert_content(tr, perrecord.bets.Sum());

                table.appendChild(tr);
            }
        }

        div.appendChild(table);
    }

    o.Init = function () {
        o.Init = function () {

        }
        ele.btn_Update.onclick = Control.Online.Send;

        o.Div.appendChild(o.DivList);

        RightPage.Append(o.Div);
        //$("body").append(o.Div);
    }

    o.Error = function (msg) {
        ele.ErrorMsg.innerText = msg;
    }

    o.Show = function () {
        Right_Title.Set(Language.Title_Online);
        o.Div.style.display = "inline-block";
    }
    o.Hide = function () {
        o.DivList.innerHTML = ""
        o.Div.style.display = "none";
    }

    o.Refresh = function () {
        Right_Title.Set(Language.Title_Online);
        o.ListView(o.Data);
    }

    return o;
}();

var TransSettingView = function () {
    var SetType = Tools.Enumify("WEEK", "TIME");

    var Table_Column = {
        Week: [
            "ID",
            "Currency",
            "w0",
            "w1",
            "w2",
            "w3",
            "w4",
            "w5",
            "w6"
        ],
        Time: [

        ]
    }

    var Padding = {
        Tab: {
            Div: "10px",
            Table: "5px"
        }
    }

    WebTool.addCss({
        ".Setting_td": {
            "padding": Padding.Tab.Table
        }
    });

    var t = {}, ele = {};
    t.errSpan = null;

    t.Data = {};
    t.TargetUID = 0;

    t.Div = WebTool.CreateElement("div", {
        style: {
            "display": "none",
            "width": "75%"
        }
    });

    var CREATE = {
        DIV: function () {
            return WebTool.CreateElement("div");
        },
        TABLE: function () {
            return WebTool.CreateElement("table");
        },
        TR: function () {
            return WebTool.CreateElement("tr");
        },
        TD_MENU: function () {
            return WebTool.CreateElement("td", {
                style: {
                    "padding": "5px"
                }
            });
        },
        TableTitleObj: function (type) {
            var result = [];
            var col = ele.Tab.GetColumn(type),
                title = ele.Tab.GetTitle(type);

            if (col.length != title.length)
                return result;

            for (var i = 0; i < col.length; i++) {
                result[i] = {
                    column: col[i],
                    title: title[i]
                }
            }

            return result;
        },
        Btn: function () {
            return WebTool.CreateElement("input", { type: "button" });
        }
    }

    ele.Tab = {
        Week: {
            Div: CREATE.DIV(),
            Table: null
        },
        Time: {
            Div: CREATE.DIV(),
            Table: null
        },
        GetCId: function (col, key_col, record) {
            var result = -1, k;

            for (var i = 0; i < col.length; i++) {
                if (!("column" in col[i]) || key_col != col[i].column)
                    continue;

                result = record[i];
                break;
            }

            return result;
        },
        Init: function () {
            this.Init = function () { }

            this.HideAll();
            this.Week.Div.style.padding = this.Time.Div.style.padding = Padding.Tab.Div;
            t.Div.appendChild(this.Week.Div);
            var _table = new WebTool.Table(this.Week.Div);
            _table.Title = CREATE.TableTitleObj(SetType.WEEK);
            _table.PerTitle.SetDataView = function () {
                this.Td.style.padding = Padding.Tab.Table;
                this.Td.style.backgroundColor = ColorSetting.TableTitle.BG;
                this.Td.style.color = ColorSetting.TableTitle.Font;
            }

            _table.PerCell.SetDataView = function () {
                var td = this.Td;
                var t_obj = this.TitleObj;
                var all_col = this.AllTitleObj;

                td.style.padding = Padding.Tab.Table;

                if (t_obj.column == "Currency") {
                    this.Text = Language.Cell_Currency[this.Text - 1];
                } else if (t_obj.column.charAt(0) == "w") {
                    /*week setting*/
                    var checkbox = WebTool.CreateElement("input", {
                        type: "checkbox",
                        id: "checkbox_" + t_obj.column + "_" + ele.Tab.GetCId(all_col, "Currency", this.RecordArr)
                    });
                    checkbox.checked = this.Text == 1;
                    this.Td.appendChild(checkbox);
                }
            }

            this.Week.Table = _table;
            this.Week.Table.Init();
            //this.Time.Table = new WebTool.Table(this.Time.Div);

        },
        Switch: function (type) {
            this.HideAll();
            switch (type) {
                case SetType.TIME:
                    ele.Tab.Time.Div.style.display = "";
                    break;
                case SetType.WEEK:
                    ele.Tab.Week.Div.style.display = "";
                    break;
            }
        },
        HideAll: function () {
            for (var item in SetType) {
                this.Hide(SetType[item]);
            }
        },
        Hide: function (type) {
            switch (type) {
                case SetType.TIME:
                    ele.Tab.Time.Div.style.display = "none";
                    break;
                case SetType.WEEK:
                    ele.Tab.Week.Div.style.display = "none";
                    break;
            }
        },
        GetColumn: function (type) {
            switch (type) {
                case SetType.TIME:
                    return Table_Column.Time;
                    break;
                case SetType.WEEK:
                    return Table_Column.Week;
                    break;
            }

            return [];
        },
        GetTitle: function (type) {
            switch (type) {
                case SetType.TIME:
                    return Language.Setting_Time_Title;
                    break;
                case SetType.WEEK:
                    return Language.Setting_Week_Title;
                    break;
            }

            return [];
        },
        GetTable: function (type) {
            switch (type) {
                case SetType.TIME:
                    return ele.Tab.Time.Table;
                    break;
                case SetType.WEEK:
                    return ele.Tab.Week.Table;
                    break;
            }

            return null;
        },
        RefreshTitle: function () {
            this.Week.Table = this._updateTitle(this.Week.Table, SetType.WEEK);

            this.Time.Table = this._updateTitle(this.Time.Table, SetType.TIME);
        },
        _updateTitle: function (_table, type) {
            if (!_table)
                return _table;

            var col = _table.Title,
                title = ele.Tab.GetTitle(type);

            if (col.length == title.length) {
                for (var i = 0; i < col.length; i++) {
                    col[i].title = title[i];
                }
                _table.RefreshTitle();
            }

            return _table;
        },

    };

    ele.Menu = {
        Div: CREATE.DIV(),
        Table: CREATE.TABLE(),
        Btn: [],
        Init: function () {
            this.Init = function () { }

            /*Init*/
            t.Div.appendChild(this.Div);
            this.Div.appendChild(this.Table);
            this.Table.style.borderCollapse = "separate";

            var tr = CREATE.TR(), td = CREATE.TD_MENU(), btn;
            this.Table.appendChild(tr);
            tr.appendChild(td);

            /*設定日期*/
            btn = CREATE.Btn();
            btn.onclick = function () {
                ele.Tab.Switch(SetType.WEEK);
                Control.Setting.RequestData(SetType.WEEK, 0);
            }
            this.Btn.push(btn);
            td.appendChild(btn);

            td = CREATE.TD_MENU()
            tr.appendChild(td);

            /*設定時間*/
            btn = CREATE.Btn();
            btn.onclick = function () {
                ele.Tab.Switch(SetType.TIME);
            }
            this.Btn.push(btn);
            td.appendChild(btn);

            this.Refresh();
        },
        Refresh: function () {
            for (var i = 0; i < Language.Setting_Menu_Title.length; i++) {
                if (!ele.Menu.Btn[i])
                    continue;

                ele.Menu.Btn[i].value = Language.Setting_Menu_Title[i];
            }
        }
    }

    t.RefreshData = function (type, Data) {
        var table = ele.Tab.GetTable(type);
        if (!table)
            return;

        t.TargetUID = Data.UID;
        table.Data = Data;
        table.RefreshContent();
    }

    /*Error Msg*/
    ele.ErrorMsg = WebTool.CreateElement("span", {
        style: {
            "color": ColorSetting.ErrorMsg
        }
    });

    t.Init = function () {
        t.Init = function () {

        }

        ele.Menu.Init();
        ele.Tab.Init();

        $("body").append(t.Div);
    }

    t.Error = function (msg) {
        ele.ErrorMsg.innerText = msg;
    }

    t.Show = function () {
        Right_Title.Set(Language.Menu_reoprtCfg);
        t.Div.style.display = "inline-block";
    }
    t.Hide = function () {
        t.Div.style.display = "none";
    }

    t.Refresh = function () {
        //ele.Menu.Refresh();
        //ele.Tab.RefreshTitle();
    }

    return t;
}();

var CheckOutView = function () {
    var o = {};
    var pageColor = ColorSetting.Page;

    o.Data = {};
    o.Data_Time = {};

    var ele = {};

    o.Div = WebTool.CreateElement("div", {
        style: {
            "display": "none",
            "width": "75%"
        }
    });

    /*Info*/
    o.Div_Info = buildEle("div", {
        style: {
            "padding": "10px",
            "padding-top": "0"
        }
    });
    o.Div.appendChild(o.Div_Info);

    ele.span_lastcheckouttime_title = buildEle("span", {
        innerText: Language.txt_LastCheckOutTime
    });
    o.Div_Info.appendChild(ele.span_lastcheckouttime_title);

    o.span_LastCheckOutTime = buildEle("span");
    o.Div_Info.appendChild(o.span_LastCheckOutTime);

    ele.span_lastcheckoutfinish_title = buildEle("span", {
        innerText: Language.txt_LastCheckOutFinish
    });
    o.Div_Info.appendChild(ele.span_lastcheckoutfinish_title);

    o.span_LastCheckOutFinish = buildEle("span");
    o.Div_Info.appendChild(o.span_LastCheckOutFinish);

    /*Menu*/
    o.Div_Menu = buildEle("div", {
        style: {
            "padding": "10px",
            "padding-top": "0"
        }
    });
    o.Div.appendChild(o.Div_Menu);

    o.btn_CheckOut = buildEle("input", {
        type: "button",
        value: Language.btn_CheckOut,
        disabled: true
    });

    o.btn_CheckOut.onclick = Control.Cashout.Excute;
    o.Div_Menu.appendChild(o.btn_CheckOut);

    o.btn_CheckOutFinish = buildEle("input", {
        type: "button",
        value: Language.btn_CheckOutFinish,
        disabled: true
    });

    o.btn_CheckOutFinish.onclick = Control.Cashout.Finish;
    o.Div_Menu.appendChild(o.btn_CheckOutFinish);

    o.btn_CheckOutRelease = buildEle("input", {
        type: "button",
        value: Language.btn_CheckOutRelease,
        style: {
            "color": "red",
            "display":"none"
        }
    });

    o.btn_CheckOutRelease.onclick = Control.Cashout.Release;
    o.Div_Menu.appendChild(o.btn_CheckOutRelease);

    /*Duration*/
    o.Div_Duration = buildEle("div", {
        style: {
            "padding": "10px",
            "padding-top": "0"
        }
    });
    o.Div.appendChild(o.Div_Duration);

    ele.span_duration = buildEle("span", {
    });
    o.Div_Duration.appendChild(ele.span_duration);

    o.DivList = WebTool.CreateElement("div", {
        style: {
            "width": "100%",
            "padding-left": "10px",
            "padding-bottom": "10px"
        }
    });

    /*Error Msg*/
    ele.ErrorMsg = WebTool.CreateElement("span", {
        style: {
            "color": ColorSetting.ErrorMsg
        }
    });
    o.Div_Menu.appendChild(ele.ErrorMsg);

    o.ListView = function (list) {
        if (Test.Undefined(o.DivList)) {
            return;
        }

        var div = o.DivList;
        div.innerHTML = "";
        o.Data = {};

        if (isUndefined(list))
            return;

        o.Data = list;

        if (Test.Undefined(list.Column) || Test.Undefined(list.Record))
            return;

        var col = ["name", "credit", "creditremain", "ud", "cash_sum","cash_flow", "credit_return", "gamerefund", "credit_out", "member_sum"];

        var record = list.Record, column = list.Column, content, colIndex;

        var tr = WebTool.CreateElement("tr", {
            style: {
                "color": ColorSetting.TableTitle.Font, "background-color": ColorSetting.TableTitle.BG
            }
        });
        var table = WebTool.CreateElement("table", {
            "border": "1"
        });
        table.style.pageBreakAfter = "always";
        div.appendChild(table);

        CreateView.Table.Title(table, Language.Column_CheckOut_Title);

        /*Record*/
        for (var i = 0; i < record.length; i++) {
            tr = WebTool.CreateElement("tr", {
                style: {
                    color:ColorSetting.TableTitle.Content
                }
            });

            for (var j = 0; j < col.length; j++) {
                colIndex = column.indexOf(col[j]);

                if (colIndex == -1) {
                    DataSheet.insert_content(tr, "");
                    DataSheet.insert()
                    continue;
                }

                content = record[i][colIndex];

                if (col[j] == "credit_return")
                    DataSheet.insert_content_red(tr, content);
                else
                    DataSheet.insert_content(tr, content);
            }

            table.appendChild(tr);
        }

        /*Sum*/
        if (!Test.Undefined(list.Sum)) {
            record = list.Sum;
            tr = WebTool.CreateElement("tr", {
                style: {
                    "border": "3px solid " + ColorSetting.TableTitle.Line,
                    color: ColorSetting.TableTitle.Content
                }
            });

            for (var j = 0; j < col.length; j++) {
                colIndex = column.indexOf(col[j]);

                if (colIndex == -1) {
                    DataSheet.insert_content(tr, "");
                    continue;
                }

                content = record[colIndex];

                if (col[j] == "credit_return")
                    DataSheet.insert_content_red(tr, content);
                else
                    DataSheet.insert_content(tr, content);
            }

            table.appendChild(tr);
        }
    }

    o.Init = function () {
        o.Init = function () {

        }

        o.Div.appendChild(o.DivList);

        RightPage.Append(o.Div);
        //$("body").append(o.Div);
    }

    o.Error = function (msg) {
        if (Test.Undefined(ele.ErrorMsg))
            return;
        ele.ErrorMsg.innerText = msg;
    }

    o.Show = function () {
        o.Error("");
        Right_Title.Set(Language.Menu_Checkout);
        o.Div.style.display = "inline-block";
    }
    o.Hide = function () {
        o.DivList.innerHTML = ""
        o.Div.style.display = "none";
    }

    o.Refresh = function () {
        o.btn_CheckOutFinish.value = Language.btn_CheckOutFinish;
        o.btn_CheckOut.value = Language.btn_CheckOut;
        ele.span_lastcheckouttime_title.innerText = Language.txt_LastCheckOutTime;
        ele.span_lastcheckoutfinish_title.innerText = Language.txt_LastCheckOutFinish;
        o.ListView(o.Data);

        o.span_LastCheckOutFinish.innerText = o.Data_Time.finish ? Language.CheckOutStatus[1] : Language.CheckOutStatus[0];
    }

    o.UpdateCheckOutTime = function (msg) {
        o.Data_Time = msg;
        ele.span_duration.innerText = msg.time + " ~ " + getNow();
        o.span_LastCheckOutTime.innerText = msg.time;
        o.span_LastCheckOutFinish.innerText = msg.finish ? Language.CheckOutStatus[1] : Language.CheckOutStatus[0];


        if (msg.time != "" && msg.time != "無") {
            var tmp = new Date(msg.time);

            CreateView.Date.Calendar.SetTime(CreateView.Date.Type.DateTime, RightView.SelectObj, RightView.Search, CreateView.Date.Type.Start, tmp);
            CreateView.Date.Calendar.SetTime(CreateView.Date.Type.Date, Day_Trans_Report_View.SelectObj, Day_Trans_Report_View.Search, CreateView.Date.Type.Start, tmp);
            CreateView.Date.Calendar.SetTime(CreateView.Date.Type.DateTime, CheckOutHistoryView.SelectObj, CheckOutHistoryView.Search, CreateView.Date.Type.Start, tmp);

        }
    }

    o.UpdateCheckOutBtn = function (msg) {
        if (!("btn_ckeckout" in msg) || !("btn_checkout_finish" in msg))
            return;

        if (o.btn_CheckOut && o.btn_CheckOutFinish) {
            o.btn_CheckOut.disabled = !msg.btn_ckeckout;
            o.btn_CheckOutFinish.disabled = !msg.btn_checkout_finish;
        }
    }

    return o;
}();

var CheckOutSettingView = function () {

    var Table_Column = {
        Week: [
            "w0",
            "w1",
            "w2",
            "w3",
            "w4",
            "w5",
            "w6"
        ]
    }

    var Padding = {
        Tab: {
            Div: "10px",
            Table: "5px"
        }
    }

    var t = {}, ele = {};
    t.errSpan = null;

    t.Data = {};

    t.Div = WebTool.CreateElement("div", {
        style: {
            "display": "none"
        }
    });

    var select_key_id = "select_";

    var CREATE = {
        DIV: function () {
            return WebTool.CreateElement("div");
        },
        TABLE: function () {
            return WebTool.CreateElement("table");
        },
        TR: function () {
            return WebTool.CreateElement("tr");
        },
        TD_MENU: function () {
            return WebTool.CreateElement("td", {
                style: {
                    "padding": "5px"
                }
            });
        },
        TableTitleObj: function () {
            var result = [];
            var col = Table_Column.Week,
                title = Language.Setting_Week_Title;

            if (col.length != title.length)
                return result;

            for (var i = 0; i < col.length; i++) {
                result[i] = {
                    column: col[i],
                    title: title[i]
                }
            }

            return result;
        },
        Btn: function () {
            return WebTool.CreateElement("input", {
                type: "button",
                className: "confirmbtn leftBtn",
                style: {
                    "margin": "0",
                    "padding": "3px"
                },
            });
        }
    }

    t.Btn_Confirm = CREATE.Btn();

    t.Select_Control = {};

    ele.Tab = {
        Div: CREATE.DIV(),
        Table: null,
        GetCId: function (col, key_col, record) {
            var result = -1, k;

            for (var i = 0; i < col.length; i++) {
                if (!("column" in col[i]) || key_col != col[i].column)
                    continue;

                result = record[i];
                break;
            }

            return result;
        },
        GetSelect: function (col) {
            if (t.Select_Control[col])
                return t.Select_Control[col];

            var _select = WebTool.CreateElement("select", {
                id: select_key_id + col
            }), _op = WebTool.CreateElement("option");
            _op.value = _op.textContent = "-";
            _select.appendChild(_op);

            for (var i = 0; i < 24; i++) {
                _op = WebTool.CreateElement("option");
                _op.value = _op.textContent = padLeft(2, i.toString(), "0") + ":00";

                _select.appendChild(_op);
            }

            t.Select_Control[col] = _select;
            return t.Select_Control[col];
        },
        Init: function () {
            this.Init = function () { }

            this.Div.style.padding = Padding.Tab.Div;
            t.Div.appendChild(this.Div);
            var _table = new WebTool.Table(this.Div);
            _table.Title = CREATE.TableTitleObj();
            _table.PerTitle.SetDataView = function () {
                this.Td.style.padding = Padding.Tab.Table;
                this.Td.style.backgroundColor = ColorSetting.TableTitle.BG;
                this.Td.style.color = ColorSetting.TableTitle.Font;
                this.Td.style.border = "1px solid " + ColorSetting.TableTitle.Line;
                this.Td.style.textAlign = "center";
            }

            _table.PerCell.SetDataView = function () {
                var td = this.Td;
                var t_obj = this.TitleObj;
                var all_col = this.AllTitleObj;

                td.style.padding = Padding.Tab.Table;
                td.style.border = "1px solid " + ColorSetting.TableTitle.Line;

                /*week setting*/
                var _select = ele.Tab.GetSelect(t_obj.column);

                _select.value = this.Text.substring(0, 5);

                this.Text = "";

                this.Td.appendChild(_select);
            }

            this.Table = _table;
            this.Table.Init();
        },
        Switch: function (type) {
            this.HideAll();
            switch (type) {
                case SetType.TIME:
                    ele.Tab.Time.Div.style.display = "";
                    break;
                case SetType.WEEK:
                    ele.Tab.Week.Div.style.display = "";
                    break;
            }
        },
        Hide: function () {
            ele.Tab.Div.style.display = "none";
        },
        GetTable: function () {
            return ele.Tab.Table;

        },
        RefreshTitle: function () {
            this.Table = this._updateTitle(this.Table);

            //this.Time.Table = this._updateTitle(this.Time.Table, SetType.TIME);
        },
        _updateTitle: function (_table) {
            if (!_table)
                return _table;

            var col = _table.Title,
                title = Language.Setting_Week_Title;

            if (col.length == title.length) {
                for (var i = 0; i < col.length; i++) {
                    col[i].title = title[i];
                }
                _table.RefreshTitle();
            }

            return _table;
        },

    };

    t.RefreshData = function (Data) {
        var table = ele.Tab.Table;
        if (!table)
            return;

        t.TargetUID = Data.UID;
        table.Data = Data;
        table.RefreshContent();
    }

    /*Error Msg*/
    ele.ErrorMsg = WebTool.CreateElement("span", {
        style: {
            "color": ColorSetting.ErrorMsg
        }
    });

    t.Init = function () {
        t.Init = function () {

        }

        t.Btn_Confirm.onclick = Control.Setting.Confirm;
        t.Btn_Confirm.value = Language.btn_Confirm;
        t.Btn_Confirm.style.marginLeft = Padding.Tab.Div;
        t.Div.appendChild(t.Btn_Confirm);
        t.Div.appendChild(ele.ErrorMsg);

        //ele.Menu.Init();
        ele.Tab.Init();

        RightPage.Append(t.Div);
    }

    t.Error = function (msg) {
        ele.ErrorMsg.innerText = msg;
    }

    t.Show = function () {
        t.Error("");
        Right_Title.Set(Language.Menu_reoprtCfg);

        Control.Setting.RequestData(0);

        t.Div.style.display = "inline-block";
    }
    t.Hide = function () {
        t.Div.style.display = "none";
    }

    t.Refresh = function () {
        //ele.Menu.Refresh();
        ele.Tab.RefreshTitle();

        t.Btn_Confirm.value = Language.btn_Confirm;
    }

    t.GetSelectVal = function () {
        var result = [];
        var control, col;

        for (var i = 0; i < Table_Column.Week.length; i++) {
            col = Table_Column.Week[i];
            if (col in t.Select_Control)
                result[i] = t.Select_Control[col].value;
            else
                result[i] = "-";
        }

        return result;
    }

    return t;
}();

var CheckOutHistoryView = function () {

    var Table_Column = [
        "Start",
        "CreateDate",
        "name",
        "credit",
        "creditremain",
        "ud",
        "cash_sum",
        "cash_flow",
        "credit_return",
        "gamerefund",
        "credit_out",
        "member_sum"
    ]

    var Padding = {
        Tab: {
            Div: "10px",
            Table: "5px"
        }
    }

    var t = {}, ele = {};
    t.errSpan = null;

    t.Data = {};

    t.Search = {
        StartTime: "",
        EndTime: "",
        PageIndex: [1, 1, 1, 1],
        PageCount: [],
        Id: [1, 1, 1, 1]
    }

    t.Div = WebTool.CreateElement("div", {
        style: {
            "display": "none"
        }
    });

    var select_key_id = "select_";

    var CREATE = {
        DIV: function () {
            return WebTool.CreateElement("div");
        },
        TABLE: function () {
            return WebTool.CreateElement("table");
        },
        TR: function () {
            return WebTool.CreateElement("tr");
        },
        TD_MENU: function () {
            return WebTool.CreateElement("td", {
                style: {
                    "padding": "5px"
                }
            });
        },
        TableTitleObj: function () {
            var result = [];
            var col = Table_Column,
                title = Language.Column_CheckOut_History_Title;

            if (col.length != title.length)
                return result;

            for (var i = 0; i < col.length; i++) {
                result[i] = {
                    column: col[i],
                    title: title[i]
                }
            }

            return result;
        },
        Btn: function () {
            return WebTool.CreateElement("input", {
                type: "button",
                className: "confirmbtn leftBtn",
                style: {
                    "margin": "0",
                    "padding": "3px"
                },
            });
        }
    }

    ele.searchDurationDiv = buildEle("div", {
        style: {
            "padding-left": Padding.Tab.Table,
            "padding-bottom": Padding.Tab.Table
        }
    });
    ele.duration_span = buildEle("span");
    ele.searchDurationDiv.appendChild(ele.duration_span);

    t.Btn_Confirm = CREATE.Btn();

    t.Btn_Confirm.style.marginRight = Padding.Tab.Div;

    t.TextName = buildEle("input");
    ele.Span_Name = buildEle("span", {
        innerText: Language.txt_searchUser,
        className: "span_right"
    });

    /*Error Msg*/
    ele.ErrorMsg = WebTool.CreateElement("span", {
        style: {
            "color": ColorSetting.ErrorMsg
        }
    });

    //t.searchDiv = buildEle("div", { style: { "margin-top": "10px" } });
    t.SelectObj = CreateView.Date.Calendar.Control(CreateView.Date.Type.DateTime);
    t.searchDiv = t.SelectObj.Container;

    t.searchDiv.insertBefore(t.TextName, t.searchDiv.firstChild);
    t.searchDiv.insertBefore(ele.Span_Name, t.searchDiv.firstChild);

    t.searchDiv.insertBefore(ele.ErrorMsg, t.SelectObj.Select_checkoutduration);
    t.searchDiv.insertBefore(t.Btn_Confirm, t.SelectObj.Select_checkoutduration);
    
    t.searchDiv.style.paddingLeft = Padding.Tab.Table;

    t.Select_Control = {};

    ele.Tab = {
        Div: CREATE.DIV(),
        Table: null,
        GetCId: function (col, key_col, record) {
            var result = -1, k;

            for (var i = 0; i < col.length; i++) {
                if (!("column" in col[i]) || key_col != col[i].column)
                    continue;

                result = record[i];
                break;
            }

            return result;
        },
        GetSelect: function (col) {
            if (t.Select_Control[col])
                return t.Select_Control[col];

            var _select = WebTool.CreateElement("select", {
                id: select_key_id + col
            }), _op = WebTool.CreateElement("option");
            _op.value = _op.textContent = "-";
            _select.appendChild(_op);

            for (var i = 0; i < 24; i++) {
                _op = WebTool.CreateElement("option");
                _op.value = _op.textContent = padLeft(2, i.toString(), "0") + ":00";

                _select.appendChild(_op);
            }

            t.Select_Control[col] = _select;
            return t.Select_Control[col];
        },
        Init: function () {
            this.Init = function () { }

            this.Div.style.padding = Padding.Tab.Div;
            t.Div.appendChild(this.Div);
            var _table = new WebTool.Table(this.Div);
            _table.Config.SetFooter = true;
            _table.Title = CREATE.TableTitleObj();
            _table.PerTitle.SetDataView = function () {
                this.Td.style.padding = Padding.Tab.Table;
                this.Td.style.backgroundColor = ColorSetting.TableTitle.BG;
                this.Td.style.color = ColorSetting.TableTitle.Font;
                this.Td.style.border = "1px solid " + ColorSetting.TableTitle.Line;
                this.Td.style.textAlign = "center";
            }

            _table.PerCell.SetDataView = function () {
                var td = this.Td;
                var t_obj = this.TitleObj;
                var all_col = this.AllTitleObj;

                this.Td.style.border = "1px solid " + ColorSetting.TableTitle.Line;
                this.Td.style.textAlign = "center";
                this.Td.style.color = ColorSetting.TableTitle.Content;

                td.style.padding = Padding.Tab.Table;

                if (t_obj.column == "CreateDate" || t_obj.column == "Start") {
                    this.Text = ParseDate(this.Text).format("yyyy/MM/dd HH:mm:ss");
                }

                if (!isNaN(this.Text)) {
                    var _num = Number(this.Text);
                    if (_num < 0)
                        this.Td.style.color = "red";

                    this.Td.style.textAlign = "right";
                    this.Text = numberWithCommas(_num);
                }

            }

            _table.PerFoot.SetDataView = function () {
                this.Td.style.padding = Padding.Tab.Table;
                this.Td.style.textAlign = "center";
                this.Td.style.color = ColorSetting.TableTitle.Content;
                this.Td.style.border = "1px solid " + ColorSetting.TableTitle.Line;

                if (this.Text == "0" || (this.Text != "" && !isNaN(this.Text))) {
                    var _num = Number(this.Text);
                    if (_num < 0)
                        this.Td.style.color = "red";

                    this.Td.style.textAlign = "right";
                    this.Text = numberWithCommas(_num);
                }
            }

            this.Table = _table;
            this.Table.Init();

            this.Table._tfooterObj.style.border = "3px solid " + ColorSetting.TableTitle.Line;
        },
        Switch: function (type) {
            this.HideAll();
            switch (type) {
                case SetType.TIME:
                    ele.Tab.Time.Div.style.display = "";
                    break;
                case SetType.WEEK:
                    ele.Tab.Week.Div.style.display = "";
                    break;
            }
        },
        Hide: function () {
            ele.Tab.Div.style.display = "none";
        },
        GetTable: function () {
            return ele.Tab.Table;

        },
        RefreshTitle: function () {
            this.Table = this._updateTitle(this.Table);
        },
        _updateTitle: function (_table) {
            if (!_table)
                return _table;

            var col = _table.Title,
                title = Language.Column_CheckOut_History_Title;

            if (col.length == title.length) {
                for (var i = 0; i < col.length; i++) {
                    col[i].title = title[i];
                }
                _table.RefreshTitle();
            }

            return _table;
        },

    };

    t.RefreshData = function (Data) {
        var table = ele.Tab.Table;
        if (!table)
            return;

        ele.duration_span.innerText = t.Search.StartTime + " ~ " + (t.Search.EndTime == "" ? getNow() : t.Search.EndTime);

        //t.TargetUID = Data.UID;
        table.Data = Data;
        table.RefreshContent();

        if (Data.Column) {
            table.PerFoot.Data = {
                Record: [Data.Sum],
                Column: Data.Column
            }
            table.RefreshFooter();
        }
    }
    
    t.Init = function () {
        t.Init = function () {

        }

        t.Btn_Confirm.onclick = function () {
            CheckOutHistoryView.Search.StartTime = Control.RightView.Calendar.getSearch(CreateView.Date.Type.DateTime, CheckOutHistoryView.SelectObj, CreateView.Date.Type.Start);
            CheckOutHistoryView.Search.EndTime = Control.RightView.Calendar.getSearch(CreateView.Date.Type.DateTime, CheckOutHistoryView.SelectObj, CreateView.Date.Type.End);

            Control.CheckOutSelect.GetList(CheckOutHistoryView.Search.StartTime, CheckOutHistoryView.Search.EndTime, 2);

            Control.Cashout.GetHistory();
        };
        t.Btn_Confirm.value = Language.btn_Search;
        t.Btn_Confirm.style.marginLeft = Padding.Tab.Div;
        
        t.Div.appendChild(t.searchDiv);
        
        t.Div.appendChild(ele.searchDurationDiv);

        //ele.Menu.Init();
        ele.Tab.Init();

        RightPage.Append(t.Div);
    }

    t.Error = function (msg) {
        ele.ErrorMsg.innerText = msg;
    }

    t.Show = function () {
        //t.SelectObj.Input[CreateView.Date.Type.Start].YearMonthDay.value =
        //    t.SelectObj.Input[CreateView.Date.Type.End].YearMonthDay.value = "";


        t.Error("");
        Right_Title.Set(Language.Menu_CheckoutHistory);

        t.RefreshData([]);

        t.Div.style.display = "inline-block";

        CheckOutHistoryView.Search.StartTime = Control.RightView.Calendar.getSearch(CreateView.Date.Type.DateTime, CheckOutHistoryView.SelectObj, CreateView.Date.Type.Start);
        CheckOutHistoryView.Search.EndTime = Control.RightView.Calendar.getSearch(CreateView.Date.Type.DateTime, CheckOutHistoryView.SelectObj, CreateView.Date.Type.End);

        Control.CheckOutSelect.GetList(CheckOutHistoryView.Search.StartTime, CheckOutHistoryView.Search.EndTime, 2);

        Control.CheckOutSelect.CheckOutTimeSearch(Control.CheckOutSelect.ActiveOption[1]);

        Control.Cashout.GetHistory();
    }
    t.Hide = function () {
        t.TextName.value = "";
        t.Div.style.display = "none";
    }

    t.Refresh = function () {
        //ele.Menu.Refresh();
        Right_Title.Set(Language.Menu_CheckoutHistory);
        ele.Tab.RefreshTitle();

        t.Btn_Confirm.value = Language.btn_Search;
        ele.Span_Name.innerText = Language.txt_searchUser;
    }

    return t;
}();

var EditPasswordView = function () {
    var GameView = {};

    GameView.SetPassword = {
        Obj: {
            Div: null,
            ErrorDiv: null,
            OldPassword: null,
            NewPassword: null,
            RePassword: null,
            JqueryErrorDiv: null
        }
    };

    function checkLogin() {
        if (!GameView.SetPassword.Obj.ErrorDiv)
            return false;

        GameView.SetPassword.Obj.JqueryErrorDiv.empty();
        var msg = "";
        if (ele.input_oldpa.value == "" || ele.input_pwd.value == "" || ele.input_repwd.value == "")
            msg = "所有欄位必填!";

        if (ele.input_pwd.value != ele.input_repwd.value)
            msg = "新密碼與確認新密碼不一致!";

        if (msg.length > 0) {
            GameView.SetPassword.SetError(msg);
            return false;
        } else
            return true;
    }

    var ele = {}, tmp;

    ele.div_pop = buildEle("div", {
        id: "pop_window",
        className: "pop_window",
        style: { "display": "none" }
    });

    ele.div_pop.appendChild(
        ele.bg = buildEle("div", {
            className: "pop_window_bg"
        }));

    ele.div_pop.appendChild(
        ele.div_call = buildEle("div", {
            className: "confirm_call",
            style: {
                "border": "1px solid " + ColorSetting.TableTitle.Line
            }
        }));

    ele.div_call.appendChild(
        ele.div_title = buildEle("div", {
            className: "confirm_title",
            style: {
                "background-color": ColorSetting.TableTitle.BG
            }
        }));
    ele.div_title.appendChild(DeepCopyProps.call(document.createElement("span"), {
        innerHTML: "修改密碼"
    }));

    tmp = buildEle("span", {
        innerText: "X",
        style: {
            "float": "right"
        },
        className: "pointer"
    });
    tmp.onclick = function () {
        GameView.SetPassword.Hide();
    }
    ele.div_title.appendChild(tmp);

    ele.div_call.appendChild(
        ele.div_inner = buildEle("div", {
            style: { padding: "5%" }
        }));

    ele.div_inner.appendChild(
        ele.input_oldpa = buildEle("input", {
            type: "password",
            placeholder: "舊密碼",
            className: "form-control",
            style: { width: "95%" }
        }));

    ele.div_inner.appendChild(
        ele.input_pwd = buildEle("input", {
            type: "password",
            placeholder: "新密碼",
            className: "form-control",
            style: { width: "95%", "margin-top": "5%" }
        }));

    ele.div_inner.appendChild(
        ele.input_repwd = buildEle("input", {
            type: "password",
            placeholder: "確認新密碼",
            className: "form-control",
            style: { width: "95%", "margin-top": "5%" }
        }));

    ele.div_inner.appendChild(
        ele.error = buildEle("div", {
        }));

    ele.div_inner.appendChild(
        ele.div_func = buildEle("div", {
            style: {
                "text-align": "center", "margin-top": "5%"
            }
        }));

    ele.div_func.appendChild(
        ele.btn_login = buildEle("input", {
            type: "button",
            value: "確認",
            className: "confirmbtn btn leftBtn",
            style: {
                "margin":"0"
            },
            onclick: function Login() {
                if (checkLogin())
                    ws.Send("edit.password", {
                        GID: Reporter.GID,
                        oldpwd: ele.input_oldpa.value,
                        newpwd: ele.input_pwd.value
                    });
            }
        }));

    GameView.Init = function () {
        GameView.Init = function () { }
        document.getElementsByTagName("body")[0].appendChild(ele.div_pop);

        GameView.SetPassword.Obj.Div = ele.div_pop;
        GameView.SetPassword.Obj.OldPassword = ele.input_oldpa;
        GameView.SetPassword.Obj.NewPassword = ele.input_pwd;
        GameView.SetPassword.Obj.RePassword = ele.input_repwd;
        GameView.SetPassword.Obj.ErrorDiv = ele.error;
        GameView.SetPassword.Obj.JqueryErrorDiv = $(ele.error);
    }

    GameView.SetPassword.Show = function () {
        GameView.SetPassword.Obj.Div.style.display = "block";
    }

    GameView.SetPassword.Hide = function () {
        if (!GameView.SetPassword.Obj.Div)
            return;

        GameView.SetPassword.Obj.OldPassword.value = "";
        GameView.SetPassword.Obj.NewPassword.value = "";
        GameView.SetPassword.Obj.RePassword.value = "";
        GameView.SetPassword.Obj.JqueryErrorDiv.empty();
        GameView.SetPassword.Obj.Div.style.display = "none";
    }

    GameView.SetPassword.SetError = function (msg) {
        if (!GameView.SetPassword.Obj.JqueryErrorDiv)
            return;

        GameView.SetPassword.Obj.JqueryErrorDiv.empty();
        GameView.SetPassword.Obj.JqueryErrorDiv.append(buildEle("span", {
            innerHTML: msg, style: { color: "red" }
        }));
    }

    GameView.SetPassword.Receive = function (msg) {
        GameView.SetPassword.Obj.OldPassword.value = "";
        GameView.SetPassword.Obj.NewPassword.value = "";
        GameView.SetPassword.Obj.RePassword.value = "";

        GameView.SetPassword.SetError(msg.reason);
    }

    GameView.Refresh = function () {
        ele.btn_login.value = Language.btn_Confirm;
    }

    return GameView;
}();

var PopMsgView = function () {
    var self = {};
    var acc = "", uid = 0;
    var ele = {};
    var win_width = 300;
    var ErrorMsg = "Invalid Number";
    var Id = {}

    var JqObj = {};

    ele = CreateView.Pop.Window(ele, win_width);

    ele.btn_creditSet = CreateView.Pop.btn(Language.btn_Confirm);
    ele.btn_creditSet.onclick = function () {
        self.Hide();
    }

    ele.div_footer.appendChild(ele.btn_creditSet);

    self.Error = function (msg) {
        JqObj.Error.empty();
        JqObj.Error.append(buildEle("span", { innerText: msg, style: { color: ColorSetting.ErrorMsg } }));
    }

    self.Complete = function (msg) {
        if (msg.success) {
            returnFunc();
        } else {
            self.Show(msg.message);
            LoadingPanel.Hide();
        }
    }

    self.Init = function () {
        self.Init = function () {

        }
        JqObj.Error = $(ele.error);
        JqObj.Div = $(ele.div_pop);

        self.Refresh();
        ele.btn_close.onclick = self.Hide;

        $("body").append(ele.div_pop);
    }
    self.Show = function (msg) {
        if (msg)
            self.Error(msg);
        else
            self.Error("");
        JqObj.Div.show();
    }

    self.Hide = function () {
        JqObj.Div.hide();
    }

    self.Refresh = function () {
        ele.title.innerText = Language.Title_Message;
        ele.btn_creditSet.value = Language.btn_Confirm;

    }

    return self;
}();

var AddShareHolderView = function () {
    var self = {};
    var acc = "";
    var ele = {};
    var win_width = 300;
    var ErrorMsg = "Invalid Number";
    var Id = {}
    self.Mode = Modes.ADD;
    self.UID = 0;

    var width = {
        input: "100%"
    }

    var JqObj = {};

    ele = CreateView.Pop.Window(ele, win_width);

    CreateView.Pop.table(ele, Language.Pop_AddShareHolder_Title.length);

    /*Line One*/
    ele.table_content_td[0].appendChild(
        ele.input_name = buildEle("input", {
            style: {
                width: width.input
            }
        })
    );

    /*Line TWO*/
    ele.table_content_td[1].appendChild(
         ele.input_acc = buildEle("input", {
             style: {
                 width: width.input
             }
         })
        );

    /*Line Three*/
    ele.table_content_td[2].appendChild(
        ele.input_pa = buildEle("input", {
            type: "password",
            style: {
                "width": width.input
            }
        }));

    /*Line Four*/
    ele.table_content_td[3].appendChild(
        ele.input_repa = buildEle("input", {
            type: "password",
            style: {
                "width": width.input
            }
        }));

    ele.btn_add = CreateView.Pop.btn(Language.btn_Confirm);
    ele.btn_add.onclick = function () {
        //if (ele.input_add.value != "") {
        //    acc = ele.input_add.value;
        //} else if (ele.input_minus.value != "") {
        //    acc = "-" + ele.input_minus.value;
        //} else {
        //    self.Error(ErrorMsg);
        //    return;
        //}

        if (checkVal())
            Control.AddShareHolder(ele.input_name.value, ele.input_acc.value, ele.input_pa.value);
    }


    ele.div_footer.appendChild(ele.btn_add);

    function checkVal() {
        JqObj.Error.empty();
        var msg = "";
        if (ele.input_name.value == "" || ele.input_acc.value == "" || ele.input_pa.value == "" || ele.input_repa == "")
            msg = "所有欄位必填!";

        if (ele.input_pa.value != ele.input_repa.value)
            msg = "密碼與確認密碼不一致!";

        if (msg.length > 0) {
            self.Error(msg);
            return false;
        } else
            return true;
    }

    self.Error = function (msg) {
        JqObj.Error.empty();
        JqObj.Error.append(buildEle("span", { innerText: msg, style: { color: ColorSetting.ErrorMsg } }));
    }

    self.Complete = function (msg) {
        if (!msg.success) {
            AddShareHolderView.Error(msg.reason);
        } else {
            setLayout(Layouts.STOCKHOLDERMANAGE);
        }
        LoadingPanel.Hide();
    }

    self.Init = function () {
        self.Init = function () {

        }
        JqObj.Error = $(ele.error);
        JqObj.Div = $(ele.div_pop);

        self.Refresh();
        ele.btn_close.onclick = self.Hide;

        $("body").append(ele.div_pop);
    }
    self.Show = function (UserID) {
        //LoadingPanel.Show();
        //uid = UserID;
        //ele.input_minus.value = ele.input_add.value = acc = "";
        JqObj.Error.empty();
        if (self.Mode == Modes.ADD)
            JqObj.Div.show();
        else {
            self.UID = UserID;
            Control.GetShareHolder();
        }

        //Control.Cash.GetCreditInfo(UserID, PopType.SETCREDIT);
    }

    self.Set = function (msg) {
        var record = msg.Record, column = msg.Column

        if (record.length == 0) {
            /*no data*/
            LoadingPanel.Hide();
            self.UID = 0;
            self.Mode = Modes.ADD;
            JqObj.Div.show();
            return;
        }

        ele.input_name.value = record[0][column.indexOf("Name")];
        ele.input_acc.value = record[0][column.indexOf("KeyString")];

        JqObj.Div.show();
        LoadingPanel.Hide();
    }
    self.Hide = function () {
        ele.input_acc.value = ele.input_name.value = ele.input_pa.value = ele.input_repa.value = "";
        self.UID = 0;
        JqObj.Div.hide();
    }

    self.Refresh = function () {
        ele.title.innerText = Language.btn_AddStockholder;
        ele.btn_add.value = Language.btn_Confirm;

        for (var i = 0; i < Language.Pop_AddShareHolder_Title.length; i++) {
            ele.table_title_span[i].innerText = Language.Pop_AddShareHolder_Title[i];
        }
    }

    return self;
}();

var AutoCheckView = function () {
    var Table_Column = [        
        "name",
        "credit",
        "creditremain",
        "ud",
        "cash_sum",
        "credit_return",
        "totalbet",
        "totalwin",
        "profit",
        "status"
    ]

    var Padding = {
        Tab: {
            Div: "10px",
            Table: "5px"
        }
    }

    var t = {}, ele = {};
    t.errSpan = null;

    t.Data = {};

    t.Search = {
        StartTime: "",
        EndTime: "",
        PageIndex: [1, 1, 1, 1],
        PageCount: [],
        Id: [1, 1, 1, 1]
    }

    t.Div = WebTool.CreateElement("div", {
        style: {
            "display": "none"
        }
    });

    var select_key_id = "select_";

    var CREATE = {
        DIV: function () {
            return WebTool.CreateElement("div");
        },
        TABLE: function () {
            return WebTool.CreateElement("table");
        },
        TR: function () {
            return WebTool.CreateElement("tr");
        },
        TD_MENU: function () {
            return WebTool.CreateElement("td", {
                style: {
                    "padding": "5px"
                }
            });
        },
        TableTitleObj: function () {
            var result = [];
            var col = Table_Column,
                title = Language.Column_AutoCheck_Title;

            if (col.length != title.length)
                return result;

            for (var i = 0; i < col.length; i++) {
                result[i] = {
                    column: col[i],
                    title: title[i]
                }
            }

            return result;
        },
        Btn: function () {
            return WebTool.CreateElement("input", { type: "button" });
        }
    }

    ele.searchDurationDiv = buildEle("div", {
        style: {
            "padding-left": Padding.Tab.Table,
            "padding-bottom": Padding.Tab.Table
        }
    });
    ele.duration_span = buildEle("span");
    ele.searchDurationDiv.appendChild(ele.duration_span);

    t.Btn_Confirm = CREATE.Btn();

    t.TextName = buildEle("input");
    ele.Span_Name = buildEle("span", {
        innerText: Language.txt_searchUser,
        className: "span_right"
    });

    //t.searchDiv = buildEle("div", { style: { "margin-top": "10px" } });
    t.SelectObj = CreateView.Date.Calendar.Control(CreateView.Date.Type.DateTime);
    t.searchDiv = t.SelectObj.Container;

    t.searchDiv.insertBefore(t.TextName, t.searchDiv.firstChild);
    t.searchDiv.insertBefore(ele.Span_Name, t.searchDiv.firstChild);
    t.searchDiv.style.paddingLeft = Padding.Tab.Table;

    t.Select_Control = {};

    ele.Tab = {
        Div: CREATE.DIV(),
        Table: null,
        GetCId: function (col, key_col, record) {
            var result = -1, k;

            for (var i = 0; i < col.length; i++) {
                if (!("column" in col[i]) || key_col != col[i].column)
                    continue;

                result = record[i];
                break;
            }

            return result;
        },
        GetSelect: function (col) {
            if (t.Select_Control[col])
                return t.Select_Control[col];

            var _select = WebTool.CreateElement("select", {
                id: select_key_id + col
            }), _op = WebTool.CreateElement("option");
            _op.value = _op.textContent = "-";
            _select.appendChild(_op);

            for (var i = 0; i < 24; i++) {
                _op = WebTool.CreateElement("option");
                _op.value = _op.textContent = padLeft(2, i.toString(), "0") + ":00";

                _select.appendChild(_op);
            }

            t.Select_Control[col] = _select;
            return t.Select_Control[col];
        },
        Init: function () {
            this.Init = function () { }

            this.Div.style.padding = Padding.Tab.Div;
            t.Div.appendChild(this.Div);
            var _table = new WebTool.Table(this.Div);
            _table.Title = CREATE.TableTitleObj();
            _table.PerTitle.SetDataView = function () {
                this.Td.style.padding = Padding.Tab.Table;
                this.Td.style.backgroundColor = ColorSetting.TableTitle.BG;
                this.Td.style.color = ColorSetting.TableTitle.Font;
                this.Td.style.textAlign = "center";
            }

            _table.PerCell.SetDataView = function () {
                var td = this.Td;
                var t_obj = this.TitleObj;
                var all_col = this.AllTitleObj;

                this.Td.style.textAlign = "center";

                td.style.padding = Padding.Tab.Table;

                if (t_obj.column == "CreateDate" || t_obj.column == "Start") {
                    this.Text = ParseDate(this.Text).format("yyyy/MM/dd HH:mm:ss");
                }

                if (!isNaN(this.Text)) {
                    var _num = Number(this.Text);
                    if (_num < 0)
                        this.Td.style.color = "red";

                    this.Td.style.textAlign = "right";
                    this.Text = numberWithCommas(_num);
                }

            }
            
            this.Table = _table;
            this.Table.Init();
        },        
        Hide: function () {
            ele.Tab.Div.style.display = "none";
        },
        GetTable: function () {
            return ele.Tab.Table;

        },
        RefreshTitle: function () {
            this.Table = this._updateTitle(this.Table);
        },
        _updateTitle: function (_table) {
            if (!_table)
                return _table;

            var col = _table.Title,
                title = Language.Column_AutoCheck_Title;

            if (col.length == title.length) {
                for (var i = 0; i < col.length; i++) {
                    col[i].title = title[i];
                }
                _table.RefreshTitle();
            }

            return _table;
        },

    };

    t.RefreshData = function (Data) {
        var table = ele.Tab.Table;
        if (!table)
            return;

        ele.duration_span.innerText = t.Search.StartTime + " ~ " + (t.Search.EndTime == "" ? getNow() : t.Search.EndTime);

        //t.TargetUID = Data.UID;
        table.Data = Data;
        table.RefreshContent();
    }

    /*Error Msg*/
    ele.ErrorMsg = WebTool.CreateElement("span", {
        style: {
            "color": ColorSetting.ErrorMsg
        }
    });

    t.Init = function () {
        t.Init = function () {

        }

        t.Btn_Confirm.onclick = function () {
            AutoCheckView.Search.StartTime = Control.RightView.Calendar.getSearch(CreateView.Date.Type.DateTime, AutoCheckView.SelectObj, CreateView.Date.Type.Start);
            AutoCheckView.Search.EndTime = Control.RightView.Calendar.getSearch(CreateView.Date.Type.DateTime, AutoCheckView.SelectObj, CreateView.Date.Type.End);

            //Control.CheckOutSelect.GetList(AutoCheckView.Search.StartTime, AutoCheckView.Search.EndTime, 2);

            //Control.Cashout.GetHistory();
        };
        t.Btn_Confirm.value = Language.btn_Search;
        t.Btn_Confirm.style.marginLeft = Padding.Tab.Div;


        t.Div.appendChild(t.searchDiv);

        t.searchDiv.appendChild(t.Btn_Confirm);
        t.searchDiv.appendChild(ele.ErrorMsg);

        t.Div.appendChild(ele.searchDurationDiv);

        //ele.Menu.Init();
        ele.Tab.Init();

        RightPage.Append(t.Div);
    }

    t.Error = function (msg) {
        ele.ErrorMsg.innerText = msg;
    }

    t.Show = function () {
        //t.SelectObj.Input[CreateView.Date.Type.Start].YearMonthDay.value =
        //    t.SelectObj.Input[CreateView.Date.Type.End].YearMonthDay.value = "";


        t.Error("");
        Right_Title.Set("");

        t.RefreshData([]);

        t.Div.style.display = "inline-block";

        AutoCheckView.Search.StartTime = Control.RightView.Calendar.getSearch(CreateView.Date.Type.DateTime, AutoCheckView.SelectObj, CreateView.Date.Type.Start);
        AutoCheckView.Search.EndTime = Control.RightView.Calendar.getSearch(CreateView.Date.Type.DateTime, AutoCheckView.SelectObj, CreateView.Date.Type.End);

        Control.CheckOutSelect.GetList(AutoCheckView.Search.StartTime, AutoCheckView.Search.EndTime, 3);

    }
    t.Hide = function () {
        t.TextName.value = "";
        t.Div.style.display = "none";
    }

    t.Refresh = function () {
        //ele.Menu.Refresh();
        Right_Title.Set(Language.Menu_CheckoutHistory);
        ele.Tab.RefreshTitle();

        t.Btn_Confirm.value = Language.btn_Search;
        ele.Span_Name.innerText = Language.txt_searchUser;
    }

    return t;
}();

var ABCView = function () {
    var Table_Column = [
        "Value",
        "Winner",
        "Pokers",
        "GID",
        "btn"
    ]

    var Padding = {
        Tab: {
            Div: "10px",
            Table: "5px"
        }
    }

    var ABCStatus = Tools.Enumify(
        "None", "Enabled", "Recording", "Updating");

    var t = {}, ele = {};
    t.errSpan = null;

    t.Data = {};

    t.Search = {
        StartTime: "",
        EndTime: "",
        PageIndex: [1, 1, 1, 1],
        PageCount: [],
        Id: [1, 1, 1, 1]
    }

    t.Div = WebTool.CreateElement("div", {
        style: {
            "display": "none"
        }
    });

    var select_key_id = "select_";

    var CREATE = {
        DIV: function () {
            return WebTool.CreateElement("div");
        },
        TABLE: function () {
            return WebTool.CreateElement("table");
        },
        TR: function () {
            return WebTool.CreateElement("tr");
        },
        TD_MENU: function () {
            return WebTool.CreateElement("td", {
                style: {
                    "padding": "5px"
                }
            });
        },
        TableTitleObj: function () {
            var result = [];
            var col = Table_Column,
                title = Language.Column_ABC_Title;

            if (col.length != title.length)
                return result;

            for (var i = 0; i < col.length; i++) {
                result[i] = {
                    column: col[i],
                    title: title[i]
                }
            }

            return result;
        },
        Btn: function () {
            return WebTool.CreateElement("input", { type: "button" });
        }
    }

    ele.Span_Name = buildEle("span", {
        innerText: Language.txt_searchUser,
        className: "span_right"
    });

    ele.Tab = {
        Div: CREATE.DIV(),
        Table: null,
        GetCId: function (col, key_col, record) {
            var result = -1, k;

            for (var i = 0; i < col.length; i++) {
                if (!("column" in col[i]) || key_col != col[i].column)
                    continue;

                result = record[i];
                break;
            }

            return result;
        },
        GetSelect: function (col) {
            if (t.Select_Control[col])
                return t.Select_Control[col];

            var _select = WebTool.CreateElement("select", {
                id: select_key_id + col
            }), _op = WebTool.CreateElement("option");
            _op.value = _op.textContent = "-";
            _select.appendChild(_op);

            for (var i = 0; i < 24; i++) {
                _op = WebTool.CreateElement("option");
                _op.value = _op.textContent = padLeft(2, i.toString(), "0") + ":00";

                _select.appendChild(_op);
            }

            t.Select_Control[col] = _select;
            return t.Select_Control[col];
        },
        Init: function () {
            this.Init = function () { }

            this.Div.style.padding = Padding.Tab.Div;
            t.Div.appendChild(this.Div);
            var _table = new WebTool.Table(this.Div);
            _table.Title = CREATE.TableTitleObj();
            _table.PerTitle.SetDataView = function () {
                this.Td.style.padding = Padding.Tab.Table;
                this.Td.style.backgroundColor = ColorSetting.TableTitle.BG;
                this.Td.style.color = ColorSetting.TableTitle.Font;
                this.Td.style.textAlign = "center";
            }

            _table.PerCell.SetDataView = function () {
                var td = this.Td;
                var t_obj = this.TitleObj;
                var all_col = this.AllTitleObj;

                this.Td.style.textAlign = "center";

                td.style.padding = Padding.Tab.Table;

                if (t_obj.column == "CreateDate" || t_obj.column == "Start") {
                    this.Text = ParseDate(this.Text).format("yyyy/MM/dd HH:mm:ss");
                }
                
                if (t_obj.column == "btn") {
                    this.Text = "";
                    var _btn = CREATE.Btn();
                    _btn.value = Language.txt_Enabled;
                    _btn.Column = Table_Column.slice(0, Table_Column.length - 1);
                    _btn.Record = this.RecordArr;
                    _btn.onclick = function () {
                        sendToGame(this);
                    }
                    this.Td.appendChild(_btn);

                    if (this.RecordArr[Table_Column.indexOf("Value")] != "0") {
                        _btn = CREATE.Btn();
                        _btn.value = Language.btn_Delete;
                        _btn.Column = Table_Column.slice(0, Table_Column.length - 1);
                        _btn.Record = this.RecordArr;
                        _btn.onclick = function () {
                            DeleteABC(this);
                        }
                        this.Td.appendChild(_btn);
                    }
                }
            }

            this.Table = _table;
            this.Table.Init();
        },
        Hide: function () {
            ele.Tab.Div.style.display = "none";
        },
        GetTable: function () {
            return ele.Tab.Table;

        },
        RefreshTitle: function () {
            this.Table = this._updateTitle(this.Table);
        },
        _updateTitle: function (_table) {
            if (!_table)
                return _table;

            var col = _table.Title,
                title = Language.Column_ABC_Title;

            if (col.length == title.length) {
                for (var i = 0; i < col.length; i++) {
                    col[i].title = title[i];
                }
                _table.RefreshTitle();
            }

            return _table;
        },

    };

    ele.StatusList = {
        Div: CREATE.DIV(),
        btn: [],
        Status:ABCStatus.None,
        Title: WebTool.CreateElement("span", {
            innerText:Language.txt_Status
        }),
        Label: WebTool.CreateElement("span", {
            innerText:Language.ABC_Status[ABCStatus.None]
        }),
        Init: function () {
            this.Init = function () { }
            this.Div.style.paddingLeft = Padding.Tab.Table;

            var _btn;
            /*Status Label*/
            this.Div.appendChild(this.Title);
            this.Div.appendChild(WebTool.CreateElement("span", {
                innerText:":"
            }));
            this.Div.appendChild(this.Label);

            /*btn*/
            for (var i = 0; i < Language.ABC_Status.length; i++) {
                _btn = CREATE.Btn();
                _btn.statusindex = i;
                _btn.value = Language.ABC_Status[i];
                _btn.onclick = function () {
                    sendStatus(this.statusindex);
                }
                this.btn.push(_btn);
                if (i == 1) 
                    /*啟用中skip*/
                    continue;                
                this.Div.appendChild(_btn);
            }

            t.Div.appendChild(this.Div);
        },
        Refresh: function () {
            this.Title.innerText = Language.txt_Status;
            this.Label.innerText = Language.ABC_Status[ele.StatusList.Status];

            for (var i = 0; i < this.btn.length; i++) {
                this.btn[i].value = Language.ABC_Status[i];
            }
        },
        Update: function (status) {
            ele.StatusList.Status = status;
            ele.StatusList.Label.innerText = Language.ABC_Status[ele.StatusList.Status];
        }
    }

    t.RefreshData = function (Data) {        
        var table = ele.Tab.Table;
        if (!table)
            return;
        
        table.Data = Data;
        table.RefreshContent();
    }

    /*Error Msg*/
    ele.ErrorMsg = WebTool.CreateElement("span", {
        style: {
            "color": ColorSetting.ErrorMsg
        }
    });

    t.Init = function () {
        t.Init = function () {

        }

        //t.searchDiv.appendChild(ele.ErrorMsg);

        //ele.Menu.Init();
        ele.StatusList.Init();
        ele.Tab.Init();        

        RightPage.Append(t.Div);
    }

    t.Error = function (msg) {
        ele.ErrorMsg.innerText = msg;
    }

    t.Show = function () {
        //t.SelectObj.Input[CreateView.Date.Type.Start].YearMonthDay.value =
        //    t.SelectObj.Input[CreateView.Date.Type.End].YearMonthDay.value = "";
        t.RequestStatus();

        t.Error("");
        Right_Title.Set(Language.Menu_ABC);

        t.RefreshData([]);

        requestData();

        t.Div.style.display = "inline-block";

    }
    t.Hide = function () {
        //t.TextName.value = "";
        t.RefreshData([]);
        t.Div.style.display = "none";        
    }

    t.Refresh = function () {
        //ele.Menu.Refresh();
        Right_Title.Set(Language.Menu_ABC);
        ele.Tab.RefreshTitle();
        ele.StatusList.Refresh();
    }

    function sendStatus(status) {
        ws.Send("abc.setstatus", {
            GID:Reporter.GID,
            status: status
        });
    }

    function requestData() {
        ws.Send("abc.requestdata", {
            GID: Reporter.GID
        });
    }
    function sendToGame(obj) {
        ws.Send("abc.sendtogame", {
            GID: Reporter.GID,
            Column:obj.Column,
            Record:obj.Record
        });
    }
    function DeleteABC(obj) {
        ws.Send("abc.delete", {
            GID: Reporter.GID,
            Column: obj.Column,
            Record: obj.Record
        });
    }

    t.SetStatus = function (obj) {
        if (!obj.success)
            return;
        ele.StatusList.Update(obj.status);
    }
    t.RequestStatus = function () {
        ws.Send("abc.getstatus", {
            GID:Reporter.GID
        });
    }

    return t;
}();