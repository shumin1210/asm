﻿DataSheet.Game_21 = {
    _Record: {
        gid: {
            col: "機台編號",
            Value: "",
            Center: true,
            Set: function (val) {
                if (val == null)
                    return;
                this.Value = val;
            }
        },
        StartTime: {
            col: "進入機台時間",
            Value: "",
            Set: function (val) {
                if (val) {
                    val = val.Year + '/' + val.Month + '/' + val.Day + " " + val.Hour + ":" + val.Minute;
                    this.Value = val;
                } else
                    this.Value = "";

            }
        },
        EndTime: {
            col: "離開機台時間",
            Value: "",
            Set: function (val) {
                if (val) {
                    val = val.Year + '/' + val.Month + '/' + val.Day + " " + val.Hour + ":" + val.Minute;
                    this.Value = val;
                } else
                    this.Value = "";

            }
        },
        TotalCost: {
            col: "總押分",
            Value: "",
            Center: true,
            Set: function (val) {
                if (val == null)
                    return;
                this.Value = val;
            }
        },
        TotalGain: {
            col: "總贏分",
            Value: "",
            Center: true,
            Set: function (val) {
                if (val == null)
                    return;
                this.Value = val;
            }
        },
    },
    //_col: ["機台編號", "進入機台時間", "離開機台時間", "總押分", "總贏分"],
    _col: ["gid", "StartTime", "EndTime", "TotalCost", "TotalGain"],
    _width: [150, 200, 200, 200, 200],
    Get: {
       
    }
}