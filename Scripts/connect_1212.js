﻿/// <reference path="data.js" />
/// <reference path="../../jquery/jquery-1.12.3.min.js" />
/// <reference path="../../jquery/Tools.js" />


ws.AddOnMessageHandler("login", function (msg) {    
    if (msg.Games.length > 0) {
        Login.Hide();
        LoadingPanel.Show();

        Reporter.GID = msg.Games[0];
        Reporter.ID = msg.ID;
        Reporter.Name = msg.Name;
        Reporter.Token = msg.Token;
        Reporter.Role = msg.Role;
        Reporter.Permission = msg.Permission;

        ws.Send("enter_report", { GID: Reporter.GID });

        //setLayout(Layouts.MAINPAGE);
    }
    
});

ws.AddOnMessageHandler("login.error", function (msg) {
    $("#" + Login.ErrorMsgId).empty();
    $("#" + Login.ErrorMsgId).append(buildEle("span", { innerHTML: msg.Msg, style: { color: "red" } }));
});

ws.AddOnMessageHandler("error", function (msg) {
    setLayout(Layouts.LOGIN);
});

ws.AddOnMessageHandler("gamelist", function (msg) {    
    LeftView.Data = msg;
    LeftView.Set();

    Control.LeftView.Confirm(true);
    
});

ws.AddOnMessageHandler("request.userdata", function (msg) {
    AddEditPack.Set(msg);
});

ws.AddOnMessageHandler("request.language", function (msg) {
    Language.Set(msg.Record);

    ReportTitle.Refresh();
    //MainPage.Refresh();

    AddEditPack.GetInputValue();
    RightView.Refresh();

    var isEdit = AddEditPack.Mode != Modes.VIEW;
    var tmpMode = AddEditPack.Mode;

    setLayout(Layout);

    if (isEdit) {
        AddEditPack.SetMode(tmpMode);
        RightView.Show.EditPanel(null, AddEditPack.Mode, true);
        AddEditPack.SetInputValue();
    }

    AddEditPack.tmp_input = [];

    LoadingPanel.Hide();
});

ws.AddOnMessageHandler("edit.error", function (msg) {
    AddEditPack.SetError(msg.Msg);
    LoadingPanel.Hide();
});
ws.AddOnMessageHandler("edit.success", function (msg) {
    AddEditPack.SetError("");
    returnFunc();
    
    RightView.Show.Data();
});
ws.AddOnMessageHandler("delete.success", function (msg) {
    if (msg.success) {
        returnFunc();
    }
});

ws.onopen = function () {
    Init();

    setLayout(Layouts.LOGIN);
}

ws.StartConnect();

function returnFunc() {
    switch (Layout) {
        case Layouts.LAYER1:
        default:
            RightView.Data.MemberList.InsertFirst = true;
            Control.LeftView.Confirm(false);
            break;
        case Layouts.LAYER2:
            Control.RightView.Layer1.Row({ currentTarget: { UserID: RightView.Data.CompanyList.ReceivePlayerID } });
            break;
    }
}