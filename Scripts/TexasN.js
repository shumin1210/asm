﻿DataSheet.Game_11 = {
    _Record: {
        createdate: {
            col: "結算時間",
            Value: "",
            Set: function (val) {
                if (val) {
                    val = val.Year + '/' + val.Month + '/' + val.Day + " " + val.Hour + ":" + val.Minute;
                    this.Value = val;
                } else
                    this.Value = "";

            }
        },
        UNO: {
            col: "回合",
            Value: "",
            Center: true,
            Set: function (val) {
                if (val == null)
                    return;

                this.Value = val;
            }
        },
        uspoker1: {
            col: "玩家手牌1",
            Value: "",
            Center: true,
            Set: function (val) {
                if (val == null)
                    return;
                this.Value = DataSheet.Game_11.Get.PokerSuitAndPoint(val);
            }
        },
        uspoker2: {
            col: "玩家手牌2",
            Value: "",
            Center: true,
            Set: function (val) {
                if (val == null)
                    return;

                this.Value = DataSheet.Game_11.Get.PokerSuitAndPoint(val);
            }
        },        
        winmoney: {
            col: "總贏分",
            Value: "",
            Center: true,
            Set: function (val) {
                if (val == null)
                    return;

                this.Value = val;
            }
        },
        allmoney: {
            col: "總押注",
            Value: "",
            Center: true,
            Set: function (val) {
                if (val == null)
                    return;

                this.Value = val;
            }
        },
        publicpoker1: {
            col: "公牌1",
            Value: "",
            Center: true,
            Set: function (val) {
                if (val == null)
                    return;

                this.Value = DataSheet.Game_11.Get.PokerSuitAndPoint(val);
            }
        },
        publicpoker2: {
            col: "公牌2",
            Value: "",
            Center: true,
            Set: function (val) {
                if (val == null)
                    return;

                this.Value = DataSheet.Game_11.Get.PokerSuitAndPoint(val);
            }
        },
        publicpoker3: {
            col: "公牌3",
            Value: "",
            Center: true,
            Set: function (val) {
                if (val == null)
                    return;

                this.Value = DataSheet.Game_11.Get.PokerSuitAndPoint(val);
            }
        }
    },
    _col: ["UNO", "createdate", "uspoker1", "uspoker2", "winmoney", "allmoney", "publicpoker1", "publicpoker2", "publicpoker3"],
    _width: [100, 250, 200, 200, 220, 220, 200, 200, 200],
    Get: {
        Poker: {
            //花色index轉換
            Suit: function (index) {
                index = typeof index == "string" ? Number(index) : index;

                return (Math.floor((index - 1) / 13));
            },
            //點數index轉換
            Point: function (index) {
                index = typeof index == "string" ? Number(index) : index;

                var result = index - Math.floor(index / 13) * 13;

                if (result == 0) {
                    result = 13;
                }

                return result;
            }
        },
        PokerSuitAndPoint: function (val) {
            var _poker = DataSheet.Game_11.Get.Poker.Suit(val),
                _point = DataSheet.Game_11.Get.Poker.Point(val);

            var _r = DataSheet.Game_11.eNum(_poker, _point);

            return _r;
        }
    },
    eNum: function (Suit, Poker) {
        var _Poker = ["A", "2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K"],
            _Suit = ["♠", "♥", "♣", "♦"];

        return _Suit[Suit] + _Poker[Poker - 1];
    }
}