﻿/*! modernizr 3.1.0 (Custom Build) | MIT */
!function (e, n, a) { function o(e, n) { return typeof e === n } function s() { var e, n, a, s, t, c, r; for (var u in l) if (l.hasOwnProperty(u)) { if (e = [], n = l[u], n.name && (e.push(n.name.toLowerCase()), n.options && n.options.aliases && n.options.aliases.length)) for (a = 0; a < n.options.aliases.length; a++) e.push(n.options.aliases[a].toLowerCase()); for (s = o(n.fn, "function") ? n.fn() : n.fn, t = 0; t < e.length; t++) c = e[t], r = c.split("."), 1 === r.length ? Modernizr[r[0]] = s : (!Modernizr[r[0]] || Modernizr[r[0]] instanceof Boolean || (Modernizr[r[0]] = new Boolean(Modernizr[r[0]])), Modernizr[r[0]][r[1]] = s), i.push((s ? "" : "no-") + r.join("-")) } } function t(e) { var n = u.className, a = Modernizr._config.classPrefix || ""; if (f && (n = n.baseVal), Modernizr._config.enableJSClass) { var o = new RegExp("(^|\\s)" + a + "no-js(\\s|$)"); n = n.replace(o, "$1" + a + "js$2") } Modernizr._config.enableClasses && (n += " " + a + e.join(" " + a), f ? u.className.baseVal = n : u.className = n) } function c() { return "function" != typeof n.createElement ? n.createElement(arguments[0]) : f ? n.createElementNS.call(n, "http://www.w3.org/2000/svg", arguments[0]) : n.createElement.apply(n, arguments) } var i = [], l = [], r = { _version: "3.1.0", _config: { classPrefix: "", enableClasses: !0, enableJSClass: !0, usePrefixes: !0 }, _q: [], on: function (e, n) { var a = this; setTimeout(function () { n(a[e]) }, 0) }, addTest: function (e, n, a) { l.push({ name: e, fn: n, options: a }) }, addAsyncTest: function (e) { l.push({ name: null, fn: e }) } }, Modernizr = function () { }; Modernizr.prototype = r, Modernizr = new Modernizr, Modernizr.addTest("websockets", "WebSocket" in e && 2 === e.WebSocket.CLOSING); var u = n.documentElement, f = "svg" === u.nodeName.toLowerCase(); Modernizr.addTest("audio", function () { var e = c("audio"), n = !1; try { (n = !!e.canPlayType) && (n = new Boolean(n), n.ogg = e.canPlayType('audio/ogg; codecs="vorbis"').replace(/^no$/, ""), n.mp3 = e.canPlayType("audio/mpeg;").replace(/^no$/, ""), n.opus = e.canPlayType('audio/ogg; codecs="opus"').replace(/^no$/, ""), n.wav = e.canPlayType('audio/wav; codecs="1"').replace(/^no$/, ""), n.m4a = (e.canPlayType("audio/x-m4a;") || e.canPlayType("audio/aac;")).replace(/^no$/, "")) } catch (a) { } return n }), Modernizr.addTest("canvas", function () { var e = c("canvas"); return !(!e.getContext || !e.getContext("2d")) }), s(), t(i), delete r.addTest, delete r.addAsyncTest; for (var p = 0; p < Modernizr._q.length; p++) Modernizr._q[p](); e.Modernizr = Modernizr }(window, document);
//End
var Tools = window.Tools || {};

/*------------------通用工具------------------*/
/*tool 滑鼠在canvas上的座標*/
function getMousePos(target, evt) {
    var rect = target.getBoundingClientRect() || 0;
    return {
        x: evt.clientX - rect.left,
        y: evt.clientY - rect.top
    };
}
/*tool 設定滑鼠游標圖示(0:連結 1:箭頭)*/
function SetCursor(type) {
    switch (type) {
        case 0: $('body').css({ 'cursor': 'pointer' }); break;
        case 1: $('body').css({ 'cursor': 'default' }); break;
    }
}
function Round(num) { return ~~(0.5 + num); }
//返回座標物件(x,y)，將原坐標系轉換成現在的座標
function _Point(ox, oy, ratio) {
    var x, y;
    switch (arguments.length) {
        case 1:
            x = arguments[0].x; y = arguments[0].y; r = zoom; break;
        case 2:
            if (typeof (arguments[0]) == 'object') {
                x = arguments[0].x; y = arguments[0].y; r = arguments[1];
            } else {
                x = ox; y = oy; r = zoom;
            }
            break;
        case 3:
            x = ox; y = oy; ratio = ratio;
            break;
    }
    return Point(x * r, y * r);
}
//返回(x,y)座標物件
function Point(x, y) {
    var point = { x: 0, y: 0 };
    switch (arguments.length) {
        case 0:
            break;
        case 1:
            if (arguments[0]) {
                point.x = arguments[0].x, point.y = arguments[0].y;
            }
            break;
        case 2:
            point.x = x; point.y = y;
            break;
    }
    point.Move = function (w, h) {
        if (arguments.length == 1) {
            return new Point(this.x + arguments[0].w, this.y + arguments[0].h);
        } else {
            return new Point(this.x + w, this.y + h);
        }
    }
    return point;
}
//返回尺寸物件(w,h)，並自動從原坐標系轉換成現在的座標
function _Size(w, h) {
    if (arguments.length == 1) {
        return Size(arguments[0].w, arguments[0].h);
    } else {
        return Size(w * zoom, h * zoom);
    }

}
//返回(w,h)尺寸物件
function Size(w, h) {
    var size = { w: 0, h: 0 };
    switch (arguments.length) {
        case 0: return size;
        case 2: return { w: w, h: h };
    }
}

function Resize(Size, ratio) {
    var nw = Size.w * ratio;
    var nh = Size.h * ratio;
    return { w: nw, h: nh };
};

function MidPoint(rect) {
    switch (arguments.length) {
        case 1://rect
            return { x: rect.x + rect.w / 2, y: rect.y + rect.h / 2 };
        case 2:
            if ('w' in arguments[1]) {//point+size
                return { x: rect.x + arguments[1].w / 2, y: rect.y + arguments[1].h / 2 };
            }
            else {//point+point
                return { x: (rect.x + arguments[1].x) / 2, y: (rect.y + arguments[1].y) / 2 };
            }
        case 4:
            return { x: arguments[0] + arguments[2] / 2, y: arguments[1] + arguments[3] / 2 };
            break;
    }
}
//Rect 從中心點反推範圍rect
function _MidPoint(midpoint, size) {
    var x = 0, y = 0, w = 0, h = 0;
    switch (arguments.length) {
        case 2://midpoint+size
            x = midpoint.x;
            y = midpoint.y;
            w = size.w;
            h = size.h;
            break;
        case 3:
            if ("x" in arguments[0]) {
                x = midpoint.x;
                y = midpoint.y;
                w = arguments[1];
                h = arguments[2];
            } else {
                x = arguments[0];
                y = arguments[1];
                w = arguments[2].w;
                h = arguments[2].h;
            }
            break;
        case 4:
            x = arguments[0]; y = arguments[1];
            w = arguments[2]; h = arguments[3];
            break;
    }
    return { x: x - w / 2, y: y - h / 2, w: w, h: h };
}


//返回長方形物件(x,y,w,h)，並自動轉換座標
function _Rect(x, y, w, h) {
    return Rect(x * zoom, y * zoom, w * zoom, h * zoom);
}

//返回長方形物件Rect(x,y,w,h)
function Rect(x, y, w, h) {
    var rect = { x: 0, y: 0, w: 0, h: 0, Type: "rect" };
    switch (arguments.length) {
        case 0: return rect;
        case 1://Rect
            rect.x = arguments[0].x;
            rect.y = arguments[0].y;
            rect.w = arguments[0].w;
            rect.h = arguments[0].h;
            break;
        case 2://point+size
            rect.x = arguments[0].x;
            rect.y = arguments[0].y;
            rect.w = arguments[1].w;
            rect.h = arguments[1].h;
            break;
        case 3:
            if (typeof (arguments[0]) == 'object') {
                rect.x = arguments[0].x;
                rect.y = arguments[0].y;
                rect.w = arguments[1];
                rect.h = arguments[2];
            } else {
                rect.x = arguments[0];
                rect.y = arguments[1];
                rect.w = arguments[2].w;
                rect.h = arguments[2].h;
            }
            break;
        case 4:
            rect.x = x; rect.y = y; rect.w = w; rect.h = h;
            break;
    }
    this.x = rect.x;
    this.y = rect.y;
    this.w = rect.w;
    this.h = rect.h;
}
Rect.prototype = {
    Type: "Rect", constructor: Rect,
    CenterTop : function () { return new Point(this.x + this.w / 2, this.y); },
     CenterBottom : function () { return new Point(this.x + this.w / 2, this.y + this.h); },
     LeftMiddle : function () { return new Point(this.x, this.y + this.h / 2); },
     RightMiddle : function () { return new Point(this.x + this.w, this.y + this.h / 2); },
     LeftTop : function () { return new Point(this.x, this.y); },
     LeftBottom : function () { return new Point(this.x, this.y + this.h); },
     RightTop : function () { return new Point(this.x + this.w, this.y); },
     RightBottom : function () { return new Point(this.x + this.w, this.y + this.h); },
     MidPoint : function () { return new Point(this.x + this.w / 2, this.y + this.h / 2); },
    Size: function () { return new Size(this); },
    Css: function () { return new Rect_Css(this); },
    ZoomedCss: function (zm) {
        var z = window.zoom || zm || 1;
        return new Rect_Css(this.x * z, this.y * z, this.w * z, this.h * z);
    },
    CssLocation: function () { return { 'left': x + 'px', 'top': y + 'px' }; },
    CssSzie: function () { return { 'width': w + 'px', 'height': h + 'px' }; },
    CentralResize: function (z) {
        var nx = this.MidPoint().x - z * this.w / 2;
        var ny = this.MidPoint().y - z * this.h / 2;
        return new Rect(nx, ny, this.w * z, this.h * z);
    },
    Move: function (dx, dy) { return new Rect(this.x + dx, this.y + dy, this.w, this.h); },
    Resize: function (dw, dh) { return new Rect(this.x, this.y, this.w + dw, this.h + dh); },
    Transfer: function (px, py, pw, ph) { return new Rect(this.x * px, this.y * py, this.pw * pw, this.h * ph); },
    ToRectangle: function () { return { x: this.x, y: this.y, width: this.w, height: this.h }; },
}

function SlideBetweenRects(rect1, rect2, percentage) {
    var r = percentage > 1 ? 1 : percentage < 0 ? 0 : percentage;
    if (r == 0) { return new Rect(rect1); }
    function calR(a, b, r) { return a + (b - a) * r; }
    return new Rect(calR(rect1.x, rect2.x, r), calR(rect1.y, rect2.y, r), calR(rect1.w, rect2.w, r), calR(rect1.h, rect2.h, r));
}

/*tool 判斷座標是否在Rect{x,y,w,h}中*/
var InRect = function (zoom, p0, rect) {
    var i = (typeof (zoom) == "number") ? 1 : 0;
    var z = i ? zoom : 1;

    var x = 0, y = 0, w = 0, h = 0;
    switch (arguments.length) {
        case i + 2:
            x = arguments[i + 1].x * z; y = arguments[i + 1].y * z; w = arguments[i + 1].w * z; h = arguments[i + 1].h * z;
            break;
        case i + 5:
            x = arguments[i + 1] * z; y = arguments[i + 2] * z; w = arguments[i + 3] * z; h = arguments[i + 4] * z;
            break;
    }
    if (arguments[i].x > x && arguments[i].x <= x + w && arguments[i].y > y && arguments[i].y < y + h) {
        return true;
    } else {
        return false;
    }
}
/*tool 依據size決定x,y方向變量，移動Rect*/
var ShiftRect = function (rect, size, multiply) {

    if (typeof (arguments[1]) != "number") {
        var m = multiply || 1;
        return Rect(rect.x + size.w * m, rect.y + size.h * m, rect);
    } else {
        var m = arguments[4] || 1;
        return Rect(rect.x + arguments[1] * m, rect.y + arguments[2] * m, rect);
    }
}

/*bool 檢測是否在圓形範圍內
[zoom],p,rect
[zoom],p,midp,radius
*/
var InCircle = function (zoom, p, CircleRect) {
    var i = (typeof (zoom) == "number") ? 1 : 0;
    var z = i ? zoom : 1;

    var x0, x1, y0, y1, r;
    switch (arguments.length) {
        case i + 2://point+CircleRect(x,y,w,h)
            x0 = arguments[i].x;
            y0 = arguments[i].y;
            var midp = new MidPoint(arguments[i + 1]);
            x1 = midp.x * z;
            y1 = midp.y * z;
            r = arguments[i + 1].w / 2 * z;
            break;
        case i + 3://Point+midPoint+Radius
            x0 = arguments[i].x;
            y0 = arguments[i].y;
            x1 = arguments[i + 1].x * z;
            y1 = arguments[i + 1].y * z;
            r = arguments[i + 2] * z;
            break;
    }

    var rx = Math.abs(x1 - x0), ry = Math.abs(y1 - y0);
    var dis = Math.sqrt((rx * rx + ry * ry));
    if (dis < r) {
        return true;
    } else {
        return false;
    }

}
/*tool 畫數字(zoom,ing,str,align,str)*///
/*align: center,start,end*/
CanvasRenderingContext2D.prototype.NumberR = function (zoom, img, numstr, align, rect, noclear) {
    this.Number(zoom, img, numstr, rect.x, rect.y, align, rect.w, rect.h, noclear);
}
/*tool 畫數字(數字字串,x,y,w,h)*///
CanvasRenderingContext2D.prototype.Number = function (zoom, img, numstr, x, y, align, w, h, noclear) {
    if (noclear == undefined) {
        noclear = 0;
    }
    var cw = w, ch = h;
    //try convert to str
    var str;
    if (typeof (numstr) != 'String') { str = numstr.toString(); }
    else if (typeof (numstr) == 'string') { str = numstr; }
    //set num array
    var nums = [];
    for (var i = 0; i < str.length; i++) { nums[i] = Number(str[i]); }
    //setImg

    var vw, vh, IsArray = img instanceof Array;
    if (IsArray) {
        vw = img[0].width; vh = img[0].height;
    } else {
        vw = img.width; vh = img.height / 10;
    }


    //set width
    var width = vw, height = vh;
    if (w === undefined || h === undefined) {
        h = vh * zoom; w = vw * zoom * str.length;
    } else {
        if (h < vh) {
            height = h;
            width = vw * height / vh;
        } else {
            width = w / str.length;
            height = vh * width / vw;
        }
    }

    //set align
    if (!noclear) {
        this.Clear(zoom, x, y, cw, ch);
    }
    if (!align) align = "center";
    switch (align) {
        case "center": default:
            x += cw / 2 - width * str.length / 2;
            break;
        case "start":
            break;
        case "end":
            x += cw / 2 - str.length * width;
            break;
    }

    //Clear

    for (var i = 0; i < str.length; i++) {
        if (IsArray) {
            this.DrawImg(zoom, img[nums[i]], x + i * width, y, width, height);
        } else {
            this.DrawImg(zoom, img, 0, nums[i] * vh, vw, vh, x + i * width, y, width, height);
        }
    }
}
/*tool 根據origin的長寬比去限制目標長寬比
mode:0:寬優先 1:高優先*/
var RatioFixedResize = function (mode, origin, target) {
    if (mode) {//高優先
        var wh = origin.w / origin.h;
        var nw = target.h * wh;
        return Rect(target, nw, target.h);
    } else {
        var hw = origin.h / origin.w;
        var nh = target.w * hw;
        return Rect(target, target.w, nh);
    }
}

///////////////////////////////////////////
$(document).ready(function () {

    //FS
    //document.addEventListener("fullscreenchange", FShandler);
    //document.addEventListener("webkitfullscreenchange", FShandler);
    //document.addEventListener("mozfullscreenchange", FShandler);
    //document.addEventListener("MSFullscreenChange", FShandler);
    //旋轉事件
    $(window).on("orientationchange", function () {
        SetGameScale();
    });
    //FSEnabled = document.fullscreenEnabled || document.webkitFullscreenEnabled || document.mozFullScreenEnabled || document.msFullscreenEnabled;
    //GoFullScreen = 1;
    //if (IsIOs) {
    //    window.scrollTo(0, 1);
    //}
});
//bool 是否支援全螢幕模式
var FSEnabled = 0;
//bool 是否可啟動全螢幕
var GoFullScreen = 0;
//{name,version}取得瀏覽器資訊
function get_browser_info() {
    var ua = navigator.userAgent, tem, M = ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
    if (/trident/i.test(M[1])) {
        tem = /\brv[ :]+(\d+)/g.exec(ua) || [];
        return { name: 'IE', version: (tem[1] || '') };
    }
    if (M[1] === 'Chrome') {
        tem = ua.match(/\bOPR\/(\d+)/)
        if (tem != null) { return { name: 'Opera', version: tem[1] }; }
    }
    M = M[2] ? [M[1], M[2]] : [navigator.appName, navigator.appVersion, '-?'];
    if ((tem = ua.match(/version\/(\d+)/i)) != null) { M.splice(1, 1, tem[1]); }
    return {
        name: M[0],
        version: M[1]
    };
}
/*bool 偵測是否為手機瀏覽*/
var CheckMobile = function () {
    window.IsMobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);
    window.IsIOs = /iPhone|iPad|iPod/i.test(navigator.userAgent);
    //screenLock = 1;
    window.browser = get_browser_info();
}
CheckMobile();
/*tool ws訊息生成(maintype,subtype,參數[])*/
var Action = function () {
    var actioncontent = "";    
    var _Ar = Array.prototype.slice.call(arguments, 2, arguments.length);
    actioncontent = _Ar.join(",");
    return {
        "actionmaintype": arguments[0],
        "actionsubtype": arguments[1],
        "actiontime": "",
        "actioncontent": actioncontent
    }
}
////////////////////////////////////////////
CanvasRenderingContext2D.prototype.DrawEnabled = 1;
CanvasRenderingContext2D.prototype.FontSize = 12;
CanvasRenderingContext2D.prototype.Font = function (zoom, size, unit, font) {
    var i = (typeof (arguments[0]) == "number") ? 1 : 0;
    var z = i ? zoom : 1;
    this.FontSize = size < 12 ? 12 : size;
    switch (arguments.length) {
        case i + 1:
            this.font = 'bold ' + size * z + 'px 微軟正黑體';
            break;
        case i + 2:
            if (/px|em|%/i.test(unit)) {
                this.font = 'bold ' + size * z + unit + ' 微軟正黑體';
            } else {
                this.font = 'bold ' + size * z + 'px ' + font;
            }
            break;
        case i + 3:
            this.font = 'bold ' + size * z + unit + ' ' + font;
            break;
    }
}

CanvasRenderingContext2D.prototype.FillText = function (zoom, text, point) {
    var i = (typeof (arguments[0]) == "number") ? 1 : 0;
    var z = i ? zoom : 1;
    var textlines = String(arguments[i]).split('\n');
    if (/bottom/i.test(this.textBaseline)) {
        for (var j = textlines.length - 1; j >= 0; j--) {
            switch (arguments.length) {
                case i + 2://text+rect
                    this.fillText(textlines[j], arguments[i + 1].x * z, arguments[i + 1].y * z - this.FontSize * z * (textlines.length - j - 1));
                case i + 3://text+x+y
                    this.fillText(textlines[j], arguments[i + 1] * z, arguments[i + 2] * z - this.FontSize * z * (textlines.length - j - 1));
            }
        }
    } else {
        for (var j = 0; j < textlines.length; j++) {
            switch (arguments.length) {
                case i + 2://text+rect
                    this.fillText(textlines[j], arguments[i + 1].x * z, arguments[i + 1].y * z + this.FontSize * z * j);
                case i + 3://text+x+y
                    this.fillText(textlines[j], arguments[i + 1] * z, arguments[i + 2] * z + this.FontSize * z * j);
            }
        }
    }
}
//canvas渲染方法擴充
CanvasRenderingContext2D.prototype.DrawImg = function (zoom, img, rect) {
    if (this.DrawEnabled != undefined) {
        if (!this.DrawEnabled) { return; }
    }


    var i = (typeof (arguments[0]) != 'object') ? 1 : 0;
    var z = i ?window.zoom : 1;
    var ratio = function (iimg) {
        var ow = iimg.width || iimg.Width;
        var oh = iimg.height || iimg.Height;
        return oh / ow;
    }
    try {
        switch (arguments.length) {
            case i + 1:
                if ('img' in arguments[i]) {//obj
                    if ('x' in arguments[i] && 'w' in arguments[i]) {
                        this.drawImage(arguments[i].img, arguments[i].x * z, arguments[i].y * z, arguments[i].w * z, arguments[i].h* z);
                    } else {
                        var rect = ('rect' in arguments[i]) ? 'rect' : 'Rect';
                        this.drawImage(arguments[i].img, arguments[i][rect].x * z, arguments[i][rect].y * z, arguments[i][rect].w * z, arguments[i][rect].h* z);
                    }

                } else {//img
                    this.drawImage(arguments[i], 0, 0);
                }
                break;
            case i + 2://img+rect
                this.drawImage(arguments[i], arguments[i + 1].x * z, arguments[i + 1].y * z, arguments[i + 1].w * z, arguments[i + 1].h * z);
                break;
            case i + 3://img+rect+rect
                if (arguments[i + 1].w && arguments[i + 2].w) {
                    var target = new RatioFixedResize(0, arguments[i + 1], arguments[i + 2]);
                    this.drawImage(arguments[i],//img
                        arguments[i + 1].x, arguments[i + 1].y, arguments[i + 1].w, arguments[i + 1].h,//source rect
                        target.x * z, target.y * z, target.w * z, target.w * (arguments[i + 1].h / arguments[i + 1].w) * z);//destination rect
                } else {//img+point+size
                    this.drawImage(arguments[i], arguments[i + 1].x * z, arguments[i + 1].y * z, arguments[i + 2].w * z, arguments[i + 2].h * z);
                }

                break;
            case i + 4://img+point+w+h
                if (typeof (arguments[i + 1]) == 'object') {
                    this.drawImage(arguments[i], rect.x * z, rect.y * z, arguments[i + 2] * z, arguments[i + 2] * ratio(arguments[i]) * z);
                } else {//img+x+y+size
                    this.drawImage(arguments[i], arguments[i + 1] * z, arguments[i + 2] * z, arguments[i + 3].w * z, arguments[i + 3].h * z);
                }
                break;
            case i + 5://img+x+y+w+h
                this.drawImage(arguments[i], arguments[i + 1] * z, arguments[i + 2] * z, arguments[i + 3] * z, arguments[i +4] * z);
                break;
            case i + 6://img+sx+sy+sw+sh+obj
                var target = new RatioFixedResize(0, new Rect(arguments[i + 1], arguments[i + 2], arguments[i + 3], arguments[i + 4]), arguments[i + 5]);
                this.drawImage(arguments[i], arguments[i + 1], arguments[i + 2], arguments[i + 3], arguments[i + 4], target.x * z, target.y * z, target.w * z, target.h * z);
                break;
            case i + 9://img+sx+sy+sw+sh+dx+dy+dw+dh
                var target = new RatioFixedResize(0, new Rect(arguments[i + 1], arguments[i + 2], arguments[i + 3], arguments[i + 4]), new Rect(arguments[i + 5], arguments[i + 6], arguments[i + 7], arguments[i + 8]));
                this.drawImage(arguments[i], arguments[i + 1], arguments[i + 2], arguments[i + 3], arguments[i + 4], target.x * z, target.y * z, target.w * z, target.w * (arguments[i + 4] / arguments[i + 3]) * z);
                break;
        }
    } catch (e) {
        console.warn(e.message);
    }
}
CanvasRenderingContext2D.prototype.ShadowBlur = function (zoom, blur, color, offsetX, offsetY) {
    var z = zoom;
    var b = blur || 0, c = color || 'black', ox = offsetX || 0, oy = offsetY || 0;
    this.shadowBlur = b * z;
    this.shadowColor = c;
    this.shadowOffsetX = ox * z;
    this.shadowOffsetY = oy * z;
}
//清除區塊
// rect 
//x,y,w,h
CanvasRenderingContext2D.prototype.Clear = function (zoom, rect, size) {
    var i = (typeof (arguments[0]) != 'object') ? 1 : 0;
    var z = i ? zoom : 1;

    var x = 0, y = 0, w = this.canvas.width, h = this.canvas.height;
    switch (arguments.length) {
        case i + 1://rect
            x = arguments[i].x * z; y = arguments[i].y * z;
            w = arguments[i].w * z; h = arguments[i].h * z;
            break;
        case i + 2://point+size
            x = arguments[i].x * z; y = arguments[i].y * z;
            w = arguments[i + 1].w * z; h = arguments[i + 1].h * z;
            break;
        case i + 3:
            if (typeof (arguments[i]) == "object") {//rect+w+h
                x = arguments[i].x * z; y = arguments[i].y * z; w = arguments[i + 1] * z; h = arguments[i + 2] * z;
            } else {
                x = arguments[i] * z; y = arguments[i + 1] * z;
                w = arguments[i + 2].w * z; h = arguments[i + 2].h * z;
            }
            break;
        case i + 4://x+y+w+h
            x = arguments[i] * z;
            y = arguments[i + 1] * z;
            w = arguments[i + 2] * z;
            h = arguments[i + 3] * z;
            break;
    }
    this.clearRect(x, y, w, h);
    return 1;
}

var textAlign = ["Start", "Left", "Center", "Right", "End"];
var textBaseline = ["Top", "Middle", "Alphabetic", "Bottom"];

for (var i = 0; i < textAlign.length; i++) {
    for (var j = 0; j < textBaseline.length; j++) {
        var str =
         "CanvasRenderingContext2D.prototype.SetText" + textAlign[i] + textBaseline[j] +
         " = function () {"
        + "this.textAlign = '" + textAlign[i].toLowerCase() + "';"
        + "this.textBaseline = '" + textBaseline[j].toLowerCase() + "';};";
        eval(str);
    }
};

function AnimationObjs(ctx) {
    this.Truncate = function () { AniObjQueue = []; }
    var AniObjQueue = [];
    this.Count = function () { return AniObjQueue.length; }

    var Processing = 0;
    this.ctx = ctx;
    var fps = 50;
    var interval = 1000 / fps;
    this.SetFPS = function (nfps) {
        fps = nfps;
        interval = 1000 / nfps;
    }

    function AniObj(img, startRect, endRect, duration, delay, callback, opt) {

        var start = startRect;
        var end = endRect;
        var time = duration;
        var dx = (end.x - start.x) / time;
        var dy = (end.y - start.y) / time;
        var dz = (end.w / start.w) / time;
        //mid center
        var startMid = new MidPoint(startRect);
        var endMid = new MidPoint(endRect);
        var dMid = { dx: (endMid.x - startMid.x) / time, dy: (endMid.y - startMid.y) / time };

        var obj = {
            img: img,
            start: start, end: end,
            x: start.x, y: start.y, w: start.w, h: start.h,
            endx: end.x, endy: end.y, endw: end.w, endh: end.h, endz: end.w / start.w,
            endMid: endMid, startMid: startMid, dMid: dMid,
            dx: dx, dy: dy, t: time, dt: 0,
            dz: dz, z: 1, duration: time, delay: delay,
            state: 0, pre: new Date().getTime() + delay,
            callback: callback, ctx: ctx,
            endcheck: function () {
                if (obj.t <= 0) {
                    obj.x = obj.endx;
                    obj.y = obj.endy;
                    obj.w = obj.endw;
                    obj.h = obj.endh;
                    obj.z = 1;
                    obj.state = 2;
                }
            },
            draw: function () {
                ctx.DrawImg(_zoom, this);
            },
            restore: function () {
                this.x = this.prex;
                this.y = this.prey;
                this.w = this.prew;
                this.h = this.preh;
            }
        }
        if (opt) {
            for (var prop in opt) {
                obj[prop] = opt[prop];
            }
        }

        return obj;
    }
    //最底層物件
    this.PushObj = function (img, startRect, endRect, duration, delay, callback, opt) {
        AniObjQueue.push(new AniObj(img, startRect, endRect, duration, delay, callback, opt));
    };
    //最上層物件
    this.AddObj = function (img, startRect, endRect, duration, delay, callback, opt) {
        AniObjQueue.unshift(new AniObj(img, startRect, endRect, duration, delay, callback, opt));
    };
    //新增動畫方法物件{Update,Duration,Delay,opt{callback,After}}
    this.AddAniact = function (duration, delay, update, opt) {
        //init
        var obj = {
            Update: update,
            t: duration, dt: 0, duration: duration, delay: delay,
            state: 0, pre: new Date().getTime() + delay,
            ctx: ctx
        };
        //end check
        obj.endcheck = function () {
            if (obj.t <= 0) {
                obj.state = 2;
            }
        }
        //opts
        if (opt) {
            for (var prop in opt) {
                obj[prop] = opt[prop];
            }
        }

        AniObjQueue.unshift(obj);
    }
    this.Array = function () {
        return AniObjQueue;
    }
    this.Act = function () {
        if (!AniObjQueue.length || Processing) { return; }
        Processing = 1;
        for (var i = AniObjQueue.length - 1; i >= 0; i--) {
            ctx.save();
            var obj = AniObjQueue[i];
            //update time
            var now = new Date().getTime();
            if (now - obj.pre < 0) {
                continue;
            }
            var dt = now - obj.pre;
            var _zoom = ("zoom" in window) ? window.zoom : 1;
            obj.pre = now;
            obj.t -= dt;//剩餘時間
            obj.dt = dt;//時間差

            ///////////////////////

            if (typeof (obj.Before) == "function") {
                //Before 
                obj.Before(obj);
            }

            if (typeof (obj.Update) == "function") {
                //update 
                obj.Update(obj);
                obj.endcheck();
            } else {
                //default
                obj.z = 1 + (obj.endw / obj.w - 1) * (obj.duration - obj.t) / obj.duration;
                //center standard
                obj.startMid.x += obj.dMid.dx * dt;
                obj.startMid.y += obj.dMid.dy * dt;
                obj.x = (obj.startMid.x - obj.w * obj.z / 2);
                obj.y = (obj.startMid.y - obj.h * obj.z / 2);
                if (obj.z != 1) { obj.z; }

                //end check
                if (obj.t <= 0) {
                    obj.x = obj.endx;
                    obj.y = obj.endy;
                    obj.w = obj.endw;
                    obj.h = obj.endh;
                    obj.z = 1;
                    obj.state = 2;
                }

                //draw obj
                this.ctx.DrawImg(_zoom, obj.img, obj.x, obj.y, obj.w * obj.z, obj.h * obj.z);
            }

            if (typeof (obj.After) == "function") {
                //After
                obj.After(obj);
            }

            if (obj.state == 2) {
                //Ani Ended =>do CallBack
                var rem = AniObjQueue.splice(i, 1);
                if (typeof (obj.callback) == "function") {
                    obj.callback(obj);
                }
                delete rem;
            }
            ctx.restore();
        }
        Processing = 0;
    }
};
function StaticObjs(ctx) {
    var StaticObjQueue = [];
    this.Count = function () {
        return StaticObjQueue.length;
    }
    var Processing = 0;
    this.Truncate = function () { StaticObjQueue = []; }
    this.ctx = ctx;
    this.BuildObj = function (id, img, rect, opt) {

        var o = { id: id, img: img, x: rect.x, y: rect.y, w: rect.w, h: rect.h };
        o.ctx = ctx;
        if (opt) {
            for (var prop in opt) {
                o[prop] = opt[prop];
            }
        }
        return o;
    };
    //底層
    this.PushObj = function (id, img, rect, opt) {
        StaticObjQueue.push(new this.BuildObj(id, img, rect, opt));
    };
    //頂層
    this.AddObj = function (id, img, rect, opt) {
        StaticObjQueue.unshift(new this.BuildObj(id, img, rect, opt));
    }
    this.AddPrint = function (id, func, opt) {
        if (typeof (func) != "function") {
            opt = func;
        }
        var obj = { id: id, Print: func };
        obj.ctx = ctx;
        if (opt) {
            for (var prop in opt) {
                obj[prop] = opt[prop];
            }
        }
        StaticObjQueue.unshift(obj);
    }
    this.Print = function () {
        if (Processing) {
            return;
        }
        var _zoom = window.zoom || 1;
        if (!StaticObjQueue.length) { return; }
        Processing = 1;
        for (var i = StaticObjQueue.length - 1; i >= 0; i--) {
            ctx.save();
            var obj = StaticObjQueue[i];
            if (typeof (obj.img) != "undefined") {
                if (!obj.img.complete) { continue; }
            }
            if (obj.Print) {
                obj.Print(obj);
            } else {
                this.ctx.DrawImg(_zoom, obj);
            }
            ctx.restore();
        }
        Processing = 0;
    }
    this.AddOrUpdateObj = function (id, img, rect) {
        if (!this.UpdateObj(id, img, rect)) {
            this.AddObj(id, img, rect);
        }
    }

    this.PushOrUpdateObj = function (id, img, rect) {
        if (!this.UpdateObj(id, img, rect)) {
            this.PushObj(id, img, rect);
        }
    }


    this.UpdateObj = function (id, img, rect) {
        //update
        for (var i = 0; i < StaticObjQueue.length; i++) {
            if (StaticObjQueue[i].id == id) {
                return StaticObjQueue[i] = new this.BuildObj(id, img, rect);
            }
        }
        return 0;
    }
    this.GetObj = function (id) {
        for (var i = 0; i < StaticObjQueue.length; i++) {
            if (StaticObjQueue[i].id == id) {
                return StaticObjQueue[i];
            }
            if (/:/i.test(id) && 'Tag' in StaticObjQueue[i]) {//tag
                var tags = id.slice(1).split(' ');
                for (var j = 0; j < tags.length; j++) {
                    if (StaticObjQueue[i]['Tag'].indexOf(tags[j]) > -1) {
                        return StaticObjQueue[i];
                    }
                }
            }
        }
        return 0;
    }
    this.RemoveAt = function (index) {
        StaticObjQueue.splice(index, 1);
    }
    //除去所有id包含str的物件
    this.RemoveIf = function (str) {
        for (var i = StaticObjQueue.length - 1; i >= 0; i--) {
            if (StaticObjQueue[i].id.indexOf(str) != -1) {
                StaticObjQueue.splice(i, 1);
            }
        }
    }
    this.Remove = function (id) {
        for (var i = 0; i < StaticObjQueue.length; i++) {
            if (StaticObjQueue[i].id == id) {
                return StaticObjQueue.splice(i, 1)[0];
            }
            if (/:/i.test(id) && 'Tag' in StaticObjQueue[i]) {//tag
                var tags = id.slice(1).split(' ');
                for (var j = 0; j < tags.length; j++) {
                    if (StaticObjQueue[i]['Tag'].includes(tags[j])) {
                        return StaticObjQueue.splice(i, 1)[0];
                    }
                }
            }
        }
        return 0;
    }
}
function InteractObjs(ctx) {
    var InteractObjQueue = [];
    var GroupedQueue = {};
    var Processing = 0;
    this.Truncate = function () {
        InteractObjQueue = []; this.After = function () { };
        GroupedQueue = {};
        IsActivated = [];
        preActivated = [];
    }
    this.ctx = ctx;

    this.BuildObj = function (id, img, rect, update, opt) {
        var obj = {
            ctx: ctx,
            id: id, Group: "default",
            img: img, x: rect.x, y: rect.y, w: rect.w, h: rect.h,
            prex: rect.x, prey: rect.y, prew: rect.w, preh: rect.h,
            Update: update,
            draw: function () { ctx.DrawImg(_zoom, this); },
            restore: function () {
                this.x = this.prex;
                this.y = this.prey;
                this.w = this.prew;
                this.h = this.preh;
            }
        };
        if (opt) {
            for (var prop in opt) {
                obj[prop] = opt[prop];
            }
        }
        if ("Group" in obj) {
            if (obj.Group in GroupedQueue) {
            } else {
                GroupedQueue[obj.Group] = [];
            }
            GroupedQueue[obj.Group].unshift(obj);
        }
        return obj;
    };
    this.After = function () { };
    this.PushObj = function (id, img, rect, update, opt) {
        InteractObjQueue.push(new this.BuildObj(id, img, rect, update, opt));
    };
    this.AddObj = function (id, img, rect, update, opt) {
        InteractObjQueue.unshift(new this.BuildObj(id, img, rect, update, opt));
    }

    this.AddDisplay = function (id, func, opt) {
        if (typeof (func) != "function") {
            opt = func;
        }
        var obj = { id: id, Update: func };
        if (opt) {
            for (var prop in opt) {
                obj[prop] = opt[prop];
            }
        }
        if ("Group" in obj) {
            if (obj.Group in GroupedQueue) {
            } else {
                GroupedQueue[obj.Group] = [];
            }
            GroupedQueue[obj.Group].unshift(obj);
        }
        InteractObjQueue.unshift(obj);
    }
    this.Display = function () {
        if (Processing) {
            return;
        }

        if (!InteractObjQueue.length) {
            if (typeof (this.After) == "function") {
                this.After(ctx);
            }
            return;
        }
        var _zoom = window.zoom || 1;
        ctx.Clear();
        Processing = 1;
        for (var i = InteractObjQueue.length - 1; i >= 0; i--) {
            ctx.save();
            var obj = InteractObjQueue[i];
            if (typeof (obj.Update) == "function") {
                obj.Update(obj);
            } else {
                ctx.DrawImg(_zoom, obj);
            }
            ctx.restore();
        }
        if (typeof (this.After) == "function") {
            this.After(ctx);
        }
        Processing = 0;
    }
    this.GetObj = function (id) {
        for (var i = 0; i < InteractObjQueue.length; i++) {
            if (InteractObjQueue[i].id == id) {
                return InteractObjQueue[i];
            }
        }
    }
    this.RemoveAt = function (index) {
        InteractObjQueue.splice(index, 1);
    }
    this.Hovered = function () {
        return hovered;
    }

    //Hovered/////////////////////////////////
    var IsHovered = "0";
    var preHovered = "0";
    this.Hover = function (id) {
        var rid = String(id).toLowerCase();
        if (preHovered == rid) {
            return;
        }
        IsHovered = "0";

        if (preHovered == "0" && rid == "0") {
            return this.Display();
        }

        for (var i = 0; i < InteractObjQueue.length; i++) {
            InteractObjQueue[i].IsHovered = 0;
            if (String(InteractObjQueue[i].id).toLowerCase() == rid) {
                IsHovered = rid;
                InteractObjQueue[i].IsHovered = 1;
            }
        }
        if (preHovered != IsHovered) {
            preHovered = IsHovered;
            this.Display();
        }
    }
    //Activated_GroupUI/////////////////////////
    var IsActivated = [];
    var preActivated = [];
    this.Activate = function (id) {
        var rid = String(id).toLowerCase();
        IsActivated = rid;
        if (preActivated == IsActivated) {
            return;
        }
        var o = this.GetObj(rid);
        if (!o) { return; }
        if (o.Group != "default") {
            for (var i = 0; i < GroupedQueue[o.Group].length; i++) {
                GroupedQueue[o.Group][i].IsActivated = 0;
            }
        }
        o.IsActivated = 1;
        if (preActivated != IsActivated) {
            preActivated = IsActivated;
            //this.Display();
        }
    }
    this.Deactivate = function (id) {
        var rid = String(id).toLowerCase();
        var o = this.GetObj(rid);
        if (o.IsActivated) {
            o.IsActivated = 0;
            if (preActivated == rid) {
                preActivated = 0;
            }
            this.Display();
        }
    }

    this.Remove = function (id) {
        for (var i = 0; i < InteractObjQueue.length; i++) {
            if (InteractObjQueue[i].id == id) {
                return InteractObjQueue.splice(i, 1);
            }
        }
    }
    this.DeleteAll = function (id) {
        for (var i = 0; i < InteractObjQueue.length; i++) {
            if (InteractObjQueue[i].id == id) {
                InteractObjQueue.splice(i, 1);
                i--;
            }
        }
    }
}



//繪圖物件
var CanvasObject = function () {
    this.AniQueue; this.StaticQueue; this.InteractQueue;
    var mainqueue = {};
    this.Anim = function (img, rect, rect, time, delay) {


        return this;
    };
    this.Callback = function (func) {

    };
}

String.prototype.SplitToNumber = function (seperator) {
    return this.split(seperator).map(function (item) { return Number(item) });
}

function ArraySum(arr) {
    return arr.reduce(function (pre, cur) { return pre + cur; });
}

window.IsLocalStorageSupported = 0;
try {
    localStorage.setItem("test", "check");
    localStorage.removeItem("test");
    IsLocalStorageSupported = 1;
} catch (e) {
}
window.localStorage.SetItem = function (key, value) {
    if (!IsLocalStorageSupported) { return 0; }
    try { localStorage.setItem(key, value); } catch (e) { return 0; }
}
function getPixelRatio(context) {
    var backingStore = context.backingStorePixelRatio ||
      context.webkitBackingStorePixelRatio ||
      context.mozBackingStorePixelRatio ||
      context.msBackingStorePixelRatio ||
      context.oBackingStorePixelRatio ||
      context.backingStorePixelRatio || 1;
    return (window.devicePixelRatio || 1) / backingStore;
};
function SetHDPICanvas(can, w, h, ratio) {
    if (!ratio) { ratio = getPixelRatio(can.getContext('2d')); }
    can.width = w * ratio;
    can.height = h * ratio;
    can.style.width = w + "px";
    can.style.height = h + "px";
    can.getContext("2d").setTransform(ratio, 0, 0, ratio, 0, 0);
    return can.getContext('2d');
}

//金額縮寫
function ReCalcNumber(num) {
    if (typeof num != 'string') { num = String(num) }
    var result = '';
    if (num.length <= 3) { return num; }
    var position = num.length % 3 != 0 ? num.length % 3 : 3;

    if (num.length > 9) { result = num.slice(0, position) + (num.charAt(position) != '0' ? '.' + num.charAt(position) : '') + 'B'; }
    else if (num.length > 6) { result = num.slice(0, position) + (num.charAt(position) != '0' ? '.' + num.charAt(position) : '') + 'M'; }
    else { result = num.slice(0, num.length - 3) + (num.charAt(num.length - 3) != '0' ? '.' + num.charAt(num.length - 3) : '') + 'K'; }
    return result;
}

//音效集合物件
Tools.AudioController = function () {
    var o = new Object(null);
    var playingQueue = new Object(null);
    var AudioType = Modernizr.audio.mp3 ? "mp3" : (Modernizr.audio.ogg ? "ogg" : 0);
    var path, init, bgm, mute = 0;
    var bindingEvent = "click";
    IsMobile ? bindingEvent = "touchend" : "click";
    //((IsIOs && iOSVersion > 8) ? "touchend" : "click");
    function none() { };
    /*靜音開關*/
    this.toggleMute = function (m) {
        mute = m || !mute;
        if (mute) {
            this.play = this.pause = this.resumeAll = this.pauseAll = none;
        } else {
            this.play = Play;
            this.pause = Pause;
            this.resumeAll = ResumeAll;
            this.pauseAll = PauseAll;
        }
    }
    this.Path = function (p) { path = p; return this; };
    /*新增音效
    * id:音效名稱  (file:音效檔名)  (opt:自訂屬性)    */
    this.push = function (id, file, opt) {
        o[id] = new (function () {
            if (typeof file != "string") {
                opt = file;
                file = 0;
            }
            var pa = new Audio(path + (file || id) + "." + AudioType);
            if (typeof opt == "object") {
                for (var prop in opt) { pa[prop] = opt[prop]; }
            }
            return pa;
        })(id, file, opt);
        return this;
    }
    /*設定指定id音效的音量 
    * id:音效名稱  val:音量(0~1)*/
    this.vol = function (id, val) {
        if (o[id]) { o[id].volume = IsIOs ? (val ? 1 : 0) : val; }
        return this;
    }
    /*設定全部音效的音量 
    * val:音量(0~1)*/
    this.volAll = function (val) {
        for (var id in o) {
            if (o[id]) { o[id].volume = IsIOs ? (val ? 1 : 0) : val; }
        }
        return this;
    }
    /*播放音效
    * id:指定音效名稱*/
    this.play = Play;
    function Play(id) {
        if (o[id]) {
            o[id].pause(); o[id].currentTime = 0; o[id].play();
            if (id != bgm) {
                playingQueue[id] = 1;
                if (!o[id].loop) {
                    o[id].onended = function () { delete playingQueue[id]; }
                }
            }
        }
        return this;
    }
    /*停止所有音效*/
    this.pauseNowPlaying = function () {
        for (var id in o) {
            if (bgm != id) {
                o[id].pause();
                delete playingQueue[id];
            }
        }
        return this;
    }
    /*設定bgm
    * id:指定為背景音樂的名稱*/
    this.bgm = function (id) {
        bgm = id;
        o[bgm].autoplay = 1;
        return this;
    }
    /*停止指定音效
    * id:指定音效名稱*/
    this.pause = Pause;
    function Pause(id) {
        if (o[id]) { o[id].pause(); delete playingQueue[id]; }
        return this;
    }
    /*繼續撥放所有被暫停的音效*/
    this.resumeAll = ResumeAll;
    function ResumeAll() {
        for (var id in playingQueue) {
            if (o[id].isPaused) {
                o[id].play();
                delete o[id].isPaused;
            }
        }
        if (bgm) {
            if (o[bgm].isPaused) {
                o[bgm].play();
                delete o[bgm].isPaused;
            }
        }
        return this;
    }
    /*停止所有正在撥放的音效*/
    this.pauseAll = PauseAll;
    function PauseAll() {
        for (var id in playingQueue) {
            if (!o[id].paused) {
                o[id].pause();
                o[id].isPaused = 1;
            }
        }
        if (bgm) {
            if (!o[bgm].paused) {
                o[bgm].pause();
                o[bgm].isPaused = 1;
            }
        }
        return this;
    }
    /*取得指定音效物件
    * id:特定音效名稱*/
    this.get = function (id) {
        if (o[id]) { return o[id]; }
        return 0;
    }
    var inittarget
    /*設定移動裝置的初始化事件*/
    this.init = function (target) {
        inittarget = target;
        target.addEventListener(bindingEvent, this.HandleAudioInit);
        //$(target).delegate("*","click",this.HandleAudioInit);
    }
    this.HandleAudioInit = function (evt) {
        evt.preventDefault();
        if (IsMobile && !init) {
            for (var i in o) {
                o[i].volume = 0;
                o[i].play();
                o[i].pause();
                o[i].volume = 1;
            }
            init = 1;
        }
        if (bgm && o[bgm].paused) {
            o[bgm].play();
        } else {
            //$(inittarget).undelegate("*","click", this.HandleAudioInit);
            inittarget.removeEventListener(bindingEvent, this.HandleAudioInit);
        }
    }
}

//網頁切換事件
Tools.VisibilityChange = new (function () {
    this.onInvisible = [], this.onVisible = [];
    var hidden, visibilityChange, IsInvisible, Supported = 0;
    var vi = this;
    this.addOnVisible = function (ele, func) {
        if (typeof func == "function") {
            vi.onVisible.push({ scope: ele, func: func });
        }
        return this;
    }
    this.addOnInvisible = function (ele, func) {
        if (typeof func == "function") {
            vi.onInvisible.push({ scope: ele, func: func });
        }
        return this;
    }
    function actInvisible() {
        for (var i = 0; i < vi.onInvisible.length; i++) { vi.onInvisible[i].func.call(vi.onInvisible[i].scope); }
    }
    function actVisible() {
        for (var i = 0; i < vi.onVisible.length; i++) { vi.onVisible[i].func.call(vi.onVisible[i].scope); }
    }
    this.PatchEventListener = function () {
        if (typeof document.hidden !== "undefined") { // Opera 12.10 and Firefox 18 and later support 
            hidden = "hidden";
            visibilityChange = "visibilitychange";
        } else if (typeof document.mozHidden !== "undefined") {
            hidden = "mozHidden";
            visibilityChange = "mozvisibilitychange";
        } else if (typeof document.msHidden !== "undefined") {
            hidden = "msHidden";
            visibilityChange = "msvisibilitychange";
        } else if (typeof document.webkitHidden !== "undefined") {
            hidden = "webkitHidden";
            visibilityChange = "webkitvisibilitychange";
        } else { hidden = 0; visibilityChange = 0; }
        Supported = (!document[hidden]);
        if (!Supported) {
            $(window).focus(actVisible);
            $(window).onpageshow = actVisible;

            $(window).blur(actInvisible);
            $(window).onpagehide = actInvisible;
        } else {
            document.addEventListener(visibilityChange, function () {
                if (document[hidden]) {
                    IsInvisible = 1;
                    actInvisible();
                } else {
                    IsInvisible = 0;
                    actVisible();
                }
            }, false);
        }
    }
    var test = 0;
})

$(document).ready(function () {
    Tools.VisibilityChange.PatchEventListener();
});
