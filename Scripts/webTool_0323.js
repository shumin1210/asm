﻿/// <reference path="../JQuery/jquery-1.10.2.js" />
/// <reference path="Data.js" />
/// <reference path="ToolsForWebTool.js" />
/*/// <reference path="https://www.crazybet.win/library/Tools.latest.js"/>*/


var WebTool = WebTool || {};

(function () {
    var core = WebTool;
    var base_width = 1280;
    var base_height = 785;
    var base_font_size = 15;

    var vars = [];

    /**
     * Read a page's GET URL variables and return them as an associative array.
     */
    core._getUrlVars = function () {
        var hash;
        var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
        for (var i = 0; i < hashes.length; i++) {
            hash = hashes[i].split('=');
            vars.push(hash[0]);
            vars[hash[0]] = decodeURIComponent(hash[1]);
        }
    }

    /**
     * 
     * @param {String} version :版本
     * @param {type} comparison
     * @returns {bool} 
     */
    core.isIE = function (version, comparison) {
        var cc = 'IE',
            b = document.createElement('B'),
            docElem = document.documentElement,
            isIE;

        if (version) {
            cc += ' ' + version;
            if (comparison) { cc = comparison + ' ' + cc; }
        }

        b.innerHTML = '<!--[if ' + cc + ']><b id="iecctest"></b><![endif]-->';
        docElem.appendChild(b);
        isIE = !!document.getElementById('iecctest');
        docElem.removeChild(b);
        return isIE;
    }
    

    core.Element = function () {
        var el = {};
        
        /**
         * 水平置中
         * @param {String} id :輸入設定的 id or css 字串
         */
        el.Center = function (id) {
            var control = $(id);

            if (Test.Undefined(control)) {
                return;
            }

            var pa = control.parent();
            var pa_width = pa.width();
            var m_left = (pa_width - control.width()) / 2;
            control.css({
                'margin-left': m_left + 'px'
            });
        }

        /**
         * 垂直置中
         * @param {String} id :輸入設定的 id or css 字串
         */
        el.Middle = function (id) {
            var control = $(id);

            if (Test.Undefined(control)) {
                return;
            }

            var pa = control.parent();
            var pa_height = pa.height();
            var m_top = (pa_height - control.height()) / 2;
            control.css({
                'margin-top': m_top + 'px'
            });
        }

        /**
         * 兩element相同高度
         * @param {HTMLElement} base_control :此control高度
         * @param {HTMLElement} target_control :需改變高度之control
         */
        el.SameHeight = function (base_control, target_control) {            
            if (Test.Undefined(base_control) || Test.Undefined(target_control)) {
                return;
            }

            target_control.height = base_control.height;
        }

        /**
         * 兩element相同寬度
         * @param {HTMLElement} base_control :此control寬度
         * @param {HTMLElement} target_control :需改變寬度之control
         */
        el.SameWidth = function (base_control, target_control) {
            if (Test.Undefined(base_control) || Test.Undefined(target_control)) {
                return;
            }

            target_control.width = base_control.width;
        }
        
        el._AdjustFontSize = [];
        /**
         * 根據螢幕寬度改變此element的字體大小
         * @param {type} id
         * @param {type} base_font_size :預設(寬=1280)字體大小 (opt)
         */
        el.AdjustFontSize = function (id, font_size) {
            if (!Tools.IsMobile) {
                font_size = Test.Undefined(font_size) ? base_font_size : font_size;

                var w_width, w_height;

                w_width = $(window).width();

                var w_radio = w_width / base_width;

                var _size = w_radio * font_size;

                if (_size < 12) {
                    _size = 12;
                }

                $(id).css({
                    'font-size': _size + 'px'
                });

                /*TODO:add resize事件*/
                //Tools.Orientation.Add(el.AdjustFontSize(id, font_size));
            }
        }

        

        /**
         * 新增select 選項
         * @param {HTMLElememt} select
         * @param {String} value
         * @param {String} text
         * @returns select
         */
        el.SelectOpt = function (select, value, text) {
            var option = document.createElement("option");
            option.text = text;
            option.value = value;
            select.appendChild(option);

            return select;
        }

        /**
         * Div或圖片或Control等比例縮放,以螢幕寬或高度為基準
         * @param {String} id :id or class
         * @param {Number} width :圖片原始寬度
         * @param {Number} height :圖片原始高度
         * @param {Number} pa :改變寬或高度(pa數)
         * @param {ConfigObject} opt :  {(選填)
         *                                  h_middle: bool(是否水平置中)(預設:false), 
         *                                  v_middle: bool(是否垂直置中)(預設:false), 
         *                                  Fixed: (0:以寬為基準1:以高為基準) (預設:0)
         *                              } 
         */
        el.Resize = function (id, width, height, pa, opt) {
            var control = $(id);

            if (Test.Undefined(control)) {
                return;
            }

            //parent尺寸
            var w_width, w_height;
            var w_h;
            var setting = { h_middle: false, v_middle: false, Fixed: 0 };
            if (opt) {
                for (var i in opt) {
                    setting[i] = opt[i];
                }
            }

            w_h = width / height;

            var parent = control.parent();

            w_width = parent.width();
            w_height = parent.height();

            if (setting.Fixed == 0) {
                control.css({
                    'width': w_width * pa + 'px',
                    'height': w_width * pa / w_h + 'px'
                });
            } else {
                control.css({
                    'height': w_height * pa + 'px',
                    'width': w_height * pa * w_h + 'px'
                });
            }

            if (setting.h_middle) {
                control.css({
                    'margin': '0 auto'
                });
            }

            if (setting.v_middle) {
                
                var pa_height = parent.height();
                var m_top = (pa_height - control.height()) / 2;


                control.css({
                    'margin-top': m_top + 'px'
                });
            }

            /*TODO: add resize*/
        }

        return el;
    }();
        
    core.Check = function () {
        var ch = {};

        /**
         * 是否為有效電子郵件
         * @param {String} Email
         * @returns {bool} 
         */
        ch.Email = function (Email) {
            var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
            return re.test(email);
        }


        return ch;
    }();

    /** 
     * 設定HTMLElement control的Attribute 
     * @param {HTMLElement} control
     * @param {ConfigObject} setting(Init:true 初始化)
     * @returns control
     */
    core.SetAttribute = function (control, setting) {
        if (Test.Undefined(setting)) {
            return control;
        }

        var init = "Init";
        for (var key in setting) {
            if (key == init) {
                continue;
            }
            if (key in control) {
                if (key == "style") {
                    $(control).css(setting[key]);
                } else if (key.toLowerCase() == "innertext"
                    || key.toLowerCase() == "text") {
                    control.appendChild(document.createTextNode(setting[key]));
                } else
                    control[key] = setting[key];
            }
        }

        
        if (init in setting)
            switch (type) {
                case "a":
                    control.style.textDecoration = "none";
                    break;
                default:

            }

        return control;
    }
    
    /**
     * 取得HTMLElement control的Attribute 
     * @param {Event} evt:(點擊)事件
     * @param {String} attr :Attribute值
     * * @param {HTMLElement} control:(opt)
     * @returns {object} 
     */
    core.GetAttr = function (evt, attr, control) {
        if (core.isIE(8)|| core.isIE(9)) {
            return control.attr;
        }
        try {
            return evt.currentTarget.attributes[attr].value;
        } catch (exception) {
            return "";
        }
    }

    /** 
     * create 控制項
     * @param {String} type 控制項類別
     * @param {type} setting 控制項的設定檔(Init:true 初始化)
     * @returns {HTMLElement} control 
     */
    core.CreateElement = function (type, setting) {
        var control = document.createElement(type);
        control = core.SetAttribute(control, setting);
        return control;
    }

    /**
     * 
     * @param {Number} length :長度
     * @param {String} str :判斷字串
     * @param {String} char :不足補字元
     * @returns {String} str 
     */
    core.PadLeft = function (length, str, char) {
        str = str.toString();
        if (str.length < length) {
            return core.PadLeft(length, char + str, char);
        } else {
            return str;
        }
    }

    /**
     * 取得日期天數
     * @param {Number} month
     * @param {Number} year
     * @returns {Number} Days 
     */
    core.GetDaysInMonth = function (month, year) {
        return new Date(year, month, 0).getDate();
    }

    /**
     * 依外部container來改變target大小
     * @param {String} target
     * @param {Number} org_width :原始寬
     * @param {Number} org_height :原始高    
     */
    core.Fit = function (target, org_width, org_height) {
        var set_w, set_h, control = $(target);
        if (Test.Undefined(control)) {
            return;
        }

        var pa = control.parent();
        var pa_w = pa.width(), pa_h = pa.height();

        if (pa_w > width) {
            set_w = width;
        } else {
            set_w = pa_w;
        }

        if (pa_h > height) {
            set_h = height;
        } else {
            set_h = pa_h;
        }

        pa.css({
            "width": set_w + "px",
            "height": set_h + "px"
        });

        control.each(function (i, obj) {
            var o_width = obj.naturalWidth;
            var o_height = obj.naturalHeight;

            if (o_width >= o_height) {
                $(obj).css({
                    "width": set_w + "px",
                    "height": "auto"
                });
            } else {
                $(obj).css({
                    "height": set_h + "px",
                    "width": "auto"
                });
            }
        });

    }

    /**
     * 限制字串長度,可輸入替代文字     
     * @param {String} str
     * @param {Number} length
     * @param {String} replaceStr :超過設定長度替代字串(opt)
     * @returns str
     */
    core.CutString = function (str, length, replaceStr) {
        if (str.length > length) {
            str = str.substring(0, length);
            str += (arguments.length == 2
                    || Test.Undefined(replaceStr))
                    ? ""
                    : replaceStr;
        }
        return str;
    }

    /** 移入移出效果 */
    core.Hover = function () {
        var b = {};

        /**
         * 改變圖片
         * @param {String} target_id
         * @param {String} orginalImg :路徑
         * @param {String} changeImg :路徑
         */
        b.ChangeImg = function (target_id, orginalImg, changeImg) {
            var target = $("#" + target_id);

            if (Test.Undefined(target)) {
                return;
            }

            target.on({
                mouseenter: function () {
                    target.attr("src", changeImg)                    
                },
                mouseleave: function () {
                    target.attr("src", orginalImg)
                },
                touchstart: function () {
                    target.attr("src", changeImg)
                },
                touchend: function () {
                    target.attr("src", orginalImg)
                }
            });
        }

        return b;
    }();

    /**
     * 
     * @param {type} ) {

    }
     */
    
    //function handler(param, self) {
    //    //...
    //}
    //for (var i = 0; i < 5; i++) {
    //    //TODO :IE8 test
    //    var btn = document.createElement("input");
    //    /*bind =>call,apply*/
    //    btn.onclick = handler.bind(btn, data);
    //    /*closure*/
    //    btn.onclick = function (data) {
    //        var _data = data;
    //        var result = function (evt) {
    //            handler(_data);
    //        }
    //        return result;
    //    }(data);
    //}


    core._Init = function () {
        core._Init = function () {

        }
        core._getUrlVars();

    }
    

    core._Init();

    Object.defineProperties(WebTool, {        
        UrlVars: {
            get: function () { return vars; }
        },
        
    });
}());

(function () {
    WebTool.Ajax = {};
    var url = "/GetData.asmx/getData";
    var core = WebTool,
        ajax = core.Ajax;

    var ajaxData = {};
    
    ajax.New = function (callback) {
        this.url = url;
        this.paraments = {};
        this.callback = callback;
    }

    /**
     * 執行function By String
     * @param {String} func :function名
     * @param {Object} namespace :namespace
     * @param {Object} data :參數
     * @param {Object} paramenter :參數     
     */
    ajax._ExecuteFunc = function (func, namespace, data, paramenter) {
        var args = [].slice.call(arguments).splice(2);
        var namespaces = func.split(".");
        var func = namespaces.pop();
        for (var i = 0; i < namespaces.length; i++) {
            namespace = namespace[namespaces[i]];
        }
        return namespace[func].apply(namespace, [data, paramenter]);
    }

    ajax._CreateRequest = function () {
        var request;
        try {
            request = new XMLHttpRequest();
        } catch (tryMS) {
            try {
                request = new ActiveXObject("Msxml2.XMLHTTP");
            } catch (otherMS) {
                try {
                    request = new ActiveXObject("Microsoft.XMLHTTP");
                } catch (failed) {
                    request = null;
                }
            }
        }
        return request;
    }


    function GetData(dataCollection) {
        var datastr = JSON.stringify(ajaxData[dataCollection].paraments);

        var request = ajax._CreateRequest();
        if (request != null) {

            request.attr = dataCollection;
            request.open("POST", ajaxData[dataCollection].url, true);
            request.responseType = "json";
            request.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
            request.send(datastr);
            request.onreadystatechange = function () {
                if (this.readyState == 4 && this.status == 200) {

                    var responseData = this.response;

                    if (responseData == null || responseData == "") {
                        if (!core.isIE(8) && !core.isIE(9)) {
                            return;
                        }
                        responseData = this.responseText;
                        if (responseData == null || responseData == "") {
                            return;
                        }
                    }

                    var test = this;
                    var ajaxCollection = ajaxData[this.attr];
                    var fun = ajaxCollection.callback;

                    if (typeof responseData != "object") {
                        responseData = JSON.parse(responseData);
                    }

                    var data = $.parseJSON(responseData.d);

                    ajax._ExecuteFunc(fun, window, data, ajaxCollection.paraments);
                }
                
            }

        }
    }
    
    /**
     * 
     * @param {String} Type
     * @param {func} callback
     * @param {Object} DataToServer
     * @param {Object} DataFromClient
     */
    ajax.Excute = function (Type, callback,
        DataToServer, DataFromClient) {

        if (Test.Undefined(DataToServer)) {
            DataToServer = {};
        }

        /*TODO: 參數縮減*/
        var tmp = window.JSON.stringify({
            type: Type,
            content: DataToServer
        });

        if (!ajaxData[Type])
            ajaxData[Type] = new ajax.New(callback);

        ajaxData[Type].paraments = {
            message: tmp
        };

        if (!Test.Undefined(DataFromClient))
            for (var item in DataFromClient)
                ajaxData[Type]["paraments"][item] = DataFromClient[item];

        GetData(Type);
    }
    

}());

//取得及確認此Div是否有append過(return div)
function getCheckDiv(divName, className, apPosition) {
    var div = document.getElementById(divName);

    if (!div) {
        div = document.createElement("div");
        div.id = divName;
        document.getElementById(apPosition).appendChild(div);
    }

    if (className != undefined && className != "") {
        div.className = className;
    }

    return div;
}

$(document).ready(function () {

    //Tools.Orientation.Add()
});