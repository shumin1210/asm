﻿DataSheet.Game_15 = {   
    _Record: {
        gid: {
            col: "機台編號",
            Value: "",
            Center: true,
            Set: function (val) {
                if (val == null)
                    return;
                this.Value = val;
            }
        },
        StartTime: {
            col: "進入機台時間",
            Value: "",
            Set: function (val) {
                if (val) {
                    //var date = new Date(ParseDate(val));
                    val = val.Year + '/' + val.Month + '/' + val.Day + " " + val.Hour + ":" + val.Minute;
                    this.Value = val;
                } else
                    this.Value = "";

            }
        },
        EndTime: {
            col: "離開機台時間",
            Value: "",
            Set: function (val) {
                if (val) {
                    //var date = new Date(ParseDate(val));
                    val = val.Year + '/' + val.Month + '/' + val.Day + " " + val.Hour + ":" + val.Minute;
                    this.Value = val;
                } else
                    this.Value = "";

            }
        }, 
        TotalCost: {
            col: "總押分",
            Value: "",
            Center: true,
            Set: function (val) {
                if (val == null)
                    return;
                this.Value = val;
            }
        },        
        TotalGain: {
            col: "總贏分",
            Value: "",
            Center: true,
            Set: function (val) {
                if (val == null)
                    return;
                this.Value = val;
            }
        },
    },
    //_col: ["機台編號", "進入機台時間", "離開機台時間", "總押分", "總贏分"],
    _col: ["gid", "StartTime", "EndTime", "TotalCost", "TotalGain"],
    _width: [150, 200, 200, 200, 200],
    Get: {
        /*
        Poker: {
            //花色index轉換
            Suit: function (index) {
                index = typeof index == "string" ? Number(index) : index;

                return (Math.floor((index - 1) / 13) + 1);
            },
            //點數index轉換
            Point: function (index) {
                index = typeof index == "string" ? Number(index) : index;

                var result = index - Math.floor(index / 13) * 13;

                if (result == 0) {
                    result = 13;
                }

                return result;
            },
            //花色點數轉換index
            Index: function (Suit, Point) {
                return (Suit - 1) * 13 + Point;
            }
        }*/
    }
}