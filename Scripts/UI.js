﻿/// <reference path="_reference.js" />

var SearchUI = function () {
    var self = {}, ele = {};
    
    self.id = {
        Radio: {
            Search: "RightView_Search_Radio_Trans"
        }
    }
    
    self.Trans = function () {
        var searchControlDiv_Trans = buildEle("div");

        var searchRadioList_Trans_label = [];
        var searchRadioList = Language.searchRadioList_Trans, searchRadioList_val = ["", "E", "F"], tmp;
        for (var i = 0; i < searchRadioList.length; i++) {
            tmp = buildEle("input", {
                type: "radio",
                value: searchRadioList_val[i],
                checked: i == 0,
                name: self.id.Radio.Search,
                id: self.id.Radio.Search + "_" + i
            });
            searchControlDiv_Trans.appendChild(tmp);

            tmp = buildEle("label", {
                htmlFor: self.id.Radio.Search + "_" + i,
                innerText: searchRadioList[i]
            });
            searchControlDiv_Trans.appendChild(tmp);
            searchRadioList_Trans_label.push(tmp);
        }

        ele.searchBtn_Trans = buildEle("input", {
            type: "button",
            id: "SearchBtn",
            className: "confirmbtn leftBtn",
            style: {
                "margin": "0",
                "margin-left": "3px",
                "padding": "3px"
            },
            value: Language.btn_Search
        });

        ele.searchBtn_Trans.onclick = function () { Control.RightView.Search(); }

        searchControlDiv_Trans.appendChild(ele.searchBtn_Trans);

        Language.AddRefreshList(Language.RefreshType.Span, searchRadioList_Trans_label, "searchRadioList_Trans");

        Language.AddRefreshList(Language.RefreshType.Btn, ele.searchBtn_Trans, "btn_Search");

        return searchControlDiv_Trans;
    }
    
    self.Normal = function () {
        var searchControlDiv_Total = buildEle("div");
        ele.searchBtn_Total = buildEle("input", {
            type: "button",
            id: "SearchBtn",
            className: "confirmbtn leftBtn",
            style: {
                "margin": "0",
                "padding":"3px"
            },
            value: Language.btn_Search
        });

        ele.searchBtn_Total.onclick = function () { Control.RightView.Search(); }

        searchControlDiv_Total.appendChild(ele.searchBtn_Total);

        Language.AddRefreshList(Language.RefreshType.Btn, ele.searchBtn_Total, "btn_Search");

        return searchControlDiv_Total;
    }

    self.QRCode = function () {
        var Div = WebTool.CreateElement("div", {
            style: {
                "width": "100%",
                "padding-top": "10px",
                "padding-left": "1%"
            }
        });

        var select_status = WebTool.CreateElement("select");

        for (var i = 0; i < Language.SearchStatus.length; i++) {
            WebTool.Element.SelectOpt(select_status, i, Language.SearchStatus[i]);
        }

        Language.AddRefreshList(Language.RefreshType.Select, select_status, "SearchStatus");

        Div.appendChild(select_status);

        var span_user = WebTool.CreateElement("span", {
            innerText: Language.txt_searchUser,
            style: {
                "margin-Left": "5px"
            }
        });
        Div.appendChild(span_user);

        Language.AddRefreshList(Language.RefreshType.Span, span_user, "txt_searchUser");

        var input_user = WebTool.CreateElement("input");
        Div.appendChild(input_user);

        var SearchBtn = WebTool.CreateElement("input", {
            "type": "button",
            "value": Language.btn_Search
        });

        SearchBtn.onclick = function () {
            QRView.Search.Page.index = 0;
            QRView.Search.Page.isInit = false;
            QRView.GetList();
        }

        Div.appendChild(SearchBtn);

        Language.AddRefreshList(Language.RefreshType.Btn, SearchBtn, "btn_Search");

        var AddBtn = WebTool.CreateElement("input", {
            "type": "button",
            "value": Language.btn_Add,
            style: {
                "float": "right"
            }
        });

        AddBtn.onclick = function () {
            QRView.DivAdd.style.display = "block";
        }

        Div.appendChild(AddBtn);

        Language.AddRefreshList(Language.RefreshType.Btn, AddBtn, "btn_Add");

        var PrintBtn = WebTool.CreateElement("input", {
            "type": "button",
            "value": Language.btn_Print,
            style: {
                "float": "right"
            }
        });

        PrintBtn.onclick = function () {
            LoadingPanel.Show();

            ws.Send("get.printqrlist", {
                GID: Reporter.GID
            });
        };

        Div.appendChild(PrintBtn);

        Language.AddRefreshList(Language.RefreshType.Btn, PrintBtn, "btn_Print");

        return { Div: Div ,select_status:select_status,input_user:input_user};
    }
    
    return self;
}();