﻿DataSheet.Game_16 = {
    _Record: {        
        createdate: {
            col: "結算時間",
            Value: "",
            Set: function (val) {
                if (val) {
                    //var date = new Date(ParseDate(val));
                    val = val.Year + '/' + val.Month + '/' + val.Day + " " + val.Hour + ":" + val.Minute;
                    this.Value = val;
                } else
                    this.Value = "";

            }
        },
        UNO: {
            col: "回合",
            Value: "",
            Center: true,
            Set: function (val) {
                if (val == null)
                    return;

                this.Value = val;
            }
        },
        pokers: {
            col: "玩家手牌",
            Value: "",
            Center: true,
            Set: function (val) {
                if (val == null)
                    return;

                var result = val.split(",");
                var _p = [], _point, _poker, _r;
                for (var i = 0; i < result.length; i++) {
                    if (result[i] == "0") {
                        continue;
                    }

                    _p.push(DataSheet.Game_16.Get.PokerSuitAndPoint(result[i]));
                }

                this.Value = _p.join(",");
            }
        },
        win: {
            col: "總贏分",
            Value: "",
            Center: true,
            Set: function (val) {
                if (val == null)
                    return;

                this.Value = val;
            }
        },
        bet: {
            col: "總押注",
            Value: "",
            Center: true,
            Set: function (val) {
                if (val == null)
                    return;

                this.Value = val;
            }
        },        
        publicpoker1: {
            col: "公牌1",
            Value: "",
            Center: true,
            Set: function (val) {
                if (val == null)
                    return;

                this.Value = DataSheet.Game_16.Get.PokerSuitAndPoint(val);
            }
        },
        publicpoker2: {
            col: "公牌2",
            Value: "",
            Center: true,
            Set: function (val) {
                if (val == null)
                    return;

                this.Value = DataSheet.Game_16.Get.PokerSuitAndPoint(val);
            }
        },
        publicpoker3: {
            col: "公牌3",
            Value: "",
            Center: true,
            Set: function (val) {
                if (val == null)
                    return;

                this.Value = DataSheet.Game_16.Get.PokerSuitAndPoint(val);
            }
        },
        publicpoker4: {
            col: "公牌4",
            Value: "",
            Center: true,
            Set: function (val) {
                if (val == null)
                    return;

                this.Value = DataSheet.Game_16.Get.PokerSuitAndPoint(val)
            }
        },
        publicpoker5: {
            col: "公牌5",
            Value: "",
            Center: true,
            Set: function (val) {
                if (val == null)
                    return;

                this.Value = DataSheet.Game_16.Get.PokerSuitAndPoint(val);
            }
        },
    },
    _col: ["UNO", "createdate", "pokers", "win", "bet", "publicpoker1", "publicpoker2", "publicpoker3", "publicpoker4", "publicpoker5"],
    _width: [100, 200, 150, 200, 200, 220, 220, 220, 200,200],
    Get: {
        Poker: {
            //花色index轉換
            Suit: function (index) {
                index = typeof index == "string" ? Number(index) : index;

                return (Math.floor((index - 1) / 13) + 1);
            },
            //點數index轉換
            Point: function (index) {
                index = typeof index == "string" ? Number(index) : index;

                var result = index - Math.floor(index / 13) * 13;

                if (result == 0) {
                    result = 13;
                }

                return result;
            },
            //花色點數轉換index
            Index: function (Suit, Point) {
                return (Suit - 1) * 13 + Point;
            }
        },
        PokerSuitAndPoint: function(val){
            var _poker = DataSheet.Game_16.Get.Poker.Suit(val),
            _point = DataSheet.Game_16.Get.Poker.Point(val);

            var _r = DataSheet.Game_16.eNum.Suit[_poker - 1] + DataSheet.Game_16.eNum.Poker[_point - 1];
            return _r;
        }
    },
    eNum: {
        Poker: ["A", "2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K"],
        Suit: ["♠", "♥", "♦", "♣"]
    }
}