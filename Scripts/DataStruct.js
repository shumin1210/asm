﻿/// <reference path="../jquery/jquery-1.12.3.min.js" />
/// <reference path="../jquery/Tools.js" />
/// <reference path="data.js" />
/// <reference path="webTool.js" />
/// <reference path="DayTransReport.js" />

var DataStruct_GameList = function () {
    var self = {};

    self.Title = Language.Menu_enterReport;

    self.Column = {
        Layer1: ["Name", "DataCount", "totalBet", "totalWin", "Profit", "Refund", "Sum", "AgentRefund", "AgentBonus", "CompanyBonus", "Currency"],
        Layer2: ["Name", "DataCount", "totalBet", "totalWin", "Profit", "Refund", "Sum", "Currency"],
        Layer3: ["GName", "DataCount", "Name", "totalBet", "totalWin", "Profit", "Refund", "Sum"],
        Layer4: ["GName", "DataCount", "Name", "totalBet", "totalWin", "Profit", "Refund", "Sum"]
    }

    Object.defineProperty(self, "ColumnTitleText", {
        get: function () {
            var _col, top_title;

            switch (Layout) {
                case Layouts.LAYER1:
                    _col = Language.Column_Records_Layer1;
                    
                    top_title = _col.slice(0, _col.length - 1);
                    for (var i = Reporter.MaxLevel - 3; i >= Reporter.Level - 1 ; i--) {
                        top_title.push(Language.Column_UpperBonus_Title[i]);
                    }
                    top_title.push(_col[_col.length - 1]);

                    break;
                case Layouts.LAYER2:
                    _col = Language.Column_Records_Layer2;
                    break;
                case Layouts.LAYER3:                
                    _col = Language.Column_Records_Layer3;
                    top_title = _col.slice(1, _col.length);

                    break;
                case Layouts.LAYER4:
                    _col = Language.Column_Records_Layer3;
                    break;
                default:

            }

            if (!top_title)
                top_title = _col;

            return { top: top_title, bottom:_col };
        }
    })

    return self;
}();

var DataStruct_Trans = function () {
    var self = {};

    self.Title = Language.subMenu_reportQue;

    self.Column = {
        Layer1: ["Name", "Cash", "Credit", "MemberCost", "Currency"],
        Layer2: ["Name", "Cash", "Credit", "MemberCost", "Currency"],
        Layer3: ["Mode", "Name", "Cash", "Credit", "MemberCost", "Currency"],
        Layer4: ["Name", "Cash", "Credit", "CreditMax", "MemberCost", "Currency"]
    }

    Object.defineProperty(self, "ColumnTitleText", {
        get: function () {
            var _col, top_title;

            switch (Layout) {
                case Layouts.LAYER1:
                    _col = Language.Column_Trans_Records_Layer1;

                    top_title = _col.slice(0, _col.length);
                    for (var i = 0; i < top_title.length; i++) {
                        if (top_title[i] == "現金" || top_title[i] == "信用" || top_title[i] == "信用餘額")
                            top_title[i] = "總" + top_title[i] + "<br/>(含下線)";
                        else if (top_title[i] == "Cash" || top_title[i] == "Credit" || top_title[i] == "Credit balance")
                            top_title[i] = "Sum " + top_title[i] + "<br/>(Include DownLine)";
                    }

                    break;
                case Layouts.LAYER2:
                    _col = Language.Column_Trans_Records_Layer2;
                    break;
                case Layouts.LAYER3:                
                    _col = Language.Column_Trans_Records_Layer3;

                    top_title = _col.slice(1, _col.length);

                    break;
                case Layouts.LAYER4:
                    _col = Language.Column_Trans_Records_Layer3;
                    break;
                default:

            }

            if (!top_title)
                top_title = _col;

            return { top: top_title, bottom: _col };
        }
    })

    return self;
}();

var DataStruct_MemberManage = function () {
    var self = {};

    self.Title = Language.Menu_MemberManage;

    self.Column = {
        Layer1: ["Name", "UserUD", "CreditRemain", "CreditSum", "RemainRefund", "LastLoginTime", "Currency"],
        Layer2: ["Name", "UserUD", "CreditRemain", "CreditSum", "CashSum", "RemainRefund", "LastLoginTime", "Currency"]
    }

    self.Btn = {
        Add: {
            Type: {
                Agnet: "btn_AddAgent",
                Member: "btn_AddMember",
                ShareHolder: "btn_AddStockholder"
            },
            Create: function (Type) {
                var btn = createBtn(Language[Type], ColorSetting.Btn.AddAgentMember);
                btn.UserID = RightView.Data.CompanyList.ReceivePlayerID;

                switch (Type) {
                    case this.Type.Agnet:
                        btn.onclick = function (evt) {
                            AddEditPack.Role = 1;
                            RightView.Show.EditPanel(evt, Modes.ADD);
                        }
                        break;
                    case this.Type.ShareHolder:
                        btn.onclick = function (evt) {
                            AddShareHolderView.Mode = Modes.ADD;
                            AddShareHolderView.Show();
                        }
                        break;
                    case this.Type.Member:
                    default:
                        btn.onclick = function (evt) {
                            AddEditPack.Role = 0;
                            RightView.Show.EditPanel(evt, Modes.ADD);
                        }
                        break;
                }

                return btn;
            }
        },
        Edit: function (userID) {
            var btn = createBtn(Language.btn_Edit, ColorSetting.Btn.AddEditDel);
            btn.UserID = userID;
            btn.onclick = function (evt) {
                RightView.Show.EditPanel(evt, Modes.EDIT);
            }

            return btn;
        },
        EditShareHolder: function (userID) {
            var btn = createBtn(Language.btn_Edit, ColorSetting.Btn.AddEditDel);
            btn.UserID = userID;
            btn.onclick = function (evt) {
                LoadingPanel.Show();

                AddShareHolderView.Mode = Modes.EDIT;
                AddShareHolderView.Show(evt.currentTarget.UserID);
            }

            return btn;
        },
        Delete: function (userID) {
            var btn = createBtn(Language.btn_Delete, ColorSetting.Btn.AddEditDel);
            btn.UserID = userID;
            btn.onclick = function (evt) {
                LoadingPanel.Show();
                ws.Send("deletedata", {
                    GID: Reporter.GID,
                    ShopID: evt.currentTarget.UserID
                });
            }

            return btn;
        },
        BanOrReRight: function (status, userID) {
            var btn = createBtn(status == 0 ? Language.btn_ban : Language.btn_reright,
                            status == 0 ? ColorSetting.Btn.Ban : ColorSetting.Btn.ReRight);

            btn.User_Status = status;
            btn.UserID = userID;
            btn.onclick = function (evt) {
                Control.RightView.Ban(evt);
            }

            return btn;
        },
        CashIn: function (userID) {
            var btn = createBtn(Language.btn_CashIn, ColorSetting.Btn.CashIn);
            btn.UserID = userID;
            btn.onclick = function (evt) {
                CashInView.Show(evt.currentTarget.UserID);
            }

            return btn;
        },
        CashOut: function (userID) {
            var btn = createBtn(Language.btn_CashOut, ColorSetting.Btn.CashOut);
            btn.UserID = userID;
            btn.onclick = function (evt) {
                CashOutView.Show(evt.currentTarget.UserID);
            }

            return btn;
        },
        SetCredit: function (userID) {
            var btn = createBtn(Language.btn_Credit, ColorSetting.Btn.CashOut);
            btn.UserID = userID;
            btn.onclick = function (evt) {
                SetCreditView.Show(evt.currentTarget.UserID);
            }

            return btn;
        },
        EditPassword: function () {
            var btn = createBtn(Language.btn_EditPassword, ColorSetting.Btn.CashOut);
            btn.onclick = function (evt) {
                EditPasswordView.SetPassword.Show();
            }

            return btn;
        }
    }

    self.ManageView = {
        Layer1: function (row) {
            var cell = document.createElement("td");
            cell.style.textAlign = "right";


        },
        Layer2: function () {

        }
    }

    Object.defineProperty(self, "ColumnTitleText", {
        get: function () {
            var _col, top_title;

            switch (Layout) {
                case Layouts.LAYER1:
                    _col = Language.Column_MemberManage_Records_Layer1;

                    top_title = [];
                    for (var i = 0; i < _col.length; i++) {
                        if (_col[i] != "現金開分")
                            top_title.push(_col[i]);
                    }

                    break;
                case Layouts.LAYER2:
                    _col = Language.Column_MemberManage_Records_Layer2;
                    break;
                case Layouts.LAYER3:
                case Layouts.LAYER4:
                    _col = Language.Column_MemberManage_Records_Layer3;
                    break;
                default:

            }

            if (!top_title)
                top_title = _col;

            return { top: top_title, bottom: _col };
        }
    })

    return self;
}();

var DataStruct_Stockholder = function () {
    var self = {};

    self.Title = Language.Menu_StockholderManage;

    self.Column = {
        Layer1: ["Name"],
        Layer2: ["Name"]
    }

    Object.defineProperty(self, "ColumnTitleText", {
        get: function () {
            var _col, top_title;

            switch (Layout) {
                case Layouts.LAYER1:
                    _col = top_title = Language.Column_StockholderManage_Records_Layer1;
                    break;
                case Layouts.LAYER2:
                    _col = Language.Column_StockholderManage_Records_Layer1;
                    break;
                default:

            }

            if (!top_title)
                top_title = _col;

            return { top: top_title, bottom: _col };
        }
    })

    return self;
}();

var DataStruct_GameRefundList = function () {
    var self = {};

    self.Title = Language.Menu_GamerefundList;

    self.Column = {
        Layer1: ["Name", "Refund", "Currency"],
        Layer2: ["Name", "Refund", "Currency"],
        Layer3: ["Name", "Refund", "Currency"],
        Layer4: ["Name", "Refund", "Currency"]
    }

    Object.defineProperty(self, "ColumnTitleText", {
        get: function () {
            var _col, top_title;

            switch (Layout) {
                case Layouts.LAYER1:
                    _col = top_title = Language.Column_GameRefundList_Records_Layer1;

                    break;
                case Layouts.LAYER2:
                    _col = top_title = Language.Column_GameRefundList_Records_Layer1;
                    break;
                case Layouts.LAYER3:
                    _col = top_title = Language.Column_GameRefundList_Records_Layer1;

                    break;
                case Layouts.LAYER4:
                    _col = top_title = Language.Column_GameRefundList_Records_Layer1;
                    break;
                default:

            }

            if (!top_title)
                top_title = _col;

            return { top: top_title, bottom: _col };
        }
    })

    return self;
}();

var RightView_DataStruct = {
    GetDataStruct: function () {
        switch (Reporter.DataType) {
            case DataType.GAMELIST:
                return DataStruct_GameList;
            case DataType.TRANSACTION:
                return DataStruct_Trans;
            case DataType.MEMBERMANAGE:
                return DataStruct_MemberManage;
            case DataType.STOCKHOLDERMANAGE:
                return DataStruct_Stockholder;
            case DataType.GAMEREFUNDLIST:
                return DataStruct_GameRefundList;
        }

        return DataStruct_GameList;
    },
    GetColumn: function () {
        if (Reporter.DataType == DataType.NONE)
            return [];

        var struct = this.GetDataStruct();
        var col = struct.Column;
        if (!col)
            return [];

        switch (Layout) {
            case Layouts.LAYER1:
                return col.Layer1;
            case Layouts.LAYER2:
                return col.Layer2;
            case Layouts.LAYER3:
                if (!col.Layer3)
                    return [];

                return col.Layer3;
            case Layouts.LAYER4:
                if (!col.Layer4)
                    return [];

                return col.Layer4;
            default:
                return [];
        }
    },
}