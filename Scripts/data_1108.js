﻿/// <reference path="../jquery/jquery-1.12.3.min.js" />
/// <reference path="../jquery/Tools.js" />
/// <reference path="connect.js" />
/// <reference path="view.js" />

var url = "wss://twistt.crazybet.win/webHandler.ashx";
var ws = new Tools.Connection(url);

var Layouts = Tools.Enumify("NONE", "LOGIN", "MAINPAGE",
    "GAMELIST", "LAYER1", "LAYER2", "LAYER3", "LAYER4");

var Modes = Tools.Enumify("VIEW","ADD", "EDIT");

var Layout = Layouts.NONE;

var _Report = function () { }

_Report.prototype = {
    GID: "",
    Language: "TW",
    BorderColor: "gray",
    ID:0,
    Name: "",
    Token: "",
    Role: 0,
    Permission:0
}

var Reporter = new _Report();

var _TreeNode = function () {}

_TreeNode.prototype = {
    ID:0,
    UpLevelID: 0,
    Level:0,
    Text: "",
    TabWidth:20,
    State: 0,
    _GroupName:"",
    _Clickabled: false,
    ele_txt:null,
    Set: function (recordID, uplevel, lv, GroupName) {
        this.ID = recordID;
        this.UpLevelID = uplevel;
        this.Level = lv;

        this._GroupName = GroupName ? GroupName : "";

        if (recordID == Reporter.ID) {
            this.SetState(0);
            this.Disabled();
            return this;
        } else if (uplevel == recordID) {
            this.SetState(1);
        } else {
            this.SetState(2);
        }
        
        this.Enabled();        

        return this;
    },
    Disabled:function(){
        this._Clickabled = false;
    },
    Enabled:function(){
        this._Clickabled = true;
    },
    SetState: function (State) {
        this.State = State;

        switch (State) {
            case 0:
                this.Text = "";
                break
            case 1:
                this.Text = "-";
                break
            case 2:
                this.Text = "+";
                break
            default:
        }

        if (this.ele_txt) {
            this.ele_txt.innerText = this.Text;
        }

        return this;
    },    
    CreateTD:function(){
        var cell = document.createElement("td");
        cell.style.padding = "5px";
        
        var txt = buildEle("span", {
            "innerText": this.Text,style:{"color":"red"}
            });

        txt.onclick = this.Click;
        txt.treePack = this;

        if(this._Clickabled)
            txt.className = "pointer";

        txt.style.marginLeft = (this.Level * this.TabWidth) + "px";

        this.ele_txt = txt;

        cell.appendChild(txt);

        return cell;
    },
    Click: function (evt) {
        var _treepack = evt.currentTarget.treePack;

        if (!_treepack._Clickabled) return;
        
        //取資料
        if (_treepack.State == 2)
            Control.RightView.Layer1.TreeClick(_treepack);
        else
            $("." + getTreeNodeName(_treepack.ID)).remove();
        
        _treepack.SetState(_treepack.State == 1 ? 2 : 1);
    }
}

function getTreeNodeName(ID) {
    return "tr_" + Layout + "_" + ID;
}

function Init() {
    ReportTitle.Init();
    Login.Init();
    //MainPage.Init();
    LeftView.Init();

    RightView.Init();
    LoadingPanel.Init();

    RightView.Select.Year.push("yy");
    RightView.Select.Month.push("mm");
    RightView.Select.Day.push("dd");
    RightView.Select.HH.push("HH");
    RightView.Select.Min.push("min");

    RightView.Select.Year.push("yyEnd");
    RightView.Select.Month.push("mmEnd");
    RightView.Select.Day.push("ddEnd");
    RightView.Select.HH.push("HHEnd");
    RightView.Select.Min.push("minEnd");

    RightView.Select.Initial();
}

function buildEle(type, opt) {
    return DeepCopyProps.call(document.createElement(type), opt);
}

function padLeft(length, str, char) {
    str = str.toString();
    if (str.length < length) {
        return padLeft(length, char + str, char);
    } else {
        return str;
    }
}

function createRadioElement(text, value, name, checked) {
    var container = buildEle("div", { style: { "display": "inline-block" } });
    var input = document.createElement('input');
    input.id = name + value;
    input.type = 'radio';
    input.value = value;
    input.style.display = "inline-block";
    input.style.color = "black";
    input.style.width = "15px";

    input.name = name;
    if (checked) {
        input.checked = 'checked';
    }

    container.appendChild(input);

    var label = document.createElement("label");
    label.htmlFor = input.id;
    label.innerText = text;

    container.appendChild(label);

    return container;
}

function createTd(msg, tr, center, clickFunc, clickOpt, tdStyle) {
    var cell = document.createElement("td");
    var txt = buildEle("span", {
        "innerText": msg
    });

    if (tdStyle) {
        for (var item in tdStyle) {
            if (item == "width") {
                cell.width = tdStyle[item];
            }

            if (item in cell.style) {
                cell.style[item] = tdStyle[item];
            }
        }
    }

    if (clickFunc) {
        txt.onclick = clickFunc;
        txt.className = "pointer";

        if (clickOpt) {
            for (var item in clickOpt) {
                txt[item] = clickOpt[item];
            }
        }
    }

    cell.appendChild(txt);
    cell.className = "text-center";
    cell.style.padding = "5px";
    if (center) {
        cell.style.textAlign = "center";
    }
    cell.style.border = "1px solid black";

    tr.appendChild(cell);
    return tr;
}

function _GameRecord() { }
_GameRecord.prototype = {
    GID: 0,
    GName: "",
    Record: [],
    Column: [],
    Reset: function () {
        this.GID = 0;
        this.GName = "";
        this.Record = [];
        this.Column = [];

        return this;
    }
}

function _GameRecordFreePool() { }
_GameRecordFreePool.prototype = {
    GameRecord:{
        _FreePool: [],
        Get: function () {
            if (this._FreePool.length == 0) {
                return new _GameRecord();
            } else {
                return this._FreePool.pop();
            }
        },
        Add: function (obj) {
            obj.Reset();
            this._FreePool.push(obj);
        }
    },
    UserTotal:{
        _FreePool: [],
        Get: function (UID, Name) {
            if (this._FreePool.length == 0) {
                return new UserTotal(UID, Name);
            } else {
                var _tmp = this._FreePool.pop();
                _tmp.SetUserID(UID, Name);
                return _tmp;
            }
        },
        Add: function (obj) {
            obj.Reset();
            this._FreePool.push(obj);
        }
    }    
}

var GameRecordPack = new _GameRecordFreePool();

function UserTotal(UID, Name) {
    this.UserID = UID;
    this.Name = Name;
    this.GameRecord = [];
}

UserTotal.prototype = {
    //UserID: 0,
    Reset:function(){
        this.UserID = 0;
        this.Name = "";
        this.GameRecord = [];
    },
    SetUserID:function(UID, Name){
        this.UserID = UID;
        this.Name = Name;
    },
    Add: function (obj, GID, GName, Column) {
        var gamepack = GameRecordPack.GameRecord.Get();
        gamepack.GID = GID;
        gamepack.GName = GName;
        gamepack.Column = Column;
        gamepack.Record = obj;

        this.GameRecord.push(gamepack);
        delete gamepack;

        return this;
    },    
    GetTotal: function () {
        var _record;
        if (this.GameRecord.length > 0) {
            _record = zeroArr(this.GameRecord[0].Column.length);
        }

        for (var i = 0; i < this.GameRecord.length; i++) {
            for (var j = 0; j < this.GameRecord[i].Record.length; j++) {
                if (this.GameRecord[0].Column[j] == RightView.Data.KeyColumn)
                    _record[j] = Number(this.GameRecord[i].Record[j]);
                else
                    _record[j] = isNaN(this.GameRecord[i].Record[j]) ? this.GameRecord[i].Record[j] : _record[j] + Number(this.GameRecord[i].Record[j]);
            }
        }

        return _record;
    },
    GetColumn: function () {
        return this.GameRecord.length == 0 ? [] : this.GameRecord[0].Column;
    },
    GetGameList: function () {
        var _record = [], _column = [], _tmpRecored, _tmpCol;
        
        for (var i = 0; i < this.GameRecord.length; i++) {
            _tmpCol = this.GameRecord[i].Column;
            _tmpCol.push("GID");
            _tmpCol.push("GName");

            _column.push(_tmpCol);

            _tmpRecored = this.GameRecord[i].Record;
            _tmpRecored.push(this.GameRecord[i].GID);
            _tmpRecored.push(this.GameRecord[i].GName);

            _record.push(_tmpRecored);
        }

        return { Column: _column, Record: _record };
    }
};

(function LanguagePack() {
    function lan() { }
    lan.prototype = {
        UpdateLanguagePack: function (pack) {
            CopyProps(this, pack);
        },
        txt_applying: "套用變更中...",
        txt_edit_success: "修改成功!",
        txt_edit_failed: "修改失敗!",
        confirm_changes: "確認修改",
        Menu_enterReport: "進入報表頁",
        Title: "會員分紅及遊戲報表系統",
        btn_Confirm: "確認",
        btn_Return: "返回",
        btn_Edit: "修改",
        btn_Delete: "刪除",
        btn_Add: "新增",
        btn_Search: "查詢",
        btn_AddMember: "新增會員",
        btn_AddAgent: "新增代理",
        RowFirst_Member:"會員",
        AddEditPanelTitle: "子公司(會員)",
        AlertMsg: ["欄位必填", "密碼輸入不一致"],
        Game_25:null,
        Column_AddEdit: [            
            "姓名",
            "分紅%數",
            "水錢%數",
            "地址",
            "電話",
            "IP網段",
            "信箱",
            "銀行帳戶",
            "帳號",
            "密碼",
            "確認密碼",
            "是否可看下一層公司"
        ],        
        Column_Records_Layer1: [
            "代理商:公司",
            "總押注",
            "總贏分",
            "總計(總押-總贏)",
            "總水錢",
            "代理商紅利",
            "公司紅利"
        ],
        Column_Records_Layer2: [
            "玩家名稱",
            "總押注",
            "總贏分",
            "總計(總押-總贏)",
            "總水錢",
            "代理商紅利",
            "公司紅利"
        ],
        Column_Records_Layer3: [
            "遊戲名稱",
            "玩家名稱",            
            "總押注",
            "總贏分",
            "總計(總押-總贏)",
            "總水錢",
            "代理商紅利",
            "公司紅利"
        ],
        Column_Manage: "管理",
        Set: function (obj) {
            for (var i = 0; i < obj.length; i++) {
                if (obj[i].length != 2)
                    continue;

                if (obj[i][0] in this) {
                    if (obj[i][0].includes("Game_")) {
                        var data = DataSheet[obj[i][0]];
                        
                        if (data && data._col) {
                            var game_lang = obj[i][1].split(';');

                            for (var j = 0; j < data._col.length; j++) {
                                var tmp = game_lang[j].split(':');
                                var tmp_col = tmp[0].split(',');

                                if (tmp.length == 2) {
                                    DataSheet[obj[i][0]]._Record[data._col[j]].addText = tmp[1].split(',');
                                }

                                DataSheet[obj[i][0]]._Record[data._col[j]].col = Array.isArray(tmp_col) ? tmp_col : tmp[0];                                
                            }
                        }

                        continue;
                    }

                    if (Array.isArray(this[obj[i][0]]))
                        this[obj[i][0]] = obj[i][1].split(';');
                    else
                        this[obj[i][0]] = obj[i][1];
                }
            }

            return this;
        }
    }

    window.Language = new lan();
}());

var DataSheet = {
    _obj: {},
    GameSetting:"",
    Game_25:{
        _Record: {
            name: {
                col:"廳別",
                Value: "",
                Center: true,
                Set: function (val) {
                    if (val == null)
                        return;
                    this.Value = val;
                }
            },
            time: {
                col: "結算時間",
                Value: "",
                Set: function (val) {
                    if (val) {
                        //var date = new Date(ParseDate(val));
                        val = val.Year + '/' + val.Month + '/' + val.Day + " " + val.Hour + ":" + val.Minute;
                        this.Value = val;
                    } else
                        this.Value = "";

                }
            },
            round: {
                col: "回合",
                Value: "",
                Center: true,
                Set: function (val) {
                    if (val == null)
                        return;

                    this.Value = val;
                }
            },
            pokers: {
                col: "發牌結果",
                addText: ["閒", "莊"],
                Value: "",
                Set: function (val) {
                    if (val == null)
                        return;

                    var result = val.split(",");
                    var _p = [], _b = [], _point, _poker, _r;
                    for (var i = 0; i < result.length; i++) {
                        if (result[i] == "0") {
                            continue;
                        }

                        _poker = DataSheet[DataSheet.GameSetting].Get.Poker.Suit(result[i]);
                        _point = DataSheet[DataSheet.GameSetting].Get.Poker.Point(result[i]);

                        _r = DataSheet[DataSheet.GameSetting].eNum.Suit[_poker - 1] + DataSheet[DataSheet.GameSetting].eNum.Poker[_point - 1];
                        i % 2 ? _b.push(_r) : _p.push(_r);
                    }

                    this.Value = this.addText[0] + ":" + _p.join(",") + "<br/>" + this.addText[1] + ":" + _b.join(",");
                }
            },
            totalwin: {
                col: "總贏分",
                Value: "",
                Center: true,
                Set: function (val) {
                    if (val == null)
                        return;

                    this.Value = val;
                }
            },
            refund: {
                col: "退水",
                Value: "",
                Center: true,
                Set: function (val) {
                    if (val == null)
                        return;

                    this.Value = val;
                }
            },
            bets: {
                col: ["押閒", "押莊", "押和", "押閒對", "押莊對"],
                Value: [],
                Center: true,
                Set: function (val) {
                    if (val == null)
                        return;

                    this.Value = [];
                    var tmp = val.split(",");

                    for (var i = 0; i < tmp.length; i++) {
                        this.Value.push(tmp[i]);
                    }
                }
            },
            points: {
                col: "發牌點數",
                addText: ["閒", "莊"],
                Value: "",
                Center: true,
                Set: function (val) {
                    if (val == null)
                        return;

                    var result = val.split(":");

                    if (result.length == 2)
                        this.Value = this.addText[0] + result[0] + " : " + this.addText[1] + result[1];
                }
            },
            win: {
                col: "本局結果",
                addText: ["閒贏", "莊贏", "和"],
                Value: "",
                Center: true,
                Set: function (val) {
                    if (val == null)
                        return;

                    switch (val) {
                        case 0:
                            this.Value = this.addText[0];
                            break;
                        case 1:
                            this.Value = this.addText[1];
                            break;
                        case 2:
                            this.Value = this.addText[2];
                            break;
                        default:
                            this.Value = "";
                    }

                }
            }
        },
        //_col: ["廳別", "結算時間", "回合", "發牌結果", "發牌點數", "本局結果", "總贏分", "退水", "押閒", "押莊", "押和", "押閒對", "押莊對"],
        _col: ["name", "time", "round", "points", "pokers", "win", "totalwin", "refund", "bets"],
        _width: [200, 150, 150, 180, 220, 220, 220, 220, 250],
        Get: {
            Poker: {
                //花色index轉換
                Suit: function (index) {
                    index = typeof index == "string" ? Number(index) : index;

                    return (Math.floor((index - 1) / 13) + 1);
                },
                //點數index轉換
                Point: function (index) {
                    index = typeof index == "string" ? Number(index) : index;

                    var result = index - Math.floor(index / 13) * 13;

                    if (result == 0) {
                        result = 13;
                    }

                    return result;
                },
                //花色點數轉換index
                Index: function (Suit, Point) {
                    return (Suit - 1) * 13 + Point;
                }
            }
        },
        eNum: {
            Poker: ["A", "2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K"],
            Suit: ["♠", "♥", "♦", "♣"]
        }
    },    
    div: null,
    table: null,    
    Initial: function () {
        $(DataSheet.div).empty();
        DataSheet.table = document.createElement("table");
        DataSheet.table.border = 1;
        DataSheet.table.width = (DataSheet[DataSheet.GameSetting]._width.Sum() + 5) + "px";

        $(DataSheet.div).append(DataSheet.table);
    },
    Create: function (obj, GID) {
        var GameSetting = "Game_" + GID;

        DataSheet.GameSetting = GameSetting;

        this.Initial(GameSetting);
        this._obj = obj;        

        var Record = obj.Record;
        var column = obj.Column;

        var row = document.createElement("tr");
        row.style.backgroundColor = "Blue";
        row.style.color = "white";
        row.style.height = "22px";

        var col_index = 0;

        for (var i = 0; i < DataSheet[GameSetting]._col.length; i++) {
            var tmp_col = DataSheet[GameSetting]._Record[DataSheet[GameSetting]._col[i]];
            if (tmp_col && tmp_col.col) {
                if (tmp_col.col instanceof Array) {
                    for (var j = 0; j < tmp_col.col.length; j++) {
                        DataSheet.insert(GameSetting, row, tmp_col.col[j], col_index, true);
                        col_index++;
                    }
                } else {
                    DataSheet.insert(GameSetting, row, tmp_col.col, col_index, true);
                    col_index++;
                }
            }

        }

        DataSheet.table.appendChild(row);

        for (var i = 0; i < Record.length; i++) {
            if (Record[i] == null)
                continue;

            row = document.createElement("tr");

            for (var j = 0; j < column.length; j++) {
                DataSheet[GameSetting]._Record[column[j]].Set(Record[i][j]);

                DataSheet.insert(GameSetting, row, DataSheet[GameSetting]._Record[column[j]].Value, j, DataSheet[GameSetting]._Record[column[j]].Center);
            }

            DataSheet.table.appendChild(row);
        }

    },
    insert: function (GameSetting, row, content, index, isCenter) {
        var col;
        if (content instanceof Array) {
            for (var i = 0; i < content.length; i++) {
                DataSheet.insert(GameSetting, row, content[i], index, isCenter);
            }
        } else {
            col = document.createElement("td");
            col.width = DataSheet[GameSetting]._width[index] + "px";
            col.style.padding = "5px";
            col.innerHTML = content;

            isCenter ? col.align = "center" : col.align = "left";

            row.appendChild(col);
        }

        return row;
    }
}

var _AddEditPack = function () { };
_AddEditPack.prototype = {
    ColumnTitle: Language.Column_AddEdit,
    Title: {},
    BtnList: {},
    EditID:0,
    Mode: Modes.VIEW,
    Role:0,
    _errorMsg: null,
    tmp_input:[],
    Type: [
        "text",
        "text",
        "text",
        "text",
        "text",
        "text",
        "email",
        "text",
        "text",
        "password",
        "password",
        "radio"
    ],
    ID: [
        "txtName",
        "txtBonus",
        "txtRefund",
        "txtAddress",
        "txtPhone",
        "txtIP",
        "txtEmail",
        "txtBankAccount",
        "txtAccount",
        "txtPassword",
        "txtRePassword",
        "radioCanSeeNext"
    ],
    SetMode: function (Mode) {
        this.ColumnTitle = Language.Column_AddEdit;

        this.Mode = Mode;
        var txt = this.Mode == Modes.ADD ? Language.btn_Add : Language.btn_Edit;

        if (this.BtnList.Confirm && this.BtnList.Confirm.Btn) {
            this.BtnList.Confirm.Btn.value = txt;
        }

        if (this.Title)
            this.Title.innerText = Language.AddEditPanelTitle + txt;

    },
    SetError:function(msg){
        this._errorMsg.innerText = msg;
    },
    Set: function (obj) {
        if (obj.Record.length == 0)
            return;

        var _column = obj.Column, _record = obj.Record[0], colIndex, input_ele;

        var col = [
            "Name",
            "ShopBonus",
            "Refund",
            "ShopAddress",
            "ShopPhone",
            "IP",
            "Email",
            "BankAccount",
            "KeyString",
            "pa",
            "repa",
            "CanSeeNext"
        ];

        for (var i = 0; i < col.length; i++) {
            colIndex = _column.indexOf(col[i]);

            if (colIndex == -1) {
                continue;
            }

            if (this.Type[i] == "radio") {
                $("input[name=" + this.ID[i] + "][value=" + _record[colIndex] + "]").prop('checked', true);
            } else {
                input_ele = document.getElementById(this.ID[i]);
                if (input_ele) {
                    input_ele.value = _record[colIndex];
                }
            }
        }

        LoadingPanel.Hide();
    },
    Create: function (panel) {
        var div;
        this.Title = buildEle("span");
        div = buildEle("div", {
            style: { "margin-bottom": "10px", "font-size": "18px", "text-align": "center" }
        });
        div.appendChild(this.Title);
        panel.appendChild(div);

        var tbl = buildEle("table", { style: { "width": "100%" } });
        var row, td_ele;
        for (var i = 0; i < this.ColumnTitle.length - 1; i++) {
            row = buildEle("tr");
            createTd(this.ColumnTitle[i], row, false, null, null, { "width": "150px" });

            td_ele = buildEle("td");
            td_ele.appendChild(buildEle("input", {
                id: this.ID[i],
                type: this.Type[i],
                className: "txtwidth"
            }));

            row.appendChild(td_ele);

            tbl.appendChild(row);
        }

        row = buildEle("tr", {id:"row_canSeeNext"});
        
        createTd(this.ColumnTitle[this.ColumnTitle.length - 1], row, false);

        td_ele = buildEle("td");

        var ra = createRadioElement("Yes", "1", this.ID[this.ColumnTitle.length - 1], false);
        td_ele.appendChild(ra);

        ra = createRadioElement("No", "0", this.ID[this.ColumnTitle.length - 1], true);
        td_ele.appendChild(ra);

        row.appendChild(td_ele);

        tbl.appendChild(row);
        

        /*button*/
        row = buildEle("tr");
        td_ele = buildEle("td", { style: { "text-align": "right", "padding-top": "5px", "padding-bottom": "5px" } });
        td_ele.colSpan = "2";

        this._errorMsg = buildEle("span", { style: { "color": "red" } });
        td_ele.appendChild(this._errorMsg);

        var _btn = buildEle("input", { type: "button", value: Language.btn_Edit });
        _btn.onclick = function () { Control.RightView.AddEdit(); };
        this.BtnList["Confirm"] = { Lan: "btn_Edit", Btn: _btn };
        
        _btn = buildEle("input", { type: "button", value: Language.btn_Return });
        _btn.onclick = function () { AddEditPack.SetError(""); RightView.Show.Data(); };

        this.BtnList["Return"] = { Lan: "btn_Return", Btn: _btn };

        for (var item in this.BtnList) {
            if (this.BtnList[item]["Btn"]) {
                td_ele.appendChild(this.BtnList[item]["Btn"]);
            }
        }
                
        row.appendChild(td_ele);
        tbl.appendChild(row);
        panel.appendChild(tbl);

        return panel;
    },
    InitInput: function () {
        var input_ele;
        for (var i = 0; i < this.ID.length; i++) {
            input_ele = document.getElementById(this.ID[i]);
            if (input_ele) {
                input_ele.value = "";
            }
        }
    },
    GetInputValue:function(){
        var ele_input;
        for (var i = 0; i < this.ColumnTitle.length; i++) {
            ele_input = document.getElementById(this.ID[i]);
            if (ele_input)
                this.tmp_input[i] = ele_input.value;
        }

        return this;
    },
    SetInputValue: function () {
        var ele_input;
        for (var i = 0; i < this.ColumnTitle.length; i++) {
            ele_input = document.getElementById(this.ID[i]);
            if (ele_input && this.tmp_input[i])
                ele_input.value = this.tmp_input[i];
        }
    },
    Refresh: function (panel) {        
        this.ColumnTitle = Language.Column_AddEdit;
        
        $(panel).empty();
        this.Create(panel);
    }
}

var AddEditPack = new _AddEditPack();

var LoadingPanel = function () {
    var self = {};
    var acc = "", pwd = "", errorMsgId = "errorMsg";
    var ele = Object.create(null);


    ele.div_pop = buildEle("div", {
        id: "pop_window",
        className: "pop_window"
    });

    //ele.div_pop.appendChild(
    //    ele.bg = buildEle("div", {
    //        className: "pop_window_bg"
    //    }));

    ele.div_call = buildEle("div", {
        style: {
            "color": "black",
            "z-index": "100",
            "position": "fixed",
            "top": "0",
            "bottom": "0",
            "left": "0",
            "right": "0"
        }
    });
            
    var txt = buildEle("span", {
        innerText: "Loading...",
        style: {
            "z-index":"101",
            "font-weight":"bolder",
            "font-size": "50px",
            "position": "fixed",
            "top": "50%",
            "left":"50%",
            "margin-top": "-30px",
            "margin-left":"-100px"
        }
    });
    ele.div_call.appendChild(txt);

    ele.div_pop.appendChild(
        ele.div_call
    );

    self.Init = function () {
        $("body").append(ele.div_pop);
        self.Hide();
    }
    self.Show = function () {        
        $(ele.div_pop).show();
    }
    self.Hide = function () {
        $(ele.div_pop).hide();
    }
    
    return self;
}();

var Login = function () {
    var self = {};
    var acc = "", pwd = "", errorMsgId = "errorMsg";
    var ele = Object.create(null);


    ele.div_pop = buildEle("div", {
        id: "pop_window",
        className: "pop_window",
        style: {"display":"none"}
    });

    ele.div_pop.appendChild(
        ele.bg = buildEle("div", {
            className: "pop_window_bg"
        }));

    ele.div_pop.appendChild(
        ele.div_call = buildEle("div", {
            className: "confirm_call"
        }));

    ele.div_call.appendChild(
        ele.div_title = buildEle("div", {
            className: "confirm_title"
        }));
    ele.div_title.appendChild(DeepCopyProps.call(document.createElement("span"), {
        innerHTML: "Login"
    }));

    ele.div_call.appendChild(
        ele.div_inner = buildEle("div", {
            style: { padding: "5%" }
        }));

    ele.div_inner.appendChild(
        ele.input_acc = buildEle("input", {
            id: "account",
            type: "text",
            placeholder: "Account",
            className: "form-control",
            style: { width: "100%" }
        }));

    ele.div_inner.appendChild(
        ele.input_pwd = buildEle("input", {
            id: "pa",
            type: "password",
            placeholder: "Password",
            className: "form-control",
            style: { width: "100%", "margin-top": "5%" }
        }));

    ele.div_inner.appendChild(
        ele.error = buildEle("div", {
            id: errorMsgId
        }));

    ele.div_inner.appendChild(
        ele.div_func = buildEle("div", {
            style: {
                "text-align": "center", "margin-top": "5%"
            }
        }));

    ele.div_func.appendChild(
        ele.btn_login = buildEle("input", {
            id: "yesButton",
            type: "button",
            value: "Login",
            className: "confirmbtn btn",
            onclick: function Login() {
                acc = ele.input_acc.value;
                pwd = ele.input_pwd.value;
                if (checkLogin())
                    ws.Send("login", { acc: acc, pwd: pwd });
            }
        }));
    function checkLogin() {
        $(ele.error).empty();
        var msg = "";
        if (acc == "")
            msg = "Invalid account";
        else if (pwd == "")
            msg = "Invalid password";

        if (msg.length > 0) {
            $(ele.error).append(buildEle("span", { innerHTML: msg, style: { color: "red" } }));
            return false;
        } else
            return true;
    }

    self.Init = function () {
        $("body").append(ele.div_pop);
    }
    self.Show = function () {
        ele.input_acc.value = acc = "";
        ele.input_pwd.value = pwd = "";
        $(ele.error).empty();
        $(ele.div_pop).show();
    }
    self.Hide = function () {
        $(ele.div_pop).hide();
    }
    Object.defineProperties(self, {
        ErrorMsgId: { get: function () { return errorMsgId; } },
        Account: { get: function () { return acc; } },
        Password: { get: function () { return pwd; } }
    })
    return self;
}();

var ReportTitle = function () {
    var self = {}, height = 50;
    var ele = Object.create(null);
    
    ele.div_top = buildEle("div", {
        style: {
            "display":"none",
            "height": height + "px",
            "line-height": height + "px",
            "text-align": "center",
            "vertical-align": "middle",
            "font-size": "30px",
            "border-bottom":"1px solid "+Reporter.BorderColor
        }
    });

    ele.div_top_title = buildEle("span", {
        innerText: Language.Title
    });

    ele.div_top.appendChild(ele.div_top_title);

    //Create array of options to be added
    var array = ["TW", "EN"];    
    var check_language = buildEle("select", { style: {"position":"absolute","right":"0"}});

    //Create and append the options
    for (var i = 0; i < array.length; i++) {
        var option = document.createElement("option");
        option.value = array[i];
        option.text = array[i];
        check_language.appendChild(option);
    }

    check_language.onchange = function () {
        LoadingPanel.Show();
        RightView.Data.MemberList.InsertFirst = true;
        ws.Send("request.language", { GID:Reporter.GID,lan: check_language.value });
    }

    ele.div_top.appendChild(check_language);

    self.Refresh = function () {
        ele.div_top_title.innerText = Language.Title;
    }

    self.Init = function () {
        $("body").append(ele.div_top);
    }
    self.Show = function () {
        $(ele.div_top).show();
    }
    self.Hide = function () {
        $(ele.div_top).hide();
    }

    Object.defineProperties(self, {
        Height: { get: function () { return height; } }
    })

    return self;
}();

//主頁
var MainPage = {};
MainPage.Init = function () {
    var self = {};
    
    self.Div = buildEle("div", {
        id: "mainPageDiv",
        "className": "adjustHeight",
        style: { display: "none", width: "20%", "border-right": "1px solid " + Reporter.BorderColor }
    });
    
    
    var div = buildEle("div", {
        style: { "text-align": "center", "padding-top": "10px", "font-size": "15px", "color": "blue" }
    });

    self.menu0 = buildEle("span", {
        id: "menu0",
        innerText: Language.Menu_enterReport,
        className:"pointer"
    });

    self.menu0.onclick = function () {
        LoadingPanel.Show();        
        ws.Send("enter_report", { GID:Reporter.GID });
    }

    div.appendChild(self.menu0);
    self.Div.appendChild(div);

    self.Refresh = function () {
        self.menu0.innerText = Language.Menu_enterReport        
    }

    self.Show = function () {
        $(self.Div).show();
    }
    self.Hide = function () {
        $(self.Div).hide();
    }

    $("body").append(self.Div);
    MainPage = self;
};
