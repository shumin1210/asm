﻿var now_url;
var now_collect_name;

//Function to make AJAX call to the Web Method
//參數:(此ajax集合的名稱)
function GetData(dataCollection) {
    var $res;
    
    var datastr = JSON.stringify(ajaxData[dataCollection].paraments);

    //now_url = ajaxData[dataCollection].url;
    //now_collect_name = dataCollection;

    $.ajax({
        type: "POST",
        url: ajaxData[dataCollection].url,
        data: datastr,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg, jqxhr, settings) {
            var fun = ajaxData[dataCollection].successFunc;

            if (typeof window[fun] == 'function') {
                //ajaxData[dataCollection].returnParament = JSON.parse(settings.data);

                //var jsonObj = JSON.parse(msg);// 將JSON格式資料轉為物件
                var jsonObj = msg;
                if (jsonObj.hasOwnProperty("d")) {
                    $res = jsonObj.d;
                }
                else
                    $res = jsonObj;
                var data = $.parseJSON(jsonObj.d);

                window[fun](data, ajaxData[dataCollection].paraments);
            }
        },        
        error: function (xhr, ajaxOptions, thrownError) {
            //alert(xhr.status);
            //alert(thrownError);
        }

    });
}



