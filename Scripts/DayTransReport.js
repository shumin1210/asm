﻿/// <reference path="_reference.js" />


var Day_Trans_Report_View = function () {
    var self = {};
    var ele = {}, Data, _id = "Day_Trans_Report_View";
    var border_color = Reporter.BorderColor;
    var check_list = [];
    var margin = 3, status_padding = 5;
    var pageIsInitial=false;

    self.PerPageCount = 10;
    ele.Div = buildEle("div", {
        id: _id + "_DIV",
        style: {
            "display": "none",
            "width": "84%",
            "padding-left": "1%"
        }
    });

    self.Search = {
        StartTime: "",
        EndTime: "",
        PageIndex: [1, 1, 1, 1],
        PageCount: [],
        Id: [1, 1, 1, 1]
    }

    self.searchDiv = buildEle("div", { style: { "margin-top": "10px" } });
    self.SelectObj = CreateView.Date.Calendar.Control(CreateView.Date.Type.Date);
    self.searchTimeDiv = self.SelectObj.Container;

    self.searchDiv.appendChild(self.searchTimeDiv);
    ele.Div.appendChild(self.searchDiv);
    
    self.contenterDiv = buildEle("div", { style: { "margin-top": "10px" } });
    ele.Div.appendChild(self.contenterDiv);
    
    self.ListDiv = buildEle("div", { });
    self.contenterDiv.appendChild(self.ListDiv);
    self.Table = new WebTool.Table(self.ListDiv);
    self.Table.style.Table = {
        "border": "1px solid " + border_color
    };
    self.Table.Title = [    
    {
        title: "建立日期",
        column: "CreateDate",
        //width: "120px",
        datatype: "datetime",
        sort: {
            type: "date",
            desc: true
        }
    },
    {
        title: "開始時間",
        column: "start",
        //width: "120px",
        datatype: "date"
    },
    {
        title: "結束時間",
        column: "end",
        //width: "120px",
        datatype: "date"
    },
    {
        title: "",
        column: "FileName",
        //width: "70px",
        type:"btn"
    }
    ];

    self.Table.PerTitle.SetDataView = function () {
        var td = self.Table.PerTitle.Td;

        td.style.border = "1px solid " + border_color;
        td.style.textAlign = "center";
        td.style.backgroundColor = "Blue";
        td.style.color = "white";
        td.style.padding = "5px";

    }

    self.Table.PerCell.SetDataView = function () {
        var td = self.Table.PerCell.Td;
        var data = self.Table.Data;
        var text = self.Table.PerCell.Text;
        var titleObj = self.Table.PerCell.TitleObj;

        if (titleObj.column == "FileName") {
            self.Table.PerCell.Td = td = WebTool.CreateElement("td");
            var btn = WebTool.CreateElement("a", { innerText: Language.btn_Download });
            $(btn).bind("click", { msg: text }, function (evt) {
                Control.OpenPDF(evt.data.msg, 0);
            });
            btn.style.cursor = "pointer";
            btn.style.color = "red";
            td.appendChild(btn);

            self.Table.PerCell.Text = self.Table.PerCell.ToolsTipText = "";
        }

        td.style.border = "1px solid "+ border_color;
        td.style.padding = "1px 5px";
        td.style.overflow = "hidden";
        td.style.whiteSpace = "nowrap";
        
        td.style.textAlign = "center";
                
        /*date format*/
        if (titleObj.datatype == "date" && !isNaN(Date.parse(ParseDate(text)))) {
            self.Table.PerCell.Text = self.Table.PerCell.ToolsTipText = text = ParseDate(text).format("yyyy-MM-dd");
        } else if (titleObj.datatype == "datetime" && !isNaN(Date.parse(ParseDate(text)))) {
            self.Table.PerCell.Text = self.Table.PerCell.ToolsTipText = text = ParseDate(text).format("yyyy-MM-dd HH:mm:ss");
        }
                
        return self.Table;
    }

    self.PageDiv = buildEle("div", { });
    //self.contenterDiv.appendChild(self.PageDiv);
    self.Page = new WebTool.Page();

    function pageChange(index) {
        if (!Day_Trans_Report_View.Page.Change(index))
            return false;
        pageSetElement(Day_Trans_Report_View.Page.Count.PageCount, index);

        Control.DayReport.getSearch(pageIsInitial);
        return true;
    }
    function pageSetElement(pageCount, pageIndex) {
        Day_Trans_Report_View.Span_PageCount.innerText = pageCount;
        Day_Trans_Report_View.Text_PageIndex.value = pageIndex;

        Day_Trans_Report_View.Span_prepage.className = "pointer";
        Day_Trans_Report_View.Span_firstpage.className = "pointer";
        Day_Trans_Report_View.Span_nextpage.className = "pointer";
        Day_Trans_Report_View.Span_lastpage.className = "pointer";

        if (pageIndex == 1) {
            Day_Trans_Report_View.Span_prepage.className = "";
            Day_Trans_Report_View.Span_firstpage.className = "";
        } else if (pageIndex == pageCount) {
            Day_Trans_Report_View.Span_nextpage.className = "";
            Day_Trans_Report_View.Span_lastpage.className = "";
        }
    }

    function createPage() {
        Day_Trans_Report_View.Page.DivPage.style.padding = margin + "px";
        Day_Trans_Report_View.Page.Count.PerPageData = self.PerPageCount;
        Day_Trans_Report_View.Page.Config.StyleType = Day_Trans_Report_View.Page.StyleType.Custom;
        Day_Trans_Report_View.Page.CustomControl = function () {
            Day_Trans_Report_View.Page.CustomControl = function () {
                var page = Day_Trans_Report_View.Page;

                pageSetElement(page.Count.PageCount, page.Index());
            }
            var page = Day_Trans_Report_View.Page, obj, page_margin = 10, page_width = 40;
            Day_Trans_Report_View.Span_firstpage = WebTool.CreateElement("span", {
                "innerText": "|<",
                style: {
                    "margin-right": page_margin + "px"
                }
            });
            Day_Trans_Report_View.Span_firstpage.onclick = function () {
                pageChange(1);
            };
            page.DivPage.appendChild(Day_Trans_Report_View.Span_firstpage);

            Day_Trans_Report_View.Span_prepage = WebTool.CreateElement("span", {
                "innerText": "<",
                style: {
                    "margin-right": page_margin + "px"
                }
            });
            Day_Trans_Report_View.Span_prepage.onclick = function () {
                pageChange(Day_Trans_Report_View.Page.Index() - 1);
            }
            page.DivPage.appendChild(Day_Trans_Report_View.Span_prepage);

            Day_Trans_Report_View.Text_PageIndex = WebTool.CreateElement("input", {
                "type": "text",
                "value": Day_Trans_Report_View.Page.Index(),
                style: {
                    "width": page_width + "px"
                }
            });
            Day_Trans_Report_View.Text_PageIndex.onblur = function () {
                if (!pageChange(this.value)) {
                    pageChange(Day_Trans_Report_View.Page.Count.AllData);
                }
            }
            page.DivPage.appendChild(Day_Trans_Report_View.Text_PageIndex);

            page.DivPage.appendChild(WebTool.CreateElement("span", {
                "innerText": "/"
            }));

            Day_Trans_Report_View.Span_PageCount = WebTool.CreateElement("span", {
                "innerText": Day_Trans_Report_View.Page.Count.PageCount,
                style: {
                    "margin-right": page_margin + "px"
                }
            });
            page.DivPage.appendChild(Day_Trans_Report_View.Span_PageCount);

            Day_Trans_Report_View.Span_nextpage = WebTool.CreateElement("span", {
                "innerText": ">",
                style: {
                    "margin-right": page_margin + "px"
                }
            });
            Day_Trans_Report_View.Span_nextpage.onclick = function () {
                pageChange(Day_Trans_Report_View.Page.Index() + 1);
            }
            page.DivPage.appendChild(Day_Trans_Report_View.Span_nextpage);

            Day_Trans_Report_View.Span_lastpage = WebTool.CreateElement("span", {
                "innerText": ">|"
            });
            Day_Trans_Report_View.Span_lastpage.onclick = function () {
                pageChange(Day_Trans_Report_View.Page.Count.PageCount);
            }
            page.DivPage.appendChild(Day_Trans_Report_View.Span_lastpage);

            page.CustomControl();
        }
    }
    ele.searchBtn = WebTool.CreateElement("input", {
        type: "button",
        value: Language.btn_Search
    });
    ele.searchBtn.onclick = function () { Control.DayReport.getSearch(); }

    self.searchDiv.appendChild(ele.searchBtn);

    self.Init = function () {
        self.Table.Init();
        self.tFooter = WebTool.CreateElement("tfoot");
        var tr = WebTool.CreateElement("tr");
        var td = WebTool.CreateElement("td");
        td.colSpan = self.Table.Title.length;
        tr.appendChild(td);
        self.tFooter.appendChild(tr);
        self.Table._tableObj.appendChild(self.tFooter);
        td.appendChild(self.PageDiv);

        createPage();
        self.Page.Init(self.PageDiv, 0);
        RightPage.Append(ele.Div);
        //$("body").append(ele.Div);
    }
    var date = new Date();
    var firstDay = new Date(date.getFullYear(), date.getMonth(), 1);

    self.Show = function () {
        Right_Title.Set(Language.subMenu_dayreportQue);
        self.SelectObj.Input[CreateView.Date.Type.Start].YearMonthDay.value = firstDay.format(Report_Config.DateFormat.Normal);
        
        self.tFooter.style.display = "none";
        pageIsInitial = false;
        Control.DayReport.getSearch(pageIsInitial);
        ele.Div.style.display = "inline-block";
    }
    self.Hide = function () {
        $(ele.Div).hide();
    }

    self.Refresh = function () {
        var lan = {
            TW: ["建立日期", "開始時間", "結束時間", ""],
            EN:["Create Date", "Start Date", "End Date",""]
        }
        for (var i = 0; i < self.Table.Title.length; i++) {
            self.Table.Title[i].title = lan[Reporter.Language][i];
        }
        self.Table.RefreshTitle();

        ele.searchBtn.value = Language.btn_Search;
        
    }

    self.Receive = function (obj) {
        if (!pageIsInitial) {
            pageIsInitial = true;
            Day_Trans_Report_View.Page.Reset(obj.DataCount);
            var pCount = Math.ceil(obj.DataCount / self.PerPageCount);
            pageSetElement(pCount, 1);
            Day_Trans_Report_View.Page.Change(1);
        }

        if (obj.Record.length == 0) {
            self.tFooter.style.display = "none";
        } else
            self.tFooter.style.display = "table-footer-group";
        self.Table.Data = obj;
        self.Table.RefreshContent();                
    }

    Object.defineProperties(self, {
        Data: {
            get: function () { return Data; },
            set: function (val) { Data = val; }
        },
        CheckList: {
            get: function () { return check_list; },
            set: function (val) { check_list = val; }
        },
        game_ws: {
            get: function () { return game_ws; }
        },
        Div:{
            get: function () { return ele.Div;}
        }
    })

    return self;
}();