﻿/// <reference path="../jquery/jquery-1.12.3.min.js" />
/// <reference path="../jquery/Tools.js" />
/// <reference path="connect.js" />
/// <reference path="view.js" />

var url = "wss://" + _game_server + "/webHandler.ashx";
var ws = new Tools.Connection(url);

var Layouts = Tools.Enumify("NONE", "LOGIN", "MAINPAGE",
    "GAMELIST", "LAYER1", "LAYER2", "LAYER3", "LAYER4",
    "QRVIEW", "DAYTRANSREPORT", "TRANS_CONFIG", "MEMBERMANAGE",
    "ONLINE", "CHECKOUT", "CHECKOUTHISTORY", "STOCKHOLDERMANAGE","AUTOCHECK","ABC", "GAMEREFUNDLIST");

var DataType = Tools.Enumify("NONE",
    "GAMELIST", "TRANSACTION",
    "MEMBERMANAGE", "STOCKHOLDERMANAGE", "GAMEREFUNDLIST");

var Modes = Tools.Enumify("VIEW", "ADD", "EDIT");

var PopType = Tools.Enumify("CASHIN", "CASHOUT", "SETCREDIT");

var Layout = Layouts.NONE;

var Report_Config = {
    DateFormat: {
        Normal: "yy-MM-dd",
        DatePicker: "yy-mm-dd",
        DateTime:"yyyy-MM-dd HH:mm:ss"
    }
}

function getAttr(evt, attr) {
    return evt.currentTarget[attr];
}

Date.prototype.addHours = function (h) {
    this.setTime(this.getTime() + (h * 60 * 60 * 1000));
    return this;
}

function getIndex(column, key) {
    var index = -1;
    for (var i = 0; i < column.length; i++) {
        if (column[i] == key) {
            index = i;
            break;
        }
    }

    return index;
}

function getNow() {
    return new Date().format(Report_Config.DateFormat.DateTime);
}

var pageClick = false;

var FontSizeSetting = {
    ReporterTitle: 23,
    Loading: 48,
    AddEditPanel: 17,
    LeftView: 27,
    RightView: {
        TW: 18,
        EN: 12,
        Max: 22,
        Min: 8
    },
    DayTransReport: 18
}

var ColorSetting = {
    TableTitle: {
        BG: "#2A43A4",
        Font: "white",
        Line: "#B3C1D5",
        Content: "#4D4D4D"
    },
    LeftMenu: "white",
    Btn: {
        AddEditDel: "#DC4040",
        Ban: "#DC4040",
        ReRight: "#269500",
        CashIn: "#269500",
        CashOut: "#3DACDD",
        AddAgentMember: "#3DACDD"
    },
    Column: {
        RowOne: "red",
        Status: "red"
    },
    Page: "red",
    ErrorMsg: "red"
}

var _Report = function () { }

_Report.prototype = {
    GID: "",
    Language: "TW",
    BorderColor: "gray",
    ID: 0,
    Name: "",
    Token: "",
    Role: 0,
    Permission: 0,
    MaxLevel: 0,
    Level: 0,
    DataType: DataType.NONE,
    Title: "",
    isAdmin: false
}

var Reporter = new _Report();

var _TreeNode = function () { }

_TreeNode.prototype = {
    ID: 0,
    UpLevelID: 0,
    Level: 0,
    Text: "",
    TabWidth: 20,
    State: 0,
    _GroupName: "",
    _Clickabled: false,
    ele_txt: null,
    Set: function (recordID, uplevel, lv, GroupName) {
        this.ID = recordID;
        this.UpLevelID = uplevel;
        this.Level = lv;

        this._GroupName = GroupName ? GroupName : "";

        if (recordID == Reporter.ID) {
            this.SetState(0);
            this.Disabled();
            return this;
        } else if (uplevel == recordID) {
            this.SetState(1);
        } else {
            this.SetState(2);
        }

        this.Enabled();

        return this;
    },
    Disabled: function () {
        this._Clickabled = false;
    },
    Enabled: function () {
        this._Clickabled = true;
    },
    SetState: function (State) {
        this.State = State;

        switch (State) {
            case 0:
                this.Text = "";
                break
            case 1:
                this.Text = "-";
                break
            case 2:
                this.Text = "+";
                break
            default:
        }

        if (this.ele_txt) {
            this.ele_txt.innerText = this.Text;
        }

        return this;
    },
    CreateTD: function () {
        var cell = document.createElement("td");
        cell.style.padding = "5px";
        cell.style.border = "1px solid " + ColorSetting.TableTitle.Line;

        var txt = buildEle("span", {
            "innerText": this.Text, style: { "color": "red" }
        });

        txt.onclick = this.Click;
        txt.treePack = this;

        if (this._Clickabled)
            txt.className = "pointer";

        txt.style.marginLeft = (this.Level * this.TabWidth) + "px";

        this.ele_txt = txt;

        cell.appendChild(txt);

        return cell;
    },
    Click: function (evt) {
        var _treepack = evt.currentTarget.treePack;

        if (!_treepack._Clickabled) return;

        //取資料
        if (_treepack.State == 2)
            Control.RightView.Layer1.TreeClick(_treepack);
        else {
            /*根據Tree Class 取ID 移除下層*/
            var tmp = $("." + getTreeNodeName(_treepack.ID));
            while (!Test.Undefined(tmp)) {
                tmp.remove();
                if (tmp.length == 0)
                    break;
                tmp = $("." + tmp[0].id);
            }
        }

        _treepack.SetState(_treepack.State == 1 ? 2 : 1);
    }
}

function getTreeNodeName(ID) {
    return "tr_" + Layout + "_" + ID;
}

function Init() {
    Init = function () {

    }

    ReportTitle.Init();
    Login.Init();
    MainPage.Init();
    LeftView.Init();
    LeftView_Trans.Init();
    RightPage.Init();
    Right_Title.Init();
    RightView.Init();
    LoadingPanel.Init();
    PopMsgView.Init();

    Day_Trans_Report_View.Init();
    CheckOutHistoryView.Init();
    EditPasswordView.Init();
    AutoCheckView.Init();

    //RightView.Select.Initial();
    //CreateView.Date.Initial(CreateView.Date.Type.Date, Day_Trans_Report_View.SelectObj, Day_Trans_Report_View.Search);

    CreateView.Date.Calendar.Init(CreateView.Date.Type.DateTime, RightView.SelectObj, RightView.Search);
    CreateView.Date.Calendar.Init(CreateView.Date.Type.Date, Day_Trans_Report_View.SelectObj, Day_Trans_Report_View.Search);
    CreateView.Date.Calendar.Init(CreateView.Date.Type.DateTime, CheckOutHistoryView.SelectObj, CheckOutHistoryView.Search);

    CreateView.Date.Calendar.Init(CreateView.Date.Type.DateTime, AutoCheckView.SelectObj, AutoCheckView.Search);

    QRView.Init();
    CashInOutView.Init();
    HomePageView.Init();
    QRView.Hide();
    SetCreditView.Init();
    CashOutView.Init();
    CashInView.Init();
    OnlineView.Init();
    TransSettingView.Init();
    CheckOutView.Init();
    CheckOutSettingView.Init();
    AddShareHolderView.Init();
    ABCView.Init();
}

function buildEle(type, opt) {
    return DeepCopyProps.call(document.createElement(type), opt);
}

function padLeft(length, str, char) {
    str = str.toString();
    if (str.length < length) {
        return padLeft(length, char + str, char);
    } else {
        return str;
    }
}

function createRadioElement(text, value, name, checked) {
    var container = buildEle("div", { style: { "display": "inline-block" } });
    var input = document.createElement('input');
    input.id = name + value;
    input.type = 'radio';
    input.value = value;
    input.style.display = "inline-block";
    input.style.color = "black";
    input.style.width = "15px";

    input.name = name;
    if (checked) {
        input.checked = 'checked';
    }

    container.appendChild(input);

    var label = document.createElement("label");
    label.htmlFor = input.id;
    label.innerText = text;

    container.appendChild(label);

    return container;
}

function createTd(msg, tr, center, number, clickFunc, clickOpt, tdStyle) {
    var cell = document.createElement("td");

    var txt = buildEle("span", {
        "innerHTML": msg
    });

    if (tdStyle) {
        for (var item in tdStyle) {
            //if (item == "width") {
            //    cell.width = tdStyle[item];
            //}

            if (item in cell.style) {
                cell.style[item] = tdStyle[item];
            }
        }
    }

    if (clickFunc) {
        txt.onclick = clickFunc;
        txt.className = "pointer";

        if (clickOpt) {
            for (var item in clickOpt) {
                txt[item] = clickOpt[item];
            }
        }
    }

    cell.appendChild(txt);
    cell.className = "text-center";
    cell.style.padding = "3px";
    if (center) {
        cell.style.textAlign = "center";
    }

    if (number)
        cell.style.textAlign = "right";

    cell.style.border = "1px solid "+ ColorSetting.TableTitle.Line;

    tr.appendChild(cell);
    return tr;
}

function createBtn(value, backcolor) {
    return buildEle("input", {
        type: "button",
        className: "pointer editBtn",
        style: {
            "background-color": backcolor,
            "color": "white",
            "width": "auto",
            "padding": "3px"
        },
        value: value
    });
}

function _GameRecord() { }
_GameRecord.prototype = {
    GID: 0,
    GName: "",
    Record: [],
    Column: [],
    Reset: function () {
        this.GID = 0;
        this.GName = "";
        this.Record = [];
        this.Column = [];

        return this;
    }
}

function _GameRecordFreePool() { }
_GameRecordFreePool.prototype = {
    GameRecord: {
        _FreePool: [],
        Get: function () {
            if (this._FreePool.length == 0) {
                return new _GameRecord();
            } else {
                return this._FreePool.pop();
            }
        },
        Add: function (obj) {
            obj.Reset();
            this._FreePool.push(obj);
        }
    },
    UserTotal: {
        _FreePool: [],
        Get: function (UID, Name) {
            if (this._FreePool.length == 0) {
                return new UserTotal(UID, Name);
            } else {
                var _tmp = this._FreePool.pop();
                _tmp.SetUserID(UID, Name);
                return _tmp;
            }
        },
        Add: function (obj) {
            obj.Reset();
            this._FreePool.push(obj);
        }
    }
}

var GameRecordPack = new _GameRecordFreePool();

function UserTotal(UID, Name) {
    this.UserID = UID;
    this.Name = Name;
    this.GameRecord = [];
}

UserTotal.prototype = {
    //UserID: 0,
    Reset: function () {
        this.UserID = 0;
        this.Name = "";
        this.GameRecord = [];
    },
    SetUserID: function (UID, Name) {
        this.UserID = UID;
        this.Name = Name;
    },
    Add: function (obj, GID, GName, Column) {
        var gamepack = GameRecordPack.GameRecord.Get();
        gamepack.GID = GID;
        gamepack.GName = GName;
        gamepack.Column = Column;
        gamepack.Record = obj;

        this.GameRecord.push(gamepack);
        delete gamepack;

        return this;
    },
    GetTotal: function () {
        var _record;
        if (this.GameRecord.length > 0) {
            _record = zeroArr(this.GameRecord[0].Column.length);
        }

        for (var i = 0; i < this.GameRecord.length; i++) {
            for (var j = 0; j < this.GameRecord[i].Record.length; j++) {

                if (this.GameRecord[0].Column[j] == "Currency") {
                    _record[j] = Number(this.GameRecord[i].Record[j]);
                    continue;
                }

                if (this.GameRecord[0].Column[j] == RightView.Data.KeyColumn)
                    _record[j] = Number(this.GameRecord[i].Record[j]);
                else
                    _record[j] = isNaN(this.GameRecord[i].Record[j]) ? this.GameRecord[i].Record[j] : _record[j] + Number(this.GameRecord[i].Record[j]);
            }
        }

        return _record;
    },
    GetColumn: function () {
        return this.GameRecord.length == 0 ? [] : this.GameRecord[0].Column;
    },
    GetGameList: function () {
        var _record = [], _column = [], _tmpRecored, _tmpCol;

        for (var i = 0; i < this.GameRecord.length; i++) {
            _tmpCol = this.GameRecord[i].Column;
            _tmpCol.push("GID");
            _tmpCol.push("GName");

            _column.push(_tmpCol);

            _tmpRecored = this.GameRecord[i].Record;
            _tmpRecored.push(this.GameRecord[i].GID);
            _tmpRecored.push(this.GameRecord[i].GName);

            _record.push(_tmpRecored);
        }

        return { Column: _column, Record: _record };
    }
};

(function LanguagePack() {
    function lan() { }
    lan.prototype = {
        UpdateLanguagePack: function (pack) {
            CopyProps(this, pack);
        },
        txt_applying: "套用變更中...",
        txt_edit_success: "修改成功!",
        txt_edit_failed: "修改失敗!",
        txt_qrcount: "QRCode 數量:",
        txt_searchUser: "帳號:",
        txt_Status: "狀態",
        txt_HomePageTitle: "帳目資訊",
        txt_LastCheckOutTime: "上次結帳時間:",
        txt_LastCheckOutFinish: ", 結帳:",
        txt_Enabled:"啟用",
        confirm_changes: "確認修改",
        Menu_HomePage: "首頁",
        Menu_enterReport: "遊戲報表頁",
        Menu_transReport: "帳務報表頁",
        Menu_MemberManage: "會員管理頁",
        Menu_StockholderManage: "股東管理頁",
        Menu_Checkout: "帳務結帳頁",
        Menu_CheckoutHistory: "歷史結帳頁",
        Menu_GamerefundList:"遊戲洗水錢頁",
        Menu_reoprtCfg: "結帳參數設定",
        Menu_ABC:"ABC",
        Menu_QRCode: "管理QRCode",
        subMenu_reportQue: "報表查詢",
        subMenu_dayreportQue: "每日報表查詢",
        SearchDefaultText: "查詢時段",
        Title: "會員分紅及遊戲報表系統",
        Title_Online: "各廳押注",
        Title_Message:"訊息",
        CashInOutTitle: "開/洗分",
        CreditTitle: "設定信用",
        OnlineTitle: "在線人數",
        OnlineTableNo: "桌號",
        btn_Confirm: "確認",
        btn_Return: "返回",
        btn_Edit: "修改",
        btn_Delete: "刪除",
        btn_Add: "新增",
        btn_Print: "列印",
        btn_Search: "查詢",
        btn_Download: "下載",
        btn_Save: "儲存",
        btn_Logout: "登出",
        btn_CashIn: "開分",
        btn_CashOut: "洗分",
        btn_Credit: "信用",
        btn_CheckOut: "結帳",
        btn_CheckOutFinish: "結帳完畢",
        btn_CheckOutRelease: "解除結帳",
        btn_AddMember: "新增會員",
        btn_AddAgent: "新增下線",
        btn_AddStockholder: "新增股東",
        btn_ban: "停權",
        btn_reright: "復權",
        btn_ConvertStockholder: "變為股東",
        btn_ConvertMember: "變為會員",
        btn_Update: "更新",
        btn_EditPassword:"修改密碼",
        RowFirst_Member: ["會員群組", "股東群組"],
        Page: ["前10頁", "後10頁"],
        AddEditPanelTitle: ["會員", "子公司", "股東", ""],
        SearchStatus: ["全部", "有設定使用者", "沒設定使用者"],
        CheckOutStatus:["未完畢","已完畢"],
        AlertMsg: ["欄位必填", "密碼輸入不一致"],
        searchRadioList_Trans: ["全部", "現金", "信用"],
        Game_25: null,
        Cell_Currency: [
            "臺幣",
            "人民幣",
            "美金",
            "印尼盾"
        ],
        Column_AddEdit: [
            "名稱",
            "分紅%數",
            "代理商水錢",
            "信用額度",
            "風險限額",
            //"地址",
            //"電話",
            "微信Line",
            //"信箱",
            "銀行帳戶",
            "帳號",
            "密碼",
            "確認密碼",
            //"是否可看下線公司",
            //"貨幣"
        ],
        Column_Records_Layer1: [            
            "名稱:",
            "筆數",
            "總押注",
            "總贏分",
            "盈餘<br/>(總押-總贏)",
            "遊戲水錢",
            "總盈餘<br/>(總押-總贏-遊戲水錢)",
            "代理商水錢",
            "公司紅利<br/>(會員消費(%)+下線紅利(%))",
            "上線紅利<br/>(會員消費+下線紅利)(%)",
            "貨幣"
        ],
        Column_Records_Layer2: [            
            "玩家名稱",
            "筆數",
            "總押注",
            "總贏分",
            "盈餘<br/>(總押-總贏)",
            "遊戲水錢",
            "總盈餘<br/>(總押-總贏-遊戲水錢)",
            //"代理商水錢",
            //"公司紅利<br/>(會員消費(%)+下線紅利(%))",
            //"上線紅利<br/>(會員消費+下線紅利)(%)",
            "貨幣"            
        ],
        Column_Records_Layer3: [
            "遊戲名稱",
            "筆數",
            "玩家名稱",
            "總押注",
            "總贏分",
            "盈餘<br/>(總押-總贏)",
            "遊戲水錢",
            "總盈餘<br/>(總押-總贏-遊戲水錢)",
            //"代理商水錢",            
            //"公司紅利<br/>(會員消費(%)+下線紅利(%))",
            //"上線紅利<br/>(會員消費+下線紅利)(%)"
        ],
        Column_Trans_Records_Layer1: [
            "名稱:",
            "現金消費",
            "信用消費",
            "會員消費<br/>(現金+信用)",
            //"公司紅利<br/>(會員消費(%)+下線紅利(%))",
            //"上線紅利<br/>(會員消費+下線紅利)(%)",
            "貨幣"
        ],
        Column_Trans_Records_Layer2: [
            "玩家名稱",
            "現金消費",
            "信用消費",
            "會員消費<br/>(現金+信用)",
            //"公司紅利<br/>(會員消費(%)+下線紅利(%))",
            //"上線紅利<br/>(會員消費+下線紅利)(%)",
            "貨幣"
        ],
        Column_Trans_Records_Layer3: [
            "玩家名稱",
            "現金消費",
            "信用消費",
            "信用額度",
            "會員消費<br/>(現金+信用)",
            //"公司紅利<br/>(會員消費(%)+下線紅利(%))",
            //"上線紅利<br/>(會員消費+下線紅利)(%)",
            "貨幣"
        ],
        Column_MemberManage_Records_Layer1: [
            "名稱:",
            "遊戲點數",
            "信用餘額",
            "信用額度",
            "遊戲剩餘水錢",
            "最後登入時間",
            "貨幣"
        ],
        Column_MemberManage_Records_Layer2: [
            "玩家名稱",
            "遊戲點數",
            "信用餘額",
            "信用額度",
            "現金開分",
            "遊戲剩餘水錢",
            "最後登入時間",
            "貨幣"
        ],
        Column_MemberManage_Records_Layer3: [
            "交易方式",
            "玩家名稱",
            "遊戲點數",
            "信用餘額",
            "信用額度",
            "貨幣"
        ],
        Column_StockholderManage_Records_Layer1: [
            "名稱:"
        ],
        Column_GameRefundList_Records_Layer1: [
            "名稱:",
            "洗水金額",
            "貨幣"
        ],
        Column_QRList: [
            "No",
            "推薦碼",
            "QRCode圖片",
            "建立日期",
            "使用者",
            ""
        ],
        Column_Status: [
            "正常",
            "停權"
        ],
        Column_HomePage_Title: [
            "帳號 :",
            "暱稱 :",
            "遊戲點數 : ",
            //"未清帳款總額 : ",
            "總公司信用額度",
            "分公司信用額度",
            "總代理信用額度",
            "代理信用額度",
            "會員信用額度",
            "分公司總計",
            "總代理數總計",
            "代理數總計",
            "會員數總計"
        ],
        Column_UpperBonus_Title: [
            "分公司上線紅利",
            "總代理上線紅利",
            "代理上線紅利"
        ],
        Column_Online_Title: [
            "回合編號",
            "押閒",
            "押莊",
            "押和",
            "押閒對",
            "押莊對",
            "押莊六",
            "總押注",
            "累積押注",
            "累積獎金"
        ],
        Column_Online_PlayerList_Title: [
            "帳號",
            "暱稱",
            "押閒",
            "押莊",
            "押和",
            "押閒對",
            "押莊對",
            "押莊六",
            "總押注"
        ],
        Column_CheckOut_Title: [
            "暱稱",
            "信用額度",
            "信用餘額",
            "遊戲點數",
            "現金總帳",
            "遊戲端現金洗分",
            "歸還信用額度",
            "剩餘遊戲水錢",
            "可洗分",
            "會員總結"
        ],
        Column_CheckOut_History_Title: [
            "開始時間",
            "結帳時間",
            "暱稱",
            "信用額度",
            "信用餘額",
            "遊戲點數",
            "現金總帳",
            "遊戲端現金洗分",
            "歸還信用額度",
            "剩餘遊戲水錢",
            "可洗分",
            "會員總結"
        ],
        Column_AutoCheck_Title: [            
            "暱稱",
            "信用額度",
            "信用餘額",
            "遊戲點數",
            "現金總帳",
            "歸還信用額度",
            "總押",
            "總贏",
            "總押-總贏",
            "是否符合"
        ],
        Column_ABC_Title: [
            "流水號",
            "贏家",
            "牌組",
            "廳號",
            "啟用"
        ],
        ABC_Status: [
            "未啟用",
            "啟用中",
            "錄製中",
            "更新中"
        ],
        Column_Manage: "管理",
        Pop_SetCredit_Title: [
            "信用額度",
            "信用餘額",
            "增加",
            "減少"
        ],
        Pop_CashOut_Title: [
            "信用額度",
            "信用餘額",
            "遊戲點數",
            "歸還信用額度",
            "剩餘遊戲水錢",
            "可洗分",
            "會員總結"
        ],
        Pop_CashIn_Title: [
            "現金",
            "信用"
        ],
        Pop_AddShareHolder_Title: [
            "名稱",
            "帳號",
            "密碼",
            "確認密碼"
        ],
        Setting_Menu_Title: [
            "設定日期",
            "設定時間"
        ],
        Setting_Week_Title: [
            "星期一",
            "星期二",
            "星期三",
            "星期四",
            "星期五",
            "星期六",
            "星期日"
        ],
        Setting_Time_Title: [

        ],
        Set: function (obj) {
            for (var i = 0; i < obj.length; i++) {
                if (obj[i].length != 2)
                    continue;

                if (obj[i][0] in this || obj[i][0].includes("Game_")) {
                    if (obj[i][0].includes("Game_")) {
                        var data = DataSheet[obj[i][0]];
                        if (data && data._col) {
                            var game_lang = obj[i][1].split(';');

                            for (var j = 0; j < data._col.length; j++) {
                                var tmp = game_lang[j].split(':');
                                var tmp_col = tmp[0].split(',');

                                if (tmp.length == 2) {
                                    DataSheet[obj[i][0]]._Record[data._col[j]].addText = tmp[1].split(',');
                                }

                                DataSheet[obj[i][0]]._Record[data._col[j]].col = Array.isArray(tmp_col) ? tmp_col : tmp[0];
                            }
                        }

                        continue;
                    }

                    if (Array.isArray(this[obj[i][0]]))
                        this[obj[i][0]] = obj[i][1].split(';');
                    else
                        this[obj[i][0]] = obj[i][1];
                }
            }

            return this;
        },
        _refreshList:{
            Btn:[],
            Radio:[],
            Select: [],
            Span:[]
        },        
        RefreshType: {
            "Btn": "Btn",
            "Radio": "Radio",
            "Select": "Select",
            "Span":"Span"
        },
        AddRefreshList: function (type, control, attr) {
            this._refreshList[type].push({ control: control, attr: attr });
        },
        Refresh: function () {
            var c;

            /*btn*/
            for (var i = 0; i < this._refreshList.Btn.length; i++) {
                c = this._refreshList.Btn[i];

                if (!("control" in c))
                    continue;

                c.control.value = Language[c.attr];
            }

            /*Span*/
            for (var i = 0; i < this._refreshList.Span.length; i++) {
                c = this._refreshList.Span[i];

                if (!("control" in c))
                    continue;

                if (Test.Array(c.control)) {
                    for (var j = 0; j < c.control.length; j++) {
                        c.control[j].innerText = Language[c.attr][j];
                    }
                }else
                    c.control.innerText = Language[c.attr];                
            }

            /*Select*/
            for (var i = 0; i < this._refreshList.Select.length; i++) {
                c = this._refreshList.Select[i];

                if (!("control" in c))
                    continue;
                c.control.options.length = 0;

                for (var j = 0; j < Language[c.attr].length; j++) {
                    WebTool.Element.SelectOpt(c.control, j, Language[c.attr][j]);
                }
            }
            //qr.Search.select_status.options.length = 0;

            //for (var i = 0; i < Language.SearchStatus.length; i++) {
            //    WebTool.Element.SelectOpt(qr.Search.select_status, i, Language.SearchStatus[i]);
            //}
        }
    }

    window.Language = new lan();
}());

var DataSheet = {
    _obj: {},
    GameSetting: "",
    Game_25: {
        _Record: {
            name: {
                col: "廳別",
                Value: "",
                Center: true,
                Set: function (val) {
                    if (val == null)
                        return;
                    this.Value = val;
                }
            },
            time: {
                col: "結算時間",
                Value: "",
                Set: function (val) {
                    if (val) {
                        //var date = new Date(ParseDate(val));
                        val = val.Year + '/' + val.Month + '/' + val.Day + " " + val.Hour + ":" + val.Minute;
                        this.Value = val;
                    } else
                        this.Value = "";

                }
            },
            round: {
                col: "回合",
                Value: "",
                Center: true,
                Set: function (val) {
                    if (val == null)
                        return;

                    this.Value = val;
                }
            },
            pokers: {
                col: "發牌結果",
                addText: ["閒", "莊"],
                Value: "",
                Set: function (val) {
                    if (val == null)
                        return;

                    if (DataSheet.checkIsError(val)) {
                        this.Value = "error";
                        return;
                    }
                    var result = val.split(",");
                    var _p = [], _b = [], _point, _poker, _r;
                    for (var i = 0; i < result.length; i++) {
                        if (result[i] == "0") {
                            continue;
                        }

                        _poker = DataSheet[DataSheet.GameSetting].Get.Poker.Suit(result[i]);
                        _point = DataSheet[DataSheet.GameSetting].Get.Poker.Point(result[i]);

                        _r = DataSheet[DataSheet.GameSetting].eNum.Suit[_poker - 1] + DataSheet[DataSheet.GameSetting].eNum.Poker[_point - 1];
                        i % 2 ? _b.push(_r) : _p.push(_r);
                    }

                    this.Value = this.addText[0] + ":" + _p.join(",") + "<br/>" + this.addText[1] + ":" + _b.join(",");
                }
            },
            totalwin: {
                col: "總贏分",
                Value: "",
                Center: true,
                Set: function (val) {
                    if (val == null)
                        return;

                    this.Value = val;
                }
            },
            agent_refund: {
                col: "代理商水錢",
                Value: "",
                Center: true,
                Set: function (val) {
                    if (val == null)
                        return;

                    this.Value = val;
                }
            },
            refund: {
                col: "遊戲水錢",
                Value: "",
                Center: true,
                Set: function (val) {
                    if (val == null)
                        return;

                    this.Value = val;
                }
            },
            bets: {
                col: ["押閒", "押莊", "押和", "押閒對", "押莊對", "押莊六"],
                Value: [],
                Center: true,
                Set: function (val) {
                    if (val == null)
                        return;

                    this.Value = [];
                    var tmp = val.split(",");

                    for (var i = 0; i < tmp.length; i++) {
                        this.Value.push(tmp[i]);
                    }

                    if (this.Value.length == 5) {
                        this.Value.push("0");
                    }
                }
            },
            points: {
                col: "發牌點數",
                addText: ["閒", "莊"],
                Value: "",
                Center: true,
                Set: function (val) {
                    if (val == null)
                        return;

                    var result = val.split(":");

                    if (result.length == 2)
                        this.Value = this.addText[0] + result[0] + " : " + this.addText[1] + result[1];
                }
            },
            win: {
                col: "本局結果",
                addText: ["閒贏", "莊贏", "和"],
                Value: "",
                Center: true,
                Set: function (val, isTie, isError) {
                    if (val == null)
                        return;

                    if (isError) {
                        this.Value = "error";
                        return;
                    }

                    if (isTie) {
                        this.Value = this.addText[2];
                        return;
                    }

                    switch (val) {
                        case 0:
                            this.Value = this.addText[0];
                            break;
                        case 1:
                            this.Value = this.addText[1];
                            break;
                        case 2:
                            this.Value = this.addText[2];
                            break;
                        default:
                            this.Value = "";
                    }
                }
            },
            fee: {
                col: "押注規則",
                addText: ["莊六", "抽傭"],
                Value: "",
                Center: true,
                Set: function (val) {
                    if (val == null)
                        return;

                    var tmp = val.split(",");
                    if (tmp.length == 2)
                        this.Value = this.addText[Number(tmp[0])];
                }
            },
        },
        //_col: ["廳別", "結算時間", "回合", "發牌結果", "發牌點數", "本局結果", "總贏分", "退水", "押閒", "押莊", "押和", "押閒對", "押莊對"],
        _col: ["name", "time", "round", "points", "pokers", "win", "totalwin", "refund", "bets", "fee"],
        _width: [200, 150, 150, 180, 220, 220, 220, 220, 250],
        Get: {
            Poker: {
                //花色index轉換
                Suit: function (index) {
                    index = typeof index == "string" ? Number(index) : index;

                    return (Math.floor((index - 1) / 13) + 1);
                },
                //點數index轉換
                Point: function (index) {
                    index = typeof index == "string" ? Number(index) : index;

                    var result = index - Math.floor(index / 13) * 13;

                    if (result == 0) {
                        result = 13;
                    }

                    return result;
                },
                //花色點數轉換index
                Index: function (Suit, Point) {
                    return (Suit - 1) * 13 + Point;
                }
            }
        },
        eNum: {
            Poker: ["A", "2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K"],
            Suit: ["♠", "♥", "♦", "♣"]
        }
    },
    Game_17: {
        _Record: {
            GNO: {
                col: "回合",
                Value: "",
                Center: true,
                Set: function (val) {
                    if (val == null)
                        return;
                    this.Value = val;
                }
            },
            CreateDate: {
                col: "結算時間",
                Value: "",
                Center: true,
                Set: function (val) {
                    if (val) {
                        //var date = new Date(ParseDate(val));
                        val = val.Year + '/' + val.Month + '/' + val.Day + " " + val.Hour + ":" + val.Minute;
                        this.Value = val;
                    } else
                        this.Value = "";

                }
            },
            BigPoint: {
                col: "贏分點數",
                Value: "",
                addText: ["點"],
                Center: true,
                Set: function (val) {
                    if (val == null)
                        return;

                    this.Value = val + this.addText;
                }
            },
            WinType: {
                col: "本局結果",
                addText: ["閒贏", "莊贏", "和"],
                Value: "",
                Center: true,
                Set: function (val) {
                    if (val == null)
                        return;

                    switch (val) {
                        case 1:
                            this.Value = this.addText[0];
                            break;
                        case 2:
                            this.Value = this.addText[1];
                            break;
                        case 3:
                            this.Value = this.addText[2];
                            break;
                        default:
                            this.Value = "";
                    }

                }
            },
            Banker: {
                col: "是否當莊",
                Value: "",
                addText: ["否", "是"],
                Center: true,
                Set: function (val) {
                    if (val == null)
                        return;

                    this.Value = this.addText[Number(val)];
                }
            },
            Gain: {
                col: "總贏分",
                Value: "",
                Center: true,
                Set: function (val) {
                    if (val == null)
                        return;

                    this.Value = val;
                }
            },
            Bet: {
                col: "總押注",
                Value: "",
                Center: true,
                Set: function (val) {
                    if (val == null)
                        return;

                    this.Value = val;
                }
            }
        },
        //_col: ["回合", "結算時間", "贏分點數", "本局結果", "總贏分","總押注", "是否當莊"],
        _col: ["GNO", "CreateDate", "BigPoint", "WinType", "Gain", "Bet", "Banker"],
        _width: [150, 150, 150, 180, 220, 220, 220]
    },
    Game_CashCredit: {
        _Record: {
            CreateDate: {
                col: "時間",
                Value: "",
                Center: true,
                Set: function (val) {
                    if (val) {
                        var date = new Date(ParseDate(val)).format("yyyy/MM/dd HH:mm");
                        this.Value = date;
                    } else
                        this.Value = "";

                }
            },
            Cash: {
                col: "現金消費",
                Value: "",
                Center: true,
                Set: function (val) {
                    if (val == null)
                        return;

                    this.Value = val;

                }
            },
            Credit: {
                col: "信用消費",
                Value: "",
                Center: true,
                Set: function (val) {
                    if (val == null)
                        return;

                    this.Value = val;

                }
            },
            MemberCost: {
                col: "會員消費",
                Value: "",
                Center: true,
                Set: function (val) {
                    if (val == null)
                        return;

                    this.Value = val;
                }
            },
            AgentBonus: {
                col: "公司紅利",
                Value: "",
                Center: true,
                Set: function (val) {
                    if (val == null)
                        return;

                    this.Value = val;
                }
            },
            CompanyBonus: {
                col: "上線紅利",
                Value: "",
                Center: true,
                Set: function (val) {
                    if (val == null)
                        return;

                    this.Value = val;
                }
            },
            CreditMax: {
                col: "信用額度",
                Value: "",
                Center: true,
                Set: function (val) {
                    if (val == null)
                        return;

                    this.Value = val;
                }
            }
        },
        _col: ["CreateDate", "Cash", "Credit", "CreditMax", "MemberCost"],
        _width: [150, 150, 150, 180, 220, 220]
    },
    Game_CashInOut: {
        _Record: {
            CreateDate: {
                col: "時間",
                Value: "",
                Center: true,
                Set: function (val) {
                    if (val) {
                        var date = new Date(ParseDate(val)).format("yyyy/MM/dd HH:mm");
                        this.Value = date;
                    } else
                        this.Value = "";

                }
            },
            Place: {
                col: "地點",
                Value: "",
                Center: true,
                Set: function (val) {
                    if (val == null)
                        return;

                    this.Value = val;
                }
            },
            Price: {
                col: "交易金額",
                Value: "",
                Center: true,
                Set: function (val) {
                    if (val == null)
                        return;

                    this.Value = val;

                }
            },
            UD: {
                col: "點數",
                Value: "",
                Center: true,
                Set: function (val) {
                    if (val == null)
                        return;

                    this.Value = val;
                }
            }

        },
        _col: ["CreateDate", "Place", "Price", "UD"],
        _width: [150, 150, 150, 180]
    },
    Game_GameRefundList: {
        _Record: {
            time: {
                col: "時間",
                Value: "",
                Center: true,
                Set: function (val) {
                    if (val) {
                        var date = new Date(ParseDate(val)).format("yyyy/MM/dd HH:mm");
                        this.Value = date;
                    } else
                        this.Value = "";

                }
            },            
            refund: {
                col: "洗水金額",
                Value: "",
                Center: true,
                Set: function (val) {
                    if (val == null)
                        return;

                    this.Value = val;

                }
            }
        },
        _col: ["time", "refund"],
        _width: [150, 150]
    },
    div: null,
    table: null,
    Initial: function () {
        $(DataSheet.div).empty();
        DataSheet.table = document.createElement("table");
        DataSheet.table.border = 1;
        //DataSheet.table.width = (DataSheet[DataSheet.GameSetting]._width.Sum() + 5) + "px";

        $(DataSheet.div).append(DataSheet.table);
    },
    Create: function (obj, GID) {
        var GameSetting = "Game_" + GID;

        DataSheet.GameSetting = GameSetting;

        this.Initial(GameSetting);
        this._obj = obj;

        var Record = obj.Record;
        var column = obj.Column;

        var row = document.createElement("tr");
        row.style.backgroundColor = ColorSetting.TableTitle.BG;
        row.style.color = "white";
        row.style.height = "22px";

        var col_index = 0;
        /*add title*/
        for (var i = 0; i < DataSheet[GameSetting]._col.length; i++) {
            var tmp_col = DataSheet[GameSetting]._Record[DataSheet[GameSetting]._col[i]];
            if (tmp_col && tmp_col.col) {
                if (tmp_col.col instanceof Array) {
                    for (var j = 0; j < tmp_col.col.length; j++) {
                        DataSheet.insert(GameSetting, row, tmp_col.col[j], col_index, true);
                        col_index++;
                    }
                } else {
                    DataSheet.insert(GameSetting, row, tmp_col.col, col_index, true);
                    col_index++;
                }
            }

        }

        var _i;
        DataSheet.table.appendChild(row);
        /*add record*/
        var isTie = false, isError, pointIndex = column.indexOf("points"),
            pokersIndex = column.indexOf("pokers");
        for (var i = 0; i < Record.length; i++) {
            if (Record[i] == null)
                continue;

            row = document.createElement("tr");
            isTie = pointIndex == -1 ? false : this.checkIsTie(Record[i][pointIndex]);

            isError = pokersIndex == -1 ? false : this.checkIsError(Record[i][pokersIndex]);

            for (var j = 0; j < DataSheet[GameSetting]._col.length; j++) {
                _i = column.indexOf(DataSheet[GameSetting]._col[j]);
                if (_i == -1)
                    continue;

                DataSheet[GameSetting]._Record[column[_i]].Set(Record[i][_i], isTie, isError);

                DataSheet.insert(GameSetting, row, DataSheet[GameSetting]._Record[column[_i]].Value, _i, DataSheet[GameSetting]._Record[column[_i]].Center, true);
            }

            DataSheet.table.appendChild(row);
        }

    },
    checkIsTie: function (points) {
        var result = points.split(":");
        if (result.length == 2 && result[0] == result[1])
            return true;

        return false;
    },
    checkIsError: function (pokers) {
        var r = pokers.split(",");
        return r[0] == "0";
    },
    insert: function (GameSetting, row, content, index, isCenter, isDefaultColor) {
        var col, tmp, IsRed = false, isNumber = false;
        if (content instanceof Array) {
            for (var i = 0; i < content.length; i++) {
                DataSheet.insert(GameSetting, row, content[i], index, isCenter, isDefaultColor);
            }
        } else {
            col = document.createElement("td");
            col.style.whiteSpace = "nowrap";
            col.style.minWidth = "30px";
            //col.style.width = DataSheet[GameSetting]._width[index] + "px";
            col.style.padding = "5px";

            IsRed = false;
            if (!isNaN(content)) {
                if (Number(content) < 0)
                    IsRed = true;
                isNumber = true;
                tmp = numberWithCommas(content);
            } else
                tmp = content;

            col.innerHTML = tmp;
            col.style.color = IsRed ? "red" : (isDefaultColor ? ColorSetting.TableTitle.Content : "");
            isCenter ? col.align = "center" : col.align = "left";

            col.align = isNumber ? "right" : col.align;

            col.style.border = "1px solid " + ColorSetting.TableTitle.Line;

            row.appendChild(col);
        }

        return row;
    },
    insert_content: function (row, content) {
        var col, txt;
        if (content instanceof Array) {
            for (var i = 0; i < content.length; i++) {
                DataSheet.insert_content(row, content[i]);
            }
        } else {
            var span, isNumber = false;
            for (var i = 1; i < arguments.length; i++) {
                if (arguments[i] instanceof Array) {
                    this.insert_content(row, arguments[i]);
                    continue;
                }

                isNumber = false;

                col = document.createElement("td");
                span = document.createElement("span");

                txt = arguments[i];
                
                if (!isNaN(txt)) {
                    isNumber = true;
                    txt = Number(txt);
                    if (txt < 0)
                        span.style.color = "red";
                    txt = numberWithCommas(txt)
                }

                span.innerHTML = txt;

                //col.width = DataSheet._width[index] + "px";
                col.style.padding = "5px";
                col.style.minWidth = "50px";

                col.style.border = "1px solid " + ColorSetting.TableTitle.Line;
                //col.style.wordWrap = "break-word";
                //col.style.wordBreak = "normal";
                col.appendChild(span);

                if(!isNumber)
                    col.align = "center";
                else
                    col.align = "right";

                row.appendChild(col);
            }

        }

        return row;
    },
    insert_content_red: function (row, txt) {
        var span, col, isNumber = false;

        if (!isNaN(txt)) {
            txt = numberWithCommas(Number(txt));
            isNumber = true;
        }

        col = document.createElement("td");
        span = buildEle("span", {
            "innerHTML": txt,
            style: {
                "color": "red"
            }
        });

        col.style.padding = "5px";
        col.style.minWidth = "50px";

        col.appendChild(span);

        if(!isNumber)
            col.align = "center";
        else
            col.align = "right";
        col.style.border = "1px solid " + ColorSetting.TableTitle.Line;
        row.appendChild(col);

        return row;
    }
}

var _AddEditPack = function () { };
_AddEditPack.prototype = {
    ColumnTitle: Language.Column_AddEdit,
    Title: {},
    BtnList: {},
    EditID: 0,
    Mode: Modes.VIEW,
    Role: 0,
    CurrencyPerLineCount: 4,
    _errorMsg: null,
    tmp_input: [],
    Type: [
        "text",
        "text",
        "text",
        "text",
        "text",
        //"text",
        //"text",
        "text",
        //"email",
        "text",
        "text",
        "password",
        "password",
        //"radio",
       // "radio"
    ],
    ID: [
        "txtName",
        "txtBonus",
        "txtRefund",
        "txtMaxCredit",
        "txtMaxLimit",
        //"txtAddress",
        //"txtPhone",
        "txtIP",
        //"txtEmail",
        "txtBankAccount",
        "txtAccount",
        "txtPassword",
        "txtRePassword",
        "radioCanSeeNext",
        "radioCurrency"
    ],
    Column: [
        "Name",
        "ShopBonus",
        "Refund",
        "CreditMax",
        "MoneyLimit",
        //"ShopAddress",
        //"ShopPhone",
        "IP",
        //"Email",
        "BankAccount",
        "KeyString",
        "pa",
        "repa",
        "CanSeeNext",
        "Currency"
    ],
    SetMode: function (Mode) {
        this.ColumnTitle = Language.Column_AddEdit;

        this.Mode = Mode;
        var txt = this.Mode == Modes.ADD ? Language.btn_Add : Language.btn_Save;

        if (this.BtnList.Confirm && this.BtnList.Confirm.Btn) {
            this.BtnList.Confirm.Btn.value = txt;
        }

        if (this.Mode == Modes.EDIT) {
            this.Role = 3;
        }

        if (this.Title)
            this.Title.innerText = Language.AddEditPanelTitle[this.Role] + (this.Mode == Modes.ADD ? Language.btn_Add : Language.btn_Edit);

    },
    SetError: function (msg) {
        this._errorMsg.innerText = msg;
    },
    Set: function (obj) {
        if (obj.Record.length == 0)
            return;

        var _column = obj.Column, _record = obj.Record[0], colIndex, input_ele;

        var col = this.Column;

        for (var i = 0; i < col.length; i++) {
            colIndex = _column.indexOf(col[i]);

            if (colIndex == -1) {
                continue;
            }

            if (this.Type[i] == "radio") {
                $("input[name=" + this.ID[i] + "][value=" + _record[colIndex] + "]").prop('checked', true);
            } else {
                input_ele = document.getElementById(this.ID[i]);
                if (input_ele) {
                    input_ele.value = _record[colIndex];
                }
            }
        }

        LoadingPanel.Hide();
    },
    SetPerColumn: function (col, val) {
        var colIndex = this.Column.indexOf(col);
        if (colIndex == -1)
            return;

        var ele_input = document.getElementById(this.ID[colIndex]);
        if (ele_input)
            ele_input.value = val;
    },
    Create: function (panel) {
        var div;
        this.Title = buildEle("span");
        div = buildEle("div", {
            style: {
                "margin-bottom": "10px",
                "font-size": FontSizeSetting.AddEditPanel + "px"
                //"text-align": "center"
            }
        });
        div.appendChild(this.Title);
        panel.appendChild(div);

        var tbl = buildEle("table");
        var row, td_ele;
        for (var i = 0; i < this.ColumnTitle.length; i++) {
            if (this.Type[i] == "radio")
                continue;

            row = buildEle("tr", { id: "row_" + this.ID[i] });
            createTd(this.ColumnTitle[i], row, false, null, null, {
                //"width": "180px"
            });

            td_ele = buildEle("td", {
                style: {
                    "border":"1px solid "+ColorSetting.TableTitle.Line
                }
            });
            td_ele.appendChild(buildEle("input", {
                id: this.ID[i],
                type: this.Type[i],
                className: "txtwidth"
            }));

            row.appendChild(td_ele);

            tbl.appendChild(row);
        }

        /*canSeeNext ----------------*/
        row = buildEle("tr", { id: "row_canSeeNext" });

        createTd(this.ColumnTitle[this.ColumnTitle.length - 2], row, false);

        td_ele = buildEle("td");

        var ra = createRadioElement("Yes", "1", this.ID[this.ColumnTitle.length - 2], false);
        td_ele.appendChild(ra);

        ra = createRadioElement("No", "0", this.ID[this.ColumnTitle.length - 2], true);
        td_ele.appendChild(ra);

        row.appendChild(td_ele);

        tbl.appendChild(row);

        /*Currency ----------------*/
        row = buildEle("tr", { id: "row_Currency" });

        createTd(this.ColumnTitle[this.ColumnTitle.length - 1], row, false);

        td_ele = buildEle("td");

        var currency_div;
        for (var i = 0; i < Language.Cell_Currency.length; i++) {
            if (i % this.CurrencyPerLineCount == 0) {
                /*換行*/
                currency_div = buildEle("div");
                td_ele.appendChild(currency_div);
            }
            ra = createRadioElement(Language.Cell_Currency[i], (i + 1), this.ID[this.ColumnTitle.length - 1], i == 0);
            currency_div.appendChild(ra);
        }

        row.appendChild(td_ele);

        tbl.appendChild(row);


        /*button*/
        row = buildEle("tr");
        td_ele = buildEle("td", {
            style: {
                "text-align": "right",
                "padding-top": "5px",
                "padding-bottom": "5px",
                "border":"1px solid "+ColorSetting.TableTitle.Line
            }
        });
        td_ele.colSpan = "2";

        this._errorMsg = buildEle("span", { style: { "color": "red" } });
        td_ele.appendChild(this._errorMsg);

        var _btn = buildEle("input", {
            type: "button",
            className: "confirmbtn leftBtn",
            style: {
                "margin":"0"
            },
            value: Language.btn_Save
        });
        _btn.onclick = function () { Control.RightView.AddEdit(); };
        this.BtnList["Confirm"] = { Lan: "btn_Save", Btn: _btn };

        _btn = buildEle("input", {
            type: "button",
            className: "confirmbtn leftBtn",
            style: {
                "margin": "0"
            },
            value: Language.btn_Return
        });
        _btn.onclick = function () { AddEditPack.SetError(""); RightView.Show.Data(); };

        this.BtnList["Return"] = { Lan: "btn_Return", Btn: _btn };

        for (var item in this.BtnList) {
            if (this.BtnList[item]["Btn"]) {
                td_ele.appendChild(this.BtnList[item]["Btn"]);
            }
        }

        row.appendChild(td_ele);
        tbl.appendChild(row);
        panel.appendChild(tbl);

        return panel;
    },
    InitInput: function () {
        var input_ele;
        for (var i = 0; i < this.ID.length; i++) {
            input_ele = document.getElementById(this.ID[i]);
            if (input_ele) {
                input_ele.value = "";
            }
        }
    },
    GetInputValue: function () {
        var ele_input;
        for (var i = 0; i < this.ColumnTitle.length; i++) {
            ele_input = document.getElementById(this.ID[i]);
            if (ele_input)
                this.tmp_input[i] = ele_input.value;
        }

        return this;
    },
    SetInputValue: function () {
        var ele_input;
        for (var i = 0; i < this.ColumnTitle.length; i++) {
            ele_input = document.getElementById(this.ID[i]);
            if (ele_input && this.tmp_input[i])
                ele_input.value = this.tmp_input[i];
        }
    },
    Refresh: function (panel) {
        this.ColumnTitle = Language.Column_AddEdit;

        $(panel).empty();
        this.Create(panel);
    }
}

var AddEditPack = new _AddEditPack();

var LoadingPanel = function () {
    var self = {};
    var acc = "", pwd = "", errorMsgId = "errorMsg";
    var ele = Object.create(null);


    ele.div_pop = buildEle("div", {
        id: "pop_window",
        className: "pop_window",
        style: {
            "z-index": "11000"
        }
    });

    //ele.div_pop.appendChild(
    //    ele.bg = buildEle("div", {
    //        className: "pop_window_bg"
    //    }));

    ele.div_call = buildEle("div", {
        style: {
            "color": "black",
            "z-index": "11100",
            "position": "fixed",
            "top": "0",
            "bottom": "0",
            "left": "0",
            "right": "0"
        }
    });

    var txt = buildEle("span", {
        innerText: "Loading...",
        style: {
            "z-index": "11101",
            "font-weight": "bolder",
            "font-size": FontSizeSetting.Loading + "px",
            "position": "fixed",
            "top": "50%",
            "left": "50%",
            "margin-top": "-30px",
            "margin-left": "-100px"
        }
    });
    ele.div_call.appendChild(txt);

    ele.div_pop.appendChild(
        ele.div_call
    );

    self.Init = function () {
        $("body").append(ele.div_pop);
        self.Hide();
    }
    self.Show = function () {
        $(ele.div_pop).show();
    }
    self.Hide = function () {
        $(ele.div_pop).hide();
    }

    return self;
}();

var Login = function () {
    var _rect = {
        "bg": { "type": "image", "x": 0, "y": 0, "width": 1366, "height": 768 },
        "acc": { "type": "input", "x": 573, "y": 346, "width": 280, "height": 40 },
        "pa": { "type": "input", "x": 573, "y": 405, "width": 280, "height": 40 },
        "err_msg": { "type": "container", "x": 510, "y": 463, "width": 80, "height": 77 },
        "loginbtn": {
            "type": "image", "x": 743, "y": 463, "width": 161, "height": 55, onclick: function () {
                acc = self.objs["acc"].Obj[0].value;
                pwd = self.objs["pa"].Obj[0].value;
                if (checkLogin())
                    ws.Send("login", { acc: acc, pwd: pwd, lan: Reporter.Language });
            }
        }
    }

    var root_path = "image/", _extension = ".png";
    var self = {};
    self.objs = {};
    var acc = "", pwd = "", errorMsgId = "errorMsg";
    var ele = Object.create(null);
    ele.Container = buildEle("div", {
        id: "login_container",
        style: { "position": "fixed" }
    });


    var CreateObj = {
        Img: function (item) {
            var _img = document.createElement("img");
            _img.src = self.objs[item].Path;
            _img.style.position = "absolute";

            return _img;
        },
        Input: function (item) {
            var _input = document.createElement("input");
            _input.id = item;
            _input.type = item == "acc" ?"text":"password";
            _input.placeholder = item == "acc" ? "Account" : "Password";
            _input.className = "form-control";
            _input.style.width = "100%";
            _input.style.backgroundColor = "transparent";
            _input.style.border = "0px solid";
            _input.style.position = "absolute";
            _input.style.color = "white";
            _input.style.fontSize = "20px";

            return _input;
        },
        Div: function () {
            var _div = document.createElement("div");
            _div.id = errorMsgId;
            _div.style.position = "absolute";

            return _div;
        }
    }

    function checkLogin() {
        self.objs["err_msg"].Obj.empty();
        var msg = "";
        if (acc == "")
            msg = "Invalid account";
        else if (pwd == "")
            msg = "Invalid password";

        if (msg.length > 0) {
            $(self.objs["err_msg"]).append(buildEle("span", { innerHTML: msg, style: { color: "red" } }));
            return false;
        } else {
            return true;
        }
    }

    self.Init = function () {
        self.Set();
        ele.window = $(window);
        ele.Container.style.background = "url('" + self.objs.bg.Path + "')";
        ele.Container.style.backgroundSize = "100% 100%";
        $("body").append(ele.Container);
        $("#acc,#pa").keypress(function (e) {
            code = (e.keyCode ? e.keyCode : e.which);
            if (code == 13) {
                acc = self.objs["acc"].Obj[0].value;
                pwd = self.objs["pa"].Obj[0].value;
                if (checkLogin())
                    ws.Send("login", { acc: acc, pwd: pwd, lan: Reporter.Language });
            }
        });

    }

    var margin = new Tools.Point();

    self.Resize = function () {
        if (!ele.Container) {
            return;
        }

        var windowwidth = document.documentElement.clientWidth;
        var windowheight = document.documentElement.clientHeight;
        var windowwtoh = windowwidth / windowheight;
        margin.Set();

        var _CanvasW = 0;
        var _CanvasH = 0;
        canvaswtoh = 1366 / 768;
        if (windowwtoh > canvaswtoh) {// window width is too wide relative to desired game width
            _CanvasW = windowheight * canvaswtoh;
            _CanvasH = windowheight;
            margin.x = (windowwidth - _CanvasW) / 2;
        } else { // window height is too high relative to desired game height
            _CanvasW = windowwidth;
            _CanvasH = windowwidth / canvaswtoh;
            margin.y = (windowheight - _CanvasH) / 2;
        }
        Tools.Zoom = _CanvasW / 1366;

        $(ele.Container).css(self.objs.bg.Rect.ZoomedCss().Move(margin).toCss());

        var setting_obj = self.objs;

        for (var item in setting_obj) {
            if (setting_obj[item].Obj) {
                setting_obj[item].Obj.css(setting_obj[item].Rect.ZoomedCss().toCss());
                if (item == "acc" || item == "pa") {
                    setting_obj[item].Obj[0].style.fontSize = 20 * Tools.Zoom + "px";
                }

            }
        }


    }

    self.Set = function () {
        var _p, _obj;
        var setting_obj = self.objs;

        for (var item in _rect) {
            _p = _rect[item];
            setting_obj[item] = {};
            setting_obj[item].Rect = new Tools.Rect(_p.x, _p.y, _p.width, _p.height);
            setting_obj[item].Path = _p.type == "image" ? root_path + item + _extension : "";

            if (item != "bg") {
                switch (_p.type) {
                    case "image":
                        _obj = CreateObj.Img(item);
                        break;
                    case "input":
                        _obj = CreateObj.Input(item);
                        break;
                    case "container":
                        _obj = CreateObj.Div();
                        break;
                }

                if ("onclick" in _p) {
                    _obj.onclick = _p.onclick;
                }

                setting_obj[item].Obj = $(_obj);

                $(ele.Container).append(setting_obj[item].Obj);
            }
        }
    }
    self.Show = function () {
        self.objs["acc"].Obj[0].value = acc = "";
        self.objs["pa"].Obj[0].value = pwd = "";
        self.objs["err_msg"].Obj.empty();
        $(ele.Container).show();
    }
    self.Hide = function () {
        $(ele.Container).hide();
    }
    Object.defineProperties(self, {
        ErrorMsgId: { get: function () { return errorMsgId; } },
        Account: { get: function () { return acc; } },
        Password: { get: function () { return pwd; } }
    })
    return self;
}();

var ReportTitle = function () {
    var self = {}, height = 50, info_fontsize = 12,
        backColor = "#343C46";
    var ele = Object.create(null), ele_info = {};

    ele.div_top = buildEle("div", {
        style: {
            "display": "none",
            "height": height + "px",
            "line-height": height + "px",
            "text-align": "center",
            "vertical-align": "middle",
            "font-size": FontSizeSetting.ReporterTitle + "px",
            "background-color": backColor,
            "color": "white"
        }
    });

    ele.div_info_container = buildEle("div", {
        style: {
            "vertical-align": "middle",
            "position": "absolute",
            "top": (height / 3) + "px",
            "left": "10px"
        }
    });
    ele.div_info = buildEle("div", {
        style: {
            "color": ColorSetting.TableTitle.Content,
            "display": "inline-block"
        },
        className: "AccSumB"
    });
    ele.div_info_container.appendChild(ele.div_info);
    ele.div_top.appendChild(ele.div_info_container);

    ele.div_info_JQObj = $(ele.div_info);

    ele.div_top_title = buildEle("span", {
        innerText: ""
    });

    ele.div_top.appendChild(ele.div_top_title);

    //Create array of options to be added
    var array = ["TW", "EN"];
    var check_language = buildEle("select", {
        style: {
            "position": "absolute", "right": "0"
        }
    });

    //Create and append the options
    for (var i = 0; i < array.length; i++) {
        var option = document.createElement("option");
        option.value = array[i];
        option.text = array[i];
        check_language.appendChild(option);
    }

    check_language.onchange = function () {
        LoadingPanel.Show();
        RightView.Data.MemberList.InsertFirst = true;
        Reporter.Language = check_language.value;
        ws.Send("request.language", { GID: Reporter.GID, lan: check_language.value });
    }

    ele.div_top.appendChild(check_language);

    self.Refresh = function () {
        //ele.div_top_title.innerText = Language.Title;
    }

    self.Init = function () {
        self.UpdateInfo();
        $("body").append(ele.div_top);
    }
    self.Show = function () {
        $(ele.div_top).show();
    }
    self.Hide = function () {
        $(ele.div_top).hide();
    }

    var Create = {
        TdWithDivCss: function (css) {
            var td = buildEle("td", {
                style: {
                    "margin": "0",
                    "padding-right": "2px"
                }
            }),
            obj = buildEle("div", {
                className: css
            });
            td.appendChild(obj);
            return td;
        },
        TdWithSpan: function (text, css) {
            return this.TdWithSpanPuthToEle(text, false, css);
        },
        TdWithSpanPuthToEle: function (text, key, css, placeholder) {
            var td = buildEle("td", {
                style: {
                    "margin": "0"
                }
            }),
            obj = buildEle("span", {
                className: "AccSumText " + (css ? css : ""),
                innerText: text
            });
            if (placeholder)
                obj.title = placeholder;

            td.appendChild(obj);

            if (key != "") {
                ele_info[key] = obj;
            }

            return td;
        }
    }

    self.UpdateInfo = function (data) {
        self.UpdateInfo = function (data) {
            if (!data) {
                return;
            }

            ele_info.name.innerText = Reporter.Name + " (" + Reporter.Title + ")";
            ele_info.nextlevel.innerText = data.lowerTitle + "：";
            ele_info.nextlevelcount.innerText = data.lowerCount;
            ele_info.usedcredit.innerText = numberWithCommas(data.usedCredit);
        }

        ele.div_info_JQObj.empty();
        var tbl = buildEle("table", {
            "border": "0"
        }), tr = buildEle("tr", {
            style: {
                "border": "none",
                "height": "auto",
                "font-size": info_fontsize + "px"
            }
        }), td;
        tbl.cellPadding = tbl.cellSpacing = "0";

        ele.div_info.appendChild(tbl);
        tbl.appendChild(tr);

        tr.appendChild(Create.TdWithDivCss("AccSumLCorner"));

        td = Create.TdWithDivCss("IdIcon");
        td.className = "AccSumTdPaddingLeft";
        tr.appendChild(td);
        tr.appendChild(Create.TdWithSpan("戶口： "));
        tr.appendChild(Create.TdWithSpanPuthToEle("", "name", "AccSumTdPaddingRight"));

        tr.appendChild(Create.TdWithDivCss("UserIcon"));
        tr.appendChild(Create.TdWithSpanPuthToEle("", "nextlevel"));
        tr.appendChild(Create.TdWithSpanPuthToEle("", "nextlevelcount", "AccSumTdPaddingRight"));

        tr.appendChild(Create.TdWithDivCss("UcIcon"));
        tr.appendChild(Create.TdWithSpanPuthToEle("", "usedcredit", "AccSumTdPaddingRight", "已使用信用額度"));

        td = Create.TdWithDivCss("AccSumRCorner");
        td.style.padding = "0";
        td.style.backgroundColor = backColor;
        tr.appendChild(td);

        self.UpdateInfo(data);
    }

    Object.defineProperties(self, {
        Height: { get: function () { return height; } }
    })

    return self;
}();

//主頁
var MainPage = {};
MainPage.Init = function () {
    var self = {}, ele = {};

    var btnWidth = "98%";

    self.Div = buildEle("div", {
        id: "mainPageDiv",
        "className": "adjustHeight leftback",
        style: { display: "none", width: "20%" }
    });

    var div = buildEle("div", {
        style: {
            /*"text-align": "center",*/
            "padding": "10px",
            "font-size": FontSizeSetting.LeftView + "px",
            "color": ColorSetting.LeftMenu
        },
        "className": "LeftView"
    }), menuDiv = buildEle("div");

    self.menu_homepage = buildEle("span", {
        innerText: Language.Menu_HomePage,
        className: "pointer leftBtn",
        style: {
            width: btnWidth,
            display: "block"
        }
    });

    self.menu_homepage.onclick = function () {
        if (Layout == Layouts.MAINPAGE)
            return;

        LoadingPanel.Show();
        Control.HomePage.Send();
        setLayout(Layouts.MAINPAGE);
    }

    menuDiv.appendChild(self.menu_homepage);

    self.menu_membermanage = buildEle("span", {
        id: "menu2",
        innerText: Language.Menu_MemberManage,
        className: "pointer leftBtn",
        style: {
            width: btnWidth,
            display: "block"
        }
    });

    self.menu_membermanage.onclick = function () {
        LoadingPanel.Show();
        self.Hide();
        QRView.Hide();
        Reporter.DataType = DataType.MEMBERMANAGE;
        setLayout(Layouts.MEMBERMANAGE);
    }

    menuDiv.appendChild(self.menu_membermanage);

    self.menu_stockholder = buildEle("span", {
        innerText: Language.Menu_StockholderManage,
        className: "pointer leftBtn",
        style: {
            width: btnWidth,
            display: "block"
        }
    });

    self.menu_stockholder.onclick = function () {
        LoadingPanel.Show();
        self.Hide();
        QRView.Hide();
        Reporter.DataType = DataType.STOCKHOLDERMANAGE;
        setLayout(Layouts.STOCKHOLDERMANAGE);
    }

    menuDiv.appendChild(self.menu_stockholder);

    self.menu_gamelist = buildEle("span", {
        id: "menu0",
        innerText: Language.Menu_enterReport,
        className: "pointer leftBtn",
        style: {
            width: btnWidth,
            display: "block"
        }
    });

    self.menu_gamelist.onclick = function () {
        LoadingPanel.Show();
        HomePageView.Hide();
        CheckOutView.Hide();
        self.Hide();
        QRView.Hide();
        RightView.Hide();
        Reporter.DataType = DataType.GAMELIST;
        RightView.Data.MemberList.InsertFirst = true;
        ws.Send("enter_report", { GID: Reporter.GID });
    }

    menuDiv.appendChild(self.menu_gamelist);
    div.appendChild(menuDiv);

    self.menu_trans = buildEle("span", {
        //id: "menu1",
        innerText: Language.Menu_transReport,
        className: "pointer leftBtn",
        style: {
            width: btnWidth,
            display: "block"
        }
    });

    self.menu_trans.onclick = function () {
        //LoadingPanel.Show();
        Right_Title.Set(Language.Menu_transReport);
        OnlineView.Hide();
        HomePageView.Hide();
        self.Hide();
        QRView.Hide();
        RightView.Hide();
        LoadingPanel.Hide();
        CheckOutView.Hide();
        Reporter.DataType = DataType.TRANSACTION;
        LeftView_Trans.Show();
        CheckOutSettingView.Hide();
        CheckOutHistoryView.Hide();
    }

    menuDiv.appendChild(self.menu_trans);

    self.menu_gamerefundlist = buildEle("span", {
        //id: "menu1",
        innerText: Language.Menu_GamerefundList,
        className: "pointer leftBtn",
        style: {
            width: btnWidth,
            display: "block"
        }
    });

    self.menu_gamerefundlist.onclick = function () {
        //LoadingPanel.Show();
        LoadingPanel.Show();
        self.Hide();
        QRView.Hide();
        Reporter.DataType = DataType.GAMEREFUNDLIST;
        setLayout(Layouts.GAMEREFUNDLIST);
    }

    menuDiv.appendChild(self.menu_gamerefundlist);

    self.menu_checkout = buildEle("span", {
        //id: "menu1",
        innerText: Language.Menu_Checkout,
        className: "pointer leftBtn",
        style: {
            width: btnWidth,
            display: "block"
        }
    });

    self.menu_checkout.onclick = function () {
        LoadingPanel.Show();
        OnlineView.Hide();
        HomePageView.Hide();
        QRView.Hide();
        RightView.Hide();
        setLayout(Layouts.CHECKOUT);
        Control.Cashout.GetList();
    }

    menuDiv.appendChild(self.menu_checkout);

    self.menu_checkout_history = buildEle("span", {
        //id: "menu1",
        innerText: Language.Menu_CheckoutHistory,
        className: "pointer leftBtn",
        style: {
            width: btnWidth,
            display: "block"
        }
    });

    self.menu_checkout_history.onclick = function () {
        LoadingPanel.Show();
        OnlineView.Hide();
        HomePageView.Hide();
        QRView.Hide();
        RightView.Hide();
        setLayout(Layouts.CHECKOUTHISTORY);
        //Control.Cashout.GetList();
    }

    menuDiv.appendChild(self.menu_checkout_history);

    self.menu_checkout_config = buildEle("span", {
        //id: "menu1",
        innerText: Language.Menu_reoprtCfg,
        className: "pointer leftBtn",
        style: {
            width: btnWidth,
            display: "block"
        }
    });

    self.menu_checkout_config.onclick = function () {
        LoadingPanel.Show();
        OnlineView.Hide();
        HomePageView.Hide();
        QRView.Hide();
        RightView.Hide();
        setLayout(Layouts.TRANS_CONFIG);
        //Control.Cashout.GetList();
    }

    menuDiv.appendChild(self.menu_checkout_config);

    self.menu_abc = buildEle("span", {
        id: "menu2",
        innerText: Language.Menu_ABC,
        className: "pointer leftBtn",
        style: {
            width: btnWidth,
            display: "block"
        }
    });

    self.menu_abc.onclick = function () {
        LoadingPanel.Show();
        setLayout(Layouts.ABC);
    }

    menuDiv.appendChild(self.menu_abc);

    self.menu10 = buildEle("span", {
        //id: "menu10",
        innerText: Language.Menu_QRCode,
        className: "pointer leftBtn",
        style: {
            width: btnWidth
        }
    });

    self.menu10.onclick = function () {
        LoadingPanel.Show();
        RightView.Hide();
        //Layout = Layouts.QRVIEW;

        setLayout(Layouts.QRVIEW);
        //QRView.Show();

        QRView.GetList();
    }

    menuDiv = buildEle("div")
    menuDiv.appendChild(self.menu10);
    div.appendChild(menuDiv);

    self.menu_logout = buildEle("span", {
        //id: "menu10",
        innerText: Language.btn_Logout,
        className: "pointer leftBtn",
        style: {
            width: btnWidth,
            display:"block"
        }
    });
    self.menu_logout.onclick = function () {
        Control.Logout();
    }
    menuDiv = buildEle("div")
    menuDiv.appendChild(self.menu_logout);
    div.appendChild(menuDiv);

    self.Div.appendChild(div);

    self.onlineDiv = buildEle("div", {
        style: {
            "margin-top": "10px",
            "padding": "10px"
        }
    });
    self.Div.appendChild(self.onlineDiv);

    self.onlineTitle = buildEle("span", {
        innerText: Language.OnlineTitle
    });

    self.Refresh = function () {
        self.menu_gamelist.innerText = Language.Menu_enterReport;
        self.menu10.innerText = Language.Menu_QRCode;
        self.menu_trans.innerText = Language.Menu_transReport;
        self.menu_membermanage.innerText = Language.Menu_MemberManage;
        self.menu_homepage.innerText = Language.Menu_HomePage;
        self.menu_logout.innerText = Language.btn_Logout;
        self.onlineTitle.innerText = Language.OnlineTitle;
        self.menu_checkout.innerText = Language.Menu_Checkout;
        self.menu_checkout_config.innerText = Language.Menu_reoprtCfg;
        self.menu_checkout_history.innerText = Language.Menu_CheckoutHistory;
        self.menu_stockholder.innerText = Language.Menu_StockholderManage;
        self.menu_abc.innerText = Language.Menu_ABC;
    }

    self.Show = function () {
        self.Div.style.display = "inline-block";
    }
    self.Hide = function () {
        $(self.Div).hide();
    }

    self.onlineGID = "";

    self.UpdateRole = function () {
        self.menu_homepage.style.display = "block";
        self.menu10.style.display = "none";
        self.menu_membermanage.style.display = "block";
        self.menu_trans.style.display = "block";
        self.menu_checkout.style.display = "none";
        self.menu_checkout_config.style.display = "none";
        self.menu_checkout_history.style.display = "block";
        self.menu_stockholder.style.display = "none";
        self.menu_abc.style.display = "none";

        self.onlineGID = "";
        OnlineView.Hide();
        self.onlineDiv.style.display = "none";

        if (Reporter.Role == 1) {
            self.menu_gamelist.style.display = "block";
            self.menu_stockholder.style.display = "block";
        } else if (Reporter.Role == 2) {
            self.menu_membermanage.style.display = "none";
            self.menu10.style.display = "none";
            OnlineView.Show();
            self.onlineDiv.style.display = "";
        } else {
            self.menu10.style.display = "none";
            self.menu_membermanage.style.display = "none";
            self.menu_trans.style.display = "none";
            self.menu_homepage.style.display = "none";
            self.menu_checkout_history.style.display = "none";
        }

        if (Reporter.isAdmin) {
            OnlineView.Show();
            self.onlineDiv.style.display = "";
            self.menu_checkout.style.display = "block";
            self.menu_checkout_config.style.display = "block";
            self.menu_abc.style.display = "block";
        } 

    }

    ele.PerLobbyDiv = [];

    self.onlinePeopleCount = {};

    self.UpdateOnlineList = function (data) {
        self.UpdateOnlineList = function (data) {
            /*Update Online People*/
            var perLobbyPeopleCount = 0;

            if (Test.Undefined(data) || Test.Undefined(data.Games))
                return;

            var record = data.Games, _tables, _players;

            for (var item in record) {
                if (!(item in self.onlinePeopleCount) || Test.Undefined(record[item].Tables))
                    return;
                perLobbyPeopleCount = 0;

                for (var i = 0; i < record[item].Tables.length; i++) {
                    _tables = record[item].Tables[i];
                    for (var j = 0; j < _tables.Players.length; j++) {
                        _players = _tables.Players[j];
                        if (_players != null)
                            perLobbyPeopleCount += 1;
                    }
                }

                self.onlinePeopleCount[item].innerText = perLobbyPeopleCount;
            }
        }

        var tmp_table = buildEle("table", {
            border: "0", style: {
                "border-collapse": "separate",
                "display": "inline-block",
                "min-width": "50px",
                "color":"white"
            },
        }), tmp_tr, tmp_td;
        /*Title*/
        self.onlineDiv.appendChild(tmp_table);
        tmp_tr = buildEle("tr");
        tmp_table.appendChild(tmp_tr);

        tmp_td = buildEle("td", {
            style: {
                "padding-right": "5px"
            }
        });
        tmp_tr.appendChild(tmp_td);
        tmp_td.appendChild(self.onlineTitle);

        tmp_td = buildEle("td");
        tmp_tr.appendChild(tmp_td);
        var tmp_img = buildEle("img", {
            src: "image/refresh.png",
            style: {
                width: "20px",
                "margin-top": "2px"
            }
        });
        tmp_img.onclick = function () {
            Control.Online.Send();
        }
        tmp_td.appendChild(tmp_img);

        var tmp_div, tmp_span, count = 0;

        for (var item in data.Games) {
            count += 1;
            tmp_div = buildEle("div", {
                style: {
                    "margin-top": "5px"
                },
                className: "pointer LeftView leftBtn"
            });
            tmp_div.GID = item;
            tmp_div.onclick = function (evt) {
                self.onlineGID = this.GID;
                Control.Online.Send();
                setLayout(Layouts.ONLINE);
            };
            tmp_span = buildEle("span", {
                innerText: "第" + count + "廳("
            });
            tmp_div.appendChild(tmp_span);

            tmp_span = buildEle("span", {
                innerText: "0"
            });
            self.onlinePeopleCount[item] = tmp_span;
            tmp_div.appendChild(tmp_span);

            tmp_span = buildEle("span", {
                innerText: ")"
            });
            tmp_div.appendChild(tmp_span);

            self.onlineDiv.appendChild(tmp_div);
        }

        self.UpdateOnlineList(data);
        resize();
    }

    $("body").append(self.Div);
    MainPage = self;
};

(function () {
    if (!Function.prototype.bind) {
        Function.prototype.bind = function (oThis) {
            if (typeof this !== "function") {
                // closest thing possible to the ECMAScript 5
                // internal IsCallable function
                throw new TypeError("Function.prototype.bind - what is trying to be bound is not callable");
            }

            var aArgs = Array.prototype.slice.call(arguments, 1),
                fToBind = this,
                fNOP = function () { },
                fBound = function () {
                    return fToBind.apply(this instanceof fNOP
                                           ? this
                                           : oThis || this,
                                         aArgs.concat(Array.prototype.slice.call(arguments)));
                };

            fNOP.prototype = this.prototype;
            fBound.prototype = new fNOP();

            return fBound;
        };
    }
}());

var CreateView = {
    Date: {
        Type: {
            DateTime: 0,
            Date: 1,
            Start: 0,
            End: 1
        },
        List: ["Year", "Month", "Day", "Hour", "Minute"],
        Span: ["&nbsp;-&nbsp;", "&nbsp;-&nbsp;", "&nbsp;&nbsp;", "&nbsp;:&nbsp;"],
        Control: function (DateType) {
            var result = {}, selectObj, spanObj;
            result.Container = buildEle("div", { style: { "margin-bottom": "10px" } });

            result.Select = [];
            result.Span = [];
            result.Select_checkoutduration = {};

            var _list = DateType == this.Type.DateTime ? this.List : this.List.slice(0, 3);

            for (var i = 0; i < 2; i++) {
                selectObj = {};
                spanObj = [];
                for (var j = 0; j < _list.length; j++) {
                    /*Create Select*/
                    selectObj[_list[j]] = buildEle("select");
                    /*Append to Container*/
                    result.Container.appendChild(selectObj[_list[j]]);
                    if (i == 0 && j == _list.length - 1) {
                        /*Start last one append split*/
                        spanObj.push(buildEle("span", { innerHTML: "&nbsp;~&nbsp;&nbsp;" }));
                        /*Append to Container*/
                        result.Container.appendChild(spanObj[j]);
                    } else if (j < _list.length - 1) {
                        /*Create Span*/
                        spanObj.push(buildEle("span", { innerHTML: this.Span[j] }));
                        /*Append to Container*/
                        result.Container.appendChild(spanObj[j]);
                    }
                }
                result.Select.push(selectObj);
                result.Span.push(spanObj);
            }
            
            /*Bind Event*/
            for (var i = 0; i < 2; i++) {
                $(result.Select[i].Year).change({ msg: result.Select[i] }, function (evt) {
                    CreateView.Date.SelDate(evt.data.msg);
                });
                $(result.Select[i].Month).change({ msg: result.Select[i] }, function (evt) {
                    CreateView.Date.SelDate(evt.data.msg);
                });
            }

            return result;
        },
        Initial: function (DateType, obj, bindTarget) {
            var nowDate = new Date(), option;
            var txt = nowDate.getFullYear(), namespace = CreateView.Date;
            var _list = namespace.List;
            /*Year*/
            this.Set(obj, _list[0], 10, txt, txt - 10);
            /*Month*/
            this.Set(obj, _list[1], 12, padLeft("2", nowDate.getMonth() + 1, "0"));

            this._SelDate(obj.Select);
            /*Day Start End*/
            obj.Select[0][_list[2]].value = obj.Select[1][_list[2]].value = nowDate.getDate();

            if (DateType == namespace.Type.Date) {
                bindTarget.StartTime = Control.RightView.getSearch(DateType, obj.Select[CreateView.Date.Type.Start]);
                bindTarget.EndTime = Control.RightView.getSearch(DateType, obj.Select[CreateView.Date.Type.End]);

                return;
            }

            /*Hour*/
            this.Set(obj, _list[3], 24, "00", -1);
            /*Minute*/
            this.Set(obj, _list[4], 60, "00", -1);
            /*End Hour*/
            obj.Select[1][_list[3]].value = "23";
            /*End Minute*/
            obj.Select[1][_list[4]].value = "59";

            bindTarget.StartTime = Control.RightView.getSearch(DateType, obj.Select[CreateView.Date.Type.Start]);
            bindTarget.EndTime = Control.RightView.getSearch(DateType, obj.Select[CreateView.Date.Type.End]);
        },
        Set: function (obj, type, length, defaultText, start_index) {
            var select, start, end;
            for (var i = 0; i < 2; i++) {
                select = obj.Select[i][type];
                start = start_index == -1 ? 0 : start_index ? start_index : 1;
                end = start_index ? start_index + length : length;
                for (var j = start; j <= end; j++) {
                    if (select)
                        select.appendChild(this._setOption(j));
                }

                if (select && defaultText)
                    select.value = defaultText;

            }
        },
        _setOption: function (val) {
            var option = document.createElement("option");
            option.text = padLeft(2, val, "0");
            option.value = padLeft(2, val, "0");

            return option;
        },
        _SelDate: function (obj) {
            if (obj.length != 2)
                return;
            for (var i = 0; i < obj.length; i++)
                CreateView.Date.SelDate(obj[i]);
        },
        SelDate: function (obj) {
            if (!obj.Year || !obj.Month || !obj.Day)
                return;

            var dy = obj.Day;
            var mth = obj.Month;
            var yr = obj.Year;
            dy.options.length = 0;

            if (mth.value && yr.value) {
                var days = new Date(yr.value, mth.value, 1, -1).getDate(); // the first day of the next month minus 1 hour
                for (var j = 0; j < days; j++) {
                    dy.options[j] = new Option(padLeft(2, j + 1, "0"), j + 1, true, true);
                }
                dy.selectedIndex = 0;
            }
        },
        Calendar: {
            List: ["YearMonthDay", "Hour"],
            Control: function (DateType) {
                var result = {}, selectObj, inputObj;
                result.Container = buildEle("div", { style: { "margin-bottom": "10px" } });

                result.Select = [];
                result.Input = [];
                result.ErrorSpan = buildEle("span", {
                    style: {
                        color: ColorSetting.ErrorMsg
                    }
                });

                var _list = DateType == CreateView.Date.Type.DateTime ? this.List : this.List.slice(0, 1);

                for (var i = 0; i < 2; i++) {
                    selectObj = {};
                    inputObj = {};

                    /*Create Input*/
                    inputObj[_list[0]] = buildEle("input", { type: "text", readOnly: true });
                    /*Append to Container*/
                    result.Container.appendChild(inputObj[_list[0]]);
                    if (DateType == CreateView.Date.Type.DateTime) {
                        /*Create Select*/
                        selectObj[_list[1]] = buildEle("select");
                        /*Append to Container*/
                        result.Container.appendChild(selectObj[_list[1]]);
                    }

                    i == 0 && result.Container.appendChild(buildEle("span", {
                        innerHTML: "&nbsp;&nbsp;~&nbsp;&nbsp;"
                    }));

                    result.Select.push(selectObj);
                    result.Input.push(inputObj);
                }

                result.Container.appendChild(buildEle("span", {
                    innerHTML: "&nbsp;&nbsp;"
                }));
                result.Select_checkoutduration = buildEle("select");
                result.Container.appendChild(result.Select_checkoutduration);

                result.Container.appendChild(result.ErrorSpan);

                /*Bind Event*/
                for (var i = 0; i < 2; i++) {
                    $(result.Input[i].YearMonthDay).change({ msg: result.ErrorSpan }, function (evt) {
                        CreateView.Date.Calendar.SetError(evt.data.msg, "");

                        if (this.value != "" && !CreateView.Date.Calendar.isValidDate(this.value)) {
                            CreateView.Date.Calendar.SetError(evt.data.msg, "日期格式錯誤!");
                            this.value = "";
                        }
                    });
                    if (DateType == CreateView.Date.Type.DateTime) {
                        $(result.Select[i].Hour).change({ msg: result.ErrorSpan }, function (evt) {
                            CreateView.Date.Calendar.SetError(evt.data.msg, "");
                        });
                    }
                }

                return result;
            },
            /*format yyyy-MM-dd*/
            isValidDate: function (dateString) {
                var regEx = /^\d{4}-\d{2}-\d{2}$/;
                return dateString.match(regEx) != null;
            },
            Init: function (DateType, obj, bindTarget) {
                var nowDate = new Date(), option;
                var yyyymmdd = nowDate.format(Report_Config.DateFormat.Normal), namespace = CreateView.Date.Calendar;
                var _list = namespace.List;

                for (var i = 0; i < 2; i++) {
                    $(obj.Input[i].YearMonthDay).datepicker();
                    $(obj.Input[i].YearMonthDay).datepicker("option", "dateFormat", Report_Config.DateFormat.DatePicker);

                    obj.Input[i].YearMonthDay.value = yyyymmdd;
                }

                if (DateType == CreateView.Date.Type.Date) {
                    bindTarget.StartTime = Control.RightView.Calendar.getSearch(DateType, obj, CreateView.Date.Type.Start);
                    bindTarget.EndTime = Control.RightView.Calendar.getSearch(DateType, obj, CreateView.Date.Type.End);

                    return;
                }

                /*Hour*/
                CreateView.Date.Set(obj, _list[1], 25, "00", -1);
                /*End Hour*/
                obj.Select[CreateView.Date.Type.End][_list[1]].value = "24";

                bindTarget.StartTime = Control.RightView.Calendar.getSearch(DateType, obj, CreateView.Date.Type.Start);
                bindTarget.EndTime = Control.RightView.Calendar.getSearch(DateType, obj, CreateView.Date.Type.End);
            },
            SetTime: function (DateType, obj, bindTarget, StartOrEndType, setTime) {
                var yyyymmdd = setTime.format(Report_Config.DateFormat.Normal), namespace = CreateView.Date.Calendar;
                var _list = namespace.List;
                                
                obj.Input[StartOrEndType].YearMonthDay.value = yyyymmdd;
                

                if (DateType == CreateView.Date.Type.Date) {
                    bindTarget.StartTime = Control.RightView.Calendar.getSearch(DateType, obj, StartOrEndType);
                    return;
                }

                /*Hour*/                
                obj.Select[StartOrEndType][_list[1]].value = setTime.format("HH");

                bindTarget.StartTime = Control.RightView.Calendar.getSearch(DateType, obj, StartOrEndType);
            },
            SetError: function (obj, msg) {
                if (!obj)
                    return;
                obj.innerText = msg;
            }
        }
    },
    Pop: {
        Window: function (ele, width) {
            ele.div_pop = buildEle("div", {
                className: "pop_window",
                style: {
                    "display": "none"
                }
            });

            ele.div_pop.appendChild(
                ele.bg = buildEle("div", {
                    className: "pop_window_bg"
                }));

            ele.div_pop.appendChild(
                ele.div_call = buildEle("div", {
                    className: "confirm_call",
                    style: {
                        "width": width + "px",
                        "margin-left": (-width / 2) + "px",
                        "border": "1px solid " + ColorSetting.TableTitle.Line
                    }
                }));

            ele.div_call.appendChild(
                ele.div_title = buildEle("div", {
                    className: "confirm_title",
                    style: {
                        "background-color": ColorSetting.TableTitle.BG                        
                    }
                }));
            ele.div_title.appendChild(
                ele.title = DeepCopyProps.call(document.createElement("span"), {
                    innerText: ""
                }));

            ele.div_title.appendChild(
                ele.btn_close = DeepCopyProps.call(document.createElement("span"), {
                    innerText: "X",
                    className: "pointer",
                    style: {
                        float: "right"
                    }
                }));

            ele.div_call.appendChild(
                ele.div_inner = buildEle("div", {
                    style: { padding: "5%" }
                }));

            ele.div_inner.appendChild(
                ele.inner_table = buildEle("table", {
                    style: { width: "100%" }
                }));

            /*Error*/
            ele.div_inner.appendChild(
                ele.error = buildEle("div", {

                }));

            ele.div_inner.appendChild(
                ele.div_footer = buildEle("div", {
                    style: {
                        "text-align": "center",
                        "margin-top": "5%"
                    }
                }));

            return ele;
        },
        table: function (ele, row) {
            ele.table_title_span = [];
            ele.table_content_td = [];
            var tr, td, title;
            for (var i = 0; i < row; i++) {
                tr = buildEle("tr", {
                    style: {
                        "border": "none"
                    }
                });
                ele.inner_table.appendChild(tr);

                /*title*/
                td = buildEle("td");
                tr.appendChild(td);
                title = buildEle("span");
                td.appendChild(title);
                ele.table_title_span.push(title);

                /*Content*/
                td = buildEle("td");
                tr.appendChild(td);
                ele.table_content_td.push(td);
            }

            return ele;
        },
        btn: function (text) {
            return buildEle("input", {
                type: "button",
                value: text,
                className: "confirmbtn btn leftBtn",
                style: {
                    "width": "65px",
                    "margin": "0"
                }
            });
        }
    },
    Table: {
        Title: function (table, titleArr) {
            var tr = WebTool.CreateElement("tr", {
                style: {
                    "color": ColorSetting.TableTitle.Font,
                    "background-color": ColorSetting.TableTitle.BG
                }
            });

            /*Title*/
            for (var i = 0; i < titleArr.length; i++) {
                DataSheet.insert_content(tr, titleArr[i]);
            }
            table.appendChild(tr);

            return table;
        }
    }
}

function _textList() {
    this.panel = null;
    this.table = buildEle("table", { style: { width: "100%" } });
    this.table.style.borderCollapse = "separate";
    this.Data = { Department: [], Name: [] };
    this.Active = false;
    this.isInsertData = true;
    this.lId = 0;
    this.Control = {
        Department: buildEle("select"),
        Name: buildEle("div", { style: { "margin": this.margin + "px" } }),
        SelectList: buildEle("div", { style: { "margin": this.margin + "px" } }),
        ErrorSpan: buildEle("span", {
            style: {
                "float": "right",
                "color": "red"
            }
        })
    };
    this.Select = {
        Department: "",
        Name: "",
        SelectList: [],
        QRCode: ""
    };
};
_textList.prototype = {
    Height: 150,
    margin: 9,
    //縮排寬度
    tabWidth: 15,
    selectCssName: "textListSelect",
    selectCss: {
        "color": "white",
        "background-color": "orange"
    },
    unselectCss: {
        "color": "",
        "background-color": ""
    },
    selectProperty: {
        Department: { id: "UpLevelTree", key: "uid", "val": "NickName" },
        Name: { id: "uid", key: "UpLevelTree", "val": "NickName" }
    },
    border_color: "black",
    GetUserList: function () {
        this.Reset();

        ws.Send("get.qruserlist", {
            GID: Reporter.GID
        });

    },

    SetUserList: function (data) {
        if (data.Record) {
            var _r = data.Record,
                _col = data.Column,
                id_index = _col.indexOf(textList.selectProperty["Name"].key);

            var sort_data = _r, tmp, count = 0;


            for (var i = 0; i < _r.length; i++) {
                sort_data = this._SortOrder(i, sort_data, id_index);
            }

            this.Data.Name.Record = sort_data;
        }
        this.Data.Name.Column = data.Column;
        this.Refresh("Name", "div");
    },
    _SortOrder: function (startIndex, arr, idIndex) {
        var sort_data = [], tmp, len;
        for (var i = 0; i < startIndex; i++) {
            sort_data.push(arr[i]);
        }

        for (var i = startIndex; i < arr.length; i++) {
            tmp = arr[i][idIndex].split(",");

            if (i == startIndex) {
                len = tmp.length;
            }

            if (tmp.length < len) {
                len = tmp.length;
                sort_data.splice(startIndex, 0, arr[i]);
            } else
                sort_data.push(arr[i]);
        }

        return sort_data;
    },
    SetComplete: function () {
        LoadingPanel.Show();
        $('#pop_win').hide();
        QRView.GetList();
    },
    Create: function () {
        if (!this.panel)
            return;

        this.Create = function () {

            if (this.panel) {

                this.CreateTableTr("人員:", this.Control.Name);

                this.panel.appendChild(this.table);

                this.Reset();

                this.Active = true;

                var btn = buildEle("input", {
                    "type": "button",
                    "value": Language.btn_Confirm,
                    "className": "contentBtn",
                    "style": {
                        "float": "right",
                        "margin": this.margin + "px"
                    }
                });

                var self = this;

                /*SetQRUser*/
                btn.onclick = function () {
                    LoadingPanel.Show();
                    ws.Send("set.qruser", {
                        GID: Reporter.GID,
                        data: textList.Select.SelectList[0],
                        qrcode: textList.Select.QRCode
                    });
                };

                this.panel.appendChild(btn);
                this.panel.appendChild(this.Control.ErrorSpan);
            }
        }

        var css = {
            "border": "1px solid " + this.border_color,
            "height": this.Height + "px",
            "overflow": "auto",
            "overflow-x": "hidden",
            "padding": "1px",
            "background-color": "white",
            "margin": "0",
            "margin-right": this.margin + "px"
        }

        $(this.Control.Name).css(css);

        $(this.Control.SelectList).css(css);

        this.Create();

    },

    CreateTableTr: function (title, content) {
        var tr = buildEle("tr");
        var td = buildEle("td", {
            style: {
                width: "75px",
                "vertical-align": "top",
                "padding-top": this.margin + "px"
            }
        });

        td.appendChild(buildEle("span", {
            innerText: title,
            style: {
                "margin-left": "5px",
            }
        }));

        tr.appendChild(td);

        td = buildEle("td", {
            style: {
                "padding-top": this.margin + "px"
            }
        });
        td.appendChild(content);

        tr.appendChild(td);

        this.table.appendChild(tr);

        return this.table;
    },
    Refresh: function (Type, ControlType) {
        this.Select.SelectList = [];
        $(this.Control[Type]).empty();

        var _data = this.Data[Type].Record, opt, _col = this.Data[Type].Column;

        if (_data.length > 0) {
            var tmp, text, spanDiv, tabCount = 0, parentDiv, span;
            var self = this;

            if (ControlType == "select") {
                opt = document.createElement("option");
                this.Control[Type].appendChild(opt);
            }

            for (var i = 0; i < _data.length; i++) {
                tmp = String(_data[i][_col.indexOf(this.selectProperty[Type].key)]).split(",");
                tmp.pop();

                tabCount = tmp.length;

                parentDiv = tmp.pop();

                ControlType == "select" ?
                this.CreateControl.Select(Type, this, _data[i], tabCount) :
                this.CreateControl.Div(Type, this, _data[i], tabCount - 2, parentDiv);
            }

            if (ControlType == "select") {
                this.Control[Type].onchange = function (evt) {
                    self._select(evt, Type);

                    self.CreateControl.Change[Type](self.Select[Type]);
                }
            }
        }
    },
    CreateControl: {
        Select: function (Type, obj, _data, tabCount) {
            var opt = document.createElement("option"), _col = textList.Data[Type].Column;
            var id_index = getIndex(_col, obj.selectProperty[Type].id),
                key_index = getIndex(_col, obj.selectProperty[Type].key),
                val_index = getIndex(_col, obj.selectProperty[Type].val);


            opt.className = "List" + _data[id_index];
            opt.value = _data[key_index];

            opt.id = _data[id_index];
            opt.innerHTML = "";
            //opt.style.paddingLeft = obj.tabWidth * tabCount + "px";
            for (var i = 0; i < tabCount * 4; i++) {
                opt.innerHTML += "&nbsp;";
            }
            opt.innerHTML += _data[val_index];

            obj.Control[Type].appendChild(opt);

            return obj;
        },
        Div: function (Type, obj, _data, tabCount, parentId) {
            var _col = textList.Data[Type].Column
            var id_index = _col.indexOf(obj.selectProperty[Type].id),
                key_index = getIndex(_col, obj.selectProperty[Type].key),
                val_index = getIndex(_col, obj.selectProperty[Type].val);

            var spanDiv = document.createElement("div");

            var self = obj, _div = WebTool.CreateElement("div");

            var span = document.createElement("span");
            span.className = "List" + _data[id_index];

            span.style.paddingLeft = self.tabWidth * tabCount + "px";
            span.appendChild(document.createTextNode(_data[val_index]));

            var idStr = "_divSpan";

            _div.uname = _data[val_index];
            _div.id = idStr + _data[id_index];
            _div.className = "pointer";

            _div.onclick = function (evt) {


                if (self.Select.SelectList.length > 0) {
                    $("#" + idStr + self.Select.SelectList[0].id).css(self.unselectCss);
                }

                self.CreateControl.Change[Type](
                    this.id.replace(idStr, ""),
                    evt.currentTarget.uname, self);

                if (self.Select.SelectList.length > 0) {
                    $("#" + idStr + self.Select.SelectList[0].id).css(self.selectCss);
                }
            }

            _div.appendChild(span);
            spanDiv.appendChild(_div);

            var parentDiv = document.getElementsByClassName("List" + parentId);
            if (!parentDiv || parentId == "" || parentDiv.length == 0) {
                obj.Control[Type].appendChild(spanDiv);
            } else
                parentDiv[0].parentNode.parentNode.appendChild(spanDiv);

            return obj;
        },
        Change: {
            Department: function (id) {
                ajaxExcuting("userdesc", "get", "userlist", 'textList.SetUserList',
                    {
                        id: id,
                        "autcode": pageColumn.autcode
                    }
                );
            },
            Name: function (id, name, obj) {
                obj.PushSelect(id, name);
            }
        },
    },
    PushSelect: function (id, name) {
        var unselect = false;
        for (var i = 0; i < this.Select.SelectList.length; i++) {
            if (String(this.Select.SelectList[i].id) == String(id)) {
                unselect = true;
                this.Select.SelectList.splice(i, 1);
                break;
            }
        }

        if (unselect) {
            return;
        }

        this.Select.SelectList[0] = {
            id: id, name: name
        };

    },
    addHeader: function () {
        this.addHeader = function () { }

        var _tmp = {};
        _tmp["." + this.selectCssName] = this.selectCss;

        $('head').append("<style type='text/css'>" +
            Tools.Utility.ToStyle(_tmp)
        + "</style>")
    },
    _select: function (evt, Type) {
        this.Select[Type] = evt.type == "change" ? evt.currentTarget.value : evt.currentTarget[this.selectProperty[Type].key];


    },
    Reset: function () {
        this.Data.Department = [];
        this.Data.Name = [];

        this.Select.Department = "";
        this.Select.Name = "";
        this.Select.SelectList = [];
        this.Select.QRCode = "";

        this.Control.Department.innerHTML = '';
        this.Control.Name.innerHTML = '';
        this.Control.SelectList.innerHTML = '';
        this.Control.ErrorSpan.innerText = "";

    },
    SetError: function (msg) {
        this.Control.ErrorSpan.innerText = msg;
    }
}

var textList = new _textList();