﻿DataSheet.Game_ = {
    _Record: {
        name: {
            col: "廳別",
            Value: "",
            Center: true,
            Set: function (val) {
                if (val == null)
                    return;
                this.Value = val;
            }
        },
        time: {
            col: "結算時間",
            Value: "",
            Set: function (val) {
                if (val) {
                    //var date = new Date(ParseDate(val));
                    val = val.Year + '/' + val.Month + '/' + val.Day + " " + val.Hour + ":" + val.Minute;
                    this.Value = val;
                } else
                    this.Value = "";

            }
        },
        round: {
            col: "回合",
            Value: "",
            Center: true,
            Set: function (val) {
                if (val == null)
                    return;

                this.Value = val;
            }
        },
        pokers: {
            col: "發牌結果",
            addText: ["閒", "莊"],
            Value: "",
            Set: function (val) {
                if (val == null)
                    return;

                var result = val.split(",");
                var _p = [], _b = [], _point, _poker, _r;
                for (var i = 0; i < result.length; i++) {
                    if (result[i] == "0") {
                        continue;
                    }

                    _poker = DataSheet[DataSheet.GameSetting].Get.Poker.Suit(result[i]);
                    _point = DataSheet[DataSheet.GameSetting].Get.Poker.Point(result[i]);

                    _r = DataSheet[DataSheet.GameSetting].eNum.Suit[_poker - 1] + DataSheet[DataSheet.GameSetting].eNum.Poker[_point - 1];
                    i % 2 ? _b.push(_r) : _p.push(_r);
                }

                this.Value = this.addText[0] + ":" + _p.join(",") + "<br/>" + this.addText[1] + ":" + _b.join(",");
            }
        },
        totalwin: {
            col: "總贏分",
            Value: "",
            Center: true,
            Set: function (val) {
                if (val == null)
                    return;

                this.Value = val;
            }
        },
        refund: {
            col: "退水",
            Value: "",
            Center: true,
            Set: function (val) {
                if (val == null)
                    return;

                this.Value = val;
            }
        },
        bets: {
            col: ["押閒", "押莊", "押和", "押閒對", "押莊對", "押莊六"],
            Value: [],
            Center: true,
            Set: function (val) {
                if (val == null)
                    return;

                this.Value = [];
                var tmp = val.split(",");

                for (var i = 0; i < tmp.length; i++) {
                    this.Value.push(tmp[i]);
                }

                if (this.Value.length == 5) {
                    this.Value.push("0");
                }
            }
        },
        points: {
            col: "發牌點數",
            addText: ["閒", "莊"],
            Value: "",
            Center: true,
            Set: function (val) {
                if (val == null)
                    return;

                var result = val.split(":");

                if (result.length == 2)
                    this.Value = this.addText[0] + result[0] + " : " + this.addText[1] + result[1];
            }
        },
        win: {
            col: "本局結果",
            addText: ["閒贏", "莊贏", "和"],
            Value: "",
            Center: true,
            Set: function (val) {
                if (val == null)
                    return;

                switch (val) {
                    case 0:
                        this.Value = this.addText[0];
                        break;
                    case 1:
                        this.Value = this.addText[1];
                        break;
                    case 2:
                        this.Value = this.addText[2];
                        break;
                    default:
                        this.Value = "";
                }

            }
        },
        fee: {
            col: "押注規則",
            addText: ["莊六", "不抽佣"],
            Value: "",
            Center: true,
            Set: function (val) {
                if (val == null)
                    return;

                this.Value = this.addText[Number(val)];
            }
        },
    },
    //_col: ["廳別", "結算時間", "回合", "發牌結果", "發牌點數", "本局結果", "總贏分", "退水", "押閒", "押莊", "押和", "押閒對", "押莊對"],
    _col: ["name", "time", "round", "points", "pokers", "win", "totalwin", "refund", "bets", "fee"],
    _width: [200, 150, 150, 180, 220, 220, 220, 220, 250],
    Get: {
        Poker: {
            //花色index轉換
            Suit: function (index) {
                index = typeof index == "string" ? Number(index) : index;

                return (Math.floor((index - 1) / 13) + 1);
            },
            //點數index轉換
            Point: function (index) {
                index = typeof index == "string" ? Number(index) : index;

                var result = index - Math.floor(index / 13) * 13;

                if (result == 0) {
                    result = 13;
                }

                return result;
            },
            //花色點數轉換index
            Index: function (Suit, Point) {
                return (Suit - 1) * 13 + Point;
            }
        }
    },
}