﻿$(document).ready(function () {
    
    //GetData("mainShop");
    ajaxData["MainMenu"] = new ajaxD("MainMenu");
    ajaxData["subMenu"] = new ajaxD("subMenu");
    ajaxData["pageChange"] = new ajaxD("pageChange");
    //ajaxData["MainMenu"].paraments = {
    //    pa: pa, newpa: newpa, newpaconfirm: newpaconfirm
    //}

    GetData("MainMenu");
});



var marginLeft = 10;
var TableWidth = 100;
var widthArr = ["8%", "8%", "8%", "8%", "8%", "15%", "10%", "8%", "6%", "6%", "5%"];
var TitleArr = ["組織ID", "組織名", "負責人", "組織分紅%數", "水錢%數", "地址", "電話", "停權", "修改", "子公司", "銷售紀錄"];

var widthEmArr = ["10%", "35%", "25%", "20%", "10%"];
var TitleEmArr = ["組織ID", "負責人", "推薦序號", "銷售紀錄", "修改"];
//總銷售額
var sumPrice = 0;

//分頁相關
var nowPage = 1, sumPage = 0, pageCount = 10,nowShopID = 0;

function mainShopResult(data) {
    $("#shopIndex").empty();
    var cooperate = $.parseJSON(data["cooperate"]);

    var aut = data["aut"];

    var tbl = createTitle(TableWidth + "", TitleArr, widthArr, data, aut, 0);

    for (var key in data) {
        tbl = apData(cooperate, "1", tbl);
    }

    $("#shopIndex").append(tbl);
}

function createTitle(width, TitleArr, widthArr, aut, showType) {
    var tbl = document.createElement("table");
    var row = document.createElement("tr");

    for (var i = 0; i < TitleArr.length; i++) {        
        row = createTitleTd(TitleArr[i], row, widthArr[i]);
    }

    row.style.backgroundColor = "Blue";
    row.style.color = "white";
    row.style.height = "22px";
    row.style.textAlign = "center";

    tbl.setAttribute('border', '0');    
    tbl.style.width = width + "%";

    tbl.appendChild(row);
    
    return tbl;
    //$(divId).append(tbl);
}

function createTitleTd(msg, tr, width) {
    var cell = document.createElement("td");
    cell.appendChild(document.createTextNode(msg));
    cell.className = "text-center controlmargin";
    cell.style.height = "22px";
    //cell.style.margin = "0";
    cell.style.border = "1px solid black";
    if (width != "0") {
        cell.style.width = width;
    }
    tr.appendChild(cell);
    return tr;
}

function createTd(msg, tr, center) {
    var cell = document.createElement("td");
    cell.appendChild(document.createTextNode(msg));
    cell.className = "text-center controlmargin";
    if (center) {
        cell.style.textAlign = "center";
    }
    cell.style.border = "1px solid black";
    
    tr.appendChild(cell);
    return tr;
}

function subShopResult(data) {
    nowPage = 1;
    $("#shopIndex").empty();
    var aut = data["aut"];

    var tbl = createTitle(TableWidth + "", TitleArr, widthArr, aut, 0);

    var cooperate = $.parseJSON(data["cooperate"]);

    var employee = $.parseJSON(data["em"]);

    var discountPrice = 0.0;

    for (var key in cooperate) {        
        tbl = apData(cooperate[key], "1", tbl, aut);        
    }

    if (cooperate.length > 0) {
        discountPrice = parseFloat(cooperate[0].ShopBonus);
    }

    $("#shopIndex").append(tbl);

    $("#employeeIndex").empty();

    sumPage = Math.ceil(employee.length / pageCount);
    //append page
    //apPage();

    tbl = createTitle(TableWidth + "", TitleEmArr, widthEmArr, 0, 1);
    sumPrice = 0;
    for (var key in employee) {
        if(key < pageCount)
            tbl = apEmData(employee[key], tbl);        
    }
    discountPrice = discountPrice * sumPrice / 100;

    document.getElementById("sumPrice").innerText = sumPrice;

    document.getElementById("discountPrice").innerText = discountPrice;
    $("#employeeIndex").append(tbl);
    $("#sumInfo").show();
    $("#title").show();

    $("#pageIndex").empty();
    $("#pageIndex").append(apPage());
}

//公司資訊
function apData(data, level, tbl, aut) {
    var row = document.createElement("tr");

    var cell = document.createElement("td");
    
    row = createTd(data.UID, row, true);
    row = createTd(data.ShopName, row, true);
    row = createTd(data.Name, row, true);
    row = createTd(data.ShopBonus, row, true);
    row = createTd(data.Refund, row, true);
    
    row = createTd(data.ShopAddress, row);
    row = createTd(data.ShopPhone, row, true);

    cell = document.createElement("td");
    cell.appendChild(document.createTextNode(data.Resignation == "1" ? "已停權" : "授權"));
    cell.className = "text-center controlmargin";
    cell.style.textAlign = "center";
    cell.style.border = "1px solid black";

    row.appendChild(cell);

    var edit = document.createElement("a");
    
    cell = document.createElement("td");

    edit.href = "fShopUpdate.aspx?ShopID=" + data.UID;
    edit.text = "修改";
    edit.style.color = "Red";
    cell.appendChild(edit);
    cell.className = "text-center controlmargin";
    cell.style.border = "1px solid black";
    cell.style.textAlign = "center";
    row.appendChild(cell);

    cell = document.createElement("td");
    edit = document.createElement("a");
    edit.href = "fShopAdd.aspx?ShopID=" + data.UID;
    edit.text = "新增";
    edit.style.color = "Red";
    cell.appendChild(edit);
    cell.className = "text-center controlmargin";
    cell.style.border = "1px solid black";
    cell.style.textAlign = "center";
    row.appendChild(cell);
    

    cell = document.createElement("td");
    var s1 = document.createElement("a");
    s1.href = "fcafeRecordsCheck.aspx?ShopID=" + data.UID;
    s1.text = "查詢";
    s1.style.color = "Red";
    cell.appendChild(s1);
    cell.className = "text-center controlmargin";
    cell.style.border = "1px solid black";
    cell.style.textAlign = "center";
    row.appendChild(cell);

    //cell = document.createElement("td");
    //edit = document.createElement("a");
    //edit.href = "fEmployeeAdd.aspx?ShopID=" + data.EmployeeID;
    //edit.text = "新增";
    //edit.style.color = "Red";
    //cell.appendChild(edit);
    //cell.className = "text-center controlmargin";
    //cell.style.border = "1px solid black";
    //cell.style.textAlign = "center";
    //row.appendChild(cell);
    
    tbl.appendChild(row);

    row = document.createElement("tr");
    row.id = "subAppendTr" + data.ShopID + "_" + level;
    row.style.display = "none";

    cell = document.createElement("td");

    cell.colSpan = TitleArr.length;
    cell.id = "subAppend" + data.ShopID + "_" + level;
    cell.style.border = "1px solid black";
    row.appendChild(cell);

    tbl.appendChild(row);

    return tbl;
}

//下線組織列表
function apEmData(data, tbl) {
    var row = document.createElement("tr");

    var cell = document.createElement("td");

    
    row = createTd(data.UID, row, true);

    row = createTd(data.Name, row, true);

    row = createTd(data.Code, row, true);

    if (data.Price == null) {
        row = createTd("0", row, true);
    } else {
        sumPrice += parseInt(data.Price);
        var now = new Date();

        var now_format = formatDate(now);

        cell = document.createElement("td");
        var edit = document.createElement("a");
        edit.href = "fEmpContent.aspx?employeeID=" + data.UID + "&CreateDate=" + now_format;
        edit.text = data.Price;
        edit.style.color = "Red";
        cell.appendChild(edit);
        cell.className = "text-center controlmargin";
        cell.style.border = "1px solid black";
        cell.style.textAlign = "center";
        row.appendChild(cell);


        //row = createTd(data.Price, row, true);
    }
    cell = document.createElement("td");
    edit = document.createElement("a");
    edit.href = "fEmployeeAdd.aspx?ShopID=" + data.UpLevelId + "&EmployeeID=" + data.UID;
    edit.text = "修改";
    edit.style.color = "Red";
    cell.appendChild(edit);
    cell.className = "text-center controlmargin";
    cell.style.border = "1px solid black";
    cell.style.textAlign = "center";
    row.appendChild(cell);
    
    tbl.appendChild(row);

    return tbl;
}

function apPage() {    
    var tbl = document.createElement("table");
    var row = document.createElement("tr");

    var cell = document.createElement("td");
    
    for (var i = 1; i <= sumPage; i++) {
        var a;
        if (nowPage == i) {
            a = document.createElement("span");
            a.style.color = "white";
            a.style.marginRight = "5px";
            a.innerText = i;            
        } else {
            a = document.createElement("a");
            a.style.cursor = "pointer";
            a.setAttribute("pageValue", i);
            a.onclick = function (e) {
                ajaxData.pageChange.paraments = {
                    pageIndex: e.target.attributes["pageValue"].value,
                    pageCount: pageCount,
                    id: nowShopID
                };

                nowPage = parseInt(e.target.attributes["pageValue"].value);
                GetData("pageChange");
            }
            a.style.textDecoration = "underline";
            a.style.color = "white";
            a.style.marginRight = "5px";
            a.text = i;            
        }

        cell.appendChild(a);
    }

    cell.className = "text-center controlmargin";
    cell.style.height = "22px";

    //cell.style.margin = "0";
    cell.style.border = "1px solid black";
    cell.style.borderTop = "none";
    row.appendChild(cell);

    row.style.backgroundColor = "Blue";
    row.style.color = "white";
    row.style.height = "22px";
    row.style.textAlign = "center";

    tbl.setAttribute('border', '0');
    tbl.style.width = "100%";

    tbl.appendChild(row);

    return tbl;
}

//format date 格式
function formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    //if (day.length < 2) day = '0' + day;

    return [year, month].join('-');
}

function pageChangeResult(data) {
    $("#employeeIndex").empty();

    var employee = $.parseJSON(data["em"]);

    var tbl = createTitle(TableWidth + "", TitleEmArr, widthEmArr, 0, 1);
    for (var key in employee) {
        if (key < pageCount)
            tbl = apEmData(employee[key], tbl);
    }

    $("#employeeIndex").append(tbl);

    $("#pageIndex").empty();
    $("#pageIndex").append(apPage());
}

function MainMenuResult(data) {
    $("#menu").empty();
    var tbl = document.createElement("table");
    tbl.style.width = "100%";
    for (var key in data) {
        tbl = apMenuData(data[key], "1", tbl);
    }

    $("#menu").append(tbl);
}

function subMenuResult(data) {
    var preLevel = parseInt(ajaxData.subMenu.paraments["buttonLevel"]);
    $("#menu_subAppend" + ajaxData.subMenu.paraments["id"] + "_" + preLevel).empty();

    var TWidth = TableWidth - marginLeft;

    var tbl = document.createElement("table");
    tbl.style.marginLeft = marginLeft + "%";

    var row = document.createElement("tr");
    var cell = document.createElement("td");

    if (data.length != 0) {
        var nowLevel = preLevel + 1;

        for (var key in data) {
            tbl = apMenuData(data[key], nowLevel, tbl);

        }
        //} else {
        //    //row = document.createElement("tr");        
        //    //cell = document.createElement("td");

        //    cell.colSpan = 8;
        //    cell.style.border = "1px solid black";
        //    cell.appendChild(document.createTextNode("無資料"));
        //    cell.className = "text-center controlmargin";        
        //    cell.style.textAlign = "center";

        //    row.appendChild(cell);

        //    tbl.appendChild(row);
        $("#menu_subAppend" + ajaxData.subMenu.paraments["id"] + "_" + preLevel).append(tbl);
        $("#menu_subAppendTr" + ajaxData.subMenu.paraments["id"] + "_" + preLevel).show();
    }
}

//append Menu
function apMenuData(data,level,tbl){
    var row = document.createElement("tr");
    var cell = document.createElement("td");

    row.style.borderBottom = "1px solid black";
    //row = createTd("︱_", row, false);

    var btn = document.createElement("button");
    btn.type = "button";
    btn.textContent = "+";
    btn.id = "menu_show_" + data.UID + "_" + level;
    btn.setAttribute("shopValue", data.UID);
    btn.setAttribute("LevelValue", level);    
    btn.onclick = function (e) {
        e.target.style.display = "none";
        $("#menu_hide_" + e.target.attributes["shopValue"].value + "_" + level).show();

        ajaxData.subMenu.paraments = {
            id: e.target.attributes["shopValue"].value,
            buttonLevel: e.target.attributes["LevelValue"].value
        };        

        GetData("subMenu");
    }
    btn.className = "btn btn-default btn-xs";
    //btn.style.padding = "2px";
    //btn.style.paddingTop = "2px";
    //btn.style.textAlign = "start";
    cell.appendChild(btn);
    //cell.className = "text-center ";

    btn = document.createElement("button");
    btn.style.display = "none";
    btn.type = "button";
    btn.textContent = "-";
    btn.id = "menu_hide_" + data.UID + "_" + level;
    btn.setAttribute("shopValue", data.UID);
    btn.setAttribute("LevelValue", level);

    btn.onclick = function (e) {
        e.target.style.display = "none";
        $("#menu_show_" + e.target.attributes["shopValue"].value + "_" + level).show();

        $("#menu_subAppendTr" + e.target.attributes["shopValue"].value + "_" + level).hide();
    }
    btn.className = "btn btn-default btn-xs";
    cell.appendChild(btn);

    //cell.style.border = "1px solid black";
    //cell.style.textAlign = "center";
    row.appendChild(cell);

    cell = document.createElement("td");
    edit = document.createElement("a");
    edit.style.cursor = "pointer";
    edit.setAttribute("shopValue", data.UID);
    edit.setAttribute("LevelValue", level);
    edit.onclick = function (e) {
        ajaxData.subShop.paraments = {
            id: e.target.attributes["shopValue"].value,
            buttonLevel: e.target.attributes["LevelValue"].value
        };

        nowShopID = e.target.attributes["shopValue"].value;
        GetData("subShop");

    }
    //edit.href = "fEmployeeAdd.aspx?ShopID=" + data.ShopID;
    edit.text = data.ShopName;
    //edit.style.color = "Red";
    cell.appendChild(edit);
    cell.className = "controlmargin";
    //cell.style.border = "1px solid black";
    //cell.style.textAlign = "center";
    row.appendChild(cell);

    tbl.appendChild(row);

    row = document.createElement("tr");
    row.id = "menu_subAppendTr" + data.UID + "_" + level;
    row.style.display = "none";

    cell = document.createElement("td");

    cell.colSpan = TitleArr.length;
    cell.id = "menu_subAppend" + data.UID + "_" + level;
    //cell.style.border = "1px solid black";
    row.appendChild(cell);

    tbl.appendChild(row);

    return tbl;
}
