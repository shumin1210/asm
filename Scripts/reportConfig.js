﻿/// <reference path="_reference.js" />

var Day_Trans_Report_View = function () {
    var self = {};
    var ele = {}, Data, _id = "Day_Trans_Report_View";

    var check_list = [];
    var search_select = {
        start: {
            year: buildEle("select"),
            month: buildEle("select"),
            day: buildEle("select")
        },
        end: {
            year: buildEle("select"),
            month: buildEle("select"),
            day: buildEle("select")
        }
    };

    ele.Div = buildEle("div", {
        id: _id + "_DIV",
        style: {
            "display": "none",
            "width": "84%",
            "padding-top": "10px",
            "padding-left": "1%"
        }
    });

    self.Select = {
        Year: [],
        Month: [],
        Day: [],
        HH: [],
        Min: [],
        Initial: function () {
            var select = document.getElementById(_id + "yy");
            var select2 = document.getElementById(_id + "yyEnd");
            var nowDate = new Date(), option;
            var txt = nowDate.getFullYear();

            this.Set(_id + "Year", 10, txt, txt - 10);
            this.Set(_id + "Month", 12, padLeft("2", nowDate.getMonth() + 1, "0"));

            this._SelDate();
            document.getElementById(_id + "dd").value = nowDate.getDate();
            document.getElementById(_id + "ddEnd").value = nowDate.getDate();

            this.Set(_id + "HH", 24, "00", -1);
            this.Set(_id + "Min", 60, "00", -1);
            document.getElementById(_id + "HHEnd").value = "23";
            document.getElementById(_id + "minEnd").value = "59";

            RightView.Search.StartTime = Control.RightView.getSearch("");
            RightView.Search.EndTime = Control.RightView.getSearch("End");
        },
        Set: function (type, length, defaultText, start_index) {
            var select, start, end;
            for (var i = 0; i < this[type].length; i++) {
                select = document.getElementById(this[type][i]);
                start = start_index == -1 ? 0 : start_index ? start_index : 1;
                end = start_index ? start_index + length : length;
                for (var j = start; j <= end; j++) {
                    if (select)
                        select.appendChild(this._setOption(j));
                }

                if (select && defaultText)
                    select.value = defaultText;

            }
        },
        _setOption: function (val) {
            var option = document.createElement("option");
            option.text = padLeft(2, val, "0");
            option.value = padLeft(2, val, "0");

            return option;
        },
        _SelDate: function () {
            var dy, mth, yr;
            for (var i = 0; i < this.Year.length; i++) {
                this.SelDate(this.Day[i], this.Month[i], this.Year[i]);
            }
        },
        SelDate: function (dd, mm, yy) {
            var dy = document.getElementById(dd);
            var mth = document.getElementById(mm);
            var yr = document.getElementById(yy);
            dy.options.length = 0;

            if (mth.value && yr.value) {
                var days = new Date(yr.value, mth.value, 1, -1).getDate(); // the first day of the next month minus 1 hour
                for (var j = 0; j < days; j++) {
                    dy.options[j] = new Option(padLeft(2, j + 1, "0"), j + 1, true, true);
                }
                dy.selectedIndex = 0;
            }
        }
    }

    self.searchDiv = buildEle("div", { id: "searchDiv", style: { "margin-top": "10px" } });

    /*yy-----------------*/
    search_select.start.year.onchange = function () { self.Select.SelDate('dd', 'mm', 'yy') };
    self.searchDiv.appendChild(search_select.start.year);

    self.searchDiv.appendChild(buildEle("span", { innerHTML: "&nbsp;-&nbsp;" }));

    /*mm-----------------*/
    search_select.start.month.onchange = function () { self.Select.SelDate('dd', 'mm', 'yy') };
    self.searchDiv.appendChild(search_select.start.month);

    self.searchDiv.appendChild(buildEle("span", { innerHTML: "&nbsp;-&nbsp;" }));

    /*dd-----------------*/
    self.searchDiv.appendChild(search_select.start.day);
    self.searchDiv.appendChild(buildEle("span", { innerHTML: "&nbsp;&nbsp;" }));
    
    /*end*/
    /*yy-----------------*/
    search_select.end.year.onchange = function () { self.Select.SelDate('ddEnd', 'mmEnd', 'yyEnd') };
    self.searchDiv.appendChild(search_select.end.year);

    self.searchDiv.appendChild(buildEle("span", { innerHTML: "&nbsp;-&nbsp;" }));

    /*mm-----------------*/
    search_select.end.month.onchange = function () { self.Select.SelDate('ddEnd', 'mmEnd', 'yyEnd') };
    self.searchDiv.appendChild(search_select.end.month);

    self.searchDiv.appendChild(buildEle("span", { innerHTML: "&nbsp;-&nbsp;" }));

    /*dd-----------------*/
    self.searchDiv.appendChild(search_select.end.day);

    self.searchDiv.appendChild(buildEle("span", { innerHTML: "&nbsp;&nbsp;" }));
    
    ele.searchBtn = buildEle("input", {
        type: "button", id: "SearchBtn",
        value: Language.btn_Search
    });

    ele.searchBtn.onclick = function () { Control.RightView.Search(); }

    self.searchDiv.appendChild(ele.searchBtn);

    self.Init = function () {
        $("body").append(ele.Div);
    }
    self.Show = function () {
        ele.Div.style.display = "inline-block";
    }
    self.Hide = function () {
        $(ele.Div).hide();
    }

    self.Refresh = function () {
        
    }

    Object.defineProperties(self, {
        Data: {
            get: function () { return Data; },
            set: function (val) { Data = val; }
        },
        CheckList: {
            get: function () { return check_list; },
            set: function (val) { check_list = val; }
        },
        game_ws: {
            get: function () { return game_ws; }
        }
    })

    return self;
}();