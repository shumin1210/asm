﻿/// <reference path="../jquery/jquery-1.12.3.min.js" />
/// <reference path="../jquery/Tools.js" />
/// <reference path="data.js" />

function setLayout(layout) {
    Layout = layout;

    Login.Hide();
    //MainPage.Hide();
    ReportTitle.Show();    
    LeftView.Hide();
    RightView.Hide();
    
    switch (Layout) {
        case Layouts.LOGIN:
            ReportTitle.Hide();
            Login.Show();            
            break;
        //case Layouts.MAINPAGE:            
        //    MainPage.Show();
        //    break;
        case Layouts.LAYER1:
        case Layouts.LAYER2:
        case Layouts.LAYER3:
        case Layouts.LAYER4:
            LeftView.Show();
            RightView.Build();
            RightView.Show.Data();            
            break;
        default:

    }

    LoadingPanel.Hide();
}

var LeftView = function () {
    var self = {};
    var ele = Object.create(null), Data;

    var check_list = [], game_ws = [];

    self.check_list = [];
    self.check_name = "ck_gamelist";

    ele.Div = buildEle("div", {
        "className": "adjustHeight",
        style: { display: "none", width: "20%", "border-right": "1px solid " + Reporter.BorderColor }
    });

    var div = buildEle("div", {
        style: { "text-align": "center", "padding-top": "10px", "font-size": "15px", "color": "blue" }
    });
    
    ele.Div.appendChild(div);

    ele.gameListDiv = buildEle("div", {
        id: "Left_gameList",
        style:{"text-align":"left"}
    });

    ele.Div.appendChild(ele.gameListDiv);
    
    self.Set = function () {
        $(ele.gameListDiv).empty();

        if (!Data) {
            return;
        }

        var col = Data.Column, row = Data.Record, container;                

        for (var i = 0; i < row.length; i++) {
            container = buildEle("div", { style: {"padding":"5px"}});

            var checkbox = document.createElement('input');
            checkbox.type = "checkbox";
            checkbox.name = self.check_name;
            checkbox.value = row[i][1];
            checkbox.id = "ck_" + row[i][0];
            checkbox.gid = row[i][0];
            checkbox.gtitle = row[i][2];
            checkbox.style.width = "15px";
            checkbox.checked = true;
            checkbox.onchange = function () {
                Control.LeftView.Confirm(true);
            }

            var label = document.createElement('label')
            label.htmlFor = "ck_" + row[i][0];
            label.appendChild(document.createTextNode(row[i][2]));

            container.appendChild(checkbox);
            container.appendChild(label);
            
            ele.gameListDiv.appendChild(container);
        }

    }
    
    self.Init = function () {
        $("body").append(ele.Div);
    }
    self.Show = function () {
        ele.Div.style.display = "inline-block";
    }
    self.Hide = function () {
        $(ele.Div).hide();
    }

    Object.defineProperties(self, {
        Data: {
            get: function () { return Data; },
            set: function (val) { Data = val;}
        },
        CheckList: {
            get: function () { return check_list; },
            set: function (val) { check_list = val;}
        },
        game_ws: {
            get: function () { return game_ws;}
        }
    })

    return self;
}();


var RightView = function () {
    var self = {};
    var ele = Object.create(null), Data_Addtext = [], DataTop = [];
    var Column = [], table;

    self.Data = {
        KeyColumn: "UserID",
        NameColumn:"Name",
        TopTree:[],
        DataTop: {
            Column:[],
            Record: []
        },
        CompanyList: {
            ReceivePlayerID: 0,
            ReceivePlayerName: "",
            Column: [],
            Record: [],
            List:[],            
            Add: function (data, GID, GName) {
                var isFind = -1, UserIDIndex = this.Column.indexOf(self.Data.KeyColumn), UserNameIndex = this.Column.indexOf(self.Data.NameColumn);
                for (var i = 0; i < this.List.length; i++) {
                    if (this.List[i].UserID == data[UserIDIndex]) {
                        isFind = i;
                        break;
                    }
                }

                if (isFind != -1) {
                    this.List[i].Add(data, GID, GName, this.Column);
                } else {
                    var tmp = GameRecordPack.UserTotal.Get(data[UserIDIndex], data[UserNameIndex]);
                    tmp.Add(data, GID, GName, this.Column);
                    this.List.push(tmp);

                    delete tmp;
                }
            },
            Init: function () {
                for (var i = 0; i < this.List.length; i++) {
                    for (var j = 0; j < this.List[i].GameRecord.length; j++) {
                        GameRecordPack.GameRecord.Add(this.List[i].GameRecord[j]);
                    }
                    
                    GameRecordPack.UserTotal.Add(this.List[i]);
                }

                this.List = [];

                return this;
            }
        },
        MemberList: {
            ReceivePlayerID: 0,
            ReceivePlayerName: "",
            Column: [],
            Record: [],
            InsertFirst:true,
            Add: function (data) {                
                for (var i = 0; i < data.length; i++) {
                    for (var j = 0; j < data[i].length; j++) {
                        this.Record[j] = isNaN(data[i][j]) ? data[i][j] : this.Record[j] + Number(data[i][j]);
                    }
                }

            },            
        },
        GameList: {
            ReceivePlayerID: 0,
            ReceivePlayerName: ""
        },
        GameDetail: {
            ReceiveGID: 0,
            ReceiveGtitle: "",
            Column: [],
            Record:[]
        }
    }

    ele.Div = buildEle("div", {
        style: {
            "display": "none",
            "width": "78%",
            "padding-top": "10px",
            "padding-left": "1%"
        }
    });

    var today_dateTime=

    self.Select = {
        Year: [],
        Month: [],
        Day: [],
        HH: [],
        Min:[],
        Initial: function () {
            var select = document.getElementById("yy");
            var select2 = document.getElementById("yyEnd");
            var nowDate = new Date(), option;
            var txt = nowDate.getFullYear();
            this.Set("Year", 10, txt, txt - 10);

            this.Set("Month", 12, padLeft("2", nowDate.getMonth() + 1, "0"));

            this._SelDate();
            document.getElementById("dd").value = nowDate.getDate();
            document.getElementById("ddEnd").value = nowDate.getDate();

            this.Set("HH", 24, "00", -1);
            this.Set("Min", 60, "00", -1);
            document.getElementById("HHEnd").value = "23";
            document.getElementById("minEnd").value = "59";

            RightView.Search.StartTime = Control.RightView.getSearch("");
            RightView.Search.EndTime = Control.RightView.getSearch("End");
        },
        Set: function (type, length, defaultText, start_index) {
            var select, start, end;
            for (var i = 0; i < this[type].length; i++) {
                select = document.getElementById(this[type][i]);
                start = start_index == -1 ? 0 : start_index ? start_index : 1;
                end = start_index ? start_index + length : length;
                for (var j = start; j <= end; j++) {
                    if (select)
                        select.appendChild(this._setOption(j));
                }

                if (select && defaultText)
                    select.value = defaultText;

            }
        },
        _setOption: function (val) {
            var option = document.createElement("option");
            option.text = padLeft(2, val, "0");
            option.value = padLeft(2, val, "0");

            return option;
        },
        _SelDate: function () {
            var dy, mth, yr;
            for (var i = 0; i < this.Year.length; i++) {
                this.SelDate(this.Day[i], this.Month[i], this.Year[i]);
            }

        },
        SelDate: function (dd, mm, yy) {
            var dy = document.getElementById(dd);
            var mth = document.getElementById(mm);
            var yr = document.getElementById(yy);
            dy.options.length = 0;

            if (mth.value && yr.value) {
                var days = new Date(yr.value, mth.value, 1, -1).getDate(); // the first day of the next month minus 1 hour
                for (var j = 0; j < days; j++) {
                    dy.options[j] = new Option(padLeft(2, j + 1, "0"), j + 1, true, true);
                }
                dy.selectedIndex = 0;
            }
        }
    }

    self.searchDiv = buildEle("div", { id: "searchDiv", style: { "margin-top": "10px" } });

    /*yy-----------------*/
    ele.yy = buildEle("select", { id: "yy" });
    ele.yy.onchange = function () { self.Select.SelDate('dd', 'mm', 'yy') };
    self.searchDiv.appendChild(ele.yy);

    ele.yyText = buildEle("span", { id: "yyText", innerHTML: "&nbsp;-&nbsp;" });
    self.searchDiv.appendChild(ele.yyText);

    /*mm-----------------*/
    ele.mm = buildEle("select", { id: "mm" });
    ele.mm.onchange = function () { self.Select.SelDate('dd', 'mm', 'yy') };
    self.searchDiv.appendChild(ele.mm);

    ele.mmText = buildEle("span", { id: "mmText", innerHTML: "&nbsp;-&nbsp;" });
    self.searchDiv.appendChild(ele.mmText);

    /*dd-----------------*/
    ele.dd = buildEle("select", { id: "dd" });
    self.searchDiv.appendChild(ele.dd);

    ele.ddText = buildEle("span", { id: "ddText", innerHTML: "&nbsp;&nbsp;" });
    self.searchDiv.appendChild(ele.ddText);

    /*HH-----------------*/
    ele.HH = buildEle("select", { id: "HH" });
    self.searchDiv.appendChild(ele.HH);

    ele.HHText = buildEle("span", { id: "HHText", innerHTML: "&nbsp;:&nbsp;" });
    self.searchDiv.appendChild(ele.HHText);

    /*minute-----------------*/
    ele.min = buildEle("select", { id: "min" });
    self.searchDiv.appendChild(ele.min);

    ele.middleText = buildEle("span", { innerHTML: "&nbsp;~&nbsp;&nbsp;" });
    self.searchDiv.appendChild(ele.middleText);

    /*end*/
    /*yy-----------------*/
    ele.yyEnd = buildEle("select", { id: "yyEnd" });
    ele.yyEnd.onchange = function () { self.Select.SelDate('ddEnd', 'mmEnd', 'yyEnd') };
    self.searchDiv.appendChild(ele.yyEnd);

    ele.yyEndText = buildEle("span", { id: "yyEndText", innerHTML: "&nbsp;-&nbsp;" });
    self.searchDiv.appendChild(ele.yyEndText);

    /*mm-----------------*/
    ele.mmEnd = buildEle("select", { id: "mmEnd" });
    ele.mmEnd.onchange = function () { self.Select.SelDate('ddEnd', 'mmEnd', 'yyEnd') };
    self.searchDiv.appendChild(ele.mmEnd);

    ele.mmEndText = buildEle("span", { id: "mmEndText", innerHTML: "&nbsp;-&nbsp;" });
    self.searchDiv.appendChild(ele.mmEndText);

    /*dd-----------------*/
    ele.ddEnd = buildEle("select", { id: "ddEnd" });
    self.searchDiv.appendChild(ele.ddEnd);

    ele.ddEndText = buildEle("span", { id: "ddEndText", innerHTML: "&nbsp;&nbsp;" });
    self.searchDiv.appendChild(ele.ddEndText);

    /*HH-----------------*/
    ele.HHEnd = buildEle("select", { id: "HHEnd" });
    self.searchDiv.appendChild(ele.HHEnd);

    ele.HHEndText = buildEle("span", { id: "HHEndText", innerHTML: "&nbsp;:&nbsp;" });
    self.searchDiv.appendChild(ele.HHEndText);

    /*minute-----------------*/
    ele.minEnd = buildEle("select", { id: "minEnd" });
    self.searchDiv.appendChild(ele.minEnd);

    ele.searchBtn = buildEle("input", {
        type: "button", id: "SearchBtn",
        value: Language.btn_Search
    });

    ele.searchBtn.onclick = function () { Control.RightView.Search();}

    self.searchDiv.appendChild(ele.searchBtn);

    /*Duration*/
    self.DurationDiv = buildEle("div", { style: { "margin-top": "10px" } });
    self.DurationDiv.appendChild(buildEle("span", { id: "durationTitle", innerText: "" }));
    self.DurationDiv.appendChild(buildEle("span", { id: "duration", innerText: "" }));

    /*page*/
    self.PageDiv = buildEle("div", { style: { "margin-top": "15px", "text-align": "center" } });
    self.pageIndexSelect = buildEle("select", { id: "pageIndex", style: {"display":"none"} });
    
    self.PageDiv.appendChild(self.pageIndexSelect);

    /*----------------------------*/

    self.Search = {
        StartTime: "",
        EndTime: "",
        PageIndex: [1, 1, 1, 1],
        PageCount: [],
        Id: [1, 1, 1, 1]
    };

    ele.Div_Tree = buildEle("div", {
        style: {
            "width": "100%"
        }
    });

    ele.Table_top = buildEle("table", { style: { "width": "100%", "margin-top": "10px" } });
    self.Table_Bottom = buildEle("table", { style: { "width": "100%", "margin-top":"10px" } });

    ele.btn_return = buildEle("input", {
        "type": "button",
        "value": Language.btn_Return,
        "style": {
            "display":"none"
        }
    })
    ele.btn_return.onclick = function () {
        setLayout(Layout - 1);
    }

    ele.Div.appendChild(ele.btn_return);

    ele.Div.appendChild(ele.Div_Tree);
    ele.Div.appendChild(self.searchDiv);
    ele.Div.appendChild(self.DurationDiv);
    
    ele.Div.appendChild(ele.Table_top);
    ele.Div.appendChild(self.Table_Bottom);


    DataSheet.div = buildEle("div");
    ele.Div.appendChild(DataSheet.div);

    ele.Div.appendChild(self.PageDiv);

    /* AddEditPanel ------------------------------- */
    self.AddEditPanel = buildEle("div", {
        style: {
            "display": "none",
            "width": "78%",
            "padding-top": "10px",
            "padding-left": "1%"
        }
    });

    AddEditPack.Create(self.AddEditPanel);
    /* ------------------------------------- */
    
    self.Build = function () {
        $(ele.Div_Tree).empty();
        $(ele.Table_top).empty();
        $(self.Table_Bottom).empty();
        $(DataSheet.div).empty();
        $(self.searchDiv).hide();
        $(self.DurationDiv).show();
        ele.btn_return.style.display = "none";
        $(self.pageIndexSelect).hide();

        document.getElementById("duration").innerText = self.Search.StartTime + " ~ " + (self.Search.EndTime == "" ? "Now" : self.Search.EndTime);

        switch (Layout) {
            case Layouts.LAYER1:
                $(self.searchDiv).show();
                $(self.DurationDiv).hide();
                Column = Language.Column_Records_Layer1;
                CreateTitle(ele.Table_top, {
                    last: Reporter.Role ? [Language.Column_Manage] : []
                });

                CreateTitle(self.Table_Bottom, {
                    pre: Reporter.Permission ? [""]:[],
                    last: [Language.Column_Manage]
                });
                self._create.CompanyList(self.Table_Bottom, false, 0);

                if (RightView.Data.TopTree.length == 0) {
                    RightView.Data.TopTree.push({
                        Name: self.Data.CompanyList.ReceivePlayerName + RightView.Col_AddText[0],
                        ID: self.Data.CompanyList.ReceivePlayerID,
                        OnClick: function () {
                            self.Data.MemberList.InsertFirst = true;
                            Control.LeftView.Confirm(false);
                        }
                    });
                }

                break;
            case Layouts.LAYER2:
                if (RightView.Data.TopTree.length == 1 && Reporter.Role) {
                    RightView.Data.TopTree.push({
                        Name: self.Data.CompanyList.ReceivePlayerName,
                        ID: self.Data.CompanyList.ReceivePlayerID,
                        OnClick: function () { Control.RightView.Layer1.Row({ currentTarget: { UserID: RightView.Data.CompanyList.ReceivePlayerID } }); }
                    });
                }
                Column = Language.Column_Records_Layer2;
                CreateTitle(ele.Table_top);

                CreateTitle(self.Table_Bottom, {                    
                    last: [Language.Column_Manage]
                });
                self._create.CompanyList(self.Table_Bottom, false, 0);

                if (!Reporter.Role) {
                    Control.RightView.GameList({ currentTarget: {UserID:Reporter.ID}});
                    return;
                }
                break;
            case Layouts.LAYER3:
                if (!Reporter.Role) {
                    $(self.searchDiv).show();
                    $(self.DurationDiv).hide();
                }
                if (RightView.Data.TopTree.length == 2 || RightView.Data.TopTree.length == 0) {
                    RightView.Data.TopTree.push({
                        Name: self.Data.GameList.ReceivePlayerName,
                        ID: self.Data.GameList.ReceivePlayerID,
                        OnClick: function () { setLayout(Layouts.LAYER3); }
                    });
                }
                Column = [];
                for (var i = 1; i < Language.Column_Records_Layer3.length; i++) {
                    Column.push(Language.Column_Records_Layer3[i]);
                }
                CreateTitle(ele.Table_top);

                Column = Language.Column_Records_Layer3;

                CreateTitle(self.Table_Bottom);
                
                self._create.GameList(self.Table_Bottom);

                break;
            case Layouts.LAYER4:
                if (RightView.Data.TopTree.length == 3 || RightView.Data.TopTree.length == 1) {
                    RightView.Data.TopTree.push({
                        Name: self.Data.GameDetail.ReceiveGtitle,
                        ID: self.Data.GameDetail.ReceiveGID,
                        OnClick: function () { }
                    });
                }
                DataSheet.Create(DataSheet._obj, self.Data.GameDetail.ReceiveGID);

                Column = Language.Column_Records_Layer3;
                CreateTitle(ele.Table_top);
                self._create.GameDetail(ele.Table_top);

                break;
            default:

        }
        
        self._create.PageIndex();
        self._create.Tree();

    }
    self._create = {
        Tree: function () {
            var tree_arr = self.Data.TopTree, tree_ele, container;
            
            for (var i = 0; i < tree_arr.length; i++) {
                container = buildEle("div", {
                    style: { "margin-right": "5px","display":"inline-block" }
                });

                container.appendChild(document.createTextNode(">>"));

                tree_ele = buildEle("a", {
                    innerText: tree_arr[i].Name
                });

                if (i != tree_arr.length - 1) {
                    tree_ele.className = "pointer";
                    tree_ele.arrIndex = i;
                    tree_ele.arrName = tree_arr[i].Name;
                    tree_ele.arrID = tree_arr[i].ID;
                    tree_ele.onclick = function (evt) {
                        var arr_Index = evt.currentTarget.arrIndex;
                        var tmp = [];
                        for (var i = 0; i < arr_Index; i++) {
                            tmp.push(self.Data.TopTree[i]);
                        }

                        self.Data.TopTree = tmp;

                        tree_arr[i].OnClick();
                    };
                }

                container.appendChild(tree_ele);
                ele.Div_Tree.appendChild(container);
            }

        },
        CompanyList: function (tbl, clickabled, lv, groupName) {            
            var row, colData,
                col = ["Name", "totalBet", "totalWin", "Sum", "Refund", "AgentBonus", "CompanyBonus"];

            //Initial Top Record
            if(!lv)
                self.Data.DataTop.Record = zeroArr(col.length);

            var _record = [], _column = [];

            for (var i = 0; i < self.Data.CompanyList.List.length; i++) {
                _record.push(RightView.Data.CompanyList.List[i].GetTotal());
                _column.push(RightView.Data.CompanyList.List[i].GetColumn());
            }

            var _tree = "", KeyIndex, treeNode, rowIndex = -1, colIndex = -1;

            //get parent node index
            var parentNode = document.getElementById(getTreeNodeName(self.Data.CompanyList.ReceivePlayerID));
            if (parentNode)
                rowIndex = parentNode.rowIndex;

            var cell, txt;

            for (var j = 0; j < _record.length; j++) {
                //get insert row index
                row = tbl.insertRow(rowIndex == -1 ? j + 1: rowIndex + 1);
                KeyIndex = _column[j].indexOf(self.Data.KeyColumn);

                if (Layout == Layouts.LAYER1 && Reporter.Permission) {
                    /* tree node Setting---------------- */
                    treeNode = new _TreeNode();

                    row.id = getTreeNodeName(_record[j][KeyIndex]);
                    row.className = groupName ? groupName : "";
                    
                    treeNode.Set(_record[j][KeyIndex], self.Data.CompanyList.ReceivePlayerID, lv);

                    row.appendChild(treeNode.CreateTD());
                }
                /* ---------------------------------- */

                /* column Td ------------------- */
                for (var i = 0; i < col.length; i++) {
                    colIndex = _column[j].indexOf(col[i]);

                    colData = colIndex == -1 ? "" : _record[j][colIndex];
                    colData = isNaN(colData) ? colData : Number(colData);

                    if (i == 0) {
                        if (Layout == Layouts.LAYER1)
                            if(Reporter.Permission)
                                createTd(colData + Data_Addtext[i], row, true, function (evt) {
                                    self.Data.MemberList.InsertFirst = true;
                                    Control.LeftView.Confirm(false, evt.currentTarget.UserID);
                                }, { UserID: _record[j][KeyIndex] }, { "color": "red" });
                            else
                                createTd(colData + Data_Addtext[i], row, true);
                            
                        else if(Layout == Layouts.LAYER2)
                            createTd(colData + Data_Addtext[i], row, true, Control.RightView.GameList, { UserID: _record[j][KeyIndex] }, { "color": "red" });
                    } else
                        createTd(colData + Data_Addtext[i], row, true);

                    //Top Data
                    self.Data.DataTop.Record[i] += colData;
                }

                cell = document.createElement("td");
                cell.style.textAlign = "center";

                //Edit Btn
                txt = buildEle("span", { "innerText": Language.btn_Edit, style: { "color": "red", "margin-right": "5px" }, className: "pointer" });
                txt.UserID = _record[j][KeyIndex];
                txt.onclick = function (evt) {
                    RightView.Show.EditPanel(evt, Modes.EDIT);
                }

                cell.appendChild(txt);

                //Delete Btn
                txt = buildEle("span", { "innerText": Language.btn_Delete, style: { "color": "red" }, className:"pointer" });
                txt.UserID = _record[j][KeyIndex];
                txt.onclick = function (evt) {
                    LoadingPanel.Show();
                    ws.Send("deletedata", {
                        GID: Reporter.GID,                        
                        ShopID: evt.currentTarget.UserID
                    });
                }

                cell.appendChild(txt);

                row.appendChild(cell);
                                

                if (rowIndex != -1) {
                    rowIndex++;
                }
            }

            /* Row One*/
            if (Layout == Layouts.LAYER1 && self.Data.MemberList.InsertFirst) {
                self.Data.MemberList.InsertFirst = false;

                row = tbl.insertRow(1);
                          
                if(Reporter.Permission)
                    createTd("", row, false);

                KeyIndex = RightView.Data.MemberList.Column.indexOf(self.Data.KeyColumn);

                if (RightView.Data.MemberList.Record.length > 0) {
                    
                    createTd(Language.RowFirst_Member, row, true, Control.RightView.Layer1.Row, { UserID: RightView.Data.MemberList.Record[KeyIndex] }, { "color": "red" });
                    
                    /* column Td ------------------- */
                    for (var j = 1; j < col.length; j++) {
                        colIndex = RightView.Data.MemberList.Column.indexOf(col[j]);

                        colData = colIndex == -1 ? "" : RightView.Data.MemberList.Record[colIndex];
                        colData = isNaN(colData) ? colData : Number(colData);


                        createTd(colData + Data_Addtext[j], row, true);


                        //Top Data
                        self.Data.DataTop.Record[j] += colData;
                    }
                } else {
                    createTd(Language.RowFirst_Member, row, true);
                    for (var j = 1; j < col.length; j++) {
                        createTd("0", row, true);
                    }
                }


                createTd("", row, false);
            }

            /* Top Column Td ---------------------------- */
            if (!lv) {
                self.Data.DataTop.Record[0] = self.Data.CompanyList.ReceivePlayerName;
                row = document.createElement("tr");
                for (var i = 0; i < col.length; i++) {
                    createTd(self.Data.DataTop.Record[i] + Data_Addtext[i], row, true);
                }

                if (Layout == Layouts.LAYER1 && Reporter.Role) {
                    cell = buildEle("td");
                    cell.style.textAlign = "center";

                    txt = buildEle("span", {
                        "innerText": Language.btn_AddAgent,
                        style: {
                            "color": "red",
                            "margin-right":"5px"
                        }
                    });

                    txt.className = "pointer";
                    txt.UserID = self.Data.CompanyList.ReceivePlayerID;
                    txt.onclick = function (evt) {
                        AddEditPack.Role = 1;
                        RightView.Show.EditPanel(evt, Modes.ADD);
                    }

                    cell.appendChild(txt);

                    txt = buildEle("span", {
                        "innerText": Language.btn_AddMember,
                        style: {
                            "color": "red"
                        }
                    });

                    txt.className = "pointer";
                    txt.UserID = self.Data.CompanyList.ReceivePlayerID;
                    txt.onclick = function (evt) {
                        AddEditPack.Role = 0;
                        RightView.Show.EditPanel(evt, Modes.ADD);
                    }

                    cell.appendChild(txt);
                    row.appendChild(cell);
                }
                
                ele.Table_top.appendChild(row);
            }

            return tbl;
        },
        GameList: function (tbl) {
            var row, colData,
                col = ["GName", "Name", "totalBet", "totalWin", "Sum", "Refund", "AgentBonus", "CompanyBonus"];

            //Initial Top Record            
            self.Data.DataTop.Record = zeroArr(col.length);

            var _record = self.Data.CompanyList.Record, _column = self.Data.CompanyList.Column;
            
            var _tree = "", KeyIndex, GIDIndex, rowIndex = -1, colIndex = -1;
                        
            var cell, txt;

            for (var j = 0; j < _record.length; j++) {
                //get insert row index
                row = tbl.insertRow(j + 1);
                KeyIndex = _column[j].indexOf(self.Data.KeyColumn);
                GIDIndex = _column[j].indexOf("GID");

                /* column Td ------------------- */
                for (var i = 0; i < col.length; i++) {
                    colIndex = _column[j].indexOf(col[i]);

                    colData = colIndex == -1 ? "" : _record[j][colIndex];
                    colData = isNaN(colData) ? colData : Number(colData);

                    if (i == 0) {                        
                        createTd(colData + Data_Addtext[i], row, true, Control.RightView.GameDetail, {
                            UserID: _record[j][KeyIndex], gid: _record[j][GIDIndex],
                            record: _record[j], column: _column[j],
                            pageindex:1
                        }, { "color": "red" });
                    } else
                        createTd(colData + Data_Addtext[i], row, true);

                    //Top Data
                    self.Data.DataTop.Record[i] += colData;
                }                
            }

            /* Top Column Td ---------------------------- */            
            self.Data.DataTop.Record[1] = self.Data.GameList.ReceivePlayerName;
            row = document.createElement("tr");
            for (var i = 1; i < col.length; i++) {
                createTd(self.Data.DataTop.Record[i] + Data_Addtext[i], row, true);
            }

            ele.Table_top.appendChild(row);            

            return tbl;
        },
        GameDetail: function (tbl) {
            var row, colData,
                col = ["GName", "Name", "totalBet", "totalWin", "Sum", "Refund", "AgentBonus", "CompanyBonus"];
                        
            var _record = self.Data.GameDetail.Record, _column = self.Data.GameDetail.Column;

            row = tbl.insertRow(1);
            
            /* column Td ------------------- */
            for (var i = 0; i < col.length; i++) {
                colIndex = _column.indexOf(col[i]);

                colData = colIndex == -1 ? "" : _record[colIndex];
                                        
                createTd(colData, row, true);
            }            

            return tbl;
        },
        PageIndex: function () {
            $(self.pageIndexSelect).empty();
            $(self.pageIndexSelect).show();

            var option;
            for (var i = 1; i <= self.Search.PageCount[Layout - Layouts.LAYER1]; i++) {
                option = document.createElement("option");
                option.text = i;
                option.value = i;
                self.pageIndexSelect.appendChild(option);
            }

            self.pageIndexSelect.value = self.Search.PageIndex[Layout - Layouts.LAYER1];

            
            switch (Layout) {
                default:
                    self.Search.PageIndex[3] = 1;
                    $(self.pageIndexSelect).hide();
                    break;
                case Layouts.LAYER4:
                    self.pageIndexSelect.record = self.Data.GameDetail.Record;
                    self.pageIndexSelect.column = self.Data.GameDetail.Column;
                    self.pageIndexSelect.gid = self.Data.GameDetail.ReceiveGID;

                    self.pageIndexSelect.onchange = function (evt) {
                        self.Search.PageIndex[Layout - Layouts.LAYER1] = Number(self.pageIndexSelect.value);
                        evt.currentTarget.pageindex = Number(self.pageIndexSelect.value);
                        Control.RightView.GameDetail(evt);
                    }
                    break;
                
            }
            
        }
    }
    
    //opt:pre,last
    function CreateTitle(tbl, opt) {
        var row = document.createElement("tr"), text;
        row = tbl.insertRow(0);

        var pre = [], last = [];

        if (opt) {
            if (opt.pre) {
                pre = opt.pre;
            }

            if (opt.last) {
                last = opt.last;
            }
        }


        for (var i = 0; i < pre.length; i++) {            
            row = createTd(pre[i], row, true);
        }

        for (var i = 0; i < Column.length; i++) {
            text = Column[i].split(':');

            text.length == 1 ? Data_Addtext[i] = "" : Data_Addtext[i] = text[1];

            row = createTd(text[0], row, true);
        }

        for (var i = 0; i < last.length; i++) {            
            row = createTd(last[i], row, true);
        }

        row.style.backgroundColor = "Blue";
        row.style.color = "white";
        row.style.height = "22px";
        row.style.textAlign = "center";

        tbl.setAttribute('border', '0');

        return tbl;
    }
    
    self.Refresh = function () {
        ele.btn_return.value = Language.btn_Return;
        
        AddEditPack.Refresh(self.AddEditPanel);

        ele.searchBtn.value = Language.btn_Search;
    }

    self.Init = function () {
        $("body").append(ele.Div);
        $("body").append(self.AddEditPanel);
    }
    self.Show = {
        EditPanel: function (evt, Mode, isRefresh) {
            AddEditPack.SetMode(Mode);

            if (AddEditPack.Mode == Modes.EDIT && !isRefresh) {
                LoadingPanel.Show();
                AddEditPack.EditID = evt.currentTarget.UserID;
                ws.Send("request.userdata", { GID: Reporter.GID, UserID: evt.currentTarget.UserID });                
            }

            if (!Reporter.Permission) {
                document.getElementById("row_canSeeNext").style.display = "none";
            }

            self.AddEditPanel.style.display = "inline-block";
            $(ele.Div).hide();            
        },
        Data: function () {
            $(self.AddEditPanel).hide();
            AddEditPack.InitInput();
            ele.Div.style.display = "inline-block";

            AddEditPack.SetMode(Modes.VIEW);
        }
    }
    self.Hide = function () {
        $(ele.Div).hide();
                
        $(self.AddEditPanel).hide();
        AddEditPack.InitInput();

        AddEditPack.SetMode(Modes.VIEW);
    }
    
    Object.defineProperties(self, {
        Col_AddText: {
            get: function () { return Data_Addtext; }
        }
    })

    return self;
}();

function zeroArr(len) {
    var target = [];

    for (var i = 0; i < len; i++) {
        target[i] = 0;
    }

    return target;
}

var Control = {
    LeftView: {
        Confirm: function (initial) {
            if(initial)
                Control.gameWebsockect.Initial();

            RightView.Data.CompanyList.Init();
            RightView.Data.MemberList.Record = [];

            Control.gameWebsockect.Receive.DataCount = 0;
            Control.gameWebsockect.CompleteFunc = function () {
                if (Reporter.Role)
                    setLayout(Layouts.LAYER1);
                else
                    setLayout(Layouts.LAYER2);
            };

            Control.RightView.SendTotal(arguments.length == 2?arguments[1]: Reporter.ID, Reporter.Role);
        }
    },
    RightView: {
        Layer1: {
            TreeClick: function (treePack) {
                Control.gameWebsockect.Receive.DataCount = 0;
                RightView.Data.CompanyList.Init();

                var self = treePack;

                Control.gameWebsockect.CompleteFunc = function () {
                    RightView._create.CompanyList(RightView.Table_Bottom, false, self.Level + 1, "tr_" + Layout + "_" + self.ID);
                    
                    LoadingPanel.Hide();
                };

                Control.RightView.SendTotal(self.ID, 1);

            },
            Row: function (evt) {
                Control.gameWebsockect.Receive.DataCount = 0;
                RightView.Data.CompanyList.Init();

                Control.gameWebsockect.CompleteFunc = function () { setLayout(Layouts.LAYER2) };

                Control.RightView.SendTotal(evt.currentTarget.UserID, 0);
            }
        },
        GameList:function(evt){
            LoadingPanel.Show();
            RightView.Data.CompanyList.Record = [];

            var _uid = evt.currentTarget.UserID, isFind = false;
            for (var i = 0; i < RightView.Data.CompanyList.List.length; i++) {
                if (Number(RightView.Data.CompanyList.List[i].UserID) == Number(_uid)) {
                    isFind = true;
                    var obj = RightView.Data.CompanyList.List[i].GetGameList(), colIndex;

                    RightView.Data.CompanyList.Record = obj.Record;
                    RightView.Data.CompanyList.Column = obj.Column;

                    RightView.Data.GameList.ReceivePlayerID = RightView.Data.CompanyList.List[i].UserID;
                    RightView.Data.GameList.ReceivePlayerName = RightView.Data.CompanyList.List[i].Name;
                    
                    break;
                }
            }

            //if (isFind) {
                setLayout(Layouts.LAYER3);
            //}

        },
        GameDetail:function(evt){
            var _gid = evt.currentTarget.gid;

            Control.gameWebsockect.Receive.DataCount = 0;
            Control.gameWebsockect.Receive.ReceiveCount = 1;

            Control.gameWebsockect.CompleteFunc = function () { setLayout(Layouts.LAYER4) };

            for (var i = 0; i < LeftView.game_ws.length; i++) {
                if (_gid == LeftView.game_ws[i].gid) {
                    Control.gameWebsockect.Send(i, "reporter.GameRecord", {
                        Token: Reporter.Token,
                        UserID: RightView.Data.GameList.ReceivePlayerID,
                        gid: LeftView.check_list[i].gid,
                        gtitle: LeftView.check_list[i].gtitle,
                        pageindex: evt.currentTarget.pageindex,
                        start: RightView.Search.StartTime,
                        end: RightView.Search.EndTime
                    });

                    RightView.Data.GameDetail.Record = evt.currentTarget.record;
                    RightView.Data.GameDetail.Column = evt.currentTarget.column;

                    break;
                }
            }
        },
        SendTotal: function (pID, Type) {
            Control.gameWebsockect.Receive.ReceiveCount = LeftView.check_list.length;

            for (var i = 0; i < LeftView.check_list.length; i++) {
                Control.gameWebsockect.Send(i, "reporter.Total", {
                    Token: Reporter.Token,
                    pID: pID,
                    Type: Type,
                    gid: LeftView.check_list[i].gid,
                    gtitle: LeftView.check_list[i].gtitle,
                    start: RightView.Search.StartTime,
                    end: RightView.Search.EndTime
                });
            }
            if (!LeftView.check_list.length && Control.gameWebsockect.CompleteFunc) {                
                RightView.Data.TopTree = [];
                Control.gameWebsockect.CompleteFunc();
            }
        },
        AddEdit: function () {
            LoadingPanel.Show();

            var input = [], ele_input, pa = "";
            var isOK = true;

            for (var i = 0; i < AddEditPack.ID.length; i++) {
                if (AddEditPack.Type[i] == "radio") {
                    ele_input = document.getElementsByName(AddEditPack.ID[i]);
                }else
                    ele_input = document.getElementById(AddEditPack.ID[i]);

                if (ele_input) {
                    if (ele_input.value=="") {
                        AddEditPack.SetError(AddEditPack.ColumnTitle[i] + Language.AlertMsg[0]);
                        isOK = false;

                        break;
                    }

                    if (AddEditPack.Type[i] == "password") {
                        if (pa == "") {
                            pa = ele_input.value;
                        } else if(pa != ele_input.value) {
                            isOK = false;
                            AddEditPack.SetError(Language.AlertMsg[1]);
                        }                        
                    }

                    if (AddEditPack.Type[i] == "radio") {
                        for (var j = 0; j < ele_input.length; j++) {
                            if (ele_input[j].checked) {
                                input.push(ele_input[j].value);
                                break;
                            }
                        }
                    }else
                        input.push(ele_input.value);
                }
            }

            if (!isOK) {
                LoadingPanel.Hide();
                return;
            }

            AddEditPack.SetError("");

            ws.Send("editdata", {
                GID: Reporter.GID,
                Input: input, Column: AddEditPack.ColumnTitle,
                Type: AddEditPack.Type,
                Mode: AddEditPack.Mode,
                Role:AddEditPack.Role,
                //Layer: Layout,
                EditShopID: AddEditPack.Mode == Modes.EDIT? AddEditPack.EditID:RightView.Data.CompanyList.ReceivePlayerID
            });
        },
        Search: function () {
            LoadingPanel.Show();
            RightView.Search.PageIndex[Layout - Layouts.LAYER1] = 1;
            RightView.Search.StartTime = this.getSearch("");
            RightView.Search.EndTime = this.getSearch("End");

            Control.LeftView.Confirm();
        },
        getSearch: function (val) {
            var result = "";
            var select = document.getElementById("yy" + val).value;
            result += select + "-";
            select = document.getElementById("mm" + val).value;
            result += select + "-";
            select = padLeft(2, document.getElementById("dd" + val).value, "0");
            result += select + " ";

            select = padLeft(2, document.getElementById("HH" + val).value, "0");
            result += select + ":";
            select = padLeft(2, document.getElementById("min" + val).value, "0");
            result += select;

            return result;
        },
    },
    //WebSockect 傳遞+接收
    gameWebsockect: {
        CompleteFunc: null,
        Initial:function(){
            //所有ws連線先斷線
            for (var i = 0; i < LeftView.game_ws.length; i++) {
                LeftView.game_ws[i].close();
            }

            LeftView.check_list = [];
            LeftView.game_ws = [];            
            
            $("input[type=checkbox][name=" + LeftView.check_name + "]").each(function () {
                if (this.checked) {
                    LeftView.check_list.push({ val: $(this).val(), gid: this.gid, gtitle: this.gtitle });
                    //LeftView.check_list.push({ val: $(this).val(), gid: this.gid, gtitle: this.gtitle });
                }
            });

            for (var i = 0; i < LeftView.check_list.length; i++) {
                LeftView.game_ws[i] = new Tools.Connection(LeftView.check_list[i].val);
                LeftView.game_ws[i].gid = LeftView.check_list[i].gid;
                LeftView.game_ws[i].gtitle = LeftView.check_list[i].gtitle;

                LeftView.game_ws[i].AddOnMessageHandler("reporter.Total", Control.gameWebsockect.Receive.Total);
                LeftView.game_ws[i].AddOnMessageHandler("error", Control.gameWebsockect.Receive.Error);
                LeftView.game_ws[i].AddOnMessageHandler("request.GameRecord", Control.gameWebsockect.Receive.GameRecord);
                LeftView.game_ws[i].onclose = function () { Control.gameWebsockect.Receive.CheckComplete(); };
                LeftView.game_ws[i].onerror = function () { Control.gameWebsockect.Receive.CheckComplete(); };

                LeftView.game_ws[i].StartConnect();
            }
        },
        Send: function (index, type, opt) {
            if (!LeftView.game_ws[index] ||
                LeftView.game_ws[index].readyState == WebSocket.CLOSED ||
                LeftView.game_ws[index].readyState == WebSocket.CLOSING) {

                this.Receive.CheckComplete();

                return;
            }

            if (LeftView.game_ws[index].readyState == WebSocket.CONNECTING) {
                setTimeout(function () {
                    Control.gameWebsockect.Send(index, type, opt)
                }, 10);

                return;
            }

            LoadingPanel.Show();
            LeftView.game_ws[index].Send(type, opt);
        },
        Receive: {
            DataCount: 0,
            ReceiveCount:0,
            CheckComplete: function () {
                Control.gameWebsockect.Receive.DataCount += 1;

                if (Control.gameWebsockect.Receive.DataCount == Control.gameWebsockect.Receive.ReceiveCount) {
                    //接收完成
                    if (Control.gameWebsockect.CompleteFunc)
                        Control.gameWebsockect.CompleteFunc();
                }
            },
            Total: function (msg) {
                if (msg.Record.length) {
                    RightView.Data.CompanyList.ReceivePlayerID = msg.pID;
                    RightView.Data.CompanyList.ReceivePlayerName = msg.pName;
                    RightView.Data.CompanyList.Column = msg.Column;
                    RightView.Data.MemberList.Column = msg.Column_User;

                    for (var i = 0; i < msg.Record.length; i++) {
                        RightView.Data.CompanyList.Add(msg.Record[i], msg.gameID, msg.gameTitle);                        
                    }

                    if (RightView.Data.MemberList.Record.length == 0) {
                        RightView.Data.MemberList.Record = zeroArr(RightView.Data.MemberList.Column.length);
                    }

                    RightView.Data.MemberList.Add(msg.Record_User);

                }

                Control.gameWebsockect.Receive.CheckComplete();
            },
            GameRecord: function (msg) {
                //if (msg.Record.length) {
                RightView.Data.GameDetail.ReceiveGID = msg.gameID;
                RightView.Data.GameDetail.ReceiveGtitle = msg.gameTitle;
                DataSheet._obj = msg;
                RightView.Search.PageCount[3] = msg.pageCount;
                //}

                Control.gameWebsockect.Receive.CheckComplete();
            },
            Error: function (msg) {
                Control.gameWebsockect.Receive.CheckComplete();
            }
        }
    }
}