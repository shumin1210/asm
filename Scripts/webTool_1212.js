﻿/// <reference path="jquery-1.10.2.min.js" />

var p_width = 1280;
var p_height = 785;
var resize_time = 300;
var container_padding_width = 30;

//div或圖片等比例縮放,以螢幕寬或高度為基準(para:id,圖片原始寬度,圖片原始高度,改變寬或高度(pa數),0:以寬為基準1:以高為基準,是否垂直置中,是否水平置中)
function ele_Resize(id, i_width, i_height, _pa, opt) {
    //parent尺寸
    var w_width, w_height;
    var w_h;
    var setting = { h_middle: false, v_middle: false, Fixed: 0 };
    if (opt) {
        for (var i in opt) {
            setting[i] = opt[i];
        }
    }

    w_h = i_width / i_height;

    w_width = $(id).parent().width();
    w_height = $(id).parent().height();

    if (setting.Fixed == 0) {
        $(id).css({
            'width': w_width * _pa + 'px',
            'height': w_width * _pa / w_h + 'px'
        });
    } else {
        $(id).css({
            'height': w_height * _pa + 'px',
            'width': w_height * _pa * w_h + 'px'
        });
    }

    if (setting.h_middle) {
        $(id).css({
            'margin': '0 auto'
        });
    }

    if (setting.v_middle) {
        var pa = $(id).parent();
        var pa_height = pa.height();
        var m_top = (pa_height - $(id).height()) / 2;


        $(id).css({
            'margin-top': m_top + 'px'
        });
    }

}

//依據parent寬度為基準,改變element寬度(%)
function ele_width(id, _pa) {
    //parent尺寸
    var w_width;

    w_width = $(id).parent().width();

    $(id).css({
        'width': w_width * _pa + 'px'
    });
}

//改變element高度(px)
function ele_height_px(id, value) {    
    $(id).css({
        'height': value + 'px'
    });
}

//垂直置中
function ele_v_middle(id) {
    var pa = $(id).parent();
    var pa_height = pa.height();
    var m_top = (pa_height - $(id).height()) / 2;
    $(id).css({
        'margin-top': m_top + 'px'
    });

}

//垂直置中(特定element)
function ele_v_middle_2(id,target_id) {
    var pa_height = $(id).height();
    var m_top = (pa_height - $(target_id).height()) / 2;
    $(target_id).css({
        'margin-top': - m_top + 'px'
    });

}


//兩element相同高度(para:此id高度,需改變高度之id)
function ele_sameheight(id, target_id) {
    $(target_id).css({
        'height': $(id).height() + 'px'
    });
}

//兩element相同寬度(para:此id寬度,需改變寬度之id)
function ele_samewidth(id, target_id) {
    $(target_id).css({
        'width': $(id).width() + 'px'
    });
}

//根據螢幕寬度改變此element的字體大小
function ele_adjust_font_size(id, base_font_size) {
    if (!IsMobile) {
        var w_width, w_height;

        w_width = $(window).width();

        var w_radio = w_width / p_width;

        var font_size = w_radio * base_font_size;

        if (font_size < 12) {
            font_size = 12;
        }

        $(id).css({
            'font-size': font_size + 'px'
        });

    }
}

//此element會依據此裝置為手機則垂直置中時會扣掉address bar(40px)的高度
function ele_mobile_middle(id) {
    if (IsMobile) {
        var w_width, w_height;

        w_width = $(window).width();
        w_height = $(window).height();

        if (w_height > w_width && !IsIpad) {
            //手機直向

            var pa2 = $(id).parent();
            var pa_height2 = pa2.height();
            var m_top2 = (pa_height2 - $(id).height()) / 2;

            $(id).css({
                'margin-top': m_top2 - 40 + 'px'
            });
        }

    }
}

//element加入class
function ele_addClass(id, className) {
    $(id).addClass(className);
}

/*ActionCommand WS訊息物件(maintype,subtype,參數[])*/
var Action = function () {
    var actioncontent = "";
    for (var i = 2; i < arguments.length; i++) {
        actioncontent += (arguments[i] + ",");
    }
    return {
        "actionmaintype": arguments[0],
        "actionsubtype": arguments[1],
        "actiontime": "",
        "actioncontent": actioncontent
    }
}
