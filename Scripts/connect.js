﻿/// <reference path="data.js" />
/// <reference path="../../jquery/jquery-1.12.3.min.js" />
/// <reference path="../../jquery/Tools.js" />

var WsPingPong = {
    Timer: 0,
    Duration: 60000,
    Start: function () {
        clearInterval(WsPingPong.Timer);
        WsPingPong.Timer = setInterval(function () {
            ws.Send("ping");
        }, WsPingPong.Duration);
    },
    Stop: function () {
        clearInterval(WsPingPong.Timer);
    }
}

ws.AddOnMessageHandler("login", function (msg) {    
    if (msg.Games.length > 0) {
        Login.Hide();
        LoadingPanel.Show();

        Reporter.GID = msg.Games[0];
        Reporter.ID = msg.ID;
        Reporter.Name = msg.Name;
        Reporter.Token = msg.Token;
        Reporter.Role = msg.Role;
        Reporter.Permission = msg.Permission;
        Reporter.MaxLevel = msg.MaxLevel;
        Reporter.Title = msg.Title;
        Reporter.Level = msg.Level;
        Reporter.isAdmin = msg.isAdmin;
        MainPage.UpdateRole();
        //ws.Send("enter_report", { GID: Reporter.GID });
                
        Control.HomePage.Send();
        setLayout(Layouts.MAINPAGE);
                
        Control.Online.Send();
        Control.Cashout.GetTime();
        Control.Cashout.GetStatus();

        RightView.Search.StartTime = Control.RightView.Calendar.getSearch(CreateView.Date.Type.DateTime, RightView.SelectObj, CreateView.Date.Type.Start);
        RightView.Search.EndTime = Control.RightView.Calendar.getSearch(CreateView.Date.Type.DateTime, RightView.SelectObj, CreateView.Date.Type.End);

        Control.CheckOutSelect.GetList(msg.lastCheckoutTime, RightView.Search.EndTime, 1);

        Control.GetAccoutSum();

        resize();
    }
    
});

ws.AddOnMessageHandler("double login", function (msg) {
    //$("body").empty();

    //所有ws連線先斷線
    for (var i = 0; i < LeftView.game_ws.length; i++) {
        LeftView.game_ws[i].close();
    }    
});

ws.AddOnMessageHandler("login.error", function (msg) {
    $("#" + Login.ErrorMsgId).empty();
    $("#" + Login.ErrorMsgId).append(buildEle("span", { innerHTML: msg.Msg, style: { color: "red" } }));
});

ws.AddOnMessageHandler("error", function (msg) {
    ws.close();
    //setLayout(Layouts.LOGIN);
});

ws.AddOnMessageHandler("gamelist", function (msg) {    
    LeftView.Data = msg;
    LeftView.Set();

    Control.LeftView.FirstConfirm();    
});

ws.AddOnMessageHandler("transReportQue", function (msg) {
    
    //LeftView1.Data = msg;
    
    Control.gameWebsockect.Receive.Total(msg);
});

ws.AddOnMessageHandler("reporter.Total", function (msg) {
    Control.gameWebsockect.Receive.Total(msg);
});

ws.AddOnMessageHandler("reporter.OpenCash", function (msg) {
    Control.gameWebsockect.Receive.Total(msg);
});

ws.AddOnMessageHandler("reporter_Stockholder", function myfunction(msg) {
    Control.gameWebsockect.Receive.Total(msg);
});

ws.AddOnMessageHandler("reporter.GameRefundList", function (msg) {
    Control.gameWebsockect.Receive.Total(msg);
});

ws.AddOnMessageHandler("reporter.GameRecord", function (msg) {
    Control.gameWebsockect.Receive.GameRecord(msg);
});

ws.AddOnMessageHandler("reporter.GameRefundRecord", function (msg) {
    Control.gameWebsockect.Receive.GameRecord(msg);
});

ws.AddOnMessageHandler("game.error", function (msg) {
    if (msg.Msg == "GID Length Zero!") {
        LoadingPanel.Hide();
        RightView.Hide();
        LeftView.Show();
    }
    else
        Control.gameWebsockect.Receive.CheckComplete();
});

ws.AddOnMessageHandler("request.userdata", function (msg) {
    if (Reporter.DataType == DataType.STOCKHOLDERMANAGE) {
        AddShareHolderView.Set(msg);
    }else
        AddEditPack.Set(msg);
});

ws.AddOnMessageHandler("request.userdata_refund", function (msg) {
    AddEditPack.SetPerColumn("Refund", msg.refund);
    LoadingPanel.Hide();
});

ws.AddOnMessageHandler("request.qrlist", function (msg) {
    QRView.ListView(msg);
    QRView.Show();
    LoadingPanel.Hide();
});

ws.AddOnMessageHandler("request.printqrlist", function (msg) {
    QRView.printReceive(msg);
});

ws.AddOnMessageHandler("request.qruserlist", function (msg) {
    textList.SetUserList(msg);
    LoadingPanel.Hide();
});

ws.AddOnMessageHandler("request.checkouttimelist", function (msg) {
    Control.CheckOutSelect.Update(msg);
});

ws.AddOnMessageHandler("request.homepage", function (msg) {
    HomePageView.Refresh.Data(msg);
    LoadingPanel.Hide();
});

ws.AddOnMessageHandler("1_error", function () {
    LoadingPanel.Hide();
});

ws.AddOnMessageHandler("2_error", function () {
    LoadingPanel.Hide();
});

ws.AddOnMessageHandler("request.usercredit", function (msg) {
    switch (msg.type) {
        case PopType.SETCREDIT:
            SetCreditView.Set.CreditInfo(msg);
            break;
        case PopType.CASHOUT:
            CashOutView.Set.CreditInfo(msg);
            break;
        default:

    }
    LoadingPanel.Hide();
});

ws.AddOnMessageHandler("request.online", function (msg) {
    MainPage.UpdateOnlineList(msg.Data);
    OnlineView.ListView(msg.Data);
    LoadingPanel.Hide();
});

ws.AddOnMessageHandler("request.setting_week", function (msg) {
    CheckOutSettingView.RefreshData(msg);
    LoadingPanel.Hide();
});

ws.AddOnMessageHandler("request.setting_confirm", function (msg) {
    CheckOutSettingView.Error(msg.reason);
    Control.Setting.RequestData(0);
    //LoadingPanel.Hide();
});

ws.AddOnMessageHandler("set.qruser", function (data) {
    var msg = data.msg;
    if (msg == "") {
        textList.SetComplete();
    } else {
        textList.SetError(msg);
        LoadingPanel.Hide();
    }
});

ws.AddOnMessageHandler("request.language", function (msg) {
    Reporter.Title = msg.Title;
    Language.Set(msg.Record);

    ReportTitle.Refresh();
    MainPage.Refresh();

    AddEditPack.GetInputValue();
    RightView.Refresh();
    QRView.Refresh();
    LeftView.Refresh();
    LeftView_Trans.Refresh();
    Day_Trans_Report_View.Refresh();
    CashInOutView.Refresh();
    HomePageView.Refresh.Title();
    CashInView.Refresh();
    CashOutView.Refresh();
    SetCreditView.Refresh();
    OnlineView.Refresh();
    CheckOutView.Refresh();
    CheckOutSettingView.Refresh();
    CheckOutHistoryView.Refresh();
    EditPasswordView.Refresh();
    PopMsgView.Refresh();
    ABCView.Refresh();

    Language.Refresh();

    var isEdit = AddEditPack.Mode != Modes.VIEW;
    var tmpMode = AddEditPack.Mode;

    setLayout(Layout);

    if (isEdit) {
        AddEditPack.SetMode(tmpMode);
        RightView.Show.EditPanel(null, AddEditPack.Mode, true);
        AddEditPack.SetInputValue();
    }

    AddEditPack.tmp_input = [];

    resize();

    LoadingPanel.Hide();
});

ws.AddOnMessageHandler("user.ban", function () {
    Reporter.DataType = DataType.MEMBERMANAGE;
    setLayout(Layouts.MEMBERMANAGE);
    
    LoadingPanel.Hide();
});

ws.AddOnMessageHandler("edit.error", function (msg) {
    AddEditPack.SetError(msg.Msg);
    LoadingPanel.Hide();
});
ws.AddOnMessageHandler("edit.success", function (msg) {
    AddEditPack.SetError("");
    returnFunc();
    
    RightView.Show.Data();
});
ws.AddOnMessageHandler("delete.success", function (msg) {
    PopMsgView.Complete(msg);
    //if (msg.success) {
    //    returnFunc();
    //}
});
ws.AddOnMessageHandler("add.shareholder", function (msg) {
    AddShareHolderView.Complete(msg);
});

ws.AddOnMessageHandler("request.pdf", function (msg) {
    if (msg.fileName)
        Control.OpenPDF(msg.fileName, 1);
    LoadingPanel.Hide();
});

ws.AddOnMessageHandler("request.DayReport", function (msg) {
    Day_Trans_Report_View.Receive(msg);
    LoadingPanel.Hide();
});

ws.AddOnMessageHandler("request.cashout_list", function (msg) {    
    if (msg.Success && "Record" in msg && "Column" in msg) {
        CheckOutView.ListView(msg);
    }
    LoadingPanel.Hide();
});

ws.AddOnMessageHandler("request.checkout", function (msg) {
    CheckOutView.Error(msg.reason);

    Control.Cashout.GetList();
});

ws.AddOnMessageHandler("request.updatecheckout", function (msg) {
    CheckOutView.UpdateCheckOutTime(msg);
    LoadingPanel.Hide();
});

ws.AddOnMessageHandler("request.cashout_list_history", function (msg) {
    CheckOutHistoryView.RefreshData(msg);
    LoadingPanel.Hide();
});

ws.AddOnMessageHandler("request.checkout_status", function (msg) {
    CheckOutView.UpdateCheckOutBtn(msg);
    LoadingPanel.Hide();
});

ws.AddOnMessageHandler("user.changeRole", function (msg) {
    if (msg.Success) {
        Reporter.DataType = DataType.MEMBERMANAGE;
        setLayout(Layouts.MEMBERMANAGE);
    } else
        LoadingPanel.Hide();
});

ws.AddOnMessageHandler("edit.password", function (msg) {
    EditPasswordView.SetPassword.Receive(msg);

    LoadingPanel.Hide();
});

ws.AddOnMessageHandler("request.accountsum", function (msg) {
    ReportTitle.UpdateInfo(msg);
});

ws.AddOnMessageHandler("600.0", function (msg) {
    switch (msg.type) {
        case PopType.CASHIN:
            CashInView.Complete(msg);
            break;
        case PopType.CASHOUT:
            CashOutView.Complete(msg);
            break;
        case PopType.SETCREDIT:
            SetCreditView.Complete(msg);
            Control.GetAccoutSum();
            break;
        default:

    }
    LoadingPanel.Hide();
    //CashInOutView.Complete(msg);
});

ws.AddOnMessageHandler("600.1", function (msg) {
    CashInOutView.Complete(msg);
});

ws.AddOnMessageHandler("abc.setstatus", function (msg) {
    ABCView.SetStatus(msg);
});
ws.AddOnMessageHandler("abc.requestdata", function (msg) {
    ABCView.RefreshData(msg);
});

ws.onopen = function () {
    Init();
        
    setLayout(Layouts.LOGIN);
    WsPingPong.Start();
}

ws.onclose = function () {
    setTimeout(function () {
        ws.StartConnect();
    }, 500);
    WsPingPong.Stop();
}

function returnFunc() {
    switch (Layout) {
        case Layouts.LAYER1:
        default:
            RightView.Data.MemberList.InsertFirst = true;
            Control.LeftView.Confirm(false);
            break;
        case Layouts.LAYER2:
            Control.RightView.Layer1.Row({ currentTarget: { UserID: RightView.Data.CompanyList.ReceivePlayerID } });
            break;
    }
}