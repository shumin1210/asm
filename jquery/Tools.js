﻿/// <reference path="modernizr.js" />
/// <reference path="../jquery/jquery-1.11.3.js" />
var Tools = Tools || {};
Tools.Debug = {
    Gesture: false,
    Connection: false,
    ImageLoader: false,
};

Tools.Configs = {
    enableTransform: false,
    enableConnect: true,
    enableHoverEvent: true,
    enableGesture: true,
    useSimpleLogInfo: false
};

/*initail Detections*/
(function () {

    var ua = navigator.userAgent.toLowerCase();
    var isAndroid = /android/.test(ua);
    var isiOs = /(iphone|ipod|ipad)/.test(ua);
    var isStandAlone = window.navigator.standalone;
    var isiPad = ua.match(/ipad/);
    var isDevice = 'ontouchstart' in window;
    var isChrome = "chrome" in window;
    var isMoz = "mozAnimationStartTime" in window;

    IsMobile = 0;
    Tools.IsMobile = function () { return IsMobile; }
    IsIOs = 0;
    Tools.IsiOs = function () { return IsIOs; }
    iOSVersion = 0;
    Tools.iOSVersion = function () { return iOSVersion; }
    IsWideScreen = 1;
    Tools.IsWideScreen = function () { return IsWideScreen; }

    Object.defineProperties(Tools, {
        IsMobile: {
            get: function () { return IsMobile; }
        },
        IsIOs: {
            get: function () { return IsIOs; }
        }
    })
    //偵測手機瀏覽/////////////////////////////////////////////////////////////
    function CheckMobile() {
        IsMobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);
        screenLock = 1;
        IsIOs = /iPhone|iPad|iPod/i.test(navigator.userAgent);
        browser = get_browser_info();
        if (!IsMobile) {
            var w = document.documentElement.clientWidth;
            var h = document.documentElement.clientHeight;
            var r = w / h;
            IsWideScreen = (r > 1.5);
        }
        function iOSversion() {
            if (/iP(hone|od|ad)/.test(navigator.platform)) {
                var v = (navigator.appVersion).match(/OS (\d+)_(\d+)_?(\d+)?/);
                return [parseInt(v[1], 10), parseInt(v[2], 10), parseInt(v[3] || 0, 10)];
            }
        }
        if (IsIOs) {
            if (ver = iOSversion()) {
                iOSversion = ver[0];
            }
        }
    }
    Tools.CheckMobile = CheckMobile;

    //偵測瀏覽器支援度/////////////////////////////////////////////////////////////
    function CheckSupport() {
        var result = (
            ((typeof (Modernizr) != "undefined") &&
            Modernizr.canvas &&
            Modernizr.websockets) &&
            (document.createElement("iframe") ? 1 : 0) &&
            (Modernizr.audio.mp3 || Modernizr.audio.ogg)) ? 1 : 0;
        return result;
    }
    Tools.CheckSupport = CheckSupport;

    CheckMobile();
    CheckSupport();

    //{name,version}取得瀏覽器資訊
    function get_browser_info() {
        var ua = navigator.userAgent, tem,
            M = ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
        if (/trident/i.test(M[1])) {
            tem = /\brv[ :]+(\d+)/g.exec(ua) || [];
            return { name: 'IE', version: (tem[1] || '') };
        }
        if (M[1] === 'Chrome') {
            tem = ua.match(/\bOPR\/(\d+)/)
            if (tem != null) { return { name: 'Opera', version: tem[1] }; }
        }
        M = M[2] ? [M[1], M[2]] : [navigator.appName, navigator.appVersion, '-?'];
        if ((tem = ua.match(/version\/(\d+)/i)) != null) { M.splice(1, 1, tem[1]); }
        return {
            name: M[0],
            version: M[1]
        };
    }
    Tools.GetBrowserInfo = get_browser_info;
    Tools.isVertical = false;
    Tools.OrientationHandler = function () { }

    $(document).ready(function () {
        Tools.isVertical = (document.documentElement.clientWidth < document.documentElement.clientHeight);
        //旋轉事件
        $(window).resize(function () {
            Tools.OrientationHandler();
        });
        $(window).on("orientationchange", function () {
            Tools.isVertical = (document.documentElement.clientWidth < document.documentElement.clientHeight);
            Tools.OrientationHandler();
        });
        if (IsIOs) { window.scrollTo(0, 1); }
    });
}());

/*Extensions */
(function () {
    var linq = linq || {};
    window.linq = linq;
    linq.Where = function (db, check) {
        var results = [];
        if (check instanceof Function) {
            for (var i in db)
                if (db.hasOwnProperty(i))
                    if (check(db))
                        results.push(db[i]);
        }
        return result;
    }

    String.prototype.SplitToNumber = function (seperator) {
        return this.split(seperator).map(function (item) { return Number(item) });
    }

    /*check Bool(target)=>*/
    /*[].Where(function (element) {  return (element > 5) ;})*/
    Array.prototype.Where = function (check) {
        var result = [];
        for (var i = 0; i < this.length; i++)
            if (check(this[i]))
                result.push(this[i]);
        return result;
    }
    Array.prototype.Exist = function (check) {
        for (var i = 0; i < this.length; i++)
            if (check(this[i]))
                return true;
        return false;
    }

    Array.prototype.Count = function (check) {
        return this.Where(check).length;
    }

    Array.prototype.Group = function (attr) {
        var groups = {};
        groups.count = 0;
        for (var i = 0; i < this.length; i++) {
            if (this[i].hasOwnProperty(attr) && this[i][attr]) {
                var group = this[i][attr];
                if (!groups[group]) {
                    groups[group] = [];
                    groups.count++;
                }
                groups[group].push(this[i]);
            }
        }
        return groups;
    }
    Array.prototype.IndexOf = function (check) {
        for (var i = 0; i < this.length; i++)
            if (this[i] && check(this[i]))
                return i;
    }
    Array.prototype.Sum = function () {
        var arr;
        try {
            arr = this.reduce(function (pre, cur) { return pre + cur; });
        } catch (e) {
            arr = 0;
        }
        return arr;
    }

    Array.prototype.First = function (ele) {
        if (ele instanceof Function) {
            for (var i = 0; i < this.length; i++) {
                if (ele(this[i])) {
                    return this[i];
                }
            }
        } else {
            for (var i = 0; i < this.length; i++) {
                if (this[i] === ele) {
                    return this[i];
                }
            }
        }
        return null;
    }

    Array.prototype.Remove = function (obj) {
        return this.RemoveWhere(function (o) { return o == obj; });
    }
    Array.prototype.RemoveWhere = function (check, length) {
        var dif = 0;
        var result = [];
        for (var i = this.length - 1; i >= 0; i--) {
            if (check(this[i]))
                result.push(this.splice(i, 1)[0]);
            if (result.length == length)
                break;
        }
        return result;
    }

    Array.prototype.Select = function (func) {
        var l = [];
        for (var i = 0; i < this.length; i++)
            l.push(func(this[i]));
        return l;
    }

    Array.prototype.RandomElement = function () {
        if (this.length == 0) { return null; }
        if (this.length == 1) { return this[0]; }
        return this[Math.Random(this.length)];
    }

    Array.prototype.ForEach = function (func, skipUndefined) {
        for (var i = 0; i < this.length; i++) {
            if (skipUndefined && typeof (this[i]) == "undefined")
                continue;
            func(this[i]);
        }
    }

    Array.prototype.Take = function (length) {
        return this.slice(0, length);
    }
    Array.prototype.Last = function () {
        if (this.length)
            return this[this.length - 1];
        else
            return null;
    }
    Math.Random = function (min, max) {
        if (arguments.length == 1) {
            if (min < 0) { max = 0; }
            else if (min > 0) { max = min; min = 0; }
        }
        return Math.floor(Math.random() * (max - min)) + min;
    }

    Math.InRange = function (target, a, b) {
        return target == target.Range(a, b)
    }

    Number.prototype.InRange = function (a, b) {
        return Math.InRange(this, a, b);
    }

    Number.prototype.Range = function (a, b) {
        var val = this;
        var min = Math.min(a, b),
            max = Math.max(a, b);
        if (isNaN(val)) { val = 0; }
        if (val < min) { return min; }
        if (val >= max) { return max; }
        return val;
    }

    Array.prototype.RandomElement = function () {
        if (this.length == 0) { return null; }
        if (this.length == 1) { return this[0]; }
        return this[Math.Random(this.length)];
    }



    Object.defineProperty(Number.prototype, "length", {
        get: function () {
            return this.toString().length;
        }
    })
    window.IsLocalStorageSupported = 0;
    try {
        localStorage.setItem("test", "check");
        localStorage.removeItem("test");
        IsLocalStorageSupported = 1;
    } catch (e) {
    }
    window.localStorage.SetItem = function (key, value) {
        if (!IsLocalStorageSupported) { return 0; }
        try { localStorage.setItem(key, value); } catch (e) { return 0; }
    }
}());

window.requestAnimFrame = (function () {
    return window.requestAnimationFrame ||
        window.webkitRequestAnimationFrame ||
        window.mozRequestAnimationFrame ||
        function (callback) {
            window.setTimeout(callback, 1000 / 60);
        };
})();

(function () {
    return;
    var raf = window.requestAnimationFrame ||
           window.webkitRequestAnimationFrame ||
           window.mozRequestAnimationFrame ||
           function (callback) {
               window.setTimeout(callback, 1000 / 60);
           };
    Tools.updateQueue = {
        updates: [],
        interval: 1000 / 60,
        lastTime: 0,
        isWorking: false,
        Update: function (timestamp) {
            Tools.updateQueue.isWorking = true;
            if (!Tools.updateQueue.lastTime)
                Tools.updateQueue.lastTime = timestamp;
            if (timestamp - Tools.updateQueue.lastTime > Tools.updateQueue.interval) {
                Tools.updateQueue.lastTime = timestamp;
            } else {
                raf(Tools.updateQueue.Update);
                return;
            }
            if (Tools.updateQueue.updates.length > 0) {
                raf(Tools.updateQueue.Update);
                var l = Tools.updateQueue.updates.slice();
                Tools.updateQueue.updates = [];
                for (var i = 0; i < l.length; i++)
                    l[i].update(timestamp);
                delete l;
            } else {
                Tools.updateQueue.isWorking = false;
            }
        }
    };
    function updater(func, ele) {
        return { id: ele, update: func };
    }

    window.requestAnimationFrame = function (callback, ele) {
        Tools.updateQueue.updates.RemoveWhere(function (u) { return u.id == ele; });
        Tools.updateQueue.updates.push(updater(callback, ele));
        Tools.updateQueue.isWorking || raf(Tools.updateQueue.Update);
    }
}());
/*Utilities*/
(function () {
    window.requestAnimFrame = window.requestAnimationFrame;
    Tools.defaultSize = { width: 1366, height: 768 };
    Tools.isImageElement = function (img) {
        return (img instanceof Image) || (img instanceof HTMLCanvasElement) || (img instanceof HTMLImageElement)
    }
    window.Swap = function (a, b) {
        var tmp = b;
        b = a;
        a = tmp;
        delete tmp;
    }
    window.isArguments = function (item) {
        return Object.prototype.toString.call(item) === '[object Arguments]';
    }
    window.SliceArgs = function (args, start, length) {
        return Array.prototype.slice.apply(args, Array.prototype.slice.call(arguments, 1));
    }
    window.ArgsParseArray = function (args) {
        return Array.prototype.slice.call(arguments[0]);
    }
    window.ClearIndivisualProps = function (obj, skip) {
        if (!obj.hasOwnProperty) { return; }
        var check = function () { return false; };
        if (skip) {
            if (skip instanceof Array)
                check = function (o) { return skip.indexOf(o) > -1; }
            else
                check = function (o) { return o in skip; }
        }
        for (var i in obj)
            if (obj.hasOwnProperty(i) && !(skip && check(i)))
                delete obj[i];
        return obj;
    }
    window.ParseArray = function () {
        return Array.prototype.slice.call(arguments);
    }
    window.CopyProps = function (target, src) {
        for (var i in src)
            target[i] = src[i];
        return target;
    }
    function DeepCopyProps(opt) {
        if (arguments.length > 1) {
            for (var i = 0; i < arguments.length; i++)
                DeepCopyProps.call(this, arguments[i]);
        } else {
            for (var i in opt) {
                if (opt.hasOwnProperty(i)) {
                    if (opt[i] instanceof Object && this[i])
                        DeepCopyProps.call(this[i], opt[i]);
                    else
                        this[i] = opt[i];
                }
            }
            delete opt;
        }
        return this;
    }
    window.DeepCopyProps = DeepCopyProps;

    window.isPercentage = function (str) {
        var type = typeof (str);
        if (type == "number") return str;
        if (type != "string") return 0;
        var num = str.replace(/\D/g, "");
        if ((str.indexOf("%") != -1) && Number(num))
            return str;
        else return false;
    }
    Number.prototype.Pad = function (pad, length) {
        var str = String(this);
        var len = str.length;
        var prefix = (this >= 0 ? "" : "-");
        return prefix + Array(length - len + 1).join(pad) + Math.abs(this);
    }
    window.ParseDate = function (val) {
        if (val.value)
            val = val.value;
        else if (val.Value)
            val = val.Value;
        if (typeof (val) == "string")
            return new Date(Number(val.replace(/[^0-9]/g, "")));
        else
            return val;
    }
    window.ParsePercentage = function (str, base) {
        base = base || 1;
        var type = typeof (str);
        if (type == "number") return str;
        if (type != "string") return 0
        var percentage = (str.indexOf("%") != -1) ? (base / 100) : 1;
        //var num = str.replace(/\D/g, "");
        var num = str.replace("%", "");
        return Number(num) * percentage;
    }
    window.isUndefined = function (target) {
        return !(typeof (target) != "undefined" && target != null);
    }
    window.TestUndefined = function (target, defaultValue) {
        return (typeof (target) != "undefined" && target != null) ? target : defaultValue;
    }

    var shortenDollar = " KMB";
    Tools.DollarParser = function (num, decimal, prefix) {
        prefix = prefix || "";
        decimal = TestUndefined(decimal, 2);
        var str = String(num);
        var digit = str.length;
        var dif = Math.floor((str.length - 1) / 3);
        var appfix = dif ? shortenDollar[dif] : "";
        str = Math.floor(num / Math.pow(1000, dif) * 100) / 100;
        return prefix + str + appfix;
    }

    Tools.Enumify = function (arr) {
        var obj = Object.create(null);
        for (var i = 0; i < arguments.length; i++) {
            obj[arguments[i]] = i;
        }
        return obj;
    }
    Tools.timeDif = 0;
    Object.defineProperties(Tools, {
        Now: {
            get: function () {
                var t = new Date(Tools.NowTime + Tools.timeDif);
                return t;
            }
        },
        NowTime: {
            get: function () {
                var t = new Date();
                var result = t.getTime();
                delete t;
                return result + Tools.timedif;
            }
        }
    })

    Tools.StaticValuePacker = function () {
        var o = {}, src = arguments[0];
        for (var i in src) {
            if (src.hasOwnProperty(i)) {
                if (src[i] instanceof Array) {
                    o[i] = Object.create(null);
                    for (var prop = 0; prop < src[i].length; prop++) {
                        o[i][src[i][prop]] = src[i][prop];
                    }
                } else {
                    o[i] = Tools.StaticValuePacker(src[i]);
                }
            }
        }
        return o;
    }
    Tools.Normalize = function () {
        for (var i in Tools.Category) {
            for (var j in Tools.Category[i]) {
                window[j] = Tools.Category[i][j];
            }
        }
    }

    var ErrType = {
        Connection: Tools.StaticValuePacker({ values: ["NOT_CLOSED", "SENDING"] }).values,
        Arguments: Tools.StaticValuePacker({ values: ["NON_VALID_ARGUMENT"] }).values
    };
    Tools.ErrorMsg =
        function (maintype, subtype, params) {
            var type = Array.prototype.splice.call(arguments, 0, 2);
            switch (type[0]) {
                default: return;
                case "Connection":
                    switch (type[1]) {
                        case ErrType.Connection.NOT_CLOSED:
                            return [arguments[0].Name + ":Not closed"].join(" ");
                        case ErrType.Connection.SENDING:
                            return [arguments[0].Name + ":msg sent, waiting for response, not allow to send more msg"].join(" ");
                    }
                    break;
                case "Arguments":
                    switch (type[1]) {
                        case ErrType.Arguments.NON_VALID_ARGUMENT:
                            return [arguments[0].constructor.name + "." + arguments[1].callee.name, ": non valid argument length", arguments[1].length].join(" ");
                        default:
                            return [arguments[0].constructor.name + "." + arguments[1].callee.name, " : ", arguments[2]].join(" ");

                    }
            }
        }
    var err = Tools.ErrorMsg;
    err.Type = ErrType;
    err.Connection = function (sender) {
        return err("Connection", type, sender)
    }
    err.Arguments = function (sender, args) {
        return err.apply(this, ["Arguments", type, sender, args].concat(Array.prototype.slice.call(arguments, 2)));
    }
    for (var typeName in ErrType) {
        var type = ErrType[typeName];
        for (var subType in type) {
            switch (typeName) {
                case "Connection":
                    err[typeName][subType] = function (subType) {
                        return function (sender) {
                            return err("Connection", subType, sender)
                        }
                    }(subType)
                    break;
                case "Arguments":
                    err[typeName][subType] = function (subType) {
                        return function (sender, args) {
                            return err("Arguments", subType, sender, args);
                        }
                    }(subType)
                    break;
            }
        }
    }


    /*func extend */
    window.ClassExtend = function (subclass, superclass, opt) {
        function o() { this.constructor = subclass; }
        o.prototype = superclass.prototype;
        var proto = (subclass.prototype = new o());
        if (opt) CopyProps(proto, opt);
        return proto;
    }

    //tool 滑鼠在canvas上的座標
    window.getMousePos = function (obj, evt, deep) {
        var target = evt.target;
        if (evt.changedTouches) {
            evt = evt.changedTouches[0];
        } else if (evt.touches) {
            evt = evt.touches[0];
        }
        var rect = target.getBoundingClientRect() || 0;
        //if (deep) {
        //    $.extend(p, evt);
        //}
        obj.x = evt.clientX - rect.left;
        obj.y = evt.clientY - rect.top;
        return obj;
    }
    /*tool 設定滑鼠游標圖示(0:連結 1:箭頭)*/
    window.SetCursor = function (type) {
        switch (type) {
            case 0: $('body').css({ 'cursor': 'pointer' }); break;
            case 1: $('body').css({ 'cursor': 'default' }); break;
        }
    }


    /*Utility Poolize*/
    window.Poolize = function (target) {
        target._freePool = [];
        target.Get = Get;
        target.Release = Release;
    }
    function Release(obj) {
        if (obj instanceof this) {
            ClearIndivisualProps(obj)
            this._freePool.push(obj);
        }
    }
    function Get() {
        if (this._freePool.length) {
            return this._freePool.pop();
        } else {
            return this.constructor.apply(this, arguments);
        }
    }

    /*Utility Tween*/
    function Tween() { }
    Tools.Tween = {
        dif: function (p, a, b) {
            return a + (b - a) * p;
        },
        Rect: function (target, p, s, d) {
            p = p || 0;
            target.x = Tools.Tween.dif(p, s.x, d.x);
            target.y = Tools.Tween.dif(p, s.y, d.y);
            target.width = Tools.Tween.dif(p, s.width, d.width || s.width);
            target.height = Tools.Tween.dif(p, s.height, d.height || s.height);
            return target;
        }
    };

    /*class Timer */
    //int duration, int delay, (bool nonStop), (opt:{string pool})
    function Timer(duration, opt) {
        this.Set(duration, opt);
        Timer.AddToPool(this);
        Timer.count++;
    }
    Timer.count = 0;
    Timer._pools = { _default: [] },
    Timer._free = [];
    Timer.Get = function (duration, opt) {
        var timer;
        if (Timer._free.length > 0) {
            timer = Timer._free.pop();
            timer.Set(duration, opt);
        } else {
            timer = new Timer(duration, opt);
        }
        Timer.AddToPool(timer);
        return timer;
    }
    Timer.Release = function (obj) {
        if (obj instanceof Timer) {
            Timer.RemoveFromPool(obj)
            ClearIndivisualProps(obj)
            Timer._free.push(obj);
        }
    }
    Timer.ReleasePool = function (pool) {
        if (pool in Timer._pools)
            for (var i in Timer._pools[pool])
                if (Timer._pools[pool].hasOwnProperty(i))
                    Timer.Release(Timer._pools[pool][i]);
    }
    Timer.AddToPool = function (obj) {
        if (obj instanceof Timer) {
            if (!Timer._pools[obj.pool])
                Timer._pools[obj.pool] = [];
            Timer._pools[obj.pool].push(obj);
        }
    }
    Timer.RemoveFromPool = function (obj) {
        if (obj instanceof Timer)
            Timer._pools[obj.pool].RemoveWhere(function (o) { return (o === obj); });
    }
    Timer.UpdatePool = function (pool) {
        pool = pool || "_default";
        var now = Tools.NowTime;
        if (!Timer._pools[pool]) { return; }
        for (var i = 0; i < Timer._pools[pool].length; i++) {
            var t = Timer._pools[pool][i];
            if (t.isEnabled)
                t.Update(now);
        }
    }
    Timer.Clear = function (pool) {
        pool = pool || "_default";
        for (var i = 0; i < Timer._pools.length; i++) {
            Timer._pools[pool][i].Disable();
        }
        Timer._pools[pool] = [];
    }
    Timer.PassPool = function (pass, pool) {
        pool = pool || "_default";
        for (var i = 0; i < Timer._pools.length; i++) {
            Timer._pools[pool][i].SetProgress(pass);
        }
    }
    Timer.DISABLED = 0;
    Timer.WAIT = 1;
    Timer.WORKING = 2;
    Timer.ENDED = 3;
    Timer.prototype = {
        pool: "_default",
        isEnabled: false,
        isContinuous: false,
        State: 0,
        Start: 0,
        End: 0,
        Duration: 0,
        Pass: 0,
        Remain: 0,
        Porportion: 0,
        Enable: function () {
            this.isEnabled = true;
            return this;
        },
        Restart: function () {
            this.Enable();
            this.Set(this.Duration);
        },
        Disable: function () {
            this.isEnabled = false;
            return this;
        },
        UpdateState: function (now) {
            if (!this.isEnabled) {
                this.State = Timer.DISABLED;
            } else {
                if (now < this.Start) {
                    this.State = Timer.WAIT;
                } else {
                    if (!this.isContinuous && now > this.End) {
                        if (this.State == Timer.WORKING) {
                            this.callback && this.callback();
                            delete this.callback;
                        }
                        this.State = Timer.ENDED;
                    } else {
                        this.State = Timer.WORKING;
                    }
                }
            }
        },
        Update: function (now) {
            if (!this.isEnabled) {
                this.State = Timer.DISABLED;
                return this;
            }
            now = now || Tools.NowTime;
            this.Pass = now - this.Start;
            this.Remain = this.End - now;
            this.Porportion = this.Pass / this.Duration;
            if (!this.isContinuous) {
                this.Porportion = this.Porportion.Range(0, 1);
            }
            this.UpdateState(now);
            return this;
        },
        Set: function (duration, opt) {
            opt = opt || {};
            this.isContinuous = opt.nonstop || this.isContinuous || false;
            this.pool = opt.pool || this.pool || "_default";
            this.callback = opt.callback || null;
            duration = duration || 0;
            delay = opt.delay || 0;
            var now = Tools.NowTime;
            this.Start = now + delay;
            this.End = this.Start + duration;
            this.Duration = duration;
            this.isEnabled = true;
            this.Update(now);
            return this;
        },
        SetProgress: function (pass) {
            this.Start -= pass;
            this.End -= pass;
            this.Update();
        },
        Finish: function () {
            var now = Tools.NowTime;
            this.End = now;
            this.Update(now);
        },
    };


    Tools.Timer = Timer;
    var TotalImages = 0, ImageLoaded = 0;
    Tools.GetTotalImage = function () { return TotalImages; }
    Tools.GetImageLoaded = function () { return ImageLoaded; }
    Tools.ClearImageLoadedCount = function () {
        TotalImages = 0;
        ImageLoaded = 0;
    }
    Object.defineProperties(Tools, {
        ImageLoaded: { get: function () { return ImageLoaded; } },
        ImageCount: { get: function () { return TotalImages; } },
        ImageLoadingProgress: { get: function () { return ImageLoaded / (TotalImages || 1); } }
    })
    /*class ImgObj*/
    function ImgO(src, callback) {
        TotalImages++;
        var image = new Image();
        image.src = src;
        image.onload = callback || LoadComplete;
        return image;
    }
    window.ImgO = ImgO;

    /*Event LoadComplete*/
    function LoadComplete() {
        try {
            if (++ImageLoaded >= TotalImages) {
                Tools.ImgAllLoaded();
                LoadComplete = function () { };
                //setTimeout(parent.gameHideLoad, 500);
                //parent.LoaderUpdate(1);
            } else {
                Tools.ImgLoaded();
                //parent.LoaderUpdate(ImageLoaded / TotalImages);
            }
            //window.parent && window.parent.LoaderUpdate(ImageLoaded / TotalImages);
        } catch (e) { }
    }

    /*Event ImgLoad*/
    function ImgLoaded() { }
    Tools.ImgLoaded = ImgLoaded;

    /*Event ImgAllLoaded*/
    function ImgAllLoaded() { }
    Tools.ImgAllLoaded = ImgAllLoaded;

    /*float Easing*/
    Tools.Easing = {
        // no easing, no acceleration
        linear: function (t) { return t },
        // accelerating from zero velocity
        easeInQuad: function (t) { return t * t },
        // decelerating to zero velocity
        easeOutQuad: function (t) { return t * (2 - t) },
        // acceleration until halfway, then deceleration
        easeInOutQuad: function (t) { return t < .5 ? 2 * t * t : -1 + (4 - 2 * t) * t },
        // accelerating from zero velocity 
        easeInCubic: function (t) { return t * t * t },
        // decelerating to zero velocity 
        easeOutCubic: function (t) { return (--t) * t * t + 1 },
        // acceleration until halfway, then deceleration 
        easeInOutCubic: function (t) { return t < .5 ? 4 * t * t * t : (t - 1) * (2 * t - 2) * (2 * t - 2) + 1 },
        // accelerating from zero velocity 
        easeInQuart: function (t) { return t * t * t * t },
        // decelerating to zero velocity 
        easeOutQuart: function (t) { return 1 - (--t) * t * t * t },
        // acceleration until halfway, then deceleration
        easeInOutQuart: function (t) { return t < .5 ? 8 * t * t * t * t : 1 - 8 * (--t) * t * t * t },
        // accelerating from zero velocity
        easeInQuint: function (t) { return t * t * t * t * t },
        // decelerating to zero velocity
        easeOutQuint: function (t) { return 1 + (--t) * t * t * t * t },
        // acceleration until halfway, then deceleration 
        easeInOutQuint: function (t) { return t < .5 ? 16 * t * t * t * t * t : 1 + 16 * (--t) * t * t * t * t }
    }

    // t: current time, b: begInnIng value, c: change In value, d: duration
    $.easing.jswing = $.easing.swing;

    $.extend($.easing,
    {
        def: 'easeOutQuad',
        swing: function (x, t, b, c, d) {
            //alert($.easing.default);
            return $.easing[$.easing.def](x, t, b, c, d);
        },
        easeInQuad: function (x, t, b, c, d) {
            return c * (t /= d) * t + b;
        },
        easeOutQuad: function (x, t, b, c, d) {
            return -c * (t /= d) * (t - 2) + b;
        },
        easeInOutQuad: function (x, t, b, c, d) {
            if ((t /= d / 2) < 1) return c / 2 * t * t + b;
            return -c / 2 * ((--t) * (t - 2) - 1) + b;
        },
        easeInCubic: function (x, t, b, c, d) {
            return c * (t /= d) * t * t + b;
        },
        easeOutCubic: function (x, t, b, c, d) {
            return c * ((t = t / d - 1) * t * t + 1) + b;
        },
        easeInOutCubic: function (x, t, b, c, d) {
            if ((t /= d / 2) < 1) return c / 2 * t * t * t + b;
            return c / 2 * ((t -= 2) * t * t + 2) + b;
        },
        easeInQuart: function (x, t, b, c, d) {
            return c * (t /= d) * t * t * t + b;
        },
        easeOutQuart: function (x, t, b, c, d) {
            return -c * ((t = t / d - 1) * t * t * t - 1) + b;
        },
        easeInOutQuart: function (x, t, b, c, d) {
            if ((t /= d / 2) < 1) return c / 2 * t * t * t * t + b;
            return -c / 2 * ((t -= 2) * t * t * t - 2) + b;
        },
        easeInQuint: function (x, t, b, c, d) {
            return c * (t /= d) * t * t * t * t + b;
        },
        easeOutQuint: function (x, t, b, c, d) {
            return c * ((t = t / d - 1) * t * t * t * t + 1) + b;
        },
        easeInOutQuint: function (x, t, b, c, d) {
            if ((t /= d / 2) < 1) return c / 2 * t * t * t * t * t + b;
            return c / 2 * ((t -= 2) * t * t * t * t + 2) + b;
        },
        easeInSine: function (x, t, b, c, d) {
            return -c * Math.cos(t / d * (Math.PI / 2)) + c + b;
        },
        easeOutSine: function (x, t, b, c, d) {
            return c * Math.sin(t / d * (Math.PI / 2)) + b;
        },
        easeInOutSine: function (x, t, b, c, d) {
            return -c / 2 * (Math.cos(Math.PI * t / d) - 1) + b;
        },
        easeInExpo: function (x, t, b, c, d) {
            return (t == 0) ? b : c * Math.pow(2, 10 * (t / d - 1)) + b;
        },
        easeOutExpo: function (x, t, b, c, d) {
            return (t == d) ? b + c : c * (-Math.pow(2, -10 * t / d) + 1) + b;
        },
        easeInOutExpo: function (x, t, b, c, d) {
            if (t == 0) return b;
            if (t == d) return b + c;
            if ((t /= d / 2) < 1) return c / 2 * Math.pow(2, 10 * (t - 1)) + b;
            return c / 2 * (-Math.pow(2, -10 * --t) + 2) + b;
        },
        easeInCirc: function (x, t, b, c, d) {
            return -c * (Math.sqrt(1 - (t /= d) * t) - 1) + b;
        },
        easeOutCirc: function (x, t, b, c, d) {
            return c * Math.sqrt(1 - (t = t / d - 1) * t) + b;
        },
        easeInOutCirc: function (x, t, b, c, d) {
            if ((t /= d / 2) < 1) return -c / 2 * (Math.sqrt(1 - t * t) - 1) + b;
            return c / 2 * (Math.sqrt(1 - (t -= 2) * t) + 1) + b;
        },
        easeInElastic: function (x, t, b, c, d) {
            var s = 1.70158; var p = 0; var a = c;
            if (t == 0) return b; if ((t /= d) == 1) return b + c; if (!p) p = d * .3;
            if (a < Math.abs(c)) { a = c; var s = p / 4; }
            else var s = p / (2 * Math.PI) * Math.asin(c / a);
            return -(a * Math.pow(2, 10 * (t -= 1)) * Math.sin((t * d - s) * (2 * Math.PI) / p)) + b;
        },
        easeOutElastic: function (x, t, b, c, d) {
            var s = 1.70158; var p = 0; var a = c;
            if (t == 0) return b; if ((t /= d) == 1) return b + c; if (!p) p = d * .3;
            if (a < Math.abs(c)) { a = c; var s = p / 4; }
            else var s = p / (2 * Math.PI) * Math.asin(c / a);
            return a * Math.pow(2, -10 * t) * Math.sin((t * d - s) * (2 * Math.PI) / p) + c + b;
        },
        easeInOutElastic: function (x, t, b, c, d) {
            var s = 1.70158; var p = 0; var a = c;
            if (t == 0) return b; if ((t /= d / 2) == 2) return b + c; if (!p) p = d * (.3 * 1.5);
            if (a < Math.abs(c)) { a = c; var s = p / 4; }
            else var s = p / (2 * Math.PI) * Math.asin(c / a);
            if (t < 1) return -.5 * (a * Math.pow(2, 10 * (t -= 1)) * Math.sin((t * d - s) * (2 * Math.PI) / p)) + b;
            return a * Math.pow(2, -10 * (t -= 1)) * Math.sin((t * d - s) * (2 * Math.PI) / p) * .5 + c + b;
        },
        easeInBack: function (x, t, b, c, d, s) {
            if (s == undefined) s = 1.70158;
            return c * (t /= d) * t * ((s + 1) * t - s) + b;
        },
        easeOutBack: function (x, t, b, c, d, s) {
            if (s == undefined) s = 1.70158;
            return c * ((t = t / d - 1) * t * ((s + 1) * t + s) + 1) + b;
        },
        easeInOutBack: function (x, t, b, c, d, s) {
            if (s == undefined) s = 1.70158;
            if ((t /= d / 2) < 1) return c / 2 * (t * t * (((s *= (1.525)) + 1) * t - s)) + b;
            return c / 2 * ((t -= 2) * t * (((s *= (1.525)) + 1) * t + s) + 2) + b;
        },
        easeInBounce: function (x, t, b, c, d) {
            return c - $.easing.easeOutBounce(x, d - t, 0, c, d) + b;
        },
        easeOutBounce: function (x, t, b, c, d) {
            if ((t /= d) < (1 / 2.75)) {
                return c * (7.5625 * t * t) + b;
            } else if (t < (2 / 2.75)) {
                return c * (7.5625 * (t -= (1.5 / 2.75)) * t + .75) + b;
            } else if (t < (2.5 / 2.75)) {
                return c * (7.5625 * (t -= (2.25 / 2.75)) * t + .9375) + b;
            } else {
                return c * (7.5625 * (t -= (2.625 / 2.75)) * t + .984375) + b;
            }
        },
        easeInOutBounce: function (x, t, b, c, d) {
            if (t < d / 2) return $.easing.easeInBounce(x, t * 2, 0, c, d) * .5 + b;
            return $.easing.easeOutBounce(x, t * 2 - d, 0, c, d) * .5 + c * .5 + b;
        }
    });
    //取得canvas解析度比例(SetHDCanvas使用)
    function getPixelRatio(context) {
        var backingStore = context.backingStorePixelRatio ||
          context.webkitBackingStorePixelRatio ||
          context.mozBackingStorePixelRatio ||
          context.msBackingStorePixelRatio ||
          context.oBackingStorePixelRatio ||
          context.backingStorePixelRatio || 1;
        return (window.devicePixelRatio || 1) / backingStore;
    };
    Tools.GetPixelRatio = getPixelRatio;

    //設定HD的canvas物件
    function SetHDPICanvas(can, w, h, ratio) {
        can = can || Object.create(HTMLCanvasElement.prototype);
        if (!ratio) { ratio = getPixelRatio(can.getContext('2d')); }
        w = w || can.width;
        h = h || can.height;
        can.width = w * ratio;
        can.height = h * ratio;
        can.style.width = w + "px";
        can.style.height = h + "px";
        can.scale = ratio;
        var tx = can.getContext("2d");
        tx.setTransform(ratio, 0, 0, ratio, 0, 0);
        return tx;
    }
    Tools.SetHDPICanvas = SetHDPICanvas;


    //Event 網頁切換事件
    Tools.VisibilityChange = new (function () {
        this.onInvisible = [], this.onVisible = [];
        var hidden, visibilityChange, IsInvisible, Supported = 0;
        var vi = this;
        this.addOnVisible = function (ele, func) {
            if (typeof func == "function") {
                vi.onVisible.push({ scope: ele, func: func });
            }
            return this;
        }
        this.addOnInvisible = function (ele, func) {
            if (typeof func == "function") {
                vi.onInvisible.push({ scope: ele, func: func });
            }
            return this;
        }
        function actInvisible() {
            for (var i = 0; i < vi.onInvisible.length; i++) { vi.onInvisible[i].func.call(vi.onInvisible[i].scope); }
        }
        function actVisible() {
            for (var i = 0; i < vi.onVisible.length; i++) { vi.onVisible[i].func.call(vi.onVisible[i].scope); }
        }
        this.PatchEventListener = function () {
            if (typeof document.hidden !== "undefined") { // Opera 12.10 and Firefox 18 and later support 
                hidden = "hidden";
                visibilityChange = "visibilitychange";
            } else if (typeof document.mozHidden !== "undefined") {
                hidden = "mozHidden";
                visibilityChange = "mozvisibilitychange";
            } else if (typeof document.msHidden !== "undefined") {
                hidden = "msHidden";
                visibilityChange = "msvisibilitychange";
            } else if (typeof document.webkitHidden !== "undefined") {
                hidden = "webkitHidden";
                visibilityChange = "webkitvisibilitychange";
            } else { hidden = 0; visibilityChange = 0; }
            Supported = (!document[hidden]);
            if (!Supported) {
                $(window).focus(actVisible);
                $(window).onpageshow = actVisible;

                $(window).blur(actInvisible);
                $(window).onpagehide = actInvisible;
            } else {
                document.addEventListener(visibilityChange, function () {
                    if (document[hidden]) {
                        IsInvisible = 1;
                        actInvisible();
                    } else {
                        IsInvisible = 0;
                        actVisible();
                    }
                }, false);
            }
        }
        var test = 0;
    })
    $(document).ready(function () {
        Tools.VisibilityChange.PatchEventListener();
    });
}());

Tools.DebugType = Tools.StaticValuePacker({ val: ["Connection", "Gesture", "ImageLoader"] }).val;
/*utility RandomGenerator*/
(function () {
    var char = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
    var num = "0123456789";
    Tools.RandomGenerator = {
        String: function (length, seed) {
            return generate(length, char, seed);
        },
        Number: function (length, seed) {
            return generate(length, num, seed);
        },
        Mix: function (length, seed) {
            return generate(length, char + num, seed);
        }
    }
    function generate(length, src, seed) {
        var text = (TestUndefined(seed, "").toString()).replace(/\W/g, "");
        for (var i = 0; i < length; i++)
            text += src.charAt(Math.floor(Math.random() * src.length));
        return text;
    }
}());

/*utility imageloader*/
(function () {
    var ImageLoader =
     new function () {
         this.res = null;
         var self = this;

         this.isLoading = false;
         var _packageCode;
         var _loaded = 0, _loadFailed = 0, _resCount = 0;


         function searchManifest(manifest) {
             for (var i in manifest)
                 if (manifest.hasOwnProperty(i)) {
                     if (manifest[i].id && manifest[i].src) {
                         self.res[manifest[i].id] = manifest[i].src;
                         _resCount++;
                     } else {
                         searchManifest(manifest[i]);
                     }
                 }
         }

         this.push = function (id, src) {
             if (arguments[0] instanceof Object) {
                 searchManifest(arguments[0])
                 return;
             }

             if (!(typeof (id) == "string")) {
                 Tools.RecordLog("ImageLoader: non valid id: " + id, Tools.DebugType.ImageLoader);
                 return;
             }
             if (res[id]) {
                 Tools.RecordLog("ImageLoader: duplicate res: " + id, Tools.DebugType.ImageLoader);
                 _resCount--;
             }

             self.res[id] = src;
             _resCount++;
         }



         this.Reset = function () {
             self.res = Object.create(null);
             _loaded = 0;
             _resCount = 0;
             _packageCode = Tools.RandomGenerator.Mix(1, Tools.NowTime)
         }
         function res() {
             var src = arguments[0],
                 code = arguments[1];
             if (arguments[0] instanceof Image)
                 src = arguments[0].src;
             var img = new Image();
             img.src = src;
             //img.onload = onload;
             //img.onerror = onerror
             img.onload = HandleOnLoad;
             img.onerror = HandleOnLoad;
             img.packageCode = code;
             return img;
         }
         this.Load = function (manifest) {
             self.Reset();
             self.push.apply(this, arguments);
             self.StartQueue();
         }
         var queue;
         this.StartQueue = function () {
             queue = [];
             _resCount = 0;
             for (var i in self.res) {
                 queue.push(i);
                 _resCount++;
             }
             _onProgressChange();
             LoadNext(_packageCode);
         }
         function LoadNext(code) {
             if (code != _packageCode || !queue.length) return;
             var i = queue.pop();
             self.res[i] = res(self.res[i], _packageCode);
         }
         function HandleOnLoad(e) {
             switch (e.type) {
                 case "error":
                     (e.target.packageCode == _packageCode) && _loadFailed++;
                     break;
                 case "load":
                     (e.target.packageCode == _packageCode) && _loaded++;
                     break;
             }
             _onProgressChange();
             setTimeout(LoadNext.bind(this, e.target.packageCode), 10);
         }


         function _onProgressChange() {
             self.OnProgressChange(_loaded, _resCount, _loadFailed);
             if (_loaded + _loadFailed == _resCount)
                 _loadComplete();
         }

         function _loadComplete() {
             Tools.RecordLog("ImageLoader: load complete", Tools.Debug.ImageLoader);
             //self.Reset();
             self.OnLoadComplete.call(window);
         }
         this.OnLoadComplete = function () { }
         /*on every progress changed
             nowLoaded: res loaded count
             totalCount: length of loader queue
             errorCount: counter that fail to load */
         this.OnProgressChange = function (nowLoaded, totalCount, errorCount) { }

         /*init*/
         this.Reset();

         /*cache from remote resource*/
         this.cacheRes = Object.create(null);
         this.AddCache = function (id, base64str) {
             var image = new Image();
             image.src = base64str;
             this.cacheRes[id] = image;
         }

         Object.defineProperties(this, {
             Status: {
                 get: function () {
                     if (!_resCount) return 0;
                     if (_loaded + _loadFailed != _resCount) return 1;
                     else return 2;
                 }
             },
         })

         /*Tools.Res Tools.CacheRes*/
         Object.defineProperties(Tools, {

             Res: {
                 get: function () {
                     return Tools.ImageLoader.res;
                 }
             },
             CacheRes: {
                 get: function () {
                     return Tools.ImageLoader.cacheRes;
                 }
             }
         })
     }
    Tools.ImageLoader = ImageLoader;

    function getImage(img) {
        if (typeof (img) == "string") {
            return Tools.Res[img] || Tools.CacheRes[img] || img;
        } else
            return img
    }
    Tools.GetImage = getImage;
}());


/*main part*/
(function () {
    /*Main Zoom*/
    var zoom = zoom || 1;

    Tools.Release = function (obj) {
        if (arguments.length > 1) {
            for (var i = 0; i < arguments.length; i++) {
                Tools.Release(arguments[i]);
            }
        } else if (obj && obj.length && obj.length > 1) {
            for (var i = 0; i < obj.length; i++) {
                Tools.Release(obj[i]);
            }
        } else {
            if (!obj) { return; }
            if (obj instanceof Tools.Point) {
                Tools.Point.Release(obj);
            } else if (obj instanceof Tools.Size) {
                Tools.Size.Release(obj);
            } else if (obj instanceof Tools.Rect) {
                Tools.Rect.Release(obj);
            }
        }
    }

    Tools.SetZoom = function (value) {
        if (zoom != value) {
            zoom = value;
        }
    }
    Tools.GetZoom = function () {
        return zoom;
    }

    Object.defineProperty(Tools, "Zoom", {
        get: function () { return zoom; },
        set: function (val) { if (typeof (val) == "number") zoom = val; }
    })
    /*bool 判斷座標是否在Rect{x,y,w,h}中*/
    Tools.InRect = function (p0, rect) {
        var px, py, x, y, w, h;
        if (p0 instanceof Tools.Point) {
            px = p0.x; py = p0.y;
        }
        if (arguments[1] instanceof Tools.Rect) {
            x = arguments[1].x; y = arguments[1].y; w = arguments[1].width; h = arguments[1].height;
        }
        if (arguments.length == 3) {
            if (p0 instanceof Tools.Point) {//point point size
                x = arguments[1].x;
                y = arguments[1].y;
                w = arguments[2].width;
                h = arguments[2].height;
            } else {//x y arguments[2]
                px = arguments[0];
                py = arguments[1];
                x = arguments[2].x;
                y = arguments[2].y;
                w = arguments[2].width;
                h = arguments[2].height;
            }
        }
        if (!w || !h) {
            return false;
        } else {
            return ((px > x) && (px <= x + w) && (py > y) && (py <= y + h));
        }
    }
    Tools.InTriangle = function (p0, pa, pb, pc) {
        var totalArea = AreaOfTriangle(pa, pb, pc),
        testArea = AreaOfTriangle(p0, pa, pb) + AreaOfTriangle(p0, pa, pc) + AreaOfTriangle(p0, pb, pc);
        return totalArea >= testArea;
    }
    function AreaOfTriangle(p0, p1, p2) {
        return ((p0.x - p2.x)(p1.y - p2.y) - (p1.x - p2.x)(p0.y - p2.y)) / 2;
    }

    /*bool 檢測是否在圓形範圍內
    [zoom],p,rect
    [zoom],p,midp,radius    */
    Tools.InCircle = function (p, CircleRect) {
        var z = zoom;
        var x0, x1, y0, y1, r;
        switch (arguments.length) {
            case 2://point+CircleRect(x,y,w,h)
                x0 = arguments[0].x;
                y0 = arguments[0].y;
                var midp = new MidPoint(arguments[1]);
                x1 = midp.x * z;
                y1 = midp.y * z;
                r = arguments[1].width / 2 * z;
                break;
            case 3://Point+midPoint+Radius
                x0 = arguments[0].x;
                y0 = arguments[0].y;
                x1 = arguments[1].x * z;
                y1 = arguments[1].y * z;
                r = arguments[2] * z;
                break;
        }

        var rx = Math.abs(x1 - x0), ry = Math.abs(y1 - y0);
        var dis = Math.sqrt((rx * rx + ry * ry));
        if (dis < r) {
            return true;
        } else {
            return false;
        }

    }

    function newObject(obj) {
        return new (Function.prototype.bind.apply(obj, arguments[1]));
    }
    //class Point/////////////////////////////////////////////////////////////
    function Point(x, y) {
        if (isArguments(arguments[0])) {
            arguments = arguments[0];
        }
        this.Set(arguments);
        Point.count++;
    }
    CopyProps(Point, {
        count: 0,
        _freeCache: [],
        Release: function (obj) {
            if (obj instanceof Tools.Point) {
                ClearIndivisualProps(obj);
                Point._freeCache.push(obj);
            }
        },
        Get: function () {
            if (isArguments(arguments[0])) {
                arguments = arguments[0];
            }
            if (Point._freeCache.length > 0) {
                var r = Point._freeCache.pop();
                r.Set(arguments);
                return r;
            } else {
                return new Point(arguments);
            }
        }
    });

    Point.prototype = {
        x: 0,
        y: 0,
        GetPoint: function (p) {
            this.x = p.x;
            this.y = p.y;
            return this;
        },
        Set: function () {
            if (arguments.length == 0) {
                this.x = 0; this.y = 0;
            }
            if (isArguments(arguments[0])) {
                arguments = arguments[0];
            }
            if ((arguments[0] instanceof Tools.Point)
           || (arguments[0] instanceof Tools.Rect)) {
                this.x = arguments[0].x;
                this.y = arguments[0].y;
            } else {
                this.x = arguments[0] || 0;
                this.y = arguments[1] || 0;
            }
            return this;
        },
        Clone: function () {
            return Tools.Point.Get(this);
        },
        Type: "Point",
        Move: function () {
            var dx, dy;
            if (arguments.length == 1) {
                if ((arguments[0] instanceof Point) || (arguments[0] instanceof Rect)) {
                    dx = arguments[0].x;
                    dy = arguments[0].y;
                } else if (arguments[0] instanceof Size) {
                    dx = arguments[0].width;
                    dy = arguments[0].height
                }
            } else {
                dx = arguments[0];
                dy = arguments[1];
            }
            this.x += dx; this.y += dy;
            return this;
        },
        InRect: function () {
            var nArguments = [this].concat(Array.prototype.slice.call(arguments));
            return Tools.InRect.apply(this, nArguments);
        },
        Scale: function (dx, dy) {
            switch (arguments.length) {
                case 0: dx = dy = zoom; break;
                case 1: dx = dy = arguments[0]; break;
            }
            this.x *= dx;
            this.y *= dy;
            return this;
        },
        Translate: function (dx, dy) {
            if ((arguments[0] instanceof Point) || (arguments[0] instanceof Rect)) {
                dy = arguments[0].y;
                dx = arguments[0].x;
            }
            this.x -= dx;
            this.y -= dy;
            return this;
        },
        VectorTo: function (p) {
            return Tools.Size.Get(p.x - this.x, p.y - this.y);
        },
        toCss: function (r) {
            r = r || 1;
            return { left: this.x * r, top: this.y * r };
        }
    };
    Tools.Point = Point;

    //class Size尺寸物件/////////////////////////////////////////////////////////////
    function Size(w, h) {
        if (isArguments(arguments[0])) {
            arguments = arguments[0];
        }
        this.Set(arguments);
        Size.count++;
    }
    Size.count = 0;
    Size._freeCache = [];
    Size.Release = function (obj) {
        if (obj instanceof Tools.Size) {
            for (var p in obj) {
                if (obj.hasOwnProperty(p))
                    delete obj[p];
            }
            if (Size._freeCache.length < 1000) {
                Size._freeCache.push(obj);
            }
        }
    }
    Size.Get = function () {
        if (isArguments(arguments[0])) {
            arguments = arguments[0];
        }
        if (Size._freeCache.length > 0) {
            var r = Size._freeCache.pop();
            r.Set(arguments);
            return r;
        } else {
            return new Size(arguments);
        }
    }
    Size.prototype = {
        width: 0,
        height: 0,
        GetSize: function (s) {
            this.width = s.width;
            this.height = s.height;
            return this;
        },
        Set: function () {
            if (isArguments(arguments[0])) {
                arguments = arguments[0];
            }
            switch (arguments.length) {
                case 0:
                    this.width = 0; this.height = 0;
                    break;
                case 1:
                    if (arguments[0] instanceof Tools.Rect || arguments[0] instanceof Tools.Size
                        || ("width" in arguments[0] && "height" in arguments[0])) {
                        this.height = arguments[0].height;
                        this.width = arguments[0].width;
                    } else {
                        this.height = this.width = arguments[0];
                    }

                    break;
                case 2:
                    if (arguments[0] instanceof Tools.Point) {
                        this.width = arguments[1].x - arguments[0].x;
                        this.height = arguments[1].y - arguments[0].y;
                    } else {
                        this.width = arguments[0] || 0;
                        this.height = arguments[1] || 0;
                    }
                    break;
            }
            return this;
        },
        Clone: function () {
            return Tools.Size.Get(this);
        },
        WtoH: function () { return this.width / this.height },
        Scale: function (x, y) {
            if (arguments.length == 1) {
                y = x;
            }
            this.width *= x;
            this.height *= y;
            return this;
        },
        ZoomedCss: function () {

            return { width: this.width * zoom + "px", height: this.height * zoom + "px" }
        },
        Diagnal: function () { return Math.sqrt(this.height * this.height + this.width * this.width); },
        Rotate: function () {
            var h = this.width;
            this.width = this.height;
            this.height = h;
            return this;
        }
    }
    Tools.Size = Size;

    //class Rect////////////////////////////////////////////////////////////
    function Rect(x, y, w, h) {
        if (isArguments(arguments[0])) {
            arguments = arguments[0];
        }
        this.Set(arguments);
        Rect.count++;
    }
    Rect.count = 0;
    Rect._freeCache = []
    Rect.Release = function (rect) {
        if (rect instanceof Tools.Rect) {
            ClearIndivisualProps(rect);
            Rect._freeCache.push(rect);
        }
    }
    Rect.Get = function () {
        if (isArguments(arguments[0])) {
            arguments = arguments[0];
        }
        if (Rect._freeCache.length > 0) {
            var r = Rect._freeCache.pop();
            r.Set(arguments);
            return r;
        } else {
            return new Rect(arguments);
        }
    }
    Rect.prototype = {
        ID: "",
        Type: "Rect",
        toString: function () {
            return "[" + this.Type + " " + this.ID + "]";
        },
        x: 0,
        y: 0,
        width: 0,
        height: 0,
        Enabled: true,
        Disable: function () {
            this.Enabled = false;
        },
        Enable: function () {
            this.Enabled = true;
        },
        Point: function () {
            return Tools.Point.Get(this.x, this.y);
        },
        Size: function () {
            return Tools.Size.Get(this.width, this.height);
        },
        Matrix: null,
        //(x,y)<=>(w,h)
        ValueReverse: function () {
            var x = this.width, y = this.height;
            this.width = this.x; this.height = this.y;
            this.x = x; this.y = y;
        },
        GetRect: function (r) {
            this.x = r.x; this.y = r.y; this.width = r.width; this.height = r.height;
            return this;
        },
        Set: function () {
            var x = 0, y = 0, w = 0, h = 0;
            if (arguments.length == 0) {
                x = 0, y = 0, w = 0, h = 0;
            }
            if (isArguments(arguments[0]) || arguments[0] instanceof Array) {
                arguments = arguments[0];
            }
            var r, p = [], s, num = [];
            for (var i = 0; i < arguments.length; i++) {
                if ((arguments[i] instanceof Tools.Rect) || (arguments[i] instanceof Tools.DisplayObj)) {
                    r = arguments[i];
                } else if (arguments[i] instanceof Tools.Point) {
                    p.push(arguments[i]);
                } else if (arguments[i] instanceof Tools.Size) {
                    s = arguments[i];
                } else {
                    num.push(arguments[i]);
                }
            }

            if (num.length == 4) {
                x = num[0]; y = num[1]; w = num[2]; h = num[3];
            } else if (r) {
                x = r.x || 0; y = r.y || 0; w = r.width; h = r.height;
                if (s) {
                    w = s.width; h = s.height;
                }
                if (p.length == 1) {
                    x = p[0].x; y = p[0].y;
                }
            } else if (p.length || s) {
                if (p.length == 2) {
                    x = p[0].x; y = p[0].y;
                    w = p[1].x - p[0].x;
                    h = p[1].y - p[0].y;
                } else if (p.length == 1) {
                    x = p[0].x; y = p[0].y;
                    if (s) {
                        w = s.width; h = s.height;
                    } else {
                        w = num[0]; h = num[1];
                    }
                } else {
                    if (s) {
                        w = s.width; h = s.height;
                        x = num[0]; y = num[1];
                    }
                }
            } else {
                if (num.length == 2) {
                    x = 0; y = 0;
                    if (s) {
                        w = s.width; h = s.height;
                    } else {
                        w = num[0]; h = num[1];
                    }
                }
            }
            delete r, p, s, num;
            this.x = x; this.y = y; this.width = w; this.height = h;
            if (!this.Matrix) {
                this.Matrix = new Tools.Matrix2D();
            } else {
                this.Matrix.SetMatrix(Tools.Matrix2D.Identity);
            }
            this.Matrix.translate(this.x, this.y);
            return this;
        },
        SetSize: function () {
            Tools.Size.prototype.Set.apply(this, arguments);
            return this;
        },
        SetPoint: function () {
            Tools.Point.prototype.Set.apply(this, arguments);
            return this;
        },
        Append: function (opt) {
            return DeepCopyProps.apply(this, arguments);
        },
        updateContext: function (ctx, accuMatrix) {
            accuMatrix.appendMatrix(this.Matrix);
            ctx.SetTransform(accuMatrix);
        },
        restoreContext: function (ctx, accuMatrix) {
            accuMatrix.appendMatrix(this.Matrix.invert());
            ctx.SetTransform(accuMatrix);
        },
        Clone: function () {
            return Rect.Get(this);
        },
        /*tool 依據size決定x,y方向變量，移動Rect*/
        ShiftRect: function () {
            var w, h, m;
            if (typeof (arguments[0]) != "number") {//size multi
                m = arguments[1] || 1;
                w = arguments[0].width;
                h = arguments[0].height;
            } else {//w h multi
                m = arguments[2] || 1;
                w = arguments[0];
                h = arguments[1];
            }
            this.x += w * m;
            this.y += h * m;
            return this;
        },
        InRect: function (p) {
            if (this.Enabled)
                return Tools.InRect(p, this);
            else
                return false;
        },
        Anchor: function (position) {
            if (position in this)
                return this[position];
        },
        CenterTop: function (p) {
            if (p)
                return p.Set(this.x + this.width / 2, this.y);
            return Tools.Point.Get(this.x + this.width / 2, this.y);
        },
        CenterBottom: function (p) {
            if (p)
                return p.Set(this.x + this.width / 2, this.y + this.height);
            return Tools.Point.Get(this.x + this.width / 2, this.y + this.height);
        },
        LeftMiddle: function (p) {
            if (p)
                return p.Set(this.x, this.y + this.height / 2);
            return Tools.Point.Get(this.x, this.y + this.height / 2);
        },
        RightMiddle: function (p) {
            if (p)
                return p.Set(this.x + this.width, this.y + this.height / 2);
            return Tools.Point.Get(this.x + this.width, this.y + this.height / 2);
        },
        LeftTop: function (p) {
            if (p)
                return p.Set(this.x, this.y);
            return Tools.Point.Get(this.x, this.y);
        },
        LeftBottom: function (p) {
            if (p)
                return p.Set(this.x, this.y + this.height);

            return Tools.Point.Get(this.x, this.y + this.height);
        },
        RightTop: function (p) {
            if (p)
                return p.Set(this.x + this.width, this.y);
            return Tools.Point.Get(this.x + this.width, this.y);
        },
        RightBottom: function (p) {
            if (p)
                return p.Set(this.x + this.width, this.y + this.height);
            return Tools.Point.Get(this.x + this.width, this.y + this.height);
        },
        MidPoint: function (p) {
            if (p)
                return p.Set(this.x + this.width / 2, this.y + this.height / 2);
            if (!this._midpoint) {
                this._midpoint = Tools.Point.Get();
            }
            return this._midpoint.Set(this.x + this.width / 2, this.y + this.height / 2);
        },
        Css: function () {
            return new Tools.RCss(this)
        },
        toCss: function () {
            return new Tools.RCss(this).toCss()
        },
        ZoomedCss: function (zm) {
            var z = zoom || 1;
            if (!this._zoomedCss) {
                this._zoomedCss = new Tools.RCss();
            }
            this._zoomedCss.SetRect(this, z);
            return this._zoomedCss;
        },
        CssLocation: function () {
            return {
                'left': this.x + 'px',
                'top': this.y + 'px'
            };
        },
        CssSzie: function () {
            return {
                'width': w + 'px',
                'height': h + 'px'
            };
        },
        CentralResize: function (z) {
            var mp = this.MidPoint();
            this.x = mp.x - z * this.width / 2;
            this.y = mp.y - z * this.height / 2;
            this.width *= z;
            this.height *= z;
            return this;
        },
        Translate: function (dx, dy) {
            if (arguments.length == 0) {
                dx = this.x;
                dy = this.y;
            } else {
                dy = "y" in arguments[0] ? arguments[0].y : arguments[0].height;
                dx = "x" in arguments[0] ? arguments[0].x : arguments[0].width;
            }
            this.x -= dx;
            this.y -= dy;
            return this;

        },
        Move: function () {
            var dx, dy;
            if (arguments.length == 2) {
                dx = arguments[0];
                dy = arguments[1];
            } else {
                dx = typeof (arguments[0].x) != "undefined" ? arguments[0].x : arguments[0].width;
                dy = typeof (arguments[0].y) != "undefined" ? arguments[0].y : arguments[0].height;
            }
            this.x += dx;
            this.y += dy;
            return this;

        },
        Resize: function (dw, dh) {
            if (arguments.length == 1) {
                dh = dw;
            }
            this.width *= dw;
            this.height *= dh;
            return this;
        },
        Transfer: function (px, py, pw, ph) {
            this.x *= px;
            this.y *= py;
            this.width *= pw;
            this.height *= ph;
            return this;

        },
        Scale: function () {
            var zx = zoom, zy = zoom;
            switch (arguments.length) {
                case 1:
                    zx = zy = arguments[0];
                    break;
                case 2:
                    zx = arguments[0];
                    zy = arguments[1];
            }
            this.x *= zx;
            this.y *= zy;
            this.width *= zx;
            this.height *= zy;
            return this;
        },
        CentralIn: function (rectFrame) {
            this.x = rectFrame.x + (rectFrame.width - this.width) / 2;
            this.y = rectFrame.y + (rectFrame.height - this.height) / 2;
            return this;
        },
        UpperIn: function (rectFrame, part) {
            this.x = rectFrame.x + (rectFrame.width - this.width) / 2;
            this.y = rectFrame.height * part;
            return this;
        },
        LowerIn: function (rectFrame, part) {
            this.x = rectFrame.x + (rectFrame.width - this.width) / 2;
            this.y = rectFrame.height * part - this.height;
            return this;
        },
        Rotate: function () {
            var h = this.width;
            this.width = this.height;
            this.height = h;
            return this;
        },
        AddEventHandler: function (type, func, bubble) {
            if (!this.hasOwnProperty("_eventHandler")) {
                this._eventHandler = {};
            }
            this._capture = !bubble;
            this._eventHandler[type] = function (self) {
                return function (target, evt) {
                    return TestUndefined(func.apply(self, arguments), true);
                }
            }(this);
            return this;
        },
        HasEvent: function (type) {
            if (this._eventHandler)
                if (type)
                    if (type instanceof Array) {
                        for (var i = 0; i < type.length; i++)
                            if (type[i] in this._eventHandler)
                                return true;
                        return false;
                    } else
                        return this._eventHandler && this._eventHandler[type] ? true : false;
                else return true;
            else return false;
        },
        TriggerEvent: function (type, target, evt) {
            if (this._eventHandler && this._eventHandler[type]) {
                var result = this._eventHandler[type](target, evt);
                Tools.RecordLog("fire event : " + type, "Gesture", Tools.DebugType.Gesture);
                return typeof (result) == "undefined" ? true : result;
            }
            return false;
        },
        _eventHandler: null,
        Dispose: function () {
            if (this._disposed) return;
            ClearIndivisualProps(this);
            this._disposed = true;
        }
    }
    Object.defineProperties(Rect.prototype, {
        x: {
            get: function () {
                return this.percentageBase ? ParsePercentage(this._x, this.percentageBase.width) : this._x;
            },
            set: function (val) {
                this._x = val;
                this.needUpdate = true;
            }
        },
        y: {
            get: function () {
                return this.percentageBase ? ParsePercentage(this._y, this.percentageBase.height) : this._y;
            },
            set: function (val) {
                this._y = val;
                this.needUpdate = true;
            }
        },
        GlobalLeft: {
            get: function () {
                if (this.parent) {
                    return (this.parent.GlobalLeft || 0) + this.x + this.offsetX;
                }
                else return this.x;
            }
        },
        GlobalTop: {
            get: function () {
                if (this.parent) {
                    return (this.parent.GlobalTop || 0) + this.y + this.offsetY;
                }
                else return this.y;
            }
        },
        width: {
            get: function () {
                return this.percentageBase ? ParsePercentage(this._width, this.percentageBase.width) : this._width;
            },
            set: function (val) {
                this._width = val;
                this.needUpdate = true;
            }
        },
        height: {
            get: function () {
                return this.percentageBase ? ParsePercentage(this._height, this.percentageBase.height) : this._height;
            },
            set: function (val) {
                this._height = val;
                this.needUpdate = true;
            }
        },
        parent: {
            get: function () {
                return this._parent;
            },
            set: function (val) {
                if (this._parent && this._parent.remove)
                    this._parent.remove(this);
                this._needUpdate = true;
                this._parent = val;
                this.percentageBase = this._parent;
            }
        },
        stage: {
            get: function () { return this._stage; },
            set: function (val) { this._stage = val; }
        },
        needUpdate: {
            get: function () { return this._needUpdate; },
            set: function (val) {
                this._needUpdate = val ? true : false;
                if (val && this.parent)
                    this.parent.needUpdate = val;
            }
        },

        EventCaptured: {
            get: function () { return this._capture; },
            set: function (val) { this._capture = val; }
        },
        onClick: {
            get: function () { return this._eventHandler ? this._eventHandler[Tools.Gesture.Click] : null; },
            set: function (func) { this.AddEventHandler(Tools.Gesture.Click, func); }
        },
        onDown: {
            get: function () { return this._eventHandler ? this._eventHandler[Tools.Gesture.Down] : null; },
            set: function (func) { this.AddEventHandler(Tools.Gesture.Down, func); }
        },
        onRelease: {
            get: function () { return this._eventHandler ? this._eventHandler[Tools.Gesture.Release] : null; },
            set: function (func) { this.AddEventHandler(Tools.Gesture.Release, func); }
        },
        onDrag: {
            get: function () { return this._eventHandler ? this._eventHandler[Tools.Gesture.Drag] : null; },
            set: function (func) { this.AddEventHandler(Tools.Gesture.Drag, func); }
        },
        onEnter: {
            get: function () { return this._eventHandler ? this._eventHandler[Tools.Gesture.Enter] : null; },
            set: function (func) { this.AddEventHandler(Tools.Gesture.Enter, func); }
        },
        onHover: {
            get: function () { return this._eventHandler ? this._eventHandler[Tools.Gesture.Hover] : null; },
            set: function (func) { this.AddEventHandler(Tools.Gesture.Hover, func); }
        },
        onLeave: {
            get: function () { return this._eventHandler ? this._eventHandler[Tools.Gesture.Leave] : null; },
            set: function (func) { this.AddEventHandler(Tools.Gesture.Leave, func); }
        },
    })
    Tools.Rect = Rect;


    function MidPoint(rect) {
        var r, s, p = [], num = [];
        for (var i in arguments) {
            if (arguments[i] instanceof Tools.Rect) {
                r = arguments[i];
                break;
            } else if (arguments[i] instanceof Tools.Size) {
                s = arguments[i];
            } else if (arguments[i] instanceof Tools.Point) {
                p.push(arguments[i]);
            } else
                num.push(arguments[i]);
        }
        var x, y, w, h;
        if (num.length == 4) {
            x = num[0]; y = num[1]; w = num[2]; h = num[3];
        } else if (r) {
            x = r.x; y = r.y; w = r.width; h = r.height;
        } else {
            x = p[0].x; y = p[0].y;
            if (p.length == 2) {
                w = p[1].x - p[0].x;
                h = p[1].y - p[0].y;
            } else {
                w = s.width; h = s.height;
            }
        }
        delete r, s, p, num;
        this.x = x + w / 2; this.y = y + h / 2;
    }
    var midp = ClassExtend(MidPoint, Tools.Point);
    Tools.MidPoint = MidPoint;
    //class RCss
    function RCss(x, y, width, height) {
        var opt = {};
        if (arguments[arguments.length - 1] instanceof Object) {
            opt = arguments[arguments.length - 1];
        }
        this.unit = opt.unit || 'px';
        this.left = { value: x || 0, unit: this.unit };
        this.top = { value: y || 0, unit: this.unit };
        this.width = { value: width || 0, unit: this.unit };
        this.height = { value: height || 0, unit: this.unit };
    }
    RCss.prototype = {
        unit: 'px',
        left: {
            value: 0, unit: 'px'
        },
        top: {
            value: 0, unit: 'px'
        },
        width: {
            value: 0, unit: 'px'
        },
        height: {
            value: 0, unit: 'px'
        },
        Move: function (dx, dy) {
            if (arguments.length == 1) {
                this.left.value += TestUndefined(arguments[0].x, arguments[0].width);
                this.top.value += TestUndefined(arguments[0].y, arguments[0].height);
            } else {
                this.left.value += dx || 0;
                this.top.value += dy || 0;
            }
            return this;
        },
        SetRect: function (r, z) {
            this.left.value = r.x * z;
            this.top.value = r.y * z;
            this.width.value = r.width * z;
            this.height.value = r.height * z;
            return this;
        },
        Set: function (prop, value) {
            var opt = {};
            if (typeof prop == 'string') { opt[prop] = value; }
            for (var p in opt) {
                if (typeof opt[p] != 'string') { continue; }
                var ind = opt[p].search(/\D/);
                if (ind > 0) {//width : 123 px
                    this[p] = { value: opt[p].slice(0, ind), unit: opt[p].slice(ind, opt[p].length) };
                } else if (ind == 0) {//position : absolute
                    this[p] = { value: opt[p], unit: '' };
                } else {//z-index : 12
                    this[p] = { value: opt[p], unit: '' };
                }
            }
            return this;
        },
        toCss: function () {
            var style = {};
            for (var prop in this) {
                if (typeof this[prop] != 'function') {
                    if (this[prop] instanceof Object) {
                        style[prop] = this[prop].value + this[prop].unit;
                    } else {
                        style[prop] = this[prop] + this[prop].unit;
                    }
                }
            }
            return style;
        }
    };
    Tools.RCss = RCss;

    /*class displayobj*/
    function DisplayObj() {
        this.SetDisplayObj(arguments);
    }

    ClassExtend(DisplayObj, Tools.Rect, {
        Type: "DisplayObj",
        Display: true,
        Hide: function (ignoreDisable) {
            this.Display = false;
            !ignoreDisable && (this.Disable());
        },
        Show: function (ignoreEnable) {
            this.Display = true;
            !ignoreEnable && (this.Enable());
        },
        SetDisplayObj: function () {
            if (isArguments(arguments[0]) || arguments[0] instanceof Array) {
                arguments = arguments[0];
            }
            var imgID, r, s, p, num = [];
            for (var i = 0; i < arguments.length; i++) {
                if (typeof (arguments[i]) == "string") {
                    imgID = arguments[i];
                } else if (arguments[i] instanceof Tools.Rect) {
                    r = arguments[i];
                } else if (arguments[i] instanceof Tools.Size) {
                    s = arguments[i];
                } else if (arguments[i] instanceof Tools.Point) {
                    p = arguments[i];
                } else {
                    num.push(arguments[i]);
                }
            }
            if (num.length == 2) {
                num.unshift(0, 0);
            }
            if (r && (s || p)) {
                this.Set(r, s || p);
            } else {
                this.Set(r || s || p || num);
            }

            if (imgID)
                if (imgID.indexOf(".") > -1)
                    this.Image = [new ImgO(imgID)];
                else
                    this.Image = [imgID];
            delete num;
            this.imgNo = 0;
        },
        Image: null,
        SetImages: function () {
            this.Image = [];
            var src = [];
            if (arguments[0] instanceof Array) {
                src = arguments[0];
            } else if (arguments.length > 1) {
                src = arguments;
            } else {
                src[0] = arguments[0];
            }
            for (var i = 0; i < src.length; i++) {
                this.Image[i] = src[i].indexOf(".") > -1 ? new ImgO(src[i]) : src[i];
            }
            delete src;
            return this;
        },
        Update: function (ctx) {
            if (!this.Display) { return; }
            ctx.save();
            ctx.DrawImg(this, this.x, this.y);
            ctx.restore();
        }
    });
    Tools.DisplayObj = DisplayObj;


    /*class matrix  
        modified from createjs.Matrix2D.js*/
    function Matrix2D(a, b, c, d, x, y) {
        this.Set(a, b, c, d, x, y);
        Matrix2D.count++;
    }
    Matrix2D.count = 0;
    Matrix2D.DegToRad = Math.PI / 180;
    Matrix2D.prototype = {
        SetMatrix: function (matx) {
            return this.Set(matx.a, matx.b, matx.c, matx.d, matx.x, matx.y);
        },
        Set: function (a, b, c, d, x, y) {
            if (arguments[0] instanceof Matrix2D)
                return this.SetMatrix(arguments[0]);
            this.a = (a == null) ? 1 : a;
            this.b = b || 0;
            this.c = c || 0;
            this.d = (d == null) ? 1 : d;
            this.x = x || 0;
            this.y = y || 0;
            return this;
        },
        Clone: function () {
            return new Matrix2D(this);
        },
        setScale: function (x, y) {
            if (arguments.length == 1)
                y = x;
            this.b *= x / this.a;
            this.a = x;
            this.c *= y / this.d;
            this.d = y;
        },
        scale: function (x, y) {
            if (arguments.length == 1)
                y = x;
            this.a *= x;
            this.b *= x;
            this.c *= y;
            this.d *= y;
            return this;
        },
        translate: function (x, y) {
            this.x += this.a * x + this.c * y;
            this.y += this.b * x + this.d * y;
            return this;
        },
        rotate: function (angle) {
            angle = angle * Matrix2D.DEG_TO_RAD;
            var cos = Math.cos(angle);
            var sin = Math.sin(angle);

            var a1 = this.a;
            var b1 = this.b;

            this.a = a1 * cos + this.c * sin;
            this.b = b1 * cos + this.d * sin;
            this.c = -a1 * sin + this.c * cos;
            this.d = -b1 * sin + this.d * cos;
            return this;
        },
        centralRotate: function (ctx, angle) {
            this.translate(ctx.width / 2, ctx.height / 2);
            this.rotate(angle);
            this.translate(-ctx.width / 2, -ctx.height / 2);
        },
        transformPoint: function () {
            var pt = Tools.Point.Get(arguments);
            var x = pt.x, y = pt.y;
            pt.x = x * this.a + y * this.c + this.x;
            pt.y = x * this.b + y * this.d + this.y;
            return pt;
        },
        appendMatrix: function (matx) {
            return this.append(matx.a, matx.b, matx.c, matx.d, matx.x, matx.y);
        },
        /*|a1 c1||a c||x|
          |b1 d1||b d||y|*/
        append: function (a, b, c, d, tx, ty) {
            if (arguments[0] instanceof Matrix2D)
                return this.appendMatrix(arguments[0])

            var a1 = this.a;
            var b1 = this.b;
            var c1 = this.c;
            var d1 = this.d;
            if (a != 1 || b != 0 || c != 0 || d != 1) {
                this.a = a1 * a + c1 * b;
                this.b = b1 * a + d1 * b;
                this.c = a1 * c + c1 * d;
                this.d = b1 * c + d1 * d;
            }
            this.x = a1 * tx + c1 * ty + this.x;
            this.y = b1 * tx + d1 * ty + this.y;
            return this;
        },
        prependMatrix: function (matx) {
            return this.prepend(matx.a, matx.b, matx.c, matx.d, matx.x, matx.y);
        },
        /*|a c||x||a1 c1|
          |b d||y||b1 d1|*/
        prepend: function (a, b, c, d, tx, ty) {
            if (arguments[0] instanceof Matrix2D)
                return this.prependMatrix(arguments[0]);

            var a1 = this.a;
            var c1 = this.c;
            var tx1 = this.tx;
            this.a = a * a1 + c * this.b;
            this.b = b * a1 + d * this.b;
            this.c = a * c1 + c * this.d;
            this.d = b * c1 + d * this.d;
            this.x = a * tx1 + c * this.y + tx;
            this.y = b * tx1 + d * this.y + ty;
            return this;
        },
        appendTransform: function (x, y, scaleX, scaleY, rotation, skewX, skewY, regX, regY) {
            var cos = 1, sin = 0;
            if (rotation % 360) {
                var r = rotation * Matrix2D.DegToRad;
                cos = Math.cos(r);
                sin = Math.sin(r);
            }

            if (skewX || skewY) {
                skewX *= Matrix2D.DEG_TO_RAD;
                skewY *= Matrix2D.DEG_TO_RAD;
                this.append(Math.cos(skewY), Math.sin(skewY), -Math.sin(skewX), Math.cos(skewX), x, y);
                this.append(cos * scaleX, sin * scaleX, -sin * scaleY, cos * scaleY, 0, 0);
            } else {
                this.append(cos * scaleX, sin * scaleX, -sin * scaleY, cos * scaleY, x, y);
            }

            if (regX || regY) {
                // append the registration offset:
                this.x -= regX * this.a + regY * this.c;
                this.y -= regX * this.b + regY * this.d;
            }
            return this;
        },
        prependTransform: function (x, y, scaleX, scaleY, rotation, skewX, skewY, regX, regY) {
            var cos = 1, sin = 0;
            if (rotation % 360) {
                var r = rotation * Matrix2D.DEG_TO_RAD;
                cos = Math.cos(r);
                sin = Math.sin(r);
            }

            if (regX || regY) {
                this.x -= regX; this.y -= regY;
            }
            if (skewX || skewY) {
                skewX *= Matrix2D.DEG_TO_RAD;
                skewY *= Matrix2D.DEG_TO_RAD;
                this.prepend(cos * scaleX, sin * scaleX, -sin * scaleY, cos * scaleY, 0, 0);
                this.prepend(Math.cos(skewY), Math.sin(skewY), -Math.sin(skewX), Math.cos(skewX), x, y);
            } else {
                this.prepend(cos * scaleX, sin * scaleX, -sin * scaleY, cos * scaleY, x, y);
            }
            return this;
        },
        inverted: null,
        invert: function () {
            var a1 = this.a;
            var b1 = this.b;
            var c1 = this.c;
            var d1 = this.d;
            var tx1 = this.x;
            var n = a1 * d1 - b1 * c1;

            if (!this.inverted) {
                this.inverted = new Matrix2D();
            }
            this.inverted.Set(d1 / n, -b1 / n, -c1 / n, a1 / n, (c1 * this.y - d1 * tx1) / n, -(a1 * this.y - b1 * tx1) / n);
            //this.a = d1 / n;
            //this.b = -b1 / n;
            //this.c = -c1 / n;
            //this.d = a1 / n;
            //this.x = (c1 * this.y - d1 * tx1) / n;
            //this.y = -(a1 * this.y - b1 * tx1) / n;
            return this.inverted;
        }
    }
    Matrix2D.Identity = new Matrix2D();
    Matrix2D.IdentityInvert = new Matrix2D().SetMatrix(Matrix2D.Identity).invert();
    Tools.Matrix2D = Matrix2D;
    var ctxProto = CanvasRenderingContext2D.prototype;
    Tools.CacheCanvas = {
        Cache: function () {
            var cc, ctx, rect, func;
            if (arguments[0] instanceof HTMLCanvasElement) {
                cc = arguments[0];
                rect = arguments[1];
                func = arguments[2];
            } else {
                cc = document.createElement('canvas');
                rect = arguments[0];
                func = arguments[1];
            }
            cc.width = rect.width;
            cc.height = rect.height;
            ctx = cc.getContext('2d');
            var tmpZ = zoom;
            zoom = 1;
            ctx.translate(rect.x, rect.y);
            func(ctx);
            zoom = tmpZ;
            return cc;
        },
        CombineDraw: function (size, funcArr) {
            var l = arguments.length;
            if (l == 2) {
                funcArr = [arguments[1]];
            }

            var cc = document.createElement('canvas');

            cc.width = size.width; cc.height = size.height;
            var cctx = cc.getContext('2d');
            //var cctx = SetHDPICanvas(cc, size.width, size.height);
            var tmpZ = zoom;
            zoom = 1;
            for (var i = 0; i < funcArr.length; i++) {
                funcArr[i](cctx);
            }
            zoom = tmpZ;
            return cc;
        },
        Combine: function (size, displayArr) {
            var cc = document.createElement('canvas');
            cc.width = size.width; cc.height = size.height;
            var cctx = cc.getContext('2d');
            //var cctx = SetHDPICanvas(cc, size.width, size.height);
            for (var i = 0; i < displayArr.length; i++) {
                cctx.DrawImage(displayArr[i].Image, displayArr[i].Rect);
            }
            return cc;
        },
        CombineImage: function (img, rect) {
            //calculate combine size
            var rects = [], imgs = [], testw = [], testh = [];
            var w, h;
            if (arguments[0] instanceof Size || arguments[0] instanceof Rect) {//set size
                w = arguments[0].width; h = arguments[0].height;
            }
            for (var i = (w ? 1 : 0) ; i < arguments.length; i += 2) {
                imgs.push(Tools.GetImage(arguments[i]));
                rects.push(arguments[i + 1]);
                testw.push(arguments[i + 1].RightBottom().x);
                testh.push(arguments[i + 1].RightBottom().y);
            }
            !w && (w = Math.max.apply(this, testw));
            !h && (h = Math.max.apply(this, testh));
            var cc = document.createElement('canvas');
            cc.width = w; cc.height = h;
            var cctx = cc.getContext('2d');
            //var cctx = SetHDPICanvas(cc, w, h);
            for (var i = 0; i < rects.length; i++) {
                cctx.DrawImage(imgs[i], rects[i]);
            }
            rects = imgs = testw = testh = null;

            return cc;
        },
        RotateImage: function (image, angle) {
            image = Tools.GetImage(image);
            var cc = document.createElement('canvas');
            angle = angle || 0;
            angle < 0 && (angle += 360);
            angle %= 360;
            var quadrant = angle >= 270 ? 1 : angle >= 180 ? 2 : angle >= 90 ? 3 : 4;
            var ang = angle / 180 * Math.PI;
            var w, h, vX = 0, vY = 0, cctx;
            switch (quadrant) {
                case 1: case 3:
                    quadrant == 1 ? (vX = -1) : (vY = -1);
                    w = image.height; h = image.width; break;
                case 2: case 4:
                    quadrant == 2 && (vY = vX = -1);
                    w = image.width; h = image.height; break;
                    break;
            }
            cc.width = w;
            cc.height = h;
            var cctx = cc.getContext('2d');
            //var cctx = SetHDPICanvas(cc, w, h);
            cctx.SetTransform(Tools.Matrix2D.Identity);
            cctx.rotate(ang);
            cctx.translate(image.width * vX, image.height * vY);
            cctx.DrawImage(image, 0, 0);
            cctx.SetTransform(Tools.Matrix2D.Identity);
            return cc;
        }
    }
    ctxProto.DrawImage = function () {
        var _z = zoom;
        zoom = 1;
        this.DrawImg.apply(this, arguments);
        zoom = _z;
        return this;
    }
    ctxProto.Scope = function (target, func) {
        var ctx = this;
        ctx.save();
        if (func instanceof Function) {
            func.call(target, ctx);
        }
        ctx.restore();
    }
    var halfChar = { Default: 1, x: 0.7, ",": 0.5, ".": 0.5 };
    ctxProto.NumString = function (img, arr, numstr, align, rect, noclear, opt) {
        var configs = opt ? TestUndefined(opt.Widths, halfChar) : halfChar;
        var x = rect.x, y = rect.y, w = rect.width, h = rect.height;
        noclear = noclear || 0;
        var cw = w, ch = h;
        //try convert to str
        var str;
        if (typeof (numstr) != 'String') { str = numstr.toString(); }
        else if (typeof (numstr) == 'string') { str = numstr; }

        //set num array
        var nums = [], pos = [], length = 0;
        for (var i = 0; i < str.length; i++) {
            var ind = arr.indexOf(str[i]);
            if (ind != -1) {
                nums.push(ind);
                pos.push(length);

                var dif = TestUndefined(configs[str[i]], configs.Default || 1);
                length += (i == str.length - 1) ? 1 : dif;
            }
        }

        //setImg
        var vw, vh, IsArray = img instanceof Array;
        if (IsArray) {
            vw = img[0].width; vh = img[0].height;
        } else {
            vw = img.width; vh = img.height / arr.length;
        }

        //set width
        var width = vw, height = vh;
        var ratio = vw / vh;
        if (!w || !h) {
            h = vh * zoom
            ; w = vw * zoom * length;
        } else {
            var rectRatio = length * vw / vh;
            if (rectRatio > w / h) {
                width = w / length;
                height = width * vh / vw;
            } else {
                height = h;
                width = h * vw / vh;
            }
        }

        //set align

        if (!align) align = "center";
        switch (align.toLocaleLowerCase()) {
            case "center": default:
                x += cw / 2 - width * length / 2;
                break;
            case "start":
                break;
            case "end":
                x += cw - length * width;
                break;
        }

        //Clear
        if (!noclear) {
            this.Clear(rect);
        }
        for (var i = 0; i < nums.length; i++) {
            if (IsArray) {
                this.DrawImg(img[nums[i]], pos[i] * width, (h - height) / 2, width, height);
            } else {
                this.DrawImg(img, 0, nums[i] * vh, vw, vh, x + pos[i] * width, y + (h - height) / 2, width, height);
            }
        }
        nums = pos = null;
    }
    /*畫數字(zoom,img,str,align,str)*///
    /*align: center,start,end*/
    ctxProto.NumberR = function (img, numstr, align, rect, noclear, opt) {
        this.Number(img, numstr, rect.x, rect.y, align, rect.width, rect.height, noclear || 2, opt);
    }

    /*畫數字(數字字串,x,y,w,h)*///
    ctxProto.Number = function (img, numstr, x, y, align, w, h, noclear, opt) {
        var HCheckFirst = true;
        if (typeof (opt) == "object") {
            HCheckFirst = opt.FixH || !opt.FixW;
        }
        noclear = noclear || 0;
        var cw = w, ch = h;
        //try convert to str
        var str;
        if (typeof (numstr) != 'String') { str = numstr.toString(); }
        else if (typeof (numstr) == 'string') { str = numstr; }
        //set num array
        var nums = [];
        for (var i = 0; i < str.length; i++) { nums[i] = Number(str[i]); }


        //setImg

        var vw, vh, IsArray = img instanceof Array;
        if (IsArray) {
            vw = img[0].width; vh = img[0].height;
        } else {
            vw = img.width; vh = img.height / 10;
        }

        //set width
        var width = vw, height = vh;
        if (!w || !h) {
            h = vh; w = vw * str.length;
        } else {
            if (HCheckFirst) {
                if (h < vh) {
                    height = h;
                    width = vw * height / vh;
                } else if (str.length * vw > w) {
                    width = w / str.length;
                    height = vh * width / vw;
                    y += (vh - h) / 2;
                }
            } else {
                if (str.length * vw > w) {
                    width = w / str.length;
                    height = vh * width / vw;
                    y += (vh - h) / 2;
                }
                else if (h < vh) {
                    height = h;
                    width = vw * height / vh;
                }
            }

        }

        //set align

        if (!align) align = "center";
        switch (align.toLocaleLowerCase()) {
            case "center": default:
                x += cw / 2 - width * str.length / 2;
                break;
            case "start":
                break;
            case "end":
                x -= str.length * width;
                break;
        }

        if (!noclear) {
            this.Clear(x, y, width * str.length, height);
        } else if (noclear == 2) {
            this.Clear(x, y, w, h);
        }
        //Clear

        for (var i = 0; i < str.length; i++) {
            if (IsArray) {
                this.DrawImg(img[nums[i]], x + i * width, y, width, height);
            } else {
                this.DrawImg(img, 0, nums[i] * vh, vw, vh, x + i * width, y, width, height);
            }
        }
    }
    /*渲染靜態物件*/
    ctxProto.DrawImg = function (Obj) {
        var img, dr, rr = [], p, s, num = [], sx, sy, sw, sh, dx, dy, dw, dh, imgNo;
        for (var i = 0; i < arguments.length; i++) {

            if (arguments[i] instanceof Tools.DisplayObj) {
                if (!img) {
                    img = arguments[i].Image;
                    imgNo = arguments[i].imgNo;
                }
                dr = arguments[i];
            } else if (arguments[i] instanceof Image || arguments[i] instanceof HTMLCanvasElement) {
                img = arguments[i];
            } else if (arguments[i] instanceof Tools.Rect) {
                rr.push(arguments[i]);
            } else if (arguments[i] instanceof Tools.Point) {
                p = arguments[i];
            } else if (arguments[i] instanceof Tools.Size) {
                s = arguments[i];
            } else if (typeof (arguments[i]) == "string")
                img = arguments[i];
            else
                num.push(arguments[i]);
        }
        if (num.length % 2) {
            imgNo = num.pop();
        }

        if (dr) {
            dx = dr.x; dy = dr.y;
            dw = dr.width; dh = dr.height;
            if (num.length == 4) {
                dx = num[0]; dy = num[1]; dw = num[2]; dh = num[3];
            } else if (num.length == 2) {
                dx = num[0]; dy = num[1];
            }
        }
        if (rr.length == 2) {
            sx = rr[0].x; sy = rr[0].y; sw = rr[0].width; sh = rr[0].height;
            dx = rr[1].x; dy = rr[1].y; dw = rr[1].width; dh = rr[1].height;
        } else if (rr.length == 1) {
            dx = rr[0].x; dy = rr[0].y; dw = rr[0].width; dh = rr[0].height;
        }
        if (p) {
            dx = p.x; dy = p.y;
            if (!dw || !dh) {
                if (s) {
                    dw = s.width; dh = s.height;
                } else {
                    dw = num[0]; dh = num[1];
                }
            }
        }
        if (s) {
            dw = s.width; dh = s.height;
            if (!dx || !dy) {
                if (p) {
                    dx = p.x; dy = p.y;
                } else {
                    dx = num[0]; dy = num[1];
                }
            }
        }
        if (num.length == 8) {
            sx = num[0]; sy = num[1]; sw = num[2]; sh = num[3];
            dx = num[4]; dy = num[5]; dw = num[6]; dh = num[7];
        }
        if ((!rr.length && !dr && !p && !s)) {
            if (num.length == 2) {
                dx = num[0]; dy = num[1]; dw = img.width; dh = img.height;
            } else if (num.length == 4) {
                dx = num[0]; dy = num[1]; dw = num[2]; dh = num[3];
            }
        }
        if (img instanceof Array)
            img = img[(imgNo || (dr && dr.imgNo) || 0)];

        //dx *= z; dy *= z; dw *= z; dh *= z;

        img = Tools.GetImage(img);
        if (Tools.isImageElement(img)) {
            if (!Tools.Configs.enableTransform) {
                dw = dw || img.width;
                dh = dh || img.height;
                var ratio = (sw && sh) ?
                    (sh / sw) : (img.height / img.width);
                if (dh / dw > ratio)
                    dh = dw * ratio;
                else
                    dw = dh / ratio;
            }
            if (sw && sh) {
                try {
                    this.drawImage(img, sx, sy, sw, sh, dx, dy, dw, dh);
                } catch (e) {
                    Tools.RecordLog(e.message, true);
                }

            } else {
                try {
                    this.drawImage(img, dx || 0, dy || 0, dw, dh);
                } catch (e) {
                    Tools.RecordLog(e.message, true);
                }
            }
        } else {
            Tools.RecordLog("fail to draw " + img, true);
        }
        delete dr, rr, p, s, num;
        return this;
    }
    ctxProto.DrawCacheObj = function (cacheobj) {
        this.drawImage(cacheobj.image, cacheobj.x, cacheobj.y, cacheobj.width, cacheobj.height);

    }
    ctxProto.Scale = function (x, y) {
        if (arguments.length == 1) {
            y = x;
        }
        this.scale(x, y);
        this.scaleX = x; this.scaleY = y;
        return this;

    }
    ctxProto.Translate = function () {
        var x = arguments[0], y = arguments[1];
        if (arguments.length == 1) {
            if ("x" in arguments[0]) {
                x = arguments[0].x;
                y = arguments[0].y;
            } else {
                y = x;
            }
        }
        this.translate(this.scaleX * x, this.scaleY * y);
        this.offsetX += x;
        this.offsetY += y;
        return this;
    }
    ctxProto.Transform = function (matx) {
        this.transform(matx.a, matx.b, matx.c, matx.d, matx.x, matx.y);
        return this;
    }
    ctxProto.SetTransform = function (matx) {
        this.setTransform(matx.a, matx.b, matx.c, matx.d, matx.x, matx.y);
        this.scaleX = matx.a;
        this.scaleY = matx.d;
        this.offsetX = matx.x;
        this.offsetY = matx.y;
        return this;
    }
    ctxProto.SetTextConfig = function (pack) {
        CopyProps(this, pack);
    }
    ctxProto.FillStyle = function (style) {
        this.fillStyle = style;
        return this;
    }
    ctxProto.FillRect = function (rect) {
        rect = Tools.Rect.Get.apply(this, arguments);
        this.fillRect(rect.x, rect.y, rect.width, rect.height);
        Tools.Release(rect);
        return this;
    }
    ctxProto.StrokeStyle = function (style) { this.strokeStyle = style; return this; }
    ctxProto.StrokeRect = function () {
        var rr = Tools.Rect.Get(arguments);
        if (!rr.width)
            rr.ValueReverse();
        this.strokeRect(rr.x, rr.y, rr.width, rr.height);
        Tools.Rect.Release(rr);
        return this;
    };
    ctxProto.Alpha = function (a) { this.globalAlpha = a; return this; }
    ctxProto.LineWidth = function (size) {
        this.lineWidth = (size || 1) * zoom; return this;
    }
    ctxProto.Rect = function (rect) {
        rect = Tools.Rect.Get.apply(this, arguments);
        this.rect(rr.x, rr.y, rr.width, rr.height);
        Tools.Release(rect);
        return this;
    }
    ctxProto.ClearImg = function (img, rect) {
        this.save();
        this.globalCompositeOperation = "xor";
        this.DrawImg.apply(this, arguments);
        this.restore();
        return this;
    }
    //清除區塊 
    ctxProto.Clear = function (rect, size) {
        var r;
        if (arguments.length > 0) {
            r = Tools.Rect.Get(arguments);
        } else {
            r = Tools.Rect.Get(0, 0, this.canvas.width, this.canvas.height);
        }
        this.clearRect(r.x, r.y, r.width, r.height);
        Tools.Rect.Release(r);
        r = null;
        return this;
    }
    //剪切 opt: Rect || x,y,w,h
    ctxProto.Clip = function (opt) {
        var z = zoom || 1;
        var r = new Rect(0, 0, this.canvas.width, this.canvas.height);
        if (opt instanceof Rect) {
            r = opt;
        } else if (opt) {
            if (opt["Rect"] instanceof Rect) {
                r = opt["Rect"];
            }
        }
        if (r) { r.Scale(z); }
        this.save();
        this.rect(r.x, r.y, r.width, r.height);
        this.clip();
        if (typeof opt.draw == 'function') {
            opt.draw();
        }
        this.restore();
        r = null;
        return this;
    };
    ctxProto.FontSize = 12;
    //設定文字樣式
    ctxProto.Font = function (size, unit, font) {
        this.FontSize = size < 12 ? 12 : size;
        switch (arguments.length) {
            case 1:
                this.font = 'bold ' + size + 'px 微軟正黑體';
                break;
            case 2:
                if (/px|em|%/i.test(unit)) {
                    this.font = 'bold ' + size + unit + ' 微軟正黑體';
                } else {
                    this.font = 'bold ' + size + 'px ' + font;
                }
                break;
            case 3:
                this.font = 'bold ' + size + unit + ' ' + font;
                break;
        }
        return this;
    }
    //設定顏色
    ctxProto.Color = function (color) { this.fillStyle = color; return this; }
    //設定陰影
    ctxProto.ShadowBlur = function (blur, color, offsetX, offsetY) {
        var z = zoom || 1;
        var b = blur || 0, c = color || 'black', ox = offsetX || 0, oy = offsetY || 0;
        this.shadowBlur = b * z;
        this.shadowColor = c;
        this.shadowOffsetX = ox * z;
        this.shadowOffsetY = oy * z;
    }
    //依據給定的縮放比(zoom)繪製文字
    //(zoom : 縮放比(預設為1))(float)
    //text : 要繪製的文字(string)
    //point: 繪製的座標:x(num),y(num) | (Rect) | (Point)
    ctxProto.FillText = function (text, point) {
        var textlines = String(text).split('\n');
        if (arguments[1].height) {
            this.Font(arguments[1].height);
        }
        if (/bottom/i.test(this.textBaseline)) {
            for (var j = textlines.length - 1; j >= 0; j--) {
                switch (arguments.length) {
                    case 2://text+rect
                        this.fillText(
                            textlines[j],
                            arguments[1].x,
                            arguments[1].y - this.FontSize * (textlines.length - j - 1));
                        break;
                    case 3://text+x+y
                        this.fillText(
                            textlines[j],
                            arguments[1],
                            arguments[2] - this.FontSize * (textlines.length - j - 1));
                        break;
                }
            }
        } else {
            for (var j = 0; j < textlines.length; j++) {
                switch (arguments.length) {
                    case 2://text+rect
                        this.fillText(
                            textlines[j],
                            arguments[1].x,
                            arguments[1].y + this.FontSize * j);
                        break;
                    case 3://text+x+y
                        this.fillText(
                            textlines[j],
                            arguments[1],
                            arguments[2] + this.FontSize * j);
                        break;
                }
            }
        }
        return this;
    }
    ctxProto.MoveTo = function (point) {
        switch (arguments.length) {
            case 1:
                this.moveTo(point.x, point.y);
                break;
            case 2:
                this.moveTo(arguments[0], arguments[1]);
                break;
        }
        return this;
    }
    ctxProto.BeginPath = function (startPoint) {
        this.beginPath();
        startPoint && this.MoveTo.apply(this, arguments);
        return this;
    }
    ctxProto.Arc = function (p0, r, startAngle, endAngle, counterClockWise) {
        if (arguments.length == 3) {
            endAngle = arguments[2];
            startAngle = 0;
        }
        counterClockWise = counterClockWise || 0;
        this.arc(p0.x, p0.y, startAngle, endAngle, counterClockWise);
        return this;
    }
    ctxProto.QuadraticCurveTo = function (p1, ctl) {
        this.quadraticCurveTo(ctl.x, ctl.y, p1.x, p1.y);
        return this;
    }
    ctxProto.ArcTo = function (p0, p1, r) {
        var points = 0;
        this.arcTo(p0.x, p0.y, p1.x, p1.y, r);
        return this;
    }
    ctxProto.Path = function (pArr) {
        this.MoveTo(pArr[0]);
        for (var i = 1; i < pArr.length; i++) {
            this.lineTo(pArr[i].x, pArr[i].y);
        }
        return this;
    }
    ctxProto.LineTo = function (p) {
        for (var i = 0; i < arguments.length; i++) {
            this.lineTo(arguments[i].x, arguments[i].y);
        }
        return this;
    }
    ctxProto.Clip = function (rect) {
        rect && this.Rect(rect);
        this.clip();
        return this;
    }
    ctxProto.Fill = function (color) {
        color && (this.fillStyle = color);
        this.fill();
        return this;
    }
    ctxProto.Rect = function (rect) {
        this.rect(rect.x, rect.y, rect.width, rect.height);
        return this;
    }
    //生成Canvas設定TextAlign及TextBaseline的配套方法
    var textAlign = ["Start", "Left", "Center", "Right", "End"];
    var textBaseline = ["Top", "Middle", "Alphabetic", "Bottom"];
    for (var i = 0; i < textAlign.length; i++) {
        for (var j = 0; j < textBaseline.length; j++) {
            var str =
             "ctxProto.SetText" + textAlign[i] + textBaseline[j] +
             " = function () {"
            + "this.textAlign = '" + textAlign[i].toLowerCase() + "';"
            + "this.textBaseline = '" + textBaseline[j].toLowerCase() + "';"
            + "return this;};";
            eval(str);
        }
    }

    function TextConfig(align, baseline, color) {
        this.textAlign = TextConfig.textAlign.left;
        this.textBaseline = TextConfig.textBaseline.top;
        this.fillStyle = "white";
        for (var i = 0; i < arguments.length; i++) {
            if (TextConfig.textAlign[arguments[i]]) {
                this.textAlign = arguments[i];
            } else if (TextConfig.textBaseline[arguments[i]]) {
                this.textBaseline = arguments[i];
            } else {
                this.fillStyle = arguments[i];
            }
        }
    }
    var setting = Tools.StaticValuePacker({
        textAlign: ["left", "start", "center", "end", "right"],
        textBaseline: ["top", "middle", "alphabetic", "bottom"],
    });
    CopyProps(TextConfig, setting);
    TextConfig.prototype = {
        textAlign: TextConfig.textAlign.left,
        textBaseline: TextConfig.textBaseline.top,
        fillStyle: "white"
    }
    Tools.TextConfig = TextConfig;
}());

//音效集合物件
(function () {
    //音效集合物件
    var AudioControllers = [];
    var initiaedCount = 0;
    var defaultTrack = 1;
    function AudioController() {
        var ac = this;
        var o = new Object(null);
        var playingQueue = new Object(null);
        var pausedQueue = new Object(null);
        //var bindingEvent = ((Tools.IsiOs() && iOSVersion > 8) ? "touchend" : "click");
        var AudioType = Modernizr.audio.mp3 ? "mp3" : (Modernizr.audio.ogg ? "ogg" : 0);
        var tracks = [1, 1];
        var init = !IsMobile;
        Object.defineProperties(ac, {
            Initialized: { get: function () { return init; } }
        })
        var bgm = "";
        ac.isMute = false;
        ac.Res = {};
        /*設定檔案路徑*/
        var path = "";
        ac.Path = function (p) {
            path = p;
            return ac;
        };
        Object.defineProperties(ac, {
            Res: { get: function () { return o; } }
        });
        /*加入音樂資源*/
        ac.pushAudio = function (id, pa, opt) {
            if (!Tools.IsMobile) {
                var a = new Audio(pa.src);
                delete pa.src;
                pa = CopyProps(a, pa);
            }
            pa.id = id;
            pa.track = defaultTrack;
            opt && CopyProps(pa, opt);
            if (tracks[pa.track] == null)
                tracks[pa.track] = 1;
            pa.vol = tracks[pa.track];
            pa.volume = ac.isMute ? 0 : pa.vol;
            o[id] = pa;
            return ac;
        }
        function tmpAudio(src, opt) {
            this.src = src;
            CopyProps(this, opt);
        }
        /*加入音樂資源(base64字串)*/
        ac.pushBase64 = function (id, base64str, opt) {
            var pa = new tmpAudio("data:audio/" + AudioType + ";base64," + base64str);
            ac.pushAudio(id, pa, opt);
            return ac;
        }
        /*新增音效  id:音效名稱  (file:音效檔名)  (opt:自訂屬性)    */
        ac.push = function (id, file, opt) {
            if (typeof file != "string") {
                opt = file;
                file = 0;
            }
            var pa = new tmpAudio(path + (file || id) + "." + AudioType);
            ac.pushAudio(id, pa, opt);
            return ac;
        }
        /*使用manifest載入*/
        ac.Src = function (src) {
            for (var i in src[AudioType])
                if (src[AudioType].hasOwnProperty(i) && typeof (src[AudioType][i]) != "undefined")
                    ac.pushBase64(i, src[AudioType][i]);
            return ac;
        }

        /*靜音開關*/
        ac.ToggleMute = function (isMute) {
            ac.isMute = TestUndefined(isMute, !ac.isMute);
            if (ac.isMute) {
                for (var id in o) {
                    var audio = o[id];
                    audio.vol = audio.volume;
                    audio.volume = 0;
                }
            } else {
                for (var id in o) {
                    var audio = o[id];
                    audio.volume = audio.vol;
                }
            }
        }

        /*設定指定id音效的音量 
        * id:音效名稱  val:音量(0~1)*/
        ac.SetVolume = function (id, val) {
            if (!o[id]) return ac;
            o[id].vol = Tools.IsIOs ? (val ? 1 : 0) : val.Range(0, 1);
            o[id].volume = o[id].vol;
            if (o[id] == 0) ac.Pause(id);
            return ac;
        }

        /*設定全部音效的音量 
        * val:音量(0~1)*/
        ac.SetTrackVolume = function (track, val) {
            if (arguments.length == 0) {
                tracks.forEach(function (ele, ind) {
                    ac.SetTrackVolume(ele, val);
                });
                return;
            } else if (arguments.length == 2) {
                track = TestUndefined(track) ? defaultTrack : track;
                val = Tools.IsIOs ? (val ? 1 : 0) : val;
                tracks[track] = val;
                for (var id in o)
                    o[id].track == tracl && ac.SetVolume(id, val);
            }
            return ac;
        }

        function LoopHandler(audio, opt) {
            if (typeof (audio.Loop) == "number") {
                if (isUndefined(audio.loopCounter))
                    audio.loopCounter = 0;

                if (++audio.loopCounter >= audio.Loop) {
                    if (typeof (audio.callback) == "function")
                        audio.callback(audio);

                    delete audio.loopCounter;
                    delete audio.isPlaying;
                    delete playingQueue[audio.id];
                } else
                    Play(audio.id, opt);
            } else
                Play(audio.id, opt);
        }

        function ChainHandler(audio) {
            if (audio.NextAudio && audio.NextAudio.length > 1) {
                var ids = audio.NextAudio;
                delete audio.NextAudio;

                Play(ids[0], { Loop: false, NextAudio: ids.slice(1) });
            } else {
                var id = audio.NextAudio[0];
                delete audio.NextAudio;
                Play(id, { Loop: false });
            }
        }
        /*播放音效
        * id:指定音效名稱*/
        ac.play = Play;
        function Play(id, opt) {
            if (o[id]) {
                var audio = o[id];
                $(audio).off("ended,paused");
                audio.pause();

                opt && CopyProps(audio, opt);

                if (audio.currentTime)
                    audio.currentTime = 0;

                if (audio.Loop)
                    $(audio).one("ended", LoopHandler.bind(ac, audio));
                else if (audio.NextAudio)
                    $(audio).one("ended", ChainHandler.bind(ac, audio));

                $(audio).on("paused", PauseHandler.bind(ac, audio));

                try {
                    audio.play();
                    playingQueue[id] = 1;
                } catch (e) {
                    delete playingQueue[id];
                    return ac;
                }
            }
            return ac;
        }

        var queueCallback = function () {
            audioControl.play(this.nextAudio);
            delete playingQueue[this.id];
        }
        ac.PlayQueue = function (ids) {
            if (ids instanceof Array);
            else ids = ArgsParseArray(arguments);

            Play(ids[0], { Loop: false, NextAudio: ids.slice(1) });
        }

        /*停止所有音效*/
        ac.pauseNowPlaying = function () {
            for (var id in playingQueue) {
                o[id].pause();
                delete playingQueue[id];
            }
            return ac;
        }

        /*設定bgm
        * id:指定為背景音樂的名稱*/
        ac.bgm = function (id) {
            bgm = id;
            o[bgm].autoplay = true;
            return ac;
        }

        /*停止指定音效
        * id:指定音效名稱*/
        function PauseHandler(audio) {
            var id = typeof (audio) == "string" ? audio : audio.id;
            if (o[id]) {
                o[id].pause();
                delete playingQueue[id];
            }
            return ac;
        }
        ac.pause = PauseHandler;

        /*繼續撥放所有被暫停的音效*/
        function ResumeAll() {
            for (var id in pausedQueue) {
                o[id].play();
                delete pausedQueue[id];
            }
            return ac;
        }
        ac.resumeAll = ResumeAll;

        /*停止所有正在撥放的音效*/
        function PauseAll() {
            for (var id in playingQueue) {
                o[id].pause();
                pausedQueue[id] = 1;
            }
            return ac;
        }
        ac.pauseAll = PauseAll;

        ac.Set = function (id, opt) {
            if (o[id])
                CopyProps(o[id], opt)
        }

        /*取得指定音效物件
        * id:特定音效名稱*/
        ac.Get = function (id) {
            return o[id] || {};
        }
        function audioLoadComplete() {
            ac.OnAudioLoadComlete && ac.OnAudioLoadComlete();
            delete ac.OnAudioLoadComlete;
        }
        ac.OnAudioLoadComlete = function () { }
        ac.OnAudioLoadProgressChange = function (loaded, total, error) {

        }
        var queue = [];
        var count_loaded = 0;
        var count_failed = 0;
        var count_total = 0;
        ac.Reload = function () {
            if (Tools.IsMobile) {
                init = true;
                queue = [];
                for (var i in o)
                    if (o[i] instanceof tmpAudio)
                        queue.push(i);

                count_total = queue.length;
                count_loaded = 0;
                count_failed = 0;
                loadNext();
            } else {
                audioLoadComplete();
            }
        }
        function loadNext() {
            if (queue.length) {
                var raw = o[queue.pop()];
                try {
                    if (o[raw.id] instanceof tmpAudio) {
                        o[raw.id] = new Audio(raw.src);
                        delete raw.src;
                        CopyProps(o[raw.id], raw);
                    }
                    var audio = o[raw.id];
                    audio.volume = 0;
                    audio.play();
                    audio.pause();
                    audio.volume = audio.vol;
                    count_loaded++;
                    loadNext();
                } catch (e) {
                    count_failed++;
                }
                ac.OnAudioLoadProgressChange(count_loaded, count_total, count_failed);

                //audio.oncanplay = loadNext;
            } else {
                audioLoadComplete();
            }
        }
        ac.InitAct = function () {
            if (Tools.IsMobile && !init) {
                ac.Reload();
            }
            if (bgm && o[bgm].paused)
                o[bgm].play();
        }
        AudioControllers.push(ac);
    }

    AudioController.InitAll = function () {

        if (initiaedCount == AudioControllers.length)
            return;
        initiaedCount = 0;
        for (var i = 0; i < AudioControllers.length; i++)
            if (!AudioControllers[i].Initiated)
                if (AudioControllers[i].InitAct) {
                    AudioControllers[i].InitAct();
                    initiaedCount++;
                }
    }
    Tools.AudioController = AudioController;
}());

/*new engine obj*/
(function () {

    function Gesture() { this._draged = Tools.Size.Get(); }
    CopyProps(Gesture, Tools.StaticValuePacker({
        val: ["Down", "Drag", "Release", "Click", "Enter", "Leave", "Hover"]
    }).val);
    Gesture.events = ["mousedown", "touchstart", "mousemove", "touchmove", "mouseup", "touchend"];
    Gesture.prototype = {
        status: false,
        _detectTarget: null,
        activedRegion: null,
        _sensitive: 5,
        _lastObject: null,
        _enableMove: false,
        _lastDown: ClearIndivisualProps(new Tools.Point()),
        _lastDownObj: null,
        _lastMove: ClearIndivisualProps(new Tools.Point()),
        _lastUp: ClearIndivisualProps(new Tools.Point()),
        _draged: ClearIndivisualProps(new Tools.Size()),
        ReferenceStage: function (stage) {
            this.activedRegion = stage;
        },
        ToggleMove: function (mode) {
            this._enableMove = arguments.length > 0 ? mode : !this._enableMove;
            if (Tools.IsMobile) this._enableMove = false;

            if (this._enableMove) {
                this._detectTarget.addEventListener("mousemove", this.eventHandle, false);
                this._detectTarget.addEventListener("touchmove", this.eventHandle, false);
            } else {
                this._detectTarget.removeEventListener("mousemove", this.eventHandle);
                this._detectTarget.removeEventListener("touchmove", this.eventHandle);
            }
        },
        eventHandle: function (evt) {
            if (this.status) { return; } else { this.status = true; }
            if (!this.p) { this.p = Tools.Point.Get(); }
            evt.preventDefault && evt.preventDefault();
            if (!this.activedRegion)
                return false;
            getMousePos(this.p, evt).Scale(1 / Tools.GetZoom());
            var obj = this.activedRegion.GetObjectAtPoint(this.p);
            var needUpdate = false;
            switch (evt.type) {
                case "mousedown": case "touchstart":
                    ClearIndivisualProps(this._lastUp);
                    this._lastDown.GetPoint(this.p);
                    this._lastDownObj = obj;

                    needUpdate = (obj) && (obj.TriggerEvent(Gesture.Down, obj, evt));
                    break;
                case "mousemove": case "touchmove":
                    if (this._lastDown.hasOwnProperty("x")) {
                        (evt.draged = this._draged.Set(this._lastDown, this.p));
                        if (obj && obj == this._lastDownObj && (needUpdate = obj.TriggerEvent(Gesture.Drag, obj, evt))) {
                            break;
                        } else {
                            if (obj != this._lastDownObj && (needUpdate = this._lastDownObj && this._lastDownObj.TriggerEvent(Gesture.Release, obj, evt))) {
                                break;
                            }
                        }
                    }
                    if (this._lastObject) {
                        if (this._lastObject != obj) {
                            needUpdate = (!IsMobile) && this._lastObject.TriggerEvent(Gesture.Leave, this._lastObject, evt);
                            (!IsMobile) && (obj) && obj.TriggerEvent(Gesture.Enter, obj, evt);
                        } else {
                            (!IsMobile) && (obj) && obj.TriggerEvent(Gesture.Hover, obj, evt);
                        }
                    } else {
                        needUpdate = (!IsMobile) && (obj) && obj.TriggerEvent(Gesture.Enter, obj, evt);
                    }

                    this._lastMove.GetPoint(this.p);

                    break;
                case "mouseup": case "touchend":
                    Tools.AudioController.InitAll();
                    if (this._lastDown.x) {
                        if ((obj) && obj == this._lastDownObj) {
                            if (needUpdate = obj.TriggerEvent(Gesture.Click, obj, evt)) {
                                break;
                            }
                        }
                        var path = Tools.Size.Get(this.p, this._lastDown);
                        if (path.Diagnal() > this._sensitive) {
                            if ((obj) && obj.TriggerEvent(Gesture.Release, obj, evt)) {
                                obj.TriggerEvent(Gesture.Click, obj, evt);
                            }
                        } else {
                            (obj) && obj.TriggerEvent(Gesture.Click, obj, evt);
                        }

                        Tools.Release(path);
                        this._lastUp.GetPoint(this.p);
                        needUpdate = true;
                    }
                    delete this._lastDown.x, this._lastDown.y;
                    break;
            }
            //if (needUpdate) {
            //    this.activedRegion.Update();
            //}
            this._lastObject = obj;
            this.status = false;
        },
        AttachEvents: function (target) {
            this._detectTarget = target;
            var self = this;
            for (var i = 0; i < Gesture.events.length; i++) {
                this._detectTarget.addEventListener(Gesture.events[i], function (evt) { self.eventHandle(evt) }, true)
            }
            this.ToggleMove(!IsIOs)
        }
    }
    Tools.Gesture = Gesture;

    function defineProp(obj, prop) {
        var privat = "_" + prop.toLowerCase();
        Object.defineProperty(obj, prop, {
            get: function () { return this[privat]; },
            set: function (val) {
                if (val != this[privat])
                    this.needUpdate = true;
                this[privat] = val || 0;
            }
        });
    }

    /*class numberString*/
    Tools.NumberString = function () {
        function num(imgID, value, rect) {
            this.Image = [imgID];
            this.Value = value || 0;
            this.Set.apply(this, SliceArgs(arguments, 2));
        }
        num.Align = Tools.StaticValuePacker({ val: ["center", "start", "end"] }).val;
        num.DefaultMapping = {
            Numbers: "1234567890",
            WithComma: "1234567890,",
            Dollars: "$1234567890,",
        }
        ClassExtend(num, Tools.DisplayObj, {
            _value: "",
            _align: "center",
            Widths: null,
            Mapping: num.DefaultMapping.Numbers,
            Update: function (ctx, ignoreCache) {
                if (!this.Display) { return; }
                ctx.save();
                if (ignoreCache) {
                    ctx.DrawImg(this, this.x, this.y, this.imgNo);
                } else {
                    if (!this.cached || this.needUpdate)
                        this.UpdateCache();
                    ctx.DrawImg(this.cached, this.x, this.y);
                    //ctx.StrokeStyle("white").StrokeRect(this.x, this.y, this.width, this.height);//for debug
                    this.needUpdate = false;
                }
                ctx.restore();
            },
            UpdateCache: function () {
                var self = this;
                var r = Tools.Rect.Get(this).Translate();
                var cache = function (ctx) {
                    var img = Tools.GetImage(this.Image[0]);
                    if (typeof (img) == "string") {
                        Tools.RecordLog("nonvalid number img id", true);
                        return;
                    }
                    ctx.NumString(img, this.Mapping, this.Value, this.Align, r, false, { Widths: this.Widths });
                }.bind(this);
                if (this.cached) {
                    this.cached = Tools.CacheCanvas.Cache(this.cached, r, cache);
                } else {
                    this.cached = Tools.CacheCanvas.Cache(r, cache);
                }

                Tools.Release(r);
                delete s;
            }
        });

        defineProp(num.prototype, "Value");
        defineProp(num.prototype, "Align");
        return num;
    }();

    /*class TextObj*/
    function Text(val, recr) {
        this.Value = val;
        this.Set.apply(this, SliceArgs(arguments, 1));
        this.FontSize = this.height;
        this.TextPoint = Tools.Point.Get(this.MidPoint());
    }
    CopyProps(Text, {
        CountLines: function (str, lineLengthLimit) {
            var textlines = [], result = 1;
            if (typeof (lineLengthLimit) == "number" && lineLengthLimit > 0) {
                var prestr = str.split("\r\n");
                while (prestr.length > 0) {
                    var index = 0;
                    var s = prestr.shift();
                    while (index < s.length) {
                        var len = (index + lineLengthLimit > s.length)
                                 ? (s.length - index + lineLengthLimit)
                                 : (lineLengthLimit);

                        var sepLine = s.slice(index, index + len).indexOf("\r\n");
                        if (sepLine > -1) {
                            textlines.push(s.slice(index, index + sepLine).replace("\r\n", ""));
                            index += sepLine + 1;
                        } else {
                            textlines.push(s.slice(index, index + len));
                            index += lineLengthLimit;
                        }
                    }
                }
                (textlines.length == 0) && textlines.push("");
                return textlines;
            } else
                return str.split("\r\n");
        },
        AlignPoint: Tools.StaticValuePacker({
            type: ["MidPoint", "CenterTop", "CenterBottom",
                       "LeftMiddle", "LeftTop", "LeftBottom",
                       "RightMiddle", "RightTop", "RightBottom", "None"]
        }).type,
        _Cal: function (ctx, text, point, mode, size, LineCount) {
            switch (mode) {
                default:
                case Text.AlignPoint.MidPoint:
                    text.MidPoint(point);
                    point.y -= (LineCount - 1) * size / 2;
                    ctx.SetTextCenterMiddle();
                    break;
                case Text.AlignPoint.CenterTop:
                    text.CenterTop(point);
                    ctx.SetTextCenterTop();
                    break;
                case Text.AlignPoint.CenterBottom:
                    text.CenterBottom(point);
                    ctx.SetTextCenterBottom();
                    break;
                case Text.AlignPoint.LeftTop:
                    text.LeftTop(point);
                    ctx.SetTextLeftTop();
                    break;
                case Text.AlignPoint.LeftMiddle:
                    text.LeftMiddle(point);
                    point.y -= (LineCount - 1) * size / 2;
                    ctx.SetTextLeftMiddle();
                    break;
                case Text.AlignPoint.LeftBottom:
                    text.LeftBottom(point);
                    point.y -= (LineCount - 1) * size;
                    ctx.SetTextLeftBottom();
                    break;
                case Text.AlignPoint.RightTop:
                    text.RightTop(point);
                    ctx.SetTextRightTop();
                    break;
                case Text.AlignPoint.RightMiddle:
                    text.RightMiddle(point);
                    point.y -= (LineCount - 1) * size / 2;

                    ctx.SetTextRightMiddle();
                    break;
                case Text.AlignPoint.RightBottom:
                    text.RightBottom(point);

                    point.y -= (LineCount - 1) * size;
                    ctx.SetTextRightBottom();
                    break;
            }

        }
    });
    var textProto = {
        _value: "",
        _color: "black",
        _size: 12,
        _mode: Text.AlignPoint.MidPoint,
        Display: true,
        LineLengthLimit: 0,
        TextPoint: null,
        Update: function (ctx, ignoreCache) {
            if (!this.Display) { return; }
            ctx.save();
            if (ignoreCache) {
                ctx.DrawImg(this, this.x, this.y, this.imgNo);
            } else {
                if (!this.cached || this.needUpdate)
                    this.UpdateCache();
                ctx.DrawImg(this.cached, this.x, this.y);
                this.needUpdate = false;
                //ctx.StrokeStyle("white").StrokeRect(this.x, this.y, this.width, this.height);
            }
            ctx.restore();
        },
        UpdateCache: function () {
            var self = this;

            var r = Tools.Rect.Get(this).Translate();
            function cache(ctx) {
                ctx.FillStyle(self.FontColor);
                ctx.Font(self.FontSize);

                var textlines = Tools.Text.CountLines(self._value, self.LineLengthLimit)

                var lines = textlines.length;
                var maxlength = 0;

                var txtw = 0;
                for (var i = 0; i < textlines.length; i++)
                    txtw = Math.max(txtw, ctx.measureText(textlines[i]).width);

                var tsize = self.FontSize;
                if (txtw > self.width)
                    tsize *= self.width / txtw;
                if (tsize > self.height / lines)
                    tsize = self.height / lines;
                ctx.Font(tsize);
                Text._Cal(ctx, r, self.TextPoint, self._mode, tsize, lines);
                for (var i = 0; i < lines; i++) {
                    var px = self.TextPoint.x;
                    var dx = 0;
                    var py = self.TextPoint.y;
                    var dy = tsize * i + 0;
                    ctx.FillText(textlines[i], px + dx, py + dy);
                }
            }
            var r = Tools.Rect.Get(this).Translate();
            if (this.cached) {
                this.cached = Tools.CacheCanvas.Cache(this.cached, r, cache);
            } else {
                this.cached = Tools.CacheCanvas.Cache(r, cache);
            }

            Tools.Release(r);
            delete s;
        }
    }
    for (var i in Text.AlignPoint) {
        textProto["Set" + i] = function (name) {
            return function () { this.Mode = name; }
        }(i)
    }
    ClassExtend(Text, Tools.Rect, textProto);

    Object.defineProperties(Text.prototype, {
        Value: {
            get: function () { return this._value; },
            set: function (val) {
                this._value = String(val).replace(/\<br\/\>/g, "\r\n").replace(/\&nbsp;/g, " ");
                this.needUpdate = true;
                this.parent && (this.parent.needUpdate = true);
            }
        },
        LineCount: {
            get: function () {
                return Tools.Text.CountLines(this._value, this.LineLengthLimit).length || 1
            }
        },
        FontColor: {
            get: function () { return this._color; },
            set: function (val) {
                if (val != this._color) this.needUpdate = true;
                this._color = val;
            }
        },
        FontSize: {
            get: function () {
                return this.percentageBase ? ParsePercentage(this._size, this.percentageBase.height) : this._size;

            },
            set: function (val) {
                if (val != this._size) this.needUpdate = true;
                this._size = val;
            }
        },
        Mode: {
            get: function () { return this._mode; },
            set: function (val) {
                if (val in Text.AlignPoint) this._mode = val;
            }
        },
    })
    Tools.Text = Text;

    AnimateImage.count = 0;
    /*class AniObj*/
    function AnimateImage(img, startRect, endRect, duration, opt) {
        AnimateImage.count++;
        this.Set.apply(this, arguments);
    }
    ClassExtend(AnimateImage, Tools.DisplayObj, {
        Set: function (img, startRect, endRect, duration) {
            this.Image = [img];
            this.StartRect = Tools.Rect.Get(startRect);
            this.EndRect = Tools.Rect.Get(endRect);
            Tools.Rect.prototype.Set.apply(this, startRect);

            this.Duration = duration;
            this.Timer || (this.Timer = Tools.Timer.Get(0, { pool: "ani" }));
            this.Start();
        },
        StartRect: null,
        EndRect: null,
        Duration: 0,
        Start: function () {
            if (!this.Timer)
                this.Timer = Tools.Timer.Get(0, { pool: "ani" });
            this.Timer.Enable();
            this.Timer.Set(this.Duration, {
                pool: "ani",
                delay: this.delay || 0,
                nonstop: this.nonstop || false,
            });
        },
        Update: function (ctx) {
            if (!this.Display) { return; }
            if (!this.Timer) { return; }
            switch (this.Timer.State) {
                case Tools.Timer.WORKING:
                    Tools.Tween.Rect(this, this.Timer.Porportion, this.StartRect, this.EndRect);
                    break;
                case Tools.Timer.ENDED:
                    this.Set(this.EndRect);
                    (this.callback) && this.callback(this);
                    this.Timer.Disable();
                    Tools.Timer.Release(this.Timer);
                    delete this.Timer;
                    break;
                default:
                    return;
            }
            ctx.save();
            ctx.DrawImg(this, this.x, this.y);
            ctx.restore();
        },
        ChangeImage: function (img) {
            this.Image = img;
            return this;
        }
    });
    Tools.AnimateImage = AnimateImage;

    /*class anidraw*/
    function AnimateDraw(func, rect, duration) {
        this.Draw = func;
        this.Set(rect);
        this.Duration = duration;
        this.Start();
        AnimateDraw.count++;
    }
    AnimateDraw.count = 0;
    ClassExtend(AnimateDraw, Tools.DisplayObj, {
        Start: function () {
            if (!this.Timer)
                this.Timer = Tools.Timer.Get(0, { pool: "ani" });

            this.Timer.Set(this.Duration, {
                pool: "ani",
                delay: this.delay || 0,
                nonstop: this.nonstop || false,
            });
        },
        Update: function (ctx) {
            if (!this.Display) { return; }
            switch (this.Timer.State) {
                case Tools.Timer.WORKING:
                    this.Draw(ctx);
                    break;
                case Tools.Timer.ENDED:
                    this.Draw(ctx);
                    (this.callback) && this.callback(this);
                    delete this.Timer;
                    break;
            }
        }
    })
    Tools.AnimateDraw = AnimateDraw;

    /*class staObj*/
    function StaticImage(img, rect) {
        var dif = 1;
        var opt = arguments[arguments.length - 1];
        if (typeof (opt) == "object") {
            if (opt instanceof Tools.Rect || opt instanceof Tools.Point || opt instanceof Tools.DisplayObj) {
                opt = {};
                dif = 0;
            }
        }
        this.SetDisplayObj.apply(this, arguments);
        if (arguments[0] instanceof Tools.DisplayObj)
            this.Image = img.Image;
        else
            this.Image = [img];
        this.cached = null;
        for (var prop in opt)
            this[prop] = opt[prop];
        delete opt;
        StaticImage.count++;
    }
    StaticImage.count = 0;
    ClassExtend(StaticImage, Tools.DisplayObj, {
        Update: function (ctx, ignoreCache) {
            if (!this.Display) { return; }
            ctx.save();
            if (ignoreCache) {
                ctx.DrawImg(this, this.x, this.y, this.imgNo);
            } else {
                if (!this.cached || this.needUpdate)
                    this.UpdateCache();
                ctx.DrawImg(this.cached, this.x, this.y);
                //ctx.StrokeStyle("white").StrokeRect(this.x, this.y, this.width, this.height);//for debug
                this.needUpdate = false;
            }
            ctx.restore();
        },
        imgNo: 0,
        UpdateCache: function () {
            var img = this.Image[this.imgNo || 0];
            var r = Tools.Rect.Get(this).Translate();
            if (this.cached) {
                this.cached = Tools.CacheCanvas.Cache(this.cached, r, function (ctx) { ctx.DrawImg(img, r) });
            } else {
                this.cached = Tools.CacheCanvas.Cache(r, function (ctx) { ctx.DrawImg(img, r) });
            }

            Tools.Release(r);
            delete s;
        },
        SwitchImage: function (no) {
            if (this.imgNo != no) {
                this.imgNo = no;
                this.needUpdate = true;
            }
        },
        ChangeImage: function (img) {
            this.Image[0] = img;
            this.needUpdate = true;
            return this;
        }
    });
    Tools.StaticImage = StaticImage;

    /*class drawobject*/
    function DrawObject(func, rect, opt) {
        opt = opt || {};
        this.cache = null;
        this.Draw = func;
        this.Set(rect || 0);
        for (var i in opt)
            this[i] = opt[i];
        DrawObject.count++;
    }
    DrawObject.count = 0;
    ClassExtend(DrawObject, Tools.DisplayObj, {
        Type: "DrawObject",
        ignoreCache: true,
        Update: function (ctx, ignoreCache) {
            if (!this.Display) { return; }
            ctx.save();
            if (ignoreCache || this.ignoreCache) {
                this.Draw(ctx);
                //ctx.StrokeStyle("white").StrokeRect(this.x, this.y, this.width, this.height);//for debug
            } else {
                if (!this.cached || this.needUpdate)
                    this.UpdateCache();
                ctx.DrawImg(this.cached, this.x, this.y, this.width, this.height);
                this.needUpdate = false;
            }
            ctx.restore();
        },
        UpdateCache: function () {
            if (this.cached) {
                this.cached = Tools.CacheCanvas.Cache(this.cachde, this, this.Draw);
            } else {
                this.cached = Tools.CacheCanvas.Cache(this, this.Draw);
            }
        }
    });
    Tools.DrawObject = DrawObject;

    /*class Container*/
    function Container(rect) {
        this.Set(arguments);
        this._init();
    }
    ClassExtend(Container, Tools.DisplayObj, {
        _init: function () {
            this.children = [];
            this._waitForRemove = [];
        },
        Update: function (ctx, ignoreCache, accuMatrix) {
            if (!this.Display) { return; }
            ctx.save();
            this.updateContext(ctx, accuMatrix);
            var l = this.children.slice();
            for (var i = 0; i < l.length; i++) {
                var child = l[i];
                if (!child) { continue; }
                child.Update && child.Update(ctx, ignoreCache, accuMatrix);
            }
            this.restoreContext(ctx, accuMatrix);
            ctx.restore();
            delete l;
        },
        push: function (obj) {
            if (arguments.length > 1) {
                for (var i = 0; i < arguments.length; i++)
                    this.push(arguments[i]);
            } else if (obj instanceof Array) {
                for (var i = 0; i < obj.length; i++)
                    this.push(obj[i]);
            } else {
                obj.parent && obj.parent.remove && obj.parent.remove(obj);
                obj.parent = this;
                this.children.push(obj);
            }
        },
        remove: function (child) {
            var obj;
            if (arguments.length > 1) {
                for (var i = 0; i < arguments; i++)
                    obj = this.removeAt(this.children.indexOf(arguments[i]));
            } else {
                if (arguments[0].length) {
                    for (var i = 0; i < arguments[0].length; i++)
                        obj = this.removeAt(this.children.indexOf(arguments[0][i]));
                } else
                    obj = this.removeAt(this.children.indexOf(child));
            }
            return obj;
        },
        removeAll: function () {
            for (var i = 0; i < this.children.length; i++) {
                this.children[i].parent = null;
                delete this.children[i];
            }
            this.children = [];
            return this;
        },
        pop: function () {
            var obj = this.children.pop();
            obj.parent = null;
            return obj;
        },
        removeAt: function (index) {
            var obj;
            if (arguments.length > 1) {
                for (var i = 0; i < arguments.length; i++)
                    obj = this.removeAt(arguments[i])
            } else {
                if (index < 0 || index >= this.children.length) { return false; }
                obj = this.children.splice(index, 1)[0];
                if (obj.parent)
                    obj.parent.remove(obj);
            }
            return obj;
        },
        insertAt: function (index, obj) {
            if (obj instanceof Array) {
                var args = [index, 0].concat(obj);
                Array.prototype.splice.apply(this.children, args);
            } else {
                this.children.splice(index, 0, obj);
            }
            return obj;
        },
        moveTo: function (obj, index) {
            var ind = this.children.indexOf(obj);
            if (ind < 0 || ind > this.children.length) { return false; }
            var obj = this.removeAt(ind);
            this.insertAt(obj, index);
        },
        SetEnable: function (type) {
            type = type || false;
            for (var i = 0; i < this.children.length; i++) {
                this.children[i].SetEnable(type);
            }
        },
        GetObjectAtPoint: function (p) {
            if (!this.localp) {
                this.localp = Tools.Point.Get();
            }
            this.localp.GetPoint(p).Translate(this.Matrix.x, this.Matrix.y);
            var result;
            for (var i = this.children.length - 1; i >= 0; i--) {
                if (this.children[i].InRect && this.children[i].InRect(this.localp)) {
                    if (this.children[i] instanceof Container) {
                        if (result = this.children[i].GetObjectAtPoint(this.localp)) {
                            break;
                        }
                    } else {
                        if (result = this.children[i]) {
                            break;
                        }
                    }
                }
            }
            return result;
        }
    });
    Object.defineProperties(Container.prototype, {
        length: { get: function () { return this.children.length; } }
    })
    Tools.Container = Container;

    /*class stage*/
    function Stage(canvas) {
        this._init();
        this.Set(0, 0, canvas.width, canvas.height);
        this.canvas = canvas;
        this._ctx = Tools.SetHDPICanvas(canvas, canvas.width, canvas.height);
        this._pixelRatio = Tools.GetPixelRatio(this._ctx);
    }
    ClassExtend(Stage, Container, {
        _updateing: false,
        accumulatedMatrix: Tools.Matrix2D.Identity.Clone(),
        sp_pixelRatio: 1,
        SetSize: function () {
            var w, h;
            if (arguments[0] instanceof Tools.Size) {
                w = arguments[0].width;
                h = arguments[0].height;
            } else if (arguments.length == 2) {
                w = arguments[0];
                h = arguments[1];
            }
            this._ctx = Tools.SetHDPICanvas(this.canvas, w, h);
        },
        Update: function (ignoreCache) {
            if (this._updateing) return;
            else this._updateing = true;

            this.accumulatedMatrix.SetMatrix(Tools.Matrix2D.Identity);
            this._ctx.SetTransform(this.accumulatedMatrix);
            this._ctx.Clear();
            if (!this.Display) { return; }
            this.updateContext(this._ctx, this.accumulatedMatrix);
            var l = this.children.slice();
            for (var i = 0; i < l.length; i++)
                l[i].Update && l[i].Update(this._ctx, ignoreCache, this.accumulatedMatrix);
            delete l;
            this._updateing = false;
        },
        SetScale: function (x, y) {
            if (!arguments[1])
                y = x;
            this.Matrix.setScale(x * this._pixelRatio, y * this._pixelRatio);
        }
    });


    Tools.Stage = Stage;
    //動畫運作
    /*fps based view engine*/
    function Ticker(fps) {
        this.Initialize(fps);
    }
    Ticker.prototype = {
        ID: 0,
        FPS: 60,
        _interval: 1000 / 60,
        Start: function () {
            this._tick();
        },
        Initialize: function (fps) {
            this.SetFPS(fps);
        },
        SetFPS: function (fps) {
            this.FPS = fps || 60;
            this._interval = 1000 / this.FPS;
        },
        _update: null,
        _tick: function () {
            var self = this;
            var time;
            self._tick = function (timestamp) {
                this.ID = requestAnimationFrame(tick, self);
                if (!time) { time = timestamp; }
                Tools.Timer.UpdatePool("ani");
                if (timestamp - time > this._interval) {
                    try { this._update(); } catch (e) { }
                    time = timestamp;
                }
            }
            var tick = self._tick.bind(self);
            self.ID = requestAnimationFrame(self._tick, self);
        },
        SetUpdate: function (func) {
            this._update = func;
        },
    }
    Tools.Ticker = Ticker;
}());

/*utility Record console log*/
(function () {
    Tools.RecordLog = function (status, type) {
        if ((!(type in Tools.Debug) || !Tools.Debug[type]) && (type != true)) return;
        if (Tools.Configs.useSimpleLogInfo) {
            console.log(status);
        } else {
            var time = new Date().toLocaleTimeString();
            console.log(time + " => " + status);
        }
    }
}());
/*class websocket*/
(function () {
    function Connection(url) {
        var ws;
        var _isAccessing = false;
        var self = this;
        var sentMsg = [];
        var _pingPongEnabled = false;
        var _pingPongDuration = 30000;
        var _pingPongTryTimes = 5;
        var _pingPongRetry = 0;
        var _pingPongTimer = 0;
        var _pingPongLastTime = Tools.NowTime;
        var messageReact = {};
        this.saveMsgPack = false;
        this.MsgPacks = {};
        this.URL = url;
        this.Name = "Connection";
        this.Autcode = "qazwsx";

        Object.defineProperties(this, {
            "isAccessing": {
                get: function () { return _isAccessing },
            },
            "PingPongDuration": {
                get: function () { return _pingPongDuration; },
                set: function (val) {
                    try {
                        val = Number(val);
                        _pingPongDuration = val;
                    } catch (e) { }
                }
            },
            "PingPongRetryTime": {
                get: function () { return _pingPongTryTimes; },
                set: function (val) {
                    try {
                        val = Number(val);
                        _pingPongTryTimes = val;
                    } catch (e) { }
                }
            }
        })

        /*do pingpong*/
        this.sendPingPong = function () {
            ws.send(100, 0, { PingPong: _pingPongRetry });
        }

        /*switch on/off pingpong utility */
        this.TogglePingPong = function (on) {
            on = arguments.length ? on : !_pingPongEnabled;
            on ? ResetPingPong()
                : StopPingPing();
        }

        /*reset pingpong timerout and retry times*/
        function ResetPingPong() {
            _pingPongRetry = 0;
            PausePingPingTimer();
            if (ws.readyState != WebSocket.OPEN) {
                Tools.RecordLog(self.Name + ": PingPong failed by connection closed", Tools.DebugType.Connection);
                return;
            }
            _pingPongTimer = setTimeout(function () {
                if (++_pingPongRetry > _pingPongTryTimes) {
                    //close
                    Tools.RecordLog(self.Name + ": PingPong no response", Tools.DebugType.Connection);
                    ws && ws.close();
                } else    //try pingpong
                    self.sendPingPong();
            }, _pingPongDuration);
        }

        /*stop pingpong timer*/
        function PausePingPingTimer() {
            clearTimeout(_pingPongTimer);
            _pingPongTimer = 0;
        }

        function msgPack(type, content) {
            this.Time = new Date().toISOString();
            this.Type = type;
            this.Content = ((content instanceof Object) && (this.Content = content)) || {};
        }
        msgPack.prototype = {
            Type: "", Time: "", Content: {}
        };

        /*msgpack*/
        function Send() {
            if (!Tools.Configs.enableConnect) return;

            var id = Tools.RandomGenerator.Mix(6, Tools.NowTime);
            if (_pingPongEnabled) {
                sentMsg.push(id);
            };
            var msgpack = new msgPack(arguments[0], arguments[1]);

            WebSocket.prototype.send.apply(ws, [window.JSON.stringify(msgpack)]);
            Tools.RecordLog("message send:" + msgpack.Type, Tools.DebugType.Connection);
        }
        this.Send = Send;

        /*inner onopen func*/
        function onOpen() {
            if (!Tools.Configs.enableConnect) return;
            if (ws.readyState == WebSocket.OPEN) {
                Tools.RecordLog("OnOpen", Tools.DebugType.Connection);
                self.onopen.apply(self, arguments);
                if (_pingPongEnabled && !_pingPongTimer)
                    ResetPingPongTimer();
            }
        };

        /*event onOpen*/
        this.onopen = function () {

        };

        this.LastMsg = null;

        /*inner onMsg*/
        function onMessage(e) {
            _isAccessing = false;

            var amsg = (e.data && typeof (e.data) == "string") ? window.JSON.parse(e.data) : e;
            self.LastMsg = amsg;
            if (_pingPongEnabled)
                if (sentMsg.RemoveWhere(function (o) { return o == amsg.ct.id; }).length)
                    ResetPingPongTimer();
            if ("Type" in amsg) {
                Tools.RecordLog("message get :" + amsg.Type, "Connection");

                if (amsg.Time) {
                    var dif = ParseDate(amsg.Time) - new Date().getTime();
                    if (!Tools.timedif || Math.abs(dif) < Math.abs(Tools.timedif))
                        Tools.timedif = dif;
                }
                if (self.saveMsgPack)
                    self.MsgPacks[amsg.Type] = amsg;

                if (messageReact[amsg.Type])
                    messageReact[amsg.Type].call(ws, amsg.Content);
            }
        };

        this.TriggerOnMessage = function () {
            var o = { Type: arguments[0], Content: arguments[1], Time: new Date().toISOString() };
            onMessage(o);
        }

        var sendAct = {};
        function act(type, construct) {
            this.type = type;
            (construct instanceof Function) && (this.constructor = construct);
        }
        act.prototype = {
            type: "",
            constructor: function () { return {}; },
            Send: function () {
                self.TriggerSendAct(this.type);
            }
        }
        this.AddSendAct = function (type, contentConstructor) {
            sendAct[type] = new act(type, contentConstructor);
        }
        this.TriggerSendAct = function (type) {
            self.Send(type, sendAct[type].constructor());
        }
        Object.defineProperty(self, "SendActs", {
            get: function () { return sendAct; }
        })

        /*event onmessage
           typeArr: [mainType, subType, (...)] || string(split by '.')
           msg: string[] || object*/
        this.AddOnMessageHandler = function (typeArr, handle) {
            if (!typeArr) return;
            var type = (typeArr instanceof Array) ?
                 typeArr.join(".") : typeArr;
            messageReact[type] = handle;
            return self;
        }

        this.RemoveOnMessageHandler = function (typeArr) {
            if (!typeArr) return;
            var type = (typeArr instanceof Array) ?
                 typeArr.join(".") : typeArr;
            delete messageReact[type];
        }

        /*innner onClose*/
        function onClose(evt) {
            Tools.RecordLog(self.Name + ":connection closed @" + evt.code || "" + ":" + evt.reason || "", Tools.DebugType.Connection);
            self.onclose.apply(ws, arguments);
        }

        /*event onClose
            evt: closeEvent*/
        this.onclose = function (evt) { }

        /*inner onError*/
        function onError(evt) {
            Tools.RecordLog(self.Name + ":error happened @" + evt.code || "" + ":" + evt.reason || "", Tools.DebugType.Connection);
            self.onclose.apply(ws, arguments);
        }

        /*event onError
            evt: errorEvent*/
        this.onerror = function (evt) { };

        /*void initialize connection
            url:ws|wss://url*/
        this.StartConnect = function (url) {
            url = url || this.URL;
            if (ws && ws.readyState != WebSocket.CLOSED)
                throw Tools.ErrorMsg.Connection.NOT_CLOSED(this);
            ws = new WebSocket(url);
            Tools.RecordLog(self.Name + ":Start connection to " + url, Tools.DebugType.Connection);

            /*attatch events*/
            ws.send = Send;
            ws.onopen = onOpen;
            ws.onmessage = onMessage;
            ws.onclose = onClose;
            ws.onerror = onError;
        }

        this.close = function () {
            ws.close();
        }

        Object.defineProperties(this, {
            readyState: {
                get: function () { return ws.readyState; }
            },
            Instance: { get: function () { return ws; } }
        })
    }
    Tools.Connection = Connection;
}());

/*Extensions String*/
(function () {
    String.prototype.Contains = function (test) {
        var result = false;
        for (var i = 0; i < arguments.length; i++)
            result = result || (this.valueOf().indexOf(arguments[i]) > -1);
        return result;
    }
    String.prototype.SplitToNumber = function (seperator) {
        return this.split(seperator).map(function (item) { return Number(item) });
    }
    String.prototype.capitalizeFirstLetter = function () {
        return this.charAt(0).toUpperCase() + this.slice(1);
    }
}());


/*Utility Test*/
(function () {
    var Test = Object.create(null);
    var baseTests = ["string", "number", "boolean"];
    function testBaseType(type, target, dval) {
        switch (arguments.length) {
            case 2:
                return (typeof (target) == type);
            case 3:
                return (typeof (target) == type) ? target : dval;
            default:
                return false;
        }
    }
    for (var i = 0; i < baseTests.length; i++)
        Test[baseTests[i].capitalizeFirstLetter()] = testBaseType.bind(this, baseTests[i]);

    Test.Undefined = function (obj, dval) {
        var hasValue = (typeof (obj) != "undefined") && (obj != null);
        if (arguments.length == 2)
            return hasValue ? obj : dval;
        else
            return !hasValue;
    }

    Test.Array = function (obj) {
        return Test.Class(obj, Array);
    }
    Test.Function = function (obj) {
        return obj instanceof Function;
    },
    Test.Class = function (obj, Class) {
        Class = Class || Object;
        return obj instanceof Class;
    }
    Test.InClass = function (obj, Classes) {
        var result = false;
        SliceArgs(arguments, 1).forEach(function (o, i) {
            result = result || obj instanceof o;
        })
        return result;
    }

    Tools.isImageElement = function (img) {
        return Test.InClass(img, Image, HTMLCanvasElement, HTMLImageElement);
    }

    window.Test = Test;
}());


/*Extensions Date*/
(function () {
    var Signs = Object.create(null);
    Signs.Date = function (date, char) {
        if (!(date instanceof Date)) {
            char = date;
            date = this;
        }
        switch (char) {
            case "y": case "Y":
                return date.getFullYear();
            case "M":
                return date.getMonth() + 1;
            case "d": case "D":
                return date.getDate();
            case "h": case "H":
                return date.getHours();
            case "m":
                return date.getMinutes();
            case "s": case "S":
                return date.getSeconds();
        }
    }
    Tools.Signs = Signs;

    window.Date.prototype.format = function (form, padElement) {
        var char;
        var output = form;
        var format = form.match(/([yYmMdDhHsS])\1+/g) || [];
        var values = [];
        var seprator = [];
        for (var i = 0; i < format.length - 1; i++) {
            var sign = format[i];
            seprator.push(form.slice(form.indexOf(sign) + sign.length, form.indexOf(format[i + 1])));
        }

        for (var i = 0; i < format.length; i++) {
            var sign = format[i];
            var length = sign.length;
            var value = Tools.Signs.Date.call(this, sign[0]);
            values[i] = Tools.Format.Pad(value, Tools.Format.Repeat("0", length));
        }

        for (var i = 0; i < format.length; i++)
            output = output.replace(format[i], values[i]);
        return output;
    }

    Tools.timeDif = 0;
    
}());

/*Utility Format*/
(function () {
    var Format = Object.create(null);

    Format.Pad = function (string, format) {
        if (!Test.String(string) && !string.toString && !string.toString())
            return string;

        string = string.toString();
        var length = Math.max(string.length, format.length)
        var result = [];
        if (string.length < length)
            return format.slice(string.length) + string;
        else
            return string;
    }

    Format.Repeat = function (str, count) {
        if (count == null || str == null)
            return str;
        str = "" + str;
        count = Number(count);
        if (count < 0 || count === Infinity)
            return str;

        count = Math.floor(count);
        var result = "";
        return new Array(count + 1).join(str);
    }

    Format.String = function (pattern, args) {
        if (!Test.String(pattern))
            return pattern;

        var result = pattern;
        for (var i = 1; i < arguments.length; i++)
            result = result.replace(new RegExp("\\{" + (i - 1) + "\\}", "gm"), arguments[i]);
        return result;
    }
    String.Format = Format.String;
    Tools.Format = Format;
}());