﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ssm;
using GameLibrary;
using System.Data;

public partial class NewMember : System.Web.UI.Page
{
    IDataBase mysql = new MySQL("unidb");
    string strQuery;
    protected void Page_Load(object sender, EventArgs e) {
        if (!IsPostBack) {
            Label1.Text = "";
            string code = Request.QueryString["code"];
            if (code == null || code == "") {
                Label1.Text = "查無供應商資料!";
                return;
            }

            UserInfo UpperUser = getCompanyInfo(code);
            if (UpperUser.UID == 0) {
                Label1.Text = "查無供應商資料!";
                return;
            }

            /*新增玩家*/
            addMember(UpperUser);

            Panel1.Visible = true;
        }
    }

    public string randomCode(int length) {
        Random serial = new Random(Guid.NewGuid().GetHashCode());

        char sn = new char();
        string strCode = string.Empty;

        int tmp;

        for (int i = 1; i <= length; i++) {
            tmp = serial.Next(0, 3);
            if (tmp == 1) {
                sn = (char)serial.Next(65, 91);
            } else if (tmp == 0) {
                sn = (char)serial.Next(97, 123);
            } else
                sn = (char)serial.Next(48, 58);
            strCode += sn;

        }

        return strCode;
    }

    /// <summary> 新增會員 </summary>
    /// <param name="UpperUser">上線(推薦人資訊)</param>
    public void addMember(UserInfo UpperUser) {
        int ran_length = 6;

        string keyString = "m" + randomCode(ran_length);
        string checkKey = "p" + randomCode(ran_length);
        string nickName = "會員" + GetMemberID();


        while (CkeckAccountExist(keyString)) {
            keyString = "m" + randomCode(ran_length);
        }

        while (ForgetPaCheck(keyString)) {
            keyString = "m" + randomCode(ran_length);
        }

        string ud = getParameter("13");


        mysql.ClearParameters();
        mysql.TryAddParameter("KeyString", keyString);
        mysql.TryAddParameter("BankCredit", ud);
        mysql.TryAddParameter("NickName", nickName);
        mysql.TryAddParameter("CheckKey", checkKey);
        mysql.TryAddParameter("UpLevelId", UpperUser.UID);
        mysql.TryAddParameter("UpLevelTree", UpperUser.upLevelTree + UpperUser.UID + ",");

        var str = "INSERT INTO userinfo (KeyString, BankCredit, bActive, NickName, Check_Key, gid, loginIp, loginTime, IsVerify, autcode,UpLevelId,UpLevelTree,CreateDate)  ";
        str += "VALUES (@KeyString, @BankCredit, 127, @NickName, @CheckKey, 12, '', now(), 1,'',@UpLevelId,@UpLevelTree,now()); ";

        mysql.Execute(str);

        Acc.Text = keyString;
        Pa.Text = checkKey;


    }

    #region DB
    /// <summary> 抓公司資訊(UID,UpLevelTree) </summary>
    /// <param name="InviteCode">推薦碼</param>
    public UserInfo getCompanyInfo(string InviteCode) {
        UserInfo user = new UserInfo();
        user.InviteCode = InviteCode;

        mysql.ClearParameters();
        mysql.TryAddParameter("invitecode", InviteCode);
        strQuery = "SELECT u.UpLevelTree,q.UID FROM qrcode q LEFT JOIN userinfo u ON u.UID=q.UID WHERE inviteCode =@invitecode";
        DataTable dt = mysql.ExecuteDataTable(strQuery);
        if (dt.Rows.Count == 0) {
            return user;
        }

        user.UID = Convert.ToInt32(dt.Rows[0]["UID"]);
        user.upLevelTree = dt.Rows[0]["UpLevelTree"].ToString();

        return user;
    }

    public void insertInviteCode(string inviteCode, string path) {
        mysql.ClearParameters();
        mysql.TryAddParameter("inviteCode", inviteCode);
        mysql.TryAddParameter("path", path);

        strQuery = @"INSERT INTO QRCode (inviteCode, path,CreateDate) " +
                   @"VALUES (@inviteCode, @path,now())";

        mysql.Execute(strQuery);
    }

    public bool checkExistInviteCode(string QRcode) {
        mysql.ClearParameters();
        mysql.TryAddParameter("inviteCode", QRcode);

        strQuery = @"SELECT count(inviteCode) FROM QRCode ";
        strQuery += @" WHERE inviteCode=@inviteCode ";
        if (Convert.ToInt32(mysql.ExecuteScalar(strQuery)) > 0) {
            return true;
        } else
            return false;
    }

    public DataTable getQRList() {
        mysql.ClearParameters();

        strQuery = @"SELECT q.*,u.NickName ";
        strQuery += @" FROM QRCode q LEFT JOIN userinfo u ON u.UID=q.UID ";
        strQuery += @" Order By q.CreateDate Desc ";

        return mysql.ExecuteDataTable(strQuery);
    }

    /// <summary>
    /// 確認資料庫中是否有此帳號
    /// </summary>
    /// <param name="name"></param>
    /// <returns></returns>
    public bool CkeckAccountExist(string account) {
        int tmp;
        mysql.ClearParameters();
        mysql.TryAddParameter("account", account);
        strQuery = "select UID from userinfo where KeyString=@account";
        tmp = Convert.ToInt32(mysql.ExecuteScalar(strQuery));
        if (tmp > 0) {
            return true;
        } else
            return false;
    }


    /// <summary>
    /// 確認是否存在此帳號
    /// </summary>
    /// <param name="account"></param>
    /// <param name="name"></param>
    /// <returns></returns>
    public bool ForgetPaCheck(string account) {
        bool isFind = false;

        mysql.ClearParameters();
        mysql.TryAddParameter("account", account);
        //mysql.parameters.AddWithValue("name", name);
        strQuery = @"SELECT UID FROM userinfo WHERE KeyString=@account";

        int UID = Convert.ToInt32(mysql.ExecuteScalar(strQuery));
        if (UID > 0) {
            isFind = true;

        }

        return isFind;
    }

    public string getParameter(string id) {
        mysql.ClearParameters();
        var strQuery = "select ParametersValue from iiparamenters where id=@id";
        mysql.TryAddParameter("id", id);

        return mysql.ExecuteScalar(strQuery).ToString();

    }

    public long GetMemberID() {
        var cmd = CommandBuilder.Instance;
        cmd.SELECT("Count(uid)").FROM("userinfo");
        var result = Convert.ToInt64(mysql.ExecuteScalar(cmd.Result)) + 1;
        cmd.Free();
        return result;
    }

    #endregion
}