﻿<%@ WebHandler Language="C#" Class="webHandler" %>

using System;
using System.Web;
using Microsoft.Web.WebSockets;

public class webHandler : IHttpHandler {
    public void ProcessRequest(HttpContext context) {
        if (context.IsWebSocketRequest) { }
        context.AcceptWebSocketRequest(new GameLibrary.WSHandler(Controller.GetInstance));
    }

    public bool IsReusable { get { return false; } }
}
